from django.apps import AppConfig



class BinancefutureConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'binancefuture'

    def ready(self):
        from binancfutbot import update
        from binancfutbot import websocketcheck 
        update.start()
        update.start_second()
        # websocketcheck.direc_start()
