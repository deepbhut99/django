from django.db import models
import django_filters


class EfxUsers(models.Model):
    token = models.TextField(blank=True, null=True)
    full_name = models.CharField(max_length=100, blank=True, null=True)
    username = models.CharField(
        unique=True, max_length=100, blank=True, null=True)
    password = models.CharField(max_length=50, blank=True, null=True)
    password_decrypted = models.CharField(
        max_length=100, blank=True, null=True)
    is_sponsor = models.IntegerField()
    sponsor = models.CharField(max_length=50, blank=True, null=True)
    sponsor_id = models.IntegerField(blank=True, null=True)
    email = models.CharField(max_length=150, blank=True, null=True)
    mobile = models.CharField(max_length=20, blank=True, null=True)
    country = models.CharField(max_length=100, blank=True, null=True)
    login_country = models.CharField(max_length=100, blank=True, null=True)
    state = models.CharField(max_length=10)
    city = models.IntegerField()
    address = models.TextField(blank=True, null=True)
    package = models.IntegerField(blank=True, null=True)
    upgrade_time = models.DateTimeField(blank=True, null=True)
    last_login = models.DateTimeField(blank=True, null=True)
    created_time = models.DateTimeField(blank=True, null=True)
    upgrade_agency = models.DateTimeField(blank=True, null=True)
    upgrade_bottrade = models.DateTimeField()
    modified_time = models.DateTimeField(blank=True, null=True)
    lwr = models.IntegerField()
    coin = models.CharField(max_length=6)
    wallet_amount = models.FloatField()
    unlocked_efx = models.FloatField()
    efx_wallet = models.FloatField()
    demo_wallet_amount = models.FloatField()
    copy_trade = models.IntegerField()
    copiers = models.FloatField()
    profit_share = models.FloatField()
    min_invest = models.FloatField()
    users = models.TextField(blank=True, null=True)
    total_user = models.FloatField()
    active_user = models.FloatField()
    is_profile_update = models.IntegerField()
    is_delete = models.IntegerField()
    is_status = models.IntegerField()
    trade_type = models.IntegerField()
    is_agency = models.IntegerField()
    is_bottrade = models.IntegerField()
    is_android_bonus = models.IntegerField()
    google_auth_status = models.CharField(max_length=5)
    google_auth_code = models.CharField(max_length=50, blank=True, null=True)
    my_os = models.CharField(max_length=15, blank=True, null=True)
    is_play = models.IntegerField()
    grouping = models.IntegerField()
    binance_apikey = models.TextField()
    binance_securitykey = models.TextField()
    binance_apikey_active_check = models.IntegerField()
    copy_trade_forbot = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'efx_users'


class Binancefuturebid(models.Model):
    user_id = models.IntegerField()
    created_date = models.BigIntegerField(blank=True, null=True)
    firstbuyinamout = models.FloatField(blank=True, null=True)
    triger_prise = models.FloatField(blank=True, null=True)
    buy_sell_done = models.IntegerField()
    triger_prise_buy = models.FloatField(blank=True, null=True)
    triger_prise_sell = models.FloatField(blank=True, null=True)
    stoploss = models.FloatField(blank=True, null=True)
    takeprofit = models.FloatField(blank=True, null=True)
    quntaty = models.FloatField(blank=True, null=True)
    symbol = models.CharField(max_length=144)
    type = models.CharField(max_length=144)
    # Field name made lowercase.
    clientorderid = models.CharField(
        db_column='clientOrderId', max_length=144, blank=True, null=True)
    # Field name made lowercase.
    cumqty = models.CharField(
        db_column='cumQty', max_length=144, blank=True, null=True)
    # Field name made lowercase.
    executedqty = models.CharField(
        db_column='executedQty', max_length=144, blank=True, null=True)
    # Field name made lowercase.
    orderid = models.CharField(
        db_column='orderId', max_length=144, blank=True, null=True)
    # Field name made lowercase.
    origqty = models.CharField(
        db_column='origQty', max_length=144, blank=True, null=True)
    price = models.CharField(max_length=144, blank=True, null=True)
    side = models.CharField(max_length=144, blank=True, null=True)
    # Field name made lowercase.
    positionside = models.CharField(
        db_column='positionSide', max_length=144, blank=True, null=True)
    status = models.CharField(max_length=144, blank=True, null=True)
    # Field name made lowercase.
    stopprice = models.CharField(
        db_column='stopPrice', max_length=144, blank=True, null=True)
    # Field name made lowercase.
    closeposition = models.CharField(
        db_column='closePosition', max_length=144, blank=True, null=True)
    # Field name made lowercase.
    timeinforce = models.CharField(
        db_column='timeInForce', max_length=144, blank=True, null=True)
    # Field name made lowercase.
    activateprice = models.CharField(
        db_column='activatePrice', max_length=144, blank=True, null=True)
    # Field name made lowercase.
    pricerate = models.CharField(
        db_column='priceRate', max_length=144, blank=True, null=True)
    # Field name made lowercase.
    updatetime = models.CharField(
        db_column='updateTime', max_length=144, blank=True, null=True)
    # Field name made lowercase.
    priceprotect = models.CharField(
        db_column='priceProtect', max_length=144, blank=True, null=True)
    apikey = models.TextField()
    secretkey = models.TextField()

    class Meta:
        managed = False
        db_table = 'binancefuture_binancefuturebid'


class EfxUsersSessionData(models.Model):
    user_id = models.IntegerField()
    user_name = models.CharField(max_length=100)
    user_email = models.CharField(max_length=150)
    full_name = models.CharField(max_length=50, blank=True, null=True)
    trade_type = models.IntegerField(blank=True, null=True)
    logsid = models.IntegerField(blank=True, null=True)
    language = models.CharField(max_length=45, blank=True, null=True)
    lang = models.CharField(max_length=45, blank=True, null=True)
    login_type = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'efx_users_session_data'


class ApidataGetCheckForbinance(models.Model):
    timestamp = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'apidata_get_check_for_binance'


class EfxPercentageChangeAaveusdt(models.Model):
    id = models.AutoField(primary_key=True)
    open = models.FloatField()
    close = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    volume = models.FloatField()
    timestamp = models.BigIntegerField()
    symbol = models.CharField(max_length=144, db_collation='utf8_general_ci')
    pricechange = models.FloatField()
    prisechangepercentage = models.FloatField()
    weightedaverageprice = models.FloatField()
    totaltradebaseasset = models.FloatField()

    class Meta:
        managed = False
        db_table = 'efx_percentage_change_aaveusdt'


class EfxPercentageChangeAdausdt(models.Model):
    id = models.AutoField(primary_key=True)
    open = models.FloatField()
    close = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    volume = models.FloatField()
    timestamp = models.BigIntegerField()
    symbol = models.CharField(max_length=144, db_collation='utf8_general_ci')
    pricechange = models.FloatField()
    prisechangepercentage = models.FloatField()
    weightedaverageprice = models.FloatField()
    totaltradebaseasset = models.FloatField()

    class Meta:
        managed = False
        db_table = 'efx_percentage_change_adausdt'


class EfxPercentageChangeAkrousdt(models.Model):
    id = models.AutoField(primary_key=True)
    open = models.FloatField()
    close = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    volume = models.FloatField()
    timestamp = models.BigIntegerField()
    symbol = models.CharField(max_length=144, db_collation='utf8_general_ci')
    pricechange = models.FloatField()
    prisechangepercentage = models.FloatField()
    weightedaverageprice = models.FloatField()
    totaltradebaseasset = models.FloatField()

    class Meta:
        managed = False
        db_table = 'efx_percentage_change_akrousdt'


class EfxPercentageChangeAntusdt(models.Model):
    id = models.AutoField(primary_key=True)
    open = models.FloatField()
    close = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    volume = models.FloatField()
    timestamp = models.BigIntegerField()
    symbol = models.CharField(max_length=144, db_collation='utf8_general_ci')
    pricechange = models.FloatField()
    prisechangepercentage = models.FloatField()
    weightedaverageprice = models.FloatField()
    totaltradebaseasset = models.FloatField()

    class Meta:
        managed = False
        db_table = 'efx_percentage_change_antusdt'


class EfxPercentageChangeAtomusdt(models.Model):
    id = models.AutoField(primary_key=True)
    open = models.FloatField()
    close = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    volume = models.FloatField()
    timestamp = models.BigIntegerField()
    symbol = models.CharField(max_length=144, db_collation='utf8_general_ci')
    pricechange = models.FloatField()
    prisechangepercentage = models.FloatField()
    weightedaverageprice = models.FloatField()
    totaltradebaseasset = models.FloatField()

    class Meta:
        managed = False
        db_table = 'efx_percentage_change_atomusdt'


class EfxPercentageChangeBatusdt(models.Model):
    id = models.AutoField(primary_key=True)
    open = models.FloatField()
    close = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    volume = models.FloatField()
    timestamp = models.BigIntegerField()
    symbol = models.CharField(max_length=144, db_collation='utf8_general_ci')
    pricechange = models.FloatField()
    prisechangepercentage = models.FloatField()
    weightedaverageprice = models.FloatField()
    totaltradebaseasset = models.FloatField()

    class Meta:
        managed = False
        db_table = 'efx_percentage_change_batusdt'


class EfxPercentageChangeBchusdt(models.Model):
    id = models.AutoField(primary_key=True)
    open = models.FloatField()
    close = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    volume = models.FloatField()
    timestamp = models.BigIntegerField()
    symbol = models.CharField(max_length=144, db_collation='utf8_general_ci')
    pricechange = models.FloatField()
    prisechangepercentage = models.FloatField()
    weightedaverageprice = models.FloatField()
    totaltradebaseasset = models.FloatField()

    class Meta:
        managed = False
        db_table = 'efx_percentage_change_bchusdt'


class EfxPercentageChangeBnbusdt(models.Model):
    id = models.AutoField(primary_key=True)
    open = models.FloatField()
    close = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    volume = models.FloatField()
    timestamp = models.BigIntegerField()
    symbol = models.CharField(max_length=144, db_collation='utf8_general_ci')
    pricechange = models.FloatField()
    prisechangepercentage = models.FloatField()
    weightedaverageprice = models.FloatField()
    totaltradebaseasset = models.FloatField()

    class Meta:
        managed = False
        db_table = 'efx_percentage_change_bnbusdt'


class EfxPercentageChangeBtctusdt(models.Model):
    id = models.AutoField(primary_key=True)
    open = models.FloatField()
    close = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    volume = models.FloatField()
    timestamp = models.BigIntegerField()
    symbol = models.CharField(max_length=144, db_collation='utf8_general_ci')
    pricechange = models.FloatField()
    prisechangepercentage = models.FloatField()
    weightedaverageprice = models.FloatField()
    totaltradebaseasset = models.FloatField()

    class Meta:
        managed = False
        db_table = 'efx_percentage_change_btctusdt'


class EfxPercentageChangeBtcusdt(models.Model):
    id = models.AutoField(primary_key=True)
    open = models.FloatField()
    close = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    volume = models.FloatField()
    timestamp = models.BigIntegerField()
    symbol = models.CharField(max_length=144, db_collation='utf8_general_ci')
    pricechange = models.FloatField()
    prisechangepercentage = models.FloatField()
    weightedaverageprice = models.FloatField()
    totaltradebaseasset = models.FloatField()

    class Meta:
        managed = False
        db_table = 'efx_percentage_change_btcusdt'


class EfxPercentageChangeCakeusdt(models.Model):
    id = models.AutoField(primary_key=True)
    open = models.FloatField()
    close = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    volume = models.FloatField()
    timestamp = models.BigIntegerField()
    symbol = models.CharField(max_length=144, db_collation='utf8_general_ci')
    pricechange = models.FloatField()
    prisechangepercentage = models.FloatField()
    weightedaverageprice = models.FloatField()
    totaltradebaseasset = models.FloatField()

    class Meta:
        managed = False
        db_table = 'efx_percentage_change_cakeusdt'


class EfxPercentageChangeCelrusdt(models.Model):
    id = models.AutoField(primary_key=True)
    open = models.FloatField()
    close = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    volume = models.FloatField()
    timestamp = models.BigIntegerField()
    symbol = models.CharField(max_length=144, db_collation='utf8_general_ci')
    pricechange = models.FloatField()
    prisechangepercentage = models.FloatField()
    weightedaverageprice = models.FloatField()
    totaltradebaseasset = models.FloatField()

    class Meta:
        managed = False
        db_table = 'efx_percentage_change_celrusdt'


class EfxPercentageChangeCompusdt(models.Model):
    id = models.AutoField(primary_key=True)
    open = models.FloatField()
    close = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    volume = models.FloatField()
    timestamp = models.BigIntegerField()
    symbol = models.CharField(max_length=144, db_collation='utf8_general_ci')
    pricechange = models.FloatField()
    prisechangepercentage = models.FloatField()
    weightedaverageprice = models.FloatField()
    totaltradebaseasset = models.FloatField()

    class Meta:
        managed = False
        db_table = 'efx_percentage_change_compusdt'


class EfxPercentageChangeCosusdt(models.Model):
    id = models.AutoField(primary_key=True)
    open = models.FloatField()
    close = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    volume = models.FloatField()
    timestamp = models.BigIntegerField()
    symbol = models.CharField(max_length=144, db_collation='utf8_general_ci')
    pricechange = models.FloatField()
    prisechangepercentage = models.FloatField()
    weightedaverageprice = models.FloatField()
    totaltradebaseasset = models.FloatField()

    class Meta:
        managed = False
        db_table = 'efx_percentage_change_cosusdt'


class EfxPercentageChangeCrvusdt(models.Model):
    id = models.AutoField(primary_key=True)
    open = models.FloatField()
    close = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    volume = models.FloatField()
    timestamp = models.BigIntegerField()
    symbol = models.CharField(max_length=144, db_collation='utf8_general_ci')
    pricechange = models.FloatField()
    prisechangepercentage = models.FloatField()
    weightedaverageprice = models.FloatField()
    totaltradebaseasset = models.FloatField()

    class Meta:
        managed = False
        db_table = 'efx_percentage_change_crvusdt'


class EfxPercentageChangeDashusdt(models.Model):
    id = models.AutoField(primary_key=True)
    open = models.FloatField()
    close = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    volume = models.FloatField()
    timestamp = models.BigIntegerField()
    symbol = models.CharField(max_length=144, db_collation='utf8_general_ci')
    pricechange = models.FloatField()
    prisechangepercentage = models.FloatField()
    weightedaverageprice = models.FloatField()
    totaltradebaseasset = models.FloatField()

    class Meta:
        managed = False
        db_table = 'efx_percentage_change_dashusdt'


class EfxPercentageChangeDogeusdt(models.Model):
    id = models.AutoField(primary_key=True)
    open = models.FloatField()
    close = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    volume = models.FloatField()
    timestamp = models.BigIntegerField()
    symbol = models.CharField(max_length=144, db_collation='utf8_general_ci')
    pricechange = models.FloatField()
    prisechangepercentage = models.FloatField()
    weightedaverageprice = models.FloatField()
    totaltradebaseasset = models.FloatField()

    class Meta:
        managed = False
        db_table = 'efx_percentage_change_dogeusdt'


class EfxPercentageChangeDotusdt(models.Model):
    id = models.AutoField(primary_key=True)
    open = models.FloatField()
    close = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    volume = models.FloatField()
    timestamp = models.BigIntegerField()
    symbol = models.CharField(max_length=144, db_collation='utf8_general_ci')
    pricechange = models.FloatField()
    prisechangepercentage = models.FloatField()
    weightedaverageprice = models.FloatField()
    totaltradebaseasset = models.FloatField()

    class Meta:
        managed = False
        db_table = 'efx_percentage_change_dotusdt'


class EfxPercentageChangeEosusdt(models.Model):
    id = models.AutoField(primary_key=True)
    open = models.FloatField()
    close = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    volume = models.FloatField()
    timestamp = models.BigIntegerField()
    symbol = models.CharField(max_length=144, db_collation='utf8_general_ci')
    pricechange = models.FloatField()
    prisechangepercentage = models.FloatField()
    weightedaverageprice = models.FloatField()
    totaltradebaseasset = models.FloatField()

    class Meta:
        managed = False
        db_table = 'efx_percentage_change_eosusdt'


class EfxPercentageChangeEtcusdt(models.Model):
    id = models.AutoField(primary_key=True)
    open = models.FloatField()
    close = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    volume = models.FloatField()
    timestamp = models.BigIntegerField()
    symbol = models.CharField(max_length=144, db_collation='utf8_general_ci')
    pricechange = models.FloatField()
    prisechangepercentage = models.FloatField()
    weightedaverageprice = models.FloatField()
    totaltradebaseasset = models.FloatField()

    class Meta:
        managed = False
        db_table = 'efx_percentage_change_etcusdt'


class EfxPercentageChangeEthusdt(models.Model):
    id = models.AutoField(primary_key=True)
    open = models.FloatField()
    close = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    volume = models.FloatField()
    timestamp = models.BigIntegerField()
    symbol = models.CharField(max_length=144)
    pricechange = models.FloatField()
    prisechangepercentage = models.FloatField()
    weightedaverageprice = models.FloatField()
    totaltradebaseasset = models.FloatField()

    class Meta:
        managed = False
        db_table = 'efx_percentage_change_ethusdt'


class EfxPercentageChangeFilusdt(models.Model):
    id = models.AutoField(primary_key=True)
    open = models.FloatField()
    close = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    volume = models.FloatField()
    timestamp = models.BigIntegerField()
    symbol = models.CharField(max_length=144)
    pricechange = models.FloatField()
    prisechangepercentage = models.FloatField()
    weightedaverageprice = models.FloatField()
    totaltradebaseasset = models.FloatField()

    class Meta:
        managed = False
        db_table = 'efx_percentage_change_filusdt'


class EfxPercentageChangeFttusdt(models.Model):
    id = models.AutoField(primary_key=True)
    open = models.FloatField()
    close = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    volume = models.FloatField()
    timestamp = models.BigIntegerField()
    symbol = models.CharField(max_length=144)
    pricechange = models.FloatField()
    prisechangepercentage = models.FloatField()
    weightedaverageprice = models.FloatField()
    totaltradebaseasset = models.FloatField()

    class Meta:
        managed = False
        db_table = 'efx_percentage_change_fttusdt'


class EfxPercentageChangeGrtusdt(models.Model):
    id = models.AutoField(primary_key=True)
    open = models.FloatField()
    close = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    volume = models.FloatField()
    timestamp = models.BigIntegerField()
    symbol = models.CharField(max_length=144)
    pricechange = models.FloatField()
    prisechangepercentage = models.FloatField()
    weightedaverageprice = models.FloatField()
    totaltradebaseasset = models.FloatField()

    class Meta:
        managed = False
        db_table = 'efx_percentage_change_grtusdt'


class EfxPercentageChangeIostusdt(models.Model):
    id = models.AutoField(primary_key=True)
    open = models.FloatField()
    close = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    volume = models.FloatField()
    timestamp = models.BigIntegerField()
    symbol = models.CharField(max_length=144)
    pricechange = models.FloatField()
    prisechangepercentage = models.FloatField()
    weightedaverageprice = models.FloatField()
    totaltradebaseasset = models.FloatField()

    class Meta:
        managed = False
        db_table = 'efx_percentage_change_iostusdt'


class EfxPercentageChangeIotausdt(models.Model):
    id = models.AutoField(primary_key=True)
    open = models.FloatField()
    close = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    volume = models.FloatField()
    timestamp = models.BigIntegerField()
    symbol = models.CharField(max_length=144)
    pricechange = models.FloatField()
    prisechangepercentage = models.FloatField()
    weightedaverageprice = models.FloatField()
    totaltradebaseasset = models.FloatField()

    class Meta:
        managed = False
        db_table = 'efx_percentage_change_iotausdt'


class EfxPercentageChangeJstusdt(models.Model):
    id = models.AutoField(primary_key=True)
    open = models.FloatField()
    close = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    volume = models.FloatField()
    timestamp = models.BigIntegerField()
    symbol = models.CharField(max_length=144)
    pricechange = models.FloatField()
    prisechangepercentage = models.FloatField()
    weightedaverageprice = models.FloatField()
    totaltradebaseasset = models.FloatField()

    class Meta:
        managed = False
        db_table = 'efx_percentage_change_jstusdt'


class EfxPercentageChangeKavausdt(models.Model):
    id = models.AutoField(primary_key=True)
    open = models.FloatField()
    close = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    volume = models.FloatField()
    timestamp = models.BigIntegerField()
    symbol = models.CharField(max_length=144)
    pricechange = models.FloatField()
    prisechangepercentage = models.FloatField()
    weightedaverageprice = models.FloatField()
    totaltradebaseasset = models.FloatField()

    class Meta:
        managed = False
        db_table = 'efx_percentage_change_kavausdt'


class EfxPercentageChangeLinkusdt(models.Model):
    id = models.AutoField(primary_key=True)
    open = models.FloatField()
    close = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    volume = models.FloatField()
    timestamp = models.BigIntegerField()
    symbol = models.CharField(max_length=144)
    pricechange = models.FloatField()
    prisechangepercentage = models.FloatField()
    weightedaverageprice = models.FloatField()
    totaltradebaseasset = models.FloatField()

    class Meta:
        managed = False
        db_table = 'efx_percentage_change_linkusdt'


class EfxPercentageChangeLitusdt(models.Model):
    id = models.AutoField(primary_key=True)
    open = models.FloatField()
    close = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    volume = models.FloatField()
    timestamp = models.BigIntegerField()
    symbol = models.CharField(max_length=144)
    pricechange = models.FloatField()
    prisechangepercentage = models.FloatField()
    weightedaverageprice = models.FloatField()
    totaltradebaseasset = models.FloatField()

    class Meta:
        managed = False
        db_table = 'efx_percentage_change_litusdt'


class EfxPercentageChangeLtcusdt(models.Model):
    id = models.AutoField(primary_key=True)
    open = models.FloatField()
    close = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    volume = models.FloatField()
    timestamp = models.BigIntegerField()
    symbol = models.CharField(max_length=144)
    pricechange = models.FloatField()
    prisechangepercentage = models.FloatField()
    weightedaverageprice = models.FloatField()
    totaltradebaseasset = models.FloatField()

    class Meta:
        managed = False
        db_table = 'efx_percentage_change_ltcusdt'


class EfxPercentageChangeManausdt(models.Model):
    id = models.AutoField(primary_key=True)
    open = models.FloatField()
    close = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    volume = models.FloatField()
    timestamp = models.BigIntegerField()
    symbol = models.CharField(max_length=144)
    pricechange = models.FloatField()
    prisechangepercentage = models.FloatField()
    weightedaverageprice = models.FloatField()
    totaltradebaseasset = models.FloatField()

    class Meta:
        managed = False
        db_table = 'efx_percentage_change_manausdt'


class EfxPercentageChangeMaticusdt(models.Model):
    id = models.AutoField(primary_key=True)
    open = models.FloatField()
    close = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    volume = models.FloatField()
    timestamp = models.BigIntegerField()
    symbol = models.CharField(max_length=144)
    pricechange = models.FloatField()
    prisechangepercentage = models.FloatField()
    weightedaverageprice = models.FloatField()
    totaltradebaseasset = models.FloatField()

    class Meta:
        managed = False
        db_table = 'efx_percentage_change_maticusdt'


class EfxPercentageChangeMdxusdt(models.Model):
    id = models.AutoField(primary_key=True)
    open = models.FloatField()
    close = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    volume = models.FloatField()
    timestamp = models.BigIntegerField()
    symbol = models.CharField(max_length=144)
    pricechange = models.FloatField()
    prisechangepercentage = models.FloatField()
    weightedaverageprice = models.FloatField()
    totaltradebaseasset = models.FloatField()

    class Meta:
        managed = False
        db_table = 'efx_percentage_change_mdxusdt'


class EfxPercentageChangeNeousdt(models.Model):
    id = models.AutoField(primary_key=True)
    open = models.FloatField()
    close = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    volume = models.FloatField()
    timestamp = models.BigIntegerField()
    symbol = models.CharField(max_length=144)
    pricechange = models.FloatField()
    prisechangepercentage = models.FloatField()
    weightedaverageprice = models.FloatField()
    totaltradebaseasset = models.FloatField()

    class Meta:
        managed = False
        db_table = 'efx_percentage_change_neousdt'


class EfxPercentageChangeOmgusdt(models.Model):
    id = models.AutoField(primary_key=True)
    open = models.FloatField()
    close = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    volume = models.FloatField()
    timestamp = models.BigIntegerField()
    symbol = models.CharField(max_length=144)
    pricechange = models.FloatField()
    prisechangepercentage = models.FloatField()
    weightedaverageprice = models.FloatField()
    totaltradebaseasset = models.FloatField()

    class Meta:
        managed = False
        db_table = 'efx_percentage_change_omgusdt'


class EfxPercentageChangeRvnusdt(models.Model):
    id = models.AutoField(primary_key=True)
    open = models.FloatField()
    close = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    volume = models.FloatField()
    timestamp = models.BigIntegerField()
    symbol = models.CharField(max_length=144)
    pricechange = models.FloatField()
    prisechangepercentage = models.FloatField()
    weightedaverageprice = models.FloatField()
    totaltradebaseasset = models.FloatField()

    class Meta:
        managed = False
        db_table = 'efx_percentage_change_rvnusdt'


class EfxPercentageChangeShibusdt(models.Model):
    id = models.AutoField(primary_key=True)
    open = models.FloatField()
    close = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    volume = models.FloatField()
    timestamp = models.BigIntegerField()
    symbol = models.CharField(max_length=144)
    pricechange = models.FloatField()
    prisechangepercentage = models.FloatField()
    weightedaverageprice = models.FloatField()
    totaltradebaseasset = models.FloatField()

    class Meta:
        managed = False
        db_table = 'efx_percentage_change_shibusdt'


class EfxPercentageChangeSushiusdt(models.Model):
    id = models.AutoField(primary_key=True)
    open = models.FloatField()
    close = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    volume = models.FloatField()
    timestamp = models.BigIntegerField()
    symbol = models.CharField(max_length=144)
    pricechange = models.FloatField()
    prisechangepercentage = models.FloatField()
    weightedaverageprice = models.FloatField()
    totaltradebaseasset = models.FloatField()

    class Meta:
        managed = False
        db_table = 'efx_percentage_change_sushiusdt'


class EfxPercentageChangeThetausdt(models.Model):
    id = models.AutoField(primary_key=True)
    open = models.FloatField()
    close = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    volume = models.FloatField()
    timestamp = models.BigIntegerField()
    symbol = models.CharField(max_length=144)
    pricechange = models.FloatField()
    prisechangepercentage = models.FloatField()
    weightedaverageprice = models.FloatField()
    totaltradebaseasset = models.FloatField()

    class Meta:
        managed = False
        db_table = 'efx_percentage_change_thetausdt'


class EfxPercentageChangeTrxusdt(models.Model):
    id = models.AutoField(primary_key=True)
    open = models.FloatField()
    close = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    volume = models.FloatField()
    timestamp = models.BigIntegerField()
    symbol = models.CharField(max_length=144)
    pricechange = models.FloatField()
    prisechangepercentage = models.FloatField()
    weightedaverageprice = models.FloatField()
    totaltradebaseasset = models.FloatField()

    class Meta:
        managed = False
        db_table = 'efx_percentage_change_trxusdt'


class EfxPercentageChangeUniusdt(models.Model):
    id = models.AutoField(primary_key=True)
    open = models.FloatField()
    close = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    volume = models.FloatField()
    timestamp = models.BigIntegerField()
    symbol = models.CharField(max_length=144)
    pricechange = models.FloatField()
    prisechangepercentage = models.FloatField()
    weightedaverageprice = models.FloatField()
    totaltradebaseasset = models.FloatField()

    class Meta:
        managed = False
        db_table = 'efx_percentage_change_uniusdt'


class EfxPercentageChangeXmrusdt(models.Model):
    id = models.AutoField(primary_key=True)
    open = models.FloatField()
    close = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    volume = models.FloatField()
    timestamp = models.BigIntegerField()
    symbol = models.CharField(max_length=144)
    pricechange = models.FloatField()
    prisechangepercentage = models.FloatField()
    weightedaverageprice = models.FloatField()
    totaltradebaseasset = models.FloatField()

    class Meta:
        managed = False
        db_table = 'efx_percentage_change_xmrusdt'


class EfxPercentageChangeXrpusdt(models.Model):
    open = models.FloatField()
    close = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    volume = models.FloatField()
    timestamp = models.BigIntegerField()
    symbol = models.CharField(max_length=144)
    pricechange = models.FloatField()
    prisechangepercentage = models.FloatField()
    weightedaverageprice = models.FloatField()
    totaltradebaseasset = models.FloatField()

    class Meta:
        managed = False
        db_table = 'efx_percentage_change_xrpusdt'


class EfxPercentageChangeXtzusdt(models.Model):
    open = models.FloatField()
    close = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    volume = models.FloatField()
    timestamp = models.BigIntegerField()
    symbol = models.CharField(max_length=144)
    pricechange = models.FloatField()
    prisechangepercentage = models.FloatField()
    weightedaverageprice = models.FloatField()
    totaltradebaseasset = models.FloatField()

    class Meta:
        managed = False
        db_table = 'efx_percentage_change_xtzusdt'
