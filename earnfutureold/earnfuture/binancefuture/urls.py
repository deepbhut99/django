from django.urls import path, re_path
from . import views


urlpatterns = [
    path("", views.index, name="index"),
    path("id/<str:userdetails>", views.check, name="check"),
    path("logout", views.logout, name="logout"),
    path("trading", views.trading, name="trading"),
    path("trade", views.trade, name="trade"),
    path("getuserinfo", views.getuserinfo, name="getuserinfo"),
    path("bottradefuture", views.bottradefuture, name="bottradefuture"),
    path("bottradefuture/<str:coinname>", views.bottradefuture, name="bottradefuture"),
    path("adddata", views.adddata, name="adddata"),
    path("logout_outer", views.logout_outer, name="logout_outer"),
    
]
