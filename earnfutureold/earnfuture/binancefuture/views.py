from tkinter import CURRENT
from unicodedata import decimal
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, HttpResponse
from django.http import JsonResponse
from operator import itemgetter
from imp import reload
import requests
from regex import P
# Create your views here.
from binancefuture.models import *
from django.forms.models import model_to_dict
from binance.futures import Futures
import datetime
from django.db import connection
from cryptography.fernet import Fernet
import json
from decimal import *
import time


def index(request):
    if "user_id" in request.session:
        request.session.modified = True
        return render(request, "bot/trade.html")
    else:
        # return render(request, "bot/trade.html")
        return redirect("https://www.earnfinex.com/")


def trading(request):
    if "user_id" in request.session:
        return render(request, "bot/trade.html")
    else:
        return redirect("https://www.earnfinex.com/")


def logout(request):
    try:
        del request.session["user_id"]
        return redirect("https://www.earnfinex.com/")
    except KeyError:
        return redirect("https://www.earnfinex.com/")

def logout_outer(request):
    try:
        del request.session["user_id"]
        responseData = {"success": 1}
        return JsonResponse(responseData)

    except KeyError:
        responseData = {"success": 0}
        return JsonResponse(responseData)

    

def check(request, userdetails):
    user = EfxUsersSessionData.objects.filter(login_type=userdetails).exists()
    if user:
        user_login_ip_detupdate = EfxUsersSessionData.objects.filter(
            login_type=userdetails
        ).order_by('-id')[0]
        request.session["user_id"] = user_login_ip_detupdate.user_id
        return redirect("trading")
    else:
        return redirect("https://www.earnfinex.com/")


def trade(request):
    responseData = {"id": 4, "name": "Test Response",
                    "roles": ["Admin", "User"]}
    return JsonResponse(responseData)


def getuserinfo(request):
    one_entry = EfxUsers.objects.get(id=364)
    one_entry = model_to_dict(one_entry)
    responseData = {"data": one_entry}
    return JsonResponse(responseData, safe=False)


def bottradefuture(request, coinname):
    # if "user_id" in request.session:
    user_id = 364
    one_entry = EfxUsers.objects.all()
    binance_apikey = EfxUsers.objects.values_list("binance_apikey", flat=True).get(
        id=user_id
    )
    binance_security = EfxUsers.objects.values_list(
        "binance_securitykey", flat=True
    ).get(id=user_id)

    # new connection generation
    client = Futures(binance_apikey, binance_security)

    # check balance
    data = client.balance()
    if not data:
        balance = 0
    else:
        for row in data:
            if row["asset"] == "USDT":
                usdt_balance = row["balance"]

    # leverage value
    params = {"symbol": coinname}
    leverage = client.leverage_brackets(**params)
    if not leverage:
        leverage = 0
    else:
        leverage = list(map(itemgetter("brackets"), leverage))
        for levr in leverage:
            length = len(levr)
            for levrage in levr:
                if levrage["bracket"] == 1:
                    higestvalue = levrage["initialLeverage"]
                if levrage["bracket"] == length:
                    lowest = levrage["initialLeverage"]
                    maximum = levrage["notionalCap"]

    # balance
    balance = client.account()
    margin_type = margintype(balance, coinname)
    # minimum value for size
    exchange_info = exchangeinfo(client, coinname)
    exchange_info = exchange_info['stepSize']
    current_price = client.mark_price(**params)
    current_price = current_price['markPrice']
    current_price_final = Decimal(current_price) * Decimal(exchange_info)
    if current_price_final < 5:
        current_price = new_currentprice(current_price, exchange_info)

    context = {
        "coin_n": coinname,
        "balance": usdt_balance,
        "leveragehigest": higestvalue,
        "leveragelowest": lowest,
        "maximum": maximum,
        "name": request.session.get("name", default="Guest"),
        "exchange_info": exchange_info,
        "current_price": current_price,
        "totalWalletBalance": margin_type['totalWalletBalance'],
        "totalMarginBalance": margin_type['totalMarginBalance'],
        'isolated': margin_type['isolated'],
        'positionAmt': Decimal(margin_type['positionAmt']),
        'unrealizedProfit': margin_type['unrealizedProfit'],
        'leverageset': margin_type['leverageset'],
    }
    return render(request, "bot/coin_status.html", context)


def exchangeinfo(client, coinname):
    exchange_info = client.exchange_info()
    if exchange_info['symbols']:
        for symbol in exchange_info['symbols']:
            if symbol['symbol'] == coinname:
                data = symbol['filters']
                data = data[2]
                return data


def new_currentprice(current_price, exchange_info):
    cur = [1]
    for i in cur:
        i = Decimal(i)
        current_pric = Decimal(current_price) * Decimal(i)
        if current_pric > 5:
            return i
        else:
            # increce
            j = Decimal(0.1)
            i = i + j
            i = str(i)
            cur.append(i)


def margintype(balance, coinname):
    if balance['totalMarginBalance'] and balance['totalWalletBalance']:
        totalMarginBalance = balance['totalMarginBalance']
        totalWalletBalance = balance['totalWalletBalance']
    else:
        totalMarginBalance = 0
        totalWalletBalance = 0

    margin_type = balance['positions']

    for k in margin_type:
        if k['symbol'] == coinname:
            isolated = k['isolated']
            positionAmt = k['positionAmt']
            unrealizedProfit = k['unrealizedProfit']
            leverageset = k['leverage']

    # data return
    data = {
        'leverageset': leverageset,
        'unrealizedProfit': unrealizedProfit,
        'positionAmt': positionAmt,
        'isolated': isolated,
        'totalMarginBalance': totalMarginBalance,
        'totalWalletBalance': totalWalletBalance,
    }
    return data


def adddata(request):
    # if "user_id" in request.session:
    userid = request.session.get("user_id", default="364")
    levrageperce = request.POST["levrageperce"]
    buyprice = Decimal(request.POST["buyprice"])
    size = Decimal(request.POST["size"])
    buypoint = Decimal(request.POST["buypoint"])
    sellpoint = Decimal(request.POST["sellpoint"])
    curenttime = int(time.time())
    symbol = request.POST["symbol"]
    side = request.POST["side"]
    typeofbid = request.POST["type"]
    if (
        levrageperce
        and buyprice
        and size
        and buypoint
        and sellpoint
        and symbol
        and userid
        and side
        and typeofbid
    ):
        if side == "BOT":
            try:
                insert = Binancefuturebid(
                    user_id=userid,
                    created_date=curenttime,
                    firstbuyinamout=buyprice,
                    buy_sell_done=1,
                    triger_prise_buy=buypoint,
                    triger_prise_sell=sellpoint,
                    quntaty=size,
                    symbol=symbol,
                    type=typeofbid,
                )
                insert.save()
                msg = "Done"
            except Exception as e:
                print(e)
                msg = "Something want wrong"
            context = {
                "succ": "true",
                "msg": msg,
            }
            return JsonResponse(context, safe=False)
        else:
            binance_apikey = EfxUsers.objects.values_list("binance_apikey", flat=True).get(
                id=userid
            )
            binance_security = EfxUsers.objects.values_list(
                "binance_securitykey", flat=True
            ).get(id=userid)
            client = Futures(binance_apikey, binance_security)
            levrageperce = int(levrageperce)

            # getinfo = client.exchange_info()
            # print(getinfo)
            try:
                # change margin type
                try:
                    changemtype = {"symbol": symbol, "marginType": typeofbid}
                    changemargintype = client.change_margin_type(**changemtype)
                except Exception as e:
                    error = str(e)
                    err_msg_convert_json = eval(error)
                    only_msg = err_msg_convert_json[2]
                    context = {
                        "succ": "true",
                        "msg": only_msg,
                    }
                    pass

                # first change levrage
                try:
                    levrage = {"symbol": symbol, "leverage": levrageperce}
                    response = client.change_leverage(**levrage)
                    print(response)
                except Exception as e:
                    error = str(e)
                    err_msg_convert_json = eval(error)
                    only_msg = err_msg_convert_json[2]
                    context = {
                        "succ": "true",
                        "msg": only_msg,
                    }
                    pass
                    # return JsonResponse(context, safe=False)
                # create new order
                try:
                    buysellnew = {
                        'symbol': symbol,
                        'side': side,
                        'type': 'LIMIT',
                        'timeInForce': 'GTC',
                        'quantity': size,
                        'price': buyprice
                    }
                    buysell = client.new_order_test(**buysellnew)
                    # print(buysell)
                    if buysell:
                        buy_sell_done = 3
                        if 'clientOrderId' in buysell:
                            clientOrderId = buysell['clientOrderId']
                        else:
                            clientOrderId = 0
                        if 'cumQuote' in buysell:
                            cumQty = buysell['cumQuote']
                        else:
                            cumQty = 0
                        if 'executedQty' in buysell:
                            executedQty = buysell['executedQty']
                        else:
                            executedQty = 0
                        if 'orderId' in buysell:
                            orderId = buysell['orderId']
                        else:
                            orderId = 0
                        if 'origQty' in buysell:
                            origQty = buysell['origQty']
                        else:
                            origQty = 0
                        if 'price' in buysell:
                            price = buysell['price']
                        else:
                            price = 0
                        if 'side' in buysell:
                            side = buysell['side']
                        else:
                            side = 0
                        if 'positionSide' in buysell:
                            positionSide = buysell['positionSide']
                        else:
                            positionSide = 0
                        if 'status' in buysell:
                            status = buysell['status']
                        else:
                            status = 0
                        if 'stopPrice' in buysell:
                            stopPrice = buysell['stopPrice']
                        else:
                            stopPrice = 0
                        if 'closePosition' in buysell:
                            closePosition = buysell['closePosition']
                        else:
                            closePosition = 0
                        if 'timeInForce' in buysell:
                            timeInForce = buysell['timeInForce']
                        else:
                            timeInForce = 0
                        if 'activatePrice' in buysell:
                            activatePrice = buysell['activatePrice']
                        else:
                            activatePrice = 0
                        if 'priceRate' in buysell:
                            priceRate = buysell['priceRate']
                        else:
                            priceRate = 0
                        if 'updateTime' in buysell:
                            updateTime = buysell['updateTime']
                        else:
                            updateTime = 0
                        if 'priceProtect' in buysell:
                            priceProtect = buysell['priceProtect']
                        else:
                            priceProtect = 0
                    else:
                        buy_sell_done = 3
                        clientOrderId = "0"
                        cumQty = "0"
                        executedQty = "0"
                        orderId = "0"
                        origQty = "0"
                        price = "0"
                        side = "0"
                        positionSide = "0"
                        status = "0"
                        stopPrice = "0"
                        closePosition = "0"
                        timeInForce = "0"
                        activatePrice = "0"
                        priceRate = "0"
                        updateTime = "0"
                        priceProtect = "0"

                except Exception as e:
                    error = str(e)
                    err_msg_convert_json = eval(error)
                    only_msg = err_msg_convert_json[2]
                    context = {
                        "succ": "false",
                        "msg": only_msg,
                    }
                    return JsonResponse(context, safe=False)
                # set stop loss

                # set take profit

                # insert data
                try:
                    insert = Binancefuturebid(
                        user_id=userid,
                        created_date=curenttime,
                        firstbuyinamout=buyprice,
                        buy_sell_done=buy_sell_done,
                        triger_prise_buy=buypoint,
                        triger_prise_sell=sellpoint,
                        quntaty=size,
                        symbol=symbol,
                        type=typeofbid,
                        clientorderid=str(clientOrderId),
                        cumqty=str(cumQty),
                        executedqty=str(executedQty),
                        orderid=str(orderId),
                        origqty=str(origQty),
                        price=str(price),
                        side=str(side),
                        positionside=str(positionSide),
                        status=str(status),
                        stopprice=str(stopPrice),
                        closeposition=str(closePosition),
                        timeinforce=str(timeInForce),
                        activateprice=str(activatePrice),
                        pricerate=str(priceRate),
                        updatetime=str(updateTime),
                        priceprotect=str(priceProtect),
                    )
                    insert.save()

                except Exception as e:
                    print(e)
                context = {
                    "succ": "true",
                    "msg": "Done",
                }
                return JsonResponse(context, safe=False)
            except Exception as e:
                print(e)
                succ = False
                return JsonResponse(succ, safe=False)
    else:
        context = {
            "succ": "true",
            "msg": "Done",
        }
        return JsonResponse(context, safe=False)
