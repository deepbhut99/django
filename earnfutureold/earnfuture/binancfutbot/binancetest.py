from django.conf import settings
import requests
import json
import random
from binancefuture.models import *
import asyncio
import websockets
import datetime
from asgiref.sync import sync_to_async
import os
import django
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'rest.settings')
os.environ["DJANGO_ALLOW_ASYNC_UNSAFE"] = "true"



def update_time(x):
    timestamp = ApidataGetCheckForbinance.objects.filter(
        id=1).update(timestamp=x)
    return timestamp


async def main():
    async with websockets.connect("wss://stream.binance.com:9443/ws/!ticker@arr") as ws:
        try:
            while True:
                x = datetime.datetime.now()
                x = x.strftime("%Y-%m-%d %H:%M:%S %p")
                msg = await ws.recv()
                print('websocket data store')
                input_dict = json.loads(msg)
                for value in input_dict:
                    if value["s"] == "1INCHUSDT":
                        user_id = update_time(x)
                        
                    if value["s"] == "AAVEUSDT":
                        # /////// get value \\\\\\\\\\\\\
                        open = value["o"]
                        high = value["h"]
                        low = value["l"]
                        close = value["c"]
                        volume = value["v"]
                        timestamp = value["E"]
                        symbol = value["s"]
                        pricechange = value["p"]
                        prisechangepercentage = value["P"]
                        weightedaverageprice = value["w"]
                        totaltradebaseasset = value["v"]

                        AAVEUSDT_data = EfxPercentageChangeAaveusdt(
                            open=open,
                            close=close,
                            high=high,
                            low=low,
                            timestamp=timestamp,
                            symbol=symbol,
                            volume=volume,
                            pricechange=pricechange,
                            prisechangepercentage=prisechangepercentage,
                            weightedaverageprice=weightedaverageprice,
                            totaltradebaseasset=totaltradebaseasset)
                        AAVEUSDT_data.save()
                    if value["s"] == "ADAUSDT":
                        # /////// get value \\\\\\\\\\\\\
                        open = value["o"]
                        high = value["h"]
                        low = value["l"]
                        close = value["c"]
                        volume = value["v"]
                        timestamp = value["E"]
                        symbol = value["s"]
                        pricechange = value["p"]
                        prisechangepercentage = value["P"]
                        weightedaverageprice = value["w"]
                        totaltradebaseasset = value["v"]

                        ADAUSDT_data = EfxPercentageChangeAdausdt(
                            open=open,
                            close=close,
                            high=high,
                            low=low,
                            timestamp=timestamp,
                            symbol=symbol,
                            volume=volume,
                            pricechange=pricechange,
                            prisechangepercentage=prisechangepercentage,
                            weightedaverageprice=weightedaverageprice,
                            totaltradebaseasset=totaltradebaseasset)
                        ADAUSDT_data.save()

                    if value["s"] == "AKROUSDT":
                        # /////// get value \\\\\\\\\\\\\
                        open = value["o"]
                        high = value["h"]
                        low = value["l"]
                        close = value["c"]
                        volume = value["v"]
                        timestamp = value["E"]
                        symbol = value["s"]
                        pricechange = value["p"]
                        prisechangepercentage = value["P"]
                        weightedaverageprice = value["w"]
                        totaltradebaseasset = value["v"]

                        AKROUSDT_data = EfxPercentageChangeAkrousdt(
                            open=open,
                            close=close,
                            high=high,
                            low=low,
                            timestamp=timestamp,
                            symbol=symbol,
                            volume=volume,
                            pricechange=pricechange,
                            prisechangepercentage=prisechangepercentage,
                            weightedaverageprice=weightedaverageprice,
                            totaltradebaseasset=totaltradebaseasset)
                        AKROUSDT_data.save()
                    if value["s"] == "ANTUSDT":
                        # /////// get value \\\\\\\\\\\\\
                        open = value["o"]
                        high = value["h"]
                        low = value["l"]
                        close = value["c"]
                        volume = value["v"]
                        timestamp = value["E"]
                        symbol = value["s"]
                        pricechange = value["p"]
                        prisechangepercentage = value["P"]
                        weightedaverageprice = value["w"]
                        totaltradebaseasset = value["v"]

                        ANTUSDT_data = EfxPercentageChangeAntusdt(
                            open=open,
                            close=close,
                            high=high,
                            low=low,
                            timestamp=timestamp,
                            symbol=symbol,
                            volume=volume,
                            pricechange=pricechange,
                            prisechangepercentage=prisechangepercentage,
                            weightedaverageprice=weightedaverageprice,
                            totaltradebaseasset=totaltradebaseasset)
                        ANTUSDT_data.save()
                    if value["s"] == "ATOMUSDT":
                        # /////// get value \\\\\\\\\\\\\
                        open = value["o"]
                        high = value["h"]
                        low = value["l"]
                        close = value["c"]
                        volume = value["v"]
                        timestamp = value["E"]
                        symbol = value["s"]
                        pricechange = value["p"]
                        prisechangepercentage = value["P"]
                        weightedaverageprice = value["w"]
                        totaltradebaseasset = value["v"]

                        ATOMUSDT_data = EfxPercentageChangeAtomusdt(
                            open=open,
                            close=close,
                            high=high,
                            low=low,
                            timestamp=timestamp,
                            symbol=symbol,
                            volume=volume,
                            pricechange=pricechange,
                            prisechangepercentage=prisechangepercentage,
                            weightedaverageprice=weightedaverageprice,
                            totaltradebaseasset=totaltradebaseasset)
                        ATOMUSDT_data.save()
                    if value["s"] == "BATUSDT":
                        # /////// get value \\\\\\\\\\\\\
                        open = value["o"]
                        high = value["h"]
                        low = value["l"]
                        close = value["c"]
                        volume = value["v"]
                        timestamp = value["E"]
                        symbol = value["s"]
                        pricechange = value["p"]
                        prisechangepercentage = value["P"]
                        weightedaverageprice = value["w"]
                        totaltradebaseasset = value["v"]

                        BATUSDT_data = EfxPercentageChangeBatusdt(
                            open=open,
                            close=close,
                            high=high,
                            low=low,
                            timestamp=timestamp,
                            symbol=symbol,
                            volume=volume,
                            pricechange=pricechange,
                            prisechangepercentage=prisechangepercentage,
                            weightedaverageprice=weightedaverageprice,
                            totaltradebaseasset=totaltradebaseasset)
                        BATUSDT_data.save()
                    if value["s"] == "BCHUSDT":
                        # /////// get value \\\\\\\\\\\\\
                        open = value["o"]
                        high = value["h"]
                        low = value["l"]
                        close = value["c"]
                        volume = value["v"]
                        timestamp = value["E"]
                        symbol = value["s"]
                        pricechange = value["p"]
                        prisechangepercentage = value["P"]
                        weightedaverageprice = value["w"]
                        totaltradebaseasset = value["v"]

                        BCHUSDT_data = EfxPercentageChangeBchusdt(
                            open=open,
                            close=close,
                            high=high,
                            low=low,
                            timestamp=timestamp,
                            symbol=symbol,
                            volume=volume,
                            pricechange=pricechange,
                            prisechangepercentage=prisechangepercentage,
                            weightedaverageprice=weightedaverageprice,
                            totaltradebaseasset=totaltradebaseasset)
                        BCHUSDT_data.save()
                    if value["s"] == "BNBUSDT":
                        # /////// get value \\\\\\\\\\\\\
                        open = value["o"]
                        high = value["h"]
                        low = value["l"]
                        close = value["c"]
                        volume = value["v"]
                        timestamp = value["E"]
                        symbol = value["s"]
                        pricechange = value["p"]
                        prisechangepercentage = value["P"]
                        weightedaverageprice = value["w"]
                        totaltradebaseasset = value["v"]

                        BNBUSDT_data = EfxPercentageChangeBnbusdt(
                            open=open,
                            close=close,
                            high=high,
                            low=low,
                            timestamp=timestamp,
                            symbol=symbol,
                            volume=volume,
                            pricechange=pricechange,
                            prisechangepercentage=prisechangepercentage,
                            weightedaverageprice=weightedaverageprice,
                            totaltradebaseasset=totaltradebaseasset)
                        BNBUSDT_data.save()
                    if value["s"] == "BTCUSDT":
                        # /////// get value \\\\\\\\\\\\\
                        open = value["o"]
                        high = value["h"]
                        low = value["l"]
                        close = value["c"]
                        volume = value["v"]
                        timestamp = value["E"]
                        symbol = value["s"]
                        pricechange = value["p"]
                        prisechangepercentage = value["P"]
                        weightedaverageprice = value["w"]
                        totaltradebaseasset = value["v"]

                        BTCUSDT_data = EfxPercentageChangeBtcusdt(
                            open=open,
                            close=close,
                            high=high,
                            low=low,
                            timestamp=timestamp,
                            symbol=symbol,
                            volume=volume,
                            pricechange=pricechange,
                            prisechangepercentage=prisechangepercentage,
                            weightedaverageprice=weightedaverageprice,
                            totaltradebaseasset=totaltradebaseasset)
                        BTCUSDT_data.save()
                    if value["s"] == "CAKEUSDT":
                        # /////// get value \\\\\\\\\\\\\
                        open = value["o"]
                        high = value["h"]
                        low = value["l"]
                        close = value["c"]
                        volume = value["v"]
                        timestamp = value["E"]
                        symbol = value["s"]
                        pricechange = value["p"]
                        prisechangepercentage = value["P"]
                        weightedaverageprice = value["w"]
                        totaltradebaseasset = value["v"]

                        CAKEUSDT_data = EfxPercentageChangeCakeusdt(
                            open=open,
                            close=close,
                            high=high,
                            low=low,
                            timestamp=timestamp,
                            symbol=symbol,
                            volume=volume,
                            pricechange=pricechange,
                            prisechangepercentage=prisechangepercentage,
                            weightedaverageprice=weightedaverageprice,
                            totaltradebaseasset=totaltradebaseasset)
                        CAKEUSDT_data.save()
                    if value["s"] == "COMPUSDT":
                        # /////// get value \\\\\\\\\\\\\
                        open = value["o"]
                        high = value["h"]
                        low = value["l"]
                        close = value["c"]
                        volume = value["v"]
                        timestamp = value["E"]
                        symbol = value["s"]
                        pricechange = value["p"]
                        prisechangepercentage = value["P"]
                        weightedaverageprice = value["w"]
                        totaltradebaseasset = value["v"]

                        COMPUSDT_data = EfxPercentageChangeCompusdt(
                            open=open,
                            close=close,
                            high=high,
                            low=low,
                            timestamp=timestamp,
                            symbol=symbol,
                            volume=volume,
                            pricechange=pricechange,
                            prisechangepercentage=prisechangepercentage,
                            weightedaverageprice=weightedaverageprice,
                            totaltradebaseasset=totaltradebaseasset)
                        COMPUSDT_data.save()
                    if value["s"] == "COSUSDT":
                        # /////// get value \\\\\\\\\\\\\
                        open = value["o"]
                        high = value["h"]
                        low = value["l"]
                        close = value["c"]
                        volume = value["v"]
                        timestamp = value["E"]
                        symbol = value["s"]
                        pricechange = value["p"]
                        prisechangepercentage = value["P"]
                        weightedaverageprice = value["w"]
                        totaltradebaseasset = value["v"]

                        COSUSDT_data = EfxPercentageChangeCosusdt(
                            open=open,
                            close=close,
                            high=high,
                            low=low,
                            timestamp=timestamp,
                            symbol=symbol,
                            volume=volume,
                            pricechange=pricechange,
                            prisechangepercentage=prisechangepercentage,
                            weightedaverageprice=weightedaverageprice,
                            totaltradebaseasset=totaltradebaseasset)
                        COSUSDT_data.save()
                    if value["s"] == "CRVUSDT":
                        # /////// get value \\\\\\\\\\\\\
                        open = value["o"]
                        high = value["h"]
                        low = value["l"]
                        close = value["c"]
                        volume = value["v"]
                        timestamp = value["E"]
                        symbol = value["s"]
                        pricechange = value["p"]
                        prisechangepercentage = value["P"]
                        weightedaverageprice = value["w"]
                        totaltradebaseasset = value["v"]

                        CRVUSDT_data = EfxPercentageChangeCrvusdt(
                            open=open,
                            close=close,
                            high=high,
                            low=low,
                            timestamp=timestamp,
                            symbol=symbol,
                            volume=volume,
                            pricechange=pricechange,
                            prisechangepercentage=prisechangepercentage,
                            weightedaverageprice=weightedaverageprice,
                            totaltradebaseasset=totaltradebaseasset)
                        CRVUSDT_data.save()
                    if value["s"] == "DASHUSDT":
                        # /////// get value \\\\\\\\\\\\\
                        open = value["o"]
                        high = value["h"]
                        low = value["l"]
                        close = value["c"]
                        volume = value["v"]
                        timestamp = value["E"]
                        symbol = value["s"]
                        pricechange = value["p"]
                        prisechangepercentage = value["P"]
                        weightedaverageprice = value["w"]
                        totaltradebaseasset = value["v"]

                        DASHUSDT_data = EfxPercentageChangeDashusdt(
                            open=open,
                            close=close,
                            high=high,
                            low=low,
                            timestamp=timestamp,
                            symbol=symbol,
                            volume=volume,
                            pricechange=pricechange,
                            prisechangepercentage=prisechangepercentage,
                            weightedaverageprice=weightedaverageprice,
                            totaltradebaseasset=totaltradebaseasset)
                        DASHUSDT_data.save()
                    if value["s"] == "DOGEUSDT":
                        # /////// get value \\\\\\\\\\\\\
                        open = value["o"]
                        high = value["h"]
                        low = value["l"]
                        close = value["c"]
                        volume = value["v"]
                        timestamp = value["E"]
                        symbol = value["s"]
                        pricechange = value["p"]
                        prisechangepercentage = value["P"]
                        weightedaverageprice = value["w"]
                        totaltradebaseasset = value["v"]

                        DOGEUSDT_data = EfxPercentageChangeDogeusdt(
                            open=open,
                            close=close,
                            high=high,
                            low=low,
                            timestamp=timestamp,
                            symbol=symbol,
                            volume=volume,
                            pricechange=pricechange,
                            prisechangepercentage=prisechangepercentage,
                            weightedaverageprice=weightedaverageprice,
                            totaltradebaseasset=totaltradebaseasset)
                        DOGEUSDT_data.save()
                    if value["s"] == "DOTUSDT":
                        # /////// get value \\\\\\\\\\\\\
                        open = value["o"]
                        high = value["h"]
                        low = value["l"]
                        close = value["c"]
                        volume = value["v"]
                        timestamp = value["E"]
                        symbol = value["s"]
                        pricechange = value["p"]
                        prisechangepercentage = value["P"]
                        weightedaverageprice = value["w"]
                        totaltradebaseasset = value["v"]

                        DOTUSDT_data = EfxPercentageChangeDotusdt(
                            open=open,
                            close=close,
                            high=high,
                            low=low,
                            timestamp=timestamp,
                            symbol=symbol,
                            volume=volume,
                            pricechange=pricechange,
                            prisechangepercentage=prisechangepercentage,
                            weightedaverageprice=weightedaverageprice,
                            totaltradebaseasset=totaltradebaseasset)
                        DOTUSDT_data.save()
                    if value["s"] == "EOSUSDT":
                        # /////// get value \\\\\\\\\\\\\
                        open = value["o"]
                        high = value["h"]
                        low = value["l"]
                        close = value["c"]
                        volume = value["v"]
                        timestamp = value["E"]
                        symbol = value["s"]
                        pricechange = value["p"]
                        prisechangepercentage = value["P"]
                        weightedaverageprice = value["w"]
                        totaltradebaseasset = value["v"]

                        EOSUSDT_data = EfxPercentageChangeEosusdt(
                            open=open,
                            close=close,
                            high=high,
                            low=low,
                            timestamp=timestamp,
                            symbol=symbol,
                            volume=volume,
                            pricechange=pricechange,
                            prisechangepercentage=prisechangepercentage,
                            weightedaverageprice=weightedaverageprice,
                            totaltradebaseasset=totaltradebaseasset)
                        EOSUSDT_data.save()
                    if value["s"] == "ETHUSDT":
                        # /////// get value \\\\\\\\\\\\\
                        open = value["o"]
                        high = value["h"]
                        low = value["l"]
                        close = value["c"]
                        volume = value["v"]
                        timestamp = value["E"]
                        symbol = value["s"]
                        pricechange = value["p"]
                        prisechangepercentage = value["P"]
                        weightedaverageprice = value["w"]
                        totaltradebaseasset = value["v"]

                        ETHUSDT_data = EfxPercentageChangeEtcusdt(
                            open=open,
                            close=close,
                            high=high,
                            low=low,
                            timestamp=timestamp,
                            symbol=symbol,
                            volume=volume,
                            pricechange=pricechange,
                            prisechangepercentage=prisechangepercentage,
                            weightedaverageprice=weightedaverageprice,
                            totaltradebaseasset=totaltradebaseasset)
                        ETHUSDT_data.save()
                    if value["s"] == "ETCUSDT":
                        # /////// get value \\\\\\\\\\\\\
                        open = value["o"]
                        high = value["h"]
                        low = value["l"]
                        close = value["c"]
                        volume = value["v"]
                        timestamp = value["E"]
                        symbol = value["s"]
                        pricechange = value["p"]
                        prisechangepercentage = value["P"]
                        weightedaverageprice = value["w"]
                        totaltradebaseasset = value["v"]

                        ETCUSDT_data = EfxPercentageChangeEthusdt(
                            open=open,
                            close=close,
                            high=high,
                            low=low,
                            timestamp=timestamp,
                            symbol=symbol,
                            volume=volume,
                            pricechange=pricechange,
                            prisechangepercentage=prisechangepercentage,
                            weightedaverageprice=weightedaverageprice,
                            totaltradebaseasset=totaltradebaseasset)
                        ETCUSDT_data.save()
                    if value["s"] == "FILUSDT":
                        # /////// get value \\\\\\\\\\\\\
                        open = value["o"]
                        high = value["h"]
                        low = value["l"]
                        close = value["c"]
                        volume = value["v"]
                        timestamp = value["E"]
                        symbol = value["s"]
                        pricechange = value["p"]
                        prisechangepercentage = value["P"]
                        weightedaverageprice = value["w"]
                        totaltradebaseasset = value["v"]

                        FILUSDT_data = EfxPercentageChangeFilusdt(
                            open=open,
                            close=close,
                            high=high,
                            low=low,
                            timestamp=timestamp,
                            symbol=symbol,
                            volume=volume,
                            pricechange=pricechange,
                            prisechangepercentage=prisechangepercentage,
                            weightedaverageprice=weightedaverageprice,
                            totaltradebaseasset=totaltradebaseasset)
                        FILUSDT_data.save()
                    if value["s"] == "FTTUSDT":
                        # /////// get value \\\\\\\\\\\\\
                        open = value["o"]
                        high = value["h"]
                        low = value["l"]
                        close = value["c"]
                        volume = value["v"]
                        timestamp = value["E"]
                        symbol = value["s"]
                        pricechange = value["p"]
                        prisechangepercentage = value["P"]
                        weightedaverageprice = value["w"]
                        totaltradebaseasset = value["v"]

                        FTTUSDT_data = EfxPercentageChangeFttusdt(
                            open=open,
                            close=close,
                            high=high,
                            low=low,
                            timestamp=timestamp,
                            symbol=symbol,
                            volume=volume,
                            pricechange=pricechange,
                            prisechangepercentage=prisechangepercentage,
                            weightedaverageprice=weightedaverageprice,
                            totaltradebaseasset=totaltradebaseasset)
                        FTTUSDT_data.save()
                    if value["s"] == "GRTUSDT":
                        # /////// get value \\\\\\\\\\\\\
                        open = value["o"]
                        high = value["h"]
                        low = value["l"]
                        close = value["c"]
                        volume = value["v"]
                        timestamp = value["E"]
                        symbol = value["s"]
                        pricechange = value["p"]
                        prisechangepercentage = value["P"]
                        weightedaverageprice = value["w"]
                        totaltradebaseasset = value["v"]

                        GRTUSDT_data = EfxPercentageChangeGrtusdt(
                            open=open,
                            close=close,
                            high=high,
                            low=low,
                            timestamp=timestamp,
                            symbol=symbol,
                            volume=volume,
                            pricechange=pricechange,
                            prisechangepercentage=prisechangepercentage,
                            weightedaverageprice=weightedaverageprice,
                            totaltradebaseasset=totaltradebaseasset)
                        GRTUSDT_data.save()
                    if value["s"] == "IOSTUSDT":
                        # /////// get value \\\\\\\\\\\\\
                        open = value["o"]
                        high = value["h"]
                        low = value["l"]
                        close = value["c"]
                        volume = value["v"]
                        timestamp = value["E"]
                        symbol = value["s"]
                        pricechange = value["p"]
                        prisechangepercentage = value["P"]
                        weightedaverageprice = value["w"]
                        totaltradebaseasset = value["v"]

                        IOSTUSDT_data = EfxPercentageChangeIostusdt(
                            open=open,
                            close=close,
                            high=high,
                            low=low,
                            timestamp=timestamp,
                            symbol=symbol,
                            volume=volume,
                            pricechange=pricechange,
                            prisechangepercentage=prisechangepercentage,
                            weightedaverageprice=weightedaverageprice,
                            totaltradebaseasset=totaltradebaseasset)
                        IOSTUSDT_data.save()
                    if value["s"] == "IOTAUSDT":
                        # /////// get value \\\\\\\\\\\\\
                        open = value["o"]
                        high = value["h"]
                        low = value["l"]
                        close = value["c"]
                        volume = value["v"]
                        timestamp = value["E"]
                        symbol = value["s"]
                        pricechange = value["p"]
                        prisechangepercentage = value["P"]
                        weightedaverageprice = value["w"]
                        totaltradebaseasset = value["v"]

                        IOTAUSDT_data = EfxPercentageChangeIotausdt(
                            open=open,
                            close=close,
                            high=high,
                            low=low,
                            timestamp=timestamp,
                            symbol=symbol,
                            volume=volume,
                            pricechange=pricechange,
                            prisechangepercentage=prisechangepercentage,
                            weightedaverageprice=weightedaverageprice,
                            totaltradebaseasset=totaltradebaseasset)
                        IOTAUSDT_data.save()
                    if value["s"] == "JSTUSDT":
                        # /////// get value \\\\\\\\\\\\\
                        open = value["o"]
                        high = value["h"]
                        low = value["l"]
                        close = value["c"]
                        volume = value["v"]
                        timestamp = value["E"]
                        symbol = value["s"]
                        pricechange = value["p"]
                        prisechangepercentage = value["P"]
                        weightedaverageprice = value["w"]
                        totaltradebaseasset = value["v"]

                        JSTUSDT_data = EfxPercentageChangeJstusdt(
                            open=open,
                            close=close,
                            high=high,
                            low=low,
                            timestamp=timestamp,
                            symbol=symbol,
                            volume=volume,
                            pricechange=pricechange,
                            prisechangepercentage=prisechangepercentage,
                            weightedaverageprice=weightedaverageprice,
                            totaltradebaseasset=totaltradebaseasset)
                        JSTUSDT_data.save()
                    if value["s"] == "KAVAUSDT":
                        # /////// get value \\\\\\\\\\\\\
                        open = value["o"]
                        high = value["h"]
                        low = value["l"]
                        close = value["c"]
                        volume = value["v"]
                        timestamp = value["E"]
                        symbol = value["s"]
                        pricechange = value["p"]
                        prisechangepercentage = value["P"]
                        weightedaverageprice = value["w"]
                        totaltradebaseasset = value["v"]

                        KAVAUSDT_data = EfxPercentageChangeKavausdt(
                            open=open,
                            close=close,
                            high=high,
                            low=low,
                            timestamp=timestamp,
                            symbol=symbol,
                            volume=volume,
                            pricechange=pricechange,
                            prisechangepercentage=prisechangepercentage,
                            weightedaverageprice=weightedaverageprice,
                            totaltradebaseasset=totaltradebaseasset)
                        KAVAUSDT_data.save()
                    if value["s"] == "LINKUSDT":
                        # /////// get value \\\\\\\\\\\\\
                        open = value["o"]
                        high = value["h"]
                        low = value["l"]
                        close = value["c"]
                        volume = value["v"]
                        timestamp = value["E"]
                        symbol = value["s"]
                        pricechange = value["p"]
                        prisechangepercentage = value["P"]
                        weightedaverageprice = value["w"]
                        totaltradebaseasset = value["v"]

                        LINKUSDT_data = EfxPercentageChangeLinkusdt(
                            open=open,
                            close=close,
                            high=high,
                            low=low,
                            timestamp=timestamp,
                            symbol=symbol,
                            volume=volume,
                            pricechange=pricechange,
                            prisechangepercentage=prisechangepercentage,
                            weightedaverageprice=weightedaverageprice,
                            totaltradebaseasset=totaltradebaseasset)
                        LINKUSDT_data.save()
                    if value["s"] == "LITUSDT":
                        # /////// get value \\\\\\\\\\\\\
                        open = value["o"]
                        high = value["h"]
                        low = value["l"]
                        close = value["c"]
                        volume = value["v"]
                        timestamp = value["E"]
                        symbol = value["s"]
                        pricechange = value["p"]
                        prisechangepercentage = value["P"]
                        weightedaverageprice = value["w"]
                        totaltradebaseasset = value["v"]

                        LITUSDT_data = EfxPercentageChangeLitusdt(
                            open=open,
                            close=close,
                            high=high,
                            low=low,
                            timestamp=timestamp,
                            symbol=symbol,
                            volume=volume,
                            pricechange=pricechange,
                            prisechangepercentage=prisechangepercentage,
                            weightedaverageprice=weightedaverageprice,
                            totaltradebaseasset=totaltradebaseasset)
                        LITUSDT_data.save()
                    if value["s"] == "LTCUSDT":
                        # /////// get value \\\\\\\\\\\\\
                        open = value["o"]
                        high = value["h"]
                        low = value["l"]
                        close = value["c"]
                        volume = value["v"]
                        timestamp = value["E"]
                        symbol = value["s"]
                        pricechange = value["p"]
                        prisechangepercentage = value["P"]
                        weightedaverageprice = value["w"]
                        totaltradebaseasset = value["v"]

                        LTCUSDT_data = EfxPercentageChangeLtcusdt(
                            open=open,
                            close=close,
                            high=high,
                            low=low,
                            timestamp=timestamp,
                            symbol=symbol,
                            volume=volume,
                            pricechange=pricechange,
                            prisechangepercentage=prisechangepercentage,
                            weightedaverageprice=weightedaverageprice,
                            totaltradebaseasset=totaltradebaseasset)
                        LTCUSDT_data.save()
                    if value["s"] == "MANAUSDT":
                        # /////// get value \\\\\\\\\\\\\
                        open = value["o"]
                        high = value["h"]
                        low = value["l"]
                        close = value["c"]
                        volume = value["v"]
                        timestamp = value["E"]
                        symbol = value["s"]
                        pricechange = value["p"]
                        prisechangepercentage = value["P"]
                        weightedaverageprice = value["w"]
                        totaltradebaseasset = value["v"]

                        MANAUSDT_data = EfxPercentageChangeManausdt(
                            open=open,
                            close=close,
                            high=high,
                            low=low,
                            timestamp=timestamp,
                            symbol=symbol,
                            volume=volume,
                            pricechange=pricechange,
                            prisechangepercentage=prisechangepercentage,
                            weightedaverageprice=weightedaverageprice,
                            totaltradebaseasset=totaltradebaseasset)
                        MANAUSDT_data.save()
                    if value["s"] == "MDXUSDT":
                        # /////// get value \\\\\\\\\\\\\
                        open = value["o"]
                        high = value["h"]
                        low = value["l"]
                        close = value["c"]
                        volume = value["v"]
                        timestamp = value["E"]
                        symbol = value["s"]
                        pricechange = value["p"]
                        prisechangepercentage = value["P"]
                        weightedaverageprice = value["w"]
                        totaltradebaseasset = value["v"]

                        MDXUSDT_data = EfxPercentageChangeMdxusdt(
                            open=open,
                            close=close,
                            high=high,
                            low=low,
                            timestamp=timestamp,
                            symbol=symbol,
                            volume=volume,
                            pricechange=pricechange,
                            prisechangepercentage=prisechangepercentage,
                            weightedaverageprice=weightedaverageprice,
                            totaltradebaseasset=totaltradebaseasset)
                        MDXUSDT_data.save()
                    if value["s"] == "NEOUSDT":
                        # /////// get value \\\\\\\\\\\\\
                        open = value["o"]
                        high = value["h"]
                        low = value["l"]
                        close = value["c"]
                        volume = value["v"]
                        timestamp = value["E"]
                        symbol = value["s"]
                        pricechange = value["p"]
                        prisechangepercentage = value["P"]
                        weightedaverageprice = value["w"]
                        totaltradebaseasset = value["v"]

                        NEOUSDT_data = EfxPercentageChangeNeousdt(
                            open=open,
                            close=close,
                            high=high,
                            low=low,
                            timestamp=timestamp,
                            symbol=symbol,
                            volume=volume,
                            pricechange=pricechange,
                            prisechangepercentage=prisechangepercentage,
                            weightedaverageprice=weightedaverageprice,
                            totaltradebaseasset=totaltradebaseasset)
                        NEOUSDT_data.save()
                    if value["s"] == "OMGUSDT":
                        # /////// get value \\\\\\\\\\\\\
                        open = value["o"]
                        high = value["h"]
                        low = value["l"]
                        close = value["c"]
                        volume = value["v"]
                        timestamp = value["E"]
                        symbol = value["s"]
                        pricechange = value["p"]
                        prisechangepercentage = value["P"]
                        weightedaverageprice = value["w"]
                        totaltradebaseasset = value["v"]

                        OMGUSDT_data = EfxPercentageChangeOmgusdt(
                            open=open,
                            close=close,
                            high=high,
                            low=low,
                            timestamp=timestamp,
                            symbol=symbol,
                            volume=volume,
                            pricechange=pricechange,
                            prisechangepercentage=prisechangepercentage,
                            weightedaverageprice=weightedaverageprice,
                            totaltradebaseasset=totaltradebaseasset)
                        OMGUSDT_data.save()
                    if value["s"] == "SUSHIUSDT":
                        # /////// get value \\\\\\\\\\\\\
                        open = value["o"]
                        high = value["h"]
                        low = value["l"]
                        close = value["c"]
                        volume = value["v"]
                        timestamp = value["E"]
                        symbol = value["s"]
                        pricechange = value["p"]
                        prisechangepercentage = value["P"]
                        weightedaverageprice = value["w"]
                        totaltradebaseasset = value["v"]

                        SUSHIUSDT_data = EfxPercentageChangeSushiusdt(
                            open=open,
                            close=close,
                            high=high,
                            low=low,
                            timestamp=timestamp,
                            symbol=symbol,
                            volume=volume,
                            pricechange=pricechange,
                            prisechangepercentage=prisechangepercentage,
                            weightedaverageprice=weightedaverageprice,
                            totaltradebaseasset=totaltradebaseasset)
                        SUSHIUSDT_data.save()
                    if value["s"] == "THETAUSDT":
                        # /////// get value \\\\\\\\\\\\\
                        open = value["o"]
                        high = value["h"]
                        low = value["l"]
                        close = value["c"]
                        volume = value["v"]
                        timestamp = value["E"]
                        symbol = value["s"]
                        pricechange = value["p"]
                        prisechangepercentage = value["P"]
                        weightedaverageprice = value["w"]
                        totaltradebaseasset = value["v"]

                        THETAUSDT_data = EfxPercentageChangeThetausdt(
                            open=open,
                            close=close,
                            high=high,
                            low=low,
                            timestamp=timestamp,
                            symbol=symbol,
                            volume=volume,
                            pricechange=pricechange,
                            prisechangepercentage=prisechangepercentage,
                            weightedaverageprice=weightedaverageprice,
                            totaltradebaseasset=totaltradebaseasset)
                        THETAUSDT_data.save()
                    if value["s"] == "TRXUSDT":
                        # /////// get value \\\\\\\\\\\\\
                        open = value["o"]
                        high = value["h"]
                        low = value["l"]
                        close = value["c"]
                        volume = value["v"]
                        timestamp = value["E"]
                        symbol = value["s"]
                        pricechange = value["p"]
                        prisechangepercentage = value["P"]
                        weightedaverageprice = value["w"]
                        totaltradebaseasset = value["v"]

                        TRXUSDT_data = EfxPercentageChangeTrxusdt(
                            open=open,
                            close=close,
                            high=high,
                            low=low,
                            timestamp=timestamp,
                            symbol=symbol,
                            volume=volume,
                            pricechange=pricechange,
                            prisechangepercentage=prisechangepercentage,
                            weightedaverageprice=weightedaverageprice,
                            totaltradebaseasset=totaltradebaseasset)
                        TRXUSDT_data.save()
                    if value["s"] == "UNIUSDT":
                        # /////// get value \\\\\\\\\\\\\
                        open = value["o"]
                        high = value["h"]
                        low = value["l"]
                        close = value["c"]
                        volume = value["v"]
                        timestamp = value["E"]
                        symbol = value["s"]
                        pricechange = value["p"]
                        prisechangepercentage = value["P"]
                        weightedaverageprice = value["w"]
                        totaltradebaseasset = value["v"]

                        UNIUSDT_data = EfxPercentageChangeUniusdt(
                            open=open,
                            close=close,
                            high=high,
                            low=low,
                            timestamp=timestamp,
                            symbol=symbol,
                            volume=volume,
                            pricechange=pricechange,
                            prisechangepercentage=prisechangepercentage,
                            weightedaverageprice=weightedaverageprice,
                            totaltradebaseasset=totaltradebaseasset)
                        UNIUSDT_data.save()
                    if value["s"] == "XMRUSDT":
                        # /////// get value \\\\\\\\\\\\\
                        open = value["o"]
                        high = value["h"]
                        low = value["l"]
                        close = value["c"]
                        volume = value["v"]
                        timestamp = value["E"]
                        symbol = value["s"]
                        pricechange = value["p"]
                        prisechangepercentage = value["P"]
                        weightedaverageprice = value["w"]
                        totaltradebaseasset = value["v"]

                        XMRUSDT_data = EfxPercentageChangeXmrusdt(
                            open=open,
                            close=close,
                            high=high,
                            low=low,
                            timestamp=timestamp,
                            symbol=symbol,
                            volume=volume,
                            pricechange=pricechange,
                            prisechangepercentage=prisechangepercentage,
                            weightedaverageprice=weightedaverageprice,
                            totaltradebaseasset=totaltradebaseasset)
                        XMRUSDT_data.save()
                    if value["s"] == "XTZUSDT":
                        # /////// get value \\\\\\\\\\\\\
                        open = value["o"]
                        high = value["h"]
                        low = value["l"]
                        close = value["c"]
                        volume = value["v"]
                        timestamp = value["E"]
                        symbol = value["s"]
                        pricechange = value["p"]
                        prisechangepercentage = value["P"]
                        weightedaverageprice = value["w"]
                        totaltradebaseasset = value["v"]

                        XTZUSDT_data = EfxPercentageChangeXtzusdt(
                            open=open,
                            close=close,
                            high=high,
                            low=low,
                            timestamp=timestamp,
                            symbol=symbol,
                            volume=volume,
                            pricechange=pricechange,
                            prisechangepercentage=prisechangepercentage,
                            weightedaverageprice=weightedaverageprice,
                            totaltradebaseasset=totaltradebaseasset)
                        XTZUSDT_data.save()
                    if value["s"] == "SHIBUSDT":
                        # /////// get value \\\\\\\\\\\\\
                        open = value["o"]
                        high = value["h"]
                        low = value["l"]
                        close = value["c"]
                        volume = value["v"]
                        timestamp = value["E"]
                        symbol = value["s"]
                        pricechange = value["p"]
                        prisechangepercentage = value["P"]
                        weightedaverageprice = value["w"]
                        totaltradebaseasset = value["v"]

                        SHIBUSDT_data = EfxPercentageChangeShibusdt(
                            open=open,
                            close=close,
                            high=high,
                            low=low,
                            timestamp=timestamp,
                            symbol=symbol,
                            volume=volume,
                            pricechange=pricechange,
                            prisechangepercentage=prisechangepercentage,
                            weightedaverageprice=weightedaverageprice,
                            totaltradebaseasset=totaltradebaseasset)
                        SHIBUSDT_data.save()
                    if value["s"] == "RVNUSDT":
                        # /////// get value \\\\\\\\\\\\\
                        open = value["o"]
                        high = value["h"]
                        low = value["l"]
                        close = value["c"]
                        volume = value["v"]
                        timestamp = value["E"]
                        symbol = value["s"]
                        pricechange = value["p"]
                        prisechangepercentage = value["P"]
                        weightedaverageprice = value["w"]
                        totaltradebaseasset = value["v"]

                        RVNUSDT_data = EfxPercentageChangeRvnusdt(
                            open=open,
                            close=close,
                            high=high,
                            low=low,
                            timestamp=timestamp,
                            symbol=symbol,
                            volume=volume,
                            pricechange=pricechange,
                            prisechangepercentage=prisechangepercentage,
                            weightedaverageprice=weightedaverageprice,
                            totaltradebaseasset=totaltradebaseasset)
                        RVNUSDT_data.save()
                    if value["s"] == "MATICUSDT":
                        # /////// get value \\\\\\\\\\\\\
                        open = value["o"]
                        high = value["h"]
                        low = value["l"]
                        close = value["c"]
                        volume = value["v"]
                        timestamp = value["E"]
                        symbol = value["s"]
                        pricechange = value["p"]
                        prisechangepercentage = value["P"]
                        weightedaverageprice = value["w"]
                        totaltradebaseasset = value["v"]

                        MATICUSDT_data = EfxPercentageChangeMaticusdt(
                            open=open,
                            close=close,
                            high=high,
                            low=low,
                            timestamp=timestamp,
                            symbol=symbol,
                            volume=volume,
                            pricechange=pricechange,
                            prisechangepercentage=prisechangepercentage,
                            weightedaverageprice=weightedaverageprice,
                            totaltradebaseasset=totaltradebaseasset)
                        MATICUSDT_data.save()
                    if value["s"] == "CELRUSDT":
                        # /////// get value \\\\\\\\\\\\\
                        open = value["o"]
                        high = value["h"]
                        low = value["l"]
                        close = value["c"]
                        volume = value["v"]
                        timestamp = value["E"]
                        symbol = value["s"]
                        pricechange = value["p"]
                        prisechangepercentage = value["P"]
                        weightedaverageprice = value["w"]
                        totaltradebaseasset = value["v"]

                        CELRUSDT_data = EfxPercentageChangeCelrusdt(
                            open=open,
                            close=close,
                            high=high,
                            low=low,
                            timestamp=timestamp,
                            symbol=symbol,
                            volume=volume,
                            pricechange=pricechange,
                            prisechangepercentage=prisechangepercentage,
                            weightedaverageprice=weightedaverageprice,
                            totaltradebaseasset=totaltradebaseasset)
                        CELRUSDT_data.save()
                    if value["s"] == "BTCTUSDT":
                        # /////// get value \\\\\\\\\\\\\
                        open = value["o"]
                        high = value["h"]
                        low = value["l"]
                        close = value["c"]
                        volume = value["v"]
                        timestamp = value["E"]
                        symbol = value["s"]
                        pricechange = value["p"]
                        prisechangepercentage = value["P"]
                        weightedaverageprice = value["w"]
                        totaltradebaseasset = value["v"]

                        BTCTUSDT_data = EfxPercentageChangeBtctusdt(
                            open=open,
                            close=close,
                            high=high,
                            low=low,
                            timestamp=timestamp,
                            symbol=symbol,
                            volume=volume,
                            pricechange=pricechange,
                            prisechangepercentage=prisechangepercentage,
                            weightedaverageprice=weightedaverageprice,
                            totaltradebaseasset=totaltradebaseasset)
                        BTCTUSDT_data.save()
                    if value["s"] == "XRPUSDT":
                        # /////// get value \\\\\\\\\\\\\
                        open = value["o"]
                        high = value["h"]
                        low = value["l"]
                        close = value["c"]
                        volume = value["v"]
                        timestamp = value["E"]
                        symbol = value["s"]
                        pricechange = value["p"]
                        prisechangepercentage = value["P"]
                        weightedaverageprice = value["w"]
                        totaltradebaseasset = value["v"]

                        XRPUSDT_data = EfxPercentageChangeXrpusdt(
                            open=open,
                            close=close,
                            high=high,
                            low=low,
                            timestamp=timestamp,
                            symbol=symbol,
                            volume=volume,
                            pricechange=pricechange,
                            prisechangepercentage=prisechangepercentage,
                            weightedaverageprice=weightedaverageprice,
                            totaltradebaseasset=totaltradebaseasset)
                        XRPUSDT_data.save()

        except Exception as e:
            e = str(e)
            print(e + ',error')


loop = asyncio.get_event_loop()
try:
    asyncio.ensure_future(main())
    loop.run_forever()
except KeyboardInterrupt:
    pass
finally:
    print("Closing Loop")
    loop.close()