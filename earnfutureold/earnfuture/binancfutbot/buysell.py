from decimal import Decimal
from symtable import Symbol

import json
import random
from binancefuture.models import *
from decimal import *
import time
from binance.futures import Futures
from django.db.models import Q
from django.forms.models import model_to_dict

def schedule_api():
    bot_order = Binancefuturebid.objects.filter(
        ~Q(buy_sell_done=3), Q(symbol='AAVEUSDT')).all()
    for a in bot_order:
        symbol = a.symbol
        process_type = a.buy_sell_done
        if process_type == 1:
            cur_price = current_price(symbol)
            buy_prise = a.triger_prise_buy
            if buy_prise and cur_price:
                if buy_prise <= cur_price:
                    try:
                        id = a.id
                        user_id = a.user_id
                        firstbuyinamout = a.firstbuyinamout
                        created_date = int(time.time())
                        symbol = a.symbol
                        api_key = a.apikey
                        secret_key = a.secretkey
                        quntaty = a.quntaty
                        # new connection generation
                        client = Futures(api_key, secret_key)
                        # balance check
                        balance = balance_check(client)

                        if balance != "null" and firstbuyinamout >= balance:
                            try:
                                buysellnew = {
                                    'symbol': symbol,
                                    'side': 'BUY',
                                    'type': 'LIMIT',
                                    'timeInForce': 'GTC',
                                    'quantity': quntaty,
                                    'price': cur_price
                                }
                                buysell = client.new_order_test(**buysellnew)
                                if buysell:
                                    buy_sell_done = 2
                                    if 'clientOrderId' in buysell:
                                        clientOrderId = buysell['clientOrderId']
                                    else:
                                        clientOrderId = 0
                                    if 'cumQuote' in buysell:
                                        cumQty = buysell['cumQuote']
                                    else:
                                        cumQty = 0
                                    if 'executedQty' in buysell:
                                        executedQty = buysell['executedQty']
                                    else:
                                        executedQty = 0
                                    if 'orderId' in buysell:
                                        orderId = buysell['orderId']
                                    else:
                                        orderId = 0
                                    if 'origQty' in buysell:
                                        origQty = buysell['origQty']
                                    else:
                                        origQty = 0
                                    if 'price' in buysell:
                                        price = buysell['price']
                                    else:
                                        price = 0
                                    if 'side' in buysell:
                                        side = buysell['side']
                                    else:
                                        side = 0
                                    if 'positionSide' in buysell:
                                        positionSide = buysell['positionSide']
                                    else:
                                        positionSide = 0
                                    if 'status' in buysell:
                                        status = buysell['status']
                                    else:
                                        status = 0
                                    if 'stopPrice' in buysell:
                                        stopPrice = buysell['stopPrice']
                                    else:
                                        stopPrice = 0
                                    if 'closePosition' in buysell:
                                        closePosition = buysell['closePosition']
                                    else:
                                        closePosition = 0
                                    if 'timeInForce' in buysell:
                                        timeInForce = buysell['timeInForce']
                                    else:
                                        timeInForce = 0
                                    if 'activatePrice' in buysell:
                                        activatePrice = buysell['activatePrice']
                                    else:
                                        activatePrice = 0
                                    if 'priceRate' in buysell:
                                        priceRate = buysell['priceRate']
                                    else:
                                        priceRate = 0
                                    if 'updateTime' in buysell:
                                        updateTime = buysell['updateTime']
                                    else:
                                        updateTime = 0
                                    if 'priceProtect' in buysell:
                                        priceProtect = buysell['priceProtect']
                                    else:
                                        priceProtect = 0
                                else:
                                    buy_sell_done = 3
                                    clientOrderId = "0"
                                    cumQty = "0"
                                    executedQty = "0"
                                    orderId = "0"
                                    origQty = "0"
                                    price = "0"
                                    side = "0"
                                    positionSide = "0"
                                    status = "0"
                                    stopPrice = "0"
                                    closePosition = "0"
                                    timeInForce = "0"
                                    activatePrice = "0"
                                    priceRate = "0"
                                    updateTime = "0"
                                    priceProtect = "0"
                                try:
                                    data = Binancefuturebid.objects.get(id=id)
                                    data.user_id = user_id
                                    data.created_date = created_date
                                    data.firstbuyinamout = firstbuyinamout
                                    data.buy_sell_done = buy_sell_done
                                    data.symbol = symbol
                                    data.clientorderid = str(clientOrderId)
                                    data.cumqty = str(cumQty)
                                    data.executedqty = str(executedQty)
                                    data.orderid = str(orderId)
                                    data.origqty = str(origQty)
                                    data.price = str(price)
                                    data.side = str(side)
                                    data.positionside = str(positionSide)
                                    data.status = str(status)
                                    data.stopprice = str(stopPrice)
                                    data.closeposition = str(closePosition)
                                    data.timeinforce = str(timeInForce)
                                    data.activateprice = str(activatePrice)
                                    data.pricerate = str(priceRate)
                                    data.updatetime = str(updateTime)
                                    data.priceprotect = str(priceProtect)
                                    # data set and save
                                    data.save()
                                except Exception as e:
                                    id = a.id
                                    data = Binancefuturebid.objects.get(id=id)
                                    data.buy_sell_done = 3
                                    data.status = str("Database storing Error")
                                    data.save()
                            except Exception as e:
                                error = str(e)
                                err_msg_convert_json = eval(error)
                                only_msg = err_msg_convert_json[2]
                                id = a.id
                                data = Binancefuturebid.objects.get(id=id)
                                data.buy_sell_done = 3
                                data.status = str(only_msg)
                                data.save()
                        else:
                            id = a.id
                            data = Binancefuturebid.objects.get(id=id)
                            data.buy_sell_done = 3
                            data.status = str("Balance is empty or not enough..")
                            data.save()
                    except Exception as e:
                        id = a.id
                        data = Binancefuturebid.objects.get(id=id)
                        data.buy_sell_done = 3
                        data.status = str("Database storing Error")
                        data.save()
        elif process_type == 2:
            cur_price = current_price(symbol)
            sell_prise = a.triger_prise_sell
            if sell_prise and cur_price:
                if sell_prise <= cur_price:
                    try:
                        id = a.id
                        user_id = a.user_id
                        firstbuyinamout = a.firstbuyinamout
                        created_date = int(time.time())
                        symbol = a.symbol
                        api_key = a.apikey
                        secret_key = a.secretkey
                        quntaty = a.quntaty
                        # new connection generation
                        client = Futures(api_key, secret_key)
                        # balance check
                        balance = balance_check(client)

                        if balance != "null" and firstbuyinamout >= balance:
                            try:
                                buysellnew = {
                                    'symbol': symbol,
                                    'side': 'BUY',
                                    'type': 'LIMIT',
                                    'timeInForce': 'GTC',
                                    'quantity': quntaty,
                                    'price': cur_price
                                }
                                buysell = client.new_order_test(**buysellnew)
                                if buysell:
                                    buy_sell_done = 2
                                    if 'clientOrderId' in buysell:
                                        clientOrderId = buysell['clientOrderId']
                                    else:
                                        clientOrderId = 0
                                    if 'cumQuote' in buysell:
                                        cumQty = buysell['cumQuote']
                                    else:
                                        cumQty = 0
                                    if 'executedQty' in buysell:
                                        executedQty = buysell['executedQty']
                                    else:
                                        executedQty = 0
                                    if 'orderId' in buysell:
                                        orderId = buysell['orderId']
                                    else:
                                        orderId = 0
                                    if 'origQty' in buysell:
                                        origQty = buysell['origQty']
                                    else:
                                        origQty = 0
                                    if 'price' in buysell:
                                        price = buysell['price']
                                    else:
                                        price = 0
                                    if 'side' in buysell:
                                        side = buysell['side']
                                    else:
                                        side = 0
                                    if 'positionSide' in buysell:
                                        positionSide = buysell['positionSide']
                                    else:
                                        positionSide = 0
                                    if 'status' in buysell:
                                        status = buysell['status']
                                    else:
                                        status = 0
                                    if 'stopPrice' in buysell:
                                        stopPrice = buysell['stopPrice']
                                    else:
                                        stopPrice = 0
                                    if 'closePosition' in buysell:
                                        closePosition = buysell['closePosition']
                                    else:
                                        closePosition = 0
                                    if 'timeInForce' in buysell:
                                        timeInForce = buysell['timeInForce']
                                    else:
                                        timeInForce = 0
                                    if 'activatePrice' in buysell:
                                        activatePrice = buysell['activatePrice']
                                    else:
                                        activatePrice = 0
                                    if 'priceRate' in buysell:
                                        priceRate = buysell['priceRate']
                                    else:
                                        priceRate = 0
                                    if 'updateTime' in buysell:
                                        updateTime = buysell['updateTime']
                                    else:
                                        updateTime = 0
                                    if 'priceProtect' in buysell:
                                        priceProtect = buysell['priceProtect']
                                    else:
                                        priceProtect = 0
                                else:
                                    buy_sell_done = 3
                                    clientOrderId = "0"
                                    cumQty = "0"
                                    executedQty = "0"
                                    orderId = "0"
                                    origQty = "0"
                                    price = "0"
                                    side = "0"
                                    positionSide = "0"
                                    status = "0"
                                    stopPrice = "0"
                                    closePosition = "0"
                                    timeInForce = "0"
                                    activatePrice = "0"
                                    priceRate = "0"
                                    updateTime = "0"
                                    priceProtect = "0"
                                try:
                                    data = Binancefuturebid.objects.get(id=id)
                                    data.user_id = user_id
                                    data.created_date = created_date
                                    data.firstbuyinamout = firstbuyinamout
                                    data.buy_sell_done = buy_sell_done
                                    data.symbol = symbol
                                    data.clientorderid = str(clientOrderId)
                                    data.cumqty = str(cumQty)
                                    data.executedqty = str(executedQty)
                                    data.orderid = str(orderId)
                                    data.origqty = str(origQty)
                                    data.price = str(price)
                                    data.side = str(side)
                                    data.positionside = str(positionSide)
                                    data.status = str(status)
                                    data.stopprice = str(stopPrice)
                                    data.closeposition = str(closePosition)
                                    data.timeinforce = str(timeInForce)
                                    data.activateprice = str(activatePrice)
                                    data.pricerate = str(priceRate)
                                    data.updatetime = str(updateTime)
                                    data.priceprotect = str(priceProtect)
                                    # data set and save
                                    data.save()
                                except Exception as e:
                                    id = a.id
                                    data = Binancefuturebid.objects.get(id=id)
                                    data.buy_sell_done = 3
                                    data.status = str("Database storing Error")
                                    data.save()
                            except Exception as e:
                                error = str(e)
                                err_msg_convert_json = eval(error)
                                only_msg = err_msg_convert_json[2]
                                id = a.id
                                data = Binancefuturebid.objects.get(id=id)
                                data.buy_sell_done = 3
                                data.status = str(only_msg)
                                data.save()
                        else:
                            id = a.id
                            data = Binancefuturebid.objects.get(id=id)
                            data.buy_sell_done = 3
                            data.status = str("Balance is empty or not enough..")
                            data.save()
                    except Exception as e:
                        id = a.id
                        data = Binancefuturebid.objects.get(id=id)
                        data.buy_sell_done = 3
                        data.status = str("Database storing Error")
                        data.save()
def balance_check(client):
    # check balance
    data = client.balance()
    if not data:
        balance = "null"
        return balance
    else:
        for row in data:
            if row["asset"] == "USDT":
                usdt_balance = row["balance"]
                usdt_balance = float(usdt_balance)
                return usdt_balance


def current_price(symbol):
    if symbol == "AAVEUSDT":
        data = EfxPercentageChangeAaveusdt.objects.values(
            "id", "close").order_by('-id')[0]
        data = data['close']
        return data
    elif symbol == "ADAUSDT":
        data = EfxPercentageChangeAdausdt.objects.values(
            "id", "close").order_by('-id')[0]
        data = data['close']
        return data
    elif symbol == "AKROUSDT":
        data = EfxPercentageChangeAkrousdt.objects.values(
            "id", "close").order_by('-id')[0]
        data = data['close']
        return data
    elif symbol == "ANTUSDT":
        data = EfxPercentageChangeAntusdt.objects.values(
            "id", "close").order_by('-id')[0]
        data = data['close']
        return data
    elif symbol == "ATOMUSDT":
        data = EfxPercentageChangeAtomusdt.objects.values(
            "id", "close").order_by('-id')[0]
        data = data['close']
        return data
    elif symbol == "BATUSDT":
        data = EfxPercentageChangeBatusdt.objects.values(
            "id", "close").order_by('-id')[0]
        data = data['close']
        return data
    elif symbol == "AAVEUSDT":
        data = EfxPercentageChangeAaveusdt.objects.values(
            "id", "close").order_by('-id')[0]
        data = data['close']
        return data
    elif symbol == "BCHUSDT":
        data = EfxPercentageChangeBchusdt.objects.values(
            "id", "close").order_by('-id')[0]
        data = data['close']
        return data
    elif symbol == "BNBUSDT":
        data = EfxPercentageChangeBnbusdt.objects.values(
            "id", "close").order_by('-id')[0]
        data = data['close']
        return data
    elif symbol == "BTCUSDT":
        data = EfxPercentageChangeBtcusdt.objects.values(
            "id", "close").order_by('-id')[0]
        data = data['close']
        return data
    elif symbol == "CAKEUSDT":
        data = EfxPercentageChangeCakeusdt.objects.values(
            "id", "close").order_by('-id')[0]
        data = data['close']
        return data
    elif symbol == "COMPUSDT":
        data = EfxPercentageChangeCompusdt.objects.values(
            "id", "close").order_by('-id')[0]
        data = data['close']
        return data
    elif symbol == "COSUSDT":
        data = EfxPercentageChangeCosusdt.objects.values(
            "id", "close").order_by('-id')[0]
        data = data['close']
        return data
    elif symbol == "CRVUSDT":
        data = EfxPercentageChangeCrvusdt.objects.values(
            "id", "close").order_by('-id')[0]
        data = data['close']
        return data
    elif symbol == "DASHUSDT":
        data = EfxPercentageChangeDashusdt.objects.values(
            "id", "close").order_by('-id')[0]
        data = data['close']
        return data
    elif symbol == "DOGEUSDT":
        data = EfxPercentageChangeDogeusdt.objects.values(
            "id", "close").order_by('-id')[0]
        data = data['close']
        return data
    elif symbol == "DOTUSDT":
        data = EfxPercentageChangeDotusdt.objects.values(
            "id", "close").order_by('-id')[0]
        data = data['close']
        return data
    elif symbol == "EOSUSDT":
        data = EfxPercentageChangeEosusdt.objects.values(
            "id", "close").order_by('-id')[0]
        data = data['close']
        return data
    elif symbol == "ETHUSDT":
        data = EfxPercentageChangeEtcusdt.objects.values(
            "id", "close").order_by('-id')[0]
        data = data['close']
        return data
    elif symbol == "ETCUSDT":
        data = EfxPercentageChangeEthusdt.objects.values(
            "id", "close").order_by('-id')[0]
        data = data['close']
        return data
    elif symbol == "FILUSDT":
        data = EfxPercentageChangeFilusdt.objects.values(
            "id", "close").order_by('-id')[0]
        data = data['close']
        return data
    elif symbol == "FTTUSDT":
        data = EfxPercentageChangeFttusdt.objects.values(
            "id", "close").order_by('-id')[0]
        data = data['close']
        return data
    elif symbol == "GRTUSDT":
        data = EfxPercentageChangeGrtusdt.objects.values(
            "id", "close").order_by('-id')[0]
        data = data['close']
        return data
    elif symbol == "IOSTUSDT":
        data = EfxPercentageChangeIostusdt.objects.values(
            "id", "close").order_by('-id')[0]
        data = data['close']
        return data
    elif symbol == "JSTUSDT":
        data = EfxPercentageChangeJstusdt.objects.values(
            "id", "close").order_by('-id')[0]
        data = data['close']
        return data
    elif symbol == "KAVAUSDT":
        data = EfxPercentageChangeKavausdt.objects.values(
            "id", "close").order_by('-id')[0]
        data = data['close']
        return data
    elif symbol == "LINKUSDT":
        data = EfxPercentageChangeLinkusdt.objects.values(
            "id", "close").order_by('-id')[0]
        data = data['close']
        return data
    elif symbol == "LITUSDT":
        data = EfxPercentageChangeLitusdt.objects.values(
            "id", "close").order_by('-id')[0]
        data = data['close']
        return data
    elif symbol == "LTCUSDT":
        data = EfxPercentageChangeLtcusdt.objects.values(
            "id", "close").order_by('-id')[0]
        data = data['close']
        return data
    elif symbol == "MANAUSDT":
        data = EfxPercentageChangeManausdt.objects.values(
            "id", "close").order_by('-id')[0]
        data = data['close']
        return data
    elif symbol == "MDXUSDT":
        data = EfxPercentageChangeMdxusdt.objects.values(
            "id", "close").order_by('-id')[0]
        data = data['close']
        return data
    elif symbol == "NEOUSDT":
        data = EfxPercentageChangeNeousdt.objects.values(
            "id", "close").order_by('-id')[0]
        data = data['close']
        return data
    elif symbol == "OMGUSDT":
        data = EfxPercentageChangeOmgusdt.objects.values(
            "id", "close").order_by('-id')[0]
        data = data['close']
        return data
    elif symbol == "SUSHIUSDT":
        data = EfxPercentageChangeSushiusdt.objects.values(
            "id", "close").order_by('-id')[0]
        data = data['close']
        return data
    elif symbol == "TRXUSDT":
        data = EfxPercentageChangeThetausdt.objects.values(
            "id", "close").order_by('-id')[0]
        data = data['close']
        return data
    elif symbol == "UNIUSDT":
        data = EfxPercentageChangeUniusdt.objects.values(
            "id", "close").order_by('-id')[0]
        data = data['close']
        return data
    elif symbol == "XMRUSDT":
        data = EfxPercentageChangeXmrusdt.objects.values(
            "id", "close").order_by('-id')[0]
        data = data['close']
        return data
    elif symbol == "XTZUSDT":
        data = EfxPercentageChangeXtzusdt.objects.values(
            "id", "close").order_by('-id')[0]
        data = data['close']
        return data
    elif symbol == "SHIBUSDT":
        data = EfxPercentageChangeShibusdt.objects.values(
            "id", "close").order_by('-id')[0]
        data = data['close']
        return data
    elif symbol == "RVNUSDT":
        data = EfxPercentageChangeRvnusdt.objects.values(
            "id", "close").order_by('-id')[0]
        data = data['close']
        return data
    elif symbol == "MATICUSDT":
        data = EfxPercentageChangeMaticusdt.objects.values(
            "id", "close").order_by('-id')[0]
        data = data['close']
        return data
    elif symbol == "CELRUSDT":
        data = EfxPercentageChangeCelrusdt.objects.values(
            "id", "close").order_by('-id')[0]
        data = data['close']
        return data
    elif symbol == "BTCTUSDT":
        data = EfxPercentageChangeBtctusdt.objects.values(
            "id", "close").order_by('-id')[0]
        data = data['close']
        return data
    elif symbol == "XRPUSDT":
        data = EfxPercentageChangeXrpusdt.objects.values(
            "id", "close").order_by('-id')[0]
        data = data['close']
        return data
