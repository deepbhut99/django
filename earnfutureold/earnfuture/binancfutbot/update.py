from datetime import datetime
from apscheduler.schedulers.background import BackgroundScheduler
from .buysell import schedule_api
from .websocketcheck import main_websocket


def start():
    scheduler = BackgroundScheduler()
    scheduler.add_job(schedule_api, 'interval', seconds=1)
    scheduler.start()
    


def start_second():
    scheduler1 = BackgroundScheduler()
    scheduler1.add_job(main_websocket, 'interval', seconds=20)
    scheduler1.start()
    print('socket2')
