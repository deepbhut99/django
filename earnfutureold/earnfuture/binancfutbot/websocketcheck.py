import requests
import json
import random
from binancefuture.models import *
import asyncio
import websockets
import datetime
from asgiref.sync import sync_to_async
import os
import django
import time
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'rest.settings')
os.environ["DJANGO_ALLOW_ASYNC_UNSAFE"] = "true"


# def websocketcheck():
#     check_ws_run = ApidataGetCheckForbinance.objects.filter(id=1).all()
#     if check_ws_run:
#         print('websocket2 here')
#         # timestamp store one
#         for stor_d in check_ws_run:
#             store_date = stor_d.timestamp

#         # store_date =
#         result = datetime.datetime.now()
#         date_sub = datetime.timedelta(minutes=1)
#         dat_old = result - date_sub

#         # check date
#         dat_new = result.strftime("%Y-%m-%d %H:%M:%S %p")
#         date_diff = dat_old.strftime("%Y-%m-%d %H:%M:%S %p")
#         # check in less and grater date
#         chec_date = ApidataGetCheckForbinance.objects.filter(
#             timestamp__gte=date_diff, timestamp__lte=dat_new).exists()
#         if chec_date == True:
#             date = dat_new
#         else:
#             asyncio.run(main())

#     else:
#         asyncio.run(main())


# def direc_start():
#     asyncio.run(main())


def update_time(x):
    timestamp = ApidataGetCheckForbinance.objects.filter(
        id=1).update(timestamp=x)
    return timestamp

def main_websocket():
    url = "https://api.binance.com/api/v3/ticker/24hr"
    
    try:
        x = datetime.datetime.now()
        x = x.strftime("%Y-%m-%d %H:%M:%S %p")
        print(x + "doneadwad")
        
        r = requests.get(url)
        print('websocket data store')
        input_dict = r.json()
        for value in input_dict:
            
            if value['symbol'] == "1INCHUSDT":
                user_id = update_time(x)
                
            if value['symbol'] == "AAVEUSDT":
                # /////// get value \\\\\\\\\\\\\
                open = value['openPrice']
                high = value['highPrice']
                low = value['lowPrice']
                close = value['lastPrice']
                volume = value['volume']
                timestamp = value['closeTime']
                symbol = value['symbol']
                pricechange = value['priceChange']
                prisechangepercentage = value['priceChangePercent']
                weightedaverageprice = value['weightedAvgPrice']
                totaltradebaseasset = value['volume']

                AAVEUSDT_data = EfxPercentageChangeAaveusdt(
                    open=open,
                    close=close,
                    high=high,
                    low=low,
                    timestamp=timestamp,
                    symbol=symbol,
                    volume=volume,
                    pricechange=pricechange,
                    prisechangepercentage=prisechangepercentage,
                    weightedaverageprice=weightedaverageprice,
                    totaltradebaseasset=totaltradebaseasset)
                AAVEUSDT_data.save()
            if value['symbol'] == "ADAUSDT":
                # /////// get value \\\\\\\\\\\\\
                open = value['openPrice']
                high = value['highPrice']
                low = value['lowPrice']
                close = value['lastPrice']
                volume = value['volume']
                timestamp = value['closeTime']
                symbol = value['symbol']
                pricechange = value['priceChange']
                prisechangepercentage = value['priceChangePercent']
                weightedaverageprice = value['weightedAvgPrice']
                totaltradebaseasset = value['volume']

                ADAUSDT_data = EfxPercentageChangeAdausdt(
                    open=open,
                    close=close,
                    high=high,
                    low=low,
                    timestamp=timestamp,
                    symbol=symbol,
                    volume=volume,
                    pricechange=pricechange,
                    prisechangepercentage=prisechangepercentage,
                    weightedaverageprice=weightedaverageprice,
                    totaltradebaseasset=totaltradebaseasset)
                ADAUSDT_data.save()

            if value['symbol'] == "AKROUSDT":
                # /////// get value \\\\\\\\\\\\\
                open = value['openPrice']
                high = value['highPrice']
                low = value['lowPrice']
                close = value['lastPrice']
                volume = value['volume']
                timestamp = value['closeTime']
                symbol = value['symbol']
                pricechange = value['priceChange']
                prisechangepercentage = value['priceChangePercent']
                weightedaverageprice = value['weightedAvgPrice']
                totaltradebaseasset = value['volume']

                AKROUSDT_data = EfxPercentageChangeAkrousdt(
                    open=open,
                    close=close,
                    high=high,
                    low=low,
                    timestamp=timestamp,
                    symbol=symbol,
                    volume=volume,
                    pricechange=pricechange,
                    prisechangepercentage=prisechangepercentage,
                    weightedaverageprice=weightedaverageprice,
                    totaltradebaseasset=totaltradebaseasset)
                AKROUSDT_data.save()
            if value['symbol'] == "ANTUSDT":
                # /////// get value \\\\\\\\\\\\\
                open = value['openPrice']
                high = value['highPrice']
                low = value['lowPrice']
                close = value['lastPrice']
                volume = value['volume']
                timestamp = value['closeTime']
                symbol = value['symbol']
                pricechange = value['priceChange']
                prisechangepercentage = value['priceChangePercent']
                weightedaverageprice = value['weightedAvgPrice']
                totaltradebaseasset = value['volume']

                ANTUSDT_data = EfxPercentageChangeAntusdt(
                    open=open,
                    close=close,
                    high=high,
                    low=low,
                    timestamp=timestamp,
                    symbol=symbol,
                    volume=volume,
                    pricechange=pricechange,
                    prisechangepercentage=prisechangepercentage,
                    weightedaverageprice=weightedaverageprice,
                    totaltradebaseasset=totaltradebaseasset)
                ANTUSDT_data.save()
            if value['symbol'] == "ATOMUSDT":
                # /////// get value \\\\\\\\\\\\\
                open = value['openPrice']
                high = value['highPrice']
                low = value['lowPrice']
                close = value['lastPrice']
                volume = value['volume']
                timestamp = value['closeTime']
                symbol = value['symbol']
                pricechange = value['priceChange']
                prisechangepercentage = value['priceChangePercent']
                weightedaverageprice = value['weightedAvgPrice']
                totaltradebaseasset = value['volume']

                ATOMUSDT_data = EfxPercentageChangeAtomusdt(
                    open=open,
                    close=close,
                    high=high,
                    low=low,
                    timestamp=timestamp,
                    symbol=symbol,
                    volume=volume,
                    pricechange=pricechange,
                    prisechangepercentage=prisechangepercentage,
                    weightedaverageprice=weightedaverageprice,
                    totaltradebaseasset=totaltradebaseasset)
                ATOMUSDT_data.save()
            if value['symbol'] == "BATUSDT":
                # /////// get value \\\\\\\\\\\\\
                open = value['openPrice']
                high = value['highPrice']
                low = value['lowPrice']
                close = value['lastPrice']
                volume = value['volume']
                timestamp = value['closeTime']
                symbol = value['symbol']
                pricechange = value['priceChange']
                prisechangepercentage = value['priceChangePercent']
                weightedaverageprice = value['weightedAvgPrice']
                totaltradebaseasset = value['volume']

                BATUSDT_data = EfxPercentageChangeBatusdt(
                    open=open,
                    close=close,
                    high=high,
                    low=low,
                    timestamp=timestamp,
                    symbol=symbol,
                    volume=volume,
                    pricechange=pricechange,
                    prisechangepercentage=prisechangepercentage,
                    weightedaverageprice=weightedaverageprice,
                    totaltradebaseasset=totaltradebaseasset)
                BATUSDT_data.save()
            if value['symbol'] == "BCHUSDT":
                # /////// get value \\\\\\\\\\\\\
                open = value['openPrice']
                high = value['highPrice']
                low = value['lowPrice']
                close = value['lastPrice']
                volume = value['volume']
                timestamp = value['closeTime']
                symbol = value['symbol']
                pricechange = value['priceChange']
                prisechangepercentage = value['priceChangePercent']
                weightedaverageprice = value['weightedAvgPrice']
                totaltradebaseasset = value['volume']

                BCHUSDT_data = EfxPercentageChangeBchusdt(
                    open=open,
                    close=close,
                    high=high,
                    low=low,
                    timestamp=timestamp,
                    symbol=symbol,
                    volume=volume,
                    pricechange=pricechange,
                    prisechangepercentage=prisechangepercentage,
                    weightedaverageprice=weightedaverageprice,
                    totaltradebaseasset=totaltradebaseasset)
                BCHUSDT_data.save()
            if value['symbol'] == "BNBUSDT":
                # /////// get value \\\\\\\\\\\\\
                open = value['openPrice']
                high = value['highPrice']
                low = value['lowPrice']
                close = value['lastPrice']
                volume = value['volume']
                timestamp = value['closeTime']
                symbol = value['symbol']
                pricechange = value['priceChange']
                prisechangepercentage = value['priceChangePercent']
                weightedaverageprice = value['weightedAvgPrice']
                totaltradebaseasset = value['volume']

                BNBUSDT_data = EfxPercentageChangeBnbusdt(
                    open=open,
                    close=close,
                    high=high,
                    low=low,
                    timestamp=timestamp,
                    symbol=symbol,
                    volume=volume,
                    pricechange=pricechange,
                    prisechangepercentage=prisechangepercentage,
                    weightedaverageprice=weightedaverageprice,
                    totaltradebaseasset=totaltradebaseasset)
                BNBUSDT_data.save()
            if value['symbol'] == "BTCUSDT":
                # /////// get value \\\\\\\\\\\\\
                open = value['openPrice']
                high = value['highPrice']
                low = value['lowPrice']
                close = value['lastPrice']
                volume = value['volume']
                timestamp = value['closeTime']
                symbol = value['symbol']
                pricechange = value['priceChange']
                prisechangepercentage = value['priceChangePercent']
                weightedaverageprice = value['weightedAvgPrice']
                totaltradebaseasset = value['volume']

                BTCUSDT_data = EfxPercentageChangeBtcusdt(
                    open=open,
                    close=close,
                    high=high,
                    low=low,
                    timestamp=timestamp,
                    symbol=symbol,
                    volume=volume,
                    pricechange=pricechange,
                    prisechangepercentage=prisechangepercentage,
                    weightedaverageprice=weightedaverageprice,
                    totaltradebaseasset=totaltradebaseasset)
                BTCUSDT_data.save()
            if value['symbol'] == "CAKEUSDT":
                # /////// get value \\\\\\\\\\\\\
                open = value['openPrice']
                high = value['highPrice']
                low = value['lowPrice']
                close = value['lastPrice']
                volume = value['volume']
                timestamp = value['closeTime']
                symbol = value['symbol']
                pricechange = value['priceChange']
                prisechangepercentage = value['priceChangePercent']
                weightedaverageprice = value['weightedAvgPrice']
                totaltradebaseasset = value['volume']

                CAKEUSDT_data = EfxPercentageChangeCakeusdt(
                    open=open,
                    close=close,
                    high=high,
                    low=low,
                    timestamp=timestamp,
                    symbol=symbol,
                    volume=volume,
                    pricechange=pricechange,
                    prisechangepercentage=prisechangepercentage,
                    weightedaverageprice=weightedaverageprice,
                    totaltradebaseasset=totaltradebaseasset)
                CAKEUSDT_data.save()
            if value['symbol'] == "COMPUSDT":
                # /////// get value \\\\\\\\\\\\\
                open = value['openPrice']
                high = value['highPrice']
                low = value['lowPrice']
                close = value['lastPrice']
                volume = value['volume']
                timestamp = value['closeTime']
                symbol = value['symbol']
                pricechange = value['priceChange']
                prisechangepercentage = value['priceChangePercent']
                weightedaverageprice = value['weightedAvgPrice']
                totaltradebaseasset = value['volume']

                COMPUSDT_data = EfxPercentageChangeCompusdt(
                    open=open,
                    close=close,
                    high=high,
                    low=low,
                    timestamp=timestamp,
                    symbol=symbol,
                    volume=volume,
                    pricechange=pricechange,
                    prisechangepercentage=prisechangepercentage,
                    weightedaverageprice=weightedaverageprice,
                    totaltradebaseasset=totaltradebaseasset)
                COMPUSDT_data.save()
            if value['symbol'] == "COSUSDT":
                # /////// get value \\\\\\\\\\\\\
                open = value['openPrice']
                high = value['highPrice']
                low = value['lowPrice']
                close = value['lastPrice']
                volume = value['volume']
                timestamp = value['closeTime']
                symbol = value['symbol']
                pricechange = value['priceChange']
                prisechangepercentage = value['priceChangePercent']
                weightedaverageprice = value['weightedAvgPrice']
                totaltradebaseasset = value['volume']

                COSUSDT_data = EfxPercentageChangeCosusdt(
                    open=open,
                    close=close,
                    high=high,
                    low=low,
                    timestamp=timestamp,
                    symbol=symbol,
                    volume=volume,
                    pricechange=pricechange,
                    prisechangepercentage=prisechangepercentage,
                    weightedaverageprice=weightedaverageprice,
                    totaltradebaseasset=totaltradebaseasset)
                COSUSDT_data.save()
            if value['symbol'] == "CRVUSDT":
                # /////// get value \\\\\\\\\\\\\
                open = value['openPrice']
                high = value['highPrice']
                low = value['lowPrice']
                close = value['lastPrice']
                volume = value['volume']
                timestamp = value['closeTime']
                symbol = value['symbol']
                pricechange = value['priceChange']
                prisechangepercentage = value['priceChangePercent']
                weightedaverageprice = value['weightedAvgPrice']
                totaltradebaseasset = value['volume']

                CRVUSDT_data = EfxPercentageChangeCrvusdt(
                    open=open,
                    close=close,
                    high=high,
                    low=low,
                    timestamp=timestamp,
                    symbol=symbol,
                    volume=volume,
                    pricechange=pricechange,
                    prisechangepercentage=prisechangepercentage,
                    weightedaverageprice=weightedaverageprice,
                    totaltradebaseasset=totaltradebaseasset)
                CRVUSDT_data.save()
            if value['symbol'] == "DASHUSDT":
                # /////// get value \\\\\\\\\\\\\
                open = value['openPrice']
                high = value['highPrice']
                low = value['lowPrice']
                close = value['lastPrice']
                volume = value['volume']
                timestamp = value['closeTime']
                symbol = value['symbol']
                pricechange = value['priceChange']
                prisechangepercentage = value['priceChangePercent']
                weightedaverageprice = value['weightedAvgPrice']
                totaltradebaseasset = value['volume']

                DASHUSDT_data = EfxPercentageChangeDashusdt(
                    open=open,
                    close=close,
                    high=high,
                    low=low,
                    timestamp=timestamp,
                    symbol=symbol,
                    volume=volume,
                    pricechange=pricechange,
                    prisechangepercentage=prisechangepercentage,
                    weightedaverageprice=weightedaverageprice,
                    totaltradebaseasset=totaltradebaseasset)
                DASHUSDT_data.save()
            if value['symbol'] == "DOGEUSDT":
                # /////// get value \\\\\\\\\\\\\
                open = value['openPrice']
                high = value['highPrice']
                low = value['lowPrice']
                close = value['lastPrice']
                volume = value['volume']
                timestamp = value['closeTime']
                symbol = value['symbol']
                pricechange = value['priceChange']
                prisechangepercentage = value['priceChangePercent']
                weightedaverageprice = value['weightedAvgPrice']
                totaltradebaseasset = value['volume']

                DOGEUSDT_data = EfxPercentageChangeDogeusdt(
                    open=open,
                    close=close,
                    high=high,
                    low=low,
                    timestamp=timestamp,
                    symbol=symbol,
                    volume=volume,
                    pricechange=pricechange,
                    prisechangepercentage=prisechangepercentage,
                    weightedaverageprice=weightedaverageprice,
                    totaltradebaseasset=totaltradebaseasset)
                DOGEUSDT_data.save()
            if value['symbol'] == "DOTUSDT":
                # /////// get value \\\\\\\\\\\\\
                open = value['openPrice']
                high = value['highPrice']
                low = value['lowPrice']
                close = value['lastPrice']
                volume = value['volume']
                timestamp = value['closeTime']
                symbol = value['symbol']
                pricechange = value['priceChange']
                prisechangepercentage = value['priceChangePercent']
                weightedaverageprice = value['weightedAvgPrice']
                totaltradebaseasset = value['volume']

                DOTUSDT_data = EfxPercentageChangeDotusdt(
                    open=open,
                    close=close,
                    high=high,
                    low=low,
                    timestamp=timestamp,
                    symbol=symbol,
                    volume=volume,
                    pricechange=pricechange,
                    prisechangepercentage=prisechangepercentage,
                    weightedaverageprice=weightedaverageprice,
                    totaltradebaseasset=totaltradebaseasset)
                DOTUSDT_data.save()
            if value['symbol'] == "EOSUSDT":
                # /////// get value \\\\\\\\\\\\\
                open = value['openPrice']
                high = value['highPrice']
                low = value['lowPrice']
                close = value['lastPrice']
                volume = value['volume']
                timestamp = value['closeTime']
                symbol = value['symbol']
                pricechange = value['priceChange']
                prisechangepercentage = value['priceChangePercent']
                weightedaverageprice = value['weightedAvgPrice']
                totaltradebaseasset = value['volume']

                EOSUSDT_data = EfxPercentageChangeEosusdt(
                    open=open,
                    close=close,
                    high=high,
                    low=low,
                    timestamp=timestamp,
                    symbol=symbol,
                    volume=volume,
                    pricechange=pricechange,
                    prisechangepercentage=prisechangepercentage,
                    weightedaverageprice=weightedaverageprice,
                    totaltradebaseasset=totaltradebaseasset)
                EOSUSDT_data.save()
            if value['symbol'] == "ETHUSDT":
                # /////// get value \\\\\\\\\\\\\
                open = value['openPrice']
                high = value['highPrice']
                low = value['lowPrice']
                close = value['lastPrice']
                volume = value['volume']
                timestamp = value['closeTime']
                symbol = value['symbol']
                pricechange = value['priceChange']
                prisechangepercentage = value['priceChangePercent']
                weightedaverageprice = value['weightedAvgPrice']
                totaltradebaseasset = value['volume']

                ETHUSDT_data = EfxPercentageChangeEtcusdt(
                    open=open,
                    close=close,
                    high=high,
                    low=low,
                    timestamp=timestamp,
                    symbol=symbol,
                    volume=volume,
                    pricechange=pricechange,
                    prisechangepercentage=prisechangepercentage,
                    weightedaverageprice=weightedaverageprice,
                    totaltradebaseasset=totaltradebaseasset)
                ETHUSDT_data.save()
            if value['symbol'] == "ETCUSDT":
                # /////// get value \\\\\\\\\\\\\
                open = value['openPrice']
                high = value['highPrice']
                low = value['lowPrice']
                close = value['lastPrice']
                volume = value['volume']
                timestamp = value['closeTime']
                symbol = value['symbol']
                pricechange = value['priceChange']
                prisechangepercentage = value['priceChangePercent']
                weightedaverageprice = value['weightedAvgPrice']
                totaltradebaseasset = value['volume']

                ETCUSDT_data = EfxPercentageChangeEthusdt(
                    open=open,
                    close=close,
                    high=high,
                    low=low,
                    timestamp=timestamp,
                    symbol=symbol,
                    volume=volume,
                    pricechange=pricechange,
                    prisechangepercentage=prisechangepercentage,
                    weightedaverageprice=weightedaverageprice,
                    totaltradebaseasset=totaltradebaseasset)
                ETCUSDT_data.save()
            if value['symbol'] == "FILUSDT":
                # /////// get value \\\\\\\\\\\\\
                open = value['openPrice']
                high = value['highPrice']
                low = value['lowPrice']
                close = value['lastPrice']
                volume = value['volume']
                timestamp = value['closeTime']
                symbol = value['symbol']
                pricechange = value['priceChange']
                prisechangepercentage = value['priceChangePercent']
                weightedaverageprice = value['weightedAvgPrice']
                totaltradebaseasset = value['volume']

                FILUSDT_data = EfxPercentageChangeFilusdt(
                    open=open,
                    close=close,
                    high=high,
                    low=low,
                    timestamp=timestamp,
                    symbol=symbol,
                    volume=volume,
                    pricechange=pricechange,
                    prisechangepercentage=prisechangepercentage,
                    weightedaverageprice=weightedaverageprice,
                    totaltradebaseasset=totaltradebaseasset)
                FILUSDT_data.save()
            if value['symbol'] == "FTTUSDT":
                # /////// get value \\\\\\\\\\\\\
                open = value['openPrice']
                high = value['highPrice']
                low = value['lowPrice']
                close = value['lastPrice']
                volume = value['volume']
                timestamp = value['closeTime']
                symbol = value['symbol']
                pricechange = value['priceChange']
                prisechangepercentage = value['priceChangePercent']
                weightedaverageprice = value['weightedAvgPrice']
                totaltradebaseasset = value['volume']

                FTTUSDT_data = EfxPercentageChangeFttusdt(
                    open=open,
                    close=close,
                    high=high,
                    low=low,
                    timestamp=timestamp,
                    symbol=symbol,
                    volume=volume,
                    pricechange=pricechange,
                    prisechangepercentage=prisechangepercentage,
                    weightedaverageprice=weightedaverageprice,
                    totaltradebaseasset=totaltradebaseasset)
                FTTUSDT_data.save()
            if value['symbol'] == "GRTUSDT":
                # /////// get value \\\\\\\\\\\\\
                open = value['openPrice']
                high = value['highPrice']
                low = value['lowPrice']
                close = value['lastPrice']
                volume = value['volume']
                timestamp = value['closeTime']
                symbol = value['symbol']
                pricechange = value['priceChange']
                prisechangepercentage = value['priceChangePercent']
                weightedaverageprice = value['weightedAvgPrice']
                totaltradebaseasset = value['volume']

                GRTUSDT_data = EfxPercentageChangeGrtusdt(
                    open=open,
                    close=close,
                    high=high,
                    low=low,
                    timestamp=timestamp,
                    symbol=symbol,
                    volume=volume,
                    pricechange=pricechange,
                    prisechangepercentage=prisechangepercentage,
                    weightedaverageprice=weightedaverageprice,
                    totaltradebaseasset=totaltradebaseasset)
                GRTUSDT_data.save()
            if value['symbol'] == "IOSTUSDT":
                # /////// get value \\\\\\\\\\\\\
                open = value['openPrice']
                high = value['highPrice']
                low = value['lowPrice']
                close = value['lastPrice']
                volume = value['volume']
                timestamp = value['closeTime']
                symbol = value['symbol']
                pricechange = value['priceChange']
                prisechangepercentage = value['priceChangePercent']
                weightedaverageprice = value['weightedAvgPrice']
                totaltradebaseasset = value['volume']

                IOSTUSDT_data = EfxPercentageChangeIostusdt(
                    open=open,
                    close=close,
                    high=high,
                    low=low,
                    timestamp=timestamp,
                    symbol=symbol,
                    volume=volume,
                    pricechange=pricechange,
                    prisechangepercentage=prisechangepercentage,
                    weightedaverageprice=weightedaverageprice,
                    totaltradebaseasset=totaltradebaseasset)
                IOSTUSDT_data.save()
            if value['symbol'] == "IOTAUSDT":
                # /////// get value \\\\\\\\\\\\\
                open = value['openPrice']
                high = value['highPrice']
                low = value['lowPrice']
                close = value['lastPrice']
                volume = value['volume']
                timestamp = value['closeTime']
                symbol = value['symbol']
                pricechange = value['priceChange']
                prisechangepercentage = value['priceChangePercent']
                weightedaverageprice = value['weightedAvgPrice']
                totaltradebaseasset = value['volume']

                IOTAUSDT_data = EfxPercentageChangeIotausdt(
                    open=open,
                    close=close,
                    high=high,
                    low=low,
                    timestamp=timestamp,
                    symbol=symbol,
                    volume=volume,
                    pricechange=pricechange,
                    prisechangepercentage=prisechangepercentage,
                    weightedaverageprice=weightedaverageprice,
                    totaltradebaseasset=totaltradebaseasset)
                IOTAUSDT_data.save()
            if value['symbol'] == "JSTUSDT":
                # /////// get value \\\\\\\\\\\\\
                open = value['openPrice']
                high = value['highPrice']
                low = value['lowPrice']
                close = value['lastPrice']
                volume = value['volume']
                timestamp = value['closeTime']
                symbol = value['symbol']
                pricechange = value['priceChange']
                prisechangepercentage = value['priceChangePercent']
                weightedaverageprice = value['weightedAvgPrice']
                totaltradebaseasset = value['volume']

                JSTUSDT_data = EfxPercentageChangeJstusdt(
                    open=open,
                    close=close,
                    high=high,
                    low=low,
                    timestamp=timestamp,
                    symbol=symbol,
                    volume=volume,
                    pricechange=pricechange,
                    prisechangepercentage=prisechangepercentage,
                    weightedaverageprice=weightedaverageprice,
                    totaltradebaseasset=totaltradebaseasset)
                JSTUSDT_data.save()
            if value['symbol'] == "KAVAUSDT":
                # /////// get value \\\\\\\\\\\\\
                open = value['openPrice']
                high = value['highPrice']
                low = value['lowPrice']
                close = value['lastPrice']
                volume = value['volume']
                timestamp = value['closeTime']
                symbol = value['symbol']
                pricechange = value['priceChange']
                prisechangepercentage = value['priceChangePercent']
                weightedaverageprice = value['weightedAvgPrice']
                totaltradebaseasset = value['volume']

                KAVAUSDT_data = EfxPercentageChangeKavausdt(
                    open=open,
                    close=close,
                    high=high,
                    low=low,
                    timestamp=timestamp,
                    symbol=symbol,
                    volume=volume,
                    pricechange=pricechange,
                    prisechangepercentage=prisechangepercentage,
                    weightedaverageprice=weightedaverageprice,
                    totaltradebaseasset=totaltradebaseasset)
                KAVAUSDT_data.save()
            if value['symbol'] == "LINKUSDT":
                # /////// get value \\\\\\\\\\\\\
                open = value['openPrice']
                high = value['highPrice']
                low = value['lowPrice']
                close = value['lastPrice']
                volume = value['volume']
                timestamp = value['closeTime']
                symbol = value['symbol']
                pricechange = value['priceChange']
                prisechangepercentage = value['priceChangePercent']
                weightedaverageprice = value['weightedAvgPrice']
                totaltradebaseasset = value['volume']

                LINKUSDT_data = EfxPercentageChangeLinkusdt(
                    open=open,
                    close=close,
                    high=high,
                    low=low,
                    timestamp=timestamp,
                    symbol=symbol,
                    volume=volume,
                    pricechange=pricechange,
                    prisechangepercentage=prisechangepercentage,
                    weightedaverageprice=weightedaverageprice,
                    totaltradebaseasset=totaltradebaseasset)
                LINKUSDT_data.save()
            if value['symbol'] == "LITUSDT":
                # /////// get value \\\\\\\\\\\\\
                open = value['openPrice']
                high = value['highPrice']
                low = value['lowPrice']
                close = value['lastPrice']
                volume = value['volume']
                timestamp = value['closeTime']
                symbol = value['symbol']
                pricechange = value['priceChange']
                prisechangepercentage = value['priceChangePercent']
                weightedaverageprice = value['weightedAvgPrice']
                totaltradebaseasset = value['volume']

                LITUSDT_data = EfxPercentageChangeLitusdt(
                    open=open,
                    close=close,
                    high=high,
                    low=low,
                    timestamp=timestamp,
                    symbol=symbol,
                    volume=volume,
                    pricechange=pricechange,
                    prisechangepercentage=prisechangepercentage,
                    weightedaverageprice=weightedaverageprice,
                    totaltradebaseasset=totaltradebaseasset)
                LITUSDT_data.save()
            if value['symbol'] == "LTCUSDT":
                # /////// get value \\\\\\\\\\\\\
                open = value['openPrice']
                high = value['highPrice']
                low = value['lowPrice']
                close = value['lastPrice']
                volume = value['volume']
                timestamp = value['closeTime']
                symbol = value['symbol']
                pricechange = value['priceChange']
                prisechangepercentage = value['priceChangePercent']
                weightedaverageprice = value['weightedAvgPrice']
                totaltradebaseasset = value['volume']

                LTCUSDT_data = EfxPercentageChangeLtcusdt(
                    open=open,
                    close=close,
                    high=high,
                    low=low,
                    timestamp=timestamp,
                    symbol=symbol,
                    volume=volume,
                    pricechange=pricechange,
                    prisechangepercentage=prisechangepercentage,
                    weightedaverageprice=weightedaverageprice,
                    totaltradebaseasset=totaltradebaseasset)
                LTCUSDT_data.save()
            if value['symbol'] == "MANAUSDT":
                # /////// get value \\\\\\\\\\\\\
                open = value['openPrice']
                high = value['highPrice']
                low = value['lowPrice']
                close = value['lastPrice']
                volume = value['volume']
                timestamp = value['closeTime']
                symbol = value['symbol']
                pricechange = value['priceChange']
                prisechangepercentage = value['priceChangePercent']
                weightedaverageprice = value['weightedAvgPrice']
                totaltradebaseasset = value['volume']

                MANAUSDT_data = EfxPercentageChangeManausdt(
                    open=open,
                    close=close,
                    high=high,
                    low=low,
                    timestamp=timestamp,
                    symbol=symbol,
                    volume=volume,
                    pricechange=pricechange,
                    prisechangepercentage=prisechangepercentage,
                    weightedaverageprice=weightedaverageprice,
                    totaltradebaseasset=totaltradebaseasset)
                MANAUSDT_data.save()
            if value['symbol'] == "MDXUSDT":
                # /////// get value \\\\\\\\\\\\\
                open = value['openPrice']
                high = value['highPrice']
                low = value['lowPrice']
                close = value['lastPrice']
                volume = value['volume']
                timestamp = value['closeTime']
                symbol = value['symbol']
                pricechange = value['priceChange']
                prisechangepercentage = value['priceChangePercent']
                weightedaverageprice = value['weightedAvgPrice']
                totaltradebaseasset = value['volume']

                MDXUSDT_data = EfxPercentageChangeMdxusdt(
                    open=open,
                    close=close,
                    high=high,
                    low=low,
                    timestamp=timestamp,
                    symbol=symbol,
                    volume=volume,
                    pricechange=pricechange,
                    prisechangepercentage=prisechangepercentage,
                    weightedaverageprice=weightedaverageprice,
                    totaltradebaseasset=totaltradebaseasset)
                MDXUSDT_data.save()
            if value['symbol'] == "NEOUSDT":
                # /////// get value \\\\\\\\\\\\\
                open = value['openPrice']
                high = value['highPrice']
                low = value['lowPrice']
                close = value['lastPrice']
                volume = value['volume']
                timestamp = value['closeTime']
                symbol = value['symbol']
                pricechange = value['priceChange']
                prisechangepercentage = value['priceChangePercent']
                weightedaverageprice = value['weightedAvgPrice']
                totaltradebaseasset = value['volume']

                NEOUSDT_data = EfxPercentageChangeNeousdt(
                    open=open,
                    close=close,
                    high=high,
                    low=low,
                    timestamp=timestamp,
                    symbol=symbol,
                    volume=volume,
                    pricechange=pricechange,
                    prisechangepercentage=prisechangepercentage,
                    weightedaverageprice=weightedaverageprice,
                    totaltradebaseasset=totaltradebaseasset)
                NEOUSDT_data.save()
            if value['symbol'] == "OMGUSDT":
                # /////// get value \\\\\\\\\\\\\
                open = value['openPrice']
                high = value['highPrice']
                low = value['lowPrice']
                close = value['lastPrice']
                volume = value['volume']
                timestamp = value['closeTime']
                symbol = value['symbol']
                pricechange = value['priceChange']
                prisechangepercentage = value['priceChangePercent']
                weightedaverageprice = value['weightedAvgPrice']
                totaltradebaseasset = value['volume']

                OMGUSDT_data = EfxPercentageChangeOmgusdt(
                    open=open,
                    close=close,
                    high=high,
                    low=low,
                    timestamp=timestamp,
                    symbol=symbol,
                    volume=volume,
                    pricechange=pricechange,
                    prisechangepercentage=prisechangepercentage,
                    weightedaverageprice=weightedaverageprice,
                    totaltradebaseasset=totaltradebaseasset)
                OMGUSDT_data.save()
            if value['symbol'] == "SUSHIUSDT":
                # /////// get value \\\\\\\\\\\\\
                open = value['openPrice']
                high = value['highPrice']
                low = value['lowPrice']
                close = value['lastPrice']
                volume = value['volume']
                timestamp = value['closeTime']
                symbol = value['symbol']
                pricechange = value['priceChange']
                prisechangepercentage = value['priceChangePercent']
                weightedaverageprice = value['weightedAvgPrice']
                totaltradebaseasset = value['volume']

                SUSHIUSDT_data = EfxPercentageChangeSushiusdt(
                    open=open,
                    close=close,
                    high=high,
                    low=low,
                    timestamp=timestamp,
                    symbol=symbol,
                    volume=volume,
                    pricechange=pricechange,
                    prisechangepercentage=prisechangepercentage,
                    weightedaverageprice=weightedaverageprice,
                    totaltradebaseasset=totaltradebaseasset)
                SUSHIUSDT_data.save()
            if value['symbol'] == "THETAUSDT":
                # /////// get value \\\\\\\\\\\\\
                open = value['openPrice']
                high = value['highPrice']
                low = value['lowPrice']
                close = value['lastPrice']
                volume = value['volume']
                timestamp = value['closeTime']
                symbol = value['symbol']
                pricechange = value['priceChange']
                prisechangepercentage = value['priceChangePercent']
                weightedaverageprice = value['weightedAvgPrice']
                totaltradebaseasset = value['volume']

                THETAUSDT_data = EfxPercentageChangeThetausdt(
                    open=open,
                    close=close,
                    high=high,
                    low=low,
                    timestamp=timestamp,
                    symbol=symbol,
                    volume=volume,
                    pricechange=pricechange,
                    prisechangepercentage=prisechangepercentage,
                    weightedaverageprice=weightedaverageprice,
                    totaltradebaseasset=totaltradebaseasset)
                THETAUSDT_data.save()
            if value['symbol'] == "TRXUSDT":
                # /////// get value \\\\\\\\\\\\\
                open = value['openPrice']
                high = value['highPrice']
                low = value['lowPrice']
                close = value['lastPrice']
                volume = value['volume']
                timestamp = value['closeTime']
                symbol = value['symbol']
                pricechange = value['priceChange']
                prisechangepercentage = value['priceChangePercent']
                weightedaverageprice = value['weightedAvgPrice']
                totaltradebaseasset = value['volume']

                TRXUSDT_data = EfxPercentageChangeTrxusdt(
                    open=open,
                    close=close,
                    high=high,
                    low=low,
                    timestamp=timestamp,
                    symbol=symbol,
                    volume=volume,
                    pricechange=pricechange,
                    prisechangepercentage=prisechangepercentage,
                    weightedaverageprice=weightedaverageprice,
                    totaltradebaseasset=totaltradebaseasset)
                TRXUSDT_data.save()
            if value['symbol'] == "UNIUSDT":
                # /////// get value \\\\\\\\\\\\\
                open = value['openPrice']
                high = value['highPrice']
                low = value['lowPrice']
                close = value['lastPrice']
                volume = value['volume']
                timestamp = value['closeTime']
                symbol = value['symbol']
                pricechange = value['priceChange']
                prisechangepercentage = value['priceChangePercent']
                weightedaverageprice = value['weightedAvgPrice']
                totaltradebaseasset = value['volume']

                UNIUSDT_data = EfxPercentageChangeUniusdt(
                    open=open,
                    close=close,
                    high=high,
                    low=low,
                    timestamp=timestamp,
                    symbol=symbol,
                    volume=volume,
                    pricechange=pricechange,
                    prisechangepercentage=prisechangepercentage,
                    weightedaverageprice=weightedaverageprice,
                    totaltradebaseasset=totaltradebaseasset)
                UNIUSDT_data.save()
            if value['symbol'] == "XMRUSDT":
                # /////// get value \\\\\\\\\\\\\
                open = value['openPrice']
                high = value['highPrice']
                low = value['lowPrice']
                close = value['lastPrice']
                volume = value['volume']
                timestamp = value['closeTime']
                symbol = value['symbol']
                pricechange = value['priceChange']
                prisechangepercentage = value['priceChangePercent']
                weightedaverageprice = value['weightedAvgPrice']
                totaltradebaseasset = value['volume']

                XMRUSDT_data = EfxPercentageChangeXmrusdt(
                    open=open,
                    close=close,
                    high=high,
                    low=low,
                    timestamp=timestamp,
                    symbol=symbol,
                    volume=volume,
                    pricechange=pricechange,
                    prisechangepercentage=prisechangepercentage,
                    weightedaverageprice=weightedaverageprice,
                    totaltradebaseasset=totaltradebaseasset)
                XMRUSDT_data.save()
            if value['symbol'] == "XTZUSDT":
                # /////// get value \\\\\\\\\\\\\
                open = value['openPrice']
                high = value['highPrice']
                low = value['lowPrice']
                close = value['lastPrice']
                volume = value['volume']
                timestamp = value['closeTime']
                symbol = value['symbol']
                pricechange = value['priceChange']
                prisechangepercentage = value['priceChangePercent']
                weightedaverageprice = value['weightedAvgPrice']
                totaltradebaseasset = value['volume']

                XTZUSDT_data = EfxPercentageChangeXtzusdt(
                    open=open,
                    close=close,
                    high=high,
                    low=low,
                    timestamp=timestamp,
                    symbol=symbol,
                    volume=volume,
                    pricechange=pricechange,
                    prisechangepercentage=prisechangepercentage,
                    weightedaverageprice=weightedaverageprice,
                    totaltradebaseasset=totaltradebaseasset)
                XTZUSDT_data.save()
            if value['symbol'] == "SHIBUSDT":
                # /////// get value \\\\\\\\\\\\\
                open = value['openPrice']
                high = value['highPrice']
                low = value['lowPrice']
                close = value['lastPrice']
                volume = value['volume']
                timestamp = value['closeTime']
                symbol = value['symbol']
                pricechange = value['priceChange']
                prisechangepercentage = value['priceChangePercent']
                weightedaverageprice = value['weightedAvgPrice']
                totaltradebaseasset = value['volume']

                SHIBUSDT_data = EfxPercentageChangeShibusdt(
                    open=open,
                    close=close,
                    high=high,
                    low=low,
                    timestamp=timestamp,
                    symbol=symbol,
                    volume=volume,
                    pricechange=pricechange,
                    prisechangepercentage=prisechangepercentage,
                    weightedaverageprice=weightedaverageprice,
                    totaltradebaseasset=totaltradebaseasset)
                SHIBUSDT_data.save()
            if value['symbol'] == "RVNUSDT":
                # /////// get value \\\\\\\\\\\\\
                open = value['openPrice']
                high = value['highPrice']
                low = value['lowPrice']
                close = value['lastPrice']
                volume = value['volume']
                timestamp = value['closeTime']
                symbol = value['symbol']
                pricechange = value['priceChange']
                prisechangepercentage = value['priceChangePercent']
                weightedaverageprice = value['weightedAvgPrice']
                totaltradebaseasset = value['volume']

                RVNUSDT_data = EfxPercentageChangeRvnusdt(
                    open=open,
                    close=close,
                    high=high,
                    low=low,
                    timestamp=timestamp,
                    symbol=symbol,
                    volume=volume,
                    pricechange=pricechange,
                    prisechangepercentage=prisechangepercentage,
                    weightedaverageprice=weightedaverageprice,
                    totaltradebaseasset=totaltradebaseasset)
                RVNUSDT_data.save()
            if value['symbol'] == "MATICUSDT":
                # /////// get value \\\\\\\\\\\\\
                open = value['openPrice']
                high = value['highPrice']
                low = value['lowPrice']
                close = value['lastPrice']
                volume = value['volume']
                timestamp = value['closeTime']
                symbol = value['symbol']
                pricechange = value['priceChange']
                prisechangepercentage = value['priceChangePercent']
                weightedaverageprice = value['weightedAvgPrice']
                totaltradebaseasset = value['volume']

                MATICUSDT_data = EfxPercentageChangeMaticusdt(
                    open=open,
                    close=close,
                    high=high,
                    low=low,
                    timestamp=timestamp,
                    symbol=symbol,
                    volume=volume,
                    pricechange=pricechange,
                    prisechangepercentage=prisechangepercentage,
                    weightedaverageprice=weightedaverageprice,
                    totaltradebaseasset=totaltradebaseasset)
                MATICUSDT_data.save()
            if value['symbol'] == "CELRUSDT":
                # /////// get value \\\\\\\\\\\\\
                open = value['openPrice']
                high = value['highPrice']
                low = value['lowPrice']
                close = value['lastPrice']
                volume = value['volume']
                timestamp = value['closeTime']
                symbol = value['symbol']
                pricechange = value['priceChange']
                prisechangepercentage = value['priceChangePercent']
                weightedaverageprice = value['weightedAvgPrice']
                totaltradebaseasset = value['volume']

                CELRUSDT_data = EfxPercentageChangeCelrusdt(
                    open=open,
                    close=close,
                    high=high,
                    low=low,
                    timestamp=timestamp,
                    symbol=symbol,
                    volume=volume,
                    pricechange=pricechange,
                    prisechangepercentage=prisechangepercentage,
                    weightedaverageprice=weightedaverageprice,
                    totaltradebaseasset=totaltradebaseasset)
                CELRUSDT_data.save()
            if value['symbol'] == "BTCTUSDT":
                # /////// get value \\\\\\\\\\\\\
                open = value['openPrice']
                high = value['highPrice']
                low = value['lowPrice']
                close = value['lastPrice']
                volume = value['volume']
                timestamp = value['closeTime']
                symbol = value['symbol']
                pricechange = value['priceChange']
                prisechangepercentage = value['priceChangePercent']
                weightedaverageprice = value['weightedAvgPrice']
                totaltradebaseasset = value['volume']

                BTCTUSDT_data = EfxPercentageChangeBtctusdt(
                    open=open,
                    close=close,
                    high=high,
                    low=low,
                    timestamp=timestamp,
                    symbol=symbol,
                    volume=volume,
                    pricechange=pricechange,
                    prisechangepercentage=prisechangepercentage,
                    weightedaverageprice=weightedaverageprice,
                    totaltradebaseasset=totaltradebaseasset)
                BTCTUSDT_data.save()
            if value['symbol'] == "XRPUSDT":
                # /////// get value \\\\\\\\\\\\\
                open = value['openPrice']
                high = value['highPrice']
                low = value['lowPrice']
                close = value['lastPrice']
                volume = value['volume']
                timestamp = value['closeTime']
                symbol = value['symbol']
                pricechange = value['priceChange']
                prisechangepercentage = value['priceChangePercent']
                weightedaverageprice = value['weightedAvgPrice']
                totaltradebaseasset = value['volume']

                XRPUSDT_data = EfxPercentageChangeXrpusdt(
                    open=open,
                    close=close,
                    high=high,
                    low=low,
                    timestamp=timestamp,
                    symbol=symbol,
                    volume=volume,
                    pricechange=pricechange,
                    prisechangepercentage=prisechangepercentage,
                    weightedaverageprice=weightedaverageprice,
                    totaltradebaseasset=totaltradebaseasset)
                XRPUSDT_data.save()

    except Exception as e:
        e = str(e)
        print(e + ',error')
