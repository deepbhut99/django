var EFX = angular.module("efx", ['djng.urls']);
var config = {
  headers: {
    "Content-Type": "application/x-www-form-urlencoded;charset=utf-8;",
    "Access-Control-Allow-Origin": "*",
  },
};
EFX.config(function ($interpolateProvider) {
  $interpolateProvider.startSymbol("{[{");
  $interpolateProvider.endSymbol("}]}");
});

// WEBSOCKET START FOR BOT TRADE
// BINANCE API
var toast = {
  timeOut: 5e3,
  closeButton: !0,
  debug: !1,
  newestOnTop: !0,
  progressBar: !0,
  positionClass: "toast-top-right",
  preventDuplicates: !0,
  onclick: null,
  showDuration: "300",
  hideDuration: "1000",
  extendedTimeOut: "1000",
  showEasing: "swing",
  hideEasing: "linear",
  showMethod: "fadeIn",
  hideMethod: "fadeOut",
  tapToDismiss: !1,
};
var binanceSocket1 = new WebSocket(
  "wss://stream.binance.com:9443/ws/!ticker@arr"
);
var priseopen = 0;
var pricechangepercentage = 0;
binanceSocket1.onmessage = function (event) {
  var message = JSON.parse(event.data);
  for (const ele of message) {
    if (ele.s === "1INCHUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#1INCHUSDT-h").text(ele.s);
      $("#1INCHUSDT-p").text(pricechangepercentage + "%");
      $("#1INCHUSDT-c").text(priseopenf);
    }
    if (ele.s === "AAVEUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#AAVEUSDT-h").text(ele.s);
      $("#AAVEUSDT-p").text(pricechangepercentage + "%");
      $("#AAVEUSDT-c").text(priseopenf);
    }
    if (ele.s === "ADAUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#ADAUSDT-h").text(ele.s);
      $("#ADAUSDT-p").text(pricechangepercentage + "%");
      $("#ADAUSDT-c").text(priseopenf);
    }

    if (ele.s === "AKROUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#AKROUSDT-h").text(ele.s);
      $("#AKROUSDT-p").text(pricechangepercentage + "%");
      $("#AKROUSDT-c").text(priseopenf);
    }

    if (ele.s === "ANTUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#ANTUSDT-h").text(ele.s);
      $("#ANTUSDT-p").text(pricechangepercentage + "%");
      $("#ANTUSDT-c").text(priseopenf);
    }

    if (ele.s === "ATOMUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#ATOMUSDT-h").text(ele.s);
      $("#ATOMUSDT-p").text(pricechangepercentage + "%");
      $("#ATOMUSDT-c").text(priseopenf);
    }
    if (ele.s === "BATUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#BATUSDT-h").text(ele.s);
      $("#BATUSDT-p").text(pricechangepercentage + "%");
      $("#BATUSDT-c").text(priseopenf);
    }
    if (ele.s === "BCHUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#BCHUSDT-h").text(ele.s);
      $("#BCHUSDT-p").text(pricechangepercentage + "%");
      $("#BCHUSDT-c").text(priseopenf);
    }
    if (ele.s === "BNBUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#BNBUSDT-h").text(ele.s);
      $("#BNBUSDT-p").text(pricechangepercentage + "%");
      $("#BNBUSDT-c").text(priseopenf);
    }

    if (ele.s === "BTCUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#BTCUSDT-h").text(ele.s);
      $("#BTCUSDT-p").text(pricechangepercentage + "%");
      $("#BTCUSDT-c").text(priseopenf);
    }
    if (ele.s === "CAKEUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#CAKEUSDT-h").text(ele.s);
      $("#CAKEUSDT-p").text(pricechangepercentage + "%");
      $("#CAKEUSDT-c").text(priseopenf);
    }
    if (ele.s === "COMPUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#COMPUSDT-h").text(ele.s);
      $("#COMPUSDT-p").text(pricechangepercentage + "%");
      $("#COMPUSDT-c").text(priseopenf);
    }
    if (ele.s === "COSUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#COSUSDT-h").text(ele.s);
      $("#COSUSDT-p").text(pricechangepercentage + "%");
      $("#COSUSDT-c").text(priseopenf);
    }
    if (ele.s === "CRVUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#CRVUSDT-h").text(ele.s);
      $("#CRVUSDT-p").text(pricechangepercentage + "%");
      $("#CRVUSDT-c").text(priseopenf);
    }
    if (ele.s === "DASHUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#DASHUSDT-h").text(ele.s);
      $("#DASHUSDT-p").text(pricechangepercentage + "%");
      $("#DASHUSDT-c").text(priseopenf);
    }
    if (ele.s === "DOGEUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#DOGEUSDT-h").text(ele.s);
      $("#DOGEUSDT-p").text(pricechangepercentage + "%");
      $("#DOGEUSDT-c").text(priseopenf);
    }
    if (ele.s === "DOTUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#DOTUSDT-h").text(ele.s);
      $("#DOTUSDT-p").text(pricechangepercentage + "%");
      $("#DOTUSDT-c").text(priseopenf);
    }
    if (ele.s === "EOSUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#EOSUSDT-h").text(ele.s);
      $("#EOSUSDT-p").text(pricechangepercentage + "%");
      $("#EOSUSDT-c").text(priseopenf);
    }
    if (ele.s === "ETHUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#ETHUSDT-h").text(ele.s);
      $("#ETHUSDT-p").text(pricechangepercentage + "%");
      $("#ETHUSDT-c").text(priseopenf);
    }
    if (ele.s === "ETCUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#ETCUSDT-h").text(ele.s);
      $("#ETCUSDT-p").text(pricechangepercentage + "%");
      $("#ETCUSDT-c").text(priseopenf);
    }
    if (ele.s === "FILUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#FILUSDT-h").text(ele.s);
      $("#FILUSDT-p").text(pricechangepercentage + "%");
      $("#FILUSDT-c").text(priseopenf);
    }
    if (ele.s === "FTTUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#FTTUSDT-h").text(ele.s);
      $("#FTTUSDT-p").text(pricechangepercentage + "%");
      $("#FTTUSDT-c").text(priseopenf);
    }
    if (ele.s === "GRTUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#GRTUSDT-h").text(ele.s);
      $("#GRTUSDT-p").text(pricechangepercentage + "%");
      $("#GRTUSDT-c").text(priseopenf);
    }
    if (ele.s === "IOSTUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#IOSTUSDT-h").text(ele.s);
      $("#IOSTUSDT-p").text(pricechangepercentage + "%");
      $("#IOSTUSDT-c").text(priseopenf);
    }
    if (ele.s === "IOTAUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#IOTAUSDT-h").text(ele.s);
      $("#IOTAUSDT-p").text(pricechangepercentage + "%");
      $("#IOTAUSDT-c").text(priseopenf);
    }
    if (ele.s === "JSTUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#JSTUSDT-h").text(ele.s);
      $("#JSTUSDT-p").text(pricechangepercentage + "%");
      $("#JSTUSDT-c").text(priseopenf);
    }
    if (ele.s === "KAVAUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#KAVAUSDT-h").text(ele.s);
      $("#KAVAUSDT-p").text(pricechangepercentage + "%");
      $("#KAVAUSDT-c").text(priseopenf);
    }
    if (ele.s === "LINKUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#LINKUSDT-h").text(ele.s);
      $("#LINKUSDT-p").text(pricechangepercentage + "%");
      $("#LINKUSDT-c").text(priseopenf);
    }
    if (ele.s === "LITUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#LITUSDT-h").text(ele.s);
      $("#LITUSDT-p").text(pricechangepercentage + "%");
      $("#LITUSDT-c").text(priseopenf);
    }
    if (ele.s === "LTCUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#LTCUSDT-h").text(ele.s);
      $("#LTCUSDT-p").text(pricechangepercentage + "%");
      $("#LTCUSDT-c").text(priseopenf);
    }
    if (ele.s === "MANAUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#MANAUSDT-h").text(ele.s);
      $("#MANAUSDT-p").text(pricechangepercentage + "%");
      $("#MANAUSDT-c").text(priseopenf);
    }
    if (ele.s === "MDXUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#MDXUSDT-h").text(ele.s);
      $("#MDXUSDT-p").text(pricechangepercentage + "%");
      $("#MDXUSDT-c").text(priseopenf);
    }
    if (ele.s === "NEOUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#NEOUSDT-h").text(ele.s);
      $("#NEOUSDT-p").text(pricechangepercentage + "%");
      $("#NEOUSDT-c").text(priseopenf);
    }
    if (ele.s === "OMGUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#OMGUSDT-h").text(ele.s);
      $("#OMGUSDT-p").text(pricechangepercentage + "%");
      $("#OMGUSDT-c").text(priseopenf);
    }
    if (ele.s === "SUSHIUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#SUSHIUSDT-h").text(ele.s);
      $("#SUSHIUSDT-p").text(pricechangepercentage + "%");
      $("#SUSHIUSDT-c").text(priseopenf);
    }
    if (ele.s === "THETAUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#THETAUSDT-h").text(ele.s);
      $("#THETAUSDT-p").text(pricechangepercentage + "%");
      $("#THETAUSDT-c").text(priseopenf);
    }
    if (ele.s === "TRXUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#TRXUSDT-h").text(ele.s);
      $("#TRXUSDT-p").text(pricechangepercentage + "%");
      $("#TRXUSDT-c").text(priseopenf);
    }
    if (ele.s === "UNIUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#UNIUSDT-h").text(ele.s);
      $("#UNIUSDT-p").text(pricechangepercentage + "%");
      $("#UNIUSDT-c").text(priseopenf);
    }
    if (ele.s === "XMRUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#XMRUSDT-h").text(ele.s);
      $("#XMRUSDT-p").text(pricechangepercentage + "%");
      $("#XMRUSDT-c").text(priseopenf);
    }
    if (ele.s === "XRPUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#XRPUSDT-h").text(ele.s);
      $("#XRPUSDT-p").text(pricechangepercentage + "%");
      $("#XRPUSDT-c").text(priseopenf);
    }
    if (ele.s === "XTZUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#XTZUSDT-h").text(ele.s);
      $("#XTZUSDT-p").text(pricechangepercentage + "%");
      $("#XTZUSDT-c").text(priseopenf);
    }
    if (ele.s === "RVNUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#RVNUSDT-h").text(ele.s);
      $("#RVNUSDT-p").text(pricechangepercentage + "%");
      $("#RVNUSDT-c").text(priseopenf);
    }
    if (ele.s === "SHIBUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(8);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#SHIBUSDT-h").text(ele.s);
      $("#SHIBUSDT-p").text(pricechangepercentage + "%");
      $("#SHIBUSDT-c").text(priseopenf);
    }
    if (ele.s === "MATICUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#MATICUSDT-h").text(ele.s);
      $("#MATICUSDT-p").text(pricechangepercentage + "%");
      $("#MATICUSDT-c").text(priseopenf);
    }
    if (ele.s === "CELRUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#CELRUSDT-h").text(ele.s);
      $("#CELRUSDT-p").text(pricechangepercentage + "%");
      $("#CELRUSDT-c").text(priseopenf);
    }
  }
};
var binanceSocket2 = new WebSocket("wss://fstream.binance.com/ws/!ticker@arr");
var priseopen = 0;
var pricechangepercentage = 0;
binanceSocket2.onmessage = function (event) {
  var message = JSON.parse(event.data);
  for (const ele of message) {
    if (ele.s === "AAVEUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#AAVEUSDT-hf").text(ele.s);
      $("#AAVEUSDT-pf").text(pricechangepercentage + "%");
      $("#AAVEUSDT-cf").text(priseopenf);
      $("#AAVEUSDT-futuretradelastpoint").val(priseopenf);
    }
    if (ele.s === "ADAUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#ADAUSDT-hf").text(ele.s);
      $("#ADAUSDT-pf").text(pricechangepercentage + "%");
      $("#ADAUSDT-cf").text(priseopenf);
      $("#ADAUSDT-futuretradelastpoint").val(priseopenf);
    }

    if (ele.s === "AKROUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#AKROUSDT-hf").text(ele.s);
      $("#AKROUSDT-pf").text(pricechangepercentage + "%");
      $("#AKROUSDT-cf").text(priseopenf);
      $("#AKROUSDT-futuretradelastpoint").val(priseopenf);
    }

    if (ele.s === "ANTUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#ANTUSDT-hf").text(ele.s);
      $("#ANTUSDT-pf").text(pricechangepercentage + "%");
      $("#ANTUSDT-cf").text(priseopenf);
      $("#ANTUSDT-futuretradelastpoint").val(priseopenf);
    }

    if (ele.s === "ATOMUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#ATOMUSDT-hf").text(ele.s);
      $("#ATOMUSDT-pf").text(pricechangepercentage + "%");
      $("#ATOMUSDT-cf").text(priseopenf);
      $("#ATOMUSDT-futuretradelastpoint").val(priseopenf);
    }
    if (ele.s === "BATUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#BATUSDT-hf").text(ele.s);
      $("#BATUSDT-pf").text(pricechangepercentage + "%");
      $("#BATUSDT-cf").text(priseopenf);
      $("#BATUSDT-futuretradelastpoint").val(priseopenf);
    }
    if (ele.s === "BCHUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#BCHUSDT-hf").text(ele.s);
      $("#BCHUSDT-pf").text(pricechangepercentage + "%");
      $("#BCHUSDT-cf").text(priseopenf);
      $("#BCHUSDT-futuretradelastpoint").val(priseopenf);
    }
    if (ele.s === "BNBUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#BNBUSDT-hf").text(ele.s);
      $("#BNBUSDT-pf").text(pricechangepercentage + "%");
      $("#BNBUSDT-cf").text(priseopenf);
      $("#BNBUSDT-futuretradelastpoint").val(priseopenf);
    }

    if (ele.s === "BTCUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#BTCUSDT-hf").text(ele.s);
      $("#BTCUSDT-pf").text(pricechangepercentage + "%");
      $("#BTCUSDT-cf").text(priseopenf);
      $("#BTCUSDT-futuretradelastpoint").val(priseopenf);
    }
    if (ele.s === "COMPUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#COMPUSDT-hf").text(ele.s);
      $("#COMPUSDT-pf").text(pricechangepercentage + "%");
      $("#COMPUSDT-cf").text(priseopenf);
      $("#COMPUSDT-futuretradelastpoint").val(priseopenf);
    }
    if (ele.s === "CRVUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#CRVUSDT-hf").text(ele.s);
      $("#CRVUSDT-pf").text(pricechangepercentage + "%");
      $("#CRVUSDT-cf").text(priseopenf);
      $("#CRVUSDT-futuretradelastpoint").val(priseopenf);
    }
    if (ele.s === "DASHUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#DASHUSDT-hf").text(ele.s);
      $("#DASHUSDT-pf").text(pricechangepercentage + "%");
      $("#DASHUSDT-cf").text(priseopenf);
      $("#DASHUSDT-futuretradelastpoint").val(priseopenf);
    }
    if (ele.s === "DOGEUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#DOGEUSDT-hf").text(ele.s);
      $("#DOGEUSDT-pf").text(pricechangepercentage + "%");
      $("#DOGEUSDT-cf").text(priseopenf);
      $("#DOGEUSDT-futuretradelastpoint").val(priseopenf);
    }
    if (ele.s === "DOTUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#DOTUSDT-hf").text(ele.s);
      $("#DOTUSDT-pf").text(pricechangepercentage + "%");
      $("#DOTUSDT-cf").text(priseopenf);
      $("#DOTUSDT-futuretradelastpoint").val(priseopenf);
    }
    if (ele.s === "EOSUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#EOSUSDT-hf").text(ele.s);
      $("#EOSUSDT-pf").text(pricechangepercentage + "%");
      $("#EOSUSDT-cf").text(priseopenf);
      $("#EOSUSDT-futuretradelastpoint").val(priseopenf);
    }
    if (ele.s === "ETHUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#ETHUSDT-hf").text(ele.s);
      $("#ETHUSDT-pf").text(pricechangepercentage + "%");
      $("#ETHUSDT-cf").text(priseopenf);
      $("#ETHUSDT-futuretradelastpoint").val(priseopenf);
    }
    if (ele.s === "ETCUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#ETCUSDT-hf").text(ele.s);
      $("#ETCUSDT-pf").text(pricechangepercentage + "%");
      $("#ETCUSDT-cf").text(priseopenf);
      $("#ETCUSDT-futuretradelastpoint").val(priseopenf);
    }
    if (ele.s === "FILUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#FILUSDT-hf").text(ele.s);
      $("#FILUSDT-pf").text(pricechangepercentage + "%");
      $("#FILUSDT-cf").text(priseopenf);
      $("#FILUSDT-futuretradelastpoint").val(priseopenf);
    }
    if (ele.s === "GRTUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#GRTUSDT-hf").text(ele.s);
      $("#GRTUSDT-pf").text(pricechangepercentage + "%");
      $("#GRTUSDT-cf").text(priseopenf);
      $("#GRTUSDT-futuretradelastpoint").val(priseopenf);
    }
    if (ele.s === "IOSTUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#IOSTUSDT-hf").text(ele.s);
      $("#IOSTUSDT-pf").text(pricechangepercentage + "%");
      $("#IOSTUSDT-cf").text(priseopenf);
      $("#IOSTUSDT-futuretradelastpoint").val(priseopenf);
    }
    if (ele.s === "IOTAUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#IOTAUSDT-hf").text(ele.s);
      $("#IOTAUSDT-pf").text(pricechangepercentage + "%");
      $("#IOTAUSDT-cf").text(priseopenf);
      $("#IOTAUSDT-futuretradelastpoint").val(priseopenf);
    }
    if (ele.s === "KAVAUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#KAVAUSDT-hf").text(ele.s);
      $("#KAVAUSDT-pf").text(pricechangepercentage + "%");
      $("#KAVAUSDT-cf").text(priseopenf);
      $("#KAVAUSDT-futuretradelastpoint").val(priseopenf);
    }
    if (ele.s === "LINKUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#LINKUSDT-hf").text(ele.s);
      $("#LINKUSDT-pf").text(pricechangepercentage + "%");
      $("#LINKUSDT-cf").text(priseopenf);
      $("#LINKUSDT-futuretradelastpoint").val(priseopenf);
    }
    if (ele.s === "LITUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#LITUSDT-hf").text(ele.s);
      $("#LITUSDT-pf").text(pricechangepercentage + "%");
      $("#LITUSDT-cf").text(priseopenf);
      $("#LITUSDT-futuretradelastpoint").val(priseopenf);
    }
    if (ele.s === "LTCUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#LTCUSDT-hf").text(ele.s);
      $("#LTCUSDT-pf").text(pricechangepercentage + "%");
      $("#LTCUSDT-cf").text(priseopenf);
      $("#LTCUSDT-futuretradelastpoint").val(priseopenf);
    }
    if (ele.s === "MANAUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#MANAUSDT-hf").text(ele.s);
      $("#MANAUSDT-pf").text(pricechangepercentage + "%");
      $("#MANAUSDT-cf").text(priseopenf);
      $("#MANAUSDT-futuretradelastpoint").val(priseopenf);
    }
    if (ele.s === "NEOUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#NEOUSDT-hf").text(ele.s);
      $("#NEOUSDT-pf").text(pricechangepercentage + "%");
      $("#NEOUSDT-cf").text(priseopenf);
      $("#NEOUSDT-futuretradelastpoint").val(priseopenf);
    }
    if (ele.s === "OMGUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#OMGUSDT-hf").text(ele.s);
      $("#OMGUSDT-pf").text(pricechangepercentage + "%");
      $("#OMGUSDT-cf").text(priseopenf);
      $("#OMGUSDT-futuretradelastpoint").val(priseopenf);
    }
    if (ele.s === "SUSHIUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#SUSHIUSDT-hf").text(ele.s);
      $("#SUSHIUSDT-pf").text(pricechangepercentage + "%");
      $("#SUSHIUSDT-cf").text(priseopenf);
      $("#SUSHIUSDT-futuretradelastpoint").val(priseopenf);
    }
    if (ele.s === "THETAUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#THETAUSDT-hf").text(ele.s);
      $("#THETAUSDT-pf").text(pricechangepercentage + "%");
      $("#THETAUSDT-cf").text(priseopenf);
      $("#THETAUSDT-futuretradelastpoint").val(priseopenf);
    }
    if (ele.s === "TRXUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#TRXUSDT-hf").text(ele.s);
      $("#TRXUSDT-pf").text(pricechangepercentage + "%");
      $("#TRXUSDT-cf").text(priseopenf);
      $("#TRXUSDT-futuretradelastpoint").val(priseopenf);
    }
    if (ele.s === "UNIUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#UNIUSDT-hf").text(ele.s);
      $("#UNIUSDT-pf").text(pricechangepercentage + "%");
      $("#UNIUSDT-cf").text(priseopenf);
      $("#UNIUSDT-futuretradelastpoint").val(priseopenf);
    }
    if (ele.s === "XMRUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#XMRUSDT-hf").text(ele.s);
      $("#XMRUSDT-pf").text(pricechangepercentage + "%");
      $("#XMRUSDT-cf").text(priseopenf);
      $("#XMRUSDT-futuretradelastpoint").val(priseopenf);
    }
    if (ele.s === "XRPUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#XRPUSDT-hf").text(ele.s);
      $("#XRPUSDT-pf").text(pricechangepercentage + "%");
      $("#XRPUSDT-cf").text(priseopenf);
      $("#XRPUSDT-futuretradelastpoint").val(priseopenf);
    }
    if (ele.s === "XTZUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#XTZUSDT-hf").text(ele.s);
      $("#XTZUSDT-pf").text(pricechangepercentage + "%");
      $("#XTZUSDT-cf").text(priseopenf);
      $("#XTZUSDT-futuretradelastpoint").val(priseopenf);
    }
    if (ele.s === "RVNUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#RVNUSDT-hf").text(ele.s);
      $("#RVNUSDT-pf").text(pricechangepercentage + "%");
      $("#RVNUSDT-cf").text(priseopenf);
      $("#RVNUSDT-futuretradelastpoint").val(priseopenf);
    }
    if (ele.s === "MATICUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(3);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#MATICUSDT-hf").text(ele.s);
      $("#MATICUSDT-pf").text(pricechangepercentage + "%");
      $("#MATICUSDT-cf").text(priseopenf);
      $("#MATICUSDT-futuretradelastpoint").val(priseopenf);
    }
    if (ele.s === "CELRUSDT") {
      priseclose = parseFloat(ele.c);
      pricechangepercentage = parseFloat(ele.P);
      priseopenf = priseclose.toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#CELRUSDT-hf").text(ele.s);
      $("#CELRUSDT-pf").text(pricechangepercentage + "%");
      $("#CELRUSDT-cf").text(priseopenf);
      $("#CELRUSDT-futuretradelastpoint").val(priseopenf);
    }
  }
};
var binanceSocket3 = new WebSocket(
  "wss://fstream.binance.com/ws/!markPrice@arr@1s"
);
var priseopen = 0;
var pricechangepercentage = 0;
binanceSocket3.onmessage = function (event) {
  var message = JSON.parse(event.data);
  for (const ele1 of message) {
    if (ele1.s === "AAVEUSDT") {
      var date1 = new Date(ele1.T);
      var date2 = new Date().getTime();

      var difference = date1.getTime() - date2;

      var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
      difference -= daysDifference * 1000 * 60 * 60 * 24;

      var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
      difference -= hoursDifference * 1000 * 60 * 60;

      var minutesDifference = Math.floor(difference / 1000 / 60);
      difference -= minutesDifference * 1000 * 60;

      var secondsDifference = Math.floor(difference / 1000);

      var minutesDifferencef = minutesDifference + 32;

      var datef =
        hoursDifference + ":" + minutesDifferencef + ":" + secondsDifference;
      priseclose = parseFloat(ele1.r);
      pricechangepercentage = parseFloat(ele1.P);
      priseopenf = priseclose.toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#AAVEUSDT-hf").text(ele1.s);
      $("#AAVEUSDT-tf").text(datef);
      $("#AAVEUSDT-tpf").text(priseopenf + "%");
    }
    if (ele1.s === "ADAUSDT") {
      var date1 = new Date(ele1.T);
      var date2 = Date.now();
      var difference = date1.getTime() - date2;

      var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
      difference -= daysDifference * 1000 * 60 * 60 * 24;

      var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
      difference -= hoursDifference * 1000 * 60 * 60;

      var minutesDifference = Math.floor(difference / 1000 / 60);
      difference -= minutesDifference * 1000 * 60;

      var secondsDifference = Math.floor(difference / 1000);

      var date = new Date(ele1.T);
      var minutesDifferencef = minutesDifference + 32;
      var datef =
        hoursDifference + ":" + minutesDifferencef + ":" + secondsDifference;
      priseclose = parseFloat(ele1.r);
      pricechangepercentage = parseFloat(ele1.P);
      priseopenf = (100 * priseclose.toFixed(6)).toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#ADAUSDT-hf").text(ele1.s);
      $("#ADAUSDT-tf").text(datef);
      $("#ADAUSDT-tpf").text(priseopenf + "%");
    }

    if (ele1.s === "AKROUSDT") {
      var date1 = new Date(ele1.T);
      var date2 = Date.now();
      var difference = date1.getTime() - date2;

      var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
      difference -= daysDifference * 1000 * 60 * 60 * 24;

      var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
      difference -= hoursDifference * 1000 * 60 * 60;

      var minutesDifference = Math.floor(difference / 1000 / 60);
      difference -= minutesDifference * 1000 * 60;

      var secondsDifference = Math.floor(difference / 1000);

      var minutesDifferencef = minutesDifference + 32;
      var date = new Date(ele1.T);
      var datef =
        hoursDifference + ":" + minutesDifferencef + ":" + secondsDifference;
      priseclose = parseFloat(ele1.r);
      pricechangepercentage = parseFloat(ele1.P);
      priseopenf = (100 * priseclose.toFixed(6)).toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#AKROUSDT-hf").text(ele1.s);
      $("#AKROUSDT-tf").text(datef);
      $("#AKROUSDT-tpf").text(priseopenf + "%");
    }

    if (ele1.s === "ANTUSDT") {
      var date1 = new Date(ele1.T);
      var date2 = Date.now();
      var difference = date1.getTime() - date2;

      var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
      difference -= daysDifference * 1000 * 60 * 60 * 24;

      var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
      difference -= hoursDifference * 1000 * 60 * 60;

      var minutesDifference = Math.floor(difference / 1000 / 60);
      difference -= minutesDifference * 1000 * 60;

      var secondsDifference = Math.floor(difference / 1000);

      var minutesDifferencef = minutesDifference + 32;
      var date = new Date(ele1.T);
      var datef =
        hoursDifference + ":" + minutesDifferencef + ":" + secondsDifference;
      priseclose = parseFloat(ele1.r);
      pricechangepercentage = parseFloat(ele1.P);
      priseopenf = (100 * priseclose.toFixed(6)).toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#ANTUSDT-hf").text(ele1.s);
      $("#ANTUSDT-tf").text(datef);
      $("#ANTUSDT-tpf").text(priseopenf + "%");
    }

    if (ele1.s === "ATOMUSDT") {
      var date1 = new Date(ele1.T);
      var date2 = Date.now();
      var difference = date1.getTime() - date2;

      var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
      difference -= daysDifference * 1000 * 60 * 60 * 24;

      var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
      difference -= hoursDifference * 1000 * 60 * 60;

      var minutesDifference = Math.floor(difference / 1000 / 60);
      difference -= minutesDifference * 1000 * 60;

      var secondsDifference = Math.floor(difference / 1000);

      var minutesDifferencef = minutesDifference + 32;
      var date = new Date(ele1.T);
      var datef =
        hoursDifference + ":" + minutesDifferencef + ":" + secondsDifference;
      priseclose = parseFloat(ele1.r);
      pricechangepercentage = parseFloat(ele1.P);
      priseopenf = (100 * priseclose.toFixed(6)).toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#ATOMUSDT-hf").text(ele1.s);
      $("#ATOMUSDT-tf").text(datef);
      $("#ATOMUSDT-tpf").text(priseopenf + "%");
    }
    if (ele1.s === "BATUSDT") {
      var date1 = new Date(ele1.T);
      var date2 = Date.now();
      var difference = date1.getTime() - date2;

      var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
      difference -= daysDifference * 1000 * 60 * 60 * 24;

      var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
      difference -= hoursDifference * 1000 * 60 * 60;

      var minutesDifference = Math.floor(difference / 1000 / 60);
      difference -= minutesDifference * 1000 * 60;

      var secondsDifference = Math.floor(difference / 1000);

      var minutesDifferencef = minutesDifference + 32;
      var date = new Date(ele1.T);
      var datef =
        hoursDifference + ":" + minutesDifferencef + ":" + secondsDifference;
      priseclose = parseFloat(ele1.r);
      pricechangepercentage = parseFloat(ele1.P);
      priseopenf = priseclose.toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#BATUSDT-hf").text(ele1.s);
      $("#BATUSDT-tf").text(datef);
      $("#BATUSDT-tpf").text(priseopenf + "%");
    }
    if (ele1.s === "BCHUSDT") {
      var date1 = new Date(ele1.T);
      var date2 = Date.now();
      var difference = date1.getTime() - date2;

      var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
      difference -= daysDifference * 1000 * 60 * 60 * 24;

      var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
      difference -= hoursDifference * 1000 * 60 * 60;

      var minutesDifference = Math.floor(difference / 1000 / 60);
      difference -= minutesDifference * 1000 * 60;

      var secondsDifference = Math.floor(difference / 1000);

      var minutesDifferencef = minutesDifference + 32;
      var date = new Date(ele1.T);
      var datef =
        hoursDifference + ":" + minutesDifferencef + ":" + secondsDifference;
      priseclose = parseFloat(ele1.r);
      pricechangepercentage = parseFloat(ele1.P);
      priseopenf = priseclose.toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#BCHUSDT-hf").text(ele1.s);
      $("#BCHUSDT-tf").text(datef);
      $("#BCHUSDT-tpf").text(priseopenf + "%");
    }
    if (ele1.s === "BNBUSDT") {
      var date1 = new Date(ele1.T);
      var date2 = Date.now();
      var difference = date1.getTime() - date2;

      var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
      difference -= daysDifference * 1000 * 60 * 60 * 24;

      var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
      difference -= hoursDifference * 1000 * 60 * 60;

      var minutesDifference = Math.floor(difference / 1000 / 60);
      difference -= minutesDifference * 1000 * 60;

      var secondsDifference = Math.floor(difference / 1000);

      var minutesDifferencef = minutesDifference + 32;
      var date = new Date(ele1.T);
      var datef =
        hoursDifference + ":" + minutesDifferencef + ":" + secondsDifference;
      priseclose = parseFloat(ele1.r);
      pricechangepercentage = parseFloat(ele1.P);
      priseopenf = priseclose.toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#BNBUSDT-hf").text(ele1.s);
      $("#BNBUSDT-tf").text(datef);
      $("#BNBUSDT-tpf").text(priseopenf + "%");
    }

    if (ele1.s === "BTCUSDT") {
      var date1 = new Date(ele1.T);
      var date2 = Date.now();
      var difference = date1.getTime() - date2;

      var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
      difference -= daysDifference * 1000 * 60 * 60 * 24;

      var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
      difference -= hoursDifference * 1000 * 60 * 60;

      var minutesDifference = Math.floor(difference / 1000 / 60);
      difference -= minutesDifference * 1000 * 60;

      var secondsDifference = Math.floor(difference / 1000);

      var minutesDifferencef = minutesDifference + 32;
      var date = new Date(ele1.T);
      var datef =
        hoursDifference + ":" + minutesDifferencef + ":" + secondsDifference;
      priseclose = parseFloat(ele1.r);
      pricechangepercentage = parseFloat(ele1.P);
      priseopenf = (100 * priseclose.toFixed(6)).toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#BTCUSDT-hf").text(ele1.s);
      $("#BTCUSDT-tf").text(datef);
      $("#BTCUSDT-tpf").text(priseopenf + "%");
    }
    if (ele1.s === "COMPUSDT") {
      var date1 = new Date(ele1.T);
      var date2 = Date.now();
      var difference = date1.getTime() - date2;

      var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
      difference -= daysDifference * 1000 * 60 * 60 * 24;

      var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
      difference -= hoursDifference * 1000 * 60 * 60;

      var minutesDifference = Math.floor(difference / 1000 / 60);
      difference -= minutesDifference * 1000 * 60;

      var secondsDifference = Math.floor(difference / 1000);

      var minutesDifferencef = minutesDifference + 32;
      var date = new Date(ele1.T);
      var datef =
        hoursDifference + ":" + minutesDifferencef + ":" + secondsDifference;
      priseclose = parseFloat(ele1.r);
      pricechangepercentage = parseFloat(ele1.P);
      priseopenf = (100 * priseclose.toFixed(6)).toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#COMPUSDT-hf").text(ele1.s);
      $("#COMPUSDT-tf").text(datef);
      $("#COMPUSDT-tpf").text(priseopenf + "%");
    }
    if (ele1.s === "CRVUSDT") {
      var date1 = new Date(ele1.T);
      var date2 = Date.now();
      var difference = date1.getTime() - date2;

      var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
      difference -= daysDifference * 1000 * 60 * 60 * 24;

      var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
      difference -= hoursDifference * 1000 * 60 * 60;

      var minutesDifference = Math.floor(difference / 1000 / 60);
      difference -= minutesDifference * 1000 * 60;

      var secondsDifference = Math.floor(difference / 1000);

      var minutesDifferencef = minutesDifference + 32;
      var date = new Date(ele1.T);
      var datef =
        hoursDifference + ":" + minutesDifferencef + ":" + secondsDifference;
      priseclose = parseFloat(ele1.r);
      pricechangepercentage = parseFloat(ele1.P);
      priseopenf = priseclose.toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#CRVUSDT-hf").text(ele1.s);
      $("#CRVUSDT-tf").text(datef);
      $("#CRVUSDT-tpf").text(priseopenf + "%");
    }
    if (ele1.s === "DASHUSDT") {
      var date1 = new Date(ele1.T);
      var date2 = Date.now();
      var difference = date1.getTime() - date2;

      var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
      difference -= daysDifference * 1000 * 60 * 60 * 24;

      var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
      difference -= hoursDifference * 1000 * 60 * 60;

      var minutesDifference = Math.floor(difference / 1000 / 60);
      difference -= minutesDifference * 1000 * 60;

      var secondsDifference = Math.floor(difference / 1000);

      var minutesDifferencef = minutesDifference + 32;
      var date = new Date(ele1.T);
      var datef =
        hoursDifference + ":" + minutesDifferencef + ":" + secondsDifference;
      priseclose = parseFloat(ele1.r);
      pricechangepercentage = parseFloat(ele1.P);
      priseopenf = (100 * priseclose.toFixed(6)).toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#DASHUSDT-hf").text(ele1.s);
      $("#DASHUSDT-tf").text(datef);
      $("#DASHUSDT-tpf").text(priseopenf + "%");
    }
    if (ele1.s === "DOGEUSDT") {
      var date1 = new Date(ele1.T);
      var date2 = Date.now();
      var difference = date1.getTime() - date2;

      var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
      difference -= daysDifference * 1000 * 60 * 60 * 24;

      var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
      difference -= hoursDifference * 1000 * 60 * 60;

      var minutesDifference = Math.floor(difference / 1000 / 60);
      difference -= minutesDifference * 1000 * 60;

      var secondsDifference = Math.floor(difference / 1000);

      var minutesDifferencef = minutesDifference + 32;
      var date = new Date(ele1.T);
      var datef =
        hoursDifference + ":" + minutesDifferencef + ":" + secondsDifference;
      priseclose = parseFloat(ele1.r);
      pricechangepercentage = parseFloat(ele1.P);
      priseopenf = (100 * priseclose.toFixed(6)).toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#DOGEUSDT-hf").text(ele1.s);
      $("#DOGEUSDT-tf").text(datef);
      $("#DOGEUSDT-tpf").text(priseopenf + "%");
    }
    if (ele1.s === "DOTUSDT") {
      var date1 = new Date(ele1.T);
      var date2 = Date.now();
      var difference = date1.getTime() - date2;

      var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
      difference -= daysDifference * 1000 * 60 * 60 * 24;

      var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
      difference -= hoursDifference * 1000 * 60 * 60;

      var minutesDifference = Math.floor(difference / 1000 / 60);
      difference -= minutesDifference * 1000 * 60;

      var secondsDifference = Math.floor(difference / 1000);

      var minutesDifferencef = minutesDifference + 32;
      var date = new Date(ele1.T);
      var datef =
        hoursDifference + ":" + minutesDifferencef + ":" + secondsDifference;
      priseclose = parseFloat(ele1.r);
      pricechangepercentage = parseFloat(ele1.P);
      priseopenf = priseclose.toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#DOTUSDT-hf").text(ele1.s);
      $("#DOTUSDT-tf").text(datef);
      $("#DOTUSDT-tpf").text(priseopenf + "%");
    }
    if (ele1.s === "EOSUSDT") {
      var date1 = new Date(ele1.T);
      var date2 = Date.now();
      var difference = date1.getTime() - date2;

      var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
      difference -= daysDifference * 1000 * 60 * 60 * 24;

      var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
      difference -= hoursDifference * 1000 * 60 * 60;

      var minutesDifference = Math.floor(difference / 1000 / 60);
      difference -= minutesDifference * 1000 * 60;

      var secondsDifference = Math.floor(difference / 1000);

      var minutesDifferencef = minutesDifference + 32;
      var date = new Date(ele1.T);
      var datef =
        hoursDifference + ":" + minutesDifferencef + ":" + secondsDifference;
      priseclose = parseFloat(ele1.r);
      pricechangepercentage = parseFloat(ele1.P);
      priseopenf = priseclose.toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#EOSUSDT-hf").text(ele1.s);
      $("#EOSUSDT-tf").text(datef);
      $("#EOSUSDT-tpf").text(priseopenf + "%");
    }
    if (ele1.s === "ETHUSDT") {
      var date1 = new Date(ele1.T);
      var date2 = Date.now();
      var difference = date1.getTime() - date2;

      var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
      difference -= daysDifference * 1000 * 60 * 60 * 24;

      var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
      difference -= hoursDifference * 1000 * 60 * 60;

      var minutesDifference = Math.floor(difference / 1000 / 60);
      difference -= minutesDifference * 1000 * 60;

      var secondsDifference = Math.floor(difference / 1000);

      var minutesDifferencef = minutesDifference + 32;
      var date = new Date(ele1.T);
      var datef =
        hoursDifference + ":" + minutesDifferencef + ":" + secondsDifference;
      priseclose = parseFloat(ele1.r);
      pricechangepercentage = parseFloat(ele1.P);
      priseopenf = priseclose.toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#ETHUSDT-hf").text(ele1.s);
      $("#ETHUSDT-tf").text(datef);
      $("#ETHUSDT-tpf").text(priseopenf + "%");
    }
    if (ele1.s === "ETCUSDT") {
      var date1 = new Date(ele1.T);
      var date2 = Date.now();
      var difference = date1.getTime() - date2;

      var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
      difference -= daysDifference * 1000 * 60 * 60 * 24;

      var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
      difference -= hoursDifference * 1000 * 60 * 60;

      var minutesDifference = Math.floor(difference / 1000 / 60);
      difference -= minutesDifference * 1000 * 60;

      var secondsDifference = Math.floor(difference / 1000);

      var minutesDifferencef = minutesDifference + 32;
      var date = new Date(ele1.T);
      var datef =
        hoursDifference + ":" + minutesDifferencef + ":" + secondsDifference;
      priseclose = parseFloat(ele1.r);
      pricechangepercentage = parseFloat(ele1.P);
      priseopenf = (100 * priseclose.toFixed(6)).toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#ETCUSDT-hf").text(ele1.s);
      $("#ETCUSDT-tf").text(datef);
      $("#ETCUSDT-tpf").text(priseopenf + "%");
    }
    if (ele1.s === "FILUSDT") {
      var date1 = new Date(ele1.T);
      var date2 = Date.now();
      var difference = date1.getTime() - date2;

      var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
      difference -= daysDifference * 1000 * 60 * 60 * 24;

      var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
      difference -= hoursDifference * 1000 * 60 * 60;

      var minutesDifference = Math.floor(difference / 1000 / 60);
      difference -= minutesDifference * 1000 * 60;

      var secondsDifference = Math.floor(difference / 1000);

      var minutesDifferencef = minutesDifference + 32;
      var date = new Date(ele1.T);
      var datef =
        hoursDifference + ":" + minutesDifferencef + ":" + secondsDifference;
      priseclose = parseFloat(ele1.r);
      pricechangepercentage = parseFloat(ele1.P);
      priseopenf = (100 * priseclose.toFixed(6)).toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#FILUSDT-hf").text(ele1.s);
      $("#FILUSDT-tf").text(datef);
      $("#FILUSDT-tpf").text(priseopenf + "%");
    }
    if (ele1.s === "GRTUSDT") {
      var date1 = new Date(ele1.T);
      var date2 = Date.now();
      var difference = date1.getTime() - date2;

      var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
      difference -= daysDifference * 1000 * 60 * 60 * 24;

      var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
      difference -= hoursDifference * 1000 * 60 * 60;

      var minutesDifference = Math.floor(difference / 1000 / 60);
      difference -= minutesDifference * 1000 * 60;

      var secondsDifference = Math.floor(difference / 1000);

      var minutesDifferencef = minutesDifference + 32;
      var date = new Date(ele1.T);
      var datef =
        hoursDifference + ":" + minutesDifferencef + ":" + secondsDifference;
      priseclose = parseFloat(ele1.r);
      pricechangepercentage = parseFloat(ele1.P);
      priseopenf = (100 * priseclose.toFixed(6)).toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#GRTUSDT-hf").text(ele1.s);
      $("#GRTUSDT-tf").text(datef);
      $("#GRTUSDT-tpf").text(priseopenf + "%");
    }
    if (ele1.s === "IOSTUSDT") {
      var date1 = new Date(ele1.T);
      var date2 = Date.now();
      var difference = date1.getTime() - date2;

      var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
      difference -= daysDifference * 1000 * 60 * 60 * 24;

      var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
      difference -= hoursDifference * 1000 * 60 * 60;

      var minutesDifference = Math.floor(difference / 1000 / 60);
      difference -= minutesDifference * 1000 * 60;

      var secondsDifference = Math.floor(difference / 1000);

      var minutesDifferencef = minutesDifference + 32;
      var date = new Date(ele1.T);
      var datef =
        hoursDifference + ":" + minutesDifferencef + ":" + secondsDifference;
      priseclose = parseFloat(ele1.r);
      pricechangepercentage = parseFloat(ele1.P);
      priseopenf = priseclose.toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#IOSTUSDT-hf").text(ele1.s);
      $("#IOSTUSDT-tf").text(datef);
      $("#IOSTUSDT-tpf").text(priseopenf + "%");
    }
    if (ele1.s === "IOTAUSDT") {
      var date1 = new Date(ele1.T);
      var date2 = Date.now();
      var difference = date1.getTime() - date2;

      var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
      difference -= daysDifference * 1000 * 60 * 60 * 24;

      var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
      difference -= hoursDifference * 1000 * 60 * 60;

      var minutesDifference = Math.floor(difference / 1000 / 60);
      difference -= minutesDifference * 1000 * 60;

      var secondsDifference = Math.floor(difference / 1000);

      var minutesDifferencef = minutesDifference + 32;
      var date = new Date(ele1.T);
      var datef =
        hoursDifference + ":" + minutesDifferencef + ":" + secondsDifference;
      priseclose = parseFloat(ele1.r);
      pricechangepercentage = parseFloat(ele1.P);
      priseopenf = priseclose.toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#IOTAUSDT-hf").text(ele1.s);
      $("#IOTAUSDT-tf").text(datef);
      $("#IOTAUSDT-tpf").text(priseopenf + "%");
    }
    if (ele1.s === "KAVAUSDT") {
      var date1 = new Date(ele1.T);
      var date2 = Date.now();
      var difference = date1.getTime() - date2;

      var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
      difference -= daysDifference * 1000 * 60 * 60 * 24;

      var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
      difference -= hoursDifference * 1000 * 60 * 60;

      var minutesDifference = Math.floor(difference / 1000 / 60);
      difference -= minutesDifference * 1000 * 60;

      var secondsDifference = Math.floor(difference / 1000);

      var minutesDifferencef = minutesDifference + 32;
      var date = new Date(ele1.T);
      var datef =
        hoursDifference + ":" + minutesDifferencef + ":" + secondsDifference;
      priseclose = parseFloat(ele1.r);
      pricechangepercentage = parseFloat(ele1.P);
      priseopenf = (100 * priseclose.toFixed(6)).toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#KAVAUSDT-hf").text(ele1.s);
      $("#KAVAUSDT-tf").text(datef);
      $("#KAVAUSDT-tpf").text(priseopenf + "%");
    }
    if (ele1.s === "LINKUSDT") {
      var date1 = new Date(ele1.T);
      var date2 = Date.now();
      var difference = date1.getTime() - date2;

      var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
      difference -= daysDifference * 1000 * 60 * 60 * 24;

      var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
      difference -= hoursDifference * 1000 * 60 * 60;

      var minutesDifference = Math.floor(difference / 1000 / 60);
      difference -= minutesDifference * 1000 * 60;

      var secondsDifference = Math.floor(difference / 1000);

      var minutesDifferencef = minutesDifference + 32;
      var date = new Date(ele1.T);
      var datef =
        hoursDifference + ":" + minutesDifferencef + ":" + secondsDifference;
      priseclose = parseFloat(ele1.r);
      pricechangepercentage = parseFloat(ele1.P);
      priseopenf = (100 * priseclose.toFixed(6)).toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#LINKUSDT-hf").text(ele1.s);
      $("#LINKUSDT-tf").text(datef);
      $("#LINKUSDT-tpf").text(priseopenf + "%");
    }
    if (ele1.s === "LITUSDT") {
      var date1 = new Date(ele1.T);
      var date2 = Date.now();
      var difference = date1.getTime() - date2;

      var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
      difference -= daysDifference * 1000 * 60 * 60 * 24;

      var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
      difference -= hoursDifference * 1000 * 60 * 60;

      var minutesDifference = Math.floor(difference / 1000 / 60);
      difference -= minutesDifference * 1000 * 60;

      var secondsDifference = Math.floor(difference / 1000);

      var minutesDifferencef = minutesDifference + 32;
      var date = new Date(ele1.T);
      var datef =
        hoursDifference + ":" + minutesDifferencef + ":" + secondsDifference;
      priseclose = parseFloat(ele1.r);
      pricechangepercentage = parseFloat(ele1.P);
      priseopenf = (100 * priseclose.toFixed(6)).toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#LITUSDT-hf").text(ele1.s);
      $("#LITUSDT-tf").text(datef);
      $("#LITUSDT-tpf").text(priseopenf + "%");
    }
    if (ele1.s === "LTCUSDT") {
      var date1 = new Date(ele1.T);
      var date2 = Date.now();
      var difference = date1.getTime() - date2;

      var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
      difference -= daysDifference * 1000 * 60 * 60 * 24;

      var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
      difference -= hoursDifference * 1000 * 60 * 60;

      var minutesDifference = Math.floor(difference / 1000 / 60);
      difference -= minutesDifference * 1000 * 60;

      var secondsDifference = Math.floor(difference / 1000);

      var minutesDifferencef = minutesDifference + 32;
      var date = new Date(ele1.T);
      var datef =
        hoursDifference + ":" + minutesDifferencef + ":" + secondsDifference;
      priseclose = parseFloat(ele1.r);
      pricechangepercentage = parseFloat(ele1.P);
      priseopenf = priseclose.toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#LTCUSDT-hf").text(ele1.s);
      $("#LTCUSDT-tf").text(datef);
      $("#LTCUSDT-tpf").text(priseopenf + "%");
    }
    if (ele1.s === "MANAUSDT") {
      var date1 = new Date(ele1.T);
      var date2 = Date.now();
      var difference = date1.getTime() - date2;

      var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
      difference -= daysDifference * 1000 * 60 * 60 * 24;

      var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
      difference -= hoursDifference * 1000 * 60 * 60;

      var minutesDifference = Math.floor(difference / 1000 / 60);
      difference -= minutesDifference * 1000 * 60;

      var secondsDifference = Math.floor(difference / 1000);

      var minutesDifferencef = minutesDifference + 32;
      var date = new Date(ele1.T);
      var datef =
        hoursDifference + ":" + minutesDifferencef + ":" + secondsDifference;
      priseclose = parseFloat(ele1.r);
      pricechangepercentage = parseFloat(ele1.P);
      priseopenf = (100 * priseclose.toFixed(6)).toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#MANAUSDT-hf").text(ele1.s);
      $("#MANAUSDT-tf").text(datef);
      $("#MANAUSDT-tpf").text(priseopenf + "%");
    }
    if (ele1.s === "NEOUSDT") {
      var date1 = new Date(ele1.T);
      var date2 = Date.now();
      var difference = date1.getTime() - date2;

      var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
      difference -= daysDifference * 1000 * 60 * 60 * 24;

      var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
      difference -= hoursDifference * 1000 * 60 * 60;

      var minutesDifference = Math.floor(difference / 1000 / 60);
      difference -= minutesDifference * 1000 * 60;

      var secondsDifference = Math.floor(difference / 1000);

      var minutesDifferencef = minutesDifference + 32;
      var date = new Date(ele1.T);
      var datef =
        hoursDifference + ":" + minutesDifferencef + ":" + secondsDifference;
      priseclose = parseFloat(ele1.r);
      pricechangepercentage = parseFloat(ele1.P);
      priseopenf = priseclose.toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#NEOUSDT-hf").text(ele1.s);
      $("#NEOUSDT-tf").text(datef);
      $("#NEOUSDT-tpf").text(priseopenf + "%");
    }
    if (ele1.s === "OMGUSDT") {
      var date1 = new Date(ele1.T);
      var date2 = Date.now();
      var difference = date1.getTime() - date2;

      var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
      difference -= daysDifference * 1000 * 60 * 60 * 24;

      var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
      difference -= hoursDifference * 1000 * 60 * 60;

      var minutesDifference = Math.floor(difference / 1000 / 60);
      difference -= minutesDifference * 1000 * 60;

      var secondsDifference = Math.floor(difference / 1000);

      var minutesDifferencef = minutesDifference + 32;
      var date = new Date(ele1.T);
      var datef =
        hoursDifference + ":" + minutesDifferencef + ":" + secondsDifference;
      priseclose = parseFloat(ele1.r);
      pricechangepercentage = parseFloat(ele1.P);
      priseopenf = (100 * priseclose.toFixed(6)).toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#OMGUSDT-hf").text(ele1.s);
      $("#OMGUSDT-tf").text(datef);
      $("#OMGUSDT-tpf").text(priseopenf + "%");
    }
    if (ele1.s === "SUSHIUSDT") {
      var date1 = new Date(ele1.T);
      var date2 = Date.now();
      var difference = date1.getTime() - date2;

      var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
      difference -= daysDifference * 1000 * 60 * 60 * 24;

      var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
      difference -= hoursDifference * 1000 * 60 * 60;

      var minutesDifference = Math.floor(difference / 1000 / 60);
      difference -= minutesDifference * 1000 * 60;

      var secondsDifference = Math.floor(difference / 1000);

      var minutesDifferencef = minutesDifference + 32;
      var date = new Date(ele1.T);
      var datef =
        hoursDifference + ":" + minutesDifferencef + ":" + secondsDifference;
      priseclose = parseFloat(ele1.r);
      pricechangepercentage = parseFloat(ele1.P);
      priseopenf = priseclose.toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#SUSHIUSDT-hf").text(ele1.s);
      $("#SUSHIUSDT-tf").text(datef);
      $("#SUSHIUSDT-tpf").text(priseopenf + "%");
    }
    if (ele1.s === "THETAUSDT") {
      var date1 = new Date(ele1.T);
      var date2 = Date.now();
      var difference = date1.getTime() - date2;

      var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
      difference -= daysDifference * 1000 * 60 * 60 * 24;

      var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
      difference -= hoursDifference * 1000 * 60 * 60;

      var minutesDifference = Math.floor(difference / 1000 / 60);
      difference -= minutesDifference * 1000 * 60;

      var secondsDifference = Math.floor(difference / 1000);

      var minutesDifferencef = minutesDifference + 32;
      var date = new Date(ele1.T);
      var datef =
        hoursDifference + ":" + minutesDifferencef + ":" + secondsDifference;
      priseclose = parseFloat(ele1.r);
      pricechangepercentage = parseFloat(ele1.P);
      priseopenf = (100 * priseclose.toFixed(6)).toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#THETAUSDT-hf").text(ele1.s);
      $("#THETAUSDT-tf").text(datef);
      $("#THETAUSDT-tpf").text(priseopenf + "%");
    }
    if (ele1.s === "TRXUSDT") {
      var date1 = new Date(ele1.T);
      var date2 = Date.now();
      var difference = date1.getTime() - date2;

      var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
      difference -= daysDifference * 1000 * 60 * 60 * 24;

      var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
      difference -= hoursDifference * 1000 * 60 * 60;

      var minutesDifference = Math.floor(difference / 1000 / 60);
      difference -= minutesDifference * 1000 * 60;

      var secondsDifference = Math.floor(difference / 1000);

      var minutesDifferencef = minutesDifference + 32;
      var date = new Date(ele1.T);
      var datef =
        hoursDifference + ":" + minutesDifferencef + ":" + secondsDifference;
      priseclose = parseFloat(ele1.r);
      pricechangepercentage = parseFloat(ele1.P);
      priseopenf = priseclose.toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#TRXUSDT-hf").text(ele1.s);
      $("#TRXUSDT-tf").text(datef);
      $("#TRXUSDT-tpf").text(priseopenf + "%");
    }
    if (ele1.s === "UNIUSDT") {
      var date1 = new Date(ele1.T);
      var date2 = Date.now();
      var difference = date1.getTime() - date2;

      var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
      difference -= daysDifference * 1000 * 60 * 60 * 24;

      var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
      difference -= hoursDifference * 1000 * 60 * 60;

      var minutesDifference = Math.floor(difference / 1000 / 60);
      difference -= minutesDifference * 1000 * 60;

      var secondsDifference = Math.floor(difference / 1000);

      var minutesDifferencef = minutesDifference + 32;
      var date = new Date(ele1.T);
      var datef =
        hoursDifference + ":" + minutesDifferencef + ":" + secondsDifference;
      priseclose = parseFloat(ele1.r);
      pricechangepercentage = parseFloat(ele1.P);
      priseopenf = (100 * priseclose.toFixed(6)).toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#UNIUSDT-hf").text(ele1.s);
      $("#UNIUSDT-tf").text(datef);
      $("#UNIUSDT-tpf").text(priseopenf + "%");
    }
    if (ele1.s === "XMRUSDT") {
      var date1 = new Date(ele1.T);
      var date2 = Date.now();
      var difference = date1.getTime() - date2;

      var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
      difference -= daysDifference * 1000 * 60 * 60 * 24;

      var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
      difference -= hoursDifference * 1000 * 60 * 60;

      var minutesDifference = Math.floor(difference / 1000 / 60);
      difference -= minutesDifference * 1000 * 60;

      var secondsDifference = Math.floor(difference / 1000);

      var minutesDifferencef = minutesDifference + 32;
      var date = new Date(ele1.T);
      var datef =
        hoursDifference + ":" + minutesDifferencef + ":" + secondsDifference;
      priseclose = parseFloat(ele1.r);
      pricechangepercentage = parseFloat(ele1.P);
      priseopenf = priseclose.toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#XMRUSDT-hf").text(ele1.s);
      $("#XMRUSDT-tf").text(datef);
      $("#XMRUSDT-tpf").text(priseopenf + "%");
    }
    if (ele1.s === "XRPUSDT") {
      var date1 = new Date(ele1.T);
      var date2 = Date.now();
      var difference = date1.getTime() - date2;

      var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
      difference -= daysDifference * 1000 * 60 * 60 * 24;

      var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
      difference -= hoursDifference * 1000 * 60 * 60;

      var minutesDifference = Math.floor(difference / 1000 / 60);
      difference -= minutesDifference * 1000 * 60;

      var secondsDifference = Math.floor(difference / 1000);

      var minutesDifferencef = minutesDifference + 32;
      var date = new Date(ele1.T);
      var datef =
        hoursDifference + ":" + minutesDifferencef + ":" + secondsDifference;
      priseclose = parseFloat(ele1.r);
      pricechangepercentage = parseFloat(ele1.P);
      priseopenf = priseclose.toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#XRPUSDT-hf").text(ele1.s);
      $("#XRPUSDT-tf").text(datef);
      $("#XRPUSDT-tpf").text(priseopenf + "%");
    }
    if (ele1.s === "XTZUSDT") {
      var date1 = new Date(ele1.T);
      var date2 = Date.now();
      var difference = date1.getTime() - date2;

      var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
      difference -= daysDifference * 1000 * 60 * 60 * 24;

      var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
      difference -= hoursDifference * 1000 * 60 * 60;

      var minutesDifference = Math.floor(difference / 1000 / 60);
      difference -= minutesDifference * 1000 * 60;

      var secondsDifference = Math.floor(difference / 1000);

      var minutesDifferencef = minutesDifference + 32;
      var date = new Date(ele1.T);
      var datef =
        hoursDifference + ":" + minutesDifferencef + ":" + secondsDifference;
      priseclose = parseFloat(ele1.r);
      pricechangepercentage = parseFloat(ele1.P);
      priseopenf = (100 * priseclose.toFixed(6)).toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#XTZUSDT-hf").text(ele1.s);
      $("#XTZUSDT-tf").text(datef);
      $("#XTZUSDT-tpf").text(priseopenf + "%");
    }
    if (ele1.s === "RVNUSDT") {
      var date1 = new Date(ele1.T);
      var date2 = Date.now();
      var difference = date1.getTime() - date2;

      var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
      difference -= daysDifference * 1000 * 60 * 60 * 24;

      var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
      difference -= hoursDifference * 1000 * 60 * 60;

      var minutesDifference = Math.floor(difference / 1000 / 60);
      difference -= minutesDifference * 1000 * 60;

      var secondsDifference = Math.floor(difference / 1000);

      var minutesDifferencef = minutesDifference + 32;
      var date = new Date(ele1.T);
      var datef =
        hoursDifference + ":" + minutesDifferencef + ":" + secondsDifference;
      priseclose = parseFloat(ele1.r);
      pricechangepercentage = parseFloat(ele1.P);
      priseopenf = (100 * priseclose.toFixed(6)).toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#RVNUSDT-hf").text(ele1.s);
      $("#RVNUSDT-tf").text(datef);
      $("#RVNUSDT-tpf").text(priseopenf + "%");
    }
    if (ele1.s === "MATICUSDT") {
      var date1 = new Date(ele1.T);
      var date2 = Date.now();
      var difference = date1.getTime() - date2;

      var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
      difference -= daysDifference * 1000 * 60 * 60 * 24;

      var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
      difference -= hoursDifference * 1000 * 60 * 60;

      var minutesDifference = Math.floor(difference / 1000 / 60);
      difference -= minutesDifference * 1000 * 60;

      var secondsDifference = Math.floor(difference / 1000);

      var minutesDifferencef = minutesDifference + 32;
      var date = new Date(ele1.T);
      var datef =
        hoursDifference + ":" + minutesDifferencef + ":" + secondsDifference;
      priseclose = parseFloat(ele1.r);
      pricechangepercentage = parseFloat(ele1.P);
      priseopenf = (100 * priseclose.toFixed(6)).toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#MATICUSDT-hf").text(ele1.s);
      $("#MATICUSDT-tf").text(datef);
      $("#MATICUSDT-tpf").text(priseopenf + "%");
    }
    if (ele1.s === "CELRUSDT") {
      var date1 = new Date(ele1.T);
      var date2 = Date.now();
      var difference = date1.getTime() - date2;

      var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
      difference -= daysDifference * 1000 * 60 * 60 * 24;

      var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
      difference -= hoursDifference * 1000 * 60 * 60;

      var minutesDifference = Math.floor(difference / 1000 / 60);
      difference -= minutesDifference * 1000 * 60;

      var secondsDifference = Math.floor(difference / 1000);

      var minutesDifferencef = minutesDifference + 32;
      var date = new Date(ele1.T);
      var datef =
        hoursDifference + ":" + minutesDifferencef + ":" + secondsDifference;
      priseclose = parseFloat(ele1.r);
      pricechangepercentage = parseFloat(ele1.P);
      priseopenf = (100 * priseclose.toFixed(6)).toFixed(6);
      pricechangepercentage = pricechangepercentage.toFixed(2);
      $("#CELRUSDT-hf").text(ele1.s);
      $("#CELRUSDT-tf").text(datef);
      $("#CELRUSDT-tpf").text(priseopenf + "%");
    }
  }
};
//  WAZIRX API
var wazirxSocket1 = new WebSocket("wss://stream.wazirx.com/stream");
wazirxSocket1.onopen = function (e) {
  wazirxSocket1.send('{"event":"subscribe","streams":["!ticker@arr"]}');
};
wazirxSocket1.onmessage = function (event) {
  var message = JSON.parse(event.data);
  result = "data" in message;
  if (result === true) {
    for (let country of Object.keys(message)) {
      var capital = message[country];

      if (capital.length > 10) {
        for (const ele of capital) {
          if (ele.s === "wrxinr") {
            priseclose = parseFloat(ele.c);
            pricechangepercentage = parseFloat(ele.P);
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#WRXINR-h").text(ele.s);
            $("#WRXINR-p").text(pricechangepercentage + "%");
            $("#WRXINR-c").text(priseopenf);
          }
          if (ele.s === "adainr") {
            priseclose = parseFloat(ele.c);
            pricechangepercentage = parseFloat(ele.P);
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#ADAINR-h").text(ele.s);
            $("#ADAINR-p").text(pricechangepercentage + "%");
            $("#ADAINR-c").text(priseopenf);
          }
          if (ele.s === "atominr") {
            priseclose = parseFloat(ele.c);
            pricechangepercentage = parseFloat(ele.P);
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#ATOMINR-h").text(ele.s);
            $("#ATOMINR-p").text(pricechangepercentage + "%");
            $("#ATOMINR-c").text(priseopenf);
          }
          if (ele.s === "batinr") {
            priseclose = parseFloat(ele.c);
            pricechangepercentage = parseFloat(ele.P);
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#BATINR-h").text(ele.s);
            $("#BATINR-p").text(pricechangepercentage + "%");
            $("#BATINR-c").text(priseopenf);
          }
          if (ele.s === "bchinr") {
            priseclose = parseFloat(ele.c);
            pricechangepercentage = parseFloat(ele.P);
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#BCHINR-h").text(ele.s);
            $("#BCHINR-p").text(pricechangepercentage + "%");
            $("#BCHINR-c").text(priseopenf);
          }
          if (ele.s === "bnbinr") {
            priseclose = parseFloat(ele.c);
            pricechangepercentage = parseFloat(ele.P);
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#BNBINR-h").text(ele.s);
            $("#BNBINR-p").text(pricechangepercentage + "%");
            $("#BNBINR-c").text(priseopenf);
          }
          if (ele.s === "btcinr") {
            priseclose = parseFloat(ele.c);
            pricechangepercentage = parseFloat(ele.P);
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#BTCINR-h").text(ele.s);
            $("#BTCINR-p").text(pricechangepercentage + "%");
            $("#BTCINR-c").text(priseopenf);
          }
          if (ele.s === "cakeinr") {
            priseclose = parseFloat(ele.c);
            pricechangepercentage = parseFloat(ele.P);
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#CAKEINR-h").text(ele.s);
            $("#CAKEINR-p").text(pricechangepercentage + "%");
            $("#CAKEINR-c").text(priseopenf);
          }
          if (ele.s === "compinr") {
            priseclose = parseFloat(ele.c);
            pricechangepercentage = parseFloat(ele.P);
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#COMPINR-h").text(ele.s);
            $("#COMPINR-p").text(pricechangepercentage + "%");
            $("#COMPINR-c").text(priseopenf);
          }
          if (ele.s === "crvinr") {
            priseclose = parseFloat(ele.c);
            pricechangepercentage = parseFloat(ele.P);
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#CRVINR-h").text(ele.s);
            $("#CRVINR-p").text(pricechangepercentage + "%");
            $("#CRVINR-c").text(priseopenf);
          }
          if (ele.s === "dashinr") {
            priseclose = parseFloat(ele.c);
            pricechangepercentage = parseFloat(ele.P);
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#DASHINR-h").text(ele.s);
            $("#DASHINR-p").text(pricechangepercentage + "%");
            $("#DASHINR-c").text(priseopenf);
          }
          if (ele.s === "dotinr") {
            priseclose = parseFloat(ele.c);
            pricechangepercentage = parseFloat(ele.P);
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#DOTINR-h").text(ele.s);
            $("#DOTINR-p").text(pricechangepercentage + "%");
            $("#DOTINR-c").text(priseopenf);
          }
          if (ele.s === "eosinr") {
            priseclose = parseFloat(ele.c);
            pricechangepercentage = parseFloat(ele.P);
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#EOSINR-h").text(ele.s);
            $("#EOSINR-p").text(pricechangepercentage + "%");
            $("#EOSINR-c").text(priseopenf);
          }
          if (ele.s === "ethinr") {
            priseclose = parseFloat(ele.c);
            pricechangepercentage = parseFloat(ele.P);
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#ETHINR-h").text(ele.s);
            $("#ETHINR-p").text(pricechangepercentage + "%");
            $("#ETHINR-c").text(priseopenf);
          }
          if (ele.s === "etcinr") {
            priseclose = parseFloat(ele.c);
            pricechangepercentage = parseFloat(ele.P);
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#ETCINR-h").text(ele.s);
            $("#ETCINR-p").text(pricechangepercentage + "%");
            $("#ETCINR-c").text(priseopenf);
          }
          if (ele.s === "filinr") {
            priseclose = parseFloat(ele.c);
            pricechangepercentage = parseFloat(ele.P);
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#FILINR-h").text(ele.s);
            $("#FILINR-p").text(pricechangepercentage + "%");
            $("#FILINR-c").text(priseopenf);
          }
          if (ele.s === "fttinr") {
            priseclose = parseFloat(ele.c);
            pricechangepercentage = parseFloat(ele.P);
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#FTTINR-h").text(ele.s);
            $("#FTTINR-p").text(pricechangepercentage + "%");
            $("#FTTINR-c").text(priseopenf);
          }
          if (ele.s === "iostinr") {
            priseclose = parseFloat(ele.c);
            pricechangepercentage = parseFloat(ele.P);
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#IOSTINR-h").text(ele.s);
            $("#IOSTINR-p").text(pricechangepercentage + "%");
            $("#IOSTINR-c").text(priseopenf);
          }
          if (ele.s === "linkinr") {
            priseclose = parseFloat(ele.c);
            pricechangepercentage = parseFloat(ele.P);
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#LINKINR-h").text(ele.s);
            $("#LINKINR-p").text(pricechangepercentage + "%");
            $("#LINKINR-c").text(priseopenf);
          }
          if (ele.s === "ltcinr") {
            priseclose = parseFloat(ele.c);
            pricechangepercentage = parseFloat(ele.P);
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#LTCINR-h").text(ele.s);
            $("#LTCINR-p").text(pricechangepercentage + "%");
            $("#LTCINR-c").text(priseopenf);
          }
          if (ele.s === "manainr") {
            priseclose = parseFloat(ele.c);
            pricechangepercentage = parseFloat(ele.P);
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#MANAINR-h").text(ele.s);
            $("#MANAINR-p").text(pricechangepercentage + "%");
            $("#MANAINR-c").text(priseopenf);
          }
          if (ele.s === "mdxinr") {
            priseclose = parseFloat(ele.c);
            pricechangepercentage = parseFloat(ele.P);
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#MDXINR-h").text(ele.s);
            $("#MDXINR-p").text(pricechangepercentage + "%");
            $("#MDXINR-c").text(priseopenf);
          }
          if (ele.s === "omginr") {
            priseclose = parseFloat(ele.c);
            pricechangepercentage = parseFloat(ele.P);
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#OMGINR-h").text(ele.s);
            $("#OMGINR-p").text(pricechangepercentage + "%");
            $("#OMGINR-c").text(priseopenf);
          }
          if (ele.s === "sushiinr") {
            priseclose = parseFloat(ele.c);
            pricechangepercentage = parseFloat(ele.P);
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#SUSHIINR-h").text(ele.s);
            $("#SUSHIINR-p").text(pricechangepercentage + "%");
            $("#SUSHIINR-c").text(priseopenf);
          }
          if (ele.s === "trxinr") {
            priseclose = parseFloat(ele.c);
            pricechangepercentage = parseFloat(ele.P);
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#TRXINR-h").text(ele.s);
            $("#TRXINR-p").text(pricechangepercentage + "%");
            $("#TRXINR-c").text(priseopenf);
          }
          if (ele.s === "uniinr") {
            priseclose = parseFloat(ele.c);
            pricechangepercentage = parseFloat(ele.P);
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#UNIINR-h").text(ele.s);
            $("#UNIINR-p").text(pricechangepercentage + "%");
            $("#UNIINR-c").text(priseopenf);
          }
          if (ele.s === "xrpinr") {
            priseclose = parseFloat(ele.c);
            pricechangepercentage = parseFloat(ele.P);
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#XRPINR-h").text(ele.s);
            $("#XRPINR-p").text(pricechangepercentage + "%");
            $("#XRPINR-c").text(priseopenf);
          }
          if (ele.s === "shibinr") {
            priseclose = parseFloat(ele.c);
            pricechangepercentage = parseFloat(ele.P);
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#SHIBINR-h").text(ele.s);
            $("#SHIBINR-p").text(pricechangepercentage + "%");
            $("#SHIBINR-c").text(priseopenf);
          }
          if (ele.s === "maticinr") {
            priseclose = parseFloat(ele.c);
            pricechangepercentage = parseFloat(ele.P);
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#MATICINR-h").text(ele.s);
            $("#MATICINR-p").text(pricechangepercentage + "%");
            $("#MATICINR-c").text(priseopenf);
          }
          if (ele.s === "celrinr") {
            priseclose = parseFloat(ele.c);
            pricechangepercentage = parseFloat(ele.P);
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#CELRINR-h").text(ele.s);
            $("#CELRINR-p").text(pricechangepercentage + "%");
            $("#CELRINR-c").text(priseopenf);
          }
        }
      }
    }
  }
};

// WEBSOCKET END FOR BOT TRADE

const Tradingbot_Controller = ($scope, $http) => {
  $scope.get_user_info = () => {
    $scope.userLoader = true;
    $http.get("getuserinfo").then((res) => {
      // $scope.user = res.data;
      $scope.coin_name = [
        "AAVEUSDT",
        "ADAUSDT",
        "AKROUSDT",
        "ANTUSDT",
        "ATOMUSDT",
        "BATUSDT",
        "BCHUSDT",
        "BNBUSDT",
        "BTCUSDT",
        "CAKEUSDT",
        "COMPUSDT",
        "COSUSDT",
        "CRVUSDT",
        "DASHUSDT",
        "DOGEUSDT",
        "DOTUSDT",
        "EOSUSDT",
        "ETHUSDT",
        "ETCUSDT",
        "FILUSDT",
        "FTTUSDT",
        "GRTUSDT",
        "IOSTUSDT",
        "IOTAUSDT",
        "JSTUSDT",
        "KAVAUSDT",
        "LINKUSDT",
        "LITUSDT",
        "LTCUSDT",
        "MANAUSDT",
        "MDXUSDT",
        "NEOUSDT",
        "OMGUSDT",
        "SUSHIUSDT",
        "THETAUSDT",
        "TRXUSDT",
        "UNIUSDT",
        "XMRUSDT",
        "XRPUSDT",
        "XTZUSDT",
        "RVNUSDT",
        "SHIBUSDT",
        "MATICUSDT",
        "CELRUSDT",
      ];
      $scope.coin_namef = [
        "AAVEUSDT",
        "ADAUSDT",
        "AKROUSDT",
        "ANTUSDT",
        "ATOMUSDT",
        "BATUSDT",
        "BCHUSDT",
        "BNBUSDT",
        "BTCUSDT",
        "COMPUSDT",
        "CRVUSDT",
        "DASHUSDT",
        "DOGEUSDT",
        "DOTUSDT",
        "EOSUSDT",
        "ETHUSDT",
        "ETCUSDT",
        "FILUSDT",
        "GRTUSDT",
        "IOSTUSDT",
        "IOTAUSDT",
        "KAVAUSDT",
        "LINKUSDT",
        "LITUSDT",
        "LTCUSDT",
        "MANAUSDT",
        "NEOUSDT",
        "OMGUSDT",
        "SUSHIUSDT",
        "THETAUSDT",
        "TRXUSDT",
        "UNIUSDT",
        "XMRUSDT",
        "XRPUSDT",
        "XTZUSDT",
        "RVNUSDT",
        "MATICUSDT",
        "CELRUSDT",
      ];
      $scope.coin_name_wazirx = [
        "WRXINR",
        "ADAINR",
        "ATOMINR",
        "BATINR",
        "BCHINR",
        "BNBINR",
        "BTCINR",
        "CAKEINR",
        "COMPINR",
        "CRVINR",
        "DASHINR",
        "DOTINR",
        "EOSINR",
        "ETHINR",
        "ETCINR",
        "FILINR",
        "FTTINR",
        "IOSTINR",
        "LINKINR",
        "LTCINR",
        "MANAINR",
        "MDXINR",
        "OMGINR",
        "SUSHIINR",
        "TRXINR",
        "UNIINR",
        "XRPINR",
        "SHIBINR",
        "MATICINR",
        "CELRINR",
      ];
      $scope.userLoader = false;
    });
  };
  $scope.topp_gainersforbot = () => {
    $http.get(" 'getuserinfo' %}").then((res) => {
      // $scope.gainersforbot = res.data;
    });
  };
  $scope.topp_gainersforbot();
};

const App_Controller = ($scope, $http) => {
  $scope.loader = true;
  $scope.get_notification = () => {
    // $http.get(BASE_URL + 'account/get_notification').then((res) => {
    //     $scope.notifications = res.data.notifications;
    //     $scope.pending_noti = res.data.total;
    //     $scope.loader = false;
    // })
  };

  $scope.get_notification();

  $scope.get_auth_status = () => {
    // $http.get(BASE_URL + 'trade/account/get_auth_status').then((res) => {
    //     $scope.auths = res.data;
    //     if ($scope.auths.google_auth_status == 'true') {
    //         $scope.authsuccess = true;
    //         $scope.google_auth_status = true;
    //     } else {
    //         $scope.authsuccess = false;
    //         $scope.google_auth_status = false;
    //     }
    // })
  };

  $scope.show_withdrawal = (type) => {
    $scope.withdraw_type = type;
    $scope.addressLoader = true;
    // $http.get(BASE_URL + 'trade/account/get_address/').then((res) => {
    //     $scope.type = type;
    //     $scope.fee = 25;
    //     $scope.minamount = 50
    //     $scope.withdraw_in = 'usdt';
    //     if (type == 'BTC') {
    //         $scope.address = res.data.btc_address;
    //     } else if (type == 'BNB') {
    //         $scope.address = res.data.ltc_address;
    //         $scope.fee = 5;
    //     } else if (type == 'ETH') {
    //         $scope.address = res.data.eth_address;
    //     } else if (type == 'USDTTRC20') {
    //         $scope.address = res.data.trx_address;
    //         $scope.fee = 0;
    //     } else if (type == 'USDTERC20') {
    //         $scope.address = res.data.eth_address;
    //         $scope.fee = 20;
    //     } else {
    //         $scope.fee = 0;
    //         // $scope.minamount = 50;
    //         $scope.withdraw_in = 'efx';
    //         $scope.address = res.data.ltc_address;
    //     }
    //     $scope.addressLoader = false;
    // });
    $("#modal_withdrawal").modal("show");
  };

  $scope.show_transfer = (type) => {
    $scope.transfer_type = type;
    if (type == "EFX") {
      $scope.mintransfer = 20;
    } else {
      $scope.mintransfer = 50;
    }
    $("#model_transfer").modal("show");
  };

  $scope.efx_withdrawal = (type) => {
    $scope.fee = type == "efx" ? 50 : 25;
  };

  $scope.show_deposit = (type) => {
    $scope.hide_modal();
    $scope.type = type;
    $scope.minDeposit = 20;
    $scope.payment_type = type;
    if (type == "USDTTRC20") {
      $scope.minDeposit = 15;
    } else if (type == "USDTERC20") {
      $scope.minDeposit = 30;
    }
    $("#modal_deposit").modal("show");
  };

  $scope.hide_modal = () => {
    $scope.amountf = "";
    $scope.address = "";
    $("#amount").value = "";
    $("#qrcode").text("");
    $("#address").val("");
    $("#data_form").removeClass("d-none");
    $("#payment_details").removeClass("d-block").addClass("d-none");
    // $('#payment_details');
    $("#modal_deposit").modal("hide");
  };

  $scope.exportData = (form) => {
    if (!form.$invalid) {
      $scope.csvLoader = true;
      var dataObj = $.param({
        startdate: $scope.startdate,
        enddate: $scope.enddate,
      });
      $http
        .post(BASE_URL + "admin/trading/export_bids", dataObj, config)
        .then(function (response) {
          d = new Date();
          var date = d.getDate() + "-" + d.getMonth() + "-" + d.getFullYear();
          var time = d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
          var file = new Blob([response.data], {
            type: "application/csv",
          });
          var fileURL = URL.createObjectURL(file);
          var a = document.createElement("a");
          a.href = fileURL;
          a.target = "_blank";
          a.download = "Bids-" + date + "_" + time + "_earnfinex.csv";
          document.body.appendChild(a);
          a.click();
          document.body.removeChild(a);
          $scope.csvLoader = false;
        });
    }
  };

  $scope.export_user = (form) => {
    if (!form.$invalid) {
      $scope.csvLoader = true;
      var dataObj = $.param({
        startdate: $scope.startdate,
        enddate: $scope.enddate,
      });
      $("#history").html(
        '<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>'
      );
      $("#history").attr("disable", true);
      // $http.post(BASE_URL + 'admin/user/export_user', dataObj, config).then(
      //     function (response) {
      //         d = new Date();
      //         var date = d.getDate() + '-' + d.getMonth() + '-' + d.getFullYear();
      //         var time = d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
      //         var file = new Blob([response.data], {
      //             type: 'application/csv'
      //         });
      //         var fileURL = URL.createObjectURL(file);
      //         var a = document.createElement('a');
      //         a.href = fileURL;
      //         a.target = '_blank';
      //         a.download = "Users-" + date + '_' + time + '_earnfinex.csv';
      //         document.body.appendChild(a);
      //         a.click();
      //         document.body.removeChild(a);
      //         $scope.csvLoader = false;
      //         $("#history").html('<i class="fa fa-search" aria-hidden="true"></i>');
      //         $("#history").attr('disable', false);
      //     });
    }
  };

  // $scope.redirect_url = (url) => {
  //     window.location.href = BASE_URL + url;
  // }

  // $scope.mark_read = (id) => {
  //     $http.get(BASE_URL + 'read-notification/' + id).then(res => {
  //         window.location.href = res.data.target;
  //     });
  // }

  // $scope.mark_all_read = () => {
  //     $http.get(BASE_URL + 'trade-dashboard/mark_all_read').then(res => {
  //         $scope.get_notification();
  //     });
  // }

  // $scope.close_ticket = (ticket_id) => {

  //     Swal.fire({
  //         title: "Are you sure?",
  //         text: "",
  //         type: "warning",
  //         showCancelButton: !0,
  //         confirmButtonColor: "#34c38f",
  //         cancelButtonColor: "#f46a6a",
  //         confirmButtonText: "Yes",
  //         confirmButtonClass: "btn btn-success mt-2",
  //         cancelButtonClass: "btn btn-danger ml-2 mt-2",
  //         buttonsStyling: !1,
  //     }).then(function(t) {
  //         if (t.value == true) {
  //             var data = $.param({
  //                 ticket_id: ticket_id
  //             });
  //             $http.post(BASE_URL + 'ticket/close_ticket', data, config).then((res) => {
  //                 if (res.data.success) {
  //                     location.reload();
  //                 } else {
  //                     toastr.error(res.data.message, '', toast);
  //                 }
  //             })
  //         } else {

  //         }
  //     });
  // }
};

const coin_status = ($scope, $http,djangoUrl) => {
  $scope.submit_form_futuretred = ($event, ordertype) => {
    console.log(ordertype);
    $event.preventDefault();
    ne = document.getElementById("coinnamefuture").textContent;
    symbolname = ne.trim();
    leverage = document.getElementById("leverage").value;
    type = $(".message_pri:checked").val();
    buyprice = document.getElementById(
      symbolname + "-futuretradelastpoint"
    ).value;
    size = $scope.size;
    buypoint = $scope.buypoint;
    sellpoint = $scope.sellpoint;

    csrfmiddlewaretoken = $("input[name=csrfmiddlewaretoken]").val();
    var adddata = djangoUrl.reverse("adddata");
    if (!symbolname) {
      toastr.error("Something want wrong");
    } else if (!leverage) {
      toastr.error("Please set leverage");
    } else if (!type) {
      toastr.error("Please set Cross/isolate");
    } else if (!buyprice) {
      toastr.error("Something want wrong");
    } else if (!size) {
      toastr.error("Please set size");
    } else if (!buypoint) {
      toastr.error("Please set buy price");
    } else if (!sellpoint) {
      toastr.error("Please set sell price");
    } else if (!ordertype) {
      toastr.error("Please set order type");
    } else if (!csrfmiddlewaretoken) {
      toastr.error("Something want wrong");
    } else {
      var data = $.param({
        levrageperce: leverage,
        type: type,
        buyprice: buyprice,
        symbol: symbolname,
        size: size,
        buypoint: buypoint,
        sellpoint: sellpoint,
        side: ordertype,
        csrfmiddlewaretoken: csrfmiddlewaretoken,
      });
    }
    $http.post(adddata, data, config).then(function (res) {
      if (res.data.succ == "true") {
        addbid.reset();
        toastr.success(res.data.msg);
      } else {
        toastr.error("Error");
        addbid.reset();
      }
    });
  };
};
EFX.controller("App_Controller", App_Controller);
EFX.controller("Tradingbot_Controller", Tradingbot_Controller);
EFX.controller("coin_status", coin_status);

EFX.filter("pagination", function () {
  "use strict";

  return function (input, start) {
    if (!input || !input.length) {
      return;
    }
    start = +start; //parse to int
    return input.slice(start);
  };
});
