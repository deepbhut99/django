// Create new wheel object specifying the parameters at creation time.
let theWheel = new Winwheel({
    'outerRadius': 212, // Set outer radius so wheel fits inside the background.
    'innerRadius': 75, // Make wheel hollow so segments don't go all way to center.
    'textFontSize': 24, // Set default font size for the segments.
    'textOrientation': 'curved', // Make text vertial so goes down from the outside of wheel.
    'textAlignment': 'outer', // Align text to outside of wheel.
    'numSegments': 18, // Specify number of segments.
    'responsive': true,
    // This wheel is responsive!
    'segments': // Define segments including colour and text.
        [ // font size and test colour overridden on backrupt segments.
        {
            'fillStyle': '#ee1c24',
            'text': '1'
        }, {
            'fillStyle': '#3cb878',
            'text': '6'
        }, {
            'fillStyle': '#f6989d',
            'text': '5'
        }, {
            'fillStyle': '#00aef0',
            'text': '15'
        }, {
            'fillStyle': '#f26522',
            'text': '10'
        }, {
            'fillStyle': '#000000',
            'text': '0',
            'textFontSize': 16,
            'textFillStyle': '#ffffff'
        }, {
            'fillStyle': '#e70697',
            'text': '9'
        }, {
            'fillStyle': '#fff200',
            'text': '30'
        }, {
            'fillStyle': '#f6989d',
            'text': '4'
        }, {
            'fillStyle': '#ee1c24',
            'text': '7'
        }, {
            'fillStyle': '#3cb878',
            'text': '22'
        }, {
            'fillStyle': '#f26522',
            'text': '11'
        }, {
            'fillStyle': '#a186be',
            'text': '8'
        }, {
            'fillStyle': '#fff200',
            'text': '3'
        }, {
            'fillStyle': '#000000',
            'text': '0',
            'textFontSize': 16,
            'textFillStyle': '#ffffff'
        }, {
            'fillStyle': '#ee1c24',
            'text': '12'
        }, {
            'fillStyle': '#f6989d',
            'text': '17'
        }, {
            'fillStyle': '#f26522',
            'text': '13'
        }
    ],
    'animation': // Specify the animation to use.
    {
        'type': 'spinToStop',
        'duration': 10, // Duration in seconds.
        'spins': 3, // Default number of complete spins.
        'callbackFinished': alertPrize,
        'callbackSound': playSound, // Function to call when the tick sound is to be triggered.
        'soundTrigger': 'pin' // Specify pins are to trigger the sound, the other option is 'segment'.
    },
    'pins': // Turn pins on.
    {
        'number': 18,
        'fillStyle': 'white',
        'outerRadius': 4,
        'responsive': true, // This must be set to true if pin size is to be responsive.
    }
});

// Loads the tick audio sound in to an audio object.
// let audio = new Audio(BASE_URL + 'assets/user_panel/libs/wheel/tick.mp3');

// This function is called when the sound is to be played.
function playSound() {

    $("#wheel-sound")[0].play()

    // Stop and rewind the sound if it already happens to be playing.
    // audio.pause();
    // audio.currentTime = 0;

    // Play the sound.
    // audio.play();
}

// Vars used by the code in this page to do power controls.
let wheelPower = 0;
let wheelSpinning = false;

// -------------------------------------------------------
// Function to handle the onClick on the power buttons.
// -------------------------------------------------------
function powerSelected() {
    powerLevel = 1;
    // Ensure that power can't be changed while wheel is spinning.
    if (wheelSpinning == false) {
        // Reset all to grey incase this is not the first time the user has selected the power.
        document.getElementById('pw1').className = "";
        document.getElementById('pw2').className = "";
        document.getElementById('pw3').className = "";

        // Now light up all cells below-and-including the one selected by changing the class.
        if (powerLevel >= 1) {
            document.getElementById('pw1').className = "pw1";
        }

        if (powerLevel >= 2) {
            document.getElementById('pw2').className = "pw2";
        }

        if (powerLevel >= 3) {
            document.getElementById('pw3').className = "pw3";
        }

        // Set wheelPower var used when spin button is clicked.
        wheelPower = powerLevel;

        // Light up the spin button by changing it's source image and adding a clickable class to it.
        // document.getElementById('spin_button').src = BASE_URL + "assets/user_panel/libs/wheel/spin_on.png";
        document.getElementById('spin_button').className = "clickable";
    }
}

// -------------------------------------------------------
// Click handler for spin button.
// -------------------------------------------------------
$('#spin_button').on("click", function() {

    // Ensure that spinning can't be clicked again while already running.
    if (wheelSpinning == false) {
        // Based on the power level selected adjust the number of spins for the wheel, the more times is has
        // to rotate with the duration of the animation the quicker the wheel spins.
        if (wheelPower == 1) {
            theWheel.animation.spins = 3;
        } else if (wheelPower == 2) {
            theWheel.animation.spins = 6;
        } else if (wheelPower == 3) {
            theWheel.animation.spins = 10;
        }

        // Disable the spin button so can't click again while wheel is spinning.
        $(".spin_button").prop("disabled", "disabled"),
            // document.getElementById('spin_button').src = BASE_URL + "assets/user_panel/libs/wheel/spin_off.png";
            // document.getElementById('spin_button').className = "";

            // Begin the spin animation by calling startAnimation on the wheel object.
            theWheel.startAnimation();

        // Set to true so that power can't be changed and spin button re-enabled during
        // the current animation. The user will have to reset before spinning again.
        wheelSpinning = true;
    }


});

// -------------------------------------------------------
// Function for reset button.
// -------------------------------------------------------
function resetWheel() {
    theWheel.stopAnimation(false); // Stop the animation, false as param so does not call callback function.
    theWheel.rotationAngle = 0; // Re-set the wheel angle to 0 degrees.
    theWheel.draw(); // Call draw to render changes to the wheel.

    document.getElementById('pw1').className = ""; // Remove all colours from the power level indicators.
    document.getElementById('pw2').className = "";
    document.getElementById('pw3').className = "";

    wheelSpinning = false; // Reset to false to power buttons and spin can be clicked again.
}

// -------------------------------------------------------
// Called when the spin animation has finished by the callback feature of the wheel because I specified callback in the parameters.
// -------------------------------------------------------
function alertPrize(indicatedSegment) {
    // Just alert to the user what happened.
    // In a real project probably want to do something more interesting than this with the result.

    if (indicatedSegment.text == '0') {
        toastr.success("Better luck next time !", '', toast);
    } else {
        var audio = new Audio(BASE_URL + 'assets/user_panel/libs/wheel/cheering.mp3');
        // Stop and rewind the sound if it already happens to be playing.
        // audio.pause();
        // audio.currentTime = 0;

        // Play the sound.
        audio.play();
        console.log("You have won " + indicatedSegment.text)
        setTimeout(function() {
            $('#winner-modal').modal("hide")
        }, 1000);
        var wallet = parseFloat($('#live-amount').text());
        let prize = parseFloat(indicatedSegment.text);
        let calc = wallet + prize;
        $('.account-amount').html(parseFloat(calc));
        $('#live-amount').html(parseFloat(calc));
        sessionStorage.setItem('live-amount', calc);
        toastr.success("You have won " + indicatedSegment.text + " EFX", '', toast);
    }

    $.ajax({
        url: BASE_URL + 'trading/lucky_winner',
        type: 'POST',
        data: { prize: indicatedSegment.text },
        dataType: "json",
        success: function(res) {
            if (res.success) {
                console.log('success')
            } else {
                console.log('error');
            }
        }
    });
}