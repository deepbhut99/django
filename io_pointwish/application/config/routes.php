<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';

/*******Frontend routing Start*******/
$route['faq'] = 'home/faq';
$route['news'] = 'home/news';
$route['about_us'] = 'home/about_us';
$route['404'] = 'home/error';

$route['signup'] = 'login/signup';
$route['login/forgot-password'] = 'login/forgot_password';
$route['reset_password'] = 'login/reset_password';
$route['logout'] = 'login/logout';
$route['verification'] = 'login/verification';
$route['login-auth'] = 'login/login_auth';
$route['signup-auth'] = 'login/signup_verification';
$route['maintenance'] = 'login/maintenance';
$route['privacy-policy'] = 'page/privacy_policy';
$route['terms-condition'] = 'page/terms_condition';
$route['event'] = 'page/event';
$route['home/trading-account'] = 'page/tradingaccount';



/*******Frontend routing End*********/


/*******Backend routing Start********/
/***Userside ***/
// $route['trading'] = 'trade/trading';
// $route['bot-trading'] = 'trade/trading/trade_test';
// $route['trading/(:any)'] = 'trade/trading/$1';
// $route['trade-dashboard'] = 'trade/dashboard';
// $route['trade-dashboard/(:any)'] = 'trade/dashboard/$1';
// $route['read-notification/(:any)'] = 'trade/dashboard/read_notification/$1';
// $route['copytrade'] = 'trade/trading/copy_trade';
// $route['trading-winner'] = 'trade/trading/winner';
// $route['wallet'] = 'trade/account/wallet';
// $route['account'] = 'trade/account';
$route['affiliate'] = 'trade/account/affiliate';
// $route['agency'] = 'trade/agency';
$route['history'] = 'trade/account/history';
// $route['ticket'] = 'trade/ticket';
// $route['ticket/(:any)'] = 'trade/ticket/$1';
// $route['exchange'] = 'trade/account/exchange';
// $route['change-lang/(:any)'] = 'trade/dashboard/change_language/$1';
// $route['account/(:any)'] = 'trade/account/$1';
$route['reupload/(:any)'] = 'trade/account/reupload/$1';
$route['kyc'] = 'trade/account/kyc_verification';
$route['crypto-address'] = 'trade/account/crypto_address';
$route['change-password'] = 'trade/account/change_pwd';
$route['2fa'] = 'trade/account/twoFA';
$route['apikey'] = 'trade/account/profileapi';
$route['community'] = 'trade/trading/community';
$route['portfolio'] = 'trade/trading/portfolio';
$route['expert-area'] = 'trade/trading/expert_area';
$route['deposite'] = 'trade/account/deposit_wallet';
$route['withdraw'] = 'trade/account/withdraw_wallet';
$route['wallet-transaction'] = 'trade/account/wallet_transaction';
$route['my-status'] = 'trade/trading/my_status';
$route['binance'] = 'trade/trading/binance';
$route['wazirx'] = 'trade/trading/wazirx';
$route['binance-future'] = 'trade/trading/binfuture';
$route['bot-history'] = 'trade/trading/bothistory';




$route['d46aa6f5f9401257b243ce2dfe442b9c'] = 'admin/login';
$route['admin/logout'] = 'admin/login/logout';
$route['dashboard'] = 'admin/dashboard';
$route['total-users'] = 'admin/user/total_users';
$route['paid-users'] = 'admin/user/paid_users';
$route['unpaid-users'] = 'admin/user/unpaid_users';
$route['trade-bids'] = 'admin/trading';
$route['copy-trade'] = 'admin/copytrade';
$route['trade-algorithm'] = 'admin/algorithm';
$route['update-api'] = 'admin/algorithm/updateAPI';
$route['trade-profit'] = 'admin/algorithm/profit';
$route['pending-documents'] = 'admin/document/pending_documents';
$route['approved-documents'] = 'admin/document/approved_documents';
$route['rejected-documents'] = 'admin/document/rejected_documents';
$route['transfer-fund'] = 'admin/fund/transfer_fund';
$route['fund-history'] = 'admin/fund/transfer_fund_history';
$route['pending-withdrawal'] = 'admin/withdraw/pending_withdrawal';
$route['given-withdrawal'] = 'admin/withdraw/given_withdrawal';
$route['rejected-withdrawal'] = 'admin/withdraw/rejected_withdrawal';
$route['trade-winner-list'] = 'admin/trading/winner';
$route['company-business/(:any)'] = 'admin/dashboard/company_business/$1';
$route['pwt-users'] = 'admin/user/get_pwt_users';
$route['pwt-users/(:any)'] = 'admin/user/get_pwt_users/$1';
$route['trade-agency'] = 'admin/trading/agency';

$route['coin-status/(:any)'] = 'trade/bottradebid/coin_status/$1';
$route['trade-setting/(:any)'] = 'trade/bottradebid/trade_setting/$1';
// $route['margin-configuration/(:any)'] = 'trade/bottradebid/margin_configuration/$1';
// $route['profit'] = 'trade/bottradebid/profit';
// $route['saved-strategy'] = 'trade/bottradebid/saved_strategy';
// $route['direct-referral'] = 'trade/bottradebid/direct_history';
$route['team-income'] = 'trade/bottradebid/team_income';
// $route['team-trading'] = 'trade/bottradebid/team_trading';
$route['team-reward'] = 'trade/bottradebid/team_trading';
// $route['personal-profit'] = 'trade/bottradebid/personal_profit';
$route['bot-profit'] = 'trade/bottradebid/personal_profit';





// $route['admin/user_management'] = 'admin/dashboard/user_management';
// $route['admin/profile'] = 'admin/dashboard/profile';
// $route['admin/history'] = 'admin/dashboard/history';
// $route['admin/user_history/(:num)'] = 'admin/dashboard/user_history/$i';
// $route['admin/referral_setting'] = 'admin/dashboard/referral_setting';
// $route['admin/user_wallet/(:num)'] = 'admin/dashboard/wallet/$i';
/*******Backend routing End*******/
