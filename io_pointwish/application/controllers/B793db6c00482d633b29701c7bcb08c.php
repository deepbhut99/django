<?php

defined('BASEPATH') or exit('No direct script access allowed');

class B793db6c00482d633b29701c7bcb08c extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        date_default_timezone_set('UTC');
        $this->load->model('Agency_model', 'agency');
        $this->load->model('Common_model', 'common');
    }

    // Once a day
    function remove_payment()
    {
        $results = $this->common->getWhereFromLast('pwt_user_notification', array('status' => '0', 'type' => 'fund', 'is_read' => 0));
        $ids = array();
        foreach ($results as $row) {
            if ($row['end_date'] <= date('Y-m-d')) {
                array_push($ids, $row['id']);
            }
        }
        $ids = implode(',', $ids);
        if ($ids) {
            $query = "UPDATE pwt_user_notification SET is_read = '1' WHERE FIND_IN_SET(id, '$ids')";
            $this->db->query($query);
        }
        // $this->db->where_in('user_id', $userids);
        // $this->db->delete('pw_add_fund_history');               
    }

    // Once in a week trading commission for Agency/
    function trading_commission()
    {
        $users = $this->db->get('pwt_agency_tree')->result_array();
        $header_note = array();
        foreach ($users as $row) {
            $income = 0;
            // level 1 
            $totalTrade = $this->agency->get_count_bids($row['tree'], 1);
            if ($totalTrade > 0) {
                $commission = $this->get_level_income(1)['commission'];
                $earned = $totalTrade * $commission / 100;
                $income += $earned;
                $insert = array(
                    'user_id' => $row['sponsor_id'],
                    'sponsor_user' => $row['sponsor_user'],
                    'level' => 1,
                    'commission_type' => 0,
                    'amount' => $totalTrade,
                    'earned' => $earned,
                    'created_date' => date('Y-m-d')
                );
                $this->common->insert_data('pwt_agency_commission', $insert);
            }

            // Level 2
            if ($row['tree_count'] >= 3) {
                $totalTrade = $this->agency->get_count_bids($row['tree'], 2);
                if ($totalTrade > 2000) {
                    $commission = $this->get_level_income(2)['commission'];
                    $earned = $totalTrade * $commission / 100;
                    $income += $earned;
                    $insert = array(
                        'user_id' => $row['sponsor_id'],
                        'sponsor_user' => $row['sponsor_user'],
                        'level' => 2,
                        'commission_type' => 0,
                        'amount' => $totalTrade,
                        'earned' => $earned,
                        'created_date' => date('Y-m-d')
                    );
                    $this->common->insert_data('pwt_agency_commission', $insert);

                    // Level 3
                    if ($row['tree_count'] >= 4) {
                        // $totalTrade = $this->agency->get_count_bids($row['tree'], 3, $row['sponsor_id']);
                        if ($totalTrade > 4000) {
                            $commission = $this->get_level_income(3)['commission'];
                            $earned = $totalTrade * $commission / 100;
                            $income += $earned;
                            $insert = array(
                                'user_id' => $row['sponsor_id'],
                                'sponsor_user' => $row['sponsor_user'],
                                'level' => 3,
                                'commission_type' => 0,
                                'amount' => $totalTrade,
                                'earned' => $earned,
                                'created_date' => date('Y-m-d')
                            );
                            $this->common->insert_data('pwt_agency_commission', $insert);

                            // Level 4
                            if ($row['tree_count'] >= 5) {
                                // $totalTrade = $this->agency->get_count_bids($row['tree'], 4, $row['sponsor_id']);
                                if ($totalTrade > 8000) {
                                    $commission = $this->get_level_income(4)['commission'];
                                    $earned = $totalTrade * $commission / 100;
                                    $income += $earned;
                                    $insert = array(
                                        'user_id' => $row['sponsor_id'],
                                        'sponsor_user' => $row['sponsor_user'],
                                        'level' => 4,
                                        'commission_type' => 0,
                                        'amount' => $totalTrade,
                                        'earned' => $earned,
                                        'created_date' => date('Y-m-d')
                                    );
                                    $this->common->insert_data('pwt_agency_commission', $insert);

                                    // Level 5
                                    if ($row['tree_count'] >= 6) {
                                        // $totalTrade = $this->agency->get_count_bids($row['tree'], 5, $row['sponsor_id']);
                                        if ($totalTrade > 16000) {
                                            $commission = $this->get_level_income(5)['commission'];
                                            $earned = $totalTrade * $commission / 100;
                                            $income += $earned;
                                            $insert = array(
                                                'user_id' => $row['sponsor_id'],
                                                'sponsor_user' => $row['sponsor_user'],
                                                'level' => 5,
                                                'commission_type' => 0,
                                                'amount' => $totalTrade,
                                                'earned' => $earned,
                                                'created_date' => date('Y-m-d')
                                            );
                                            $this->common->insert_data('pwt_agency_commission', $insert);

                                            // Level 6
                                            if ($row['tree_count'] >= 7) {
                                                // $totalTrade = $this->agency->get_count_bids($row['tree'], 6, $row['sponsor_id']);
                                                if ($totalTrade > 32000) {
                                                    $commission = $this->get_level_income(6)['commission'];
                                                    $earned = $totalTrade * $commission / 100;
                                                    $income += $earned;
                                                    $insert = array(
                                                        'user_id' => $row['sponsor_id'],
                                                        'sponsor_user' => $row['sponsor_user'],
                                                        'level' => 6,
                                                        'commission_type' => 0,
                                                        'amount' => $totalTrade,
                                                        'earned' => $earned,
                                                        'created_date' => date('Y-m-d')
                                                    );
                                                    $this->common->insert_data('pwt_agency_commission', $insert);

                                                    // Level 7
                                                    if ($row['tree_count'] >= 8) {
                                                        // $totalTrade = $this->agency->get_count_bids($row['tree'], 7, $row['sponsor_id']);
                                                        if ($totalTrade > 64000) {
                                                            $commission = $this->get_level_income(7)['commission'];
                                                            $earned = $totalTrade * $commission / 100;
                                                            $income += $earned;
                                                            $insert = array(
                                                                'user_id' => $row['sponsor_id'],
                                                                'sponsor_user' => $row['sponsor_user'],
                                                                'level' => 7,
                                                                'commission_type' => 0,
                                                                'amount' => $totalTrade,
                                                                'earned' => $earned,
                                                                'created_date' => date('Y-m-d')
                                                            );
                                                            $this->common->insert_data('pwt_agency_commission', $insert);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            /*******Update Wallet**Start*****/
            $u_query = "update pwt_users SET wallet_amount = wallet_amount +" . $income . " where id = '" . $row['sponsor_id'] . "'";
            $this->db->query($u_query);
            /*******Update Wallet**End*****/

            /*******Notification **Start****/
            $header_note[] = array(
                'user_id' => $row['sponsor_id'],
                'type' => 'agency',
                'target' => base_url('agency'),
                'detail' => "Trading commission $ $income is received",
                'date' => date('Y-m-d H:i:s'),
            );
            // $this->common->insert_data('pwt_notification', $insert_note);
            /*******Notification **Start****/
        }
        if ($header_note) {
            $this->db->insert_batch('pwt_notification', $header_note);
        }
        // $this->weekly_winner();
    }

    function get_level_income($level)
    {
        $income = 0;
        $commission = 0;
        switch ($level) {
            case 1:
                $income = 50;
                $commission = 1;
                break;
            case 2:
                $income = 25;
                $commission = 0.5;
                break;
            case 3:
                $income = 12.5;
                $commission = 0.25;
                break;
            case 4:
                $income = 6.25;
                $commission = 0.13;
                break;
            case 5:
                $income = 3.12;
                $commission = 0.06;
                break;
            case 6:
                $income = 1.56;
                $commission = 0.03;
                break;
            case 7:
                $income = 0.78;
                $commission = 0.02;
                break;
        }
        $data['income'] = $income;
        $data['commission'] = $commission;
        return $data;
    }

    // Each an 10 minute
    function sucess_payment()
    {
        $fund_data = $this->common->getWhereFromLast('pwt_user_notification', array('type' => 'fund', 'status' => 4));
        foreach ($fund_data as $fund) {
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://api.nowpayments.io/v1/payment/' . $fund['txn_id'],
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_HTTPHEADER => array(
                    'x-api-key: RHYY119-F5K44CG-M3TTKE0-0TM3AFE'
                ),
            ));

            $response = curl_exec($curl);
            $encode = json_encode($response);
            $payment = json_decode($response);
            $amount = ($payment->outcome_amount / get_pwt()['current_price']);
            if ($payment->payment_status == 'partially_paid' || $payment->payment_status == 'finished') {
                $time = date('Y-m-d H:i:s');
                $update_data = array(
                    'status' => 100,
                    'message' => $payment->payment_status,
                    'amount' => $amount,
                    'payment_type' => 'pwt',
                    'txn_id' => $payment->payment_id,
                    'response_data' => json_encode($payment),
                    'modified_datetime' => $time,
                    'balance' => getWalletAmountByUserID($fund['from_user'])[3] + $amount,
                );
                $this->common->update_data('pwt_user_notification', array('id' => $fund['id']), $update_data);
                /*******Update history**End*****/

                /*******Notification **Start****/
                $insert_note = array(
                    'user_id' => $fund['from_user'],
                    'type' => 'deposit',
                    'target' => base_url('wallet'),
                    'detail' => "Deposit $ " . number_format($payment->outcome_amount, 2) . " is confirmed",
                    'date' => date('Y-m-d H:i:s'),
                );
                $this->common->insert_data('pwt_notification', $insert_note);
                /*******Notification **Start****/

                /*******Update Wallet**Start*****/
                $set = ', unlocked_pwt = unlocked_pwt + ' . $amount;
                $u_query = "update pwt_users SET package='1', upgrade_time = NOW()" . $set . " where id = '" . $fund['from_user'] . "'";
                $this->db->query($u_query);
            }
            curl_close($curl);
        }
        $this->confirm_payment();
    }

    function confirm_payment()
    {
        $fund_data = $this->common->getWhereFromLast('pwt_user_notification', array('type' => 'fund', 'status != ' => 100, 'is_read' => 0));
        foreach ($fund_data as $fund) {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://api.nowpayments.io/v1/payment/?limit=20&page=0&sortBy=created_at&orderBy=desc',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_HTTPHEADER => array(
                    'x-api-key: RHYY119-F5K44CG-M3TTKE0-0TM3AFE'
                ),
            ));

            $response = curl_exec($curl);
            $encode = json_encode($response);
            $result = json_decode($response);
            curl_close($curl);

            foreach ($result->data as $res) {

                if ($fund['txn_id'] == $res->invoice_id) {
                    if ($res->payment_status == 'confirmed' || $res->payment_status == 'finished' || $res->payment_status == "partially_paid") {
                        $amount = ($res->outcome_amount / get_pwt()['current_price']);
                        /*******Update history**Start*****/
                        $time = date('Y-m-d H:i:s');
                        if ($res->payment_status == 'partially_paid') {
                            goto next;
                        }
                        $calc = ($res->actually_paid * 100) / $res->pay_amount;
                        if ($calc > 95) {
                            next:
                            $update_data = array(
                                'status' => 100,
                                'message' => $res->payment_status,
                                'amount' => $amount,
                                'payment_type' => 'pwt',
                                'txn_id' => $res->payment_id,
                                'response_data' => json_encode($res),
                                'modified_datetime' => $time,
                                'balance' => getWalletAmountByUserID($fund['from_user'])[3] + $amount,
                            );
                            $this->common->update_data('pwt_user_notification', array('txn_id' => $res->invoice_id), $update_data);
                            /*******Update history**End*****/

                            /*******Notification **Start****/
                            $insert_note = array(
                                'user_id' => $fund['from_user'],
                                'type' => 'deposit',
                                'target' => base_url('wallet'),
                                'detail' => "Deposit $ " . number_format($res->outcome_amount, 2) . " is confirmed",
                                'date' => date('Y-m-d H:i:s'),
                            );
                            $this->common->insert_data('pwt_notification', $insert_note);
                            /*******Notification **Start****/

                            /*******Update Wallet**Start*****/
                            $set = ', unlocked_pwt = unlocked_pwt + ' . $amount;
                            $u_query = "update pwt_users SET package='1', upgrade_time = NOW()" . $set . " where id = '" . $this->session->user_id . "'";
                            $this->db->query($u_query);
                        } else {
                            /*******Update history**Start*****/
                            $update_data = array(
                                'status' => 4,
                                'message' => 'In-Progress',
                                'txn_id' => $res->payment_id,
                                'amount' => $res->pay_amount,
                                'response_data' => json_encode($res),
                                'modified_datetime' => $time,
                            );
                            $this->common->update_data('pwt_user_notification', array('txn_id' => $res->invoice_id), $update_data);
                            /*******Update history**End*****/
                        }
                        /*******Update Wallet**End*****/
                    }
                }
            }
        }
    }

    // Once in a month bot trading commission/
    function bottrading_commission()
    {
        $users = $this->common->getWhere('pwt_bottrade_tree', array('direct_count >' => 0));
        $header_note = array();
        $insert_history = array();
        $insert_commission = array();
        foreach ($users as $user) {
            $tree = $this->get_user_tree($user->sponsor_id);
            if ($tree) {
                $tree = $tree['teamTree'];
                // last month record
                // $query = "SELECT SUM(qty_with_commision) as total_profit FROM pwt_bids_for_buysell WHERE user_id in ($tree) and orderListId = -1 and side = 'SELL' and created_datetime >= unix_timestamp( current_date - interval 1 month )";
                // order_date > now() - interval 1 hour (last hor record)
                // current hour record
                // $query = "SELECT SUM(qty_with_commision) as total_profit FROM pwt_bids_for_buysell WHERE user_id in ($tree) and orderListId = -1 and side = 'SELL' and date(DATE_FORMAT(FROM_UNIXTIME(created_datetime), '%Y-%m-%d %H:%i:%s')) = date(now()) and hour(DATE_FORMAT(FROM_UNIXTIME(created_datetime), '%Y-%m-%d %H:%i:%s')) = hour(now())";
                $query = $query = "SELECT SUM(qty_with_commision) as total_profit FROM pwt_bids_for_buysell WHERE user_id in ($tree) and orderListId = -1 and side = 'SELL' and DATE_FORMAT(FROM_UNIXTIME(created_datetime), '%Y-%m-%d %H:%i:%s') > now() - interval 1 hour";
                $data = $this->db->query($query);
                // var_dump($this->db->last_query());
                // exit(0);
                if ($data->num_rows() > 0) {
                    $row = $data->row();
                    if ($row->total_profit > 0) {
                        $commission = $this->get_trade_commission($user->level);
                        $calc_commission = $row->total_profit * $commission / 100;

                        /*******Notification **Start****/
                        $header_note[] = array(
                            'user_id' => $user->sponsor_id,
                            'type' => 'commission',
                            'target' => '#',
                            'detail' => "Team trading commission $ $calc_commission is received",
                            'date' => date('Y-m-d H:i:s'),
                        );
                        /*******Notification **End****/

                        /*******Commission history **Start****/
                        $insert_commission[] = array(
                            'user_id' => $user->sponsor_id,
                            'profit' => $row->total_profit,
                            'amount' => number_format($calc_commission,6),
                            'created_date' => date('Y-m-d H:i:s'),
                        );
                        /*******Commission history **End****/

                        /*******History **Start****/
                        $insert_history[] = array(
                            'from_user' => $user->sponsor_id,
                            'to_user' => $user->sponsor_id,
                            'log' => 'credit',
                            'type' => 'bottrade',
                            'payment_type' => 'USDT',
                            'amount' => number_format($calc_commission,6),
                            'balance' => getWalletAmountByUserID($user->sponsor_id)[0],
                            'txn_id' => '#TC' . rand(),
                            'message' => "Team trading commission",
                            'status' => 1,
                            'created_datetime' => date('Y-m-d H:i:s'),
                            'modified_datetime' => date('Y-m-d H:i:s'),
                        );
                        /*******History **End****/

                        /*******Update Wallet**Start*****/
                        // $set = ' wallet_amount = wallet_amount + ' . $amount;      
                        $u_query = "update pwt_users SET wallet_amount = wallet_amount + $calc_commission where id = '" . $user->sponsor_id . "'";
                        $this->db->query($u_query);
                        /*******Update Wallet**End*****/
                    }
                }
            }
        }
        if ($header_note) {
            $this->db->insert_batch('pwt_notification', $header_note);
        }
        if ($insert_history) {
            $this->db->insert_batch('pwt_user_notification', $insert_history);
        }
        if ($insert_commission) {
            $this->db->insert_batch('pwt_bottrade_commission', $insert_commission);
        }
    }

    function get_user_tree($userId)
    {
        $this->set_group_concat_max_session();
        $teamTree = "";
        $usertree = $this->common->getSingle('pwt_bottrade_tree', array('sponsor_id' => $userId));
        if ($usertree) {
            $teamTree = $usertree->team_tree;
            $query = "SELECT GROUP_CONCAT(team_tree) as user_tree FROM pwt_bottrade_tree WHERE sponsor_id in ($usertree->team_tree)";
            $data = $this->db->query($query);
            if ($data->num_rows() > 0) {
                $row = $data->row();
                if ($row->user_tree) {
                    $teamTree .= ',' . $row->user_tree;
                    $query = "SELECT GROUP_CONCAT(team_tree) as user_tree FROM pwt_bottrade_tree WHERE sponsor_id in ($row->user_tree)";
                    $data = $this->db->query($query);
                    if ($data->num_rows() > 0) {
                        $row = $data->row();
                        if ($row->user_tree) {
                            $teamTree .= ',' . $row->user_tree;
                            $query = "SELECT GROUP_CONCAT(team_tree) as user_tree FROM pwt_bottrade_tree WHERE sponsor_id in ($row->user_tree)";
                            $data = $this->db->query($query);
                            if ($data->num_rows() > 0) {
                                $row = $data->row();
                                if ($row->user_tree) {
                                    $teamTree .= ',' . $row->user_tree;
                                    $query = "SELECT GROUP_CONCAT(team_tree) as user_tree FROM pwt_bottrade_tree WHERE sponsor_id in ($row->user_tree)";
                                    $data = $this->db->query($query);
                                    if ($data->num_rows() > 0) {
                                        $row = $data->row();
                                        if ($row->user_tree) {
                                            $teamTree .= ',' . $row->user_tree;
                                            $query = "SELECT GROUP_CONCAT(team_tree) as user_tree FROM pwt_bottrade_tree WHERE sponsor_id in ($row->user_tree)";
                                            $data = $this->db->query($query);
                                            if ($data->num_rows() > 0) {
                                                $row = $data->row();
                                                if ($row->user_tree) {
                                                    $teamTree .= ',' . $row->user_tree;
                                                    $query = "SELECT GROUP_CONCAT(team_tree) as user_tree FROM pwt_bottrade_tree WHERE sponsor_id in ($row->user_tree)";
                                                    $data = $this->db->query($query);
                                                    if ($data->num_rows() > 0) {
                                                        $row = $data->row();
                                                        if ($row->user_tree) {
                                                            $teamTree .= ',' . $row->user_tree;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        $datas['teamTree'] = $teamTree;
        return $datas;
    }

    function get_trade_commission($level)
    {
        $commission = 0;
        switch ($level) {
            case 1:
                $commission = 4;
                break;
            case 2:
                $commission = 6;
                break;
            case 3:
                $commission = 8;
                break;
            case 4:
                $commission = 10;
                break;
            case 5:
                $commission = 11;
                break;
            case 6:
                $commission = 12;
                break;
        }
        return $commission;
    }

    private function set_group_concat_max_session()
    {
        $query = "SET SESSION group_concat_max_len = 18446744073709551615; ";
        $this->db->query($query);
    }
}
