<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends MY_Controller
{

	function __construct()
	{
		parent::__construct();
	}

	function index($param = '')
	{

		$device = systemInfo();
		if ($device['source'] == "APP") {
			redirect(base_url('wallet'));
		}
		$data['title'] = 'Trading & Earning Platform';
		$data['type'] = $param;
		if ($param) {
			$this->lang->load('local', $param);
			$this->session->set_userdata('language', $param);
		}
		$this->load->view('dark/home', $data);
	}

	function welcome()
	{
		if ($this->session->user_id) {
			$data['title'] = 'Welcome';
			$this->load->view('welcome', $data);
		} else {
			redirect(base_url());
		}
	}

	function about_Us()
	{
		$data['title'] = 'About Us';
		$this->load->view('about_us', $data);
	}

	function contactus($s1 = '')
	{
		if ($s1) {
			redirect(base_url('page/contactus'));
		} else {
			$data['title'] = 'Contact';
			$this->load->view('contact_us', $data);
		}
	}
	function rules($para1 = '')
	{
		if ($para1 == 'privacy_policy') {
			$this->privacypolicy();
		} else if ($para1 == 'terms') {
			$this->terms();
		} else if ($para1 == 'risk_disclosure') {
			$this->riskdisclosure();
		} else if ($para1 == 'return_refund') {
			$this->rrpolicy();
		} else if ($para1 == 'cookies_policy') {
			$this->cookiespolicy();
		} else if ($para1 == 'AML_KYC') {
			$this->amlpolicy();
		} else if ($para1 == 'demo_trading') {
			$this->tradingaccount();
		} else if ($para1 == 'payment_policy') {
			$this->paymentpolicy();
		}
	}

	function terms($s1 = '')
	{
		if ($s1) {
			redirect(base_url('page/terms'));
		} else {
			$data['title'] = "Terms and Conditions";
			$this->load->view('t&c', $data);
		}
	}

	function forget_pwd()
	{
		$data['title'] = "Forget Password";
		$this->load->view('forget_pwd', $data);
	}

	function privacypolicy($s1 = '')
	{
		if ($s1) {
			redirect(base_url('page/privacypolicy'));
		} else {
			$data['title'] = 'Privacy Policies';
			$this->load->view('privacy_policy', $data);
		}
	}

	function riskdisclosure($s1 = '')
	{
		if ($s1) {
			redirect(base_url('page/riskdisclosure'));
		} else {
			$data['title'] = 'Risk Disclaimer';
			$this->load->view('disclaimer', $data);
		}
	}

	function rrpolicy($s1 = '')
	{
		if ($s1) {
			redirect(base_url('page/rrpolicy'));
		} else {
			$data['title'] = 'Return and Refund Policy';
			$this->load->view('return_refund', $data);
		}
	}

	function cookiespolicy($s1 = '')
	{
		if ($s1) {
			redirect(base_url('page/cookiespolicy'));
		} else {
			$data['title'] = 'Cookies Policies';
			$this->load->view('cookies_policy', $data);
		}
	}

	function amlpolicy($s1 = '')
	{
		if ($s1) {
			redirect(base_url('page/amlpolicy'));
		} else {
			$data['title'] = 'AML and KYC Policy';
			$this->load->view('kyc', $data);
		}
	}

	function tradingaccount($s1 = '')
	{
		if ($s1) {
			redirect(base_url('page/tradingaccount'));
		} else {
			$data['title'] = 'Demo & Trading Account';
			$this->load->view('demo_trading', $data);
		}
	}

	function paymentpolicy($s1 = '')
	{
		if ($s1) {
			redirect(base_url('page/paymentpolicy'));
		} else {
			$data['title'] = 'Payment Policies';
			$this->load->view('payment_policy', $data);
		}
	}

	function blockchain()
	{
		$data['title'] = 'Blockchain';
		$this->load->view('blockchain', $data);
	}

	function lang($lg)
	{
		$this->lang->load('local', $lg);
		$this->session->set_userdata('language', $lg);
		echo json_encode(array('success' => 'true'));
	}

	function error()
	{
		$this->load->view('pages-404');
	}
	function contact_us()
	{
		$this->form_validation->set_rules('name2', 'User name', 'required');
		$this->form_validation->set_rules('email2', 'Email', 'required');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		if ($this->form_validation->run() == FALSE) {
		} else {
			$username = $this->input->post('name2', true);
			$email = $this->input->post('email2', true);
			$msg =  $this->input->post('msg', true);

			if (!empty($username) && !empty($email)) {
				$insert_data = array(
					'username' => $username,
					'email' => $email,
					'msg' => $msg,
					'created_datetime' => date('Y-m-d H:i:s'),
				);
				$this->common->insert_data('pw_contact_us_new', $insert_data);
				$result['success'] = 1;
				echo json_encode($result);
			}
		}
	}
	function contact_us_subscribe()
	{
		$this->form_validation->set_rules('email1', 'Email', 'required');

		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		if ($this->form_validation->run() == FALSE) {
		} else {
			$email = $this->input->post('email1', true);
			if (!empty($email)) {
				$insert_data = array(
					'email' => $email,
					'created_datetime' => date('Y-m-d H:i:s'),
				);
				$this->common->insert_data('pw_subscribe', $insert_data);
				$result['success'] = 1;
				echo json_encode($result);
			}
		}
	}
}
