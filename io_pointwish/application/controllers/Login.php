<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once APPPATH . '/third_party/googleLib/GoogleAuthenticator.php';
class Login extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		// $this->load->helper('captcha');
		// define($mail, $this->phpmailer_lib->load());
	}

	function index()
	{
		$sys = systemInfo();
		if ($this->session->userdata('user_id')) {
			redirect(base_url('wallet'));
			// if ($sys['source'] == "APP" || $sys['os'] == 'iPhone') {
			// } else {
			// 	
			// }
		}
		$data['title'] = 'Sign In';
		$this->form_validation->set_rules('username', 'User name', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('login', $data);
		} else {
			$username = $this->input->post('username', true);
			$password = $this->input->post('password', true);
			$where = array('username' => $username, 'password' => md5($password), 'is_delete' => 1);
			$login_data = $this->common->getSingle('pwt_users', $where);
			if (!empty($login_data)) {

				// if ($sys['device'] == "MOBILE" && $sys['source'] == "WEB" && $sys['os'] == 'iPhone') {
				// 	$this->session->set_flashdata('invalid', true);
				// 	$result['success'] = 4;
				// 	echo json_encode($result);
				// } else {
					$get = $this->getLocationInfoByIp();
					$ip_address = $get['ip'];
					$browser = $this->getBrowser($_SERVER['HTTP_USER_AGENT']);
					$this->common->login_count($login_data->id, $ip_address);
					if ($login_data->is_status == 1) {
						if ($login_data->google_auth_status == 'false') {
							/******Last Login Update*Start***/
							/*******Android Bonus**Start****/
							// $isAndroid = false;
							// if (($sys['os'] == 'Android' || $sys['os'] == 'iPhone') && $login_data->is_android_bonus == 0) {
								// $insert_note = array(
								// 	'from_user' => $login_data->id,
								// 	'to_user' => $login_data->id,
								// 	'log' => 'credit',
								// 	'type' => 'android',
								// 	'payment_type' => 'USDT',
								// 	'amount' => 5,
								// 	'balance' => getWalletAmountByUserID($login_data->id)[3] + 5,
								// 	'txn_id' => '#B' . rand(),
								// 	'message' => "App Bonus",
								// 	'status' => 1,
								// 	'created_datetime' => date('Y-m-d H:i:s'),
								// 	'modified_datetime' => date('Y-m-d H:i:s'),
								// );
								// $this->common->insert_data('pwt_user_notification', $insert_note);

								/*******Notification **Start****/
								// $insert_note = array(
								// 	'user_id' => $login_data->id,
								// 	'type' => 'bonus',
								// 	'target' => base_url('history'),
								// 	'detail' => "App bonus 5 USDT is received",
								// 	'date' => date('Y-m-d H:i:s'),
								// );
								// $this->common->insert_data('pwt_notification', $insert_note);
								/*******Notification **Start****/

								// $isAndroid = true;
							// }
							/*******Android Bonus **End****/
							// if ($login_data->country) {
							// 	$set = $isAndroid ? ", unlocked_pwt = unlocked_pwt + 5, is_android_bonus = 1, login_country = '" . $get['country'] . "'" : ", login_country = '" . $get['country'] . "'";
							// } else {
							// 	$set = $isAndroid ? ", unlocked_pwt = unlocked_pwt + 5, is_android_bonus = 1, country = '" . $get['country'] . "' , login_country = '" . $get['country'] . "'" : ", country = '" . $get['country'] . "' , login_country = '" . $get['country'] . "'";
							// }
							$u_query = "update pwt_users set my_os ='" . $sys['os'] . "', last_login = '" . date('Y-m-d H:i:s') . "' where id = '" . $login_data->id . "'";
							$this->db->query($u_query);
							/******Last Login Update*End***/
							$insert_data = array(
								'user_id' => $login_data->id,
								'message' => 'Login attempt success !',
								'browser_name' => $browser,
								'ip_address' => $ip_address,
								'os' => $sys['os'],
								'device' => $sys['device'],
								'status' => '1',
								'is_active' => 1,
								'created_datetime' => date('Y-m-d H:i:s'),
							);
							$log_id = $this->common->insert_data('pwt_log_history', $insert_data);

							$session_data = array(
								'user_id'   => $login_data->id,
								'user_name'  => $login_data->username,
								'user_email'  => $login_data->email,
								'full_name' => $login_data->full_name,
								'trade_type' => $login_data->trade_type,
								'logsid' => $log_id,
								'language' => "english",
								'lang' => "en"

							);
							$this->session->set_userdata($session_data);
							// $this->session->sess_update();

							/********Mail for activity ***Start****/
							$from_email = ADMIN_NO_REPLY_EMAIL; //ADMIN_INFO_EMAIL
							$to_email = $this->session->user_email;
							$subject = 'Routine Activity';
							$WEB_URL = base_url();
							$htmlContent = file_get_contents("./mail_html/activity.html");
							$tpl2 = str_replace('{{WEB_URL}}', $WEB_URL, $htmlContent);
							$tpl3 = str_replace('{{USERNAME}}', $login_data->username, $tpl2);
							$message_html = str_replace('{{INFO_EMAIL}}', $from_email, $tpl3);

							// PHPMailer object
							$mail = $this->phpmailer_lib->load();
							// SMTP configuration
							$mail->isSMTP();
							$mail->Host     = 'smtp.gmail.com';
							$mail->SMTPAuth = true;
							$mail->Username = ADMIN_NO_REPLY_EMAIL;
							$mail->Password = ADMIN_NO_REPLY_PWD;
							$mail->SMTPSecure = 'ssl';
							$mail->Port     = 465;
							$mail->setFrom($from_email, 'PointWish');
							$mail->addReplyTo($from_email, 'PointWish');
							// Add a recipient
							$mail->addAddress($to_email);
							$mail->Subject = $subject;
							$mail->isHTML(true);
							$mail->Body = $message_html;

							if (!$mail->send()) {
								$result['message'] = $mail->ErrorInfo;
							} else {
								$result['message'] = 'Message has been sent';
								$mailCount = $this->common->getSingle('pwt_mailcount', array('date' => date('Y-m-d')));
								if ($mailCount) {
									$update_data = array(
										'count' => $mailCount->count + 1
									);
									$this->common->update_data('pwt_mailcount', array('id' => $mailCount->id), $update_data);
								} else {
									$insert_data = array(
										'date' => date('Y-m-d'),
										'count' => 1
									);
									$this->common->insert_data('pwt_mailcount', $insert_data);
								}
							}
							/********Mail for signup***End****/

							$remember_me = $this->input->post('remember_me', true);
							if ($remember_me == 1) {
								$cookie_username = array(
									'name'   => 'username',
									'value'  => $username,
									'expire' => time() + 60 * 60,
								);
								$cookie_password = array(
									'name'   => 'password',
									'value'  => $this->input->post('password', true),
									'expire' => time() + 60 * 60,
								);
								$this->input->set_cookie($cookie_username);
								$this->input->set_cookie($cookie_password);
							} else {
								delete_cookie("username");
								delete_cookie("password");
							}

							$result['success'] = 1;
							echo json_encode($result);
						} else {
							$this->session->set_userdata('userid', $login_data->id);
							$randnum = mt_rand(100000, 999999);
							/********Mail for activity ***Start****/
							$from_email = ADMIN_NO_REPLY_EMAIL; //ADMIN_INFO_EMAIL
							$to_email = $login_data->email;
							$subject = 'Email Verification';
							$WEB_URL = base_url();
							$htmlContent = file_get_contents("./mail_html/verification.html");
							$tpl2 = str_replace('{{WEB_URL}}', $WEB_URL, $htmlContent);
							$tpl3 = str_replace('{{USERNAME}}', $login_data->username, $tpl2);
							$tpl4 = str_replace('{{CODE}}', $randnum, $tpl3);
							$message_html = str_replace('{{INFO_EMAIL}}', $from_email, $tpl4);
							// PHPMailer object
							$mail = $this->phpmailer_lib->load();
							// SMTP configuration
							$mail->isSMTP();
							$mail->Host     = 'smtp.gmail.com';
							$mail->SMTPAuth = true;
							$mail->Username = ADMIN_NO_REPLY_EMAIL;
							$mail->Password = ADMIN_NO_REPLY_PWD;
							$mail->SMTPSecure = 'ssl';
							$mail->Port     = 465;
							// $mail->SMTPDebug = 3;
							$mail->setFrom($from_email, 'PointWish');
							$mail->addReplyTo($from_email, 'PointWish');
							// Add a recipient
							$mail->addAddress($to_email);
							$mail->Subject = $subject;
							$mail->isHTML(true);
							$mail->priority = 1;
							$mail->Body = $message_html;

							if (!$mail->send()) {
								$result['message'] = $mail->ErrorInfo;
							} else {
								$result['message'] = 'Message has been sent';
								$mailCount = $this->common->getSingle('pwt_mailcount', array('date' => date('Y-m-d')));
								if ($mailCount) {
									$update_data = array(
										'count' => $mailCount->count + 1
									);
									$this->common->update_data('pwt_mailcount', array('id' => $mailCount->id), $update_data);
								} else {
									$insert_data = array(
										'date' => date('Y-m-d'),
										'count' => 1
									);
									$this->common->insert_data('pwt_mailcount', $insert_data);
								}
							}
							/********Mail for signup***End****/

							/******Last Login Update*Start***/
							$u_query = "update pwt_users set google_auth_code = '" . $randnum . "' where id = '" . $login_data->id . "'";
							$this->db->query($u_query);
							/******Last Login Update*End***/
							$result['success'] = 3;
							echo json_encode($result);
						}
					} else {
						$this->session->set_userdata('userid', $login_data->id);
						$randnum = mt_rand(100000, 999999);
						/********Mail for activity ***Start****/
						$from_email = ADMIN_NO_REPLY_EMAIL; //ADMIN_INFO_EMAIL
						$to_email = $login_data->email;
						$subject = 'Email Verification';
						$WEB_URL = base_url();
						$htmlContent = file_get_contents("./mail_html/verification.html");
						$tpl2 = str_replace('{{WEB_URL}}', $WEB_URL, $htmlContent);
						$tpl3 = str_replace('{{USERNAME}}', $login_data->username, $tpl2);
						$tpl4 = str_replace('{{CODE}}', $randnum, $tpl3);
						$message_html = str_replace('{{INFO_EMAIL}}', $from_email, $tpl4);
						// PHPMailer object
						$mail = $this->phpmailer_lib->load();
						// SMTP configuration
						$mail->isSMTP();
						$mail->Host     = 'smtp.gmail.com';
						$mail->SMTPAuth = true;
						$mail->Username = ADMIN_NO_REPLY_EMAIL;
						$mail->Password = ADMIN_NO_REPLY_PWD;
						$mail->SMTPSecure = 'ssl';
						$mail->Port     = 465;
						$mail->setFrom($from_email, 'PointWish');
						$mail->addReplyTo($from_email, 'PointWish');
						// Add a recipient
						$mail->addAddress($to_email);
						$mail->Subject = $subject;
						$mail->priority = 1;
						$mail->isHTML(true);
						$mail->Body = $message_html;
						if (!$mail->send()) {
							$result['message'] = $mail->ErrorInfo;
						} else {
							$result['message'] = 'Message has been sent';
							$mailCount = $this->common->getSingle('pwt_mailcount', array('date' => date('Y-m-d')));
							if ($mailCount) {
								$update_data = array(
									'count' => $mailCount->count + 1
								);
								$this->common->update_data('pwt_mailcount', array('id' => $mailCount->id), $update_data);
							} else {
								$insert_data = array(
									'date' => date('Y-m-d'),
									'count' => 1
								);
								$this->common->insert_data('pwt_mailcount', $insert_data);
							}
						}
						/********Mail for signup***End****/

						/******Last Login Update*Start***/
						$u_query = "update pwt_users set google_auth_code = '" . $randnum . "' where id = '" . $login_data->id . "'";
						$this->db->query($u_query);
						/******Last Login Update*End***/
						$result['success'] = 3;
						echo json_encode($result);
					}
				// }
			} else {
				$result['success'] = 2;
				echo json_encode($result);
			}
		}
	}

	public function generateSponsorId()
	{
		$randnum = mt_rand(10000000, 99999999);
		$res = $this->common->getSingle('pwt_users', array('username' => $randnum));
		if ($res) {
			return $this->generateSponsorId();
		} else {
			return  $randnum;
		}
	}

	function getBrowser($user_agent = '')
	{
		$browser = "Unknown Browser";
		$browser_array = array(
			'/msie/i' => 'Internet Explorer',
			'/firefox/i' => 'Firefox',
			'/safari/i' => 'Safari',
			'/chrome/i' => 'Chrome',
			'/opera/i' => 'Opera',
			'/netscape/i' => 'Netscape',
			'/maxthon/i' => 'Maxthon',
			'/konqueror/i' => 'Konqueror',
			'/mobile/i' => 'Handheld Browser'
		);
		foreach ($browser_array as $regex => $value) {
			if (preg_match($regex, $user_agent)) {
				$browser = $value;
			}
		}
		return $browser;
	}

	function signup()
	{
		$sys = systemInfo();
		if ($sys['device'] == "MOBILE" && $sys['source'] == "WEB" && $sys['os'] == 'iPhone') {
			$this->session->set_flashdata('invalid', true);
			redirect(base_url());
		}
		$data['title'] = 'Sign Up';
		$this->form_validation->set_rules('fullname', 'User Name', 'required|is_unique[pwt_users.username]');
		$this->form_validation->set_rules('email', 'Email', 'required|is_unique[pwt_users.email]');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$x = $_GET['user_id'];
		if (strpos($x, '<marquee>')) {
			echo "<script>document.write('Invalid URL')</script>";
			exit(0);
		} else {
			if ($this->form_validation->run() == FALSE) {
				$this->load->view('signup');
			} else {
				$get = $this->getLocationInfoByIp();
				$ip_address = $get['ip'];
				// $countUsers = $this->common->count_users($ip_address);
				// if (count($countUsers) > 1) {
				// 	$result['success'] = 3;
				// 	echo json_encode($result);
				// } else {
					$sponsor = $this->input->post('sponsor_id');
					if ($sponsor) {
						$is_sponser = $this->is_sponser_exist_server($sponsor);
						if ($is_sponser['status'] == 2) {
							$result['success'] = 1;
							echo json_encode($result);
						}
					}

					$username = $this->input->post('fullname');
					$email = $this->input->post('email');
					$password_decrypted = trim($this->input->post('password'));
					$password = MD5($password_decrypted);
					$token = md5(uniqid(rand(), true));
					$sponsor1 = $this->common->getSingle('pwt_users', array('username' => $sponsor));
					$insert_data = array(
						'token' => $token,
						'full_name' => $username,
						'username' => $username,
						'password' => $password,
						'password_decrypted' => $password_decrypted,
						'sponsor' => $sponsor,
						'sponsor_id' =>$sponsor1->id,
						'email' => $email,
						'created_time' => date('Y-m-d H:i:s'),
						'modified_time' => date('Y-m-d H:i:s'),
						'is_status' => 0,
						// 'pwt_wallet' => (date('Y-m-d') < '2021-07-16' ? 10 : 0)
					);
					$insert_id = $this->common->insert_data('pwt_users', $insert_data);
					/*******Update Username **Start****/

					/********Mail for signup***Start****/
					$from_email = ADMIN_NO_REPLY_EMAIL; //ADMIN_INFO_EMAIL
					$to_email = $email;
					$subject = 'PointWish: Welcome to PointWish';
					$WEB_URL = base_url();
					$active_link = base_url() . 'signup-auth?id=' . base64_encode($insert_id);
					$referral_link = base_url() . 'home?user_id=' . $username;
					$htmlContent = file_get_contents("./mail_html/welcome.html");
					$tpl2 = str_replace('{{WEB_URL}}', $WEB_URL, $htmlContent);
					$tpl4 = str_replace('{{USERNAME}}', $username, $tpl2);
					$tpl5 = str_replace('{{PASSWORD}}', $password_decrypted, $tpl4);
					$tpl6 = str_replace('{{LINK}}', $active_link, $tpl5);
					$tpl7 = str_replace('{{REFERRAL}}', $referral_link, $tpl6);
					$message_html = str_replace('{{INFO_EMAIL}}', $from_email, $tpl7);
					//echo $message_html; die;
					// PHPMailer object
					$mail = $this->phpmailer_lib->load();
					// SMTP configuration
					$mail->isSMTP();
					$mail->Host     = 'smtp.gmail.com';
					$mail->SMTPAuth = true;
					$mail->Username = ADMIN_NO_REPLY_EMAIL;
					$mail->Password = ADMIN_NO_REPLY_PWD;
					$mail->SMTPSecure = 'ssl';
					$mail->Port     = 465;
					$mail->setFrom($from_email, 'PointWish');
					$mail->addReplyTo($from_email, 'PointWish');
					// Add a recipient
					$mail->addAddress($to_email);
					$mail->Subject = $subject;
					$mail->isHTML(true);
					$mail->Body = $message_html;

					if (!$mail->send()) {
						$result['message'] = $mail->ErrorInfo;
					} else {
						// $result['message'] = 'Message has been sent';
						$mailCount = $this->common->getSingle('pwt_mailcount', array('date' => date('Y-m-d')));
						if ($mailCount) {
							$update_data = array(
								'count' => $mailCount->count + 1
							);
							$this->common->update_data('pwt_mailcount', array('id' => $mailCount->id), $update_data);
						} else {
							$insert_data = array(
								'date' => date('Y-m-d'),
								'count' => 1
							);
							$this->common->insert_data('pwt_mailcount', $insert_data);
						}
					}
					/********Mail for signup***End****/

					/*******Insert Logs **Start*****/
					$insert_data = array(
						'user_id' => $insert_id,
						'message' => 'Signup',
						'browser_name' => $this->getBrowser($_SERVER['HTTP_USER_AGENT']),
						'ip_address' => $ip_address,
						'os' => $sys['os'],
						'device' => $sys['device'],
						'status' => '1',
						'created_datetime' => date('Y-m-d H:i:s'),
					);
					$this->common->insert_data('pwt_log_history', $insert_data);
					/*******Insert Logs **End*****/

					$this->session->set_flashdata('username', $username);
					$this->session->set_flashdata('password', $password_decrypted);
					$this->session->set_flashdata('error_message', 'You have successfully signed up !');
					$result['success'] = 1;
					$result['username'] = $username;
					$result['password'] = $password_decrypted;
					echo json_encode($result);
				// }
			}
		}
	}

	private function getRealIpAddr()
	{
		if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
		{
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
		{
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}

	private function is_sponser_exist_server($sponsor)
	{
		$result = $this->common->getSingle('pwt_users', array('username' => $sponsor, 'is_delete' => 1, 'is_status' => 1));
		if (!empty($result)) {
			$status = 1;
			$user = $result->id;
		} else {
			$status = 2;
			$user = '';
		}
		$data['status'] = $status;
		$data['userid'] = $user;
		return $data;
	}

	function is_sponser_exist()
	{
		if (!isset($_GET['sponsor_id'])) {
			redirect(base_url());
		}
		$sponsor_id = $this->input->get('sponsor_id');
		$result = $this->common->getSingle('pwt_users', array('username' => $sponsor_id, 'is_delete' => 1));
		$member_name = '';
		$userid = '';
		if (!empty($result)) {
			$status = 1;
			$userid = $result->id;
			$member_name = ucfirst($result->full_name);
		} else {
			$status = 2;
			$userid = '';
		}
		$json_arr = array('status' => $status, 'member_name' => $member_name, 'userid' => $userid);
		echo json_encode($json_arr);
		exit;
	}

	function is_email_exist()
	{
		if (!isset($_POST['email'])) {
			redirect(base_url());
		}
		$email = $this->input->post('email');
		$result = $this->common->getSingle('pwt_users', array('email' => $email, 'is_delete' => 1));
		if (!empty($result)) {
			$data['status'] = 2;
		} else {
			$data['status'] = 1;
		}
		echo json_encode($data);
	}

	function refresh()
	{
		// Captcha configuration
		$config = array(
			'img_path'      => './uploads/captcha_images/',
			'img_url'       => base_url() . '/uploads/captcha_images/',
			'font_path'     => 'system/fonts/texb.ttf',
			'img_width'     => '160',
			'img_height'    => 50,
			'word_length'   => 8,
			'font_size'     => 18
		);
		$captcha = create_captcha($config);
		// Unset previous captcha and set new captcha word
		$this->session->unset_userdata('captchaCode');
		$this->session->set_userdata('captchaCode', $captcha['word']);
		// Display captcha image
		echo $captcha['image'];
	}

	function forgot_password()
	{
		$sys = systemInfo();
		if ($sys['device'] == "MOBILE" && $sys['source'] == "WEB" && $sys['os'] == 'iPhone') {
			$this->session->set_flashdata('invalid', true);
			redirect(base_url());
		}
		if ($this->session->userdata('user_id')) {
			redirect(base_url());
		}
		$data['title'] = 'Forget Password';
		$this->form_validation->set_rules('email', 'Email', 'required');
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('forgot_password', $data);
		} else {
			$username = $this->input->post('email', true);
			$user_data = $this->common->getSingle('pwt_users', array('email' => $username, 'is_delete' => 1));
			$email = $user_data->email;
			if (!empty($user_data)) {

				/********Mail***Start****/
				$from_email = ADMIN_NO_REPLY_EMAIL;
				$to_email = $email;
				$subject = 'Forgot Password';
				$WEB_URL = base_url();
				$RESET_LINK = base_url() . 'login/reset_password/' . $user_data->token;
				$USERNAME = $user_data->full_name;
				$htmlContent = file_get_contents("./mail_html/forgot_password.html");
				$tpl2 = str_replace('{{WEB_URL}}', $WEB_URL, $htmlContent);
				$tpl3 = str_replace('{{NAME}}', $USERNAME, $tpl2);
				$message_html = str_replace('{{RESET_LINK}}', $RESET_LINK, $tpl3);
				// PHPMailer object
				$mail = $this->phpmailer_lib->load();
				// SMTP configuration
				$mail->isSMTP();
				$mail->Host     = 'smtp.gmail.com';
				$mail->SMTPAuth = true;
				$mail->Username = ADMIN_NO_REPLY_EMAIL;
				$mail->Password = ADMIN_NO_REPLY_PWD;
				$mail->SMTPSecure = 'ssl';
				$mail->Port     = 465;
				$mail->setFrom($from_email, 'PointWish');
				$mail->addReplyTo($from_email, 'PointWish');
				// Add a recipient
				$mail->addAddress($to_email);
				$mail->Subject = $subject;
				$mail->isHTML(true);
				$mail->Body = $message_html;

				if (!$mail->send()) {
					$result['message'] = $mail->ErrorInfo;
				} else {
					$result['message'] = 'Message has been sent';
					$mailCount = $this->common->getSingle('pwt_mailcount', array('date' => date('Y-m-d')));
					if ($mailCount) {
						$update_data = array(
							'count' => $mailCount->count + 1
						);
						$this->common->update_data('pwt_mailcount', array('id' => $mailCount->id), $update_data);
					} else {
						$insert_data = array(
							'date' => date('Y-m-d'),
							'count' => 1
						);
						$this->common->insert_data('pwt_mailcount', $insert_data);
					}
				}
				$this->session->set_flashdata("success_message", "Please check your email for the password !");
				$result['success'] = 1;
				echo json_encode($result);
			} else {
				$this->session->set_flashdata("warning_message", "Please enter your registered username !");
				$result['success'] = 2;
				echo json_encode($result);
			}
		}
	}

	function reset_password()
	{
		// if ($this->session->userdata('user_id')) {
		// 	redirect(base_url() . 'user/home');
		// }
		$data['title'] = 'Reset Password';
		$token = $this->uri->segment(3);
		$user_data = $this->common->getSingle('pwt_users', array('token' => $token));
		if (empty($user_data)) {
			$result['success'] = 0;
			echo json_encode($result);
		}
		$new_password = $this->input->post('password', true);
		$confirm_password = $this->input->post('confirm_password', true);
		if ($new_password && $confirm_password) {
			$new_password = $this->input->post('password', true);
			$confirm_password = $this->input->post('confirm_password', true);
			$update_data = array(
				'password' => MD5($new_password),
				'password_decrypted' => $new_password
			);
			$this->common->update_data('pwt_users', array('token' => $token), $update_data);
			$this->session->set_flashdata("success_message", 'Password has been reset successfully. Please login now !');
			$result['success'] = 1;
			echo json_encode($result);
		} else {
			$this->load->view('reset_password', $data);
		}
	}

	function random_string($length)
	{
		$key = '';
		$keys = array_merge(range(0, 9), range('a', 'z'));
		for ($i = 0; $i < $length; $i++) {
			$key .= $keys[array_rand($keys)];
		}
		return $key;
	}

	function logout()
	{
		$this->session->unset_userdata('user_id');
		$this->session->unset_userdata('user_name');
		$this->session->unset_userdata('user_email');
		$this->session->unset_userdata('full_name');
		$this->session->unset_userdata('logsid');
		// 		$this->session->unset_userdata('language');
		// 		$this->session->unset_userdata('lang');
		$sys = systemInfo();
		if ($sys['device'] == "MOBILE" && $sys['source'] == "APP" && $sys['os'] == 'iPhone') {
			redirect(base_url());
		}
		redirect(base_url('home'));
	}

	function is_unique_user()
	{
		$username = $this->input->get('username');
		$result = $this->common->getSingle('pwt_users', array('username' => $username, 'is_delete' => 1));
		if (!empty($result)) {
			$status = 1;
		} else {
			$status = 2;
		}
		$json_arr = array('status' => $status);
		echo json_encode($json_arr);
		exit;
	}

	function verification()
	{
		if ($this->session->userid) {
			$data['title'] = "Verification";
			$data['page'] = "Verification";
			$this->load->view('verification', $data);
		} else {
			redirect('login');
		}
	}

	function login_auth()
	{
		$user = $this->common->getSingle('pwt_users', array('id' => $this->session->userid));
		$secret = $user->google_auth_code;
		$code = $this->input->post('code');
		if ($code) {
			$code = str_replace('-', '', $code);
			if ($code == $secret) {
				if ($user->is_status == 0) {
					// 	$usdt = 5;
					$this->common->update_data('pwt_users', array('id' => $user->id), array('is_status' => 1));
					// 	$totalUSDT = 5;
					/*******Referral History **Start****/
					// 	$insert_note = array(
					// 		'from_user' => $user->id,
					// 		'to_user' => $user->id,
					// 		'log' => 'credit',
					// 		'type' => 'referral',
					// 		'payment_type' => 'USDT',
					// 		'amount' => 5,
					// 		'balance' => getWalletAmountByUserID($user->id)[0],
					// 		'txn_id' => '#B' . rand(),
					// 		'message' => "Signup Referral",
					// 		'status' => 1,
					// 		'created_datetime' => date('Y-m-d H:i:s'),
					// 		'modified_datetime' => date('Y-m-d H:i:s'),
					// 	);
					// 	$this->common->insert_data('pwt_user_notification', $insert_note);

					/*******Referral History **Start****/
					// if ($user->sponsor != '') {
						// $totalUSDT = $totalUSDT + 1;
						/*******Direct Referral **Start****/
						// $u_query = "update pwt_users SET unlocked_pwt = unlocked_pwt + 1 where id = '" . $user->sponsor_id . "'";
						// $this->db->query($u_query);
						/*******Direct Referral **End****/

						/*******Direct Referral History **Start****/
						// $insert_note = array(
						// 	'from_user' => $user->sponsor_id,
						// 	'to_user' => $user->id,
						// 	'log' => 'credit',
						// 	'type' => 'referral',
						// 	'payment_type' => 'pwt',
						// 	'amount' => 1,
						// 	'balance' => getWalletAmountByUserID($user->sponsor_id)[3],
						// 	'txn_id' => '#B' . rand(),
						// 	'message' => "Direct Referral",
						// 	'status' => 1,
						// 	'created_datetime' => date('Y-m-d H:i:s'),
						// 	'modified_datetime' => date('Y-m-d H:i:s'),
						// );
						// $this->common->insert_data('pwt_user_notification', $insert_note);
						/*******Direct Referral History **Start****/

						/*******Notification **Start****/
						// $insert_note = array(
						// 	'user_id' => $user->id,
						// 	'type' => 'bonus',
						// 	'target' => base_url('history'),
						// 	'detail' => "Referral bonus 1 USDT is received - $user->username",
						// 	'date' => date('Y-m-d H:i:s'),
						// );
						// $this->common->insert_data('pwt_notification', $insert_note);
						/*******Notification **Start****/
					// }
					/*******Update pwt Supply **Start*****/
					// if ($totalpwt) {
					// 	$u_query = "update pwt_set_price SET total_supply = total_supply -" . $totalpwt . ", total_supplied = total_supplied + " . $totalpwt .  " where id = 1";
					// 	$this->db->query($u_query);
					// }
					/*******Update pwt Supply**End*****/

					/*******Notification **Start****/
					// 	$insert_note = array(
					// 		'user_id' => $user->id,
					// 		'type' => 'bonus',
					// 		'target' => base_url('history'),
					// 		'detail' => "Welcome bonus 5 USDT is received",
					// 		'date' => date('Y-m-d H:i:s'),
					// 	);
					// 	$this->common->insert_data('pwt_notification', $insert_note);
					/*******Notification **Start****/
				}
				$get = $this->getLocationInfoByIp();
				$ip_address = $get['ip'];
				$sys = systemInfo();
				$insert_data = array(
					'user_id' => $user->id,
					'message' => 'Login attempt success !',
					'browser_name' => $this->getBrowser($_SERVER['HTTP_USER_AGENT']),
					'ip_address' => $ip_address,
					'os' => $sys['os'],
					'device' => $sys['device'],
					'status' => '1',
					'is_active' => 1,
					'created_datetime' => date('Y-m-d H:i:s'),
				);
				$log_id = $this->common->insert_data('pwt_log_history', $insert_data);
				/*******Android Bonus**Start****/
				// $isAndroid = false;
				// if (($sys['os'] == 'Android' || $sys['os'] == 'iPhone') && $user->is_android_bonus == 0) {
				// 	$insert_note = array(
				// 		'from_user' => $user->id,
				// 		'to_user' => $user->id,
				// 		'log' => 'credit',
				// 		'type' => 'android',
				// 		'payment_type' => 'pwt',
				// 		'amount' => 5,
				// 		'balance' => getWalletAmountByUserID($user->id)[3] + 5,
				// 		'txn_id' => '#B' . rand(),
				// 		'message' => "App Bonus",
				// 		'status' => 1,
				// 		'created_datetime' => date('Y-m-d H:i:s'),
				// 		'modified_datetime' => date('Y-m-d H:i:s'),
				// 	);
				// 	$this->common->insert_data('pwt_user_notification', $insert_note);

				// 	/*******Notification **Start****/
				// 	$insert_note = array(
				// 		'user_id' => $user->id,
				// 		'type' => 'bonus',
				// 		'target' => base_url('history'),
				// 		'detail' => "App bonus 5 USDT is received",
				// 		'date' => date('Y-m-d H:i:s'),
				// 	);
				// 	$this->common->insert_data('pwt_notification', $insert_note);
				// 	/*******Notification **Start****/

				// 	$isAndroid = true;
				// }
				/*******Android Bonus **End****/
				/******Last Login Update*Start***/
				// if ($user->country) {
				// 	$set = $isAndroid ? ", unlocked_pwt = unlocked_pwt + 5, is_android_bonus = 1, login_country = '" . $get['country'] . "'" : ", login_country = '" . $get['country'] . "'";
				// } else {
				// 	$set = $isAndroid ? ", unlocked_pwt = unlocked_pwt + 5, is_android_bonus = 1, country = '" . $get['country'] . "' , login_country = '" . $get['country'] . "'" : ", country = '" . $get['country'] . "' , login_country = '" . $get['country'] . "'";
				// }
				$u_query = "update pwt_users set my_os ='" . $sys['os'] . "', google_auth_code = '', last_login = '" . date('Y-m-d H:i:s') . "' where id = '" . $user->id . "'";
				$this->db->query($u_query);
				/******Last Login Update*End***/
				$session_data = array(
					'user_id'   => $user->id,
					'user_name'  => $user->username,
					'user_email'  => $user->email,
					'full_name' => $user->full_name,
					'trade_type' => $user->trade_type,
					'logsid' => $log_id,
					'language' => "english",
					'lang' => "en"
				);
				$this->session->set_userdata($session_data);
				// $this->session->sess_update();

				/********Mail for activity ***Start****/
				$from_email = ADMIN_NO_REPLY_EMAIL; //ADMIN_INFO_EMAIL
				$to_email = $this->session->user_email;
				$subject = 'Routine Activity';
				$WEB_URL = base_url();
				$htmlContent = file_get_contents("./mail_html/activity.html");
				$tpl2 = str_replace('{{WEB_URL}}', $WEB_URL, $htmlContent);
				$tpl3 = str_replace('{{USERNAME}}', $user->username, $tpl2);
				$message_html = str_replace('{{INFO_EMAIL}}', $from_email, $tpl3);
				// PHPMailer object
				$mail = $this->phpmailer_lib->load();
				// SMTP configuration
				$mail->isSMTP();
				$mail->Host     = 'smtp.gmail.com';
				$mail->SMTPAuth = true;
				$mail->Username = ADMIN_NO_REPLY_EMAIL;
				$mail->Password = ADMIN_NO_REPLY_PWD;
				$mail->SMTPSecure = 'ssl';
				$mail->Port     = 465;
				$mail->setFrom($from_email, 'PointWish');
				$mail->addReplyTo($from_email, 'PointWish');
				// Add a recipient
				$mail->addAddress($to_email);
				$mail->Subject = $subject;
				$mail->isHTML(true);
				$mail->Body = $message_html;

				if (!$mail->send()) {
					$result['message'] = $mail->ErrorInfo;
				} else {
					$result['message'] = 'Message has been sent';
					$mailCount = $this->common->getSingle('pwt_mailcount', array('date' => date('Y-m-d')));
					if ($mailCount) {
						$update_data = array(
							'count' => $mailCount->count + 1
						);
						$this->common->update_data('pwt_mailcount', array('id' => $mailCount->id), $update_data);
					} else {
						$insert_data = array(
							'date' => date('Y-m-d'),
							'count' => 1
						);
						$this->common->insert_data('pwt_mailcount', $insert_data);
					}
				}
				/********Mail for activity ***End****/
				$data['success'] = true;
			} else {
				$data['message'] = "Invalid Code";
				$data['success'] = false;
			}
		} else {
			$data['message'] = "Invalid Code";
			$data['success'] = false;
		}
		echo json_encode($data);
	}

	function verify()
	{
		if ($_GET['id']) {
			$id = base64_decode($_GET['id']);
			$login_data = $this->common->getSingle('pwt_users', ['id' => $id]);
			if ($login_data) {
				if ($login_data->is_status == 1) {
					$this->session->set_flashdata('success_message', 'Your account is already activated');
				} else {
					// 	$usdt = 5;
					$this->common->update_data('pwt_users', array('id' => $id), array('is_status' => 1));

					// $totalpwt = 5;
					/*******Referral History **Start****/
					// 	$insert_note = array(
					// 		'from_user' => $id,
					// 		'to_user' => $id,
					// 		'log' => 'credit',
					// 		'type' => 'referral',
					// 		'payment_type' => 'USDT',
					// 		'amount' => 5,
					// 		'balance' => getWalletAmountByUserID($id)[0],
					// 		'txn_id' => '#B' . rand(),
					// 		'message' => "Signup Referral",
					// 		'status' => 1,
					// 		'created_datetime' => date('Y-m-d H:i:s'),
					// 		'modified_datetime' => date('Y-m-d H:i:s'),
					// 	);
					// 	$this->common->insert_data('pwt_user_notification', $insert_note);
					/*******Referral History **Start****/

					/*******Notification **Start****/
					// 	$insert_note = array(
					// 		'user_id' => $login_data->id,
					// 		'type' => 'bonus',
					// 		'target' => base_url('history'),
					// 		'detail' => "Signup bonus 5 USDT is received",
					// 		'date' => date('Y-m-d H:i:s'),
					// 	);
					// 	$this->common->insert_data('pwt_notification', $insert_note);
					/*******Notification **Start****/

					if ($login_data->sponsor) {
						// $totalpwt = $totalpwt + 1;
						/*******Direct Referral **Start****/
						// $u_query = "update pwt_users SET unlocked_pwt = unlocked_pwt + 1 where id = '" . $login_data->sponsor_id . "'";
						// $this->db->query($u_query);
						/*******Direct Referral **End****/

						/*******Direct Referral History **Start****/
						// $insert_note = array(
						// 	'from_user' => $login_data->sponsor_id,
						// 	'to_user' => $id,
						// 	'log' => 'credit',
						// 	'type' => 'referral',
						// 	'payment_type' => 'pwt',
						// 	'amount' => 1,
						// 	'balance' => getWalletAmountByUserID($login_data->sponsor_id)[3],
						// 	'txn_id' => '#B' . rand(),
						// 	'message' => "Direct Referral",
						// 	'status' => 1,
						// 	'created_datetime' => date('Y-m-d H:i:s'),
						// 	'modified_datetime' => date('Y-m-d H:i:s'),
						// );
						// $this->common->insert_data('pwt_user_notification', $insert_note);
						/*******Direct Referral History **End****/

						/*******Notification **Start****/
						// $insert_note = array(
						// 	'user_id' => $login_data->sponsor_id,
						// 	'type' => 'bonus',
						// 	'target' => base_url('history'),
						// 	'detail' => "Referral bonus 1 pwt is received - $login_data->username",
						// 	'date' => date('Y-m-d H:i:s'),
						// );
						// $this->common->insert_data('pwt_notification', $insert_note);
						/*******Notification **Start****/
					}
					/*******Update pwt Supply **Start*****/
					// if ($totalpwt) {
					// 	$u_query = "update pwt_set_price SET total_supply = total_supply -" . $totalpwt . ", total_supplied = total_supplied + " . $totalpwt .  " where id = 1";
					// 	$this->db->query($u_query);
					// }
					/*******Update pwt Supply**End*****/

					$this->session->set_flashdata('success_message', 'Account activated successfully');
				}
				redirect(base_url('login'));
			} else {
				redirect(base_url());
			}
		}
	}

	function signup_verification()
	{
		if ($_GET['id']) {
			$data['title'] = 'Email Verification';
			$data['id'] = base64_decode($_GET['id']);
			$this->load->view('eamil_verify', $data);
		}
	}

	function maintenance()
	{
		$data['title'] = "Maintenance";
		$this->load->view('maintenance', $data);
	}

	private function getLocationInfoByIp()
	{
		$client  = @$_SERVER['HTTP_CLIENT_IP'];
		$forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
		$remote  = @$_SERVER['REMOTE_ADDR'];
		$result  = array('country' => '', 'city' => '');
		if (filter_var($client, FILTER_VALIDATE_IP)) {
			$ip = $client;
		} elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
			$ip = $forward;
		} else {
			$ip = $remote;
		}
		$ip_data = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
		if ($ip_data && $ip_data->geoplugin_countryName != null) {
			$result['country'] = $ip_data->geoplugin_countryName;
		}
		$result['ip'] = $ip;
		return $result;
	}

	function is_activeUser()
	{
		echo json_encode(logout_session($this->input->post('userid')));
	}
}
