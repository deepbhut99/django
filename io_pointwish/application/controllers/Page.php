<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Page extends MY_Controller
{

	function __construct()
	{
		parent::__construct();
	}

	function index($param = '')
	{
		$data['title'] = 'Best Online Trading Platform';
		$data['type'] = $param;
		if ($param) {
			$this->lang->load('local', $param);
			$this->session->set_userdata('language', $param);
		}
		$this->load->view('home', $data);
	}

	function contactus($s1 = '')
	{

		if ($s1) {
			redirect(base_url('home/contactus'));
		} else {
			$data['title'] = 'Contact Us';
			$this->load->view('dark/contact_us', $data);
		}
	}

	function rules($para1 = '')
	{
		if ($para1 == 'privacy_policy') {
			$this->privacy_policy();
		} else if ($para1 == 'terms') {
			$this->terms_condition();
		}
	}

	function terms_condition($s1 = '')
	{
		$data['title'] = "Terms & Conditions";
		$this->load->view('dark/terms_condition', $data);
	}

	function privacy_policy()
	{
		$data['title'] = 'Privacy Policy';
		$this->load->view('dark/privacy_policy', $data);
	}
	
	function event()
	{
		$data['title'] = 'Events';
		$this->load->view('dark/event', $data);
	}
}
