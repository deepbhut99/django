<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Accounts extends MY_Controller
{

	function __construct()
	{
		parent::__construct();

		/****checks it is login or not start****/
		$this->_adminLoginCheck();
		/****checks it is login or not End****/
	}
	function change_password()
	{
		$data['title'] = 'Change Password';

		$this->form_validation->set_rules('old_password', 'Old password', 'required');
		$this->form_validation->set_rules('new_password', 'New password', 'required');
		$this->form_validation->set_rules('confirm_password', 'Confirm password', 'required');

		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		$user_data = $this->common->getSingle('admin_login', array('id' => $this->session->userdata('admin_id')));

		if ($this->form_validation->run() == FALSE) {
			$this->load->view('admin/profile/change_password', $data);
		} else {
			if ($user_data->password != MD5($this->input->post('old_password', true))) {
				$this->session->set_flashdata('error_message', 'Please enter correct old password !');
			} else {
				$update_data = array(
					'password' => MD5($this->input->post('new_password', true)),
					'password_d' => $this->input->post('new_password', true),
				);

				$this->common->update_data('admin_login', array('id' => $this->session->userdata('admin_id')), $update_data);

				$this->session->set_flashdata('success_message', 'Password changed successfully !');
			}

			redirect(base_url() . 'admin/accounts/change_password');
		}
	}
}
