<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Algorithm extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        /****checks it is login or not start****/
        $this->_adminLoginCheck();
        /****checks it is login or not End****/

        // 		$this->load->model('admin/Withdraw_model','withdraw');

    }

    function index()
    {
        $data['title'] = 'Algorithm Settings';
        $current = $this->db->order_by('id', 'desc')->limit(1)->get('pwt_algo_setting')->result_array()[0];
        $data['topValue'] = (int)$current['top_users_value'];
        $data['normalValue'] = (int)$current['other_users_value'];
        $data['api'] = $this->db->order_by('id', 'desc')->limit(1)->get('pwt_api')->row_array()['api'];
        $data['profit'] = $this->db->select('profit')->order_by('id', 'desc')->get('pwt_algo_profit')->result_array()[0]['profit'];
        $this->load->view('admin/algorithm/index', $data);
    }

    function updateCriteria()
    {
        $data = array(
            'userid'            =>  $this->session->userdata('admin_id'),
            'top_users_value'   => $this->input->post('topValue'),
            'other_users_value' => $this->input->post('otherValue')
        );
        $this->db->insert('pwt_algo_setting', $data);
        redirect(base_url('admin/algorithm'));
    }
    
    function updateAPI()
    {
        $data = array(
            'api' => $this->input->post('api')
        );
        echo json_encode($this->db->insert('pwt_api', $data));
    }

    function profit()
    {
        $data['title'] = 'Company Profit';
        $data['profit'] = $this->db->select('profit')->order_by('id', 'desc')->get('pwt_algo_profit')->result_array()[0]['profit'];
        $this->load->view('admin/algorithm/profit', $data);
    }

    function updateProfit()
    {
        $data = array(
            'admin_id'    =>  $this->session->userdata('admin_id'),
            'profit'    => $this->input->post('profit')
        );
        $this->db->insert('pwt_algo_profit', $data);
        redirect(base_url('admin/algorithm'));
        // $this->profit();
    }
}
