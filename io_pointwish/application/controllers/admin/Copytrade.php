<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Copytrade extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        /****checks it is login or not start****/
        $this->_adminLoginCheck();
        /****checks it is login or not End****/

        $this->load->model('admin/Copytrade_model', 'trade');
    }

    function index()
    {
        $data['title'] = 'Copy Trade';
        $data['users'] = $this->trade->get_master_users();
        $this->load->view('admin/copytrade/index', $data);
    }

    function get_users_list()
    {
        echo json_encode($this->trade->get_users_list());
    }

    function change_status()
    {
        $this->common->update_data('pwt_copy_trade', array('id' => $this->input->post('id')), array('is_active' => $this->input->post('status')));
        $data['success'] = true;
        $this->session->set_flashdata('success_message', 'Changed Successfully');
        echo json_encode($data);
    }

   
}
