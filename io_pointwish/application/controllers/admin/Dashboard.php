<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('memory_limit', -1);
ini_set('max_execution_time', 300);
class Dashboard extends MY_Controller {

    function __construct() 
    {
		parent::__construct();

		/****checks it is login or not start****/
		$this->_adminLoginCheck();
		/****checks it is login or not End****/				

		$this->load->model('admin/Dashboard_model', 'dashboard');

    }

	public function index()
	{
		$data['title'] = 'Dashboard';
		$data['today_active'] = $this->dashboard->getTodayActiveUsers();
		$users = $this->dashboard->getTotalUserListCount();
		$data['total_users'] = $users->total_users;
		$data['paid_users'] = $users->active_users;
		$data['unpaid_users'] = $data['total_users'] - $data['paid_users'];

		$data['today_business'] = $this->dashboard->getTodayBusiness();
		$data['total_business'] = $this->dashboard->getTotalBusiness();

		$data['today_withdrawal'] = $this->dashboard->getTodayWithdrawal();
		$data['total_withdrawal'] = $this->dashboard->getTotalWithdrawal();

		$data['today_pending_withdrawal'] = $this->dashboard->getTodayPendingWithdrawal();
		$data['total_pending_withdrawal'] = $this->dashboard->getTotalPendingWithdrawal();
		$this->load->view('admin/dashboard/dashboard', $data);
	}
	
	function exportdata(){
		$type = base64_decode($_GET['type']);
		$data['data'] = $this->dashboard->getpaidunpaiduser($type);
		$this->load->view('admin/users/download_paid_unpaid_users',$data);
	}
	
	function set_maintenance()
	{
		$this->common->update_data('pwt_set_price', array('id' => 1), array('is_maintenance' => $this->input->post('status')));
		redirect('dashboard');
	}
	function company_business($type)
	{
		$data['title'] = 'Company Business';

		if (!empty($_GET['offset'])) {
			$offset = $_GET['offset'];
		} else {
			$offset = 1;
		}

		$per_page = 100;

		$start_limit = ($per_page * $offset) - $per_page;


		$data['data_result'] = $this->dashboard->get_company_business($type, $start_limit, $per_page);
		$total_records = $this->dashboard->get_company_business_total($type);

		$config['base_url'] = base_url() . 'company-business/' . $type;
		$config['total_rows'] = $total_records;
		$config['per_page'] = $per_page;
		//$config["uri_segment"] = 3;
		$config['query_string_segment'] = 'offset';
		$config['page_query_string'] = true;
		// custom paging configuration
		//$config['num_links'] = 4;
		$config['use_page_numbers'] = TRUE;
		$config['reuse_query_string'] = TRUE;


		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';


		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li class="firstlink page-item page-link">';
		$config['first_tag_close'] = '</li>';

		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li class="lastlink page-item page-link">';
		$config['last_tag_close'] = '</li>';

		$config['next_link'] = 'Next';
		$config['next_tag_open'] = '<li class="nextlink page-item page-link">';
		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = 'Prev';
		$config['prev_tag_open'] = '<li class="prevlink page-item page-link">';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active page-item"><a href="#" class="page-link">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li class="numlink page-item page-link">';
		$config['num_tag_close'] = '</li>';

		$this->pagination->initialize($config);

		$data["links"] = $this->pagination->create_links();

		$data["total_records"] = $total_records;

		$this->load->view('admin/dashboard/business', $data);
	}

}
