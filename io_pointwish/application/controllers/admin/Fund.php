<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Fund extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        /****checks it is login or not start****/
        $this->_adminLoginCheck();
        /****checks it is login or not End****/

        /****checks it is admin or sub admin****/
        if ($this->session->type == 2) {
            $this->session->set_flashdata('error_message', "You don't have permission");
            redirect('admin/user/total_users');
        }
        /****checks it is admin or sub admin****/

        $this->load->model('admin/Fund_model', 'fund');
    }

    function transfer_fund()
    {
        $data['title'] = 'Transfer Fund';

        $this->form_validation->set_rules('member_id', 'Member ID', 'required');
        $this->form_validation->set_rules('amount', 'Amount', 'required');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('admin/fund/transfer_fund', $data);
        } else {
            $member_id = $this->input->post('member_id', true);
            $amount = $this->input->post('amount', true);

            $member_data = $this->common->getSingle('pwt_users', array('username' => $member_id, 'is_delete' => 1));

            if (empty($member_data)) {
                $this->session->set_flashdata('error_message', "Member ID doesn't exist !");
                redirect(base_url() . 'admin/fund/transfer_fund');
            }

            $type = $this->input->post('type');
            if ($type == 'pwt') {
                $query2 = "update pwt_users set unlocked_pwt = unlocked_pwt + " . $amount . " where username = '" . $member_id . "'";
            } else {
                $query2 = "update pwt_users set wallet_amount = wallet_amount + " . $amount . " where username = '" . $member_id . "'";
            }
            $this->db->query($query2);

            /********Insert Data in `transfer_history`**Start*******/
            $insert_data = array(
                'from_user' => 0,
                'to_user' => $member_data->id,
                'transfer_charge' => '0',
                'amount' => $amount,
                'transfer_by' => 'admin',
                'transfer_type' => $type,
                'created_datetime' => date('Y-m-d H:i:s'),
            );

            $this->common->insert_data('pwt_transfer_history', $insert_data);

            /***pwt_user_notification***/
            $insert_data_n = array(
                'from_user' => $member_data->id,
                'to_user' => $member_data->id,
                'log' => 'credit',
                'amount' => $amount,
                'payment_type' => $type,
                'type' => 'transfer',
                'balance' => $type == 'pwt' ? getWalletAmountByUserID($member_data->id)[2] : getWalletAmountByUserID($member_data->id)[0],
                'txn_id' => '#FT' . rand(),
                'message' => "Admin",
                'status' => 1,
                'created_datetime' => date('Y-m-d H:i:s'),
                'modified_datetime' => date('Y-m-d H:i:s'),
            );
            $this->common->insert_data('pwt_user_notification', $insert_data_n);
            /***pwt_user_notification***/
            
            /*******Notification **Start****/
            $insert_note = array(
                'user_id' => $member_data->id,
                'type' => 'transfer',
                'target' => base_url('history'),
                'detail' => "$amount $type transfer by admin",
                'date' => date('Y-m-d H:i:s'),
            );
            $this->common->insert_data('pwt_notification', $insert_note);
            /*******Notification **Start****/

            /********Insert Data in `transfer_history`**End*******/

            $this->session->set_flashdata('error_message', 'Amount transferred successfully !');

            redirect(base_url() . 'admin/fund/transfer_fund');
        }
    }

    function isMemberAvailabe()
    {
        if (!isset($_POST['member_id'])) {
            redirect(base_url());
        }

        $member_id = $this->input->post('member_id');

        $member_data = $this->common->getSingle('pwt_users', array('username' => $member_id, 'is_delete' => 1));

        $member_name = '';

        if (!empty($member_data)) {
            $member_name = ucfirst($member_data->full_name);
            $status = 1;
        } else {
            $status = 2;
        }

        $json_arr = array('status' => $status, 'member_name' => $member_name);

        echo json_encode($json_arr);
        exit;
    }


    function transfer_fund_history()
    {
        $data['title'] = 'Transfer Fund History';
        $data['data_result'] = $this->fund->getMyTransferAmount();
        $this->load->view('admin/fund/transfer_fund_history', $data);
    }
}
