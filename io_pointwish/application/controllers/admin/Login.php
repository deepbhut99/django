<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends MY_Controller
{

	function __construct()
	{
		parent::__construct();

		/*****loading model*/
		$this->load->model('Admin_login');
	}

	public function index()
	{
		$data['title'] = 'Login';
	    if ($this->uri->segment(1) == md5('trade_earnfinex')) {
    		if (!$this->session->userdata('admin_id')) {
    
    			$this->form_validation->set_rules('username', 'Username', 'required');
    			$this->form_validation->set_rules('password', 'Password', 'required');
    			$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
    
    			if ($this->form_validation->run() == FALSE) {
    				$this->load->view('admin/login', $data);
    			} else {
    				$username = $this->input->post('username', true);
    				$password = $this->input->post('password', true);
    
    				$redirect_url = $this->input->post('redirect_url') ? $this->input->post('redirect_url') : base_url() . 'admin/login';
    
    				$remember_me = $this->input->post('remember_me', true);
    
    				$where = array('username' => $username, 'password' => MD5($password));
    				$login_data = $this->common->getSingle('admin_login', $where);
    
    				if (!empty($login_data)) {
    					$session_data = array(
    						'admin_id'   => $login_data->id,
    						'admin_email'  => $login_data->email,
    						'admin_username'  => $login_data->username,
    					);
    					$this->session->set_userdata($session_data);
    
    					if ($remember_me == 1) {
    						$cookie_username = array(
    							'name'   => 'a_username',
    							'value'  => $username,
    							'expire' => time() + 60 * 60,
    						);
    						$cookie_password = array(
    							'name'   => 'a_password',
    							'value'  => $this->input->post('password', true),
    							'expire' => time() + 60 * 60,
    						);
    
    						$this->input->set_cookie($cookie_username);
    						$this->input->set_cookie($cookie_password);
    					} else {
    						delete_cookie("a_username");
    						delete_cookie("a_password");
    					}
    
    					redirect('admin/dashboard');
    				} else {
    
    					$this->session->set_flashdata('error_session', 'Invalid Username or Password !');
    					redirect('admin/login');
    				}
    			}
    		} else {
    			redirect('admin/dashboard');
    		}
	    } else {
	        redirect(base_url());
	    }
	}

	function default_login()
	{
		$id = base64_decode($_GET['id']);
		$login_data = $this->common->getSingle('pwt_users', ['id' => $id]);
		$session_data = array(
			'user_id'   => $login_data->id,
			'user_name'  => $login_data->username,
			'user_email'  => $login_data->email,
			'full_name' => $login_data->full_name,
			'trade_type' => $login_data->trade_type

		);
		$this->session->set_userdata($session_data);
		redirect(base_url() . 'wallet');
	}
	
	public function checkAdmin()
	{
		$pass = MD5($this->input->post('password'));
		$result = $this->db->get_where('admin_login', array('username' => 'admin', 'password' => $pass))->result_array();
		if(sizeof($result) > 0){
			echo 'true';
		}else{
			echo 'false';
		}
	}

	function logout()
	{
		$this->session->unset_userdata('admin_id');
		$this->session->unset_userdata('admin_username');
		$this->session->unset_userdata('admin_email');
		//$this->session->sess_destroy();

		redirect(base_url() . 'd46aa6f5f9401257b243ce2dfe442b9c');
	}
}
