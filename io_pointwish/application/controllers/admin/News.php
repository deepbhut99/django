<?php
defined('BASEPATH') or exit('No direct script access allowed');
ini_set('memory_limit', -1);
ini_set('max_execution_time', 300);
class News extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->_adminLoginCheck();
    }

    function index()
    {
        $data['title'] = 'News';
        $data['news'] = $this->common->getWhereFromLast('pwt_news_', array());
        $this->load->view('admin/news/index', $data);
    }

    function add_news()
    {
        /*******Upload ID***Start***/
        if (empty($_FILES['file']['name'])) {
            $datas['message'] = "Please upload a document";
            $datas['success'] = false;
            goto next;
        } else {
            $config['upload_path'] = './uploads/news/';
            $config['allowed_types'] = '*';
            $config['max_size'] = 1024 * 4; //  here it is 5 MB
            $config['encrypt_name'] = TRUE;
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('file')) {
                $datas['message'] = $this->upload->display_errors();
                $datas['success'] = false;
                goto next;
            } else {
                $data = $this->upload->data();
                $ID_proof = $data["file_name"];
                $insert_data = array(
                    'document' => $ID_proof,
                    'created_time' => date('Y-m-d'),
                );
                $this->common->insert_data('pwt_news_', $insert_data);
            }
            $datas['message'] = "Uploaded Successfully";
            $datas['success'] = true;
        }
        next:
        echo json_encode($datas);
        /*******Upload ID***End***/
    }

    function delete_news()
    {
        $news = $this->common->getSingle('pwt_news_', array('id' => $this->input->post('id')));
        if ($news) {
            $file = "./uploads/news/" . $news->document;
            unlink($file);
            $this->db->where('id', $this->input->post('id'))->delete('pwt_news_');
            $data['success'] = true;
            $data['message'] = "Deleted Successfully";
        } else {
            $data['success'] = false;
            $data['message'] = "Not exist";
        }
        echo json_encode($data);
    }

    function promotional_mail()
    {
        $username = $this->input->post('username');
        $user = $this->common->getSingle('pwt_users', array('username' => $username));
        if ($user) {
            /********Mail for activity ***Start****/
            $from_email = ADMIN_NO_REPLY_EMAIL; //ADMIN_INFO_EMAIL
            $to_email = $user->email;
            $subject = 'Promotional  Mail';
            $WEB_URL = base_url();
            $htmlContent = file_get_contents("./mail_html/promotional.html");
            $tpl2 = str_replace('{{WEB_URL}}', $WEB_URL, $htmlContent);
            $tpl3 = str_replace('{{USERNAME}}', $user->username, $tpl2);
            $tpl4 = str_replace('{{TOTAL}}', $this->input->post('total'), $tpl3);
            $tpl5 = str_replace('{{ACTIVE}}', $this->input->post('active'), $tpl4);
            $tpl6 = str_replace('{{pwt}}', $this->input->post('pwt'), $tpl5);
            $tpl7 = str_replace('{{USDT}}', $this->input->post('usdt'), $tpl6);
            $message_html = str_replace('{{DATE}}', $this->input->post('date'), $tpl7);

            // PHPMailer object
            $mail = $this->phpmailer_lib->load();
            // SMTP configuration
            $mail->isSMTP();
            $mail->Host     = 'smtp.gmail.com';
            $mail->SMTPAuth = true;
            $mail->Username = ADMIN_NO_REPLY_EMAIL;
            $mail->Password = ADMIN_NO_REPLY_PWD;
            $mail->SMTPSecure = 'ssl';
            $mail->Port     = 465;
            $mail->setFrom($from_email, 'Earnfinex');
            $mail->addReplyTo($from_email, 'Earnfinex');
            // Add a recipient
            $mail->addAddress($to_email);
            $mail->Subject = $subject;
            $mail->isHTML(true);
            $mail->Body = $message_html;

            if (!$mail->send()) {
                $result['message'] = $mail->ErrorInfo;
                $result['success'] = false;
            } else {
                $result['success'] = true;
                $result['message'] = "Mail sent successfully";
                $mailC = $this->common->getSingle('pwt_mailcount', array('date' => date('Y-m-d')));
                if ($mailC) {
                    $update_data = array(
                        'count' => $mailC->count + 1
                    );
                    $this->common->update_data('pwt_mailcount', array('id' => $mailC->id), $update_data);
                } else {
                    $insert_data = array(
                        'date' => date('Y-m-d'),
                        'count' => 1
                    );
                    $this->common->insert_data('pwt_mailcount', $insert_data);
                }
            }
        } else {
            $result['message'] = "User not exist";
            $result['success'] = false;
        }
        echo json_encode($result);
    }

    function common_promo_mail()
    {
        $idFrom = $this->input->post('idfrom');
        $idTo = $this->input->post('idto');

        $users = $this->common->get_range_data($idFrom, $idTo);
        array_push($users, array('username' => 'Admin', 'email'=>"earnfinex@gmail.com") );
        $mailCount = 0;
        foreach ($users as $user) {
            /********Mail for activity ***Start****/
            $from_email = ADMIN_NO_REPLY_EMAIL; //ADMIN_INFO_EMAIL
            $to_email = $user['email'];
            $subject = 'General  Mail';
            $htmlContent = file_get_contents("./mail_html/general.html");
            $tpl1 = str_replace('{{USERNAME}}', $user['username'], $htmlContent);
            $tpl2 = str_replace('{{DATE}}', $this->input->post('date'), $tpl1);
            $message_html = str_replace('{{COMMENTS}}', $this->input->post('comments'), $tpl2);

            // PHPMailer object
            $mail = $this->phpmailer_lib->load();
            // SMTP configuration
            $mail->isSMTP();
            $mail->Host     = 'smtp.gmail.com';
            $mail->SMTPAuth = true;
            $mail->Username = ADMIN_NO_REPLY_EMAIL;
            $mail->Password = ADMIN_NO_REPLY_PWD;
            $mail->SMTPSecure = 'ssl';
            $mail->Port     = 465;
            $mail->setFrom($from_email, 'Earnfinex');
            $mail->addReplyTo($from_email, 'Earnfinex');
            // Add a recipient
            $mail->addAddress($to_email);
            $mail->Subject = $subject;
            $mail->isHTML(true);
            $mail->Body = $message_html;
            if ($mail->send()) {
                $mailCount += 1;
            }
        }
        $mailC = $this->common->getSingle('pwt_mailcount', array('date' => date('Y-m-d')));
        if ($mailC) {
            $update_data = array(
                'count' => $mailC->count + $mailCount
            );
            $this->common->update_data('pwt_mailcount', array('id' => $mailC->id), $update_data);
        } else {
            $insert_data = array(
                'date' => date('Y-m-d'),
                'count' => $mailCount
            );
            $this->common->insert_data('pwt_mailcount', $insert_data);
        }
        $result['success'] = true;
        $result['message'] = "Mail sent successfully";
        echo json_encode($result);
    }

    public function csv_promo_mail()
    {
        $path = FCPATH . "uploads/promotional/";

        $config['upload_path'] = $path;
        $config['allowed_types'] = 'csv|xlsx';
        $config['max_size'] = 1024000;
        $this->upload->initialize($config);

        if (!$this->upload->do_upload('csvfile')) {
            $result['message'] = $this->upload->display_errors();
            $result['success'] = false;
            goto next;
        } else {

            $file_data = $this->upload->data();
            $file_path = base_url() . "uploads/promotional/" . $file_data['file_name'];

            // Load CSV reader library
            $this->load->library('Csvimport');
            
            $csv_array = $this->csvimport->get_array($file_path);
            var_dump($csv_array);
            exit(0);
            if ($csv_array) {
                foreach ($csv_array as $row) {
                    /********Mail for activity ***Start****/
                    $from_email = ADMIN_NO_REPLY_EMAIL; //ADMIN_INFO_EMAIL
                    $to_email = $row['email'];
                    $subject = 'Promotional  Mail';
                    $htmlContent = file_get_contents("./mail_html/promotional.html");
                    $tpl3 = str_replace('{{USERNAME}}', $row['username'], $htmlContent);
                    $tpl4 = str_replace('{{TOTAL}}', $row['toal'], $tpl3);
                    $tpl5 = str_replace('{{ACTIVE}}', $row['active'], $tpl4);
                    $tpl6 = str_replace('{{pwt}}', $row['pwt'], $tpl5);
                    $tpl7 = str_replace('{{USDT}}', $row['usdt'], $tpl6);
                    $message_html = str_replace('{{DATE}}', $row['date'], $tpl7);

                    // PHPMailer object
                    $mail = $this->phpmailer_lib->load();
                    // SMTP configuration
                    $mail->isSMTP();
                    $mail->Host     = 'smtp.gmail.com';
                    $mail->SMTPAuth = true;
                    $mail->Username = ADMIN_NO_REPLY_EMAIL;
                    $mail->Password = ADMIN_NO_REPLY_PWD;
                    $mail->SMTPSecure = 'ssl';
                    $mail->Port     = 465;
                    $mail->setFrom($from_email, 'Earnfinex');
                    $mail->addReplyTo($from_email, 'Earnfinex');
                    // Add a recipient
                    $mail->addAddress($to_email);
                    $mail->Subject = $subject;
                    $mail->isHTML(true);
                    $mail->Body = $message_html;
                    $mail->send();
                    $result['success'] = true;
                    $result['message'] = "Mail sent successfully";
                }
            } else {
                $result['message'] = "Error Occured";
                $result['success'] = false;
            }
        }
        next:
        echo json_encode($result);
    }
}
