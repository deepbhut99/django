<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ticket extends MY_Controller
{

	function __construct()
	{
		parent::__construct();

		/****checks it is login or not start****/
		$this->_adminLoginCheck();
		/****checks it is login or not End****/

		$this->load->model('admin/Ticket_model', 'ticket');
	}

	function index()
	{
		$data['title'] = 'Ticket';

		$data['total_ticket'] = $this->ticket->getAllTotalTicketCount();

		$data['total_pending_ticket'] = $this->ticket->getAllPendingTicketCount();
		$data['total_inprocess_ticket'] = $this->ticket->getAllInProcessTicketCount();
		$data['total_close_ticket'] = $this->ticket->getAllCloseTicketCount();

		$this->load->view('admin/ticket/ticket_list', $data);
	}

	function tickets_list()
	{
		$type = $this->input->get('type', true);

		$type_arr = array('open', 'process', 'close');

		if (empty($type) || (!empty($type) && !in_array($type, $type_arr))) {
			redirect(base_url());
		}

		if ($type == 'all') {
			$data['title'] = 'All Tickets List';
		} else if ($type == 'open') {
			$data['title'] = 'Pending Tickets List';
		} else if ($type == 'process') {
			$data['title'] = 'In Process Tickets List';
		} else if ($type == 'close') {
			$data['title'] = 'Complete Tickets List';
		}

		if (!empty($_GET['offset'])) {
			$offset = $_GET['offset'];
		} else {
			$offset = 1;
		}

		$per_page = 100;

		$start_limit = ($per_page * $offset) - $per_page;


		$data['data_result'] = $this->ticket->getTicketListByType($start_limit, $per_page);
		$total_records = $this->ticket->getTicketListByTypeCount();

		$config['base_url'] = base_url() . 'admin/ticket/tickets_list?type=' . $type;
		$config['total_rows'] = $total_records;
		$config['per_page'] = $per_page;
		//$config["uri_segment"] = 3;
		$config['query_string_segment'] = 'offset';
		$config['page_query_string'] = true;
		// custom paging configuration
		//$config['num_links'] = 4;
		$config['use_page_numbers'] = TRUE;
		$config['reuse_query_string'] = TRUE;


		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';


		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li class="firstlink page-item page-link">';
		$config['first_tag_close'] = '</li>';

		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li class="lastlink page-item page-link">';
		$config['last_tag_close'] = '</li>';

		$config['next_link'] = 'Next';
		$config['next_tag_open'] = '<li class="nextlink page-item page-link">';
		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = 'Prev';
		$config['prev_tag_open'] = '<li class="prevlink page-item page-link">';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active page-item"><a href="#" class="page-link">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li class="numlink page-item page-link">';
		$config['num_tag_close'] = '</li>';

		$this->pagination->initialize($config);

		$data["links"] = $this->pagination->create_links();

		$data["total_records"] = $total_records;

		$data['offset'] = $offset;

		if ($type == 'all') {
			$this->load->view('admin/ticket/all_tickets_list', $data);
		} else if ($type == 'open') {
			$this->load->view('admin/ticket/pending_tickets_list', $data);
		} else if ($type == 'process') {
			$this->load->view('admin/ticket/inprocess_tickets_list', $data);
		} else if ($type == 'close') {
			$this->load->view('admin/ticket/complete_tickets_list', $data);
		}
	}

	function view_ticket()
	{
		$ticket_id = $this->input->get('ticket_id', true);

		$ticket_data = $this->common->getSingle('pwt_tickets', array('ticket_id' => $ticket_id));

		if (empty($ticket_data)) {
			redirect(base_url());
		}

		$data['title'] = 'View Ticket';

		$data['data_result'] = $this->common->getSingle('pwt_tickets', array('ticket_id' => $ticket_id));

		$this->load->view('admin/ticket/view_ticket', $data);
	}

	function view_chat()
	{
		$ticket_id = $this->input->get('ticket_id', true);

		$ticket_data = $this->common->getSingle('pwt_tickets', array('ticket_id' => $ticket_id));

		if (empty($ticket_data)) {
			redirect(base_url());
		}

		$data['title'] = 'View Conversation';

		$data['data_result'] = $this->ticket->getMyTicketChatList();

		$this->load->view('admin/ticket/view_chat', $data);
	}

	function submit_ticket_reply()
	{
		if ($_POST['description'] && $_POST['ticket_id']) {

			$ticket_id = $this->input->post('ticket_id', true);
			$description = $this->input->post('description', true);

			$ticket_data = $this->common->getSingle('pwt_tickets', array('ticket_id' => $ticket_id));
			$imagename = '';
			if (!empty($_FILES['image']['name'])) {
				$config['upload_path'] = './uploads/ticket_attachment/';
				$config['allowed_types'] = '*';
				$config['max_size'] = 1024 * 2.5; //  here it is 5 MB
				$config['encrypt_name'] = TRUE;

				$this->upload->initialize($config);
				if (!$this->upload->do_upload('image')) {
					$this->session->set_flashdata('error_message', 'Image have an problem while uploading');
					redirect($_SERVER['HTTP_REFERER']);
				} else {
					$data = $this->upload->data();
					$imagename = $data['file_name'];
				}
			}

			$insert_data = array(
				'user_id' => $ticket_data->user_id,
				'ticket_id' => $ticket_id,
				'description' => $description,
				'from' => 'admin',
				'image' => $imagename,
				'created_datetime' => date('Y-m-d H:i:s'),
				'modified_datetime' => date('Y-m-d H:i:s'),
			);

			$this->common->insert_data('pwt_tickets_chat', $insert_data);
			
			/*******Notification **Start****/
			$insert_note = array(
				'user_id' => $ticket_data->user_id,
				'type' => 'ticket',
				'target' => base_url('ticket'),
				'detail' => $ticket_id . ' - Admin replied',
				'date' => date('Y-m-d H:i:s'),
			);
			$this->common->insert_data('pwt_notification', $insert_note);
			/*******Notification **Start****/

			$this->session->set_flashdata('error_message', 'Sent reply successfully !');
			redirect(base_url() . 'admin/ticket/view_chat?ticket_id=' . $ticket_id);
		} else {
			redirect(base_url());
		}
	}

	function close_ticket()
	{
		if ($_POST) {
			$ticket_id = $this->input->post('id', true);

			$update_data = array(
				'status' => 'process'
			);
			$this->common->update_data('pwt_tickets', array('ticket_id' => $ticket_id), $update_data);

			echo 1;
			exit;
		} else {
			redirect(base_url());
		}
	}

	function close_all_ticket()
	{
		$offset = $this->input->post('offset');
		$per_page = 100;
		$start_limit = ($per_page * $offset) - $per_page;
		$data_result = $this->ticket->getTicketListByType($start_limit, $per_page, 'open');
		foreach ($data_result as $row) {
			$arrayIds[] = array(
				'id' => $row->id, //this is current id       
				'status' => 'process',
			);
		}
		$this->db->update_batch('pwt_tickets', $arrayIds, 'id'); //pass the columnName in the third parameter
		$this->session->set_flashdata('success_message', 'Deleted Successfully');
		echo true;
		exit;
	}
}
