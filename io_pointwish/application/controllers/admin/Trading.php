<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Trading extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        /****checks it is login or not start****/
        $this->_adminLoginCheck();
        /****checks it is login or not End****/

        $this->load->model('Trading_Model', 'trade');
        $this->load->model('Agency_model', 'agency');

    }

    function index()
    {
        $data['title'] = 'Dashboard';
        // $data['result'] = $this->db->select('bids.*, pwt_users.username')->join('pwt_users', 'pwt_users.id = bids.user', 'left')->order_by('id', 'desc')->where(array("from_unixtime(timestamp/1000, '%Y %D %M') = " => "from_unixtime(unix_timestamp(), '%Y %D %M')", "bids.type" => 0))->get('bids')->result_array();
        $data['result'] = $this->db->select('bids.*, pwt_users.username, pwt_users.login_country, pwt_users.my_os')->join('pwt_users', 'pwt_users.id = bids.user', 'left')->order_by('bids.id', 'desc')->where("from_unixtime(timestamp/1000, '%Y %D %M') = from_unixtime(unix_timestamp(), '%Y %D %M') and bids.type = 0")->get('bids')->result_array();
        // $data['result'] = $this->trade->get_todayall_bids();
        // var_dump($this->db->last_query());
        // exit(0);
        $data['settings'] = $this->common->getSingle('admin_login', array());
        $this->load->view('admin/trading/bids', $data);
    }
    
    function export_bids()
    {
        $start = $this->input->post('startdate');
        $end = $this->input->post('enddate');
        if ($end) {
            $where["DATE_FORMAT(FROM_UNIXTIME(timestamp/1000), '%d-%m-%Y') >="] = $start;
            $where["DATE_FORMAT(FROM_UNIXTIME(timestamp/1000), '%d-%m-%Y') <="] = $end;
            $where['bids.type ='] = 0;
        }
        $result = $this->db->select('bids.*, pwt_users.username')->join('pwt_users', 'pwt_users.id = bids.user', 'left')->order_by('bids.id', 'desc')->where($where)->get('bids')->result_array();
        $count = 1;
        foreach ($result as $data) {
            $export[] = array(
                'SrNo' => $count++,
                'Coin' => $data['coin'],
                'User ID' => $data['username'],
                'Win Ratio' => $data['winRatio'],
                'LastWin Ratio' => $data['lastWinRatio'],
                'Date' => date('d-m-Y h:i:s', $data['timestamp'] / 1000),
                'Low' => $data['low'],
                'High' => $data['high'],
                'Base Amount' => $data['value'],
                'Closing Value' => $data['closeVal'],
                'M-Closing Value' => $data['mCloseVal'],
                'M Value' => ($data['closeVal'] != $data['mCloseVal'] ? abs((float)$data['value'] - (float)$data['mCloseVal']) : 0),
                'Prediction' => $data['growth'] == '1' ? 'Up' : 'Down',
                'Trade Amount' => $data['total_amount'],                
                'Profit' => $data['result'] == 1 ? $data['total_amount'] + ($data['total_amount'] * 95 / 100) : 0
            );
        }
        $filename = "Bids-".date('Y.m.d').'_'.date("H-i").'_earnfinex';
        header("Content-type: application/csv");
        header("Content-Disposition: attachment; filename=\"$filename".".csv\"");
        header("Pragma: no-cache");
        header("Expires: 0");
        $report = fopen('php://output', 'w');   
        $header = array("SrNo", "Coin", "User", "Win", "LastWin", "Date", "Low", "High", "Base Amount", "Closing Value", "M-Closing Value", "M-Value", "Prediction", "Trade Amount", "Profit");    
        fputcsv( $report, $header);
        foreach ($export as $row) {
            fputcsv($report, $row);
        }                
        fclose($report);                
        // $res['success'] = true;
        // $res['message'] = "Exported Successfully";
        // echo json_encode($res);
    }
    
    function change_status() {
        $status = $this->input->post('status');
        $this->db->where('id', 1)->update('admin_login', array('auto_refresh' => $status));
        echo true;
        exit;
    }
    
    function winner()
    {
        $data['title'] = 'Trading Winner';    
        $data['daily'] = $this->trade->get_current_winners('daily');
        $data['weekly'] = $this->trade->get_current_winners('weekly');
        $data['monthly'] = $this->trade->get_current_winners('monthly');
        $data['populars'] = $this->trade->get_popular_users();
        $this->load->view('admin/trading/winner', $data);
    }
    
    function agency() {
        $url = $this->uri->segment(1);
        if($url == 'trade-agency') {
            $data['title'] = "Agency";
            $agencies = $this->common->getWhereFromLast('pwt_users', array('is_agency' => 1, 'is_delete' => 1));
            $users = [];
            foreach($agencies as $row) {
                $network = $this->agency->get_user_matrix($row['username']);
                $report = $this->get_user_level($row['id']);
                $users[] = array(
                    'username' => $row['username'],
                    'total_users' => $network['totalUsers'],
                    'total_agency' => $network['totalAgency'],
                    'level' => $report['level'],
                    'business' => $report['totalTrade'] ? $report['totalTrade'] : 0,
                    'commission' => $this->agency->get_total_commission(0, $row['id']),
                    'date' => $row['upgrade_agency']
    
                );
            }
            $data['users'] = $users;        
            $this->load->view('admin/trading/agency', $data);
        } else {
            redirect('404');
        }
    }

    function get_user_level($user)
    {
        $level = 1;
        $tree = $this->common->getSingle('pwt_agency_tree', array('sponsor_id' => $user));
        $totalTrade = $this->agency->get_count_bids($tree->tree, 2);
        if ($tree->tree_count >= 8 && $totalTrade >= 64000) {
            $level = 7;
            goto next;
        } else if ($tree->tree_count >= 7 && $totalTrade >= 32000) {
            $level = 6;
            goto next;
        } else if ($tree->tree_count >= 6 && $totalTrade >= 16000) {
            $level = 5;
            goto next;
        } else if ($tree->tree_count >= 5 && $totalTrade >= 8000) {
            $level = 4;
            goto next;
        } else if ($tree->tree_count >= 4 && $totalTrade >= 4000) {
            $level = 3;
            goto next;
        } else if ($tree->tree_count >= 3 && $totalTrade >= 2000) {
            $level = 2;
            goto next;
        }
        next:
        $data['level'] = $level;
        $data['tree_count'] = $tree->tree_count;
        $data['tree'] = $tree->tree;
        $data['totalTrade'] = $totalTrade;
        return $data;
    }
}
