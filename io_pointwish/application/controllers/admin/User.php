<?php
defined('BASEPATH') or exit('No direct script access allowed');
set_time_limit(0);
ini_set('max_execution_time', 0);
ini_set('memory_limit', -1);
class User extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        /****checks it is login or not start****/
        $this->_adminLoginCheck();
        /****checks it is login or not End****/

        $this->load->model('admin/Users_model', 'users');
    }

    function total_users()
    {
        $data['title'] = 'Total Users';

        if (!empty($_GET['offset'])) {
            $offset = $_GET['offset'];
        } else {
            $offset = 1;
        }

        $per_page = 100;

        $start_limit = ($per_page * $offset) - $per_page;


        $data['data_result'] = $this->users->getTotalUserList($start_limit, $per_page);
        $total_records = $this->users->getTotalUserListCount();

        $config['base_url'] = base_url() . 'admin/user/total_users';
        $config['total_rows'] = $total_records;
        $config['per_page'] = $per_page;
        //$config["uri_segment"] = 3;
        $config['query_string_segment'] = 'offset';
        $config['page_query_string'] = true;
        // custom paging configuration
        //$config['num_links'] = 4;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;


        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';


        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li class="firstlink page-item page-link">';
        $config['first_tag_close'] = '</li>';

        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li class="lastlink page-item page-link">';
        $config['last_tag_close'] = '</li>';

        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li class="nextlink page-item page-link">';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = 'Prev';
        $config['prev_tag_open'] = '<li class="prevlink page-item page-link">';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active page-item"><a href="#" class="page-link">';
        $config['cur_tag_close'] = '</a></li>';

        $config['num_tag_open'] = '<li class="numlink page-item page-link">';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $data["links"] = $this->pagination->create_links();

        $data["total_records"] = $total_records;

        $this->load->view('admin/users/total_users', $data);
    }
    

    function view_user()
    {
        $data['title'] = 'User Details';

        $id = $this->input->get('id');

        if (!$id) {
            redirect(base_url());
            die;
        }

        $data['user_data'] = $this->common->getSingle('pwt_users', array('id' => $id));

        if (empty($data['user_data'])) {
            redirect(base_url());
            die;
        }

        $this->load->view('admin/users/view_user', $data);
    }

    function edit_user_profile()
    {
        $data['title'] = 'Edit User Profile';

        $token = $this->input->get('token');
        $data['user_data'] = $this->common->getSingle('pwt_users', array('token' => $token));

        if (empty($data['user_data'])) {
            redirect(base_url() . 'admin');
        }

        $this->form_validation->set_rules('full_name', 'Name', 'required');
        $this->form_validation->set_rules('country', 'Country', 'required');
        // $this->form_validation->set_rules('country_code', 'Country Code', 'required');
        // $this->form_validation->set_rules('mobile', 'Mobile', 'required');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('admin/users/edit_user_profile', $data);
        } else {

           

            $update_data = array(
                'full_name' => $this->input->post('full_name', true),                
                'country' => $this->input->post('country', true),
                'country_code' => $this->input->post('country_code', true),
                'mobile' => $this->input->post('mobile', true),
                'address' => $this->input->post('address', true),
                'modified_time' => date('Y-m-d H:i:s'),
                'email' => $this->input->post('email', true)
            );

            $this->common->update_data('pwt_users', array('token' => $token), $update_data);

            $this->session->set_flashdata('error_message', 'Profile updated successfully !');

            redirect(base_url() . 'admin/user/edit_user_profile?token=' . $token);
        }
    }


    function paid_users()
    {
        $data['title'] = 'Paid Users';

        if (!empty($_GET['offset'])) {
            $offset = $_GET['offset'];
        } else {
            $offset = 1;
        }

        $per_page = 10;

        $start_limit = ($per_page * $offset) - $per_page;


        $data['data_result'] = $this->users->getTotalPaidUserList($start_limit, $per_page);
        $total_records = $this->users->getTotalPaidUserListCount();

        $config['base_url'] = base_url() . 'admin/user/paid_users';
        $config['total_rows'] = $total_records;
        $config['per_page'] = $per_page;
        //$config["uri_segment"] = 3;
        $config['query_string_segment'] = 'offset';
        $config['page_query_string'] = true;
        // custom paging configuration
        //$config['num_links'] = 4;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;


        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';


        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li class="firstlink page-item page-link">';
        $config['first_tag_close'] = '</li>';

        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li class="lastlink page-item page-link">';
        $config['last_tag_close'] = '</li>';

        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li class="nextlink page-item page-link">';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = 'Prev';
        $config['prev_tag_open'] = '<li class="prevlink page-item page-link">';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active page-item"><a href="#" class="page-link">';
        $config['cur_tag_close'] = '</a></li>';

        $config['num_tag_open'] = '<li class="numlink page-item page-link">';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $data["links"] = $this->pagination->create_links();

        $data["total_records"] = $total_records;

        $this->load->view('admin/users/paid_users', $data);
    }

    function unpaid_users()
    {
        $data['title'] = 'Unpaid Users';

        if (!empty($_GET['offset'])) {
            $offset = $_GET['offset'];
        } else {
            $offset = 1;
        }

        $per_page = 10;

        $start_limit = ($per_page * $offset) - $per_page;


        $data['data_result'] = $this->users->getTotalUnPaidUserList($start_limit, $per_page);
        $total_records = $this->users->getTotalUnPaidUserListCount();

        $config['base_url'] = base_url() . 'admin/user/unpaid_users';
        $config['total_rows'] = $total_records;
        $config['per_page'] = $per_page;
        //$config["uri_segment"] = 3;
        $config['query_string_segment'] = 'offset';
        $config['page_query_string'] = true;
        // custom paging configuration
        //$config['num_links'] = 4;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;


        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';


        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li class="firstlink page-item page-link">';
        $config['first_tag_close'] = '</li>';

        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li class="lastlink page-item page-link">';
        $config['last_tag_close'] = '</li>';

        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li class="nextlink page-item page-link">';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = 'Prev';
        $config['prev_tag_open'] = '<li class="prevlink page-item page-link">';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active page-item"><a href="#" class="page-link">';
        $config['cur_tag_close'] = '</a></li>';

        $config['num_tag_open'] = '<li class="numlink page-item page-link">';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $data["links"] = $this->pagination->create_links();

        $data["total_records"] = $total_records;

        $this->load->view('admin/users/unpaid_users', $data);
    }

    function active_users_today()
    {
        $data['title'] = "Today's Active Users";

        if (!empty($_GET['offset'])) {
            $offset = $_GET['offset'];
        } else {
            $offset = 1;
        }

        $per_page = 10;

        $start_limit = ($per_page * $offset) - $per_page;


        $data['data_result'] = $this->users->getTodaysActiveUserList($start_limit, $per_page);
        $total_records = $this->users->getTodaysActiveUserListCount();

        $config['base_url'] = base_url() . 'admin/user/active_users_today';
        $config['total_rows'] = $total_records;
        $config['per_page'] = $per_page;
        //$config["uri_segment"] = 3;
        $config['query_string_segment'] = 'offset';
        $config['page_query_string'] = true;
        // custom paging configuration
        //$config['num_links'] = 4;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;


        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';


        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li class="firstlink page-item page-link">';
        $config['first_tag_close'] = '</li>';

        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li class="lastlink page-item page-link">';
        $config['last_tag_close'] = '</li>';

        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li class="nextlink page-item page-link">';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = 'Prev';
        $config['prev_tag_open'] = '<li class="prevlink page-item page-link">';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active page-item"><a href="#" class="page-link">';
        $config['cur_tag_close'] = '</a></li>';

        $config['num_tag_open'] = '<li class="numlink page-item page-link">';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $data["links"] = $this->pagination->create_links();

        $data["total_records"] = $total_records;

        $this->load->view('admin/users/active_users_today', $data);
    }
        
    function change_status() {
        $status = $this->input->post('status');
        $userid = $this->input->post('id');
        $this->db->where('id', $userid)->update('pwt_users', array('is_delete' => $status));
        echo true;
        exit;
    }
    
    function get_pwt_users($param1='')
    {
        $data['title'] = 'Total Users';
        if($param1) {
            $orderby = $param1;
        } else {
            $orderby = 'pwt';
        }
        if (!empty($_GET['offset'])) {
            $offset = $_GET['offset'];
        } else {
            $offset = 1;
        }

        $per_page = 500;

        $start_limit = ($per_page * $offset) - $per_page;

        $result = $this->users->get_pwt_users($start_limit, $per_page, $orderby);
        foreach($result as $row) {
            $direct = $this->users->count_referral_users($row->username);
            $data_result[] = array(
                'id' => $row->id,
                'username' => $row->username,
                'country' => $row->country,
                'email' => $row->email,
                'wallet_amount' => $row->wallet_amount,
                'pwt_wallet' => $row->pwt_wallet,
                'unlocked_pwt' => $row->unlocked_pwt,
                'created_time' => $row->created_time,
                'sponsor' => $row->sponsor,
                'is_delete' => $row->is_delete,
                'total_referrals' => $direct['total_users'],
                'active_referrals' => $direct['active_users'],
                // 'referral_income' => $this->users->count_referral_income($row->id)
            );
        }
        $data['data_result'] = $data_result;
        $total_records = $this->users->getTotalActiveUserListCount();
        $config['base_url'] = base_url() . 'pwt-users/' . $orderby;
        $config['total_rows'] = $total_records;
        $config['per_page'] = $per_page;
        //$config["uri_segment"] = 3;
        $config['query_string_segment'] = 'offset';
        $config['page_query_string'] = true;
        // custom paging configuration
        //$config['num_links'] = 4;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;


        $config['full_tag_open'] = '<ul class="pagination m-1">';
        $config['full_tag_close'] = '</ul>';


        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li class="firstlink page-item page-link">';
        $config['first_tag_close'] = '</li>';

        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li class="lastlink page-item page-link">';
        $config['last_tag_close'] = '</li>';

        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li class="nextlink page-item page-link">';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = 'Prev';
        $config['prev_tag_open'] = '<li class="prevlink page-item page-link">';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active page-item"><a href="#" class="page-link">';
        $config['cur_tag_close'] = '</a></li>';

        $config['num_tag_open'] = '<li class="numlink page-item page-link">';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $data["links"] = $this->pagination->create_links();
        $data['orderby'] = $orderby;
        $data["total_records"] = $total_records;
        $this->load->view('admin/users/pwt_users', $data);
    }
    
    function export_user()
    {
        ini_set('max_execution_time', 0); 
		ini_set('memory_limit','2048M');     
        $start = $this->input->post('startdate');
        $end = $this->input->post('enddate');
        $result = $this->users->get_export_users($start, $end);
        $count = 1;
        
        foreach ($result as $data) {
            // $direct = $this->users->count_referral_users($data->username);
            $export[] = array(
                'Sr.No' => $count++,
                'Username' => $data->username,
                // 'Total Referrals' => $direct['total_users'],
                // 'Active Referrals' => $direct['active_users'],
                // 'Referral Income' => $this->users->count_referral_income($data->id),
                'Email' => $data->email,
                'Sponsor' => $data->sponsor,
                'Wallet' => $data->wallet_amount,
                'Locked pwt' => $data->pwt_wallet,
                'Unlocked pwt' => $data->unlocked_pwt,
                'Joining Date' => date('d-m-Y h:i:s', strtotime($data->created_time)),
            );
        }
        $filename = "Users-" . date('Y.m.d') . '_' . date("H-i") . '_earnfinex';
        header("Content-type: application/csv");
        header("Content-Disposition: attachment; filename=\"$filename" . ".csv\"");
        header("Pragma: no-cache");
        header("Expires: 0");
        $report = fopen('php://output', 'w');
        $header = array("SrNo", "Username", "Email", "Sponsor", "Wallet", "Locked pwt", "Unlocked pwt", "Joining Date");
        // $header = array("SrNo", "Username", "Total", "Active", "Email", "Sponsor", "Wallet", "Locked pwt", "Unlocked pwt", "Joining Date");
        fputcsv($report, $header);
        foreach ($export as $row) {
            fputcsv($report, $row);
        }
        fclose($report);
    }
    
    function convert_wallet()
    {        
        $users = $this->common->getWhere('pwt_users', array());
        $insert = [];
        $insert_note = [];
        foreach($users as $row) {
            $pwt = $row->wallet_amount / get_pwt()['current_price'];
            /*******Notification **Start****/
            $insert[] = array(
                'from_user' => $row->id,
                'to_user' => $row->id,
                'log' => 'credit',
                'type' => 'convert',
                'payment_type' => 'pwt',
                'amount' =>  $pwt,
                'balance' =>getWalletAmountByUserID($row->id)[2] + $pwt,
                'txn_id' => '#E' . rand(),
                'message' => "Convert pwt",
                'status' => 1,
                'created_datetime' => date('Y-m-d H:i:s'),
                'modified_datetime' => date('Y-m-d H:i:s'),
            );
            
            /*******Notification **Start****/
            $amount = number_format($row->wallet_amount, 2);
			$insert_note[] = array(
				'user_id' => $row->id,
				'type' => 'convert',
				'target' => base_url('history'),
				'detail' => "Your $amount USDT Converted to pwt",
				'date' => date('Y-m-d H:i:s'),
			);		
			/*******Notification **End****/
        }
        if($insert_note) {
            $this->db->insert_batch('pwt_notification', $insert_note);
        }
        if($insert) {
            $this->db->insert_batch('pwt_user_notification', $insert);
        }
        $this->users->convert_wallet();
        echo true;
    }   
}
