<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Withdraw extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        /****checks it is login or not start****/
        $this->_adminLoginCheck();
        /****checks it is login or not End****/

        /****checks it is admin or sub admin****/
        if ($this->session->type == 2) {
            $this->session->set_flashdata('error_message', "You don't have permission");
            redirect('admin/user/total_users');
        }
        /****checks it is admin or sub admin****/

        $this->load->model('admin/Withdraw_model', 'withdraw');
    }

    function pending_withdrawal()
    {
        $data['title'] = 'Pending Withdrawal';

        if (!empty($_GET['offset'])) {
            $offset = $_GET['offset'];
        } else {
            $offset = 1;
        }

        $per_page = 100;

        $start_limit = ($per_page * $offset) - $per_page;


        $data['data_result'] = $this->withdraw->getAllPendingWithdrawal($start_limit, $per_page);
        $total_records = $this->withdraw->getAllPendingWithdrawalCount();

        $config['base_url'] = base_url() . 'admin/withdraw/pending_withdrawal';
        $config['total_rows'] = $total_records;
        $config['per_page'] = $per_page;
        //$config["uri_segment"] = 3;
        $config['query_string_segment'] = 'offset';
        $config['page_query_string'] = true;
        // custom paging configuration
        //$config['num_links'] = 4;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;


        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';


        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li class="firstlink page-item page-link">';
        $config['first_tag_close'] = '</li>';

        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li class="lastlink page-item page-link">';
        $config['last_tag_close'] = '</li>';

        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li class="nextlink page-item page-link">';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = 'Prev';
        $config['prev_tag_open'] = '<li class="prevlink page-item page-link">';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active page-item"><a href="#" class="page-link">';
        $config['cur_tag_close'] = '</a></li>';

        $config['num_tag_open'] = '<li class="numlink page-item page-link">';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $data["links"] = $this->pagination->create_links();

        $data["total_records"] = $total_records;

        $this->load->view('admin/withdraw/pending_withdrawal', $data);
    }

    function make_payment()
    {
        if ($_POST) {
            $id = $this->input->post('payment_id');
            $paytype = $this->input->post('pay_type');

            $getUserInfo = $this->common->getSingle('pwt_withdrawal', array('id' => $id));

            /***Get user address***/
            $getUserAdd = $this->common->getSingle('pwt_users_bank_details', array('user_id' => $getUserInfo->session_id));
            /***Get user address***/

            $update_data = array(
                'status' => 'G',
                'comments' => $this->input->post('comment'),
                'given_datetime' => date('Y-m-d H:i:s'),
                'pay_address' => $getUserAdd->$paytype
            );
            $this->common->update_data('pwt_withdrawal', array('id' => $id), $update_data);            

            /***pwt_user_notification***/
            $update = array(
                'message' => 'Withdrawal Approved',
                'modified_datetime' => date('Y-m-d H:i:s'),
                'status' => 1
            );
            $this->common->update_data('pwt_user_notification', array('from_user' => $getUserInfo->session_id, 'txn_id' => $getUserInfo->txn_id), $update);           
            /***pwt_user_notification***/
            
            /*******Notification **Start****/
            $insert_note = array(
                'user_id' => $getUserInfo->session_id,
                'type' => 'withdrawal',
                'target' => base_url('history'),
                'detail' => "Withdrawal is approved - $getUserInfo->txn_id",
                'date' => date('Y-m-d H:i:s'),
            );
            $this->common->insert_data('pwt_notification', $insert_note);
            /*******Notification **Start****/
            
            echo 1;
            exit;
        } else {
            redirect(base_url() . 'admin');
        }
    }

    function show_bank_with_other()
    {
        if ($_POST) {
            $user_id = $this->input->post('user_id');

            $bank_data = $this->common->getSingle('pwt_users_bank_details', array('user_id' => $user_id));

            $tbody = '';

            $tbody .= '<tr>';
            $tbody .= '<td colspan="2" align="center"><strong>Payment Details</strong></td>';
            $tbody .= '</tr>';

            $tbody .= '<tr>';
            $tbody .= '<td>BTC Address</td>';
            if (!empty($bank_data->btc_address)) {
                $tbody .= '<td>' . $bank_data->btc_address . '</td>';
            } else {
                $tbody .= '<td>--</td>';
            }
            $tbody .= '<tr>';
            $tbody .= '<td>ETH Address</td>';
            if (!empty($bank_data->eth_address)) {
                $tbody .= '<td>' . $bank_data->eth_address . '</td>';
            } else {
                $tbody .= '<td>--</td>';
            }
            $tbody .= '</tr>';

            $tbody .= '<tr>';
            $tbody .= '<td>LTC Address</td>';
            if (!empty($bank_data->ltc_address)) {
                $tbody .= '<td>' . $bank_data->ltc_address . '</td>';
            } else {
                $tbody .= '<td>--</td>';
            }
            $tbody .= '<tr>';

            $tbody .= '<tr>';
            $tbody .= '<td>TRX Address</td>';
            if (!empty($bank_data->trx_address)) {
                $tbody .= '<td>' . $bank_data->trx_address . '</td>';
            } else {
                $tbody .= '<td>--</td>';
            }
            $tbody .= '<tr>';

            echo $tbody;
            exit;
        } else {
            redirect(base_url() . 'admin');
        }
    }

    function reject_payment()
    {
        if ($_POST) {
            $id = $this->input->post('reject_id');
            $reject_reason = $this->input->post('reject_reason');

            $withdraw_data = $this->common->getSingle('pwt_withdrawal', array('id' => $id));

            $update_data = array(
                'reject_reason' => $reject_reason,
                'reject_datetime' => date('Y-m-d H:i:s'),
                'status' => 'R',
            );

            $amount = $withdraw_data->total_amount;           

            /*******Re add amount in user wallet***Start*****/
            if($withdraw_data->withdrawal_in == "pwt") {
                $u_query = "update pwt_users set `unlocked_pwt` = unlocked_pwt + " . $amount . " where id = '" . $withdraw_data->session_id . "'";
            } else {
                $u_query = "update pwt_users set `wallet_amount` = wallet_amount + " . $amount . " where id = '" . $withdraw_data->session_id . "'";
            }
            $this->db->query($u_query);
            /*******Re add amount in user wallet***End*****/

            $this->common->update_data('pwt_withdrawal', array('id' => $id), $update_data);

            /***pwt_user_notification***/
            $update = array(
                'message' => 'Withdrawal',
                'status' => 3,
                'modified_datetime' => date('Y-m-d H:i:s'),
            );            
            $this->common->update_data('pwt_user_notification', array('from_user' => $withdraw_data->session_id, 'txn_id' => $withdraw_data->txn_id), $update);           
            /***pwt_user_notification***/
            
            /*******Notification **Start****/
            $insert_note = array(
                'user_id' => $withdraw_data->session_id,
                'type' => 'withdrawal',
                'target' => base_url('history'),
                'detail' => "Withdrawal is rejected - $withdraw_data->txn_id",
                'date' => date('Y-m-d H:i:s'),
            );
            $this->common->insert_data('pwt_notification', $insert_note);
            /*******Notification **Start****/
            
            echo 1;
            exit;
        } else {
            redirect(base_url() . 'admin');
        }
    }

    function given_withdrawal()
    {
        $data['title'] = 'Given Withdrawal';

        if (!empty($_GET['offset'])) {
            $offset = $_GET['offset'];
        } else {
            $offset = 1;
        }

        $per_page = 100;

        $start_limit = ($per_page * $offset) - $per_page;


        $data['data_result'] = $this->withdraw->getAllGivenWithdrawal($start_limit, $per_page);
        $total_records = $this->withdraw->getAllGivenWithdrawalCount();

        $config['base_url'] = base_url() . 'admin/withdraw/given_withdrawal';
        $config['total_rows'] = $total_records;
        $config['per_page'] = $per_page;
        //$config["uri_segment"] = 3;
        $config['query_string_segment'] = 'offset';
        $config['page_query_string'] = true;
        // custom paging configuration
        //$config['num_links'] = 4;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;


        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';


        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li class="firstlink page-item page-link">';
        $config['first_tag_close'] = '</li>';

        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li class="lastlink page-item page-link">';
        $config['last_tag_close'] = '</li>';

        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li class="nextlink page-item page-link">';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = 'Prev';
        $config['prev_tag_open'] = '<li class="prevlink page-item page-link">';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active page-item"><a href="#" class="page-link">';
        $config['cur_tag_close'] = '</a></li>';

        $config['num_tag_open'] = '<li class="numlink page-item page-link">';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $data["links"] = $this->pagination->create_links();

        $data["total_records"] = $total_records;

        $this->load->view('admin/withdraw/given_withdrawal', $data);
    }

    function rejected_withdrawal()
    {
        $data['title'] = 'Rejected Withdrawal';

        if (!empty($_GET['offset'])) {
            $offset = $_GET['offset'];
        } else {
            $offset = 1;
        }

        $per_page = 100;

        $start_limit = ($per_page * $offset) - $per_page;


        $data['data_result'] = $this->withdraw->getAllRejectedWithdrawal($start_limit, $per_page);
        $total_records = $this->withdraw->getAllRejectedWithdrawalCount();

        $config['base_url'] = base_url() . 'admin/withdraw/rejected_withdrawal';
        $config['total_rows'] = $total_records;
        $config['per_page'] = $per_page;
        //$config["uri_segment"] = 3;
        $config['query_string_segment'] = 'offset';
        $config['page_query_string'] = true;
        // custom paging configuration
        //$config['num_links'] = 4;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;


        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';


        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li class="firstlink page-item page-link">';
        $config['first_tag_close'] = '</li>';

        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li class="lastlink page-item page-link">';
        $config['last_tag_close'] = '</li>';

        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li class="nextlink page-item page-link">';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = 'Prev';
        $config['prev_tag_open'] = '<li class="prevlink page-item page-link">';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active page-item"><a href="#" class="page-link">';
        $config['cur_tag_close'] = '</a></li>';

        $config['num_tag_open'] = '<li class="numlink page-item page-link">';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $data["links"] = $this->pagination->create_links();

        $data["total_records"] = $total_records;

        $this->load->view('admin/withdraw/rejected_withdrawal', $data);
    }
}
