<?php
require_once APPPATH . '/third_party/googleLib/GoogleAuthenticator.php';
defined('BASEPATH') or exit('No direct script access allowed');

class Account extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        /****checks it is login or not start****/
        $this->_userLoginCheck();
        /****checks it is login or not End****/
        $this->load->model('Document_model', 'document');
        $this->load->model('Dashboard_model', 'dashboard');
        $this->load->model('Notification_model', 'notification');
        $this->load->model('Agency_model', 'agency');
        $this->load->model('Trading_Model', 'trade');
        $this->load->model('Bottrading_model', 'bottreding');
    }

    function index()
    {
        $data['title'] = lang('profile');
        $data['id_data'] = $this->document->getMyDocumentByType('id');
        $data['user_data']  = $this->common->getSingle('pwt_users', array('id' => $this->session->userdata('user_id')));
        $this->load->view('trading/profile/index', $data);
    }

    function history()
    {
        $data['title'] = lang('history');
        $data['activetitle'] = "main-history";
        $data['data_result'] = $this->notification->getMyNotification();
        $this->load->view('trading/history/index', $data);
    }

    function trade()
    {

        $data['title'] = lang('trading');
        $data['start_stop'] =  $this->dashboard->getstartstopbot();
        $this->load->view('trading/trade/index', $data);
    }

    function set_auth_key()
    {
        $ga = new GoogleAuthenticator();
        $secret = $ga->createSecret();
        $qrCodeUrl = $ga->getQRCodeGoogleUrl($this->session->user_email, $secret, 'pwt');
        $update = array(
            'google_auth_code' => $secret
        );
        $check = array(
            'id' => $this->session->user_id
        );
        $result = $this->common->update_data('pwt_users', $check, $update);
        if ($result) {
            $data = array(
                'secret' => $secret,
                'qrCodeUrl' => $qrCodeUrl
            );
        }
        echo json_encode($data);
    }

    function chcek_auth()
    {
        $user = $this->common->getSingle('pwt_users', array('id' => $this->session->userdata('user_id')));
        $secret = $user->google_auth_code;
        $code = $this->input->post('code');
        if ($code) {
            if ($code == $secret) {
                $status = $this->input->post('status');
                if ($status == 'true') {
                    $update = array(
                        'google_auth_status' => 'true',
                        'google_auth_code' => ''
                    );
                } else {
                    $update = array(
                        'google_auth_status' => 'false',
                        'google_auth_code' => ''
                    );
                }
                $check = array(
                    'id' => $this->session->user_id
                );
                $this->common->update_data('pwt_users', $check, $update);
                $data['status'] = $this->input->post('status');
                $data['success'] = true;
                $data['message'] = "Successfully";
            } else {
                $data['success'] = false;
                $data['message'] = "Invalid Code";
            }
        } else {
            $data['success'] = false;
            $data['message'] = "Invalid Code";
        }
        echo json_encode($data);
    }

    function get_auth_status()
    {
        echo json_encode($this->common->getSingle('pwt_users', array('id' => $this->session->userdata('user_id'))));
    }

    function upload_document()
    {
        $this->form_validation->set_rules('fname', 'First Name', 'required');
        $this->form_validation->set_rules('lname', 'Last Name', 'required');
        $this->form_validation->set_rules('idnumber', 'Document ID', 'required');
        // $this->form_validation->set_rules('file', 'File', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error_message', 'All fields are mandatory');
        } else {
            /*******Upload ID***Start***/
            if (empty($_FILES['file']['name'])) {
                $this->session->set_flashdata('error_message', 'Please Upload a document');
            } else {
                $isExist = $this->notification->count_document($this->input->post('idnumber'));
                if ($isExist > 0) {
                    $this->session->set_flashdata('error_message', 'Document already used');
                } else {
                    $config['upload_path'] = './uploads/user_documents/';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size'] = 1024 * 5; //  here it is 5 MB
                    $config['encrypt_name'] = TRUE;

                    $this->upload->initialize($config);

                    if (!$this->upload->do_upload('file')) {
                        $this->session->set_flashdata('error_message', lang('something_wrong_file_upload'));
                        redirect(base_url() . 'account', 'refresh');
                    } else {
                        $data = $this->upload->data();
                        $ID_proof = $data["file_name"];
                        // $this->resize_image($ID_proof);
                    }

                    if (!$this->upload->do_upload('idproof')) {
                        $this->session->set_flashdata('error_message', lang('something_wrong_file_upload'));
                        redirect(base_url() . 'account', 'refresh');
                    } else {
                        $data = $this->upload->data();
                        $ID = $data["file_name"];
                        // $this->resize_image($ID_proof);
                    }

                    $insert_data = array(
                        'user_id' => $this->session->userdata('user_id'),
                        'fname' => $this->input->post('fname'),
                        'lname' => $this->input->post('lname'),
                        'document_id' => $this->input->post('idnumber'),
                        'document_type' => 'id',
                        'id_proof' => $ID,
                        'document_file' => $ID_proof,
                        'created_datetime' => date('Y-m-d H:i:s'),
                        'modified_datetime' => date('Y-m-d H:i:s'),
                    );

                    $this->common->insert_data('pwt_users_document', $insert_data);
                    $this->session->set_flashdata('success_message', 'Uploaded Successfully');
                }
            }
            /*******Upload ID***End***/
        }
        redirect(base_url() . 'kyc');
    }



    private function resize_image($file_name)
    {
        $this->load->library('image_lib');

        $config['image_library'] = 'gd2';
        $config['source_image'] = './uploads/user_documents/' . $file_name;
        $config['create_thumb'] = FALSE;
        $config['maintain_ratio'] = TRUE;
        $config['width']     = 550;
        $config['height']   = 350;

        $this->image_lib->initialize($config);
        $this->image_lib->resize();
        $this->image_lib->clear();
    }

    function wallet()
    {
        $data['title'] = 'Wallet';
        // $this->session->set_userdata('coin', true);
        $data['user_data'] = $this->common->getSingle('pwt_users', array('id' => $this->session->user_id));
        $data['history'] = $this->notification->getLastMyNotification();
        $data['unlocks'] = $this->common->getWhere('pwt_unlock_pwt', array());
        $this->load->view('trading/profile/wallet', $data);
    }

    function affiliate()
    {
        $data['title'] = 'Affiliate';
        $data['activetitle'] = "affiliatepage";
        $data['users'] = $this->dashboard->get_referral_users();
        $data['bot_trade'] =  $this->common->getSingle('pwt_bottrade_tree', array('sponsor_id' => $this->session->user_id));


        // $data['totalPWT'] = $this->dashboard->count_referral_income($this->session->user_id);
        $user_data = $this->common->getSingle('pwt_users', array('id' => $this->session->user_id));

        if ($user_data->is_bottrade == 1) {
            $direct = $this->dashboard->get_referral_users_forbot();
            $bottrading1 = 0;
            foreach ($direct as $row) {
                $bottrading1 += $row['is_bottrade'] == 1 ? 1 : 0;
            }
            $data['bottrading1'] = $bottrading1;
            $report = $this->get_my_level();
            // $data['mylevel'] = $report['level'];
            $data['nextLevel'] = $report['level'] >= 7 ? 7 : $report['level'] + 1;
            $value = $this->get_level_condition($data['nextLevel']);
            $data['mydownline'] = $report['tree_count'] ? $report['tree_count'] : 0;
            $data['total_downline'] = $value['license'];
        }
        $this->load->view('trading/profile/affiliate', $data);
    }
    function get_my_level()
    {
        $level = 1;
        $tree = $this->common->getSingle('pwt_bottrade_tree', array('sponsor_id' => $this->session->user_id));
        $totalTrade = $this->bottreding->get_count_bids($tree->tree, 2);

        $users = $this->dashboard->get_referral_users();
        $totalteamcount = count($users);
        if ($totalteamcount >= 12) {
            $level = 6;
            goto next;
        } else if ($totalteamcount >= 10) {
            $level = 5;
            goto next;
        } else if ($totalteamcount >= 8) {
            $level = 4;
            goto next;
        } else if ($totalteamcount >= 6) {
            $level = 3;
            goto next;
        } else if ($totalteamcount >= 4) {
            $level = 2;
            goto next;
        } else if ($totalteamcount >= 0) {
            $level = 1;
            goto next;
        }
        next:

        $data['level'] =  $tree->level;
        $data['tree_count'] = $tree->tree_count;
        $data['tree'] = $tree->tree;
        $data['totalTrade'] = $totalTrade;
        return $data;
    }

    function get_level_condition($level)
    {
        switch ($level) {

            case 1:
                $data['license'] = 3;
                $data['volume'] = 4;
                break;
            case 2:
                $data['license'] = 9;
                $data['volume'] = 6;
                break;
            case 3:
                $data['license'] = 27;
                $data['volume'] = 8;
                break;
            case 4:
                $data['license'] = 81;
                $data['volume'] = 10;
                break;
            case 5:
                $data['license'] = 243;
                $data['volume'] = 12;
                break;

            default:
                $data['license'] = 0;
                $data['volume'] = 0;
                break;
        }
        return $data;
    }

    function exchange()
    {

        $totalDeposit = $this->dashboard->get_total_deposit();
        $user = $this->common->getSingle('pwt_users', array('id' => $this->session->user_id));
        // if ( $totalDeposit >= 10) {
            $data['title'] = 'Exchange';
            $data['history'] = $this->common->getWhereFromLast('pwt_exchange_history', array('user_id' => $this->session->user_id));
            $data['lists'] = $this->common->getWhereFromLast('pwt_coin_history', array(), 10);
            $data['moblists'] = $this->common->getWhereFromLast('pwt_coin_history', array(), 3);
            $this->load->view('trading/profile/exchange', $data);
        // } else {
        //     $this->session->set_flashdata('error_message', "Please fill the walet!");
        //     redirect(base_url('affiliate'));
        // }
    }

    function btcaddress($str)
    {
        if ($str == '') {
            $this->form_validation->set_message('btcaddress', lang('please_enter_btc_address'));
            return false;
        } else {
            if (preg_match('/^(bc1|[13])[a-zA-HJ-NP-Z0-9]{25,39}$/', $str)) {
                return true;
            } else {
                $this->form_validation->set_message('btcaddress', lang('please_enter_crcr_btc'));
                return false;
            }
        }
    }

    function ethaddress($str)
    {
        if ($str == '') {
            $this->form_validation->set_message('ethaddress', lang('pls_enttr_eth'));
            return false;
        } else {
            if (preg_match('/^(0x)?[0-9a-f]{40}$/i', $str)) {
                return true;
            } else {
                $this->form_validation->set_message('ethaddress', lang('pls_entr_crcr_eth'));
                return false;
            }
        }
    }

    function get_address()
    {
        echo json_encode($this->common->getSingle('pwt_users_bank_details', array('user_id' => $this->session->userdata('user_id'))));
    }

    function set_eth_address()
    {
        $this->form_validation->set_rules('eth_address', 'ETH Address', 'required|callback_ethaddress');
        if ($this->form_validation->run() == FALSE) {
            $data['message'] = lang('pls_entr_crcr_eth');
            $data['success'] = false;
        } else {
            $ethdata = $this->common->getSingle('pwt_users_bank_details', array('user_id' => $this->session->userdata('user_id')));
            $update_data['eth_address'] = $this->input->post('eth_address', true);
            $update_data['modified_datetime'] = date('Y-m-d H:i:s');
            if ($ethdata) {
                $this->common->update_data('pwt_users_bank_details', array('user_id' => $this->session->userdata('user_id')), $update_data);
            } else {
                $update_data['user_id'] = $this->session->userdata('user_id');
                $update_data['created_datetime'] = date('Y-m-d H:i:s');
                $this->common->insert_data('pwt_users_bank_details', $update_data);
            }
            $insert_data = array(
                'userid' => $this->session->user_id,
                'detail' => 'ETH Address',
                'date' => date('Y-m-d H:i:s'),
            );
            $this->common->insert_data('pwt_logs', $insert_data);
            $data['message'] = lang('updated_successully');
            $data['success'] = true;
        }
        echo json_encode($data);
    }

    function set_btc_address()
    {
        $this->form_validation->set_rules('btc_address', 'BTC Address', 'required|callback_btcaddress');
        if ($this->form_validation->run() == FALSE) {
            $data['message'] = lang('please_enter_crcr_btc');
            $data['success'] = false;
        } else {
            $ethdata = $this->common->getSingle('pwt_users_bank_details', array('user_id' => $this->session->userdata('user_id')));
            $update_data['btc_address'] = $this->input->post('btc_address', true);
            $update_data['modified_datetime'] = date('Y-m-d H:i:s');
            if ($ethdata) {
                $this->common->update_data('pwt_users_bank_details', array('user_id' => $this->session->userdata('user_id')), $update_data);
            } else {
                $update_data['user_id'] = $this->session->userdata('user_id');
                $update_data['created_datetime'] = date('Y-m-d H:i:s');
                $this->common->insert_data('pwt_users_bank_details', $update_data);
            }
            $insert_data = array(
                'userid' => $this->session->user_id,
                'detail' => 'BTC Address',
                'date' => date('Y-m-d H:i:s'),
            );
            $this->common->insert_data('pwt_logs', $insert_data);
            $data['message'] = lang('updated_successully');
            $data['success'] = true;
        }
        echo json_encode($data);
    }

    function update_password()
    {
        $data['title'] = 'Change Password';
        $this->form_validation->set_rules('old_password', 'Old password', 'required');
        $this->form_validation->set_rules('new_password', 'New password', 'required');
        $this->form_validation->set_rules('confirm_password', 'Confirm password', 'required|matches[new_password]');
        $user_data = $this->common->getSingle('pwt_users', array('id' => $this->session->userdata('user_id')));

        if ($this->form_validation->run() == FALSE) {
            $data['message'] = validation_errors();
            $data['success'] = false;
        } else {
            if ($user_data->password != MD5($this->input->post('old_password', true))) {
                $data['message'] = lang('pls_enter_crct_old_pwd');
                $data['success'] = false;
            } else {
                $update_data = array(
                    'password' => MD5($this->input->post('new_password', true)),
                    'password_decrypted' => $this->input->post('new_password', true),
                );
                $this->common->update_data('pwt_users', array('id' => $this->session->userdata('user_id')), $update_data);
                $insert_data = array(
                    'userid' => $this->session->user_id,
                    'detail' => 'Change Password',
                    'date' => date('Y-m-d H:i:s'),
                );
                $this->common->insert_data('pwt_logs', $insert_data);
                $data['message'] = lang('pwd_changed_successfully');
                $data['success'] = true;
            }
        }
        echo json_encode($data);
    }

    function make_withdrawal()
    {
        $data['title'] = 'Make Withdrawal';

        $this->form_validation->set_rules('withdraw_amount', 'Amount', 'required');
        $this->form_validation->set_rules('withdraw_type', 'withdrawal Type', 'required');
        $this->form_validation->set_rules('withdraw_address', 'withdrawal Address', 'required');

        if ($this->form_validation->run() == FALSE) {
            $datas['success'] = false;
            $datas['message'] = "Please Fill valid details";
            echo json_encode($datas);
            die;
        } else {
            //Request Single withdrawal at a time
            $isExist = $this->common->getSingleFromLast('pwt_withdrawal', array('session_id' => $this->session->user_id, 'status' => 'P'));
            if ($isExist) {
                $datas['success'] = false;
                $datas['message'] = "Your withdrawal request is already pending";
                goto next;
            }

            $user_id = $this->session->userdata('user_id');
            $withdrawal_in = $this->input->post('withdraw_type', true);
            $amount = (float)$this->input->post('withdraw_amount', true);
            // if (!preg_match('/^\d+$/', $amount)) {
            //     $datas['success'] = false;
            //     $datas['message'] = "Please type correct transaction amount value !";
            //     echo json_encode($datas);
            //     die;
            // }

            if ($withdrawal_in == 'PWT' || $withdrawal_in == 'BTC' || $withdrawal_in == 'ETH' || $withdrawal_in == 'BNB' || $withdrawal_in == 'USDTTRC20' || $withdrawal_in == 'USDTERC20') {
                /**********Check Deposit condition**Start*******/
                // $totalDeposit = $this->dashboard->get_total_deposit();
                // if ($totalDeposit < 50) {
                //     $activeAmount = 50 - $totalDeposit;
                //     $datas['success'] = false;
                //     $datas['message'] = "You need to deposit $ $activeAmount to activate withdrawal";
                //     echo json_encode($datas);
                //     die;
                // }
                /**********Check Deposit condition**End*******/

                /**********Check Wallet condition**Start*******/
                if ($withdrawal_in == 'PWT') {
                    $fee = 0;
                } else if ($withdrawal_in == 'BTC' || $withdrawal_in == 'ETH') {
                    $fee = 25;
                } else if ($withdrawal_in == 'BNB') {
                    $fee = 5;
                } else if ($withdrawal_in == 'USDTTRC20') {
                    $fee = 0;
                } else if ($withdrawal_in == 'USDTERC20') {
                    $fee = 20;
                }

                if ($amount - $fee < 0) {
                    $datas['success'] = false;
                    $datas['message'] = "Invalid amount";
                } else {
                    $user_data = $this->common->getSingle('pwt_users', array('id' => $user_id));
                    $totalAmount = $amount;
                    $walletpwt = $user_data->unlocked_pwt + $user_data->pwt_wallet;
                    $walletUSDT = $user_data->wallet_amount;
                    if ($user_data->is_profile_update == 0 && $walletpwt > 500 && $walletUSDT > 250) {
                        $datas['success'] = false;
                        $datas['message'] = "Your profile is not verified";
                    } else if ($withdrawal_in == 'PWT' && $totalAmount > $user_data->unlocked_pwt) {
                        $datas['success'] = false;
                        $datas['message'] = "Amount can not be greater than your wallet amount !";
                    } else if ($withdrawal_in != 'PWT' && $totalAmount > $user_data->wallet_amount) {
                        $datas['success'] = false;
                        $datas['message'] = "Amount can not be greater than your wallet amount !";
                    } else {
                        $randnum = mt_rand(100000, 999999);
                        /********Mail for activity ***Start****/
                        $from_email = ADMIN_NO_REPLY_EMAIL; //ADMIN_INFO_EMAIL
                        $to_email = $user_data->email;
                        $subject = 'Email Verification';
                        $WEB_URL = base_url();
                        $htmlContent = file_get_contents("./mail_html/verification.html");
                        $tpl2 = str_replace('{{WEB_URL}}', $WEB_URL, $htmlContent);
                        $tpl3 = str_replace('{{USERNAME}}', $user_data->username, $tpl2);
                        $tpl4 = str_replace('{{CODE}}', $randnum, $tpl3);
                        $message_html = str_replace('{{INFO_EMAIL}}', $from_email, $tpl4);
                        // PHPMailer object
                        $mail = $this->phpmailer_lib->load();
                        // SMTP configuration
                        $mail->isSMTP();
                        $mail->Host     = 'smtp.gmail.com';
                        $mail->SMTPAuth = true;
                        $mail->Username = ADMIN_NO_REPLY_EMAIL;
                        $mail->Password = ADMIN_NO_REPLY_PWD;
                        $mail->SMTPSecure = 'ssl';
                        $mail->Port     = 465;
                        $mail->setFrom($from_email, 'PointWish');
                        $mail->addReplyTo($from_email, 'PointWish');
                        // Add a recipient
                        $mail->addAddress($to_email);
                        $mail->Subject = $subject;
                        $mail->priority = 1;
                        $mail->isHTML(true);
                        $mail->Body = $message_html;
                        if ($mail->send()) {
                            $mailCount = $this->common->getSingle('pwt_mailcount', array('date' => date('Y-m-d')));
                            if ($mailCount) {
                                $update_data = array(
                                    'count' => $mailCount->count + 1
                                );
                                $this->common->update_data('pwt_mailcount', array('id' => $mailCount->id), $update_data);
                            } else {
                                $insert_data = array(
                                    'date' => date('Y-m-d'),
                                    'count' => 1
                                );
                                $this->common->insert_data('pwt_mailcount', $insert_data);
                            }
                        }
                        /********Mail for signup***End****/

                        /******Update authentication*Start***/
                        $u_query = "update pwt_users set google_auth_code = '" . $randnum . "' where id = '" . $user_data->id . "'";
                        $this->db->query($u_query);
                        /******Update authentication*End***/

                        $datas['success'] = 'auth';
                        $datas['message'] = "OTP Sent to your mail";
                    }
                }
            } else {
                $datas['success'] = false;
                $datas['message'] = "Invalid Parameter";
            }
            next:
            echo json_encode($datas);
        }
    }

    function success_withdrawal()
    {
        $this->form_validation->set_rules('withdraw_amount', 'Amount', 'required');
        $this->form_validation->set_rules('withdraw_type', 'withdrawal Type', 'required');
        $this->form_validation->set_rules('withdraw_address', 'withdrawal Address', 'required');
        $this->form_validation->set_rules('code', 'Code', 'required');
        if ($this->form_validation->run() == FALSE) {
            $datas['success'] = false;
            $datas['message'] = "Please Fill all details";
            echo json_encode($datas);
            die;
        } else {
            $user_id = $this->session->userdata('user_id');
            $withdrawal_in = $this->input->post('withdraw_type', true);
            $amount = (float)$this->input->post('withdraw_amount', true);
            // if (!preg_match('/^\d+$/', $amount)) {
            //     $datas['success'] = false;
            //     $datas['message'] = "Please type correct transaction amount value !";
            //     echo json_encode($datas);
            //     die;
            // }

            if ($withdrawal_in == 'PWT' || $withdrawal_in == 'BTC' || $withdrawal_in == 'ETH' || $withdrawal_in == 'BNB' || $withdrawal_in == 'USDTTRC20' || $withdrawal_in == 'USDTERC20') {
                /**********Check Wallet condition**Start*******/
                $user_data = $this->common->getSingle('pwt_users', array('id' => $user_id));
                $code = $this->input->post('code');
                if ($user_data->google_auth_code == $code) {
                    if ($withdrawal_in == 'PWT') {
                        $fee = 0;
                    } else if ($withdrawal_in == 'BTC' || $withdrawal_in == 'ETH') {
                        $fee = 25;
                    } else if ($withdrawal_in == 'BNB') {
                        $fee = 5;
                    } else if ($withdrawal_in == 'USDTTRC20') {
                        $fee = 0;
                    } else if ($withdrawal_in == 'USDTERC20') {
                        $fee = 20;
                    }

                    if ($amount - $fee < 0) {
                        $datas['success'] = false;
                        $datas['message'] = "Invalid amount";
                    } else {
                        $totalAmount = $amount;
                        if ($withdrawal_in == 'PWT' && $totalAmount > $user_data->unlocked_pwt) {
                            $datas['success'] = false;
                            $datas['message'] = "Amount can not be greater than your wallet amount !";
                        } else if ($withdrawal_in != 'PWT' && $totalAmount > $user_data->wallet_amount) {
                            $datas['success'] = false;
                            $datas['message'] = "Amount can not be greater than your wallet amount !";
                        } else {
                            // $fee = (float)$this->input->post('withdraw_fee');
                            // echo "true"; die;
                            $txn_id = '#W' . rand();
                            $insert_data = array(
                                'session_id' => $user_id,
                                'username' => $user_data->username,
                                'total_amount' => $amount,
                                'amount' => $amount - $fee,
                                'admin_charge' => $fee,
                                'withdrawal_in' => $withdrawal_in,
                                'status' => 'P',
                                'created_datetime' => date('Y-m-d H:i:s'),
                                'txn_id' => $txn_id
                            );
                            $this->common->insert_data('pwt_withdrawal', $insert_data);

                            /** deduct amount from wallet***Start**/
                            if ($withdrawal_in == 'PWT') {
                                $u_query = "update pwt_users set google_auth_code = '', unlocked_pwt = unlocked_pwt - " . $amount . " where id = '" . $user_id . "'";
                            } else {
                                $u_query = "update pwt_users set google_auth_code = '', wallet_amount = wallet_amount - " . $amount . " where id = '" . $user_id . "'";
                                if ($fee == 0) {
                                    /*******Update PWT Supply **Start*****/
                                    $total = $amount - $fee;
                                    $query = "update pwt_set_price SET total_supply = total_supply +" . $total . ", total_supplied = total_supplied - " . $total .  " where id = 1";
                                    $this->db->query($query);
                                    /*******Update PWT Supply**End*****/

                                    /*******Update PWT Price **Start*****/
                                    $get = get_pwt();
                                    $cprice = (float)$get['current_price'];
                                    $supply = (float)$get['total_supply'];
                                    $purchase = $amount * 100 / $supply;
                                    $price =  $cprice * $purchase / 100;
                                    $update_data = array(
                                        'current_price' => $cprice + $price
                                    );
                                    $this->common->update_data('pwt_set_price', array('id' => 1), $update_data);

                                    $insert_history = array(
                                        'current_price' => get_pwt()['current_price'], // 10%
                                        'created_time' =>  date('Y-m-d H:i:s'),
                                    );
                                    $this->common->insert_data('pwt_coin_history', $insert_history);
                                    /*******Update PWT Price **End*****/
                                }
                            }
                            $this->db->query($u_query);
                            /** deduct amount from wallet***End**/

                            $insert_note = array(
                                'from_user' => $user_id,
                                'to_user' => $user_id,
                                'log' => 'debit',
                                'type' => 'withdrawal',
                                'payment_type' => $withdrawal_in,
                                'amount' => $amount - $fee,
                                'balance' => $withdrawal_in == 'PWT' ? getWalletAmountByUserID($user_data->id)[3] : getWalletAmountByUserID($user_data->id)[0],
                                'txn_id' => $txn_id,
                                'message' => "pending",
                                'status' => 2,
                                'created_datetime' => date('Y-m-d H:i:s'),
                                'modified_datetime' => date('Y-m-d H:i:s'),
                            );
                            $this->common->insert_data('pwt_user_notification', $insert_note);

                            /********Mail for activity ***Start****/
                            $from_email = ADMIN_NO_REPLY_EMAIL; //ADMIN_INFO_EMAIL
                            $to_email = $this->session->user_email;
                            $subject = 'Withdrawal Activity';
                            $WEB_URL = base_url();
                            $htmlContent = file_get_contents("./mail_html/withdrawal.html");
                            $tpl2 = str_replace('{{WEB_URL}}', $WEB_URL, $htmlContent);
                            $tpl3 = str_replace('{{USERNAME}}', $this->session->user_name, $tpl2);
                            $message_html = str_replace('{{INFO_EMAIL}}', $from_email, $tpl3);
                            // PHPMailer object
                            $mail = $this->phpmailer_lib->load();
                            // SMTP configuration
                            $mail->isSMTP();
                            $mail->Host     = 'smtp.gmail.com';
                            $mail->SMTPAuth = true;
                            $mail->Username = ADMIN_NO_REPLY_EMAIL;
                            $mail->Password = ADMIN_NO_REPLY_PWD;
                            $mail->SMTPSecure = 'ssl';
                            $mail->Port     = 465;
                            $mail->setFrom($from_email, 'PointWish');
                            $mail->addReplyTo($from_email, 'PointWish');
                            // Add a recipient
                            $mail->addAddress($to_email);
                            $mail->Subject = $subject;
                            $mail->priority = 1;
                            $mail->isHTML(true);
                            $mail->Body = $message_html;
                            if ($mail->send()) {
                                $mailCount = $this->common->getSingle('pwt_mailcount', array('date' => date('Y-m-d')));
                                if ($mailCount) {
                                    $update_data = array(
                                        'count' => $mailCount->count + 1
                                    );
                                    $this->common->update_data('pwt_mailcount', array('id' => $mailCount->id), $update_data);
                                } else {
                                    $insert_data = array(
                                        'date' => date('Y-m-d'),
                                        'count' => 1
                                    );
                                    $this->common->insert_data('pwt_mailcount', $insert_data);
                                }
                            }
                            /********Mail for activity ***End****/

                            $this->session->set_flashdata('success_message', "You have successfully sent withdraw request !");
                            $datas['success'] = true;
                            $datas['message'] = "You have successfully sent withdraw request !";
                        }
                    }
                } else {
                    $datas['success'] = false;
                    $datas['message'] = "Invalid Code";
                }
                echo json_encode($datas);
                die;
            } else {
                $datas['success'] = false;
                $datas['message'] = "Invalid Parameter";
                echo json_encode($datas);
                die;
            }
        }
    }

    function transfer_fund()
    {
        $user_data = $this->common->getSingle('pwt_users', array('id' => $this->session->userdata('user_id')));

        $data['title'] = 'Transfer Fund';

        $this->form_validation->set_rules('member_id', 'Member ID', 'required');
        $this->form_validation->set_rules('amount', 'Amount', 'required');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $data['user_data'] = $user_data;

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('warning_message', "Please fill all fields !");
            echo 1;
            die;
        } else {
            $member_id = $this->input->post('member_id', true);
            $amount = $this->input->post('amount', true);
            $type = $this->input->post('type', true);

            $member_data = $this->common->getSingle('pwt_users', array('username' => $member_id, 'is_delete' => 1));

            if (empty($member_data)) {
                $this->session->set_flashdata('warning_message', "Member ID doesn't exist !");
                echo 2;
                die;
            }

            if ($member_id == $this->session->userdata('user_name')) {
                $this->session->set_flashdata('warning_message', "You can't transfer funds to yourself !");
                echo 3;
                die;
            }

            if ($amount <= 0) {
                $this->session->set_flashdata('warning_message', "Invalid amount value entered !");
                echo 3;
                die;
            }

            if ($type == 'PWT' && $user_data->pwt_wallet < $amount) {
                $this->session->set_flashdata('warning_message', "Amount can not be greater than your wallet amount !");
                echo 3;
                die;
            } else if ($user_data->wallet_amount < $amount) {
                $this->session->set_flashdata('warning_message', "Amount can not be greater than your wallet amount !");
                echo 3;
                die;
            }

            if ($type == 'PWT') {
                $set = "pwt_wallet = pwt_wallet - " . $amount;
                $set1 = "pwt_wallet = pwt_wallet + " . $amount;
            } else {
                $set = "wallet_amount = wallet_amount - " . $amount;
                $set1 = "wallet_amount = wallet_amount + " . $amount;
            }
            $query1 = "update pwt_users set " . $set . " where id = '" . $this->session->userdata('user_id') . "'";
            $this->db->query($query1);

            $query2 = "update pwt_users set " . $set1 . $amount . " where username = '" . $member_id . "'";
            $this->db->query($query2);

            /********Insert Data in `transfer_history`**Start*******/
            $txn_id = '#T' . rand();
            $insert_data = array(
                'from_user' => $this->session->userdata('user_id'),
                'to_user' => $member_data->id,
                'transfer_charge' => '0',
                'amount' => $amount,
                'transfer_by' => 'user',
                'created_datetime' => date('Y-m-d H:i:s'),
                'txn_id' => $txn_id
            );
            $this->common->insert_data('pwt_transfer_history', $insert_data);
            /********Insert Data in `transfer_history`**End*******/

            /********Insert Notification**Start********/
            $insert_data = array(
                'from_user' => $this->session->userdata('user_id'),
                'to_user' => $member_data->id,
                'log' => 'debit',
                'amount' => $amount,
                'balance' => $type == 'PWT' ? getWalletAmountByUserID($this->session->userdata('user_id'))[3] : getWalletAmountByUserID($this->session->userdata('user_id'))[0],
                'type' => 'transfer',
                'txn_id' => $txn_id,
                'message' => 'transfer to ' . $member_data->username,
                'status' => 1,
                'created_datetime' => date('Y-m-d H:i:s'),
                'modified_datetime' => date('Y-m-d H:i:s'),
            );
            $this->common->insert_data('pwt_user_notification', $insert_data);

            $insert_data = array(
                'from_user' => $member_data->id,
                'to_user' => $this->session->userdata('user_id'),
                'log' => 'credit',
                'amount' => $amount,
                'type' => 'transfer',
                'balance' => $type == 'PWT' ? getWalletAmountByUserID($member_data->id)[3] : getWalletAmountByUserID($member_data->id)[0],
                'txn_id' => $txn_id,
                'message' => 'transfer',
                'status' => 1,
                'created_datetime' => date('Y-m-d H:i:s'),
                'modified_datetime' => date('Y-m-d H:i:s'),
            );
            $this->common->insert_data('pwt_user_notification', $insert_data);
            /********Insert Notification**End********/

            /********Mail for activity ***Start****/
            $from_email = ADMIN_NO_REPLY_EMAIL; //ADMIN_INFO_EMAIL
            $to_email = $this->session->user_email;
            $subject = 'Transfer Activity';
            $WEB_URL = base_url();
            $htmlContent = file_get_contents("./mail_html/transfer.html");
            $tpl2 = str_replace('{{WEB_URL}}', $WEB_URL, $htmlContent);
            $tpl3 = str_replace('{{USERNAME}}', $this->session->user_name, $tpl2);
            $message_html = str_replace('{{INFO_EMAIL}}', $from_email, $tpl3);
            //echo $message_html; die;
            // PHPMailer object
            $mail = $this->phpmailer_lib->load();
            // SMTP configuration
            $mail->isSMTP();
            $mail->Host     = 'smtp.gmail.com';
            $mail->SMTPAuth = true;
            $mail->Username = ADMIN_NO_REPLY_EMAIL;
            $mail->Password = ADMIN_NO_REPLY_PWD;
            $mail->SMTPSecure = 'ssl';
            $mail->Port     = 465;
            $mail->setFrom($from_email, 'PointWish');
            $mail->addReplyTo($from_email, 'PointWish');
            // Add a recipient
            $mail->addAddress($to_email);
            $mail->Subject = $subject;
            $mail->priority = 1;
            $mail->isHTML(true);
            $mail->Body = $message_html;
            if ($mail->send()) {
                $mailCount = $this->common->getSingle('pwt_mailcount', array('date' => date('Y-m-d')));
                if ($mailCount) {
                    $update_data = array(
                        'count' => $mailCount->count + 1
                    );
                    $this->common->update_data('pwt_mailcount', array('id' => $mailCount->id), $update_data);
                } else {
                    $insert_data = array(
                        'date' => date('Y-m-d'),
                        'count' => 1
                    );
                    $this->common->insert_data('pwt_mailcount', $insert_data);
                }
            }
            /********Mail for activity ***End****/

            $this->session->set_flashdata('success_message', 'Amount transferred successfully !');

            echo 3;
            die;
        }
    }

    function isMemberAvailabe()
    {
        if (!isset($_POST['member_id'])) {
            redirect(base_url());
        }

        $member_id = $this->input->post('member_id');

        $member_data = $this->common->getSingle('pwt_users', array('username' => $member_id, 'is_delete' => 1));

        $member_name = '';

        if (!empty($member_data)) {
            if ($member_data->id == $this->session->userdata('user_id')) {
                $member_name = ucfirst($member_data->full_name);
                $status = 3;
            } else {
                $member_name = ucfirst($member_data->full_name);
                $status = 1;
            }
        } else {
            $status = 2;
        }

        $json_arr = array('status' => $status, 'member_name' => $member_name);

        echo json_encode($json_arr);
        exit;
    }

    function ltcaddress($str)
    {
        if ($str == '') {
            $this->form_validation->set_message('ltcaddress', 'Please enter LTC address');
            return false;
        } else {
            if (preg_match('/^[LM3][a-km-zA-HJ-NP-Z1-9]{26,33}$/', $str)) {
                return true;
            } else {
                $this->form_validation->set_message('ltcaddress', 'Please enter correct LTC address');
                return false;
            }
        }
    }

    function set_ltc_address()
    {
        $this->form_validation->set_rules('ltc_address', 'LTC Address', 'required');
        if ($this->form_validation->run() == FALSE) {
            $data['message'] = "Please enter correct LTC address";
            $data['success'] = false;
        } else {
            $ethdata = $this->common->getSingle('pwt_users_bank_details', array('user_id' => $this->session->userdata('user_id')));
            $update_data['ltc_address'] = $this->input->post('ltc_address', true);
            $update_data['modified_datetime'] = date('Y-m-d H:i:s');
            if ($ethdata) {
                $this->common->update_data('pwt_users_bank_details', array('user_id' => $this->session->userdata('user_id')), $update_data);
            } else {
                $update_data['user_id'] = $this->session->userdata('user_id');
                $update_data['created_datetime'] = date('Y-m-d H:i:s');
                $this->common->insert_data('pwt_users_bank_details', $update_data);
            }
            $data['message'] = lang('updated_successully');
            $data['success'] = true;
        }
        echo json_encode($data);
    }

    function get_range_history()
    {
        $res = $this->notification->getMyNotification($this->input->post('start'), $this->input->post('end'));
        echo json_encode($res);
    }

    function remove_withdrawal()
    {
        $history = $this->common->getSingle('pwt_withdrawal', array('txn_id' => $this->input->post('withdrawal_id')));
        if ($history) {
            $this->db->where('id', $history->id)->delete('pwt_withdrawal');
            $this->db->where(array('from_user' => $history->session_id, 'txn_id' => $history->txn_id))->delete('pwt_user_notification');
            /** add amount from wallet***Start**/
            if ($history->withdrawal_in == 'PWT') {
                $u_query = "update pwt_users set unlocked_pwt = unlocked_pwt + " . ($history->total_amount) . " where id = '" . $history->session_id . "'";
            } else {
                $u_query = "update pwt_users set wallet_amount = wallet_amount + " . ($history->total_amount) . " where id = '" . $history->session_id . "'";
            }
            $this->db->query($u_query);
            /** add amount from wallet***End**/
            $this->session->set_flashdata('success_message', "Deleted withdraw request successfully !");
            echo true;
            die;
        }
    }

    function convert_price()
    {
        if ($_POST) {
            $user_data = $this->common->getSingle('pwt_users', array('id' => $this->session->user_id));
            $totalDeposit = $this->dashboard->get_total_deposit();
            // if ($totalDeposit >= 10) {
                $amount = (float)$this->input->post('amount');
                $from = $this->input->post('fromCurrecy');
                $to = $this->input->post('toCurrecy');
                // $user_data = $this->common->getSingle('pwt_users', array('id' => $this->session->user_id));
                if (($from == 'PWT' || $from == 'USDT') && ($to == 'PWT' || $to == 'USDT')) {
                    if ($from == 'PWT' && ($amount < 10 || $amount > 10000)) {
                        $data['success'] = false;
                        $data['message'] = "Amount should not be greater than 10000 PWT";
                    } else if ($from == 'USDT' && ($amount < 10 || $amount > 5000)) {
                        $data['success'] = false;
                        $data['message'] = "Amount should not be greater than 5000 USDT";
                    } else if ($from == 'PWT' && $amount > $user_data->unlocked_pwt) {
                        $data['success'] = false;
                        $data['message'] = "Insufficient Funds";
                    } else if ($from == 'USDT' && $amount > $user_data->wallet_amount) {
                        $data['success'] = false;
                        $data['message'] = "Insufficient Funds";
                    } else {
                        $pwtPrice = get_pwt()['current_price'];
                        $conv = 0;
                        if ($from == 'PWT') {
                            $calc = $amount * $pwtPrice;
                        } else if ($from == 'USDT') {
                            $calc = $amount / $pwtPrice;
                        }
                        $exchange = $this->dashboard->count_total_exchange();
                        $allow = $totalDeposit * 3; //Allow for convert pwt to usdt;
                        if ($from == 'PWT' && $user_data->is_agency == 0 && ($exchange > $allow || $calc > $allow)) {
                            $data['success'] = false;
                            $data['message'] = $allow - $exchange . " is left for exchange";
                            goto next;
                        }
                        $res = $calc; //Others 0.1% charge
                        $conv = number_format($res, 4);
                        /**********Convert History**Start*******/
                        $history = array(
                            'user_id' => $this->session->user_id,
                            'from_currency' => $from,
                            'to_currency' => $to,
                            'amount' => $amount,
                            'conv_amount' => $res,
                            'date' => date('Y-m-d')
                        );
                        $this->common->insert_data('pwt_exchange_history', $history);
                        /**********Convert History**End*******/

                        /**********Update Wallet & History**Start*******/
                        if ($from == 'PWT') {
                            $user_query = "update pwt_users set wallet_amount = wallet_amount + " . $res . ", unlocked_pwt = unlocked_pwt - " . $amount . " where id = '" . $this->session->user_id . "'";
                            $this->db->query($user_query);
                            /*******Update PWT Supply **Start*****/
                            $u_query = "update pwt_set_price SET total_supply = total_supply +" . $amount . ", total_supplied = total_supplied - " . $amount .  " where id = 1";
                            $this->db->query($u_query);
                            /*******Update PWT Supply**End*****/
                        } else if ($from == 'USDT') {
                            $user_query = "update pwt_users set unlocked_pwt = unlocked_pwt + " . $res . ", wallet_amount = wallet_amount - " . $amount . " where id = '" . $this->session->user_id . "'";
                            $this->db->query($user_query);
                            /*******Update PWT Supply **Start*****/
                            $u_query = "update pwt_set_price SET total_supply = total_supply -" . $amount . ", total_supplied = total_supplied + " . $amount .  " where id = 1";
                            $this->db->query($u_query);
                            /*******Update PWT Supply**End*****/
                        }

                        /*******Notification **Start****/
                        $insert_note = array(
                            'from_user' => $this->session->user_id,
                            'to_user' => $this->session->user_id,
                            'log' => 'debit',
                            'type' => 'exchange',
                            'payment_type' => $from,
                            'amount' => $amount,
                            'balance' => $from == 'PWT' ? getWalletAmountByUserID($user_data->id)[3] : getWalletAmountByUserID($user_data->id)[0],
                            'txn_id' => '#E' . rand(),
                            'message' => "Convert PWT",
                            'status' => 1,
                            'created_datetime' => date('Y-m-d H:i:s'),
                            'modified_datetime' => date('Y-m-d H:i:s'),
                        );
                        $this->common->insert_data('pwt_user_notification', $insert_note);

                        $insert_note = array(
                            'from_user' => $this->session->user_id,
                            'to_user' => $this->session->user_id,
                            'log' => 'credit',
                            'type' => 'exchange',
                            'payment_type' => $to,
                            'amount' => $conv,
                            'balance' => $from == 'PWT' ? getWalletAmountByUserID($user_data->id)[0] : getWalletAmountByUserID($user_data->id)[3],
                            'txn_id' => '#E' . rand(),
                            'message' => "Convert PWT",
                            'status' => 1,
                            'created_datetime' => date('Y-m-d H:i:s'),
                            'modified_datetime' => date('Y-m-d H:i:s'),
                        );
                        $this->common->insert_data('pwt_user_notification', $insert_note);
                        /*******Notification **End****/

                        /*******Update PWT Price **Start*****/
                        $get = get_pwt();
                        $cprice = (float)$get['current_price'];
                        $supply = (float)$get['total_supply'];
                        $purchase = $amount * 100 / $supply;
                        $price =  $cprice * $purchase / 100;
                        $current_price = $from == 'PWT' ? $cprice - $price : $cprice + $price;
                        $update_data = array(
                            'current_price' => $current_price
                        );
                        $this->common->update_data('pwt_set_price', array('id' => 1), $update_data);

                        $insert_history = array(
                            'current_price' => get_pwt()['current_price'], // 10%
                            'created_time' =>  date('Y-m-d H:i:s'),
                        );
                        $this->common->insert_data('pwt_coin_history', $insert_history);
                        /*******Update PWT Price **End*****/

                        /**********Update Wallet & History**End*******/

                        $data['success'] = true;
                        $data['message'] = "Converted Successfully";
                    }
                } else {
                    $data['success'] = false;
                    $data['message'] = "Invalid Parameter";
                }
            // } else {
            //     $data['success'] = false;
            //     $data['message'] = "Please activate the agency !";
            // }
            next:
            echo json_encode($data);
        }
    }

    function send_mail()
    {
        $randnum = mt_rand(100000, 999999);

        /********Mail for activity ***Start****/
        $from_email = ADMIN_NO_REPLY_EMAIL; //ADMIN_INFO_EMAIL
        $to_email = $this->session->user_email;
        $subject = 'Email Verification';
        $WEB_URL = base_url();
        $htmlContent = file_get_contents("./mail_html/verification.html");
        $tpl2 = str_replace('{{WEB_URL}}', $WEB_URL, $htmlContent);
        $tpl3 = str_replace('{{USERNAME}}', $this->session->user_name, $tpl2);
        $tpl4 = str_replace('{{CODE}}', $randnum, $tpl3);
        $message_html = str_replace('{{INFO_EMAIL}}', $from_email, $tpl4);
        // PHPMailer object
        $mail = $this->phpmailer_lib->load();
        // SMTP configuration
        $mail->isSMTP();
        $mail->Host     = 'smtp.gmail.com';
        $mail->SMTPAuth = true;
        $mail->Username = ADMIN_NO_REPLY_EMAIL;
        $mail->Password = ADMIN_NO_REPLY_PWD;
        $mail->SMTPSecure = 'ssl';
        $mail->Port     = 465;
        $mail->setFrom($from_email, 'PointWish');
        $mail->addReplyTo($from_email, 'PointWish');
        // Add a recipient
        $mail->addAddress($to_email);
        $mail->Subject = $subject;
        $mail->priority = 1;
        $mail->isHTML(true);
        $mail->Body = $message_html;
        if ($mail->send()) {
            $mailCount = $this->common->getSingle('pwt_mailcount', array('date' => date('Y-m-d')));
            if ($mailCount) {
                $update_data = array(
                    'count' => $mailCount->count + 1
                );
                $this->common->update_data('pwt_mailcount', array('id' => $mailCount->id), $update_data);
            } else {
                $insert_data = array(
                    'date' => date('Y-m-d'),
                    'count' => 1
                );
                $this->common->insert_data('pwt_mailcount', $insert_data);
            }
        }
        /********Mail for signup***End****/

        /******Last Login Update*Start***/
        $u_query = "update pwt_users set google_auth_code = '" . $randnum . "' where id = '" . $this->session->user_id . "'";
        $this->db->query($u_query);
        /******Last Login Update*End***/

        $this->session->set_flashdata('success_message', 'Mail sent successfully');
        echo true;
    }

    function set_trx_address()
    {
        $this->form_validation->set_rules('trx_address', 'TRX Address', 'required|max_length[45]');
        if ($this->form_validation->run() == FALSE) {
            $data['message'] = "Invalid address";
            $data['success'] = false;
        } else {
            $ethdata = $this->common->getSingle('pwt_users_bank_details', array('user_id' => $this->session->userdata('user_id')));
            $update_data['trx_address'] = $this->input->post('trx_address', true);
            $update_data['modified_datetime'] = date('Y-m-d H:i:s');
            if ($ethdata) {
                $this->common->update_data('pwt_users_bank_details', array('user_id' => $this->session->userdata('user_id')), $update_data);
            } else {
                $update_data['user_id'] = $this->session->userdata('user_id');
                $update_data['created_datetime'] = date('Y-m-d H:i:s');
                $this->common->insert_data('pwt_users_bank_details', $update_data);
            }
            $data['message'] = lang('updated_successully');
            $data['success'] = true;
        }
        echo json_encode($data);
    }

    function reset_wallet_address()
    {
        $user = $this->common->getSingle('pwt_users_bank_details', array('user_id' => $this->session->userdata('user_id')));
        $update_data[$this->input->post('wallet_address')] = "";
        $update_data['modified_datetime'] = date('Y-m-d H:i:s');
        if ($user) {
            $this->common->update_data('pwt_users_bank_details', array('user_id' => $this->session->userdata('user_id')), $update_data);
        }
        $data['message'] = "Reset Successfully";
        $data['success'] = true;

        echo json_encode($data);
    }

    function reupload($docid)
    {
        $this->common->update_data('pwt_users_document', array('id' => $docid), array('status' => 3));
        redirect(base_url('account'));
    }

    function get_notification()
    {
        $data = array(
            'total' =>  $this->notification->count_unread_noti(),
            'notifications' => $this->common->getWhereFromLast('pwt_notification',  array('user_id' => $this->session->user_id), 20),
        );
        echo json_encode($data);
    }


    function binanceapikey()
    {
        $this->form_validation->set_rules('apikey', 'Api key', 'required');
        $this->form_validation->set_rules('securitykey', 'Security key', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error_message', 'All fields are mandatory');
        } else {
            $update_data = array(
                'binance_apikey' => $this->input->post('apikey'),
                'binance_securitykey' => $this->input->post('securitykey'),
                'binance_apikey_active_check' => 1,

            );

            $this->common->update_data('pwt_users', array('id' => $this->session->userdata('user_id')), $update_data);
            $this->session->set_flashdata('success_message', 'Uploaded Successfully');
        }
        /*******Upload key***End***/

        redirect(base_url() . 'apikey');
    }
    function reset_binance_address()
    {
        $user = $this->common->getSingle('pwt_users', array('id' => $this->session->userdata('user_id')));

        $update_data['binance_apikey_active_check'] = 0;
        if ($user) {
            $this->common->update_data('pwt_users', array('id' => $this->session->userdata('user_id')), $update_data);
        }
        $data['message'] = "Reset Successfully";
        $data['success'] = true;

        redirect(base_url() . 'apikey');
    }
    function treding_bot_last_val_stop()
    {
        $select_stamp_val = $this->input->post('data');


        $new_update_stamp = $this->trade->check_curr_stamp_startstop($select_stamp_val);

        if ($new_update_stamp == 0) {
            $data['message'] = "Error";
            $data['success'] = false;
        } else {
            $data['message'] = lang('set_succ');
            $data['success'] = true;
        }
    }

    function kyc_verification()
    {
        $data['title'] = "KYC Verification";
        $data['activetitle'] = 'kyc';
        $data['id_data'] = $this->document->getMyDocumentByType('id');
        $data['user_data']  = $this->common->getSingle('pwt_users', array('id' => $this->session->userdata('user_id')));
        $this->load->view('trading/profile/kyc', $data);
    }
    function crypto_address()
    {
        $data['title'] = "Crypto Address";
        $data['activetitle'] = 'crypto-address';
        $this->load->view('trading/profile/crypto_address', $data);
    }
    function change_pwd()
    {
        $data['title'] = "Change Password";
        $data['activetitle'] = "change-pwd";
        $this->load->view('trading/profile/change_pwd', $data);
    }
    function twoFA()
    {
        $data['title'] = "2FA";
        $data['activetitle'] = "2fa";
        $this->load->view('trading/profile/2fa', $data);
    }
    function profileapi()
    {
        $data['title'] = "API Key";
        $data['activetitle'] = "apikey";
        $data['user_data']  = $this->common->getSingle('pwt_users', array('id' => $this->session->userdata('user_id')));
        $this->load->view('trading/profile/api', $data);
    }
    function deposit_wallet()
    {
        $data['title'] = "Deposite";
        $data['activetitle'] = "deposite-wallet";
        $this->load->view('trading/wallet/deposite', $data);
    }
    function withdraw_wallet()
    {
        $data['title'] = "Withdraw";
        $data['activetitle'] = "withdraw-wallet";
        $this->load->view('trading/wallet/withdraw', $data);
    }
    function wallet_transaction()
    {
        $data['title'] = "Wallet Transaction";
        $data['activetitle'] = "transaction-wallet";
        // $data['history'] = $this->notification->getLastMyNotification();
        $data['history'] = $this->notification->getMyNotification();
        $this->load->view('trading/wallet/transaction', $data);
    }
}
