<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Agency extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        /****checks it is login or not start****/
        $this->_userLoginCheck();
        /****checks it is login or not End****/
        $this->load->model('Agency_model', 'agency');
    }

    function index()
    {
        $user_data = $this->common->getSingle('pwt_users', array('id' => $this->session->user_id));
        if($user_data->is_agency == 1) {
            $data['title'] = 'Agency';
            $users = $this->agency->get_my_network();
            $agencies = 0;
            foreach ($users as $row) {
                $agencies += $row['is_agency'] == 1 ? 1 : 0;
                $result[] = array(
                    'username' => $row['username'],
                    'sponsor' => $row['sponsor'],
                    'is_agency' => $row['is_agency'],
                    'tree_count' => $row['tree_count'] ? $row['tree_count'] : 0,
                    'total_trade' => $row['is_agency'] == 1 ? $this->agency->get_count_bids($row['id'], 2) : 0
                );
            }
            $data['referrals'] = count($users);
            $data['agencies'] = $agencies;
            $data['trading_commission'] = $this->agency->get_total_commission(0);
            $data['agency_commission'] = $this->agency->get_total_commission(1);
            $report = $this->get_my_level();
            $data['mylevel'] = $report['level'];
            $data['nextLevel'] = $report['level'] >= 7 ? 7 : $report['level'] + 1;
            $value = $this->get_level_condition($data['nextLevel']);
            $data['mydownline'] = $report['tree_count'] ? $report['tree_count'] : 0;
            $data['total_downline'] = $value['license'];
            $data['total_volume'] = $value['volume'];
            $data['myvolume'] = $report['tree'] ? $report['totalTrade'] : 0;        
            $data['history'] = $this->agency->get_commission_history();
            $data['networks'] = $result;
            $this->load->view('trading/profile/agency', $data);
        } else {
            $this->session->set_flashdata('error_message', "Activate the agency for accessing !");
            redirect(base_url('affiliate'));
        }
    }

    function make_payment()
    {
        $type = $this->input->post('type');
        if($type == 'pwt' || $type == 'USDT') {
            $amount = $type == 'pwt' ? 200 : 100;
            $user = $this->common->getSingle('pwt_users', array('id' => $this->session->user_id));
            if ($amount > $user->unlocked_pwt && $type == 'pwt') {
                $datas['success'] = false;
                $datas['message'] = "Please refill your wallet";
            } else if ($amount > $user->wallet_amount && $type == 'USDT') {
                $datas['success'] = false;
                $datas['message'] = "Please refill your wallet";
            } else {
                $update = $type == 'pwt' ? array('is_agency' => 1, 'upgrade_agency' => date('Y-m-d H:i:s'), 'unlocked_pwt' => $user->unlocked_pwt - $amount) : array('is_agency' => 1, 'upgrade_agency' => date('Y-m-d H:i:s'), 'wallet_amount' => $user->wallet_amount - $amount);
                $this->common->update_data('pwt_users', array('id' => $this->session->user_id), $update);
                /*******Notification **Start****/
                $insert_note = array(
                    'user_id' => $this->session->user_id,
                    'type' => 'agency',
                    'target' => base_url('agency'),
                    'detail' => 'Agency activated',
                    'date' => date('Y-m-d H:i:s'),
                );
                $this->common->insert_data('pwt_notification', $insert_note);
                /*******Notification **Start****/
                //Level 1
                $sponsor_data = $this->common->getSingle('pwt_users', array('username' => $user->sponsor, 'is_agency' => 1));
                if ($sponsor_data) {
                    // Agency Tree
                    $usertree = $this->common->getSingle('pwt_agency_tree', array('sponsor_user' => $user->sponsor));
                
                    if ($usertree) {
                        $tree = array(
                            'tree' => $usertree->tree . ',' . $user->id,
                            'tree_count' => $usertree->tree_count + 1
                        );
                        $this->common->update_data('pwt_agency_tree', array('sponsor_id' => $sponsor_data->id), $tree);
                    } else {
                        $tree = array(
                            'sponsor_id' => $sponsor_data->id,
                            'sponsor_user' => $sponsor_data->username,
                            'tree' => $user->id,
                            'tree_count' => 1
                        );
                        $this->common->insert_data('pwt_agency_tree', $tree);
                    }
                    $levelIncome = $this->get_level_income(1)['income'];
                    $this->common->update_data('pwt_users', array('id' => $sponsor_data->id), array('wallet_amount' => $sponsor_data->wallet_amount + $levelIncome));
                    $insert = array(
                        'user_id' => $sponsor_data->id,
                        'sponsor_user' => $user->username,
                        'level' => 1,
                        'commission_type' => 1,
                        'amount' => $amount,
                        'earned' => $levelIncome,
                        'created_date' => date('Y-m-d')
                    );
                    $this->common->insert_data('pwt_agency_commission', $insert);
    
                    /*******Notification **Start****/
                    $insert_note = array(
                        'user_id' => $sponsor_data->id,
                        'type' => 'agency',
                        'target' => base_url('agency'),
                        'detail' => "Agency commission $ $levelIncome is received - $user->username",
                        'date' => date('Y-m-d H:i:s'),
                    );
                    $this->common->insert_data('pwt_notification', $insert_note);
                    /*******Notification **Start****/
    
                    //Level 2
                    $sponsor_data = $this->common->getSingle('pwt_users', array('username' => $sponsor_data->sponsor, 'is_agency' => 1));
                    if ($sponsor_data) {
                        $levelIncome = $this->get_level_income(2)['income'];
                        $this->common->update_data('pwt_users', array('id' => $sponsor_data->id), array('wallet_amount' => $sponsor_data->wallet_amount + $levelIncome));
                        $insert = array(
                            'user_id' => $sponsor_data->id,
                            'sponsor_user' => $user->username,
                            'level' => 2,
                            'commission_type' => 1,
                            'amount' => $amount,
                            'earned' => $levelIncome,
                            'created_date' => date('Y-m-d')
                        );
                        $this->common->insert_data('pwt_agency_commission', $insert);
    
                        /*******Notification **Start****/
                        $insert_note = array(
                            'user_id' => $sponsor_data->id,
                            'type' => 'agency',
                            'target' => base_url('agency'),
                            'detail' => "Agency commission $ $levelIncome is received - $user->username",
                            'date' => date('Y-m-d H:i:s'),
                        );
                        $this->common->insert_data('pwt_notification', $insert_note);
                        /*******Notification **Start****/
    
                        //Level 3
                        $sponsor_data = $this->common->getSingle('pwt_users', array('username' => $sponsor_data->sponsor, 'is_agency' => 1));
                        if ($sponsor_data) {
                            $levelIncome = $this->get_level_income(3)['income'];
                            $this->common->update_data('pwt_users', array('id' => $sponsor_data->id), array('wallet_amount' => $sponsor_data->wallet_amount + $levelIncome));
                            $insert = array(
                                'user_id' => $sponsor_data->id,
                                'sponsor_user' => $user->username,
                                'level' => 3,
                                'commission_type' => 1,
                                'amount' => $amount,
                                'earned' => $levelIncome,
                                'created_date' => date('Y-m-d')
                            );
                            $this->common->insert_data('pwt_agency_commission', $insert);
    
                            /*******Notification **Start****/
                            $insert_note = array(
                                'user_id' => $sponsor_data->id,
                                'type' => 'agency',
                                'target' => base_url('agency'),
                                'detail' => "Agency commission $ $levelIncome is received - $user->username",
                                'date' => date('Y-m-d H:i:s'),
                            );
                            $this->common->insert_data('pwt_notification', $insert_note);
                            /*******Notification **Start****/
    
                            //Level 4
                            $sponsor_data = $this->common->getSingle('pwt_users', array('username' => $sponsor_data->sponsor, 'is_agency' => 1));
                            if ($sponsor_data) {
                                $levelIncome = $this->get_level_income(4)['income'];
                                $this->common->update_data('pwt_users', array('id' => $sponsor_data->id), array('wallet_amount' => $sponsor_data->wallet_amount + $levelIncome));
                                $insert = array(
                                    'user_id' => $sponsor_data->id,
                                    'sponsor_user' => $user->username,
                                    'level' => 4,
                                    'commission_type' => 1,
                                    'amount' => $amount,
                                    'earned' => $levelIncome,
                                    'created_date' => date('Y-m-d')
                                );
                                $this->common->insert_data('pwt_agency_commission', $insert);
    
                                /*******Notification **Start****/
                                $insert_note = array(
                                    'user_id' => $sponsor_data->id,
                                    'type' => 'agency',
                                    'target' => base_url('agency'),
                                    'detail' => "Agency commission $ $levelIncome is received - $user->username",
                                    'date' => date('Y-m-d H:i:s'),
                                );
                                $this->common->insert_data('pwt_notification', $insert_note);
                                /*******Notification **Start****/
    
                                //Level 5
                                $sponsor_data = $this->common->getSingle('pwt_users', array('username' => $sponsor_data->sponsor, 'is_agency' => 1));
                                if ($sponsor_data) {
                                    $levelIncome = $this->get_level_income(5)['income'];
                                    $this->common->update_data('pwt_users', array('id' => $sponsor_data->id), array('wallet_amount' => $sponsor_data->wallet_amount + $levelIncome));
                                    $insert = array(
                                        'user_id' => $sponsor_data->id,
                                        'sponsor_user' => $user->username,
                                        'level' => 5,
                                        'commission_type' => 1,
                                        'amount' => $amount,
                                        'earned' => $levelIncome,
                                        'created_date' => date('Y-m-d')
                                    );
                                    $this->common->insert_data('pwt_agency_commission', $insert);
    
                                    /*******Notification **Start****/
                                    $insert_note = array(
                                        'user_id' => $sponsor_data->id,
                                        'type' => 'agency',
                                        'target' => base_url('agency'),
                                        'detail' => "Agency commission $ $levelIncome is received - $user->username",
                                        'date' => date('Y-m-d H:i:s'),
                                    );
                                    $this->common->insert_data('pwt_notification', $insert_note);
                                    /*******Notification **Start****/
    
                                    //Level 6
                                    $sponsor_data = $this->common->getSingle('pwt_users', array('username' => $sponsor_data->sponsor, 'is_agency' => 1));
                                    if ($sponsor_data) {
                                        $levelIncome = $this->get_level_income(6)['income'];
                                        $this->common->update_data('pwt_users', array('id' => $sponsor_data->id), array('wallet_amount' => $sponsor_data->wallet_amount + $levelIncome));
                                        $insert = array(
                                            'user_id' => $sponsor_data->id,
                                            'sponsor_user' => $user->username,
                                            'level' => 6,
                                            'commission_type' => 1,
                                            'amount' => $amount,
                                            'earned' => $levelIncome,
                                            'created_date' => date('Y-m-d')
                                        );
                                        $this->common->insert_data('pwt_agency_commission', $insert);
    
                                        /*******Notification **Start****/
                                        $insert_note = array(
                                            'user_id' => $sponsor_data->id,
                                            'type' => 'agency',
                                            'target' => base_url('agency'),
                                            'detail' => "Agency commission $ $levelIncome is received - $user->username",
                                            'date' => date('Y-m-d H:i:s'),
                                        );
                                        $this->common->insert_data('pwt_notification', $insert_note);
                                        /*******Notification **Start****/
    
                                        //Level 7
                                        $sponsor_data = $this->common->getSingle('pwt_users', array('username' => $sponsor_data->sponsor, 'is_agency' => 1));
                                        if ($sponsor_data) {
                                            $levelIncome = $this->get_level_income(7)['income'];
                                            $this->common->update_data('pwt_users', array('id' => $sponsor_data->id), array('wallet_amount' => $sponsor_data->wallet_amount + $levelIncome));
                                            $insert = array(
                                                'user_id' => $sponsor_data->id,
                                                'sponsor_user' => $user->username,
                                                'level' => 7,
                                                'commission_type' => 1,
                                                'amount' => $amount,
                                                'earned' => $levelIncome,
                                                'created_date' => date('Y-m-d')
                                            );
                                            $this->common->insert_data('pwt_agency_commission', $insert);
    
                                            /*******Notification **Start****/
                                            $insert_note = array(
                                                'user_id' => $sponsor_data->id,
                                                'type' => 'agency',
                                                'target' => base_url('agency'),
                                                'detail' => "Agency commission $ $levelIncome is received - $user->username",
                                                'date' => date('Y-m-d H:i:s'),
                                            );
                                            $this->common->insert_data('pwt_notification', $insert_note);
                                            /*******Notification **Start****/
                                        }
                                    }
                                }
                            }
                        }
                    }
                }                
                $insert_note = array(
                    'from_user' => $user->id,
                    'to_user' => $user->id,
                    'log' => 'debit',
                    'type' => 'agency',
                    'payment_type' => $type,
                    'amount' => $amount,
                    'balance' => $type == 'pwt' ? getWalletAmountByUserID($user->id)[3] : getWalletAmountByUserID($user->id)[0],
                    'txn_id' => '#AG' . rand(),
                    'message' => "Agency activated",
                    'status' => 1,
                    'created_datetime' => date('Y-m-d H:i:s'),
                    'modified_datetime' => date('Y-m-d H:i:s'),
                );
                $this->common->insert_data('pwt_user_notification', $insert_note);
    
                $datas['success'] = true;
                $datas['message'] = "Agency activated successfully.";
            }
        } else {
            $datas['success'] = false;
            $datas['message'] = "Invalid parameter";
        }
        echo json_encode($datas);
    }

    function get_range_history()
    {
        echo json_encode($this->agency->get_commission_history($this->input->post('start'), $this->input->post('end')));
    }

    function get_level_income($level)
    {
        $income = 0;
        $commission = 0;
        switch ($level) {
            case 1:
                $income = 50;
                $commission = 1;
                break;
            case 2:
                $income = 25;
                $commission = 0.5;
                break;
            case 3:
                $income = 12.5;
                $commission = 0.25;
                break;
            case 4:
                $income = 6.25;
                $commission = 0.13;
                break;
            case 5:
                $income = 3.12;
                $commission = 0.06;
                break;
            case 6:
                $income = 1.56;
                $commission = 0.03;
                break;
            case 7:
                $income = 0.78;
                $commission = 0.02;
                break;
        }
        $data['income'] = $income;
        $data['commission'] = $commission;
        return $data;
    }

    function get_user_info()
    {
        echo json_encode($this->common->getSingle('pwt_users', array('id' => $this->session->user_id)));
    }

    function get_my_level()
    {
        $level = 1;
        $tree = $this->common->getSingle('pwt_agency_tree', array('sponsor_id' => $this->session->user_id));
        $totalTrade = $this->agency->get_count_bids($tree->tree, 2);
        if ($tree->tree_count >= 8 && $totalTrade >= 64000) {
            $level = 7;
            goto next;
        } else if ($tree->tree_count >= 7 && $totalTrade >= 32000) {
            $level = 6;
            goto next;
        } else if ($tree->tree_count >= 6 && $totalTrade >= 16000) {
            $level = 5;
            goto next;
        } else if ($tree->tree_count >= 5 && $totalTrade >= 8000) {
            $level = 4;
            goto next;
        } else if ($tree->tree_count >= 4 && $totalTrade >= 4000) {
            $level = 3;
            goto next;
        } else if ($tree->tree_count >= 3 && $totalTrade >= 2000) {
            $level = 2;
            goto next;
        }
        next:
        $data['level'] = $level;
        $data['tree_count'] = $tree->tree_count;
        $data['tree'] = $tree->tree;
        $data['totalTrade'] = $totalTrade;
        return $data;
    }

    function get_level_condition($level)
    {
        switch ($level) {
            case 1:
                $data['license'] = 0;
                $data['volume'] = 0;
                break;
            case 2:
                $data['license'] = 3;
                $data['volume'] = 2000;
                break;
            case 3:
                $data['license'] = 4;
                $data['volume'] = 4000;
                break;
            case 4:
                $data['license'] = 5;
                $data['volume'] = 8000;
                break;
            case 5:
                $data['license'] = 6;
                $data['volume'] = 16000;
                break;
            case 6:
                $data['license'] = 7;
                $data['volume'] = 32000;
                break;
            case 7:
                $data['license'] = 8;
                $data['volume'] = 64000;
                break;
            default:
                $data['license'] = 0;
                $data['volume'] = 0;
                break;
        }
        return $data;
    }

    // function get_my_network()
    // {
    //     $users = $this->agency->get_my_network();
    //     foreach ($users as $row) {
    //         $result[] = array(
    //             'username' => $row['username'],
    //             'sponsor' => $row['sponsor'],
    //             'is_agency' => $row['is_agency'],
    //             'tree_count' => $row['tree_count'],
    //             'total_trade' => $row['is_agency'] == 1 ? $this->agency->get_count_bids($row['tree'], 2) : $this->agency->get_total_trade($row['id'])
    //         );
    //     }
    //     echo json_encode($result);
    // }
}
