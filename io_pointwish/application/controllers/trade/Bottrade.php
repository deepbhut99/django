<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bottrade extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        /****checks it is login or not start****/
        $this->_userLoginCheck();
        /****checks it is login or not End****/
        $this->load->model('Agency_model', 'agency');
    }

    function make_payment()
    {
        $type = $this->input->post('type');
        if ($type == 'USDT') {
            $amount = 100;
            $user = $this->common->getSingle('pwt_users', array('id' => $this->session->user_id));
            if ($amount > $user->wallet_amount && $type == 'USDT') {
                $datas['success'] = false;
                $datas['message'] = "Please refill your wallet";
            } else {
                $insert_note = array();
                $insert_history = array();
                $update = array('is_bottrade' => 1, 'upgrade_bottrade' => date('Y-m-d H:i:s'), 'wallet_amount' => $user->wallet_amount - $amount);
                $this->common->update_data('pwt_users', array('id' => $this->session->user_id), $update);
                /*******Notification **Start****/
                $insert_note[] = array(
                    'user_id' => $this->session->user_id,
                    'type' => 'bottrading',
                    'target' => base_url('bot-trading'),
                    'detail' => 'Bot activated',
                    'date' => date('Y-m-d H:i:s'),
                );
                /*******Notification **End****/
                /*******History**Start****/
                $insert_history[] = array(
                    'from_user' => $user->id,
                    'to_user' => $user->id,
                    'log' => 'debit',
                    'type' => 'bottrade',
                    'payment_type' => $type,
                    'amount' => $amount,
                    'balance' => getWalletAmountByUserID($user->id)[0],
                    'txn_id' => '#BT' . rand(),
                    'message' => "Bottrade activated",
                    'status' => 1,
                    'created_datetime' => date('Y-m-d H:i:s'),
                    'modified_datetime' => date('Y-m-d H:i:s'),
                );
                /*******History **End****/

                //Update to v1
                $tree = array(
                    'sponsor_id' => $user->id,
                    'level' => 1
                );
                $this->common->insert_data('pwt_bottrade_tree', $tree);


                //Level 1
                $sponsor_first = $this->common->getSingle('pwt_users', array('username' => $user->sponsor, 'is_bottrade' => 1));
                if ($sponsor_first) {
                    /*******Update Direct Income **Start****/
                    $levelIncome = 35;
                    $this->common->update_data('pwt_users', array('id' => $sponsor_first->id), array('wallet_amount' => $sponsor_first->wallet_amount + $levelIncome));
                    /*******Update Direct Income **End****/

                    /*******Notification **Start****/
                    $insert_note[] = array(
                        'user_id' => $sponsor_first->id,
                        'type' => 'bottrade1',
                        'target' => '#',
                        'detail' => "Bot Direct commission $ $levelIncome is received - $user->username",
                        'date' => date('Y-m-d H:i:s'),
                    );
                    /*******Notification **End****/
                    /*******History **Start****/
                    $insert_history[] = array(
                        'from_user' => $sponsor_first->id,
                        'to_user' => $user->id,
                        'log' => 'credit',
                        'type' => 'bottrade',
                        'payment_type' => $type,
                        'amount' => $levelIncome,
                        'balance' => getWalletAmountByUserID($sponsor_first->id)[0],
                        'txn_id' => '#BT' . rand(),
                        'message' => "BT Direct Commission",
                        'status' => 1,
                        'created_datetime' => date('Y-m-d H:i:s'),
                        'modified_datetime' => date('Y-m-d H:i:s'),
                    );
                    /*******History **End****/

                    // Bot Tree
                    $usertree = $this->common->getSingle('pwt_bottrade_tree', array('sponsor_id' => $user->sponsor_id));
                    if ($usertree) {
                        $tree = array(
                            'team_tree' => $usertree->team_tree ? $usertree->team_tree . ',' . $user->id : $user->id,
                            'direct_count' => $usertree->direct_count + 1,
                        );
                        $this->common->update_data('pwt_bottrade_tree', array('sponsor_id' => $sponsor_first->id), $tree);
                    }

                    //get direct and level count
                    $team = $this->get_count_team($user->sponsor_id);

                    //get bot level
                    $firstTreeLevel = $this->get_bot_level($team['directCount'], $team['treeCount'], $team['directLevelCount']);

                    //update bot level
                    $this->common->update_data('pwt_bottrade_tree', array('sponsor_id' => $sponsor_first->id), array('level' => $firstTreeLevel['level']));

                    //Level 2
                    $sponsor_second = $this->common->getSingle('pwt_users', array('username' => $sponsor_first->sponsor, 'is_bottrade' => 1));
                    if ($sponsor_second) {
                        // Bot Tree
                        $usertree = $this->common->getSingle('pwt_bottrade_tree', array('sponsor_id' => $sponsor_second->id));
                        if ($usertree) {
                            if ($usertree->level >= $firstTreeLevel['level'] && $usertree->level > 1 ) {
                                /*******Update Direct Income **Start****/
                                $levelIncome = 15;
                                $this->common->update_data('pwt_users', array('id' => $sponsor_second->id), array('wallet_amount' => $sponsor_second->wallet_amount + $levelIncome));
                                /*******Update Direct Income **End****/

                                /*******Notification **Start****/
                                $insert_note[] = array(
                                    'user_id' => $sponsor_second->id,
                                    'type' => 'bottrade2',
                                    'target' => '#',
                                    'detail' => "Bot Level 2 commission $ $levelIncome is received - $user->username",
                                    'date' => date('Y-m-d H:i:s'),
                                );
                                /*******Notification **End****/
                                /*******History **Start****/
                                $insert_history[] = array(
                                    'from_user' => $sponsor_second->id,
                                    'to_user' => $user->id,
                                    'log' => 'credit',
                                    'type' => 'bottrade',
                                    'payment_type' => $type,
                                    'amount' => $levelIncome,
                                    'balance' => getWalletAmountByUserID($sponsor_second->id)[0],
                                    'txn_id' => '#BT' . rand(),
                                    'message' => "BT level 2 Commission",
                                    'status' => 1,
                                    'created_datetime' => date('Y-m-d H:i:s'),
                                    'modified_datetime' => date('Y-m-d H:i:s'),
                                );
                                /*******History **End****/
                            }
                            //get direct and level count
                            $team = $this->get_count_team($sponsor_second->id);

                            //get bot level
                            $secondTreeLevel = $this->get_bot_level($team['directCount'], $team['treeCount'], $team['directLevelCount']);

                            //update bot level
                            $this->common->update_data('pwt_bottrade_tree', array('sponsor_id' => $sponsor_second->id), array('level' => $secondTreeLevel['level']));

                            //Level 3
                            $sponsor_third = $this->common->getSingle('pwt_users', array('username' => $sponsor_second->sponsor, 'is_bottrade' => 1));
                            if ($sponsor_third) {
                                // Bot Tree
                                $usertree = $this->common->getSingle('pwt_bottrade_tree', array('sponsor_id' => $sponsor_third->id));
                                if ($usertree) {
                                    if ($usertree->level >= $secondTreeLevel['level'] && $usertree->level > 2) {
                                        /*******Update Direct Income **Start****/
                                        $levelIncome = 25;
                                        $this->common->update_data('pwt_users', array('id' => $sponsor_third->id), array('wallet_amount' => $sponsor_third->wallet_amount + $levelIncome));
                                        /*******Update Direct Income **End****/

                                        /*******Notification **Start****/
                                        $insert_note[] = array(
                                            'user_id' => $sponsor_third->id,
                                            'type' => 'bottrade3',
                                            'target' => '#',
                                            'detail' => "Bot Level 3 commission $ $levelIncome is received - $user->username",
                                            'date' => date('Y-m-d H:i:s'),
                                        );
                                        /*******Notification **End****/
                                        /*******History **Start****/
                                        $insert_history[] = array(
                                            'from_user' => $sponsor_third->id,
                                            'to_user' => $user->id,
                                            'log' => 'credit',
                                            'type' => 'bottrade',
                                            'payment_type' => $type,
                                            'amount' => $levelIncome,
                                            'balance' => getWalletAmountByUserID($sponsor_third->id)[0],
                                            'txn_id' => '#BT' . rand(),
                                            'message' => "BT level 3 Commission",
                                            'status' => 1,
                                            'created_datetime' => date('Y-m-d H:i:s'),
                                            'modified_datetime' => date('Y-m-d H:i:s'),
                                        );
                                        /*******History **End****/
                                    }
                                    //get direct and level count
                                    $team = $this->get_count_team($sponsor_third->id);

                                    //get bot level
                                    $thirdTreeLevel = $this->get_bot_level($team['directCount'], $team['treeCount'], $team['directLevelCount']);

                                    //update bot level
                                    $this->common->update_data('pwt_bottrade_tree', array('sponsor_id' => $sponsor_third->id), array('level' => $thirdTreeLevel['level']));

                                    //Level 4
                                    $sponsor_fourth = $this->common->getSingle('pwt_users', array('username' => $sponsor_third->sponsor, 'is_bottrade' => 1));
                                    if ($sponsor_fourth) {
                                        // Bot Tree
                                        $usertree = $this->common->getSingle('pwt_bottrade_tree', array('sponsor_id' => $sponsor_fourth->id));
                                        if ($usertree) {
                                            if ($usertree->level >= $thirdTreeLevel['level'] && $usertree->level > 3) {
                                                /*******Update Direct Income **Start****/
                                                $levelIncome = 35;
                                                $this->common->update_data('pwt_users', array('id' => $sponsor_fourth->id), array('wallet_amount' => $sponsor_fourth->wallet_amount + $levelIncome));
                                                /*******Update Direct Income **End****/

                                                /*******Notification **Start****/
                                                $insert_note[] = array(
                                                    'user_id' => $sponsor_fourth->id,
                                                    'type' => 'bottrade4',
                                                    'target' => '#',
                                                    'detail' => "Bot Level 4 commission $ $levelIncome is received - $user->username",
                                                    'date' => date('Y-m-d H:i:s'),
                                                );
                                                /*******Notification **End****/
                                                /*******History **Start****/
                                                $insert_history[] = array(
                                                    'from_user' => $sponsor_fourth->id,
                                                    'to_user' => $user->id,
                                                    'log' => 'credit',
                                                    'type' => 'bottrade',
                                                    'payment_type' => $type,
                                                    'amount' => $levelIncome,
                                                    'balance' => getWalletAmountByUserID($sponsor_fourth->id)[0],
                                                    'txn_id' => '#BT' . rand(),
                                                    'message' => "BT level 4 Commission",
                                                    'status' => 1,
                                                    'created_datetime' => date('Y-m-d H:i:s'),
                                                    'modified_datetime' => date('Y-m-d H:i:s'),
                                                );
                                                /*******History **End****/
                                            }
                                            //get direct and level count
                                            $team = $this->get_count_team($sponsor_fourth->id);

                                            //get bot level
                                            $fourthTreeLevel = $this->get_bot_level($team['directCount'], $team['treeCount'], $team['directLevelCount']);

                                            //update bot level
                                            $this->common->update_data('pwt_bottrade_tree', array('sponsor_id' => $sponsor_fourth->id), array('level' => $fourthTreeLevel['level']));

                                            //Level 5
                                            $sponsor_five = $this->common->getSingle('pwt_users', array('username' => $sponsor_fourth->sponsor, 'is_bottrade' => 1));
                                            if ($sponsor_five) {
                                                // Bot Tree
                                                $usertree = $this->common->getSingle('pwt_bottrade_tree', array('sponsor_id' => $sponsor_five->id));
                                                if ($usertree) {
                                                    if ($usertree->level >= $fourthTreeLevel['level'] && $usertree->level > 4) {
                                                        /*******Update Direct Income **Start****/
                                                        $levelIncome = 45;
                                                        $this->common->update_data('pwt_users', array('id' => $sponsor_five->id), array('wallet_amount' => $sponsor_five->wallet_amount + $levelIncome));
                                                        /*******Update Direct Income **End****/

                                                        /*******Notification **Start****/
                                                        $insert_note[] = array(
                                                            'user_id' => $sponsor_five->id,
                                                            'type' => 'bottrade5',
                                                            'target' => '#',
                                                            'detail' => "Bot Level 5 commission $ $levelIncome is received - $user->username",
                                                            'date' => date('Y-m-d H:i:s'),
                                                        );
                                                        /*******Notification **End****/
                                                        /*******History **Start****/
                                                        $insert_history[] = array(
                                                            'from_user' => $sponsor_five->id,
                                                            'to_user' => $user->id,
                                                            'log' => 'credit',
                                                            'type' => 'bottrade',
                                                            'payment_type' => $type,
                                                            'amount' => $levelIncome,
                                                            'balance' => getWalletAmountByUserID($sponsor_five->id)[0],
                                                            'txn_id' => '#BT' . rand(),
                                                            'message' => "BT level 5 Commission",
                                                            'status' => 1,
                                                            'created_datetime' => date('Y-m-d H:i:s'),
                                                            'modified_datetime' => date('Y-m-d H:i:s'),
                                                        );
                                                        /*******History **End****/
                                                    }
                                                    //get direct and level count
                                                    $team = $this->get_count_team($sponsor_five->id);

                                                    //get bot level
                                                    $fiveTreeLevel = $this->get_bot_level($team['directCount'], $team['treeCount'], $team['directLevelCount']);

                                                    //update bot level
                                                    $this->common->update_data('pwt_bottrade_tree', array('sponsor_id' => $sponsor_five->id), array('level' => $fiveTreeLevel['level']));

                                                    //Level 6
                                                    $sponsor_six = $this->common->getSingle('pwt_users', array('username' => $sponsor_five->sponsor, 'is_bottrade' => 1));
                                                    if ($sponsor_six) {
                                                        // Bot Tree
                                                        $usertree = $this->common->getSingle('pwt_bottrade_tree', array('sponsor_id' => $sponsor_six->id));
                                                        if ($usertree) {
                                                            if ($usertree->level >= $fiveTreeLevel['level'] && $usertree->level > 5) {
                                                                /*******Update Direct Income **Start****/
                                                                $levelIncome = 55;
                                                                $this->common->update_data('pwt_users', array('id' => $sponsor_six->id), array('wallet_amount' => $sponsor_six->wallet_amount + $levelIncome));
                                                                /*******Update Direct Income **End****/

                                                                /*******Notification **Start****/
                                                                $insert_note[] = array(
                                                                    'user_id' => $sponsor_six->id,
                                                                    'type' => 'bottrade6',
                                                                    'target' => '#',
                                                                    'detail' => "Bot Level 6 commission $ $levelIncome is received - $user->username",
                                                                    'date' => date('Y-m-d H:i:s'),
                                                                );
                                                                /*******Notification **End****/
                                                                /*******History **Start****/
                                                                $insert_history[] = array(
                                                                    'from_user' => $sponsor_six->id,
                                                                    'to_user' => $user->id,
                                                                    'log' => 'credit',
                                                                    'type' => 'bottrade',
                                                                    'payment_type' => $type,
                                                                    'amount' => $levelIncome,
                                                                    'balance' => getWalletAmountByUserID($sponsor_six->id)[0],
                                                                    'txn_id' => '#BT' . rand(),
                                                                    'message' => "BT level 6 Commission",
                                                                    'status' => 1,
                                                                    'created_datetime' => date('Y-m-d H:i:s'),
                                                                    'modified_datetime' => date('Y-m-d H:i:s'),
                                                                );
                                                                /*******History **End****/
                                                            }
                                                            //get direct and level count
                                                            $team = $this->get_count_team($sponsor_six->id);

                                                            //get bot level
                                                            $sixTreeLevel = $this->get_bot_level($team['directCount'], $team['treeCount'], $team['directLevelCount']);

                                                            //update bot level
                                                            $this->common->update_data('pwt_bottrade_tree', array('sponsor_id' => $sponsor_six->id), array('level' => $sixTreeLevel['level']));
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if ($insert_note) {
                    $this->db->insert_batch('pwt_notification', $insert_note);
                }
                if ($insert_history) {
                    $this->db->insert_batch('pwt_user_notification', $insert_history);
                }
                $datas['success'] = true;
                $datas['message'] = "Bottrading activated successfully.";
            }
        } else {
            $datas['success'] = false;
            $datas['message'] = "Invalid parameter";
        }
        echo json_encode($datas);
    }

    //Get bot level.
    function get_bot_level($directCount, $teamCount, $atLeast)
    {
        // 3v5=15, 3v4=12, 3v3=9, 3v2=6
        $level = "";
        if ($directCount >= 20 && $teamCount >= 1500 && $atLeast >= 15) {
            $level = 6;
        } else if ($directCount >= 12 && $teamCount >= 800 && $atLeast >= 12) {
            $level = 5;
        } else if ($directCount >= 8 && $teamCount >= 300 && $atLeast >= 9) {
            $level = 4;
        } else if ($directCount >= 5 && $teamCount >= 100 && $atLeast >= 6) {
            $level = 3;
        } else if ($directCount >= 3 && $teamCount >= 20 && $atLeast >= 0) {
            $level = 2;
        } else {
            $level = 1;
        }
        $data['level'] = $level;
        return $data;
    }

    function get_count_team($sponsor_id)
    {
        $directCount = 0;
        $directLevelCount = 0;
        $treeCount = 0;
        $usertree = $this->common->getSingle('pwt_bottrade_tree', array('sponsor_id' => $sponsor_id));
        if ($usertree) {
            $directCount = $usertree->direct_count;
            $treeCount += $usertree->direct_count;
            $query = "SELECT sum(direct_count) as teamcount, sum(level) as levelcount, GROUP_CONCAT(team_tree) as user_tree FROM pwt_bottrade_tree WHERE sponsor_id in ($usertree->team_tree) and direct_count >= 1";
            $data = $this->db->query($query);
            if ($data->num_rows() > 0) {
                $row = $data->row();
                if ($row->user_tree) {
                    $directLevelCount = $row->levelcount;
                    $treeCount += $row->teamcount;
                    $query = "SELECT sum(direct_count) as teamcount, GROUP_CONCAT(team_tree) as user_tree FROM pwt_bottrade_tree WHERE sponsor_id in ($row->user_tree) and direct_count >= 1";
                    $data = $this->db->query($query);
                    if ($data->num_rows() > 0) {
                        $row = $data->row();
                        if ($row->user_tree) {
                            $treeCount += $row->teamcount;
                            $query = "SELECT sum(direct_count) as teamcount, GROUP_CONCAT(team_tree) as user_tree FROM pwt_bottrade_tree WHERE sponsor_id in ($row->user_tree) and direct_count >= 1";
                            $data = $this->db->query($query);
                            if ($data->num_rows() > 0) {
                                $row = $data->row();
                                if ($row->user_tree) {
                                    $treeCount += $row->teamcount;
                                    $query = "SELECT sum(direct_count) as teamcount, GROUP_CONCAT(team_tree) as user_tree FROM pwt_bottrade_tree WHERE sponsor_id in ($row->user_tree) and direct_count >= 1";
                                    $data = $this->db->query($query);
                                    if ($data->num_rows() > 0) {
                                        $row = $data->row();
                                        if ($row->user_tree) {
                                            $treeCount += $row->teamcount;
                                            $query = "SELECT sum(direct_count) as teamcount, GROUP_CONCAT(team_tree) as user_tree FROM pwt_bottrade_tree WHERE sponsor_id in ($row->user_tree) and direct_count >= 1";
                                            $data = $this->db->query($query);
                                            if ($data->num_rows() > 0) {
                                                $row = $data->row();
                                                $treeCount += $row->teamcount;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        $datas['treeCount'] = $treeCount;
        $datas['directCount'] = $directCount;
        $datas['directLevelCount'] = $directLevelCount;
        return $datas;
    }
}
