<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bottradebid extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        /****checks it is login or not start****/
        $this->_userLoginCheck();
        /****checks it is login or not End****/
        $this->load->model('Document_model', 'document');
        $this->load->model('Dashboard_model', 'dashboard');
        $this->load->model('Notification_model', 'notification');
        $this->load->model('Agency_model', 'agency');
        $this->load->model('Trading_Model', 'trade');
        $this->load->model('Bottrading_model', 'bottreding');
        $this->load->model('Common_model', 'common');
    }
    function treding_set_val()
    {
        $this->form_validation->set_rules('tradamount', 'Trade Amount', 'required');
        $this->form_validation->set_rules('ordertype', 'Order Type', 'required');
        $this->form_validation->set_rules('margincall', 'margin call', 'required');
        if ($this->form_validation->run() == FALSE) {
            $data['message'] = "Please enter correct  value";
            $data['success'] = false;
        } else {
            // multiple bid store
            $number = $this->input->post('srnum', true);
            $buypoint = $this->input->post('buy', true);
            $amountmul = $this->input->post('buyratio', true);
            $sell = $this->input->post('sell', true);
            $sellstop = $this->input->post('sellstop', true);
            $first_valu = $this->input->post('tradamount', true);
            $binance_for_cycle = $this->dashboard->getbinancekey($this->session->userdata('user_id'));
            $second_valu = array_sum($buypoint);
            $total_val = $first_valu * $second_valu;
            $save_data3['id_whom_set'] = $this->session->userdata('user_id');
            $save_data3['createdate'] = time();
            $save_data3['tradeamount'] = $first_valu;
            $save_data3['buytoatl_total'] = $total_val;
            $save_data3['coin'] = $this->input->post('ordertype', true);
            $save_data3['total_stratage'] = $this->input->post('margincall', true);
            $save_data3['apikey'] = $binance_for_cycle->binance_apikey;
            $save_data3['apisecurity'] = $binance_for_cycle->binance_securitykey;

            $this->common->insert_data('bot_stratage', $save_data3);
            $input_id_forstratage =  $this->db->insert_id();
            $newvalue =  array_map(null, $number, $buypoint, $amountmul, $sell, $sellstop);
            if (!empty($newvalue)) {
                if (!empty($newvalue)) {
                    foreach ($newvalue as $list) {
                        $insert_value['user_id'] = $this->session->userdata('user_id');
                        $insert_value['buypoint'] = $list[1];
                        $insert_value['creatdate'] = time();
                        $insert_value['buyamountmul'] = $list[2];
                        $insert_value['sell'] = $list[3];
                        $insert_value['sellstop'] = $list[4];
                        $insert_value['stratage_id'] = $input_id_forstratage;
                        $this->common->insert_data('multiple_bid_forbot', $insert_value);
                    }
                }
            }
            $follow  = $this->common->getWhere('pwt_copy_trade',  array('master_user_id' => $this->session->user_id));
            $totalbid = $this->trade->lastbed($this->input->post('margincall', true));
            foreach ($follow as $foll) {
                $user_id3 = $foll->user_id;
                $bal = $this->input->post('tradamount', true);
                $balance1 = $this->dashboard->get_last_balance($user_id3);
                $binance1 = $this->dashboard->getbinancekey($foll->user_id);
                $binance_for_cycle1 = $this->dashboard->getbinancekey($user_id3);
                $save_data5['id_whom_set'] =  $user_id3;
                $save_data5['createdate'] = time();
                $save_data5['tradeamount'] = $first_valu;
                $save_data5['buytoatl_total'] = $total_val;
                $save_data5['coin'] = $this->input->post('ordertype', true);
                $save_data5['total_stratage'] = $this->input->post('margincall', true);
                $save_data5['apikey'] = $binance_for_cycle1->binance_apikey;
                $save_data5['apisecurity'] = $binance_for_cycle1->binance_securitykey;

                $this->common->insert_data('bot_stratage', $save_data5);
                $input_id_forstratage =  $this->db->insert_id();
                $newvalue =  array_map(null, $number, $buypoint, $amountmul, $sell, $sellstop);
                if (!empty($newvalue)) {
                    if (!empty($newvalue)) {
                        foreach ($newvalue as $list) {
                            $insert_value1['user_id'] = $this->session->userdata('user_id');
                            $insert_value1['buypoint'] = $list[1];
                            $insert_value1['creatdate'] = time();
                            $insert_value1['buyamountmul'] = $list[2];
                            $insert_value1['sell'] = $list[3];
                            $insert_value1['sellstop'] = $list[4];
                            $insert_value1['stratage_id'] = $input_id_forstratage;
                            $this->common->insert_data('multiple_bid_forbot', $insert_value1);
                        }
                    }
                }
            }
            foreach ($totalbid as $bid) {
                $chek_bot_copytrad = $this->trade->check_bot_copytradeactive('pwt_users', array('id' => $this->session->user_id, 'copy_trade_forbot' => 1));
                if ($chek_bot_copytrad == TRUE) {
                    foreach ($follow as $foll) {
                        $user_id = $foll->user_id;
                        $bal = $this->input->post('tradamount', true);
                        $balance1 = $this->dashboard->get_last_balance($user_id);
                        $binance1 = $this->dashboard->getbinancekey($foll->user_id);
                        $symbol = $this->input->post('ordertype', true);
                        if ($balance1['wallet_amount'] > 5) {
                            if ($binance1 != '') {
                                // $check_bidstartor = $this->trade->get_check_bidstart($user_id);
                                $buyamount = $this->input->post('tradamount', true);
                                $save_data2['buysell'] =  3;
                                $save_data2['user_id'] = $user_id;
                                $save_data2['created_date'] = time();
                                $save_data2['margincalllimit'] = 1;
                                $save_data2['copy_bottradebid'] = $this->session->userdata('user_id');
                                $save_data2['copy_tradepercentage'] = $foll->profit_sharing;
                                $save_data2['takieprofitratio'] = -$bid['buypoint'];
                                $save_data2['firstbuyinamout'] = $buyamount * $bid['buyamountmul'];
                                $save_data2['sell_point'] = $bid['sell'];
                                $save_data2['sell_stop_limit'] = $bid['sellstop'];
                                $save_data2['typeofpoint'] = $this->input->post('ordertype', true);
                                $save_data2['binance_apikey'] = $binance1->binance_apikey;
                                $save_data2['binance_securitykey'] = $binance1->binance_securitykey;
                                $save_data2['total_stratage'] = $this->input->post('margincall', true);
                                $save_data2['stratage_id'] = $input_id_forstratage;
                                $save_data2['multiplebuyinratio'] = $bid['buyamountmul'];
                                $this->common->insert_data('etx_condition_for_btctousdt', $save_data2);
                            }
                        }
                    }
                }
                $user_id1 = $this->session->userdata('user_id');
                $buyamount = $this->input->post('tradamount', true);
                $binance = $this->dashboard->getbinancekey($user_id1);
                $balance2 = $this->dashboard->get_last_balance($user_id1);
                if ($balance2['wallet_amount'] > 5) {
                    #balance check
                    if ($binance != '') {
                        $save_data1['buysell'] =  3;
                        $save_data1['user_id'] = $user_id1;
                        $save_data1['created_date'] = time();
                        $save_data1['margincalllimit'] = 1;
                        $save_data1['takieprofitratio'] = -$bid['buypoint'];
                        $save_data1['firstbuyinamout'] = $buyamount * $bid['buyamountmul'];
                        $save_data1['sell_point'] = $bid['sell'];
                        $save_data1['sell_stop_limit'] = $bid['sellstop'];
                        $save_data1['typeofpoint'] = $this->input->post('ordertype', true);
                        $save_data1['binance_apikey'] = $binance->binance_apikey;
                        $save_data1['binance_securitykey'] = $binance->binance_securitykey;
                        $save_data1['total_stratage'] = $this->input->post('margincall', true);
                        $save_data1['stratage_id'] = $input_id_forstratage;
                        $save_data1['multiplebuyinratio'] = $bid['buyamountmul'];
                        $this->common->insert_data('etx_condition_for_btctousdt', $save_data1);

                        $data['message'] = lang('set_succ');
                        $data['success'] = true;
                        $data['type'] = $this->input->post('ordertype', true);
                    } else {
                        $data['message'] = "Plz set binance key";
                    }
                } else {
                    $data['message'] = "Balance is very low";
                }
            }


            #store stratage 
        }
        echo json_encode($data);
    }
    function binanceapikey()
    {
        $this->form_validation->set_rules('apikey', 'Api key', 'required');
        $this->form_validation->set_rules('securitykey', 'Security key', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error_message', 'All fields are mandatory');
        } else {
            $update_data = array(
                'binance_apikey' => $this->input->post('apikey'),
                'binance_securitykey' => $this->input->post('securitykey'),
                'binance_apikey_active_check' => 1,
            );

            $this->common->update_data('pwt_users', array('id' => $this->session->userdata('user_id')), $update_data);
            $this->session->set_flashdata('success_message', 'Uploaded Successfully');
        }
        /*******Upload key***End***/

        redirect(base_url() . 'account');
    }
    function reset_binance_address()
    {
        $user = $this->common->getSingle('pwt_users', array('id' => $this->session->userdata('user_id')));

        $update_data['binance_apikey_active_check'] = 0;
        if ($user) {
            $this->common->update_data('pwt_users', array('id' => $this->session->userdata('user_id')), $update_data);
        }
        $data['message'] = "Reset Successfully";
        $data['success'] = true;

        redirect(base_url() . 'account');
    }
    function treding_bot_last_bid_stop()
    {
        $select_stamp_val = $this->input->post('data');
        $user_idown = $this->session->userdata('user_id');
        // $stratage_id = $this->trade->get_currentstratage_id('etx_condition_for_btctousdt', array('user_id' => $user_idown, 'typeofpoint' => $select_stamp_val));
        $stratage_id = $this->input->post('id');

        if (!empty($stratage_id)) {
            $update_cycle = array(
                'cycle_status' => 0,
            );
            $this->common->update_data('bot_stratage', array('id' => $stratage_id, 'id_whom_set' => $this->session->userdata('user_id')), $update_cycle);
        }

        $new_update_stamp = $this->trade->check_curr_stamp_stop($select_stamp_val, $user_idown);
        $follow  = $this->common->getWhere('pwt_copy_trade',  array('master_user_id' => $this->session->user_id));
        foreach ($follow as $foll) {
            $user_id = $foll->user_id;
            $check_startage_idstart = $this->trade->get_currentstratage_id('etx_condition_for_btctousdt', array('user_id' => $user_id, 'typeofpoint' => $select_stamp_val, 'stratage_id' => $stratage_id->stratage_id));
            if ($check_startage_idstart->buysell == -1 || $check_startage_idstart->buysell == 1) {
                $update = $this->trade->check_curr_stamp_stop($select_stamp_val, $user_id);
            }
        }
        if ($new_update_stamp == FALSE) {
            $data['message'] = "Error";
            $data['success'] = FALSE;
        } else {
            $data['message'] = 'succ';
            $data['success'] = true;
        }
    }
    function treding_bot_last_bid_start()
    {
        $select_stamp_val = $this->input->post('data');
        $user_idown = $this->session->userdata('user_id');
        $stratage_id = $this->trade->get_currentstratage_id('etx_condition_for_btctousdt', array('user_id' => $user_idown, 'typeofpoint' => $select_stamp_val, 'buysell' => 3));
        $new_update_stamp = $this->trade->check_curr_stamp_start($select_stamp_val, $user_idown);
        $follow  = $this->common->getWhere('pwt_copy_trade',  array('master_user_id' => $this->session->user_id));
        foreach ($follow as $foll) {
            $user_id = $foll->user_id;
            $check_startage_idstart = $this->trade->check_startage_idstart('etx_condition_for_btctousdt', array('user_id' => $user_id, 'typeofpoint' => $select_stamp_val, 'buysell' => 3, 'stratage_id' => $stratage_id->stratage_id));
            if ($check_startage_idstart == TRUE) {
                $update = $this->trade->check_curr_stamp_start($select_stamp_val, $user_id);
            }
        }
        if ($new_update_stamp == FALSE) {
            $data['message'] = "Error";
            $data['success'] = FALSE;
        } else {
            $data['message'] = 'succ';
            $data['success'] = true;
        }
    }
    function savemultiplebid()
    {
        $number = $this->input->post('srnum', true);
        $buypoint = $this->input->post('buy', true);
        $amountmul = $this->input->post('buyratio', true);
        $sell = $this->input->post('sell', true);
        $sellstop = $this->input->post('sellstop', true);

        $newvalue =  array_map(null, $number, $buypoint, $amountmul, $sell, $sellstop);

        if (!empty($newvalue)) {
            if (!empty($newvalue)) {
                foreach ($newvalue as $list) {

                    $insert_value['user_id'] = $this->session->userdata('user_id');
                    $insert_value['buypoint'] = $list[1];
                    $insert_value['creatdate'] = time();
                    $insert_value['buyamountmul'] = $list[2];
                    $insert_value['sell'] = $list[3];
                    $insert_value['sellstop'] = $list[4];

                    $this->common->insert_data('multiple_bid_forbot', $insert_value);
                }
                $data['success'] = true;
                $data['message'] = lang('set_succ');
                echo json_encode($data);
            }
        }
    }



    function trade_setting($type)
    {
        $data['title'] = 'Trade Setting';
        $data['title1'] = 'Trade Setting';
        $data['type'] = $type;        
        $this->load->view('trading/bot/trade_setting', $data);
        // $this->load->view('trading/bot/trade_setting', $data);
    }
    function margin_configuration($type)
    {
        $data['title'] = 'Margin Configuration';
        $data['title1'] = 'Margin Configuration';
        $data['type'] = $type;
        $this->load->view('trading/bot/margin_configuration', $data);
    }
    function profit()
    {
        $data['title'] = 'Profit';
        $data['title1'] = 'Profit';
        $this->load->view('trading/bot/profit', $data);
    }
    function coin_status($type)
    {
        $data['title'] = 'Coin Status';
        $data['title1'] = 'Coin Status';
        $data['type'] = $type;

        $lastbid = $this->common->getSingleFromLast('etx_condition_for_btctousdt', array('user_id' => $this->session->user_id, 'typeofpoint' => $type));
        if (!empty($lastbid)) {
            $data['botbid'] = $lastbid;
        } else {
            $data['botbid'] = "defaultval";
        }

        $multibid = $this->common->getSingleFromLast2('etx_condition_for_btctousdt', array('user_id' => $this->session->user_id, 'buysell' => -1));
        if (!empty($multibid)) {
            $data['multibid1'] = $multibid;
        } else {
            $data['multibid1'] = "defaultval2";
        }

        $multibid2 = $this->common->getSingleFromLast2('etx_condition_for_btctousdt', array('user_id' => $this->session->user_id));
        if (!empty($multibid2)) {
            $data['multibid2'] = $multibid2;
        } else {
            $data['multibid2'] = "defaultval3";
        }

        $multibid3 = $this->common->getSingleFromLast2('etx_condition_for_btctousdt', array('user_id' => $this->session->user_id, 'typeofpoint' => $type));
        if (!empty($multibid3)) {
            $data['multibid3'] = $multibid3;
        } else {
            $data['multibid3'] = "defaultval4";
        }


        $data['botapicheck'] = $this->common->getSingle('pwt_users', array('id' => $this->session->user_id));
        $data['balancebinance'] = $this->common->getSingle('user_coin_info', array('user_id' => $this->session->user_id));
        $this->load->view('trading/bot/coin_status', $data);
    }
    function saved_strategy()
    {
        $data['title'] = 'Saved Strategy';
        $data['title1'] = 'Saved Strategy';
        $this->load->view('trading/bot/saved_strategy', $data);
    }

    function aginusepre()
    {
        $data1 = $this->input->post('data', true);
        $isExist = $this->common->getSingleFromLast('bot_stratage', array('id_whom_set' => $this->session->user_id, 'coin' => $data1));
        if ($isExist) {
            $margincall = $isExist->total_stratage;
            $treadamount = $isExist->tradeamount;

            $coin = $isExist->coin;
            $follow  = $this->common->getWhere('pwt_copy_trade',  array('master_user_id' => $this->session->user_id));
            $totalbid = $this->trade->lastbed($margincall);
            $balance = $this->dashboard->get_last_balance($this->session->user_id);
            foreach ($totalbid as $bid) {
                foreach ($follow as $foll) {

                    $user_id = $foll->user_id;
                    $bal = $this->input->post('tradamount', true);
                    $balance1 = $this->dashboard->get_last_balance($user_id);
                    $binance1 = $this->dashboard->getbinancekey($foll->user_id);
                    if ($binance1 != '') {
                        $buyamount = $treadamount;
                        $save_data2['buysell'] =  -1;
                        $save_data2['user_id'] = $user_id;
                        $save_data2['created_date'] = time();
                        $save_data2['margincalllimit'] = 1;
                        $save_data2['takieprofitratio'] = -$bid['buypoint'];
                        $save_data2['firstbuyinamout'] = (float)$buyamount * $bid['buyamountmul'];
                        $save_data2['sell_point'] = $bid['sell'];
                        $save_data2['sell_stop_limit'] = $bid['sellstop'];
                        $save_data2['typeofpoint'] = $coin;
                        $save_data2['binance_apikey'] = $binance1->binance_apikey;
                        $save_data2['binance_securitykey'] = $binance1->binance_securitykey;
                        $save_data2['multiplebuyinratio'] = $bid['buyamountmul'];
                        $this->common->insert_data('etx_condition_for_btctousdt', $save_data2);
                    }
                }
                $user_id1 = $this->session->userdata('user_id');
                $buyamount = $isExist->tradeamount;
                $binance = $this->dashboard->getbinancekey($user_id1);
                #balance check
                if ($binance != '') {
                    $save_data1['buysell'] =  -1;
                    $save_data1['user_id'] = $user_id1;
                    $save_data1['created_date'] = time();
                    $save_data1['margincalllimit'] = 1;
                    $save_data1['takieprofitratio'] = -$bid['buypoint'];
                    $save_data1['firstbuyinamout'] = $buyamount * $bid['buyamountmul'];
                    $save_data1['sell_point'] = $bid['sell'];
                    $save_data1['sell_stop_limit'] = $bid['sellstop'];
                    $save_data1['typeofpoint'] = $coin;
                    $save_data1['binance_apikey'] = $binance->binance_apikey;
                    $save_data1['binance_securitykey'] = $binance->binance_securitykey;
                    $save_data1['multiplebuyinratio'] = $bid['buyamountmul'];
                    $this->common->insert_data('etx_condition_for_btctousdt', $save_data1);

                    $data['message'] = lang('set_succ');
                    $data['success'] = true;
                } else {
                    $data['message'] = "Plz set binance key";
                }
            }

            echo json_encode($data);
        } else {
            $margincall = 1;
            $treadamount = 15;
            $coin = $data1;
            $follow  = $this->common->getWhere('pwt_copy_trade',  array('master_user_id' => $this->session->user_id));
            foreach ($follow as $foll) {
                $user_id = $foll->user_id;
                $binance1 = $this->dashboard->getbinancekey($foll->user_id);
                if ($binance1 != '') {
                    $buyamount = $treadamount;
                    $save_data2['buysell'] =  -1;
                    $save_data2['user_id'] = $user_id;
                    $save_data2['created_date'] = time();
                    $save_data2['margincalllimit'] = 1;
                    $save_data2['takieprofitratio'] = -1.5;
                    $save_data2['firstbuyinamout'] = $treadamount;
                    $save_data2['sell_point'] = 1.3;
                    $save_data2['sell_stop_limit'] = 0.3;
                    $save_data2['typeofpoint'] = $coin;
                    $save_data2['binance_apikey'] = $binance1->binance_apikey;
                    $save_data2['binance_securitykey'] = $binance1->binance_securitykey;
                    $this->common->insert_data('etx_condition_for_btctousdt', $save_data2);
                }
            }
            $user_id1 = $this->session->userdata('user_id');
            $binance = $this->dashboard->getbinancekey($user_id1);
            #balance check
            if ($binance != '') {
                $save_data1['buysell'] =  -1;
                $save_data1['user_id'] = $user_id1;
                $save_data1['created_date'] = time();
                $save_data1['margincalllimit'] = 1;
                $save_data1['takieprofitratio'] = -1.5;
                $save_data1['firstbuyinamout'] = $treadamount;
                $save_data1['sell_point'] = 1.3;
                $save_data1['sell_stop_limit'] = 0.3;
                $save_data1['typeofpoint'] = $coin;
                $save_data1['binance_apikey'] = $binance->binance_apikey;
                $save_data1['binance_securitykey'] = $binance->binance_securitykey;

                $this->common->insert_data('etx_condition_for_btctousdt', $save_data1);

                $data['message'] = lang('set_succ');
                $data['success'] = true;
            } else {
                $data['message'] = "Plz set binance key";
            }


            echo json_encode($data);
        }
        #store stratage 


    }
    function atatimebid()
    {
        $data1 = $this->input->post('data', true);
        $data2 = $this->input->post('type', true);
        $isExist = $this->common->getSingleFromLast('bot_stratage', array('id_whom_set' => $this->session->user_id, 'coin' => $data2));
        if ($data1 == 1) {
            if ($isExist) {
                $margincall = $isExist->total_stratage;
                $treadamount = $isExist->tradeamount;
                $coin = $isExist->coin;
                $follow  = $this->common->getWhere('pwt_copy_trade',  array('master_user_id' => $this->session->user_id));
                $totalbid = $this->trade->lastbed($margincall);
                $balance = $this->dashboard->get_last_balance($this->session->user_id);
                foreach ($totalbid as $bid) {
                    // foreach ($follow as $foll) {

                    //     $user_id = $foll['user_id'];
                    //     $bal = $this->input->post('tradamount', true);
                    //     $balance1 = $this->dashboard->get_last_balance($user_id);
                    //     $binance1 = $this->dashboard->getbinancekey($foll->user_id);
                    //     if ($binance1 == '') {
                    //         $buyamount = $treadamount;
                    //         $save_data2['buysell'] =  -1;
                    //         $save_data2['user_id'] = $user_id;
                    //         $save_data2['created_date'] = time();
                    //         $save_data2['margincalllimit'] = 1;
                    //         $save_data2['takieprofitratio'] = -$bid['buypoint'];
                    //         $save_data2['firstbuyinamout'] = $buyamount * $bid['buyamountmul'];
                    //         $save_data2['sell_point'] = $bid['sell'];
                    //         $save_data2['sell_stop_limit'] = $bid['sellstop'];
                    //         $save_data2['typeofpoint'] = $coin;
                    //         $save_data2['binance_apikey'] = $binance1->binance_apikey;
                    //         $save_data2['binance_securitykey'] = $binance1->binance_securitykey;
                    //         $this->common->insert_data('etx_condition_for_btctousdt', $save_data2);
                    //     }
                    // }
                    $user_id1 = $this->session->userdata('user_id');
                    $buyamount = $isExist->tradeamount;
                    $binance = $this->dashboard->getbinancekey($user_id1);
                    #balance check
                    if ($binance != '') {
                        $save_data1['buysell'] =  4;
                        $save_data1['user_id'] = $user_id1;
                        $save_data1['created_date'] = time();
                        $save_data1['margincalllimit'] = 1;
                        $save_data1['takieprofitratio'] = -$bid['buypoint'];
                        $save_data1['firstbuyinamout'] = $buyamount * $bid['buyamountmul'];
                        $save_data1['sell_point'] = $bid['sell'];
                        $save_data1['sell_stop_limit'] = $bid['sellstop'];
                        $save_data1['typeofpoint'] = $coin;
                        $save_data1['binance_apikey'] = $binance->binance_apikey;
                        $save_data1['binance_securitykey'] = $binance->binance_securitykey;

                        $this->common->insert_data('etx_condition_for_btctousdt', $save_data1);
                        var_dump($save_data1);
                        exit();

                        $data['message'] = lang('set_succ');
                        $data['success'] = true;
                    } else {
                        $data['message'] = "Plz set binance key";
                    }
                }
            } else {
                $data['message'] = 'You do not have old strategy';
                $data['success'] = false;
            }
        } else {
            $margincall = 1;
            $treadamount = 15;
            $coin = $data2;
            $follow  = $this->common->getWhere('pwt_copy_trade',  array('master_user_id' => $this->session->user_id));
            // foreach ($follow as $foll) {
            //     $user_id = $foll['user_id'];
            //     $binance1 = $this->dashboard->getbinancekey($foll->user_id);
            //     if ($binance1 == '') {
            //         $buyamount = $treadamount;
            //         $save_data2['buysell'] =  -1;
            //         $save_data2['user_id'] = $user_id;
            //         $save_data2['created_date'] = time();
            //         $save_data2['margincalllimit'] = 1;
            //         $save_data2['takieprofitratio'] = -1.5;
            //         $save_data2['firstbuyinamout'] = $treadamount;
            //         $save_data2['sell_point'] = 1.3;
            //         $save_data2['sell_stop_limit'] = 0.3;
            //         $save_data2['typeofpoint'] = $coin;
            //         $save_data2['binance_apikey'] = $binance1->binance_apikey;
            //         $save_data2['binance_securitykey'] = $binance1->binance_securitykey;
            //         $this->common->insert_data('etx_condition_for_btctousdt', $save_data2);
            //     }
            // }
            $user_id1 = $this->session->userdata('user_id');
            $binance = $this->dashboard->getbinancekey($user_id1);
            #balance check
            if ($binance != '') {
                $save_data1['buysell'] = 4;
                $save_data1['user_id'] = $user_id1;
                $save_data1['created_date'] = time();
                $save_data1['margincalllimit'] = 1;
                $save_data1['takieprofitratio'] = -1.5;
                $save_data1['firstbuyinamout'] = $treadamount;
                $save_data1['sell_point'] = 1.3;
                $save_data1['sell_stop_limit'] = 0.3;
                $save_data1['typeofpoint'] = $coin;
                $save_data1['binance_apikey'] = $binance->binance_apikey;
                $save_data1['binance_securitykey'] = $binance->binance_securitykey;


                $this->common->insert_data('etx_condition_for_btctousdt', $save_data1);

                $data['message'] = lang('set_succ');
                $data['success'] = true;
            } else {
                $data['message'] = "Plz set binance key";
                $data['success'] = false;
            }
        }
        #store stratage 
        echo json_encode($data);
    }
    function stop_bid()
    {
        $coini = $this->input->post('type', true);
        if ($coini != '') {

            $update_data = array(
                'buysell' => 5,
            );
            $this->common->update_data('etx_condition_for_btctousdt', array('user_id' => $this->session->user_id, 'buysell' => 1, 'typeofpoint' => $coini), $update_data);
            $data['message'] = lang('set_succ');
            $data['success'] = true;
        }
        $data['message'] = "Coin not select";
        $data['success'] = false;
        echo json_encode($data);
    }
    function direct_history()
    {
        $data['title'] = "Direct Referral";
        $data['directref'] =  $this->common->getWhere('pwt_notification', array('user_id' => $this->session->user_id, 'type' => 'bottrade1'));
        $this->load->view('trading/bot/direct_history', $data);
    }
    function team_income()
    {
        $data['title'] = "Team Income";
        $data['teamincome'] =  $this->trade->team_income($this->session->user_id);

        $this->load->view('trading/bot/team_income', $data);
    }
    function team_trading()
    {
        $data['title'] = "Team Reward";
        // $data['teamincome'] =  $this->common->getWhere('pwt_bottrade_commission', array('user_id' => $this->session->user_id));
        $data['teamincome'] =  $this->bottreding->gethistory_bot($this->session->user_id);
        $this->load->view('trading/bot/team_trading', $data);
    }
    function personal_profit()
    {
        $data['title'] = "Bot Profit";
        $start = $this->input->post('start');
        $end = $this->input->post('end');
        $data['res'] = $this->trade->get_copy_trade_bot_history($start, $end);



        $this->load->view('trading/bot/personal_profit', $data);
    }

    // cycle check s
    function cyclechek()
    {
        $type = $this->input->post('type', true);
        $data['check_stratage'] = $this->common->getWhereFromLast('bot_stratage', array('id_whom_set' => $this->session->user_id, 'coin' => $type), 1);
        $data['success'] = true;
        echo json_encode($data);
    }
    function cyclestart()
    {
        $type = $this->input->post('type', true);
        $user_id = $this->session->user_id;
        $id =  $this->input->post('id', true);
        if (!empty($type)) {
            $update_data = array(
                'cycle_status' => 1,

            );

            $this->common->update_data('bot_stratage', array('id_whom_set' => $user_id, 'id' => $id), $update_data);
        }
        $data['success'] = true;
        echo json_encode($data);
    }
    function cyclestop()
    {
        $type = $this->input->post('type', true);
        $user_id = $this->session->user_id;
        $id =  $this->input->post('id', true);
        if (!empty($type)) {
            $update_data = array(
                'cycle_status' => 0,

            );

            $this->common->update_data('bot_stratage', array('id_whom_set' => $user_id, 'id' => $id), $update_data);
        }
        $data['success'] = true;
        echo json_encode($data);
    }
}
