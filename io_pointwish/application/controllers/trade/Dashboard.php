<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends MY_Controller
{

	function __construct()
	{
		parent::__construct();

		/****checks it is login or not start****/
		$this->_userLoginCheck();
		/****checks it is login or not End****/	
		$this->load->model('Dashboard_model', 'dashboard');
		$this->load->model('Trading_Model', 'trade');
	}

	public function index()
	{
	
	    $trade = $this->dashboard->get_total_bids();
		$win = $this->dashboard->get_profit_bids();
		$winbids = $trade['totalbids'] == '0' ? 0 : round($win['winbids'] / $trade['totalbids'] * 100, 2);		
		$upbids = $trade['totalbids'] == '0' ? 0 :  round($this->dashboard->get_up_bids() / $trade['totalbids'] * 100, 2 );
		$data = array(		
		    'title'		 => "Dashboard",
			'total_trade' => $trade['totalamount'] ? $trade['totalamount'] : 0,
			'total_profit' => $win['totalprofit'] ? $win['totalprofit'] + ($win['totalprofit'] * 0.95) : 0,			
			'totalbids' => +$trade['totalbids'],
			'winbids' =>  isset($winbids) ? $winbids : 0,
			'lossbids' => $winbids ? 100 - $winbids : 0,
			'upbids' => $upbids,
			'downbids' => $upbids ? 100 - $upbids : 0,
			'copytrade' => $this->trade->get_total_trade_profit(),
			'bids' => $this->dashboard->get_my_bids()

		);
		$this->load->view('trading/dashboard/index', $data);
	}
	
	function change_language($param)
	{
		if ($param) {
			switch ($param) {
				case 'english':
					$lang = 'en';
					break;
				case 'vietnam':
					$lang = 'vn';
					break;
				case 'chienese':
					$lang = 'ch';
					break;
				case 'thai':
					$lang = 'th';
					break;
				case 'combodia':
					$lang = 'kh';
					break;
				case 'japanese':
					$lang = 'jp';
					break;
				case 'indonesian':
					$lang = 'id';
					break;
				case 'russian':
					$lang = "ru";
					break;
				case 'malay':
					$lang = 'my';
					break;
				case 'korean':
					$lang = 'kr';
					break;
			}
			$this->session->set_userdata('lang', $lang);
			$this->lang->load('local', $param);
			$this->session->set_userdata('language', $param);
		}
		redirect(base_url('trade-dashboard'));
	}

	function get_all_bids() {
		$start = $this->input->post('start');
		$end = $this->input->post('end');
		echo json_encode($this->dashboard->get_my_bids($start, $end));
		// echo $this->db->last_query();
	}
	
	function read_notification($id)
	{
		$notification = $this->common->getSingle('pwt_notification', array('user_id' => $this->session->user_id, 'id' => $id));
		if ($notification) {
			$this->common->update_data('pwt_notification', array('user_id' => $this->session->user_id, 'id' => $notification->id), array('is_read' => 1));
			echo json_encode(['target' => $notification->target]);
		}
	}

	function mark_all_read()
	{
		$this->common->update_data('pwt_notification', array('user_id' => $this->session->user_id), array('is_read' => 1));
		echo true;
	}

	// public function get_bids() {
	// 	echo json_encode();
	// }

	// public function get_trade_data() {	
	// 	$trade = $this->dashboard->get_total_bids();
	// 	$win = $this->dashboard->get_profit_bids();
	// 	$data = array(
	// 		'winratio' => round(($win['winbids'] / $trade['totalbids']) * 100, 2),
	// 		'total_trade' => $trade['totalamount'],
	// 		'total_profit' => $win['totalprofit'],
	// 		'winbids' => +$win['winbids'],
	// 		'totalbids' => +$trade['totalbids'],
	// 	);
	// 	echo json_encode($data);
	// }
}
