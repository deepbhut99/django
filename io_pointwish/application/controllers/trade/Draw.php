<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Draw extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        /****checks it is login or not start****/
        $this->_userLoginCheck();
        /****checks it is login or not End****/
        $this->load->model('Draw_model', 'draw');
    }

    public function index() {
        $data['title'] = 'Lucky Draw';
        // $this->draw->getLastWeekFund();
        $this->load->view('trading/trade/draw', $data);
    }

    public function lucky_draw() {

    }
}
