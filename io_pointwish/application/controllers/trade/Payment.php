<?php

use CoinbaseCommerce\ApiClient;
use CoinbaseCommerce\Resources\Charge;
use CoinbaseCommerce\Webhook;

defined('BASEPATH') or exit('No direct script access allowed');

class Payment extends MY_Controller
{

	function __construct()
	{
		parent::__construct();
	}

	function coinbase()
	{
		if ($_POST) {
			$response_data = json_encode($_POST);
			$response = json_decode($response_data);

			if ($response->amount && $response->token) {
				// $apiClientObj = ApiClient::init('55c5f0f4-f763-4e5e-ac0f-d9a745b8ee6e');
				// $apiClientObj->verifySsl(false);
				// $chargeData = array(
				// 	'name' => 'Deposit',
				// 	'description' => 'Deposit USD to wallet',
				// 	'local_price' => array(
				// 		'amount' => $response->amount,
				// 		'currency' => 'USD'
				// 	),
				// 	'meta_data' => array(
				// 		'token' => $response->token,
				// 		'amount' => $response->amount
				// 	),
				// 	'pricing_type' => 'fixed_price'
				// );

				// $result = Charge::create($chargeData);
				// if ($response->type == "pwt") {
				// 	$type = $response->type;
				// 	$response->type = "ethereum";
				// }
				$res['address'] = '0x3268ba3179FDd6738cd207375bd8DE7321bec747';
				$res['amount']  = $response->amount;
				$res['currency']    = 'pwt';
				$res['payments'] = 'coinbase';

				$user_data = $this->common->getSingle('pwt_users', array('id' => $response->token));
				$create_time = date('Y-m-d H:i:s');
				// $insert_data = array(
				// 	'user_id' => $user_data->id,
				// 	'txn_id' => $result->id,
				// 	'amount' => $response->amount,
				// 	'status' => 0,
				// 	'payment_type' => isset($type) ? $type :  $response->type,
				// 	'status_message' => "Waiting For payment",
				// 	'response_data' => json_encode($res),
				// 	'created_datetime' => $create_time,
				// 	'modified_datetime' => $create_time,
				// );
				// $this->common->insert_data('pwt_add_fund_history', $insert_data);

				$insert_note = array(
					'from_user' => $user_data->id,
					'to_user' => $user_data->id,
					'type' => 'fund',
					'payment_type' => isset($type) ? $type :  $response->type,
					'response_data' => json_encode($res),
					'log' => 'credit',
					'amount' => $response->amount,
					'balance' => 'NA',
					'txn_id' => '#D' . rand(),
					'message' => "Waiting For payment",
					'status' => 0,
					'created_datetime' => $create_time,
					'modified_datetime' => $create_time,
					'end_date' => date('Y-m-d', strtotime(' +1 days'))
				);
				$this->common->insert_data('pwt_user_notification', $insert_note);

				header('Content-Type: application/json');
				echo json_encode($res);
				exit;
			} else {
				header('Content-Type: application/json');
				echo $this->_response("error");
				exit;
			}
		} else {
			header('Content-Type: application/json');
			echo $this->_response("error");
			exit;
		}
	}

	function check()
	{
		if ($_POST) {
			$result = $this->common->getWhereFromLast('pwt_user_notification', array('from_user' => $this->session->user_id, 'type' => 'fund', 'status !=' => 100, 'is_read' => 0));
			if ($result) {
				foreach ($result as $response) {
					$apiClientObj = ApiClient::init('55c5f0f4-f763-4e5e-ac0f-d9a745b8ee6e');
					$apiClientObj->verifySsl(false);
					$chargeObj = Charge::retrieve($response['txn_id']);
					$payment = $chargeObj->payments;
					if ($response['status'] == 0) {
						if ($payment[0]['status'] == 'PENDING') {
							$time = date('Y-m-d H:i:s');
							$update_data = array(
								'status' => 2,
								'message' => "Waiting for confirmations",
								'response_data' => json_encode($payment[0]),
								'modified_datetime' => $time,
							);
							$this->common->update_data('pwt_user_notification', array('txn_id' => $response['txn_id']), $update_data);
						}
					}
					if ($response['status'] != 100) {
						if ($payment[0]['status'] == 'CONFIRMED') {
							$pwt = ($payment[0]['value']['local']['amount'] / get_pwt()['current_price']);
							$deposit = $payment[0]['value']['local']['amount'];
							$time = date('Y-m-d H:i:s');
							$update_data = array(
								'status' => 100,
								'message' => "Confirmed",
								'response_data' => json_encode($payment[0]),
								'modified_datetime' => $time,
								'balance' => getWalletAmountByUserID($this->session->user_id)[3] + $pwt,
							);
							$this->common->update_data('pwt_user_notification', array('txn_id' => $response['txn_id']), $update_data);

							/*******Notification **Start****/
							$insert_note = array(
								'user_id' => $this->session->user_id,
								'type' => 'deposit',
								'target' => base_url('wallet'),
								'detail' => "Deposit $ $deposit is confirmed",
								'date' => date('Y-m-d H:i:s'),
							);
							$this->common->insert_data('pwt_notification', $insert_note);
							/*******Notification **Start****/

							/*******Update Wallet**Start*****/
							$pwtBonus = $pwt * 0.1; //10% Bonus	
							$set = '';
							if (date('Y-m-d') < '2022-01-01') {
								$set = ', unlocked_pwt = unlocked_pwt + ' . $pwtBonus . ' + ' . $pwt;
							} else {
								$set = ', unlocked_pwt = unlocked_pwt + ' . $pwt;
							}
							$u_query = "update pwt_users SET package='1', upgrade_time = NOW()" . $set . " where id = '" . $this->session->user_id . "'";
							$this->db->query($u_query);
							/*******Update Wallet**End*****/

							if (date('Y-m-d') < '2022-01-01') {
								/*******Referral History **Start****/
								$insert_note = array(
									'from_user' => $this->session->user_id,
									'to_user' => $this->session->user_id,
									'log' => 'credit',
									'type' => 'Bonus',
									'payment_type' => 'pwt',
									'amount' => $pwtBonus,
									'balance' => getWalletAmountByUserID($this->session->user_id)[3],
									'txn_id' => '#B' . rand(),
									'message' => "Deposit Bonus",
									'status' => 1,
									'created_datetime' => date('Y-m-d H:i:s'),
									'modified_datetime' => date('Y-m-d H:i:s'),
								);
								$this->common->insert_data('pwt_user_notification', $insert_note);
								/*******Referral History **End****/

								/*******Update pwt Supply **Start*****/
								// $u_query = "update pwt_set_price SET total_supply = total_supply -" . $pwtBonus . ", total_supplied = total_supplied + " . $pwt .  " where id = 1";
								// $this->db->query($u_query);
								/*******Update pwt Supply**End*****/

								/*******Notification **Start****/
								$insert_note = array(
									'user_id' => $this->session->user_id,
									'type' => 'bonus',
									'target' => base_url('history'),
									'detail' => "Deposit bonus $pwtBonus USDT is received",
									'date' => date('Y-m-d H:i:s'),
								);
								$this->common->insert_data('pwt_notification', $insert_note);
								/*******Notification **Start****/
							}
						}
					}
				}
			}
			header('Content-Type: application/json');
			echo true;
			exit;
		}
	}

	function nowpayment()
	{
		$amount = (float)$this->input->post('amount');
		$token = "Inv-" . mt_rand(100, 999);
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => 'https://api.nowpayments.io/v1/invoice',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_POSTFIELDS => '{
            "price_amount": ' . $amount . ',
            "price_currency": "usd",            
            "order_id": "' . $token . '",
            "order_description": "Deposit Payment",
            "ipn_callback_url": "' . base_url('trade/payment/pay') . '",
            "success_url": "' . base_url('trade/payment/success_url') . '",
            "cancel_url": "' . base_url('trade/payment/cancel_url') . '"
            }',
			CURLOPT_HTTPHEADER => array(
				'x-api-key: MQEWH74-FCY4YR1-PA9J6VW-QRS8VFV',
				'Content-Type: application/json'
			),
		));

		$response = curl_exec($curl);
		$result = json_decode($response);
		$user_data = $this->common->getSingle('pwt_users', array('id' => $this->session->user_id));

		/*******Insert history**Start*****/
		$create_time = date('Y-m-d H:i:s');

		$insert_note = array(
			'from_user' => $user_data->id,
			'to_user' => $user_data->id,
			'type' => 'fund',
			'payment_type' => 'USDT',
			'response_data' => $response,
			'log' => 'credit',
			'amount' => $amount,
			'pay_amount' => $amount,
			'balance' => 'NA',
			'txn_id' => $result->id,
			'message' => "Waiting For payment",
			'status' => 0,
			'created_datetime' => $create_time,
			'modified_datetime' => $create_time,
			'end_date' => date('Y-m-d', strtotime(' +1 days'))
		);
		$this->common->insert_data('pwt_user_notification', $insert_note);
		/*******Insert history**End*****/
		header('Content-Type: application/json');
		curl_close($curl);
		$datas['payments'] = 'nowpayments';
		$datas['url'] = $result->invoice_url;
		echo json_encode($datas);
	}

	function pay()
	{
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => 'https://api.nowpayments.io/v1/payment/?limit=20&page=0&sortBy=created_at&orderBy=desc',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'GET',
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_HTTPHEADER => array(
				'x-api-key: MQEWH74-FCY4YR1-PA9J6VW-QRS8VFV'
			),
		));

		$response = curl_exec($curl);
		$encode = json_encode($response);
		$result = json_decode($response);

		$fund_data = $this->common->getSingleFromLast('pwt_user_notification', array('from_user' => $this->session->user_id, 'type' => 'fund'));
		if ($fund_data->status != 100) {
			foreach ($result->data as $res) {
				if ($fund_data->txn_id == $res->invoice_id) {
					$amount = $res->outcome_amount;
					if ($res->payment_status == 'confirmed' || $res->payment_status == 'finished') {
						/*******Update history**Start*****/
						$time = date('Y-m-d H:i:s');
						$calc = ($res->actually_paid * 100) / $res->pay_amount;
						if ($calc > 95) {
							$update_data = array(
								'status' => 100,
								'message' => $res->payment_status,
								'amount' => $amount,
								'payment_type' => 'pwt',
								'txn_id' => $res->payment_id,
								'response_data' => json_encode($res),
								'modified_datetime' => $time,
								'balance' => getWalletAmountByUserID($this->session->user_id)[0] + $amount,
							);
							$this->common->update_data('pwt_user_notification', array('txn_id' => $res->invoice_id), $update_data);
							/*******Update history**End*****/

							/*******Notification **Start****/
							$insert_note = array(
								'user_id' => $this->session->user_id,
								'type' => 'deposit',
								'target' => base_url('wallet'),
								'detail' => "Deposit $ " . number_format($res->outcome_amount, 2) . " is confirmed",
								'date' => date('Y-m-d H:i:s'),
							);
							$this->common->insert_data('pwt_notification', $insert_note);
							/*******Notification **Start****/

							/*******Update Wallet**Start*****/
							// $set = ' wallet_amount = wallet_amount + ' . $amount;      
							$u_query = "update pwt_users SET wallet_amount = wallet_amount + $amount where id = '" . $this->session->user_id . "'";
							$this->db->query($u_query);
							/*******Update Wallet**End*****/
						} else {
							/*******Update history**Start*****/
							$update_data = array(
								'status' => 4,
								'message' => 'Waiting for confirmation',
								'txn_id' => $res->payment_id,
								'amount' => $res->pay_amount,
								'response_data' => json_encode($res),
								'modified_datetime' => $time,
							);
							$this->common->update_data('pwt_user_notification', array('txn_id' => $res->invoice_id), $update_data);
							/*******Update history**End*****/
						}
					}
				}
			}
		}
		curl_close($curl);
		$this->session->set_flashdata('success_message', 'Pyament done successfully !');
		redirect(base_url('history'));
	}

	function success_url()
	{
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => 'https://api.nowpayments.io/v1/payment/?limit=20&page=0&sortBy=created_at&orderBy=desc',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'GET',
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_HTTPHEADER => array(
				'x-api-key: MQEWH74-FCY4YR1-PA9J6VW-QRS8VFV'
			),
		));

		$response = curl_exec($curl);
		$encode = json_encode($response);
		$result = json_decode($response);

		$fund_data = $this->common->getSingleFromLast('pwt_user_notification', array('from_user' => $this->session->user_id, 'type' => 'fund'));
		if ($fund_data->status != 100) {
			foreach ($result->data as $res) {
				if ($fund_data->txn_id == $res->invoice_id) {
					$amount = $res->outcome_amount;
					if ($res->payment_status == 'confirmed' || $res->payment_status == 'finished') {
						/*******Update history**Start*****/
						$time = date('Y-m-d H:i:s');
						$calc = ($res->actually_paid * 100) / $res->pay_amount;
						if ($calc > 95) {
							$update_data = array(
								'status' => 100,
								'message' => $res->payment_status,
								'amount' => $amount,
								'payment_type' => 'pwt',
								'txn_id' => $res->payment_id,
								'response_data' => json_encode($res),
								'modified_datetime' => $time,
								'balance' => getWalletAmountByUserID($this->session->user_id)[0] + $amount,
							);
							$this->common->update_data('pwt_user_notification', array('txn_id' => $res->invoice_id), $update_data);
							/*******Update history**End*****/

							/*******Notification **Start****/
							$insert_note = array(
								'user_id' => $this->session->user_id,
								'type' => 'deposit',
								'target' => base_url('wallet'),
								'detail' => "Deposit $ " . number_format($res->outcome_amount, 2) . " is confirmed",
								'date' => date('Y-m-d H:i:s'),
							);
							$this->common->insert_data('pwt_notification', $insert_note);
							/*******Notification **Start****/

							/*******Update Wallet**Start*****/
							// $set = ' wallet_amount = wallet_amount + ' . $amount;      
							$u_query = "update pwt_users SET wallet_amount = wallet_amount + $amount where id = '" . $this->session->user_id . "'";
							$this->db->query($u_query);
							/*******Update Wallet**End*****/
						} else {
							/*******Update history**Start*****/
							$update_data = array(
								'status' => 4,
								'message' => 'Waiting for confirmation',
								'txn_id' => $res->payment_id,
								'amount' => $res->pay_amount,
								'response_data' => json_encode($res),
								'modified_datetime' => $time,
							);
							$this->common->update_data('pwt_user_notification', array('txn_id' => $res->invoice_id), $update_data);
							/*******Update history**End*****/
						}
					}
				}
			}
		}
		curl_close($curl);
		$this->session->set_flashdata('success_message', 'Pyament done successfully !');
		redirect(base_url('history'));
	}

	function cancel_url()
	{
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => 'https://api.nowpayments.io/v1/payment/?limit=20&page=0&sortBy=created_at&orderBy=desc',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'GET',
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_HTTPHEADER => array(
				'x-api-key: MQEWH74-FCY4YR1-PA9J6VW-QRS8VFV'
			),
		));

		$response = curl_exec($curl);
		$encode = json_encode($response);
		$result = json_decode($response);

		$fund_data = $this->common->getSingleFromLast('pwt_user_notification', array('from_user' => $this->session->user_id));

		foreach ($result->data as $res) {
			if ($fund_data->txn_id == $res->invoice_id) {
				// $pwt_Amount = ($res->actually_paid / pwt_price()['current_price']);
				if ($res->payment_status == 'failed' || $res->payment_status == 'expired') {
					/*******Update history**Start*****/
					$update_data = array(
						'status' => -1,
						'txn_id' => $res->invoice_id,
						'status_message' => $res->payment_status,
						'response_data' => json_encode($res),
						'modified_datetime' => date('Y-m-d H:i:s')
					);
					$this->common->update_data('pwt_user_notification', array('txn_id' => $res->invoice_id), $update_data);
					/*******Update history**End*****/
				}
			}
		}

		curl_close($curl);
		$this->session->set_flashdata('error_message', 'Pyament cancelled !');
		redirect(base_url('history'));
	}
}
