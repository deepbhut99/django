<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ticket extends MY_Controller
{

	function __construct()
	{
		parent::__construct();

		/****checks it is login or not start****/
		$this->_userLoginCheck();
		/****checks it is login or not End****/

		$this->load->model('Ticket_model', 'ticket');
	}

	function index()
	{
		$data['title'] = 'Ticket';
		$data['tickets'] = $this->ticket->get_tickets();
		$this->load->view('trading/ticket/index', $data);
	}

	function create_ticket()
	{
		$data['title'] = 'Ticket';
		$ticket = $this->ticket->get_user_ticket();
		if($ticket >= 2)	{
			$this->session->set_flashdata('error_message', 'You can not create more than 2 tickets');
			redirect(base_url('ticket'));
		} else {
    		$this->form_validation->set_rules('subject', 'Subject', 'required');
    		$this->form_validation->set_rules('description', 'Description', 'required');
    
    
    		if ($this->form_validation->run() == FALSE) {
    			$this->session->set_flashdata('error_message', validation_errors());
    			$this->load->view('trading/ticket/index', $data);
    		} else {
    			$ticket_id = '';
    			$insert_data = array(
    				'user_id' => $this->session->userdata('user_id'),
    				'email' => $this->session->user_email,
    				'subject' => $this->input->post('subject', true),
    				'description' => $this->input->post('description', true),
    				'created_datetime' => date('Y-m-d H:i:s'),
    				'modified_datetime' => date('Y-m-d H:i:s'),
    			);
    
    			$insert_id = $this->common->insert_data('pwt_tickets', $insert_data);
    
    			/*******Update Ticket ID **Start****/
    			$ticket_id = 'TC' . (10000 + $insert_id);
    
    			$insert_data = array(
    				'userid' => $this->session->user_id,
    				'detail' => $ticket_id,
    				'date' => date('Y-m-d H:i:s'),
    			);
    			$this->common->insert_data('pwt_logs', $insert_data);
    
    			$update_data = array(
    				'ticket_id' => $ticket_id
    			);
    			$this->common->update_data('pwt_tickets', array('id' => $insert_id), $update_data);
    			/*******Update Ticket ID **End****/
    
    			/*********Insert for conversation ***Start******/
    			$insert_data2 = array(
    				'user_id' => $this->session->userdata('user_id'),
    				'ticket_id' => $ticket_id,
    				'description' => $this->input->post('description', true),
    				'from' => 'user',
    				'created_datetime' => date('Y-m-d H:i:s'),
    				'modified_datetime' => date('Y-m-d H:i:s'),
    			);
    
    			$insert_id2 = $this->common->insert_data('pwt_tickets_chat', $insert_data2);
    
    			/*********Insert for conversation ***End******/
    
    			$this->session->set_flashdata('success_message', 'Ticket generated successfully !');
    
    			redirect(base_url() . 'trade/ticket');
    		}
		}
	}

	function tickets_list($param1 = '', $param2 = '')
	{
		$type = $this->input->get('type', true);


		$status = $this->input->post('status');
		$start = $this->input->post('start');
		$end = $this->input->post('end');

		if ($type == 'all') {
			$data['title'] = 'All Tickets List';
		} else if ($type == 'open') {
			$data['title'] = 'Open Tickets List';
		} else if ($type == 'process') {
			$data['title'] = 'Pending Tickets List';
		} else if ($type == 'close') {
			$data['title'] = 'Close Tickets List';
		}

		if (!empty($_GET['offset'])) {
			$offset = $_GET['offset'];
		} else {
			$offset = 1;
		}

		$per_page = 10;

		$start_limit = ($per_page * $offset) - $per_page;


		$data['status'] = $status;
		$data['start'] = $start;
		$data['end'] = $end;

		$data['type'] = $type;
		$data['per_page'] = $per_page;
		if ($status) {
			$data['data_result'] = $this->ticket->getTicketListByFilter($start, $end, $status);
			$total_records = count($data['data_result']);
		} else {
			$data['data_result'] = $this->ticket->getTicketListByType($start_limit, $per_page);
			$total_records = $this->ticket->getTicketListByTypeCount();
		}

		$config['base_url'] = base_url() . 'user/ticket/tickets_list?type=' . $type;
		$config['total_rows'] = $total_records;
		$config['per_page'] = $per_page;
		//$config["uri_segment"] = 3;
		$config['query_string_segment'] = 'offset';
		$config['page_query_string'] = true;
		// custom paging configuration
		//$config['num_links'] = 4;
		$config['use_page_numbers'] = TRUE;
		$config['reuse_query_string'] = TRUE;


		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';


		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li class="firstlink page-item page-link">';
		$config['first_tag_close'] = '</li>';

		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li class="lastlink page-item page-link">';
		$config['last_tag_close'] = '</li>';

		$config['next_link'] = 'Next';
		$config['next_tag_open'] = '<li class="nextlink page-item page-link">';
		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = 'Prev';
		$config['prev_tag_open'] = '<li class="prevlink page-item page-link">';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active page-item"><a href="#" class="page-link">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li class="numlink page-item page-link">';
		$config['num_tag_close'] = '</li>';

		$this->pagination->initialize($config);

		$data["links"] = $this->pagination->create_links();

		$data["total_records"] = $total_records;


		$this->load->view('user/ticket/ticket_list', $data);
	}

	function view_ticket()
	{
		$ticket_id = $this->input->get('ticket_id', true);

		$ticket_data = $this->common->getSingle('pwt_tickets', array('ticket_id' => $ticket_id));

		if (empty($ticket_data)) {
			redirect(base_url());
		}

		$data['title'] = 'View Ticket';

		$data['data_result'] = $this->common->getSingle('pwt_tickets', array('ticket_id' => $ticket_id));

		$this->load->view('user/ticket/view_ticket', $data);
	}

	function submit_ticket_reply()
	{
		if ($_POST['description'] && $_POST['ticket_id']) {
			$ticket_id = $this->input->post('ticket_id', true);
			$ticket = $this->common->getSingle('pwt_tickets', array('ticket_id' => $ticket_id));
			if($ticket->status == 'close') {
				$this->session->set_flashdata('error_message', 'Ticket is closed');
				redirect(base_url('ticket'));
			} else {
    			$description = $this->input->post('description', true);
    
    			$insert_data = array(
    				'user_id' => $this->session->userdata('user_id'),
    				'ticket_id' => $ticket_id,
    				'description' => $description,
    				'from' => 'user',
    				'created_datetime' => date('Y-m-d H:i:s'),
    				'modified_datetime' => date('Y-m-d H:i:s'),
    			);
    
    			$this->common->insert_data('pwt_tickets_chat', $insert_data);
    			
    			$this->common->update_data('pwt_tickets', array('ticket_id' => $ticket_id), array('status' => 'open'));
    
    			$time = date('h:i');
    
    			$response_data = array('status' => 1, 'message' => $description, 'time' => $time);
    			echo json_encode($response_data);
    			exit;
			}
		} else {
			redirect(base_url());
		}
	}

	function getMessageByTicketID()
	{
		if ($_POST['ticket_id']) {
			$ticket_id = $this->input->post('ticket_id', true);

			$query = "select * from pwt_tickets_chat where ticket_id = '" . $ticket_id . "'";
			$data = $this->db->query($query);

			$message_html = '';

			if ($data->num_rows() > 0) {
				$result = $data->result();

				foreach ($result as $row) {
					if ($row->from == 'user') {
						$message_html .= '<li class="clearfix odd">';
						$message_html .= '<div class="chat-avatar">';
						$message_html .= '<img src="' . base_url() . 'assets/user_panel/images/users/user-3.jpg" class="avatar-xs rounded-circle" alt="avatar">';
						$message_html .= '<span class="time">' . date('h:i', strtotime($row->created_datetime)) . '</span>';
						$message_html .= '</div>';
						$message_html .= '<div class="conversation-text">';
						$message_html .= '<div class="ctext-wrap">';
						$message_html .= '<p>' . $row->description . '</p>';
						$message_html .= '</div>';
						$message_html .= '</div>';
						$message_html .= '</li>';
					} else if ($row->from == 'admin') {
						$message_html .= '<li class="clearfix">';
						$message_html .= '<div class="chat-avatar">';
						$message_html .= '<img src="' . base_url() . 'assets/user_panel/images/users/user-2.jpg" class="avatar-xs rounded-circle" alt="avatar">';
						$message_html .= '<span class="time">' . date('h:i', strtotime($row->created_datetime)) . '</span>';
						$message_html .= '</div>';
						$message_html .= '<div class="conversation-text">';
						$message_html .= '<div class="ctext-wrap">';
						$message_html .= '<p>' . $row->description . '</p>';
						$message_html .= '</div>';
						$message_html .= '</div>';
						$message_html .= '</li>';
					}
				}
			}

			$response_data = array('status' => 1, 'message_html' => $message_html);
			echo json_encode($response_data);
			exit;
		} else {
			redirect(base_url());
		}
	}

	function get_range_tickets()
	{
		$res = $this->ticket->get_tickets($this->input->post('start'), $this->input->post('end'));
		echo json_encode($res);
	}
	
	function close_ticket() {
		$ticket = $this->common->getSingle('pwt_tickets', array('ticket_id' => $this->input->post('ticket_id'), 'user_id' => $this->session->user_id));
		if($ticket) {
			$this->common->update_data('pwt_tickets', array('id' => $ticket->id), array('status' => 'close'));
			$data['success'] = true;
			$this->session->set_flashdata('success_message', 'Closed successfully !');
		} else {
			$data['success'] = false;
			$data['message'] = "Ticket not exist";
		}
		echo json_encode($data);
	}
}
