<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Trading extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        /****checks it is login or not start****/
        $this->_userLoginCheck();
        /****checks it is login or not End****/

        $this->load->model('Trading_Model', 'trade');
        $this->load->model('Dashboard_model', 'dashboard');
        $this->load->model('Bottrading_model', 'bottrade');
        $this->load->model('Bottrading_model', 'bottreding');
    }

    function index($param = '')
    {
        $data['title'] = 'Trading';
        $data['title1'] = 'Trading';
        if (get_pwt()['is_maintenance'] == 1) {
            $this->load->view('trading/trade/index_old', $data);
        } else {
            $user = $this->common->getSingle('pwt_users', array('id' => $this->session->user_id));
            $data['coin'] = $user->coin;
            // $data['is_play'] = $user->is_play;            
            $data['news'] = $this->common->getWhereFromLast('pwt_news_', array());
            $this->load->view('trading/trade/index', $data);
        }
    }

    function trade_test($param = '')
    {
        $data['title'] = 'pwt';
        $data['title1'] = 'pwt';
        if (get_pwt()['is_maintenance'] == 1) {
            $this->load->view('trading/trade/index_old', $data);
        } else {
            $user = $this->common->getSingle('pwt_users', array('id' => $this->session->user_id));
            // $data['coin'] = $user->coin;
            // $data['is_play'] = $user->is_play;
            // $data['news'] = $this->common->getWhereFromLast('pwt_news_', array());
            // $data['history'] =  $this->trade->get_set_bid_forbot();
            // coin deatils
            // $coinname =  ['AAVEUSDT', 'ADAUSDT', 'AKROUSDT', 'ANTUSDT', 'ATOMUSDT', 'BATUSDT', 'BCHUSDT', 'BNBUSDT', 'BTCUSDT', 'CAKEUSDT', 'COMPUSDT', 'COSUSDT', 'CRVUSDT', 'DASHUSDT', 'DOGEUSDT', 'DOTUSDT', 'EOSUSDT', 'ETHUSDT', 'ETCUSDT', 'FILUSDT', 'FTTUSDT', 'GRTUSDT', 'IOSTUSDT', 'IOTAUSDT', 'JSTUSDT', 'KAVAUSDT', 'LINKUSDT', 'LITUSDT', 'LTCUSDT', 'MANAUSDT', 'MDXUSDT', 'NEOUSDT', 'OMGUSDT', 'SUSHIUSDT', 'THETAUSDT', 'TRXUSDT', 'UNIUSDT', 'XMRUSDT', 'XRPUSDT', 'XTZUSDT', 'RVNUSDT', 'SHIBUSDT', 'MATICUSDT', 'CELRUSDT'];
            // foreach ($coinname as $coinname) {
            //     $data[$coinname] = $this->trade->getruningbids($coinname);
            // }
            // profit info

            // $dat = $this->trade->todaysprofit();
            $user_id = $this->session->user_id;
            $data['tptalprofitforbot'] = $this->trade->tptalprofitforbot($user_id);
            $data['todasprofitforbot'] = $this->trade->todasprofitforbot($user_id);

            // user direct follower income count
            $follower =  $this->common->getWhere('pwt_notification', array('user_id' => $this->session->user_id, ' DATE(date) =' => date('Y-m-d')));
            $follower1 =  $this->common->getWhere('pwt_notification', array('user_id' => $this->session->user_id));
            $directincom_fortotal = $this->common->getSingle('pwt_bottrade_tree', array('sponsor_id' => $this->session->user_id));
            $directincom_fortoday = [];
            $teammember = [];
            $teammembertoatal = [];
            foreach ($follower as $row) {
                if ($row->type == 'bottrade1') {
                    $dire = 35;
                    array_push($directincom_fortoday, $dire);
                }
                if ($row->type == 'bottrade2') {
                    $team = 15;
                    array_push($teammember, $team);
                }
                if ($row->type == 'bottrade3') {
                    $team = 25;
                    array_push($teammember, $team);
                }
                if ($row->type == 'bottrade4') {
                    $team = 35;
                    array_push($teammember, $team);
                }
                if ($row->type == 'bottrade5') {
                    $team = 45;
                    array_push($teammember, $team);
                }
                if ($row->type == 'bottrade6') {
                    $team = 55;
                    array_push($teammember, $team);
                }
            }
            foreach ($follower1 as $row) {
                if ($row->type == 'bottrade2') {
                    $team = 15;
                    array_push($teammembertoatal, $team);
                }
                if ($row->type == 'bottrade3') {
                    $team = 25;
                    array_push($teammembertoatal, $team);
                }
                if ($row->type == 'bottrade4') {
                    $team = 35;
                    array_push($teammembertoatal, $team);
                }
                if ($row->type == 'bottrade5') {
                    $team = 45;
                    array_push($teammembertoatal, $team);
                }
                if ($row->type == 'bottrade6') {
                    $team = 55;
                    array_push($teammembertoatal, $team);
                }
            }

            /////// Direct referral 
            $data['directrefincomtotal'] = ($directincom_fortotal->direct_count)  * 35;
            $data['directrefincomtoday'] = array_sum($directincom_fortoday);
            //////// Team income 
            $data['teammember'] = array_sum($teammember);
            $data['teammembertoatal'] = array_sum($teammembertoatal);
            ////// Personal profit
            $data['botapicheck'] = $this->common->getSingle('pwt_users', array('id' => $this->session->user_id));
            $data['wallbal'] = $this->dashboard->get_last_balance($this->session->user_id);

            // Team Trading Income
            $data['todayTeamTradingIncome'] = $this->bottrade->get_teamtrading_income($this->session->user_id);
            $data['totalTeamTtradingIncome'] = $this->bottrade->get_teamtrading_income_fortotal($this->session->user_id);
            //////// Total profit


            $data['totalprofit'] = $this->trade->tptalprofitforbot($user_id) + array_sum($teammembertoatal) + ($directincom_fortotal->direct_count)  * 35 + $this->bottrade->get_teamtrading_income_fortotal($user_id);
            $data['totalprofittoday'] = array_sum($teammember) + array_sum($directincom_fortoday) + $this->trade->todasprofitforbot($user_id) + $this->bottrade->get_teamtrading_income($user_id);


            $this->load->view('trading/trade/trade', $data);
        }
    }

    function getlastvaluforbot()
    {
        $coinname =  ['AAVEUSDT', 'ADAUSDT', 'AKROUSDT', 'ANTUSDT', 'ATOMUSDT', 'BATUSDT', 'BCHUSDT', 'BNBUSDT', 'BTCUSDT', 'CAKEUSDT', 'COMPUSDT', 'COSUSDT', 'CRVUSDT', 'DASHUSDT', 'DOGEUSDT', 'DOTUSDT', 'EOSUSDT', 'ETHUSDT', 'ETCUSDT', 'FILUSDT', 'FTTUSDT', 'GRTUSDT', 'IOSTUSDT', 'IOTAUSDT', 'JSTUSDT', 'KAVAUSDT', 'LINKUSDT', 'LITUSDT', 'LTCUSDT', 'MANAUSDT', 'MDXUSDT', 'NEOUSDT', 'OMGUSDT', 'SUSHIUSDT', 'THETAUSDT', 'TRXUSDT', 'UNIUSDT', 'XMRUSDT', 'XRPUSDT', 'XTZUSDT', 'RVNUSDT', 'SHIBUSDT', 'MATICUSDT', 'CELRUSDT'];
        foreach ($coinname as $coinname) {
            $data[$coinname] = $this->trade->getruningbids($coinname);
        }
        echo json_encode($data);
    }

    function bot_history()
    {
        $start = $this->input->post('start');
        $end = $this->input->post('end');
        $res = $this->trade->get_copy_trade_bot_history($start, $end);
        // $query = $this->db->last_query();
        // var_dump($query) ;
        echo json_encode($res);
    }

    function index_old($param = '')
    {
        $data['coin'] = $this->db->select('coin')->get_where('pwt_users', array('id' => $this->session->user_id))->result_array()[0]['coin'];
        $totalBids = $this->db->get_where('bids', array('user' => $this->session->user_id))->num_rows();
        $winBids = $this->db->get_where('bids', array('user' => $this->session->user_id, 'result !=' => 0))->num_rows();
        $lastBids = $this->db->select('result')->limit('10')->order_by('id', 'desc')->get_where('bids', array('user' => $this->session->user_id))->result_array();
        $wins = 0;

        for ($i = 9; $i >= 0; $i--) {
            if (abs($lastBids[$i]['result']) == 1)
                $wins++;
        }

        $data['winRatio'] =  round(($winBids / $totalBids) * 100, 2);
        $data['lastWinRatio'] = ($wins / 10)  * 100;
        $data['news'] = $this->common->getWhereFromLast('pwt_news', array(), 3);
        $data['title'] = 'Trading';
        $data['title1'] = 'Trading';

        $this->load->view('trading/trade/index', $data);
    }

    function getRatios()
    {
        $totalBids = $this->db->get_where('bids', array('user' => $this->session->user_id))->num_rows();
        $winBids = $this->db->get_where('bids', array('user' => $this->session->user_id, 'result !=' => 0))->num_rows();
        $lastBids = $this->db->select('result')->limit('10')->order_by('id', 'desc')->get_where('bids', array('user' => $this->session->user_id))->result_array();

        if (sizeof($lastBids) > 0) {
            $wins = 0;
            for ($i = 9; $i >= 0; $i--) {
                if (abs($lastBids[$i]['result']) == 1)
                    $wins++;
            }

            $data['winRatio'] =  round(($winBids / $totalBids) * 100, 2);
        } else {
            $data['winRatio'] =  0;
        }

        // $data['lastWinRatio'] = ($wins / 10)  * 100;
        $data['lastWinRatio'] = (int)$this->db->select('lwr')->get_where('pwt_users', array('id' => $this->session->user_id))->result_array()[0]['lwr'];
        if ($data['lastWinRatio'] < 0) {
            $this->db->set('lwr', 0)->where('id', $this->session->user_id)->update('pwt_users');
            $data['lastWinRatio'] = 0;
        }

        echo json_encode($data);
    }

    public function getChartId()
    {
        $charts = (int)$this->db->select('max(chart_id) charts')->get_where('pwt_indicators', array('user_id' => $this->session->user_id))->result_array()[0]['charts'];
        if ($charts > 0) {
            echo json_encode($charts + 1);
        } else {
            echo json_encode(4);
        }
    }

    public function updateTradeType()
    {
        $this->db->where('id', $this->session->user_id)->update('pwt_users', array('trade_type' => $this->input->post('type')));
        $this->session->set_userdata('trade_type', $this->input->post('type'));
    }

    function trade($param = '')
    {
        $data['title'] = lang('best_trading_platform_for_earning');
        $data['type'] = $param;
        if ($param) {
            $this->lang->load('local', $param);
            $this->session->set_userdata('language', $param);
        }
        $this->load->view('trading/trade/index_new', $data);
    }

    function winner()
    {
        redirect(base_url('wallet'));
        $daily = $this->trade->get_winner_list('daily');
        foreach ($daily as  $row) {
            $profit = $this->trade->get_profit_bids('daily', $row['from_user']);
            $daily_data[] = array(
                'id' => $row['id'],
                'username' => $row['username'],
                'profit_share' => $row['profit_share'],
                'min_invest' => $row['min_invest'],
                'copiers' => $row['copiers'],
                'win' => $profit + ($profit * 0.95),
                'amount' => $row['amount']
            );
        }
        $weekly = $this->trade->get_winner_list('weekly');
        foreach ($weekly as  $row) {
            $profit = $this->trade->get_profit_bids('weekly', $row['from_user']);
            $weekly_data[] = array(
                'id' => $row['id'],
                'username' => $row['username'],
                'profit_share' => $row['profit_share'],
                'min_invest' => $row['min_invest'],
                'copiers' => $row['copiers'],
                'win' => $profit + ($profit * 0.95),
                'amount' => $row['amount']
            );
        }
        $monthly = $this->trade->get_winner_list('monthly');
        foreach ($monthly as  $row) {
            $profit = $this->trade->get_profit_bids('monthly', $row['from_user']);
            $monthly_data[] = array(
                'id' => $row['id'],
                'username' => $row['username'],
                'profit_share' => $row['profit_share'],
                'min_invest' => $row['min_invest'],
                'copiers' => $row['copiers'],
                'win' => $profit + ($profit * 0.95),
                'amount' => $row['amount']
            );
        }
        $popular = $this->trade->get_popular_users();
        foreach ($popular as  $row) {
            $popular_data[] = array(
                'id' => $row['id'],
                'username' => $row['username'],
                'profit_share' => $row['profit_share'],
                'min_invest' => $row['min_invest'],
                'copiers' => $row['copiers'],
                'win' => $row['totalprofit'] + ($row['totalprofit'] * 0.95),
            );
        }
        $data = array(
            'title' => lang('trading_winner'),
            'daily' => $daily_data,
            'weekly' =>  $weekly_data,
            'monthly' => $monthly_data,
            'popular' => $popular_data,
        );
        // echo $this->db->last_query();
        $this->load->view('trading/trade/winner', $data);
    }

    function Copy_Trade()
    {

        $totalBids = $this->db->get_where('bids', array('user' => $this->session->user_id, 'type' => 0))->num_rows();
        $winBids = $this->db->get_where('bids', array('user' => $this->session->user_id, 'result' => 1, 'type' => 0))->num_rows();
        $trade = $this->trade->get_last_trade($this->session->user_id);
        $total_bot_play = $this->trade->totalbotplay($this->session->user_id);

        // var_dump($total_bot_play);
        // exit();
        $data = array(
            'title' => 'Copy Trade',
            'win' => $totalBids ? round(($winBids / $totalBids) * 100, 2) : 0,
            'trade' => $trade['totalbids'],
            'trade_amount' => $trade['totalamount'],
            'profit' => $this->trade->get_last_trade($this->session->user_id, 'weekly')['totalamount'],
            'user_data' => $this->common->getSingle('pwt_users', array('id' => $this->session->user_id)),
            'total_trade' => $this->trade->get_total_trade(),
            'total_profit' => $this->trade->get_total_trade_profit(),
            'history' => $this->trade->get_copy_trade_history(),
            'copiers' => $this->trade->get_copiers(),
            'peers' => $this->trade->get_peers_history(),
            'earn_commission' => $this->trade->get_earn_commission(),
            'total_bot_play' => $total_bot_play
        );
        $this->load->view('trading/trade/copy_trade', $data);
    }

    function register_expert()
    {
        $trade = $this->trade->get_last_trade($this->session->user_id);
        $profit = $this->trade->get_last_trade($this->session->user_id, 'weekly')['totalamount'];
        $total_bot_play = $this->trade->totalbotplay($this->session->user_id);

        if ($total_bot_play->totalbidforbot >= 16) {
            $this->form_validation->set_rules('profit', 'Profit', 'required');
            $this->form_validation->set_rules('invest', 'Minimum Investment', 'required');
            $this->form_validation->set_rules('pwt', 'pwtstrategy', 'required');

            if ($this->input->post('pwt', true) == 'Yes') {
                $copy_trade_forbot = 1;
            } else {

                $copy_trade_forbot = 0;
            }
            if ($this->form_validation->run() == FALSE) {
                $datas['success'] = false;
                $datas['message'] = validation_errors();
            } else {
                $update_data = array(
                    'copy_trade' => 1,
                    'copy_trade_forbot' => $copy_trade_forbot,
                    'profit_share' => $this->input->post('profit', true),
                    'min_invest' => $this->input->post('invest', true),
                );
                $this->common->update_data('pwt_users', array('id' => $this->session->user_id), $update_data);
                $datas['success'] = true;
                $this->session->set_flashdata('success_message', "Added Successfully");
            }
        } else {
            $datas['success'] = false;
            $datas['message'] = "You not eligible for expert";
        }
        echo json_encode($datas);
    }

    function disable_expert()
    {
        $update_data = array(
            'copy_trade' => 0,
            'profit_share' => 0,
            'min_invest' => 0,
            'users' => '',
            'copy_trade_forbot' => 0
        );
        $this->common->update_data('pwt_users', array('id' => $this->session->user_id), $update_data);
        $this->db->delete('pwt_copy_trade', array('master_user_id' => $this->session->user_id));
        $this->session->set_flashdata('success_message', "Disabled Successfully");
        echo 1;
        exit;
    }

    function top_gainers($most = '')
    {
        // $self = $this->common->getSingle('pwt_users', array('id' => $this->session->user_id));
        $users = $this->trade->get_top_gainers($most);
        $data = array();
        foreach ($users as $user) {
            $id = $this->session->user_id;
            $temp =  explode(',', $user['users']);
            $pattern = "$id";
            if (in_array($pattern, $temp) == true) {
                $status = true;
            } else {
                $status = false;
            }
            $winBids = $this->db->get_where('bids', array('user' => $user['id'], 'result !=' => 0, 'type' => 0))->num_rows();
            $data[] = array(
                'userid' => $user['id'],
                'full_name' => $user['full_name'],
                'copiers' => $user['copiers'],
                'profit_share' => $user['profit_share'],
                'min_invest' => $user['min_invest'],
                'win_ratio' => round(($winBids / $user['totalbids']) * 100, 2),
                'status' => $status
            );
        }
        echo json_encode($data);
    }

    function start_copy()
    {
        $users = $this->common->getSingle('pwt_users', array('id' => $this->input->post('userid')));
        if ($users->copy_trade == 1) {
            $insert = array(
                'user_id' => $this->session->user_id,
                'master_user_id' => $this->input->post('userid'),
                'profit_sharing' => $users->profit_share,
                'minimum_invest' => $users->min_invest,
                'start_date' => date('Y-m-d')
            );
            $this->common->insert_data('pwt_copy_trade', $insert);
            /*******Update Users**Start*****/
            $update = array(
                'users' => $users->users ? $users->users . ',' . $this->session->user_id : 'x' . ',' . $this->session->user_id,
                'copiers' => $users->copiers + 1
            );
            $this->common->update_data('pwt_users', array('id' => $this->input->post('userid')), $update);
            /*******Update Users**End*****/
            $data['success'] = true;
            $data['message'] = "Copied Successfully";
        } else {
            $data['success'] = false;
            $data['message'] = "It's not a expert user";
        }
        echo json_encode($data);
    }

    function stop_copy()
    {
        $users = $this->common->getSingle('pwt_users', array('id' => $this->input->post('userid')));
        if ($users->copy_trade == 1) {
            $where = array(
                'user_id' => $this->session->user_id,
                'master_user_id' => $this->input->post('userid'),
            );
            $this->db->delete('pwt_copy_trade', $where);
            /*******Update Users**Start*****/
            $search = ',' . $this->session->user_id;
            $update = array(
                'users' => str_replace($search, "", $users->users),
                'copiers' => $users->copiers - 1
            );
            $this->common->update_data('pwt_users', array('id' => $this->input->post('userid')), $update);
            /*******Update Users**End*****/
            $data['success'] = true;
            $data['message'] = "Removed Successfully";
        } else {
            $data['success'] = false;
            $data['message'] = "It's not a expert user";
        }
        echo json_encode($data);
    }


    function get_portfolio_lists()
    {
        // $self = $this->common->getSingle('pwt_users', array('id' => $this->session->user_id));
        $users = $this->trade->get_portfolio_lists();
        $datas = array();
        foreach ($users as $user) {
            // $id = $user['id'];
            // $pattern = "/$id/i";
            // if (preg_match($pattern, $self->users) == 1) {
            //     $status = true;
            // } else {
            //     $status = false;
            // }
            $winBids = $this->db->get_where('bids', array('user' => $user['id'], 'result !=' => 0, 'type' => 0))->num_rows();
            $datas[] = array(
                'userid' => $user['id'],
                'full_name' => $user['full_name'],
                'copiers' => $user['copiers'],
                'profit_share' => $user['profit_share'],
                'min_invest' => $user['min_invest'],
                'win_ratio' => round(($winBids / $user['totalbids']) * 100, 2),
                'status' => true
            );
        }
        // var_dump($datas);
        // exit(0);
        echo json_encode($datas);
    }

    function get_range_copiers()
    {
        $start = $this->input->post('start');
        $end = $this->input->post('end');
        $res = $this->trade->get_copiers($start, $end);
        echo json_encode($res);
        // echo $this->db->last_query();
    }

    function get_copytrade_history()
    {
        $start = $this->input->post('start');
        $end = $this->input->post('end');
        $res = $this->trade->get_copy_trade_history($start, $end);
        echo json_encode($res);
    }

    function get_earntrade_history()
    {
        $start = $this->input->post('start');
        $end = $this->input->post('end');
        $res = $this->trade->get_peers_history($start, $end);
        echo json_encode($res);
    }

    function update_coin()
    {
        $this->common->update_data('pwt_users', array('id' => $this->session->user_id), array('coin' => $this->input->post('coin')));
        echo true;
    }

    function community()
    {
        $totalBids = $this->db->get_where('bids', array('user' => $this->session->user_id, 'type' => 0))->num_rows();
        $winBids = $this->db->get_where('bids', array('user' => $this->session->user_id, 'result' => 1, 'type' => 0))->num_rows();
        $trade = $this->trade->get_last_trade($this->session->user_id);
        $total_bot_play = $this->trade->totalbotplay($this->session->user_id);

        // var_dump($total_bot_play);
        // exit();
        $data = array(
            'title' => 'Community',
            'activetitle' =>"community",
            'win' => $totalBids ? round(($winBids / $totalBids) * 100, 2) : 0,
            'trade' => $trade['totalbids'],
            'trade_amount' => $trade['totalamount'],
            'profit' => $this->trade->get_last_trade($this->session->user_id, 'weekly')['totalamount'],
            'user_data' => $this->common->getSingle('pwt_users', array('id' => $this->session->user_id)),
            'total_trade' => $this->trade->get_total_trade(),
            'total_profit' => $this->trade->get_total_trade_profit(),
            'history' => $this->trade->get_copy_trade_history(),
            'copiers' => $this->trade->get_copiers(),
            'peers' => $this->trade->get_peers_history(),
            'earn_commission' => $this->trade->get_earn_commission(),
            'total_bot_play' => $total_bot_play
        );
        $this->load->view('trading/copytrade/community', $data);
    }
    function portfolio()
    {
        
        $totalBids = $this->db->get_where('bids', array('user' => $this->session->user_id, 'type' => 0))->num_rows();
        $winBids = $this->db->get_where('bids', array('user' => $this->session->user_id, 'result' => 1, 'type' => 0))->num_rows();
        $trade = $this->trade->get_last_trade($this->session->user_id);
        $total_bot_play = $this->trade->totalbotplay($this->session->user_id);

        // var_dump($total_bot_play);
        // exit();
        $data = array(
            'title' => 'Portfolio',
            'activetitle' =>"portfolio",
            'win' => $totalBids ? round(($winBids / $totalBids) * 100, 2) : 0,
            'trade' => $trade['totalbids'],
            'trade_amount' => $trade['totalamount'],
            'profit' => $this->trade->get_last_trade($this->session->user_id, 'weekly')['totalamount'],
            'user_data' => $this->common->getSingle('pwt_users', array('id' => $this->session->user_id)),
            'total_trade' => $this->trade->get_total_trade(),
            'total_profit' => $this->trade->get_total_trade_profit(),
            'history' => $this->trade->get_copy_trade_history(),
            'copiers' => $this->trade->get_copiers(),
            'peers' => $this->trade->get_peers_history(),
            'earn_commission' => $this->trade->get_earn_commission(),
            'total_bot_play' => $total_bot_play
        );
        $this->load->view('trading/copytrade/portfolio', $data);
    }
    function expert_area()
    {
        $totalBids = $this->db->get_where('bids', array('user' => $this->session->user_id, 'type' => 0))->num_rows();
        $winBids = $this->db->get_where('bids', array('user' => $this->session->user_id, 'result' => 1, 'type' => 0))->num_rows();
        $trade = $this->trade->get_last_trade($this->session->user_id);
        $total_bot_play = $this->trade->totalbotplay($this->session->user_id);

        // var_dump($total_bot_play);
        // exit();
        $data = array(
            'title' => 'Expert Area',
            'activetitle' => "expert-area",
            'win' => $totalBids ? round(($winBids / $totalBids) * 100, 2) : 0,
            'trade' => $trade['totalbids'],
            'trade_amount' => $trade['totalamount'],
            'profit' => $this->trade->get_last_trade($this->session->user_id, 'weekly')['totalamount'],
            'user_data' => $this->common->getSingle('pwt_users', array('id' => $this->session->user_id)),
            'total_trade' => $this->trade->get_total_trade(),
            'total_profit' => $this->trade->get_total_trade_profit(),
            'history' => $this->trade->get_copy_trade_history(),
            'copiers' => $this->trade->get_copiers(),
            'peers' => $this->trade->get_peers_history(),
            'earn_commission' => $this->trade->get_earn_commission(),
            'total_bot_play' => $total_bot_play
        );
        $this->load->view('trading/copytrade/expert_area', $data);
    }
    function my_status()
    {
        $data['title'] = "My Status";
        $data['activetitle'] = "my-status";


        $user_id = $this->session->user_id;
        $data['tptalprofitforbot'] = $this->trade->tptalprofitforbot($user_id);
        $data['todasprofitforbot'] = $this->trade->todasprofitforbot($user_id);

        // user direct follower income count
        $follower =  $this->common->getWhere('pwt_notification', array('user_id' => $this->session->user_id, ' DATE(date) =' => date('Y-m-d')));
        $follower1 =  $this->common->getWhere('pwt_notification', array('user_id' => $this->session->user_id));
        $directincom_fortotal = $this->common->getSingle('pwt_bottrade_tree', array('sponsor_id' => $this->session->user_id));
        $directincom_fortoday = [];
        $teammember = [];
        $teammembertoatal = [];
        foreach ($follower as $row) {
            if ($row->type == 'bottrade1') {
                $dire = 35;
                array_push($directincom_fortoday, $dire);
            }
            if ($row->type == 'bottrade2') {
                $team = 15;
                array_push($teammember, $team);
            }
            if ($row->type == 'bottrade3') {
                $team = 25;
                array_push($teammember, $team);
            }
            if ($row->type == 'bottrade4') {
                $team = 35;
                array_push($teammember, $team);
            }
            if ($row->type == 'bottrade5') {
                $team = 45;
                array_push($teammember, $team);
            }
            if ($row->type == 'bottrade6') {
                $team = 55;
                array_push($teammember, $team);
            }
        }
        foreach ($follower1 as $row) {
            if ($row->type == 'bottrade2') {
                $team = 15;
                array_push($teammembertoatal, $team);
            }
            if ($row->type == 'bottrade3') {
                $team = 25;
                array_push($teammembertoatal, $team);
            }
            if ($row->type == 'bottrade4') {
                $team = 35;
                array_push($teammembertoatal, $team);
            }
            if ($row->type == 'bottrade5') {
                $team = 45;
                array_push($teammembertoatal, $team);
            }
            if ($row->type == 'bottrade6') {
                $team = 55;
                array_push($teammembertoatal, $team);
            }
        }

        /////// Direct referral 
        $data['directrefincomtotal'] = ($directincom_fortotal->direct_count)  * 35;
        $data['directrefincomtoday'] = array_sum($directincom_fortoday);
        //////// Team income 
        $data['teammember'] = array_sum($teammember);
        $data['teammembertoatal'] = array_sum($teammembertoatal);
        ////// Personal profit
        $data['botapicheck'] = $this->common->getSingle('pwt_users', array('id' => $this->session->user_id));
        $data['wallbal'] = $this->dashboard->get_last_balance($this->session->user_id);

        // Team Trading Income
        $data['todayTeamTradingIncome'] = $this->bottrade->get_teamtrading_income($this->session->user_id);
        $data['totalTeamTtradingIncome'] = $this->bottrade->get_teamtrading_income_fortotal($this->session->user_id);
        //////// Total profit


        $data['totalprofit'] = $this->trade->tptalprofitforbot($user_id) + array_sum($teammembertoatal) + ($directincom_fortotal->direct_count)  * 35 + $this->bottrade->get_teamtrading_income_fortotal($user_id);
        $data['totalprofittoday'] = array_sum($teammember) + array_sum($directincom_fortoday) + $this->trade->todasprofitforbot($user_id) + $this->bottrade->get_teamtrading_income($user_id);
        $start = '';
        $end = '';
        $data['table2'] = $this->trade->get_copy_trade_bot_history($start,$end);
        $data['table1'] =  $this->bottreding->gethistory_bot($this->session->user_id);

        $this->load->view('trading/bot/my_status', $data);
    }
    function binance()
    {
        $data['title'] = "Binance";
        $data['activetitle'] = "binance";

        $user = $this->common->getSingle('pwt_users', array('id' => $this->session->user_id));
        // $data['coin'] = $user->coin;
        // $data['is_play'] = $user->is_play;
        // $data['news'] = $this->common->getWhereFromLast('pwt_news_', array());
        // $data['history'] =  $this->trade->get_set_bid_forbot();
        // coin deatils
        // $coinname =  ['AAVEUSDT', 'ADAUSDT', 'AKROUSDT', 'ANTUSDT', 'ATOMUSDT', 'BATUSDT', 'BCHUSDT', 'BNBUSDT', 'BTCUSDT', 'CAKEUSDT', 'COMPUSDT', 'COSUSDT', 'CRVUSDT', 'DASHUSDT', 'DOGEUSDT', 'DOTUSDT', 'EOSUSDT', 'ETHUSDT', 'ETCUSDT', 'FILUSDT', 'FTTUSDT', 'GRTUSDT', 'IOSTUSDT', 'IOTAUSDT', 'JSTUSDT', 'KAVAUSDT', 'LINKUSDT', 'LITUSDT', 'LTCUSDT', 'MANAUSDT', 'MDXUSDT', 'NEOUSDT', 'OMGUSDT', 'SUSHIUSDT', 'THETAUSDT', 'TRXUSDT', 'UNIUSDT', 'XMRUSDT', 'XRPUSDT', 'XTZUSDT', 'RVNUSDT', 'SHIBUSDT', 'MATICUSDT', 'CELRUSDT'];
        // foreach ($coinname as $coinname) {
        //     $data[$coinname] = $this->trade->getruningbids($coinname);
        // }
        // profit info

        // $dat = $this->trade->todaysprofit();
        $user_id = $this->session->user_id;
        $data['tptalprofitforbot'] = $this->trade->tptalprofitforbot($user_id);
        $data['todasprofitforbot'] = $this->trade->todasprofitforbot($user_id);

        // user direct follower income count
        $follower =  $this->common->getWhere('pwt_notification', array('user_id' => $this->session->user_id, ' DATE(date) =' => date('Y-m-d')));
        $follower1 =  $this->common->getWhere('pwt_notification', array('user_id' => $this->session->user_id));
        $directincom_fortotal = $this->common->getSingle('pwt_bottrade_tree', array('sponsor_id' => $this->session->user_id));
        $directincom_fortoday = [];
        $teammember = [];
        $teammembertoatal = [];
        foreach ($follower as $row) {
            if ($row->type == 'bottrade1') {
                $dire = 35;
                array_push($directincom_fortoday, $dire);
            }
            if ($row->type == 'bottrade2') {
                $team = 15;
                array_push($teammember, $team);
            }
            if ($row->type == 'bottrade3') {
                $team = 25;
                array_push($teammember, $team);
            }
            if ($row->type == 'bottrade4') {
                $team = 35;
                array_push($teammember, $team);
            }
            if ($row->type == 'bottrade5') {
                $team = 45;
                array_push($teammember, $team);
            }
            if ($row->type == 'bottrade6') {
                $team = 55;
                array_push($teammember, $team);
            }
        }
        foreach ($follower1 as $row) {
            if ($row->type == 'bottrade2') {
                $team = 15;
                array_push($teammembertoatal, $team);
            }
            if ($row->type == 'bottrade3') {
                $team = 25;
                array_push($teammembertoatal, $team);
            }
            if ($row->type == 'bottrade4') {
                $team = 35;
                array_push($teammembertoatal, $team);
            }
            if ($row->type == 'bottrade5') {
                $team = 45;
                array_push($teammembertoatal, $team);
            }
            if ($row->type == 'bottrade6') {
                $team = 55;
                array_push($teammembertoatal, $team);
            }
        }

        /////// Direct referral 
        $data['directrefincomtotal'] = ($directincom_fortotal->direct_count)  * 35;
        $data['directrefincomtoday'] = array_sum($directincom_fortoday);
        //////// Team income 
        $data['teammember'] = array_sum($teammember);
        $data['teammembertoatal'] = array_sum($teammembertoatal);
        ////// Personal profit
        $data['botapicheck'] = $this->common->getSingle('pwt_users', array('id' => $this->session->user_id));
        $data['wallbal'] = $this->dashboard->get_last_balance($this->session->user_id);

        // Team Trading Income
        $data['todayTeamTradingIncome'] = $this->bottrade->get_teamtrading_income($this->session->user_id);
        $data['totalTeamTtradingIncome'] = $this->bottrade->get_teamtrading_income_fortotal($this->session->user_id);
        //////// Total profit


        $data['totalprofit'] = $this->trade->tptalprofitforbot($user_id) + array_sum($teammembertoatal) + ($directincom_fortotal->direct_count)  * 35 + $this->bottrade->get_teamtrading_income_fortotal($user_id);
        $data['totalprofittoday'] = array_sum($teammember) + array_sum($directincom_fortoday) + $this->trade->todasprofitforbot($user_id) + $this->bottrade->get_teamtrading_income($user_id);

        $this->load->view('trading/bot/binance', $data);
    }
    function wazirx()
    {
        $data['title'] = "Wazirx";
        $data['activetitle'] = "wazirx";
        $this->load->view('trading/bot/wazirx', $data);
    }
    function binfuture()
    {
        $data['title'] = "Binance Future";
        $data['activetitle'] = "binance-future";
        $this->load->view('trading/bot/binance_future', $data);
    }
    function bothistory()
    {
        $data['title'] = "Bot History";
        $data['activetitle'] = "bot-history";
        $start = $this->input->post('start');
        $end = $this->input->post('end');
        $data['res'] = $this->trade->get_copy_trade_bot_history($start, $end);
        $this->load->view('trading/bot/bot_history', $data);
    }
}
