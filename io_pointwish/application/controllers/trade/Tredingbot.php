<?php
require_once APPPATH . '/third_party/googleLib/GoogleAuthenticator.php';
defined('BASEPATH') or exit('No direct script access allowed');

class Tredingbot extends MY_Controller
{
    
    function __construct()
    {
        parent::__construct();

        /****checks it is login or not start****/
        $this->_userLoginCheck();
        /****checks it is login or not End****/
        $this->load->model('Document_model', 'document');
        $this->load->model('Dashboard_model', 'dashboard');
        $this->load->model('Notification_model', 'notification');
    }

}
