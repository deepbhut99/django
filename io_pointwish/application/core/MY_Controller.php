<?php

class MY_Controller extends CI_Controller
{

	public $user;
    
    function __construct(){

        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('pagination');
        $this->load->library('email');

        $this->load->library('upload');   

        $this->load->helper('cookie');
        $this->load->helper('string');
        $this->load->helper('html');
        $this->lang->load('local', 'english');   
        
        // Load PHPMailer library
        $this->load->library('phpmailer_lib'); 

        if (isset($this->session->language)) {
			$this->lang->load('local', $this->session->language);
		}
		// $url = $this->uri->segment(1);
        // $flag = false;
        // $device = systemInfo();
        // if ($device['device'] == "MOBILE" && $device['source'] == "WEB" && $device['os'] == 'Android' ) {
        //     $flag = true;
        //     if ($url == 'page' || $url == 'home' || $url == 'reset_password' || $url == 'logout' || $url == 'verification' || $url == 'maintenance' || !$url) {
        //         $flag = false;
        //     }
        // }
        // if ($flag) {
        //     $this->session->set_flashdata('invalid', true);
        //     redirect(base_url());
        // }

        $this->load->model('Common_model', 'common');
        date_default_timezone_set('UTC');
    }

    protected function _response($response = array())
    {
        return json_encode($response);
    }

    protected function _isTokenExist($token)
    {
        $query = "select * from pwt_users where token = '".$token."' and is_delete = '1'";
        $result = $this->db->query($query); 
        if($result->num_rows() == 0)
        {
            return 0;
        }
        else
        {
            return 1;
        }

    }

    protected function _userLoginCheck()
    {
        if($this->session->userdata('user_id') == '')
        {
            $next = current_url();

            // redirect(base_url().'login?next='.$next);
            redirect(base_url().'home');
            exit();
        }
    }

    function _adminLoginCheck()
    {
        if($this->session->userdata('admin_id') == '')
        {
            $next = current_url();
            redirect('admin/login?next='.$next);
            exit();
        }
    }


    function send_sms_notification($message, $mobile_number)
    {
        $AUTH_KEY = SMS_AUTH_KEY;
        $url = 'http://msg.icloudsms.com/rest/services/sendSMS/sendGroupSms?AUTH_KEY='.$AUTH_KEY.'&message='.$message.'&senderId=PTWISH&routeId=1&mobileNos='.$mobile_number.'&smsContentType=english';

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "postman-token: cf477dcf-c2ec-aabe-b39a-d01e84066949"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          //echo "cURL Error #:" . $err;
        } else {
          //echo $response;
        }
    }


}
