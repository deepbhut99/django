<?php

if (!function_exists('getSessionData')) {
	function getSessionData()
	{
		$CI = &get_instance();

		$CI->db->where(array('id' => $CI->session->userdata('user_id')));

		$data = $CI->db->get('pwt_users');

		if ($data->num_rows() > 0) {
			return $data->row();
		} else {
			return false;
		}
	}
}

if (!function_exists('getMemberNameByID')) {
	function getMemberNameByID($user_id)
	{
		$CI = &get_instance();

		$CI->db->where(array('id' => $user_id));
		$data = $CI->db->get('pwt_users');
		$result =  $data->row();

		return $result->username;
	}
}

function getWalletAmountByUserID($user_id)
{
	$CI = &get_instance();

	$query = "select wallet_amount, demo_wallet_amount, pwt_wallet, unlocked_pwt from pwt_users where id = '" . $user_id . "'";
	$data = $CI->db->query($query);
	$row = $data->row();
	return [$row->wallet_amount, $row->demo_wallet_amount, $row->pwt_wallet, $row->unlocked_pwt];
	
}

function get_my_bids()
{
	$CI = &get_instance();
	$total_bid = $CI->db->get_where('bids', array('user' => $CI->session->user_id))->num_rows();
	$win = $CI->db->get_where('bids', array('user' => $CI->session->user_id, 'result' => 1))->num_rows();
	// $CI->db->select('result');
	// $CI->db->limit('10');
	// $CI->db->order_by('id', 'desc');
	$last_bid = $CI->db->select('result')->limit('10')->order_by('id', 'desc')->get_where('bids', array('user' => $CI->session->user_id, 'result !=' => -1))->result_array();
	$data = array(
		'total_bid' => $total_bid,
		'win' => $win,
		'last_bid' => $last_bid
	);	
	return $data;
}

function get_pwt() {
	$CI = &get_instance();
	return $CI->db->get('pwt_set_price')->row_array();
}

if(!function_exists('getCountryList'))
	{
	 	function getCountryList()
		{
			$CI =& get_instance();

			$data = $CI->db->get('pwt_countries');

			if($data->num_rows() > 0)
			{
				return $data->result();		
			}
			else
			{
				return false;
			}
		}
	}
	
function logout_session($logid) {
	$CI = &get_instance();
	return $CI->db->get_where('pwt_log_history', ['id' => $logid])->row_array();
}

function systemInfo()
{
	$user_agent = $_SERVER['HTTP_USER_AGENT'];
	$os_platform    = "Unknown OS Platform";
	$os_array       = array(
		'/windows phone 8/i'    =>  'Windows Phone 8',
		'/windows phone os 7/i' =>  'Windows Phone 7',
		'/windows nt 6.3/i'     =>  'Windows 8.1',
		'/windows nt 6.2/i'     =>  'Windows 8',
		'/windows nt 6.1/i'     =>  'Windows 7',
		'/windows nt 6.0/i'     =>  'Windows Vista',
		'/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
		'/windows nt 5.1/i'     =>  'Windows XP',
		'/windows xp/i'         =>  'Windows XP',
		'/windows nt 5.0/i'     =>  'Windows 2000',
		'/windows me/i'         =>  'Windows ME',
		'/win98/i'              =>  'Windows 98',
		'/win95/i'              =>  'Windows 95',
		'/win16/i'              =>  'Windows 3.11',
		'/windows|win32/i'      =>  'Windows 10',
		'/macintosh|mac os x/i' =>  'Mac OS X',
		'/mac_powerpc/i'        =>  'Mac OS 9',
		'/linux/i'              =>  'Linux',
		'/ubuntu/i'             =>  'Ubuntu',
		'/iphone/i'             =>  'iPhone',
		'/ipod/i'               =>  'iPod',
		'/ipad/i'               =>  'iPad',
		'/android/i'            =>  'Android',
		'/blackberry/i'         =>  'BlackBerry',
		'/webos/i'              =>  'Mobile'
	);
	$found = false;	
	$device = '';
	foreach ($os_array as $regex => $value) {
		if ($found)
			break;
		else if (preg_match($regex, $user_agent)) {
			$os_platform    =   $value;
			$device = !preg_match('/(windows|mac|linux|ubuntu)/i', $os_platform)
				? 'MOBILE' : (preg_match('/phone/i', $os_platform) ? 'MOBILE' : 'SYSTEM');
		}
	}
	$device = !$device ? 'SYSTEM' : $device;
	$source = (strpos($_SERVER['HTTP_USER_AGENT'], $os_platform == 'iPhone' ? 'SwingWebView' : 'SWING2APP') > 0 ? 'APP' : 'WEB');
	return array('os' => $os_platform, 'device' => $device, 'source' => $source);
}

function get_referral_users($user_id)
{
	$CI = &get_instance();
	$CI->db->select('count(*) as total_users');
	return $CI->db->get_where('pwt_users', array('sponsor_id' => $user_id, 'is_status' => 1))->row()->total_users;
}

function get_notification($user_id) {
	$CI = &get_instance();
	$CI->db->order_by('id', 'desc');
	return $CI->db->get_where('pwt_notification', array('user_id' => $user_id, 'is_read' => 0))->result_array();
}

function get_total_deposit() {
	$CI = &get_instance();
	$CI->db->select("sum(amount) as total");
	return $CI->db->get_where('pwt_user_notification', array('from_user' => $CI->session->user_id, 'type' => 'fund', 'status' => 100))->row()->total;
}