<?php

//---------- Header Light -----------

$lang['home'] = 'Zuhause';
$lang['feature'] = 'Eigenschaften';
$lang['roadmap'] = 'Roadmap';
$lang['faq'] = 'FAQ';
$lang['news'] = 'Nachrichten';
$lang['contact'] = 'Kontakt';
$lang['signin'] = 'Anmelden / Anmelden';
$lang['dashboard'] = 'Instrumententafel';
$lang['signinn'] = 'Anmelden';
$lang['signupp'] = 'Anmelden';


//------------ Home Light ------------

$lang['download_on'] = 'Download auf';
$lang['apple_store'] = 'Apple Laden';
$lang['google_play'] = 'Google Play';
$lang['earn'] = 'Verdiene beim Spielen';
$lang['predict'] = 'Prognostizieren Sie den Trend und verdienen Sie Geld. Ein Benutzer kann auch seine Gewinnstrategie verkaufen und vom Gewinnbonus profitieren';
$lang['using_free'] = 'Starten Sie die kostenlose Nutzung';
$lang['explore_feature'] = 'Funktionen erkunden';
$lang['fast_opt'] = 'Schnell und optimiert';
$lang['system_traffic'] = 'Unser System kann den Datenverkehr problemlos verwalten.';
$lang['easy'] = 'Einfach zu verwenden';
$lang['user_friend'] = 'Benutzerfreundliche Oberfläche mit einfachem Zugriff.';
$lang['mul_income'] = 'Mehrfacheinkommen';
$lang['trading-home'] = 'Handel, Belohnung und Verkauf von Strategien.';
$lang['advance_feat'] = 'Erweiterte Funktionen geben Ihnen die volle Kontrolle.';
$lang['play_copy'] = 'Spielen, Handel kopieren & verdienen';
$lang['knowledge'] = 'Handelskenntnisse sind auf dieser Plattform nicht wichtig. Folgen Sie einfach Top-Tradern und verwenden Sie ihre Erfolgsstrategien zu normalen Bedingungen für Sie.';
$lang['demo_accc'] = 'Ein Demo-Konto ist für alle verfügbar, die ihre Fähigkeiten verbessern oder neue Strategien entwickeln möchten. Mehrere Währungen für den Handel geben Ihnen mehr Gewinnchancen.';
$lang['inc_bet'] = 'Erhöhen Sie den Einsatz um das 10-fache (x10). Ausgehend von Double (x2) verdienen Sie umso mehr, je mehr Sie spielen. Spielen Sie ein Spiel mit eigenen Strategien oder kopieren Sie es von der Rangliste. Es ist einfach zu kopieren und im Spiel zu implementieren. Sie müssen Händler gemäß ihren Bedingungen bezahlen oder eine kostenlose Handelsstrategie anwenden.';
$lang['disc_power'] = 'Entdecken Sie ein leistungsstarkes Tool für Händler.';
$lang['trade_info'] = 'Handelsinformationen';
$lang['trade_skill'] = 'Der Handel erforderte spezielle Schulungen, Fähigkeiten und analytische Fähigkeiten. Der Benutzer muss den Markttrend und die Marktdynamik anhand von Nachrichten oder täglichen Recherchen von Experten verstehen.';
$lang['educat_vedio'] = 'Unser Lehrvideo zum Thema Handel hilft Ihnen, sich als professioneller Händler zu entwickeln. Handelsindikatoren, Marktdynamik und Fundamentalanalyse halten Sie auf dem Laufenden.';
$lang['crypto_trade'] = 'Der Krypto-Handel unterscheidet sich immer vom traditionellen Börsenhandel oder Forex-Handel. Wir bieten Tutorials und Lernmaterial an, um ein wettbewerbsfähiger Händler zu sein.';
$lang['start_demo'] = 'Beginnen Sie mit dem Demo-Konto';
$lang['ext_feat'] = 'Zusätzliche Funktionen, mit denen Sie Ihre Fähigkeiten verbessern können';
$lang['enhance_skill'] = 'Verbessern Sie Ihre Handelsfähigkeiten';
$lang['smart_ai'] = 'Intelligente AI-basierte Handelsplattform, auf der AI alles verwaltet und dem Benutzer ein schnelles und problemloses Erlebnis bietet';
$lang['trade_advisor'] = 'Handelsberater';
$lang['des_skill'] = 'Eine Person mit bestimmten Fähigkeiten kann sich uns anschließen und Menschen darin schulen, individuell zu handeln und einen guten Gewinn aus dem Handel zu erzielen.';
$lang['ind'] = 'Indikatoren';
$lang['market'] = 'Handelsindikatoren spielen beim Handel immer eine entscheidende Rolle und sind immer hilfreich, um Markttrends zu identifizieren.';
$lang['strat'] = 'Strategien';
$lang['strat_tem'] = 'Strategien hängen hauptsächlich von der Fundamentalanalyse oder der technischen Analyse ab. Strategie kann für einen langfristigen oder kurzfristigen Handel sein.';
$lang['edt_trdv'] = 'Bildungshandelsvideo';
$lang['webinar'] = 'Webinare, Experten-Strategietraining und Verwendung von Indikatoren, um maximalen Gewinn aus dem Handel zu erzielen. Aktualisierung mit neuen Indikatoren und Strategien.';
$lang['better_exp'] = 'Für eine bessere Erfahrung';
$lang['our_key_feat'] = 'Unsere Hauptmerkmale';
$lang['we_res'] = 'Wir verbringen viel Zeit damit, Tonnen verschiedener Software-Websites von Top-Unternehmen zu recherchieren, um alle Best Practices und die notwendigsten Erfahrungen an einem Ort zu sammeln.';
$lang['data_protect'] = 'Datenschutz';
$lang['share_data'] = 'Jeder Benutzer ist etwas Besonderes für uns. Wir geben Ihre Daten niemals weiter.';
$lang['ref_prog'] = 'Empfehlungsprogramm';
$lang['earn_reff'] = 'Verdienen Sie mit jedem Trade, den das von Ihnen empfohlene Mitglied getätigt hat.';
$lang['news_update'] = 'Nachrichten Update';
$lang['fun_lang'] = 'Grundlegende Informationen und Marktdaten.';
$lang['trade_com'] = 'Handelswettbewerb';
$lang['compete'] = 'Messen Sie sich mit einem globalen Publikum und verdienen Sie Belohnung.';
$lang['freq'] = 'Häufig gestellte Fragen';
$lang['plat_diff'] = 'Wie unterscheidet sich diese Plattform von anderen?';
$lang['trd_curr'] = 'Handeln Sie mit mehreren Währungen wie BTC / USDT und ETH / USDT';
$lang['ben_plat'] = 'Was sind die Vorteile dieser Plattform?';
$lang['mul_fun_earn'] = 'Mehrere Funktionen an einem Ort, handeln, Spaß machen und verdienen.';
$lang['permission'] = 'Sind Gebühren oder Genehmigungen erforderlich, um den Handel zu kopieren?';
$lang['partner_profit'] = 'Nicht jeder Benutzer muss zulassen, dass seine Strategie kostenlos kopiert wird. Manchmal wird er eine gewinnorientierte Partnerschaft von bis zu 30% beantragen.';
$lang['how_ref_sys'] = 'Wie benutzt man das Überweisungssystem?';
$lang['share_social'] = 'Für den Handel mit Empfehlungsvorteilen müssen Sie lediglich Ihren Empfehlungslink oder Ihre Empfehlungs-ID in den sozialen Medien teilen und Prämien verdienen.';
$lang['random_ref_award'] = 'Wie nehme ich am "Zufällige Überweisungsbelohnung" -System teil?';
$lang['wallet_curr'] = 'Der Benutzer muss 100 $ in jeder Währung in der Brieftasche haben. Die Ziehung findet jeden Tag um UTC +0.00 Uhr statt.';
$lang['exhaustive'] = 'An Exhaustive List of Amazing Features';
$lang['platform'] = 'Eine Plattform für Handel, Nachrichten, Lernen und Verdienen.';
$lang['journey'] = 'Beginnen Sie Ihre Reise mit uns vom Anfänger bis zum professionellen Händler. Wir unterstützen unsere Kunden immer.';
$lang['benifit_us'] = 'Vorteile bei uns';
$lang['reward_sys'] = 'Belohnungssystem';
$lang['collaboration'] = 'Zusammenarbeit';
$lang['numerous'] = 'Unsere Plattform bietet zahlreiche Vorteile.';
$lang['learn_idea'] = 'Lernen Sie und teilen Sie Ihre Ideen mit anderen';
$lang['create_strat'] = 'Erstellen Sie Ihre Strategien';
$lang['available'] = 'Stellen Sie es der Öffentlichkeit zur Verfügung';
$lang['earn_ideaa'] = 'Verdiene aus deinen Ideen';
$lang['web_live'] = 'Webinar und Live-Handelssitzung';
$lang['vedio_conf'] = 'Videokonferenzen';
$lang['remote_team'] = 'Verbinden Sie sich mit Remote-Teams';
$lang['improve_skill'] = 'Verbessern Sie Ihre Handelsfähigkeiten';
$lang['mobile_app'] = 'Mobile App für alle Plattformen';
$lang['alternat'] = 'Die beste Alternative zur Website';
$lang['easy_access'] = 'Einfacher Zugang von überall';
$lang['crypto_play'] = 'Speichern Sie Krypto oder Spielen Sie über die App';
$lang['ticket_sol'] = 'Beste Ticketlösung';
$lang['fast_reply'] = 'Einfache und schnelle Antwort';
$lang['quries'] = 'Fragen Sie nach Fragen';
$lang['al_service'] = 'Immer in Betrieb';
$lang['mul_pair'] = 'Handeln Sie mit mehreren Paaren';
$lang['btc'] = 'BTC / USDT oder ETH / USDT';
$lang['multiplication'] = 'Verwenden Sie ein beliebiges Multiplikationssystem';
$lang['add_soon'] = 'Weitere werden in Kürze hinzugefügt';
$lang['rew_sys'] = 'Belohnungssystem';
$lang['mul_rew_sys'] = 'Mehrfachbelohnungssystem für alle Benutzer.';
$lang['productivity'] = 'Produktivität';
$lang['daily_rewardd'] = 'Tägliche Belohnung';
$lang['top_winner'] = 'Top 100 Gewinner';
$lang['get_rew_wallet'] = 'Holen Sie sich eine Belohnung in Ihre Brieftasche';
$lang['comp_start'] = 'Der Wettbewerb beginnt um +0.00 UTC';
$lang['month_trde'] = 'Monatlicher Handelswettbewerb';
$lang['new_comp'] = 'Jeden Monat neuer Wettbewerb';
$lang['leader_update'] = 'Regelmäßige Aktualisierung der Rangliste';
$lang['rew_dist'] = 'Größte Belohnungsverteilung';
$lang['luck_winner'] = 'Glücklicher Gewinner';
$lang['must_wall'] = 'Sie müssen 100 $ in Ihrer Brieftasche haben';
$lang['no_pass'] = 'Kein Pass erforderlich';
$lang['eligible'] = 'Alle aktiven Mitglieder sind berechtigt';
$lang['cop_trd_sys'] = 'Handelssystem kopieren';
$lang['fav_trader'] = 'Wählen Sie aus Ihrem Lieblingshändler';
$lang['flow_trm_ben'] = 'Befolgen Sie die Bedingungen und Vorteile von Händlern';
$lang['strt_earn_inst'] = 'Verdienen Sie sofort';
$lang['right_plat'] = 'Wählen Sie eine Plattform, die zu Ihnen passt';
// $lang[ 'win_strat_best' ] = 'Verdienen Sie mit den besten Gewinnstrategien';
$lang['win_strat_best'] = 'Brieftaschenanwendung';
$lang['trdeee_appl'] = 'Handelsanwendung';
$lang['coming_soon'] = 'Kommt bald';
$lang['competitive_adv'] = 'Wettbewerbsfähigste Plattform mit einer Reihe von Vorteilen';
$lang['four_five'] = '455,326,234';
$lang['peopple_joined'] = 'People Joined Already';
$lang['down_load_app'] = 'Handels-App';
$lang['down_load_wallet_app'] = 'Brieftasche App';
$lang['say_word'] = 'Unsere Statistiken sagen mehr als alle Worte';
$lang['without_border'] = 'Apps ohne Grenzen';
$lang['cumulative'] = 'Die meisten Apps wachsen jedes Jahr um 300%, wobei Benutzer auf der ganzen Welt sie immer wieder lieben. Wir sind auch kurz davor, 10 Millionen kumulative Downloads zu erreichen.';
$lang['user_review'] = 'Total User Reviews';
$lang['north_america'] = 'Nordamerika';
$lang['asia'] = 'Asien';
$lang['north_europe'] = 'Nordeuropa';
$lang['ind'] = 'Indikatoren';
$lang['south_america'] = 'Südamerika';
$lang['africa'] = 'Afrika';
$lang['australia'] = 'Australien';
$lang['roadmapp'] = 'Roadmap';


// ---------------- Footer Light------------------

$lang['tc'] = 'T & C';
$lang['aml'] = 'AML & KYC-Richtlinien';
$lang['demo_trading'] = 'Demo & Handelskonto';
$lang['privacy_policy'] = 'Datenschutz-Bestimmungen';
$lang['disclaimer'] = 'Haftungsausschluss';
$lang['copyright'] = 'Urheberrechte © © 2020.Alle Rechte vorbehalten von ';
$lang['earnfinex'] = 'Earnfinex';

// ------------------- CONTACT US PAGE ---------------------

$lang['contact_page'] = 'Contact Us';
$lang['whether_cntct'] = "Whether you're looking for a demo, have a support question or a commercial query get in touch.";
$lang['get_in_tch'] = "Get in Touch";
$lang['full_name'] = "Your Full Name";
$lang['entr_full_nme'] = "Enter Your Full Name";
$lang['your_email'] = "Your Email";
$lang['enter_email'] = "Enter Your Email";
$lang['yr_subject'] = "Your Subject";
$lang['entr_subjct'] = "Enter Your Subject";
$lang['yur_messg'] = "Your Message ";
$lang['entr_msg'] = "Enter Your Message";
$lang['i_agree'] = "I agree to receive emails, newsletters and promotional messages";
$lang['send_msg'] = "Send Message";
$lang['email_us'] = "Email Us";
$lang['maito'] = "support@earnfinex.com";



// ------------------------ LOGIN PAGE ----------------------------

$lang['wlcm_earn'] = 'Willkommen bei Earnfinex';
$lang['user_name'] = 'Nutzername';
$lang['uname'] = 'Geben Sie den Benutzernamen ein';
$lang['l_password'] = 'Passwort';
$lang['entr_pwd'] = 'Geben Sie Ihr Passwort ein';
$lang['frgt_pwd'] = 'Haben Sie Ihr Passwort vergessen?';
$lang['rcvr_pwd'] = 'Passwort wiederherstellen';
$lang['log_in'] = 'Anmeldung';
$lang['dont_hv_acc'] = "Sie haben noch keinen Account?";
$lang['sign_up_here'] = 'Sign Up Here';



// ------------------------ REGISTER PAGE ----------------------------

$lang['reff_id'] = 'Überweisungs-ID';
$lang['sponser_id'] = 'Sponser ID';
$lang['emailll'] = 'Email';
$lang['enter_email'] = 'Email eingeben';
$lang['ntr_y_pawd'] = 'Passwort eingeben';
$lang['cnfm_pwd'] = 'Kennwort bestätigen';
$lang['sign_upp'] = 'Anmelden';
$lang['already_hv_acc'] = 'Sie haben bereits ein Konto?';
$lang['sign_in_here'] = 'Hier anmelden';
$lang['sucesss'] = 'Erfolg!';
$lang['pls_chck_email'] = 'Bitte überprüfen Sie Ihre Mail.';
$lang['uunme'] = 'Nutzername:';
$lang['okay'] = 'in Ordnung';




// ------------------------- FORGET PASSWORD --------------------------

$lang['rst_yr_pwd'] = 'Setze dein Passwort zurück';
$lang['pls_entr_mail'] = "Bitte geben Sie unten Ihre E-Mail-Adresse ein. Wir senden Ihnen Anweisungen zum Zurücksetzen Ihres Passworts";
$lang['snd_instruction'] = 'Anweisungen senden';
$lang['rmrmbr_pwd'] = 'Erinnerst du dich an dein Passwort?';
$lang['loginnn'] = 'Anmeldung';




// -------------------------- ROADMAP ----------------------------------

$lang['project_init'] = 'Projekt initiiert';
$lang['the_first'] = 'Das erste Kernkomitee wurde gebildet und beschlossen, an diesem einzigartigen Konzept zu arbeiten. Technologie, Ideologie, Anwendungsfälle und Tokenomics wurden erstellt. Das Layout der aufgabenbasierten Arbeit wurde geplant.';
$lang['plat_open'] = 'Plattform für die Öffentlichkeit zugänglich';
$lang['web_activate'] = 'Die Website wurde aktiviert und ist offen für die Community. Nachrichten, Handelsplattform und Blockchain-Fortschritt werden auf der Plattform veröffentlicht. Eine Plattform für den Handel mit dem höchsten Belohnungssystem und einem einzigartigen Empfehlungssystem und
Wettbewerb.';
$lang['beta_version'] = 'Start der Blockchain Beta-Version';
$lang['block_launch'] = 'Unsere Blockchain wird mit einigen grundlegenden Funktionen wie der Erstellung, Übertragung und Änderung von Assets gestartet. Eine Handelsplattform mit Top-Währungen und Lehrvideos für neue Händler wird implementiert.';
$lang['trade_exchng_block'] = 'Handelsbörse, Blockchain und Kommerzialisierung';
$lang['full_featured'] = 'Die voll funktionsfähige Handelsbörse und Blockchain bieten weitere Vorteile. Das Asset-Management-System ist so einfach, dass jeder ein Asset erstellen kann, indem er ein einfaches Formular ausfüllt. Durch die Partnerschaft mit Domainführern werden einige zusätzliche Vorteile für die Technologieimprovisation hinzugefügt.';




// ==================================== NEWELY ADDED ===================================



$lang['win_rate'] = "Gewinnrate";
$lang['total_revenue'] = "Gesamtumsatz";
$lang['total_trade_amt'] = "Gesamthandelsbetrag";
$lang['net_profit'] = "Reingewinn";
$lang['trades_summary'] = "Handelsübersicht";
$lang['down'] = "Nieder";
$lang['up'] = "oben";
$lang['trading_historytrading_history'] = "Handelsgeschichte";
$lang['date_time'] = "Terminzeit";
$lang['base_amt'] = "Basisbetrag";
$lang['closing_val'] = "Schlusswert";
$lang['trade_amt'] = "Handelsbetrag";
$lang['prediction'] = "Prognose";
$lang['profit'] = "Profitieren";
$lang['community'] = "Gemeinschaft";
$lang['portfolio'] = "Portfolio";
$lang['expert_area'] = "Fachgebiet";
$lang['copy_trading'] = "Kopieren Sie den Handel";
$lang['earn_prft_copy_trd'] = "Verdienen Sie Gewinn durch Kopierhandel";
$lang['top_gainers'] = "Top Gainer";
$lang['most_popular'] = "Am beliebtesten";
$lang['experts'] = "Expertin";
$lang['start_copying'] = "Kopieren starten";
$lang['time'] = "Zeit";
$lang['want_to_bcm_exprt'] = "Willst du ein Experte werden?";
$lang['min_rqurmnt'] = "Minimale Anforderungen";
$lang['trde_cnt_last'] = "Handelszählung letzte 7 Tage";
$lang['profit_last'] = "Gewinne dauern 7 Tage";
$lang['trdng_vlume_last'] = "Handelsvolumen dauert 7 Tage";
$lang['you_are_not_eble_to_reg'] = "Sie sind nicht zur Registrierung berechtigt";
$lang['daily'] = "Täglich";
$lang['weekly'] = "Wöchentlich";
$lang['monthly'] = "Monatlich";
$lang['popular'] = "Beliebt";
$lang['next_draw'] = "Nächste Ziehung";
$lang['hour'] = "Stunde";
$lang['min'] = "Min";
$lang['sec'] = "Sec";
$lang['overview'] = "Überblick";
$lang['network_management'] = "Netzwerk Management";
$lang['total_refferrals'] = "Gesamtzahl der Empfehlungen";
$lang['total_agencies'] = "Agenturen insgesamt";
$lang['trading_commission'] = "Handelskommission";
$lang['agency_commission'] = "Agenturkommission";
$lang['invite_now'] = "Jetzt einladen";
$lang['invite_others'] = "Laden Sie andere ein";
$lang['invite_yr_frnds_to_reg'] = "Laden Sie Ihre Freunde ein, Earnfinex über Ihren Empfehlungslink zu registrieren";
$lang['register_successfully'] = "Erfolgreich registriert";
$lang['the_others_sign_up'] = "Die anderen melden sich mit Ihrem Link bei Earnfinex an";
$lang['earn_commission'] = "Provision verdienen";
$lang['rcv_up_to_fifty'] = "Erhalten Sie bis zu 50% Provision von Earnfinex";
$lang['your_ref_level'] = "Ihre Empfehlungsstufe";
$lang['rank'] = "Rang";
$lang['reff_requrmnrt'] = "Überweisungsanforderung";
$lang['agency'] = "Agentur";
$lang['volume'] = "Volumen";
$lang['time'] = "Zeit";
$lang['earned'] = "Verdient";
$lang['sponser'] = "Sponsor";
$lang['agency_licenece'] = "Agenturlizenz";
$lang['refferrals'] = "Empfehlungen";
$lang['agencies'] = "Agenturen";
$lang['total_vol'] = "Volle Lautstärke";
$lang['com_earned'] = "Provision verdient";
$lang['amount'] = "Menge";
