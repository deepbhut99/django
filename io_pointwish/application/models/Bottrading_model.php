<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Bottrading_model extends CI_Model
{

    function get_count_bids($tree, $level, $master = '')
    {
        $this->db->select("sum(amount) as total_trade");
        if ($level != 1) {
            // $this->db->where("(DATE_FORMAT(FROM_UNIXTIME(timestamp/1000), '%Y-%m-%d')) >= (CURRENT_DATE - INTERVAL 7 day)");
            $this->db->where("YEARWEEK(DATE_FORMAT(FROM_UNIXTIME(timestamp/1000), '%Y-%m-%d')) = YEARWEEK(NOW())");
        }
        $this->db->where("FIND_IN_SET(user, '" . $tree . "')");
        return $this->db->get_where('bids', array('type' => 0))->row()->total_trade;
    }

    private function set_group_concat_max_session()
    {
        $query = "SET SESSION group_concat_max_len = 18446744073709551615; ";
        $this->db->query($query);
    }

    function get_my_network()
    {
        $this->set_group_concat_max_session();
        $all_user_ids = array();
        $query = "select GROUP_CONCAT(username SEPARATOR ',') AS `all_user` from pwt_users where is_status = 1 and is_delete = 1 and sponsor ='" . $this->session->user_name . "'";
        $data = $this->db->query($query);
        $row = $data->row();

        if (!empty($row->all_user)) {
            array_push($all_user_ids, $row->all_user);
            $all_user = $row->all_user;
            $query = "select GROUP_CONCAT(username SEPARATOR ',') AS `all_user` from pwt_users where is_status = 1 and is_delete = 1 and FIND_IN_SET(sponsor, '" . $all_user . "')";
            $data = $this->db->query($query);
            $row = $data->row();
            if (!empty($row->all_user)) {
                array_push($all_user_ids, $row->all_user);
                $all_user = $row->all_user;
                $query = "select GROUP_CONCAT(username SEPARATOR ',') AS `all_user` from pwt_users where is_status = 1 and is_delete = 1 and FIND_IN_SET(sponsor, '" . $all_user . "')";
                $data = $this->db->query($query);
                $row = $data->row();
                if (!empty($row->all_user)) {
                    array_push($all_user_ids, $row->all_user);
                    $all_user = $row->all_user;
                    $query = "select GROUP_CONCAT(username SEPARATOR ',') AS `all_user` from pwt_users where is_status = 1 and is_delete = 1 and FIND_IN_SET(sponsor, '" . $all_user . "')";
                    $data = $this->db->query($query);
                    $row = $data->row();
                    if (!empty($row->all_user)) {
                        array_push($all_user_ids, $row->all_user);
                        $all_user = $row->all_user;
                        $query = "select GROUP_CONCAT(username SEPARATOR ',') AS `all_user` from pwt_users where is_status = 1 and is_delete = 1 and FIND_IN_SET(sponsor, '" . $all_user . "')";
                        $data = $this->db->query($query);
                        $row = $data->row();
                        if (!empty($row->all_user)) {
                            array_push($all_user_ids, $row->all_user);
                            $all_user = $row->all_user;
                            $query = "select GROUP_CONCAT(username SEPARATOR ',') AS `all_user` from pwt_users where is_status = 1 and is_delete = 1 and FIND_IN_SET(sponsor, '" . $all_user . "')";
                            $data = $this->db->query($query);
                            $row = $data->row();
                            if (!empty($row->all_user)) {
                                array_push($all_user_ids, $row->all_user);
                                $all_user = $row->all_user;
                                $query = "select GROUP_CONCAT(username SEPARATOR ',') AS `all_user` from pwt_users where is_status = 1 and is_delete = 1 and FIND_IN_SET(sponsor, '" . $all_user . "')";
                                $data = $this->db->query($query);
                                $row = $data->row();
                                if (!empty($row->all_user)) {
                                    array_push($all_user_ids, $row->all_user);
                                }
                            }
                        }
                    }
                }
            }
        }
        $ids = implode(',', $all_user_ids);
        $idss = explode(',', $ids);
        $this->db->select('pu.*, t.tree_count, t.tree');
        $this->db->join('pwt_bottrade_tree t', 'pu.id = t.sponsor_id', 'left');
        $this->db->where_in('pu.username', $idss);
        $this->db->order_by('pu.id', 'desc');
        return $this->db->get('pwt_users pu')->result_array();
    }

    function get_total_trade($user)
    {
        $this->db->select('sum(amount) as total_trade');
        return $this->db->get_where('bids', array('user' => $user))->row()->total_trade;
    }

    function get_user_matrix($username)
    {
        $this->set_group_concat_max_session();
        $all_user_ids = array();
        $query = "select GROUP_CONCAT(username SEPARATOR ',') AS `all_user` from pwt_users where is_status = 1 and is_delete = 1 and sponsor ='" . $username . "'";
        $data = $this->db->query($query);
        $row = $data->row();

        if (!empty($row->all_user)) {
            array_push($all_user_ids, $row->all_user);
            $all_user = $row->all_user;
            $query = "select GROUP_CONCAT(username SEPARATOR ',') AS `all_user` from pwt_users where is_status = 1 and is_delete = 1 and FIND_IN_SET(sponsor, '" . $all_user . "')";
            $data = $this->db->query($query);
            $row = $data->row();
            if (!empty($row->all_user)) {
                array_push($all_user_ids, $row->all_user);
                $all_user = $row->all_user;
                $query = "select GROUP_CONCAT(username SEPARATOR ',') AS `all_user` from pwt_users where is_status = 1 and is_delete = 1 and FIND_IN_SET(sponsor, '" . $all_user . "')";
                $data = $this->db->query($query);
                $row = $data->row();
                if (!empty($row->all_user)) {
                    array_push($all_user_ids, $row->all_user);
                    $all_user = $row->all_user;
                    $query = "select GROUP_CONCAT(username SEPARATOR ',') AS `all_user` from pwt_users where is_status = 1 and is_delete = 1 and FIND_IN_SET(sponsor, '" . $all_user . "')";
                    $data = $this->db->query($query);
                    $row = $data->row();
                    if (!empty($row->all_user)) {
                        array_push($all_user_ids, $row->all_user);
                        $all_user = $row->all_user;
                        $query = "select GROUP_CONCAT(username SEPARATOR ',') AS `all_user` from pwt_users where is_status = 1 and is_delete = 1 and FIND_IN_SET(sponsor, '" . $all_user . "')";
                        $data = $this->db->query($query);
                        $row = $data->row();
                        if (!empty($row->all_user)) {
                            array_push($all_user_ids, $row->all_user);
                            $all_user = $row->all_user;
                            $query = "select GROUP_CONCAT(username SEPARATOR ',') AS `all_user` from pwt_users where is_status = 1 and is_delete = 1 and FIND_IN_SET(sponsor, '" . $all_user . "')";
                            $data = $this->db->query($query);
                            $row = $data->row();
                            if (!empty($row->all_user)) {
                                array_push($all_user_ids, $row->all_user);
                                $all_user = $row->all_user;
                                $query = "select GROUP_CONCAT(username SEPARATOR ',') AS `all_user` from pwt_users where is_status = 1 and is_delete = 1 and FIND_IN_SET(sponsor, '" . $all_user . "')";
                                $data = $this->db->query($query);
                                $row = $data->row();
                                if (!empty($row->all_user)) {
                                    array_push($all_user_ids, $row->all_user);
                                }
                            }
                        }
                    }
                }
            }
        }
        $ids = implode(',', $all_user_ids);
        $idss = explode(',', $ids);
        $this->db->select('count(*) as totalUsers, sum(is_bottrade) as totalAgency');
        $this->db->where_in('pu.username', $idss);
        return $this->db->get('pwt_users pu')->row_array();
    }

    function get_teamtrading_income($userid)
    {

        $this->db->select('round(sum(amount),6) as totalEarned');
        $this->db->where(" DATE(created_date) = CURDATE() ");
        return $this->db->get_where('pwt_bottrade_commission', array('user_id' => $userid))->row()->totalEarned;
    }
    function get_teamtrading_income_fortotal($userid)
    {

        $this->db->select('round(sum(amount),6) as totalEarned');
        return $this->db->get_where('pwt_bottrade_commission', array('user_id' => $userid))->row()->totalEarned;
    }


    // gethistory 
    function gethistory_bot($user_id)
    {
        $this->db->select('pu.*,u.username');  
        $this->db->where("pu.message = 'Team trading commission'");
        $this->db->where(array('pu.from_user =' => $user_id));
        $this->db->join('pwt_users u', 'pu.to_user = u.id','left');
        return $this->db->get('pwt_user_notification pu')->result_array();
    }
}
