<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Common_model extends CI_Model
{


	/* ************* get all data as where class *************** */
	function getWhere($table, $where)
	{
		$this->db->where($where);
		$data = $this->db->get($table);
		$get = $data->result();
		if ($get) {
			return $get;
		} else {
			return FALSE;
		}
	}

	/* ************* add  data *************** */
	function insert_data($table, $data)
	{

		$this->db->insert($table, $data);
		$num = $this->db->insert_id();

		return $num;
	}

	/* ************* update  data *************** */
	function update_data($table, $where, $data)
	{
		$this->db->where($where);
		$update = $this->db->update($table, $data);

		if ($update) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	function update_data_all($table, $data)
	{
		$update = $this->db->update($table, $data);

		if ($update) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	function getSingle($table, $where)
	{
		$this->db->where($where);
		$data = $this->db->get($table);
		$get = $data->row();

		$num = $data->num_rows();

		if ($num) {
			return $get;
		} else {
			return false;
		}
	}

	function getLastRow($where)
	{
		$query = "SELECT * FROM etx_condition_for_btctousdt WHERE user_id = '$where' ORDER BY id DESC LIMIT 0, 1";
		$data = $this->db->query($query);
		return $data->result_array();
	}

	function getSingleFromLast2($table, $where)
	{
		$this->db->where($where);

		$this->db->order_by('id', 'desc');

		$data = $this->db->get($table);
		$get = $data->row();

		$num = $data->num_rows();

		if ($num) {
			return $get;
		} else {
			return false;
		}
	}

	function getSingleFromLast($table, $where)
	{
		$this->db->where($where);

		$this->db->order_by('id', 'desc');

		$data = $this->db->get($table);
		$get = $data->row();

		$num = $data->num_rows();

		if ($num) {
			return $get;
		} else {
			return false;
		}
	}

	function getWhereFromLast($table, $where, $limit = '')
	{
		$this->db->order_by('id', 'desc');
		if ($limit) {
			$this->db->limit($limit);
		}
		return $this->db->get_where($table, $where)->result_array();
	}

	function count_users($ip)
	{
		return $this->db->get_where('pwt_log_history', array('ip_address' => $ip, 'message' => "Signup"))->result_array();
	}

	function login_count($user, $ip)
	{
		$this->db->where('DATE_FORMAT(created_datetime,"%Y-%m-%d")', date('Y-m-d'));
		$this->db->where(array('user_id' => $user, 'is_active' => 1));
		return $this->db->update('pwt_log_history', array('is_active' => 0));
	}

	function get_range_data($from, $to)
	{
		$query = "SELECT username, email FROM `pwt_users` WHERE id BETWEEN $from and $to AND is_status = 1";
		$data = $this->db->query($query);
		return $data->result_array();
	}
}
