<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Dashboard_model extends CI_Model
{
	function getMyWalletAmount()
	{
		$query = "select wallet_amount from pwt_users where id = '" . $this->session->userdata('user_id') . "'";

		$data = $this->db->query($query);
		if ($data->num_rows() > 0) {
			$result = $data->row();

			return $result->wallet_amount;
		} else {
			return 0;
		}
	}

	function get_total_bids()
	{
		$this->db->select('sum(amount) as totalamount, count(*) as totalbids');
		return $this->db->get_where('bids', array('user' => $this->session->user_id, 'type' => 0))->row_array();
	}

	function get_profit_bids()
	{
		$this->db->select('sum(amount) as totalprofit, count(*) as winbids');
		return $this->db->get_where('bids', array('user' => $this->session->user_id, 'result !=' => 0, 'type' => 0))->row_array();
	}

	function get_up_bids()
	{
		$this->db->select('count(*) as upbids');
		return $this->db->get_where('bids', array('user' => $this->session->user_id, 'growth' => 1, 'type' => 0))->row()->upbids;
	}

	function get_my_bids($start = '', $end = '')
	{
		$where = array(
			'user'  =>  $this->session->user_id,
			'type'  =>  0
		);
		if ($end) {
			$where["DATE_FORMAT(FROM_UNIXTIME(timestamp/1000), '%Y-%m-%d') >="] = $start;
			$where["DATE_FORMAT(FROM_UNIXTIME(timestamp/1000), '%Y-%m-%d') <="] = $end;
		} else {
			$this->db->limit('100');
		}
		return $this->db->order_by('id', 'desc')->get_where('bids', $where)->result_array();
	}

	function get_referral_users_forbot()
	{
		return $this->db->order_by('id', 'desc')->get_where('pwt_users', array('sponsor' => $this->session->user_name, 'is_bottrade' => 1))->result_array();
	}
	function get_referral_users()
	{
		return $this->db->order_by('id', 'desc')->get_where('pwt_users', array('sponsor' => $this->session->user_name))->result_array();
	}
	function get_total_deposit()
	{
		$this->db->select("sum(amount) as total");
		return $this->db->get_where('pwt_user_notification', array('from_user' => $this->session->user_id, 'type' => 'fund', 'status' => 100))->row()->total;
	}

	function count_referral_income($user)
	{
		$this->db->select('sum(amount) as total');
		return $this->db->get_where('pwt_user_notification', array('from_user ' => $user, 'message' => "Direct Referral"))->row()->total;
	}

	function count_total_exchange()
	{
		$this->db->select("sum(conv_amount) as total");
		return $this->db->get_where('pwt_exchange_history', array('from_currency' => 'pwt', 'user_id' => $this->session->user_id, 'date >=' => '2021-08-20'))->row()->total;
	}

	function get_last_balance($id)
	{
		return $this->db->get_where('pwt_users', array('id' => $id))->row_array();
	}
	function get_last_set_ticker($WHERE)
	{
		$query =  "SELECT * FROM   $WHERE ORDER BY id DESC LIMIT 1";
		$data = $this->db->query($query);

		if ($data->num_rows() > 0) {
			$result = $data->row();

			return $result->pricechange;
		} else {
			return 0;
		}
	}


	function get_last_set_ticker_price($WHERE)
	{

		$query = "SELECT * FROM   $WHERE ORDER BY id DESC LIMIT 1";
		$data = $this->db->query($query);

		$data = $this->db->query($query);

		if ($data->num_rows() > 0) {
			$result = $data->row();

			return $result->close;
		} else {
			return 0;
		}
	}

	function getbinancekey($id)
	{
		$query = "select binance_apikey, binance_securitykey from pwt_users where id = '" . $id . "'";

		$data = $this->db->query($query);
		if ($data->num_rows() > 0) {
			$result = $data->row();

			return $result;
		} else {
			return 0;
		}
	}
	function getstartstopbot()
	{
		$query = "select * from etx_condition_for_btctousdt where id = '" . $this->session->userdata('user_id') . "'";

		$data = $this->db->query($query);
		if ($data->num_rows() > 0) {
			$result = $data->row();

			return $result;
		} else {
			return 0;
		}
	}
}
