<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Draw_model extends CI_Model
{
    function getLastWeekFund()
	{

        $this->db->select('sum(amount) as total, user_id');
        // $this->db->select('user_id');
        // $this->db->distinct('user_id');
        $this->db->group_by('user_id');
        $this->db->where("DATE_FORMAT(created_datetime, '%Y-%m-%d') >= DATE(NOW()) - INTERVAL 7 DAY");
        $this->db->get_where('pwt_add_fund_history', array('status' => 100))->result_array();      
        $sql = $this->db->last_query();
        echo $sql;
        // foreach($results as $row)  {
        //     $this->db->select('sum(amount) as total_amount');
        //     $total = $this->db->get_where('pwt_add_fund_history', array('user_id' => $row['user_id']))->row()->total_amount;
        //     $data[$row['user_id']] = $total;
        // }
        // return $data;

		// $now = new DateTime("6 days ago", new DateTimeZone('America/New_York'));
		// $interval = new DateInterval('P1D'); // 1 Day interval
		// $period = new DatePeriod($now, $interval, 5); // 7 Days
		// $dateList = array();
		// foreach ($period as $day) {
		// 	$key = $day->format('Y-m-d');
		// 	array_push($dateList, $key);
		// }
		// $data_rows = array();
		// foreach (array_reverse($dateList) as $row) {
            
		// 	$this->db->select("sum(pwt_amount) as total");
		// 	$this->db->where("DATE_FORMAT(created_datetime, '%Y-%m-%d') = ", $row);
		// 	$this->db->where(array('type !=' => 'pwt', 'status' => 100));
		// 	$total = $this->db->get('pwt_add_fund_history')->row()->total;
		// 	$data_rows[$row] = $total;
		// }
		// // $data = $this->db->query($query);
		// return $data_rows;
	}
}
