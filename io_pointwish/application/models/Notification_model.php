<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Notification_model extends CI_Model 
{
	function getMyNotificationHistory($start_limit, $end_limit)
	{
		$where = '';

		if($this->input->get('start_date')) {
			$start_date = date('Y-m-d', strtotime($this->input->get('start_date')));
			$where .= " and DATE(pwt_user_notification.created_datetime) >= '".$start_date."'";	
		}	

		if($this->input->get('end_date')) {
			$end_date = date('Y-m-d', strtotime($this->input->get('end_date')));
			$where .= " and DATE(pwt_user_notification.created_datetime) <= '".$end_date."'";	
		}		

		$query = "select pwt_user_notification.* from pwt_user_notification where (from_user='".$this->session->userdata('user_id')."') ".$where." order by pwt_user_notification.id desc LIMIT $start_limit, $end_limit";

		$data = $this->db->query($query);
		if($data->num_rows() > 0)
		{
			return $data->result();
		}
		else
		{
			return false;
		}
	}

	function getMyNotificationHistoryCount()
	{
		$where = '';

		if($this->input->get('start_date')) {
			$start_date = date('Y-m-d', strtotime($this->input->get('start_date')));
			$where .= " and DATE(pwt_user_notification.created_datetime) >= '".$start_date."'";	
		}	

		if($this->input->get('end_date')) {
			$end_date = date('Y-m-d', strtotime($this->input->get('end_date')));
			$where .= " and DATE(pwt_user_notification.created_datetime) <= '".$end_date."'";	
		}
		
		$query = "select count(pwt_user_notification.id) as total_count from pwt_user_notification where (from_user='".$this->session->userdata('user_id')."') ".$where;

		$data = $this->db->query($query);
		$row = $data->row();

		if(!empty($row->total_count))
		{
			return $row->total_count;
		}
		else
		{
			return 0;
		}
	}

	function getMyNotification($start = '', $end = '')
	{
		$where = '';
		$this->db->select('pwt_user_notification.*, pwt_users.username');
		$where = array('from_user' => $this->session->user_id, 'is_read' => 0);
		if ($end) {
			$where["DATE_FORMAT(created_datetime, '%Y-%m-%d') >="] = $start;
			$where["DATE_FORMAT(created_datetime, '%Y-%m-%d') <="] = $end;
		}
		$this->db->join('pwt_users', 'pwt_users.id = pwt_user_notification.to_user', 'left');
		return $this->db->order_by('id', 'desc')->limit(100)->get_where('pwt_user_notification', $where)->result_array();
	}

	function getLastMyNotification() {
// 		$where = "from_user=". $this->session->user_id ." AND type = 'transfer' OR type = 'withdrawal' OR type = 'deposit'";
		$this->db->order_by("id", "desc");		
		$this->db->where(array('from_user' => $this->session->user_id, 'is_read' => 0));
		$this->db->where("(type = 'fund' OR type='withdrawal')");
		return $this->db->get('pwt_user_notification')->result_array();
	}
	
	function count_document($id) {
// 		$query = "select * from pwt_users_document where status = 0 or status = 1 and document_id = '".$id. "'";
        $query = "select * from pwt_users_document where document_id = '" . $id . "' and status IN(0, 1)";
		$data = $this->db->query($query);		
		return $data->num_rows();
	}

	function count_unread_noti() {
		$this->db->select('count(*) as total');
		return $this->db->get_where('pwt_notification',array('user_id' => $this->session->user_id, 'is_read' => 0))->row()->total;
	}
}
?>
