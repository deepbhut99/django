<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ticket_model extends CI_Model 
{
	function get_tickets($start = '', $end = '')
	{
		$where = array('user_id' => $this->session->user_id);
		if ($end) {
			$where["DATE_FORMAT(created_datetime, '%Y-%m-%d') >="] = $start;
			$where["DATE_FORMAT(created_datetime, '%Y-%m-%d') <="] = $end;
		}
		return $this->db->limit(100)->get_where('pwt_tickets', $where)->result_array();
	}

	function getMyOpenTicketCount()
	{
		$query = "SELECT count(*) as total_count FROM `pwt_tickets` WHERE user_id = '".$this->session->userdata('user_id')."' and status = 'open'";
		$data = $this->db->query($query);

		if($data->num_rows())
		{
			$row = $data->row();

			return $row->total_count;
		}
		else
		{
			return 0;
		}
	}

	function getMyInProcessTicketCount()
	{
		$query = "SELECT count(*) as total_count FROM `pwt_tickets` WHERE user_id = '".$this->session->userdata('user_id')."' and status = 'process'";
		$data = $this->db->query($query);

		if($data->num_rows())
		{
			$row = $data->row();

			return $row->total_count;
		}
		else
		{
			return 0;
		}
	}	

	function getMyCloseTicketCount()
	{
		$query = "SELECT count(*) as total_count FROM `pwt_tickets` WHERE user_id = '".$this->session->userdata('user_id')."' and status = 'close'";
		$data = $this->db->query($query);

		if($data->num_rows())
		{
			$row = $data->row();

			return $row->total_count;
		}
		else
		{
			return 0;
		}
	}


	// function getTicketListByType($type, $end_limit)
	// {
		
	// 	$WHERE = "";

    // 	if($type == 'all')
    // 	{
    //     	$WHERE .= "";
    // 	}
    // 	else if($type == 'open')
    // 	{
    // 		$WHERE .= " and status = 'open'";
    // 	}
    // 	else if($type == 'process')
    // 	{
    // 		$WHERE .= " and status = 'process'";
    // 	}
    // 	else if($type == 'close')
    // 	{
    // 		$WHERE .= " and status = 'close'";
    // 	}		

	// 	$query = "SELECT * FROM `pwt_tickets` WHERE `user_id` = '".$this->session->userdata('user_id')."'".$WHERE." order by id desc LIMIT $end_limit";
	// 	$data = $this->db->query($query);

	// 	if($data->num_rows())
	// 	{
	// 		return $data->result();
	// 	}
	// 	else
	// 	{
	// 		return 0;
	// 	}
	// }

	function getTicketListByType($start_limit, $end_limit)
	{
		$type = $this->input->get('type', true);

		$WHERE = "";

    	if($type == 'all')
    	{
        	$WHERE .= "";
    	}
    	else if($type == 'open')
    	{
    		$WHERE .= " and status = 'open'";
    	}
    	else if($type == 'process')
    	{
    		$WHERE .= " and status = 'process'";
    	}
    	else if($type == 'close')
    	{
    		$WHERE .= " and status = 'close'";
    	}		

		$query = "SELECT * FROM `pwt_tickets` WHERE `user_id` = '".$this->session->userdata('user_id')."'".$WHERE." order by id desc LIMIT $start_limit, $end_limit";
		$data = $this->db->query($query);

		if($data->num_rows())
		{
			return $data->result();
		}
		else
		{
			return false;
		}
	}

	function getTicketListByFilter($start, $end, $status='')
	{
		
		$WHERE = "";    	
		if($status) 
			$WHERE .= " and status = '$status' and DATE(created_datetime)>= '$start' and DATE(created_datetime) <='$end'";		
		else {
			$WHERE .= " and DATE(created_datetime)>= '$start' and DATE(created_datetime) <= '$end'";
		}

		$query = "SELECT * FROM `pwt_tickets` WHERE `user_id` = '".$this->session->userdata('user_id')."'".$WHERE." order by id desc";
		$data = $this->db->query($query);

		if($data->num_rows())
		{
			return $data->result();
		}
		else
		{
			return 0;
		}
	}

	function getTicketListByTypeCount()
	{
		$type = $this->input->get('type', true);

		$WHERE = "";

    	if($type == 'all')
    	{
        	$WHERE .= "";
    	}
    	else if($type == 'open')
    	{
    		$WHERE .= " and status = 'open'";
    	}
    	else if($type == 'process')
    	{
    		$WHERE .= " and status = 'process'";
    	}
    	else if($type == 'close')
    	{
    		$WHERE .= " and status = 'close'";
    	}	

		$query = "SELECT * FROM `pwt_tickets` WHERE `user_id` = '".$this->session->userdata('user_id')."'".$WHERE;
		$data = $this->db->query($query);

		return $data->num_rows();
	}


	function getMyTicketChatList()
	{
		$ticket_id = $this->input->get('ticket_id', true);	

		$query = "SELECT pwt_tickets_chat.*, pwt_users.profile_pic FROM pwt_tickets_chat join pwt_users on pwt_tickets_chat.user_id = pwt_users.id WHERE pwt_tickets_chat.user_id = '".$this->session->userdata('user_id')."' and pwt_tickets_chat.ticket_id = '".$ticket_id."' order by pwt_tickets_chat.id asc";
		$data = $this->db->query($query);

		if($data->num_rows())
		{
			return $data->result();
		}
		else
		{
			return 0;
		}
	}
	
	function get_user_ticket() {
		$data = $this->db->query("select count(*) as total_ticket from pwt_tickets WHERE user_id =".$this->session->user_id. " and `status` NOT IN ('close')");
		return $data->row()->total_ticket;			
	}

}
?>
