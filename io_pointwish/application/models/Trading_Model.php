<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Trading_Model extends CI_Model
{
    function get_winners($type)
    {

        $this->db->select('b.*, sum(amount) as totalprofit, u.username, u.profit_share, u.min_invest, u.copiers');
        $where = 'result = 1 and type = 0';
        if ($type == 'daily') {
            $where .= " and DATE_FORMAT(FROM_UNIXTIME(timestamp/1000), '%Y-%m-%d') = CURDATE()";
        } else if ($type == 'weekly') {
            // $where .= " and WEEK(DATE_FORMAT(FROM_UNIXTIME(timestamp/1000), '%Y-%m-%d')) = WEEK(CURRENT_DATE - INTERVAL 1 WEEK)";
            $where .= " and DATE_FORMAT(FROM_UNIXTIME(timestamp/1000), '%Y-%m-%d') BETWEEN NOW() - INTERVAL 7 DAY and NOW()";
        } else if ($type == 'monthly') {
            $where .= " and MONTH(DATE_FORMAT(FROM_UNIXTIME(timestamp/1000), '%Y-%m-%d')) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH)";
        }
        $this->db->group_by('b.user');
        $this->db->where($where);
        $this->db->join('pwt_users u', 'u.id = b.user');
        $this->db->order_by('sum(amount)', 'desc');
        return $this->db->order_by('b.id', 'desc')->limit('10')->get('bids b')->result_array();
    }

    function get_last_trade($user, $type = '')
    {
        $this->db->select('sum(amount) as totalamount, count(*) as totalbids');
        if ($type == 'weekly') {
            $where = array('user' => $user, 'type' => 0, 'result' => 1);
        } else {
            $where = array('user' => $user, 'type' => 0);
        }
        $where["DATE_FORMAT(FROM_UNIXTIME(timestamp/1000), '%Y-%m-%d') >="] = date('Y-m-d', strtotime('-6 days'));
        $where["DATE_FORMAT(FROM_UNIXTIME(timestamp/1000), '%Y-%m-%d') <="] = date('Y-m-d');
        $this->db->where($where);
        return $this->db->get('bids')->row_array();
    }

    function get_profit_bids($type = '', $user)
    {
        $this->db->select('sum(amount) as totalprofit');
        $where = 'user = ' . $user . ' and result = 1 and type = 0';
        if ($type == 'daily') {
            $where .= " and DATE_FORMAT(FROM_UNIXTIME(timestamp/1000), '%Y-%m-%d') = subdate(CURRENT_DATE, 1)";
        } else if ($type == 'weekly') {
            // $where .= " and WEEK(DATE_FORMAT(FROM_UNIXTIME(timestamp/1000), '%Y-%m-%d')) = WEEK(CURRENT_DATE - INTERVAL 1 WEEK)";
            $where .= " and WEEK(DATE_FORMAT(FROM_UNIXTIME(timestamp/1000), '%Y-%m-%d')) = WEEK(now())-1";
            // $where .= " and WEEK(DATE_FORMAT(FROM_UNIXTIME(timestamp/1000), '%Y-%m-%d')) ='" . date('Y-m-d', strtotime('-6 days')) . "'";
        } else if ($type == 'monthly') {
            $where .= " and MONTH(DATE_FORMAT(FROM_UNIXTIME(timestamp/1000), '%Y-%m-%d')) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH)";
        }
        $this->db->where($where);
        return $this->db->get('bids')->row()->totalprofit;
    }

    function get_top_gainers($most = '')
    {
        $this->db->select('count(*) as totalbids, e.*');
        $this->db->join('bids b', 'b.user = e.id');
        $this->db->group_by('b.user');
        if ($most) {
            $this->db->order_by('e.copiers', 'desc');
        } else {
            $this->db->order_by('e.min_invest', 'desc');
        }
        return $this->db->get_where('pwt_users e', array('copy_trade' => 1))->result_array();
    }

    function get_portfolio_lists()
    {
        $this->db->select('e.*, count(*) as totalbids, ex.user_id');
        // $this->db->join('bids b', 'b.user = e.id');                
        // return $this->db->get_where('pwt_users e', array('copy_trade' => 1, 'id !=' => $this->session->user_id))->result_array();
        $this->db->join('pwt_users e', "e.id = ex.master_user_id");
        $this->db->join('bids b', 'b.user = e.id');
        // $this->db->order_by('ex.id', 'desc');
        $this->db->group_by('b.user');
        return $this->db->get_where('pwt_copy_trade ex', array('user_id' => $this->session->user_id))->result_array();
    }

    function get_copy_trade_bot_history($start = '', $end = '')
    {
        $where = array('eh.user_id' => $this->session->user_id, 'eh.orderListId' => -1);
        if ($end) {
            $where["DATE_FORMAT(created_datetime, '%Y-%m-%d') >="] = $start;
            $where["DATE_FORMAT(created_datetime, '%Y-%m-%d') <="] = $end;
        } else {
            $this->db->limit('100');
        }
        $this->db->order_by('eh.id', 'desc');

        return $this->db->get_where('pwt_bids_for_buysell eh', $where)->result_array();
    }
    function get_copy_trade_history($start = '', $end = '')
    {
        // $this->db->select('eh.*, e.username');
        $this->db->join('pwt_users e', 'e.id = eh.master_user_id', 'left');
        $where = array('eh.user_id' => $this->session->user_id);
        if ($end) {
            $where["DATE_FORMAT(FROM_UNIXTIME(trade_date/1000), '%Y-%m-%d') >="] = $start;
            $where["DATE_FORMAT(FROM_UNIXTIME(trade_date/1000), '%Y-%m-%d') <="] = $end;
        } else {
            $this->db->limit('100');
        }
        $this->db->order_by('eh.id', 'desc');
        return $this->db->get_where('pwt_copy_history eh', $where)->result_array();
    }

    function get_total_trade()
    {
        $this->db->select('sum(trade_amount) as total_trade');
        return $this->db->get_where('pwt_copy_history eh', array('eh.user_id' => $this->session->user_id))->row()->total_trade;
    }

    function get_total_trade_profit()
    {
        // return $this->db->select('sum((trade_amount * 0.95) - (trade_amount * 0.95 * (commission / 100))) as profit')->get_where('pwt_copy_history eh', array('eh.user_id' => $this->session->user_id, 'profit' => 1))->row()->profit;
        return $this->db->select('sum((trade_amount + (trade_amount * 0.95)) - (trade_amount * 0.95 * (commission / 100))) as profit')->get_where('pwt_copy_history eh', array('eh.user_id' => $this->session->user_id, 'profit' => 1))->row()->profit;
    }

    function get_popular_users()
    {
        $this->db->select('e.id, e.username, e.profit_share, e.min_invest, e.copiers, sum(amount) as totalprofit');
        $this->db->join('bids b', 'b.user = e.id');
        $this->db->order_by('sum(b.amount)', 'desc');
        $this->db->group_by('b.user');
        return $this->db->limit(10)->get_where('pwt_users e', array('result' =>  1, 'type' => 0))->result_array();
    }

    function get_copiers($start = '', $end = '')
    {
        $this->db->select('c.*, e.username,e.is_bottrade');
        $this->db->join('pwt_users e', "e.id = c.user_id");
        $this->db->order_by('c.id', 'desc');
        if ($start) {
            $this->db->where(array('c.start_date >=' => $start, 'c.start_date <=' => $end));
        } else {
            $this->db->limit('100');
        }
        return $this->db->get_where('pwt_copy_trade c', array('c.master_user_id' => $this->session->user_id))->result_array();
    }

    function get_peers_history($start = '', $end = '')
    {
        $this->db->join('pwt_users e', 'e.id = eh.user_id', 'left');
        $where = array('eh.master_user_id' => $this->session->user_id);
        if ($end) {
            $where["DATE_FORMAT(FROM_UNIXTIME(trade_date/1000), '%Y-%m-%d') >="] = $start;
            $where["DATE_FORMAT(FROM_UNIXTIME(trade_date/1000), '%Y-%m-%d') <="] = $end;
        } else {
            $this->db->limit('100');
        }
        // $this->db->order_by('eh.trade_date', 'desc');
        return $this->db->order_by('eh.id', 'desc')->get_where('pwt_copy_history eh', $where)->result_array();
    }

    function get_earn_commission()
    {
        return $this->db->select('sum((trade_amount * 0.95 * (commission / 100))) as total_commission')->get_where('pwt_copy_history eh', array('eh.master_user_id' => $this->session->user_id, 'profit' => 1))->row()->total_commission;
    }

    function get_winner_list($type = '')
    {
        $this->db->select('n.*, u.username, u.profit_share, u.min_invest, u.copiers');
        $where = '';
        $this->db->join('pwt_users u', 'u.id = n.from_user');
        $this->db->order_by('amount', 'desc');
        if ($type == 'daily') {
            $where .= " message = 'Daily Winner'";
            $where .= "and DATE_FORMAT(n.created_datetime, '%Y-%m-%d') = subdate(CURRENT_DATE, 1)";
        } else if ($type == 'weekly') {
            $where .= " message = 'Weekly Winner'";
            $where .= "and DATE_FORMAT(n.created_datetime, '%Y-%m-%d') = '" . date('Y-m-d', strtotime("saturday last week")) . "'";
        } else if ($type == 'monthly') {
            $where .= " message = 'Monthly Winner'";
            $where .= "and DATE_FORMAT(n.created_datetime, '%Y-%m-%d') = LAST_DAY(curdate() - interval 1 month) + interval 1 day";
        }
        $this->db->where($where);
        return $this->db->get('pwt_user_notification n')->result_array();
    }

    function get_current_winners($type)
    {

        $this->db->select('b.*, u.username, sum(amount) as totalprofit');
        $where = 'result = 1 and type = 0';
        if ($type == 'daily') {
            $where .= " and DATE_FORMAT(FROM_UNIXTIME(timestamp/1000), '%Y-%m-%d') = CURDATE()";
        } else if ($type == 'weekly') {
            $where .= " and YEARWEEK(DATE_FORMAT(FROM_UNIXTIME(timestamp/1000), '%Y-%m-%d')) = YEARWEEK(NOW())";
        } else if ($type == 'monthly') {
            $where .= " and MONTH(DATE_FORMAT(FROM_UNIXTIME(timestamp/1000), '%Y-%m-%d')) = MONTH(NOW())";
        }
        $this->db->group_by('b.user');
        $this->db->where($where);
        $this->db->join('pwt_users u', 'u.id = b.user');
        $this->db->order_by('sum(amount)', 'desc');
        return $this->db->order_by('b.id', 'desc')->limit('10')->get('bids b')->result_array();
    }

    function get_todayall_bids()
    {
        $this->db->select('b.*, u.username, u.login_country, l.device, l.os')
            ->join('pwt_users u', 'u.id = b.user', 'left')
            ->join('pwt_log_history l', 'l.user_id = u.id', 'left')
            ->where('date_format(\'l.created_datetime\', \'%Y-%m-%d\') = NOW() and l.is_active = 1')
            ->where("from_unixtime(timestamp/1000, '%Y %D %M') = from_unixtime(unix_timestamp(), '%Y %D %M') and b.type = 0")
            ->order_by('b.id', 'desc')
            ->get('bids b')->result_array();
    }
    function get_set_bid_forbot($start = '', $end = '')
    {
        $where = array('user_id' => $this->session->user_id);
        if ($end) {
            $where["DATE_FORMAT(created_datetime, '%Y-%m-%d') >="] = $start;
            $where["DATE_FORMAT(created_datetime, '%Y-%m-%d') <="] = $end;
        }
        return $this->db->limit(100)->order_by('pwt_bids_for_buysell.id_whichsellorbuy', 'desc')->get_where('pwt_bids_for_buysell', $where)->result_array();
    }

    function getruningbids($data)
    {
        $name = $data;
        $query =  "SELECT * FROM  etx_condition_for_btctousdt WHERE user_id = '" . $this->session->userdata('user_id') . "' AND typeofpoint = '" . $data . "' AND buysell != 0 ";
        $data = $this->db->query($query);
        if ($data->num_rows() > 0) {
            $result = $data->row();
            return $result;
        } else {
            return 0;
        }
    }
    function check_curr_stamp_start($select_stamp_val, $user_id)
    {
        $query =  "UPDATE etx_condition_for_btctousdt SET buysell= -1 WHERE user_id = '" . $user_id . "' AND typeofpoint = '" . $select_stamp_val . "' AND buysell != 0 AND margincalllimit != 0 AND buysell= 3";
        $data = $this->db->query($query);

        if ($data == '') {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    function check_curr_stamp_stop($select_stamp_val, $user_id)
    {
        $query =  "UPDATE etx_condition_for_btctousdt SET buysell= 0, margincalllimit= 0 WHERE user_id = '" . $user_id . "' AND typeofpoint = '" . $select_stamp_val . "' AND buysell != 0  AND  buysell in (-1,1)";
        $data = $this->db->query($query);
        if ($data) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    function lastbed($where)
    {
        $this->db->select('b.*');
        $this->db->where(array('b.user_id' => $this->session->user_id));
        $this->db->order_by('b.id', 'desc');
        $this->db->limit($where);
        return $this->db->get('multiple_bid_forbot b')->result_array();
    }

    function totalbotplay($user)
    {
        $query = "select count(if(set_profitratio_portfo_finalvalli !='0',1,NULL)) 'totalbidforbot' from etx_condition_for_btctousdt where user_id = '" . $user . "'";
        $data = $this->db->query($query);
        if ($data->num_rows() > 0) {
            $result = $data->row();
            return $result;
        } else {
            return 0;
        }
    }
    function get_currentstratage_id($table, $where)
    {
        $this->db->where($where);
        $data = $this->db->get($table);
        $get = $data->row();
        $num = $data->num_rows();
        if ($num) {
            return $get;
        } else {
            return false;
        }
    }
    function check_startage_idstart($table, $where)
    {
        $this->db->where($where);
        $data = $this->db->get($table);
        $get = $data->row();
        $num = $data->num_rows();
        if ($num) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    function check_bot_copytradeactive($table, $where)
    {
        $this->db->where($where);
        $data = $this->db->get($table);
        $get = $data->row();
        $num = $data->num_rows();
        if ($num) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    function tptalprofitforbot($user_id)
    {
        $this->db->select('e.*,round(sum(price_with_commision),6) as totalprofit');
        $this->db->where(array('e.user_id =' => $user_id));

        $data = $this->db->get('pwt_bids_for_buysell e');
        $get = $data->row();

        $num = $data->num_rows();

        if ($num) {
            return $get->totalprofit;
        } else {
            return false;
        }
    }
    function todasprofitforbot($user_id)
    {
        $this->db->select('e.*,round(sum(price_with_commision),6) as totalprofit');
        $this->db->where('e.user_id ', $user_id);
        $this->db->where('FROM_UNIXTIME(e.created_datetime,"%Y-%m-%d")', date('Y-m-d'));

        $data = $this->db->get('pwt_bids_for_buysell e');
        $get = $data->row();

        $num = $data->num_rows();

        if ($num) {
            return $get->totalprofit;
        } else {
            return false;
        }
    }
    function team_income($user_id){
        $this->db->where('pwt_notification.user_id',$user_id);
        $where = "type = 'bottrade2' or type = 'bottrade3' or type = 'bottrade4' or type = 'bottrade5' or type = 'bottrade6'";
        $this->db->where($where);
		$data = $this->db->get('pwt_notification');
		$get = $data->result();
		if ($get) {
			return $get;
		} else {
			return FALSE;
		}

    }
}
