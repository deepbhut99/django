<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Copytrade_model extends CI_Model {

    function get_master_users() {
        // $this->db->distinct('ec.master_user_id');
        $this->db->join('pwt_users e', 'e.id = ec.master_user_id');
        $this->db->group_by('ec.master_user_id');
        return $this->db->get('pwt_copy_trade ec')->result_array();
    }

    function get_users_list() {
        $this->db->select('e.username, ec.*');
        $this->db->join('pwt_users e', 'e.id = ec.user_id');        
        return $this->db->get_where('pwt_copy_trade ec', array('master_user_id' => $this->input->post('userid')))->result_array();
    }
}