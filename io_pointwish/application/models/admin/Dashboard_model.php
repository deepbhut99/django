<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Dashboard_model extends CI_Model
{
	function getTotalUserListCount()
	{
		$query = "select count(*) as total_users, sum(is_status) as active_users from pwt_users where is_delete = '1'";

		$data = $this->db->query($query);
		if ($data->num_rows() > 0) {
			return $data->row();
		} else {
			return 0;
		}
	}

	function getTodayActiveUsers()
	{
		$query = "select count(*) as total_users from pwt_users where is_status = 1 and is_delete = '1' and date_format(upgrade_time, '%Y-%m-%d') = '" . date('Y-m-d') . "'";

		$data = $this->db->query($query);
		if ($data->num_rows() > 0) {
			$result = $data->row();

			return $result->total_users;
		} else {
			return 0;
		}
	}

	function getTodayBusiness()
	{
		$query = "select sum(amount) as total_amount from pwt_user_notification where type = 'fund' and status = 100 and date_format(created_datetime, '%Y-%m-%d') = '" . date('Y-m-d') . "'";

		$data = $this->db->query($query);
		if ($data->num_rows() > 0) {
			$result = $data->row();

			return $result->total_amount;
		} else {
			return 0;
		}
	}

	function getTotalBusiness()
	{
		$query = "select sum(amount) as total_amount from pwt_user_notification where type = 'fund' and status = 100";

		$data = $this->db->query($query);
		if ($data->num_rows() > 0) {
			$result = $data->row();

			return $result->total_amount;
		} else {
			return 0;
		}
	}

	function getTodayWithdrawal()
	{
		$query = "select sum(amount) as total_amount from pwt_withdrawal where status = 'G' and date_format(given_datetime, '%Y-%m-%d') = '" . date('Y-m-d') . "'";

		$data = $this->db->query($query);
		if ($data->num_rows() > 0) {
			$result = $data->row();

			return $result->total_amount;
		} else {
			return 0;
		}
	}

	function getTodayPendingWithdrawal()
	{
		$query = "select sum(amount) as total_amount from pwt_withdrawal where status = 'P' and date_format(created_datetime, '%Y-%m-%d') = '" . date('Y-m-d') . "'";

		$data = $this->db->query($query);
		if ($data->num_rows() > 0) {
			$result = $data->row();

			return $result->total_amount;
		} else {
			return 0;
		}
	}

	function getTotalWithdrawal()
	{
		$query = "select sum(amount) as total_amount from pwt_withdrawal where status = 'G'";

		$data = $this->db->query($query);
		if ($data->num_rows() > 0) {
			$result = $data->row();

			return $result->total_amount;
		} else {
			return 0;
		}
	}

	function getTotalPendingWithdrawal()
	{
		$query = "select sum(amount) as total_amount from pwt_withdrawal where status = 'P'";

		$data = $this->db->query($query);
		if ($data->num_rows() > 0) {
			$result = $data->row();

			return $result->total_amount;
		} else {
			return 0;
		}
	}

	function getpaidunpaiduser($type)
	{
		if ($type == 1) {
			$query = "select * from pwt_users where package is not null and is_delete = '1'";
		} else {
			$query = "select * from pwt_users where package is null and is_delete = '1'";
		}
		return $this->db->query($query)->result();
	}
	
		function get_company_business($type, $start_limit, $end_limit)
	{
		$where = '';
		if ($type == 'today') {
			$where .= " and date_format(pwt_user_notification.created_datetime, '%Y-%m-%d') = '" . date('Y-m-d') . "'";
		}
		if ($this->input->get('search_text')) {
			$search_text = $this->input->get('search_text');

			$where .= " and pwt_users.username like '%" . $search_text . "%'";
		}
		$query = "select pwt_user_notification.*, pwt_users.username, pwt_users.email, pwt_users.wallet_amount, pwt_users.pwt_wallet, pwt_users.unlocked_pwt from pwt_user_notification join `pwt_users` on pwt_user_notification.from_user = pwt_users.id where type = 'fund' and status = 100 $where order by pwt_user_notification.id desc LIMIT $start_limit, $end_limit";

		$data = $this->db->query($query);
		if ($data->num_rows() > 0) {
			return $data->result();
		} else {
			return false;
		}
	}

	function get_company_business_total($type)
	{
		$where = '';
		if ($type == 'today') {
			$where .= " and date_format(pwt_user_notification.created_datetime, '%Y-%m-%d') = '" . date('Y-m-d') . "'";
		}
		if($this->input->get('search_text'))
		{
			$search_text = $this->input->get('search_text');

			$where .= " and pwt_users.username like '%".$search_text."%'";
		}
		$query = "select * from pwt_user_notification join `pwt_users` on pwt_user_notification.from_user = pwt_users.id where type = 'fund' and status = 100 $where";
		$data = $this->db->query($query);
		return $data->num_rows();
	}
}
