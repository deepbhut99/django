<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Document_model extends CI_Model 
{
	function getPendingDocumentList($start_limit, $end_limit)
	{
		$where = '';

		if($this->input->get('search_text'))
		{
			$search_text = $this->input->get('search_text');

			$where .= " and pwt_users_document.fname like '%".$search_text."%' or pwt_users_document.lname like '%".$search_text."%'";
		}

		$query = "select * from pwt_users_document where pwt_users_document.status = 0".$where. " order by pwt_users_document.id desc LIMIT $start_limit, $end_limit";

		$data = $this->db->query($query);
		if($data->num_rows() > 0)
		{
			return $data->result();
		}
		else
		{
			return false;
		}
	}

	function getPendingDocumentListCount()
	{
		$where = '';

		if($this->input->get('search_text'))
		{
			$search_text = $this->input->get('search_text');

			$where .= " and pwt_users_document.fname like '%".$search_text."%' or pwt_users_document.lname like '%".$search_text."%'";
		}

		$query = "select * from pwt_users_document join pwt_users on pwt_users_document.user_id = pwt_users.id where pwt_users_document.status = 0".$where;

		$data = $this->db->query($query);

		return $data->num_rows();
	}

	function getApprovedDocumentList($start_limit, $end_limit)
	{
		$where = '';

		if($this->input->get('search_text'))
		{
			$search_text = $this->input->get('search_text');

			$where .= " and pwt_users_document.fname like '%".$search_text."%' or pwt_users_document.lname like '%".$search_text."%'";
		}

		$query = "select * from pwt_users_document where pwt_users_document.status = 1".$where. " order by pwt_users_document.id desc LIMIT $start_limit, $end_limit";

		$data = $this->db->query($query);
		if($data->num_rows() > 0)
		{
			return $data->result();
		}
		else
		{
			return false;
		}
	}

	function getApprovedDocumentListCount()
	{
		$where = '';

		if($this->input->get('search_text'))
		{
			$search_text = $this->input->get('search_text');

			$where .= " and pwt_users_document.fname like '%".$search_text."%' or pwt_users_document.lname like '%".$search_text."%'";
		}

		$query = "select * from pwt_users_document where pwt_users_document.status = 1".$where;

		$data = $this->db->query($query);

		return $data->num_rows();
	}

	function getRejectedDocumentList($start_limit, $end_limit)
	{
		$where = '';

		if($this->input->get('search_text'))
		{
			$search_text = $this->input->get('search_text');

			$where .= " and pwt_users_document.fname like '%".$search_text."%' or pwt_users_document.lname like '%".$search_text."%'";
		}

		$query = "select * from pwt_users_document where pwt_users_document.status = 2".$where. " order by pwt_users_document.id desc LIMIT $start_limit, $end_limit";

		$data = $this->db->query($query);
		if($data->num_rows() > 0)
		{
			return $data->result();
		}
		else
		{
			return false;
		}
	}

	function getRejectedDocumentListCount()
	{
		$where = '';

		if($this->input->get('search_text'))
		{
			$search_text = $this->input->get('search_text');

			$where .= " and pwt_users_document.fname like '%".$search_text."%' or pwt_users_document.lname like '%".$search_text."%'";
		}

		$query = "select * from pwt_users_document where pwt_users_document.status = 2".$where;

		$data = $this->db->query($query);

		return $data->num_rows();
	}

}
