<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Fund_model extends CI_Model
{
	function getMyTransferAmount()
	{
		$this->db->select('h.*, e.username');
		$this->db->join('pwt_users e', 'e.id = h.to_user');
		$this->db->order_by('h.id', 'desc');
		return $this->db->get_where('pwt_transfer_history h', array('transfer_by' => 'admin'))->result_array();
	}
}
