<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ticket_model extends CI_Model 
{
	function getAllTotalTicketCount()
	{
		$query = "SELECT count(*) as total_count FROM `pwt_tickets`";
		$data = $this->db->query($query);

		if($data->num_rows())
		{
			$row = $data->row();

			return $row->total_count;
		}
		else
		{
			return 0;
		}
	}

	function getAllPendingTicketCount()
	{
		$query = "SELECT count(*) as total_count FROM `pwt_tickets` WHERE status = 'open'";
		$data = $this->db->query($query);

		if($data->num_rows())
		{
			$row = $data->row();

			return $row->total_count;
		}
		else
		{
			return 0;
		}
	}

	function getAllInProcessTicketCount()
	{
		$query = "SELECT count(*) as total_count FROM `pwt_tickets` WHERE status = 'process'";
		$data = $this->db->query($query);

		if($data->num_rows())
		{
			$row = $data->row();

			return $row->total_count;
		}
		else
		{
			return 0;
		}
	}	

	function getAllCloseTicketCount()
	{
		$query = "SELECT count(*) as total_count FROM `pwt_tickets` WHERE status = 'close'";
		$data = $this->db->query($query);

		if($data->num_rows())
		{
			$row = $data->row();

			return $row->total_count;
		}
		else
		{
			return 0;
		}
	}


	function getTicketListByType($start_limit, $end_limit, $status='')
	{
		if(!$status) {
			$status = $this->input->get('type', true);	
		} 
		$where = '';

		if($this->input->get('search_text'))
		{
			$search_text = $this->input->get('search_text');

			$where .= " and pwt_users.username like '%".$search_text."%'";
		}

		$query = "SELECT pwt_tickets.*, pwt_users.username, pwt_users.is_profile_update FROM `pwt_tickets` join `pwt_users` on pwt_tickets.user_id = pwt_users.id WHERE `status` = '".$status."'".$where." order by pwt_tickets.id desc LIMIT $start_limit, $end_limit";
		$data = $this->db->query($query);

		if($data->num_rows())
		{
			return $data->result();
		}
		else
		{
			return false;
		}
	}

	function getTicketListByTypeCount()
	{
		$status = $this->input->get('type', true);	

		$where = '';

		if($this->input->get('search_text'))
		{
			$search_text = $this->input->get('search_text');

			$where .= " and pwt_users.username like '%".$search_text."%'";
		}	

		$query = "SELECT * FROM `pwt_tickets` join `pwt_users` on pwt_tickets.user_id = pwt_users.id WHERE `status` = '".$status."'".$where;
		$data = $this->db->query($query);

		return $data->num_rows();
	}


	function getMyTicketChatList()
	{
		$ticket_id = $this->input->get('ticket_id', true);	

		$query = "SELECT pwt_tickets_chat.* FROM pwt_tickets_chat join pwt_users on pwt_tickets_chat.user_id = pwt_users.id WHERE pwt_tickets_chat.ticket_id = '".$ticket_id."' order by pwt_tickets_chat.id asc";
		$data = $this->db->query($query);

		if($data->num_rows())
		{
			return $data->result();
		}
		else
		{
			return false;
		}
	}

}
