<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Users_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	function getTotalUserList($start_limit, $end_limit)
	{
		$where = '';

		if ($this->input->get('search_text')) {
			$search_text = $this->input->get('search_text');

			$where .= " where username like '%" . $search_text . "%'";
		}

		$query = "select * from pwt_users " . $where . " order by id desc LIMIT $start_limit, $end_limit";

		$data = $this->db->query($query);
		if ($data->num_rows() > 0) {
			return $data->result();
		} else {
			return false;
		}
	}

	function getTotalUserListCount()
	{
		$where = '';

		if ($this->input->get('search_text')) {
			$search_text = $this->input->get('search_text');

			$where .= " where username like '%" . $search_text . "%'";
		}

		$query = "select * from pwt_users " . $where;

		$data = $this->db->query($query);

		return $data->num_rows();
	}

	function getTotalPaidUserList($start_limit, $end_limit)
	{
		$where = '';

		if ($this->input->get('search_text')) {
			$search_text = $this->input->get('search_text');

			$where .= " where username like '%" . $search_text . "%'";
		}

		$query = "select * from pwt_users where is_status = 1 and is_delete = '1'" . $where . " order by id desc LIMIT $start_limit, $end_limit";

		$data = $this->db->query($query);
		if ($data->num_rows() > 0) {
			return $data->result();
		} else {
			return false;
		}
	}

	function getTotalPaidUserListCount()
	{
		$where = '';

		if ($this->input->get('search_text')) {
			$search_text = $this->input->get('search_text');

			$where .= " and username like '%" . $search_text . "%' or mobile='" . $search_text . "' or email = '" . $search_text . "'";
		}

		$query = "select * from pwt_users where is_status = 1 and is_delete = '1'" . $where;

		$data = $this->db->query($query);

		return $data->num_rows();
	}

	function getTotalUnPaidUserList($start_limit, $end_limit)
	{
		$where = '';

		if ($this->input->get('search_text')) {
			$search_text = $this->input->get('search_text');

			$where .= " where username like '%" . $search_text . "%'";
		}

		$query = "select * from pwt_users where is_status = 0 and is_delete = '1'" . $where . " order by id desc LIMIT $start_limit, $end_limit";

		$data = $this->db->query($query);
		if ($data->num_rows() > 0) {
			return $data->result();
		} else {
			return false;
		}
	}

	function getTotalUnPaidUserListCount()
	{
		$where = '';


		if ($this->input->get('search_text')) {
			$search_text = $this->input->get('search_text');

			$where .= " where username like '%" . $search_text . "%'";
		}

		$query = "select * from pwt_users where is_status = 0 and is_delete = '1'" . $where;

		$data = $this->db->query($query);

		return $data->num_rows();
	}

	function getTodaysActiveUserList($start_limit, $end_limit)
	{
		$where = '';

		if ($this->input->get('search_text')) {
			$search_text = $this->input->get('search_text');

			$where .= " where username like '%" . $search_text . "%'";
		}

		$query = "select * from pwt_users where is_status = 1 and is_delete = '1' and date_format(upgrade_time, '%Y-%m-%d') = '" . date('Y-m-d') . "'" . $where . " order by id desc LIMIT $start_limit, $end_limit";

		$data = $this->db->query($query);
		if ($data->num_rows() > 0) {
			return $data->result();
		} else {
			return false;
		}
	}

	function getTodaysActiveUserListCount()
	{
		$where = '';

		if ($this->input->get('search_text')) {
			$search_text = $this->input->get('search_text');

			$where .= " where username like '%" . $search_text . "%'";
		}

		$query = "select * from pwt_users where is_status = 1 and is_delete = '1' and date_format(upgrade_time, '%Y-%m-%d') = '" . date('Y-m-d') . "'" . $where;

		$data = $this->db->query($query);

		return $data->num_rows();
	}

	function getcityname($id)
	{

		return  $this->db->get_where('pwt_cities', array('id' => $id))->row();
		echo $this->db->last_query();
	}

	function get_pwt_users($start_limit, $end_limit, $orderby)
	{
		$where = '';
		if ($this->input->get('search_text')) {
			$search_text = $this->input->get('search_text');
			$where .= " and username like '%" . $search_text . "%'";
		}
		if ($orderby == 'pwt' || $orderby == 'referral') {
			$order = '(unlocked_pwt + pwt_wallet)';
		} else if ($orderby == 'usdt') {
			$order = 'wallet_amount';
		}
		$query = "select * from pwt_users where is_status = 1 " . $where . " order by " . $order . "  desc LIMIT $start_limit, $end_limit";

		$data = $this->db->query($query);
		if ($data->num_rows() > 0) {
			return $data->result();
		} else {
			return false;
		}
	}

	function count_referral_users($user)
	{
		$this->db->select('count(*) as total_users, sum(is_status) as active_users');
		return $this->db->get_where('pwt_users', array('sponsor' => $user))->row_array();
	}

	function count_referral_income($user)
	{
		$this->db->select('sum(amount) as total');
		return $this->db->get_where('pwt_user_notification', array('from_user ' => $user, 'message' => "Direct Referral"))->row()->total;
	}

	function get_export_users($start = '', $end = '')
	{
		$where = '';
		if ($end) {
			$where = " AND DATE_FORMAT(created_time, '%d-%m-%Y') >= '$start' AND DATE_FORMAT(created_time, '%d-%m-%Y') <= '$end'";
		}
		$query = "select * from pwt_users where is_status = 1 AND is_delete = 1" . $where . " order by id desc";
		$data = $this->db->query($query);
		return $data->result();
	}
	
	function convert_wallet() {
		$pwt = get_pwt()['current_price'];
		$query = "update pwt_users set pwt_wallet = pwt_wallet + (wallet_amount / $pwt), wallet_amount = 0";
		return $this->db->query($query);
	}

	function get_users()
	{			
		$query = "select * from pwt_users where is_status = 1 AND sponsor_id > 0 AND DATE_FORMAT(created_time, '%Y-%m-%d') >= '2021-06-18' AND DATE_FORMAT(created_time, '%Y-%m-%d') <= '2021-06-18'";
		$data = $this->db->query($query);
		return $data->result();
	}

	function getTotalActiveUserListCount()
	{
		$where = '';

		if ($this->input->get('search_text')) {
			$search_text = $this->input->get('search_text');

			$where .= " and username like '%" . $search_text . "%'";
		}
		$query = "select * from pwt_users where is_status = 1 " . $where;
		$data = $this->db->query($query);
		return $data->num_rows();
	}
}
