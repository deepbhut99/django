<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Withdraw_model extends CI_Model 
{
	function getAllPendingWithdrawal($start_limit, $end_limit)
	{
		$where = '';

		if($this->input->get('search_text'))
		{
			$search_text = $this->input->get('search_text');

			$where .= " and pwt_users.username like '%".$search_text."%'";
		}

		$query = "select pwt_withdrawal.*, pwt_users.full_name, pwt_users.is_profile_update from pwt_withdrawal join pwt_users on pwt_withdrawal.session_id = pwt_users.id where pwt_withdrawal.status = 'P'".$where." order by pwt_withdrawal.id desc LIMIT $start_limit, $end_limit";

		$data = $this->db->query($query);
		if($data->num_rows() > 0)
		{
			return $data->result();
		}
		else
		{
			return false;
		}
	}

	function getAllPendingWithdrawalCount()
	{
		$where = '';

		if($this->input->get('search_text'))
		{
			$search_text = $this->input->get('search_text');

			$where .= " and pwt_users.username like '%".$search_text."%'";
		}

		$query = "select pwt_withdrawal.*, pwt_users.full_name from pwt_withdrawal join pwt_users on pwt_withdrawal.session_id = pwt_users.id where pwt_withdrawal.status = 'P'".$where;

		$data = $this->db->query($query);
		return $data->num_rows();
	}

	function getAllGivenWithdrawal($start_limit, $end_limit)
	{
		$where = '';

		if($this->input->get('search_text'))
		{
			$search_text = $this->input->get('search_text');

			$where .= " and pwt_users.username like '%".$search_text."%'";
		}

		$query = "select pwt_withdrawal.*, pwt_users.full_name from pwt_withdrawal join pwt_users on pwt_withdrawal.session_id = pwt_users.id where pwt_withdrawal.status = 'G'".$where." order by pwt_withdrawal.id desc LIMIT $start_limit, $end_limit";

		$data = $this->db->query($query);
		if($data->num_rows() > 0)
		{
			return $data->result();
		}
		else
		{
			return false;
		}
	}

	function getAllGivenWithdrawalCount()
	{
		$where = '';

		if($this->input->get('search_text'))
		{
			$search_text = $this->input->get('search_text');

			$where .= " and pwt_users.username like '%".$search_text."%'";
		}

		$query = "select pwt_withdrawal.*, pwt_users.full_name from pwt_withdrawal join pwt_users on pwt_withdrawal.session_id = pwt_users.id where pwt_withdrawal.status = 'G'".$where;

		$data = $this->db->query($query);
		return $data->num_rows();
	}

	function getAllRejectedWithdrawal($start_limit, $end_limit)
	{
		$where = '';

		if($this->input->get('search_text'))
		{
			$search_text = $this->input->get('search_text');

			$where .= " and pwt_users.username like '%".$search_text."%'";
		}

		$query = "select pwt_withdrawal.*, pwt_users.full_name from pwt_withdrawal join pwt_users on pwt_withdrawal.session_id = pwt_users.id where pwt_withdrawal.status = 'R'".$where." order by pwt_withdrawal.id desc LIMIT $start_limit, $end_limit";

		$data = $this->db->query($query);
		if($data->num_rows() > 0)
		{
			return $data->result();
		}
		else
		{
			return false;
		}
	}

	function getAllRejectedWithdrawalCount()
	{
		$where = '';

		if($this->input->get('search_text'))
		{
			$search_text = $this->input->get('search_text');

			$where .= " and pwt_users.username like '%".$search_text."%'";
		}

		$query = "select pwt_withdrawal.*, pwt_users.full_name from pwt_withdrawal join pwt_users on pwt_withdrawal.session_id = pwt_users.id where pwt_withdrawal.status = 'R'".$where;

		$data = $this->db->query($query);
		return $data->num_rows();
	}

}
