<?php
$this->load->view('admin/common/header');
$data = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100];
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php if (!empty($title)) echo $title; ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Trading</a></li>
      <li class="active"><?php if (!empty($title)) echo $title; ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box border-radius-1">
          <div class="box-header with-border">
            <h3 class="box-title">
              <?php if (!empty($title)) echo $title; ?>
            </h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body ">
            <form action='<?= base_url('admin/algorithm/updateCriteria'); ?>' method='post'>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="topValue">Top User Ratio</label>
                    <select class="form-control box-shadow-1" id="topValue" name="topValue">
                      <?php
                      foreach ($data as $item) {
                        echo "<option value='" . $item . "' " . ($item == $topValue ? 'selected' : '') . ">" . $item . "</option>";
                      }
                      ?>
                    </select>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="otherValue">Normal User Ratio</label>
                    <select class="form-control box-shadow-1" id="otherValue" name="otherValue">
                      <?php
                      foreach ($data as $item) {
                        echo "<option value='" . $item . "' " . ($item == $normalValue ? 'selected' : '') . ">" . $item . "</option>";
                      }
                      ?>
                    </select>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-black float-right">Update Algorithm Criteria</button>
            </form>
          </div>
        </div>
      </div>
    </div>
    
    <div class="row">
      <div class="col-md-12">
        <div class="box border-radius-1">
          <div class="box-header with-border">
            <h3 class="box-title">
              Company Profit
            </h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body ">
            <form action='<?= base_url('admin/algorithm/updateProfit'); ?>' method='post'>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="topValue">Current Profit %</label>
                    <input type="number" class="form-control box-shadow-1" value="<?= $profit; ?>" disabled />
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="otherValue">New Profit %</label>
                    <input type="number" class="form-control box-shadow-1" name="profit" step=1 min=0 max=100 value=0 />
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-black float-right">Update Profit Criteria</button>
            </form>
          </div>
          <!-- /.box-body -->

        </div>
        <!-- /.box -->


      </div>
      <!-- /.col -->

    </div>
    
    <div class="row">
      <div class="col-md-6">
        <div class="box border-radius-1">
          <div class="box-body ">
            <form method='post'>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="topValue">API</label>
                    <select class="form-control box-shadow-1" id="ddlAPI">
                      <option value="pro" <?=($api == 'pro' ? 'selected' : '');?>>Pro Coinbase</option>
                      <option value="huobi" <?=($api == 'huobi' ? 'selected' : '');?>>Huobi</option>
                      <option value="random" <?=($api == 'random' ? 'selected' : '');?>>Random</option>
                    </select>
                  </div>
                </div>
              </div>
              <!-- <button type="submit" class="btn btn-black float-right">Update Algorithm Criteria</button> -->
            </form>
          </div>
        </div>
      </div>
    </div>
    
    
    

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php $this->load->view('admin/common/footer'); ?>
<script>
  $(document).ready(function() {
    $("#trading_li").addClass("active menu-open");
    $("#trading_child_li").show();
    $("#algorithm_li").addClass("active");
    $('#ddlAPI').on('change', function(){
        api = $(this).val();
        $.ajax({
            type: 'POST',
            url: '<?=base_url('update-api');?>',
            data: {
                api
            },
            success: function(res){
                res = JSON.parse(res);
                if(res == 'true'){
                    toastr.success('API changed');
                }
            }
        });
    });
  });
</script>