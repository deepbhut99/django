<?php
$this->load->view('admin/common/header');
$data = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100];
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php if (!empty($title)) echo $title; ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Trading</a></li>
      <li class="active"><?php if (!empty($title)) echo $title; ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box border-radius-1">
          <div class="box-header with-border">
            <h3 class="box-title">
              <?php if (!empty($title)) echo $title; ?>
            </h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body ">
            <form action='<?= base_url('admin/algorithm/updateProfit'); ?>' method='post'>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="topValue">Current Profit %</label>
                    <input type="number" class="form-control box-shadow-1" value="<?= $profit; ?>" disabled />
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="otherValue">New Profit %</label>
                    <input type="number" class="form-control box-shadow-1" name="profit" step=1 min=0 max=100 value=0 />
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-black float-right">Update Algorithm Criteria</button>
            </form>
          </div>
          <!-- /.box-body -->

        </div>
        <!-- /.box -->


      </div>
      <!-- /.col -->

    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php $this->load->view('admin/common/footer'); ?>
<script>
  $(document).ready(function() {
    $("#trading_li").addClass("active menu-open");
    $("#trading_child_li").show();
    $("#profit_li").addClass("active");
  });
</script>