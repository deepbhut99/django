<script>
  $(document).ready(function() {
    var table = $('#example').DataTable({
      select: true,
      dom: 'Blfrtip',
      lengthMenu: [
        [10, 25, 50, 100, -1],
        [10, 25, 50, 100, "All"]
      ],
      dom: 'Bfrtip',
      pageLength: 100,
      buttons: [{
        extend: 'csv',
        text: ' Export a CSV'
      }, {
        extend: 'excel',
        text: ' Export a EXCEL'
      }, 'pageLength']
    });
    table.buttons().container().appendTo('#datatable_wrapper .col-md-6:eq(0)');

    function activateMenu(id) {
      $('#' + id).addClass('active');
    }
  });
</script>

<footer class="main-footer">

  <strong>Copyright &copy; <?php echo date('Y'); ?> All rights
    reserved.
</footer>


<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<!-- <script src="<?php echo base_url(); ?>assets/admin_panel/bower_components/jquery/dist/jquery.min.js"></script> -->
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url(); ?>assets/admin_panel/bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<script src="<?= base_url('assets/user_panel/libs/toastr/toastr.min.js') ?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url(); ?>assets/admin_panel/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url(); ?>assets/admin_panel/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo base_url(); ?>assets/admin_panel/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_panel/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url(); ?>assets/admin_panel/bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url(); ?>assets/admin_panel/bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_panel/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo base_url(); ?>assets/admin_panel/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url(); ?>assets/admin_panel/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url(); ?>assets/admin_panel/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>assets/admin_panel/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/admin_panel/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>assets/admin_panel/dist/js/demo.js"></script>

<!-- Data Table -->
<!-- <script src="<?php echo base_url(); ?>assets/admin_panel/plugins/datatable/datatables.min.js"></script> -->
<!-- <script src="<?php echo base_url(); ?>assets/admin_panel/plugins/datatable/dataTables.checkboxes.min.js"></script> -->

<script src="<?php echo base_url(); ?>assets/admin_panel/plugins/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_panel/plugins/datatables/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_panel/plugins/datatables/js/jszip.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_panel/plugins/datatables/js/buttons.html5.min.js"></script>


<script src="<?php echo base_url(); ?>assets/admin_panel/js/jquery.validate.js"></script>
<script src="<?php echo base_url(); ?>assets/admin_panel/js/bootstrap-dialog.js"></script>

<script src="<?= base_url('assets/user_panel/js/mask.js') ?>"></script>
<script src="<?= base_url('assets/user_panel/js/myapp.js?v' . rand()) ?>"></script>
<!-- <script src="<?= base_url('assets/admin_panel/js/app.js') ?>"></script> -->
</body>

</html>

<script>
  $(document).ready(function() {
    $(function() {
      $("#startdate").datepicker({
        format: 'dd-mm-yyyy',
        duration: "fast",
        orientation: "bottom auto",
        todayHighlight: true,
        endDate: '+0d',
      });
      $("#enddate").datepicker({
        format: 'dd-mm-yyyy',
        duration: "fast",
        orientation: "bottom auto",
        todayHighlight: true,
        endDate: '+0d',
      });
      $(".maildate").datepicker({
        format: 'dd-mm-yyyy',
        duration: "fast",
        orientation: "bottom auto",
        todayHighlight: true,       
      });
    });
    $("#vn-info-user").slideDown(1000);
    $(".card-user").addClass('active');
    $("#vn-info-ticket").slideDown(1000);
    $(".card-ticket").addClass('active');
    $("#vn-click-user").click(function() {
      $("#vn-info-user").slideDown(1000);
      $("#vn-info-business").hide();
      $("#vn-info-withdraw").hide();
      $(".card-user").addClass('active');
      $(".card-business").removeClass('active');
      $(".card-withdraw").removeClass('active');
    });
  });
  $("#vn-click-business").click(function() {
    $("#vn-info-business").slideDown(1000);
    $("#vn-info-user").hide();
    $("#vn-info-withdraw").hide();
    $(".card-user").removeClass('active');
    $(".card-business").addClass('active');
    $(".card-withdraw").removeClass('active');
  });
  $("#vn-click-withdraw").click(function() {
    $("#vn-info-withdraw").slideDown(1000);
    $("#vn-info-user").hide();
    $("#vn-info-business").hide();
    $(".card-user").removeClass('active');
    $(".card-business").removeClass('active');
    $(".card-withdraw").addClass('active');
  });
  $("#vn-click-ticket").click(function() {
    $("#vn-info-ticket").slideDown(1000);
  });
</script>

<script>
  $('body').addClass('sidebar-collapse');
</script>