<!DOCTYPE html>
<html ng-app="pwt">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>pwt | <?php if (!empty($title)) echo $title; ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- App favicon -->
  <link rel="shortcut icon" href="<?= base_url('assets/user_panel/images/logo/favicon.ico') ?>">

  <!-- Toastr css -->
  <link href="<?= base_url('assets/user_panel/libs/toastr/toastr.css') ?>" rel="stylesheet" type="text/css" />
  <script src="<?= base_url('assets/user_panel/libs/jquery/jquery.min.js') ?>"></script>
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin_panel/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin_panel/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin_panel/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin_panel/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin_panel/dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <!--<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin_panel/bower_components/morris.js/morris.css">-->
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin_panel/bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin_panel/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin_panel/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin_panel/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">


  <script src="<?= base_url('assets/user_panel/libs/angular/angular.min.js') ?>"></script>
  <script src="<?= base_url('assets/user_panel/js/angular-datatables.min.js') ?>"></script>

  <link href="<?= base_url('assets/admin_panel/dist/css/AdminLTE.css') ?>" rel="stylesheet" type="text/css" />

  <!-- Data Table -->
  <link rel="stylesheet" href="<?= base_url('assets/admin_panel/plugins/datatables/css/jquery.dataTables.min.css'); ?>">
  <link rel="stylesheet" href="<?= base_url('assets/admin_panel/plugins/datatables/css/buttons.dataTables.min.css'); ?>">
  <script>
    var BASE_URL = "<?php echo base_url(); ?>";
  </script>

</head>

<body class="hold-transition skin-blue sidebar-mini sidebar-collapse sidebar-collapse" ng-controller="App_Controller">
  <div class="wrapper">

    <header class="main-header">
      <!-- Logo -->
      <a href="<?php echo base_url(); ?>admin" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini">
          <img src="<?= base_url('assets/user_panel/images/logo/favicon.ico') ?>">
        </span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">
          <img src="<?= base_url('assets/front/images/logo/logo.svg') ?>">

        </span>
      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">


            <!-- User Account: style can be found in dropdown.less -->

            <a href="https://time.is/UTC" id="time_is_link" rel="nofollow" style="font-size:36px;color:white"></a>
            <span id="UTC_za00" class="mr-4" style="font-size:22px;color:white"></span>
            <script src="//widget.time.is/t.js"></script>
            <script>
              time_is_widget.init({
                UTC_za00: {}
              });
            </script>


            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="<?= base_url('assets/admin_panel/images/men.jpg') ?>" class="user-image" alt="User Image">
                <span class="hidden-xs"><?php echo ucwords($this->session->userdata('admin_username')); ?></span>
              </a>
              <ul class="dropdown-menu">
                <!-- User image -->
                <li class="user-header">
                  <img src="<?= base_url('assets/admin_panel/images/men.jpg') ?>" class="img-circle" alt="User Image">

                  <p>
                    <?php echo ucwords($this->session->userdata('admin_username')); ?>
                  </p>
                </li>
                <!-- Menu Body -->


                <!-- Menu Footer-->
                <li class="user-footer">
                  <!-- <div class="pull-left">
                    <a href="<?php echo base_url(); ?>admin/accounts" class="btn btn-default btn-flat">Profile</a>
                  </div> -->
                  <div class="text-center">
                    <a href="<?php echo base_url(); ?>admin/logout" class="btn btn-black">Sign out</a>
                  </div>
                </li>
              </ul>
            </li>
            <!-- Control Sidebar Toggle Button -->

          </ul>
        </div>
      </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu mt-5" data-widget="tree">


          <li class="" id="dashboard_li"><a href="<?php echo base_url('dashboard'); ?>"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
          <li class="treeview" id="users_management_li">
            <a href="#">
              <i class="fa fa-user-circle-o"></i> <span>Users Management</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu" id="users_management_child_li">
              <li id="total_users_li"><a href="<?php echo base_url('total-users'); ?>"><i class="fa fa-circle-o"></i> Users</a></li>
              <li id="users_li"><a href="<?php echo base_url('pwt-users'); ?>"><i class="fa fa-circle-o"></i> Total Users</a></li>
              <li id="paid_users_li"><a href="<?php echo base_url(); ?>paid-users"><i class="fa fa-circle-o"></i> Paid Users</a></li>
              <li id="unpaid_users_li"><a href="<?php echo base_url(); ?>unpaid-users"><i class="fa fa-circle-o"></i> Unpaid Users</a></li>
            </ul>
          </li>
          <li class="treeview" id="trading_li">
            <a href="#">
              <i class="fa fa-bar-chart"></i> <span>Trading </span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu" id="trading_child_li">
              <li id="bids_li"><a href="<?php echo base_url('trade-bids'); ?>"><i class="fa fa-circle-o"></i>Bids</a></li>
              <li id="copytrade_li"><a href="<?php echo base_url('copy-trade'); ?>"><i class="fa fa-circle-o"></i>Copy Trade</a></li>
              <li id="agency_li"><a href="<?php echo base_url('trade-agency'); ?>"><i class="fa fa-circle-o"></i>Agency</a></li>
              <li id="algorithm_li"><a href="<?php echo base_url('trade-algorithm'); ?>"><i class="fa fa-circle-o"></i>Manage Algorithm</a></li>
              <li id="winner_li"><a href="<?php echo base_url('trade-winner-list'); ?>"><i class="fa fa-circle-o"></i>Trade Winner</a></li>
            </ul>
          </li>
          <li class="treeview" id="documents_management_li">
            <a href="#">
              <i class="fa fa-file-text-o"></i> <span>Documents Management </span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu" id="documents_management_child_li">
              <li id="pending_documents_li"><a href="<?php echo base_url(); ?>pending-documents"><i class="fa fa-circle-o"></i> Pending Documents</a></li>
              <li id="approved_documents_li"><a href="<?php echo base_url(); ?>approved-documents"><i class="fa fa-circle-o"></i> Approved Documents</a></li>
              <li id="rejected_documents_li"><a href="<?php echo base_url(); ?>rejected-documents"><i class="fa fa-circle-o"></i> Rejected Documents</a></li>
            </ul>
          </li>
          <li class="treeview" id="transfer_fund_main_li">
            <a href="#">
              <i class="fa fa-exchange"></i> <span>Transfer Fund </span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu" id="transfer_fund_child_li">
              <li id="transfer_fund_li"><a href="<?php echo base_url(); ?>transfer-fund"><i class="fa fa-circle-o"></i> Transfer Fund</a></li>
              <li id="transfer_fund_history_li"><a href="<?php echo base_url(); ?>fund-history"><i class="fa fa-circle-o"></i> Transfer History</a></li>
              <li id="transfer_fund_business"><a href="<?php echo base_url('company-business/total'); ?>"><i class="fa fa-circle-o"></i> Company Business</a></li>
            </ul>
          </li>
          <li class="treeview" id="withdrawal_management_main_li">
            <a href="#">
              <i class="fa fa-users"></i> <span>Withdrawal Management </span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu" id="withdrawal_management_child_li">
              <li id="pending_withdrawal_li"><a href="<?php echo base_url(); ?>pending-withdrawal"><i class="fa fa-circle-o"></i> Pending Withdrawals</a></li>
              <li id="given_withdrawal_li"><a href="<?php echo base_url(); ?>given-withdrawal"><i class="fa fa-circle-o"></i> Given Withdrawals</a></li>
              <li id="rejected_withdrawal_li"><a href="<?php echo base_url(); ?>rejected-withdrawal"><i class="fa fa-circle-o"></i> Rejected Withdrawals</a></li>
            </ul>
          </li>
          <li class="" id="ticket_li"><a href="<?php echo base_url('admin/ticket'); ?>"><i class="fa fa-ticket"></i> <span>Ticket</span></a></li>
          <li class="" id="news_li"><a href="<?php echo base_url('admin/news'); ?>"><i class="fa fa-newspaper-o"></i> <span>News</span></a></li>
          <li class="" id="password_li"><a href="<?php echo base_url('admin/accounts/change_password'); ?>"><i class="fa fa-key"></i> <span>Change Password</span></a></li>
        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>