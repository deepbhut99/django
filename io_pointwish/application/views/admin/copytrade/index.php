<?php $this->load->view('admin/common/header'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" ng-controller="Users_Controller" ng-cloak>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php if (!empty($title)) echo $title; ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Trading</a></li>
            <li class="active"><?php if (!empty($title)) echo $title; ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box border-radius-1">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            <?php if (!empty($title)) echo $title; ?>
                        </h3>

                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php echo validation_errors(); ?>

                        <form id="data_form" name="data_form" action="" method="POST">

                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Users</label>
                                            <select class="form-control required box-shadow-1" id="user" name="user" ng-model="user">
                                                <option value="">Select Users</option>
                                                <?php foreach ($users as $user) { ?>
                                                    <option value="<?= $user['id'] ?>"><?= $user['full_name'] ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <button type="button" class="btn btn-black" ng-click="get_users(data_form);"> Get Users</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="fa fa-3x fa-spin fa-spinner mailbox-attachment-icon" ng-show="matrixLoader">
                        <div class="spinner-border" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                    </div>

                    <div ng-show="!matrixLoader && lists.length" style="overflow:auto">
                        <table datatable="ng" class="display table table-hover table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>S.No.</th>
                                    <th>Member ID</th>
                                    <th>Start Date</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="tree in lists">
                                    <td>{{$index+1}}</td>
                                    <td>{{tree.username}}</td>
                                    <td>{{tree.start_date}}</td>
                                    <td class="text-bold" ng-class="{'text-success' : tree.is_active == 1, 'text-danger' : tree.is_active == 0}">{{tree.is_active == 1 ? 'Active' : 'Inactive'}}</td>
                                    <td>
                                        <!-- <button class="btn btn-e5bc08" title="click here to change" onclick="open_reject_popup(<?= $row['id'] . ',' . $price . ',' . $row['user_id'] . ',' . $sponsor  ?>);">Change Status</button> -->
                                        <a href="javascript:void(0);" class="btn btn-black" ng-click="change_status(tree.id, tree.is_active)">Change</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->

                </div>
                <!-- /.box -->


            </div>
            <!-- /.col -->

        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php $this->load->view('admin/common/footer'); ?>
<script>
  $(document).ready(function() {
    $("#trading_li").addClass("active menu-open");
    $("#trading_child_li").show();
    $("#copytrade_li").addClass("active");
  });
</script>