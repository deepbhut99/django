<?php $this->load->view('admin/common/header'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php if (!empty($title)) echo $title; ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Business</a></li>
      <li class="active"><?php if (!empty($title)) echo $title; ?></li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box border-radius-1">
          <div class="box-header with-border">
            <h3 class="box-title">
              <?php if (!empty($title)) echo $title; ?>
            </h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body ">
            <div class="d-flex">
              <div class="mr-2">                
              </div>
              <div>
                <form id="" name="" class="mb-3" action="<?php echo base_url('company-business/'.$this->uri->segment('2')); ?>" method="GET">
                  <input style="background-image:url('<?php echo base_url(); ?>assets/admin_panel/images/searchicon.png');" type="text" class="search_input" id="search_text" name="search_text" placeholder="Search.." value="<?php if (!empty($_GET['search_text'])) echo $_GET['search_text']; ?>">

                  <button class="btn btn-e5bc08" type="submit">Search</button>
                  <a href="<?php echo base_url('company-business/'.$this->uri->segment('2')); ?>"><button class="btn btn-black" type="button">Reset</button></a>
                </form>
              </div>
            </div>
            <div class="table-responsive">
              <p class="pl-1">
                <strong>Total Users:</strong> <?php if (!empty($total_records)) echo $total_records; ?>
              </p>
              <div class="clearfix"></div>
              <?php
              if ($this->session->flashdata('error_message')) {
              ?>
                <div class="alert alert-success alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <?php echo $this->session->flashdata('error_message'); ?>
                </div>

              <?php
              }
              ?>
              <table id="example" class="display table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>S.No.</th>
                    <th>Login</th>
                    <th>Member ID</th>
                    <th>Email</th>
                    <th>Deposit</th>
                    <th>USDT Wallet</th>
                    <th>Locked pwt</th>
                    <th>Payment Date</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if (!empty($data_result)) {
                    if (!empty($_GET['offset'])) {
                      $i = ($_GET['offset'] * 10) - 9;
                    } else {
                      $i = 1;
                    }
                    $count = 1;
                    foreach ($data_result as $row) {
                  ?>
                      <tr id="tr_<?php echo $row->id; ?>">
                        <td><?= $count++; ?>.</td>
                        <td><a target="_blank" href="<?= site_url('admin/login/default_login?id=' . base64_encode($row->from_user)) ?>" class="btn btn-e5bc08 btn-sm">Login</a></td>
                        <td><?php echo $row->username; ?></td>
                        <td><?php echo $row->email; ?></td>
                        <td><?= $row->amount ?></td>
                        <td><?= number_format($row->wallet_amount, 2) ?></td>
                        <td><?= number_format($row->pwt_wallet + $row->unlocked_pwt, 2) ?></td>
                        <td><?= date('d M Y h:i A', strtotime($row->created_datetime)) ?></td>
                      </tr>
                    <?php
                      $i++;
                    }
                  } else {
                    ?>
                    <tr>
                      <td colspan="6" align="center">No Data Found !</td>
                    </tr>
                  <?php
                  }
                  ?>
                </tbody>
              </table>
              <?php
              if (!empty($links)) {
              ?>
                <nav aria-label="Page navigation example">
                  <ul class="pagination">
                    <?php echo $links; ?>
                  </ul>
                </nav>
              <?php
              }
              ?>
            </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php $this->load->view('admin/common/footer'); ?>
<script>
  $(document).ready(function() {
    $("#transfer_fund_child_li").addClass("active menu-open");
    $("#transfer_fund_child_li").show();
    $("#transfer_fund_business").addClass("active");

  });
</script>