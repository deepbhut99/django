<?php $this->load->view('admin/common/header'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Dashboard
      <label class="switch-user">
        <input type="checkbox" id="isMaintenance" <?= get_pwt()['is_maintenance'] == 1 ? 'checked' : '' ?>>
        <span class="slider-user round-user"></span>
      </label>
      (Maintenance)
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Dashboard</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="row">
      <div class="col-lg-4 col-sm-6 col-xs-12 cursor-pointer" data-toggle="tooltip" title="Click Here !!!" id="vn-click-user">
        <div class="small-box bg-theme-card border-radius-1 card-user">
          <div class="inner">
            <p class="mb-0 font-22 font-weight-700">Users</p>
          </div>
          <div class="icon">
            <img class="dashboard-icon" src="<?= base_url('assets/admin_panel/images/icons/users.svg') ?>" alt="Users">
          </div>
          <a href="<?php echo base_url(); ?>admin/user/active_users_today" class="small-box-footer border-radius-2">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-4 col-sm-6 col-xs-12 cursor-pointer" data-toggle="tooltip" title="Click Here !!!" id="vn-click-business">
        <div class="small-box bg-theme-card border-radius-1 card-business">
          <div class="inner">
            <p class="font-22 mb-0 font-weight-700">Business</p>
          </div>
          <div class="icon">
            <img class="dashboard-icon" src="<?= base_url('assets/admin_panel/images/icons/business.svg') ?>" alt="Business">
          </div>
          <a href="#" class="small-box-footer border-radius-2">&nbsp;</a>
        </div>
      </div>
      <div class="col-lg-4 col-sm-6 col-xs-12 cursor-pointer" data-toggle="tooltip" title="Click Here !!!" id="vn-click-withdraw">
        <div class="small-box bg-theme-card border-radius-1 card-withdraw">
          <div class="inner">
            <p class="mb-0 font-22 font-weight-700">Withdrawal</p>
          </div>
          <div class="icon">
            <img src="<?= base_url('assets/admin_panel/images/icons/withdraw.svg')?>" alt="Withdraw" class="dashboard-icon">
          </div>
          <a href="#" class="small-box-footer border-radius-2">&nbsp;</a>
        </div>
      </div>
    </div>


    <div class="row" id="vn-info-user">
      <div class="col-lg-3 col-sm-6 col-xs-12">
        <div class="small-box bg-theme-card border-radius-1">
          <div class="inner">
            <h3><?php if (!empty($today_active)) echo $today_active;
                else echo '0'; ?></h3>

            <p>Today's Active Users</p>
          </div>
          <div class="icon">
            <i class="ion ion-person-add"></i>
          </div>
          <a href="<?php echo base_url(); ?>admin/user/active_users_today" class="small-box-footer border-radius-2">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-3 col-sm-6 col-xs-12">
        <div class="small-box bg-theme-card border-radius-1">
          <div class="inner">
            <h3><?php if (!empty($total_users)) echo $total_users;
                else echo '0'; ?></h3>

            <p>Total Users</p>
          </div>
          <div class="icon">
            <i class="ion ion-person-add"></i>
          </div>
          <a href="<?php echo base_url(); ?>admin/user/total_users" class="small-box-footer border-radius-2">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-3 col-sm-6 col-xs-12">
        <div class="small-box bg-theme-card border-radius-1">
          <div class="inner">
            <h3><?php if (!empty($paid_users)) echo $paid_users;
                else echo '0'; ?></h3>

            <p>Active Users</p>
          </div>
          <div class="icon">
            <i class="ion ion-person-add"></i>
          </div>
          <a href="<?php echo base_url(); ?>admin/user/paid_users" class="small-box-footer border-radius-2">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-3 col-sm-6 col-xs-12">
        <div class="small-box bg-theme-card border-radius-1">
          <div class="inner">
            <h3><?php if (!empty($unpaid_users)) echo $unpaid_users;
                else echo '0'; ?></h3>

            <p>Inactive Users</p>
          </div>
          <div class="icon">
            <i class="ion ion-person-add"></i>
          </div>
          <a href="<?php echo base_url(); ?>admin/user/unpaid_users" class="small-box-footer border-radius-2">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
    </div>

    <div class="row" id="vn-info-business">
      <div class="col-lg-3 col-sm-6 col-xsm-12">
        <div class="small-box bg-theme-card border-radius-1">
          <div class="inner">
            <h3><?php if (!empty($today_business)) echo $today_business;
                else echo '0'; ?></h3>

            <p>Today's Business</p>
          </div>
          <div class="icon">
            <i class="fa fa-usd"></i>
          </div>
          <a href="<?= base_url('company-business/today'); ?>" class="small-box-footer border-radius-2">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-3 col-sm-6 col-xsm-12">
        <div class="small-box bg-theme-card border-radius-1">
          <div class="inner">
            <h3><?php if (!empty($total_business)) echo $total_business;
                else echo '0'; ?></h3>

            <p>Total Business</p>
          </div>
          <div class="icon">
            <i class="fa fa-usd"></i>
          </div>
          <a href="<?= base_url('company-business/total'); ?>" class="small-box-footer border-radius-2">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
    </div>

    <div class="row" id="vn-info-withdraw">
      <div class="col-lg-3 col-sm-6 col-xsm-12">
        <div class="small-box bg-theme-card border-radius-1">
          <div class="inner">
            <h3><?php if (!empty($today_pending_withdrawal)) echo $today_pending_withdrawal;
                else echo '0'; ?></h3>

            <p>Today's Pending Withdrawal</p>
          </div>
          <div class="icon">
            <i class="fa fa-usd"></i>
          </div>
          <a href="#" class="small-box-footer border-radius-2">&nbsp;</a>
        </div>
      </div>
      <div class="col-lg-3 col-sm-6 col-xsm-12">
        <div class="small-box bg-theme-card border-radius-1">
          <div class="inner">
            <h3><?php if (!empty($total_pending_withdrawal)) echo $total_pending_withdrawal;
                else echo '0'; ?></h3>

            <p>Total Pending Withdrawal</p>
          </div>
          <div class="icon">
            <i class="fa fa-usd"></i>
          </div>
          <a href="#" class="small-box-footer border-radius-2">&nbsp;</a>
        </div>
      </div>
      <div class="col-lg-3 col-sm-6 col-xsm-12">
        <div class="small-box bg-theme-card border-radius-1">
          <div class="inner">
            <h3><?php if (!empty($today_withdrawal)) echo $today_withdrawal;
                else echo '0'; ?></h3>

            <p>Today's Withdrawal</p>
          </div>
          <div class="icon">
            <i class="fa fa-usd"></i>
          </div>
          <a href="#" class="small-box-footer border-radius-2">&nbsp;</a>
        </div>
      </div>
      <div class="col-lg-3 col-sm-6 col-xsm-12">
        <div class="small-box bg-theme-card border-radius-1">
          <div class="inner">
            <h3><?php if (!empty($total_withdrawal)) echo $total_withdrawal;
                else echo '0'; ?></h3>

            <p>Total Withdrawal</p>
          </div>
          <div class="icon">
            <i class="fa fa-usd"></i>
          </div>
          <a href="#" class="small-box-footer border-radius-2">&nbsp;</a>
        </div>
      </div>
    </div>

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php $this->load->view('admin/common/footer'); ?>
<script type="text/javascript">
  $(document).ready(function() {
    $("#dashboard_li").addClass("active");
    //jQuery listen for checkbox change
    $("#isMaintenance").change(function() {
      var is_check;
      if (this.checked) {
        is_check = 1;
      } else {
        is_check = 0;
      }
      $.ajax({
        type: "post",
        url: "<?= base_url('admin/dashboard/set_maintenance') ?>",
        data: {
          status: is_check
        },
        // dataType: "json",
        success: function(response) {
          BootstrapDialog.show({
            title: "Message",
            message: "Changed successfully !",
          });
          location.reload();
        }
      });
    });
  });
</script>