<?php $this->load->view('admin/common/header'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php if (!empty($title)) echo $title; ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Document Management</a></li>
      <li class="active"><?php if (!empty($title)) echo $title; ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box border-radius-1">
          <div class="box-header with-border">
            <h3 class="box-title">
              <?php if (!empty($title)) echo $title; ?>

            </h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body ">

            <div>
              <form id="" name="" class="mb-3" action="<?php echo base_url(); ?>admin/document/pending_documents" method="GET">
                <input style="background-image:url('<?php echo base_url(); ?>assets/admin_panel/images/searchicon.png');" type="text" class="search_input" id="search_text" name="search_text" placeholder="Search user.." value="<?php if (!empty($_GET['search_text'])) echo $_GET['search_text']; ?>">

                <button class="btn btn-e5bc08" type="submit">Search</button>
                <a href="<?php echo base_url(); ?>admin/document/pending_documents"><button class="btn btn-black" type="button">Reset</button></a>



              </form>
            </div>

            <div class="table-responsive">

              <p class="pl-1"><strong>Total Records:</strong> <?php if (!empty($total_records)) echo $total_records; ?></p>

              <div class="clearfix"></div>

              <?php
              if ($this->session->flashdata('error_message')) {
              ?>
                <div class="alert alert-success alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <?php echo $this->session->flashdata('error_message'); ?>
                </div>

              <?php
              }
              ?>

              <table id="example" class="display table table-hover table-striped" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>S.No.</th>
                    <th>Login</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Document ID</th>
                    <th>Active Users</th>
                    <th>Status</th>
                    <th>Action </th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if (!empty($data_result)) {

                    if (!empty($_GET['offset'])) {
                      $i = ($_GET['offset'] * 10) - 9;
                    } else {
                      $i = 1;
                    }

                    foreach ($data_result as $row) {

                  ?>
                      <tr id="tr_<?php echo $row->id; ?>">
                        <td><?= $i; ?>.</td>
                        <td><a target="_blank" href="<?= site_url('admin/login/default_login?id=' . base64_encode($row->user_id)) ?>" class="btn btn-e5bc08 btn-sm">Login</a></td>
                        <td><?= $row->fname; ?></td>
                        <td><?= $row->lname ?></td>
                        <td><?= $row->document_id ?></td>
                        <td><?= get_referral_users($row->user_id) ?></td>
                        <td>
                          <a href="javascript:void(0);" class="btn btn-e5bc08" style="cursor: default;" id="status_text_<?php echo $row->id; ?>">Pending</a>
                        </td>
                        <td id="change_status_td_<?php echo $row->id; ?>">
                          <a title="ID Proof" href="<?php echo base_url(); ?>uploads/user_documents/<?php echo $row->id_proof; ?>" class="btn btn-black" download><i class="fa fa-download" aria-hidden="true"></i></a>
                          <a title="Sample Document" href="<?php echo base_url(); ?>uploads/user_documents/<?php echo $row->document_file; ?>" class="btn btn-black" download><i class="fa fa-download" aria-hidden="true"></i></a>
                          <a href="javascript:void(0);" class="btn btn-success" onclick="approve_document('<?= $row->id ?>', '<?= $row->document_type ?>', '<?= $row->user_id ?>');">Approve</a>
                          <a href="javascript:void(0);" class="btn btn-danger" onclick="open_reject_popup('<?= $row->id ?>', '<?= $row->document_type ?>', '<?= $row->user_id ?>');">Reject</a>
                        </td>

                      </tr>
                    <?php
                      $i++;
                    }
                  } else {
                    ?>
                    <tr>
                      <td colspan="8" class="text-center">No Data Found !</td>
                    </tr>
                  <?php
                  }
                  ?>
                </tbody>
              </table>

              <?php
              if (!empty($links)) {
              ?>
                <nav aria-label="Page navigation example">
                  <ul class="pagination">
                    <?php echo $links; ?>
                  </ul>
                </nav>
              <?php
              }
              ?>

            </div>
          </div>
          <!-- /.box-body -->

        </div>
        <!-- /.box -->


      </div>
      <!-- /.col -->

    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php $this->load->view('admin/common/footer'); ?>

<script type="text/javascript">
  $(document).ready(function() {
    $("#documents_management_li").addClass("active menu-open");
    $("#documents_management_child_li").show();
    $("#pending_documents_li").addClass("active");
  });

  function approve_document(id, type, user_id) {
    BootstrapDialog.show({
      title: "Confirm",
      message: "Do you really want to approve it ?",
      buttons: [{
          label: 'Yes',
          cssClass: 'btn-primary ',
          action: function(dialogItself) {

            var url = '<?php echo base_url(); ?>admin/document/approve_document';
            $.ajax({
              type: "post",
              url: url,
              data: {
                id: id,
                type: type,
                user_id: user_id
              },
              // dataType: "json",
              success: function(response) {
                dialogItself.close();

                $("#change_status_td_" + id).html('');
                $("#status_text_" + id).html('Approve');
                $("#status_text_" + id).removeClass('btn-warning');
                $("#status_text_" + id).addClass('btn-primary');


                BootstrapDialog.show({
                  title: "Message",
                  message: "Dcoument aproved successfully !",
                });

              }

            });

          }
        },
        {
          label: 'No',
          cssClass: 'btn-warning',
          action: function(dialogItself) {
            dialogItself.close();
          }
        }
      ]
    });

  }

  function open_reject_popup(id, type, user_id) {
    $("#reject_id").val(id);
    $("#type").val(type);
    $("#user_id").val(user_id);
    $("#reject_reason").val('');

    $("#reject_popup").modal('show');
  }


  function submit_reject() {
    var form = $("#reject_form");

    if (form.valid() == false) {
      return false;
    } else {
      $("#reject_button_id").html('<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>');
      $("#reject_button_id").attr('disable', true);

      var reject_id = $("#reject_id").val();
      var reject_reason = $("#reject_reason").val();

      var url = '<?php echo base_url(); ?>admin/document/reject_document';
      var dataString = 'reject_id=' + reject_id + '&reject_reason=' + reject_reason;

      $.ajax({
        type: "POST",
        data: {
          reject_id: reject_id,
          reject_reason: reject_reason,
          type: $("#type").val(),
          user_id: $("#user_id").val()
        },
        url: url,
        dataType: "json",
        success: function(response) {
          $("#reject_popup").modal('hide');

          $("#change_status_td_" + reject_id).html('');
          $("#status_text_" + reject_id).html('Rejected');
          $("#status_text_" + reject_id).removeClass('btn-warning');
          $("#status_text_" + reject_id).addClass('btn-danger');


          BootstrapDialog.show({
            title: "Message",
            message: "Dcoument rejected successfully !",
          });

        }

      });

    }

  }
</script>

<!-- sample modal content -->
<div id="reject_popup" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Reject </h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">


        <form id="reject_form" name="reject_form" class="form-horizontal" action="">

          <input type="hidden" id="reject_id" name="reject_id">
          <input type="hidden" name="user_id" id="user_id">
          <input type="hidden" name="type" id="type">

          <div class="form-group">
            <label class="control-label col-sm-2" for="reason">Reason:</label>
            <div class="col-sm-10">
              <textarea class="form-control required" id="reject_reason" name="reject_reason"></textarea>
            </div>
          </div>

          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
              <button type="button" class="btn btn-info" id="reject_button_id" onclick="submit_reject();">Submit</button>
            </div>
          </div>
        </form>


      </div>
    </div>
  </div>
</div>
<!-- /.modal -->