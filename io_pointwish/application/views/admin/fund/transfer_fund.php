<?php $this->load->view('admin/common/header'); ?>

<style type="text/css">
  .success_message {
    color: green;
  }
</style>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php if (!empty($title)) echo $title; ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Transfer</a></li>
      <li class="active"><?php if (!empty($title)) echo $title; ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box border-radius-1">
          <div class="box-header with-border">
            <h3 class="box-title">
              <?php if (!empty($title)) echo $title; ?>
            </h3>

          </div>

          <!-- /.box-header -->
          <div class="box-body">
            <?php echo validation_errors(); ?>

            <form id="data_form" name="data_form" action="" method="POST" enctype="multipart/form-data">

              <?php
              if ($this->session->flashdata('error_message')) {
              ?>
                <div class="alert alert-success alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <?php echo $this->session->flashdata('error_message'); ?>
                </div>

              <?php
              }
              ?>

              <div class="form-body">

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                          <label class="control-label">Coin</label>
                          <select class="form-control required box-shadow-1" name="type" id="type">
                          <option value="pwt">pwt</option>
                          <option value="USDT" selected>USDT</option>
                          </select>
                        </div>
                      </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label">Member Id</label>
                      <input type="text" class="form-control required box-shadow-1" id="member_id" name="member_id">

                      <p id="member_name_section" class="success_message"></p>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label">Amount</label>
                      <input type="text" class="form-control required box-shadow-1" id="amount" name="amount" onkeypress="return isNumber(event);">
                    </div>
                  </div>
                </div>
                <div class="form-actions">
                  <button type="submit" class="btn btn-black float-right"> Transfer</button>
                </div>
              </div>
          </div>

          </form>
        </div>
        <!-- /.box-body -->

      </div>
      <!-- /.box -->


    </div>
    <!-- /.col -->

</div>
<!-- /.row -->

</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php $this->load->view('admin/common/footer'); ?>

<?php
if ($this->session->flashdata("error_message")) {
?>
  <script type="text/javascript">
    BootstrapDialog.show({
      title: "Message",
      message: "<?php echo $this->session->flashdata("error_message"); ?>",
    });
  </script>
<?php
}
?>
<script>
  $(document).ready(function() {
    $("#transfer_fund_main_li").addClass("active menu-open");
    $("#transfer_fund_child_li").show();
    $("#transfer_fund_li").addClass("active");
  });
</script>
<script type="text/javascript">
  var errorMsg2 = '',
    $valid2 = false;
  $.validator.addMethod("isMemberAvailabe", function(val, elem) {
    var len = $("#member_id").val();
    if (len.length >= 5) {
      $.ajax({
        async: false,
        url: '<?php echo base_url(); ?>admin/fund/isMemberAvailabe',
        type: "POST",
        dataType: "json",
        data: {
          member_id: $("#member_id").val()
        },
        success: function(response) {
          if (response.status == 1) {
            errorMsg2 = 'Member Available !';
            $valid2 = true;

            $("#member_name_section").html('Member Name: ' + response.member_name);
          } else if (response.status == 2) {
            errorMsg2 = 'Member Not Exist !';
            $valid2 = false;

            $("#member_name_section").html('');
          }
        }
      });
    } else {
      errorMsg2 = 'Username shouled be minimum 5 letter';
      $valid2 = false;
    }

    $.validator.messages["isMemberAvailabe"] = errorMsg2;
    return $valid2;
  }, '');


  $("#data_form").validate({
    onkeyup: function(element, event) {
      if (event.which === 9 && this.elementValue(element) === "") {
        return;
      } else {
        this.element(element);
      }
    },

    rules: {
      member_id: {
        required: true,
        isMemberAvailabe: true,
      },
    },
  });

  function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  function submit_data() {
    var form = $("#data_form");

    if (form.valid() == false) {
      return false;
    } else {
      accept_submit();
    }
  }


  function accept_submit() {
    BootstrapDialog.show({
      title: "Confirm",
      message: "Do you really want to transfer amount ?",
      buttons: [{
          label: 'Yes',
          cssClass: 'btn-primary yes_class',
          action: function(dialogItself) {
            $(".yes_class").html('<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>');
            $(".yes_class").attr('disable', true);

            $("#submit_button_id").html('<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>');
            $("#submit_button_id").attr('disable', true);

            $("#data_form").submit();
          }
        },
        {
          label: 'No',
          cssClass: 'btn-warning',
          action: function(dialogItself) {
            dialogItself.close();
          }
        }
      ]
    });

  }
</script>