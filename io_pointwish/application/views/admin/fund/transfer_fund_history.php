<?php $this->load->view('admin/common/header'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php if (!empty($title)) echo $title; ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Users Management</a></li>
      <li class="active"><?php if (!empty($title)) echo $title; ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box border-radius-1">
          <div class="box-header with-border">
            <h3 class="box-title">
              <?php if (!empty($title)) echo $title; ?>

            </h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">


            <div class="table-responsive">

              <p class="pl-1"><strong>Total Records:</strong> <?php if (!empty($total_records)) echo $total_records; ?></p>

              <div class="clearfix"></div>

              <?php
              if ($this->session->flashdata('error_message')) {
              ?>
                <div class="alert alert-success alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <?php echo $this->session->flashdata('error_message'); ?>
                </div>

              <?php
              }
              ?>

              <table id="fundtable" class="display table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>S.No.</th>
                    <th>User Name</th>
                    <th>Transfer Date</th>
                    <th>Amount</th>
                    <th>pwt</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $count = 1;
                  foreach ($data_result as $row) {

                  ?>
                    <tr id="tr_<?php echo $row['id']; ?>">
                      <td><?= $count++ ?>.</td>
                      <td><?= $row['username'] ?></td>

                      <td><?php echo date('d M Y H:m', strtotime($row['created_datetime'])); ?></td>
                      <td><?= $row['transfer_type'] == 'pwt' ? '---' : '$ ' . $row['amount'] ?></td>
                      <td><?= $row['transfer_type'] == 'pwt' ? $row['amount'] : '---' ?></td>
                    </tr>

                  <?php
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
          <!-- /.box-body -->

        </div>
        <!-- /.box -->


      </div>
      <!-- /.col -->

    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php $this->load->view('admin/common/footer'); ?>

<script>
  $(document).ready(function() {
    $("#transfer_fund_main_li").addClass("active menu-open");
    $("#transfer_fund_child_li").show();
    $("#transfer_fund_history_li").addClass("active");
    var table = $('#fundtable').DataTable({
      select: true,
      dom: 'Blfrtip',
      lengthMenu: [
        [10, 25, 50, 100, -1],
        [10, 25, 50, 100, "All"]
      ],
      paging: true,
      dom: 'Bfrtip',
      pageLength: 100,
      buttons: [{
        extend: 'csv',
        text: ' Export a CSV'
      }, {
        extend: 'excel',
        text: ' Export a EXCEL'
      }, 'pageLength']
    });
    table.buttons().container().appendTo('#datatable_wrapper .col-md-6:eq(0)');
  });
</script>