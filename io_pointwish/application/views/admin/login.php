
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>pwt | <?php if(!empty($title)) echo $title; ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin_panel/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin_panel/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin_panel/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin_panel/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin_panel/plugins/iCheck/square/blue.css">

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  
  <link rel="shortcut icon" href="<?= base_url('assets/user_panel/images/logo/favicon.ico') ?>">

  <style type="text/css">
  .error
  {
    color: red;

  }  
  </style>

</head>
<body class="hold-transition login-page">
<div class="login-box">
  
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">
    <!--<img src="<?php echo base_url(); ?>assets/front/images/logo.png" width="150" alt="Logo here">-->
    </p>

<?php
if($this->session->flashdata('error_session'))
{
?>
  <div class="alert alert-danger alert-dismissible">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <?php echo $this->session->flashdata('error_session'); ?>
  </div>
<?php } ?>

    <form id="data_form" name="data_form" method="post" action="<?php echo base_url('d46aa6f5f9401257b243ce2dfe442b9c'); ?>">

    <input type="hidden" name="redirect_url" id="redirect_url" value="<?php if($this->input->get('next')) echo $this->input->get('next'); ?>">

      <div class="form-group has-feedback">
        <input class="form-control required" placeholder="Username" value="<?php echo $this->input->cookie('a_username'); ?>" type="text" id="username" name="username">
      <?php echo form_error('username', '<div class="error">', '</div>'); ?>

        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>

      <div class="form-group has-feedback">
        
        <input class="form-control required" placeholder="Password" value="<?php echo $this->input->cookie('a_password'); ?>" type="password" id="password" name="password">
        <?php echo form_error('password', '<div class="error">', '</div>'); ?>  
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>

      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
            <?php $rem = $this->input->cookie('a_username'); ?>
            <input class="form-check-input" type="checkbox" id="remember_me" name="remember_me" value="1" <?php if($rem) echo 'checked'; ?>>

               Remember Me
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Login</button>
        </div>
        <!-- /.col -->
      </div>
    </form>


  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="<?php echo base_url();?>assets/admin_panel/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url();?>assets/admin_panel/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url();?>assets/admin_panel/plugins/iCheck/icheck.min.js"></script>
</body>
</html>

<script src="<?php echo base_url(); ?>assets/admin_panel/js/jquery.validate.js"></script>

<script>

  $("#data_form").validate();

  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>
