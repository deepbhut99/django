<?php $this->load->view('admin/common/header'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" ng-controller="Users_Controller" ng-cloak>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php if (!empty($title)) echo $title; ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Trading</a></li>
            <li class="active"><?php if (!empty($title)) echo $title; ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="">
            <div class="col-md-6">
                <div class="box border-radius-1">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            Common Promotial Mail
                        </h3>
                    </div>
                    <div class="box-body">
                        <form action="" method="POST" name="promo_form" id="promo_form">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="form-group col-lg-4">
                                                <label class="control-label">Id From</label>
                                                <input type="number" name="idfrom" class="form-control box-shadow-1 required" required>
                                            </div>
                                            <div class="form-group col-lg-4">
                                                <label class="control-label">Id To</label>
                                                <input type="number" name="idto" class="form-control box-shadow-1 required" required>
                                            </div>
                                            <div class="form-group col-lg-4">
                                                <label class="control-label">Date</label>
                                                <input type="text" name="date" class="form-control box-shadow-1 maildate required" required readonly>
                                            </div>
                                        </div>
                                        <div class="">
                                            <div class="form-group">
                                                <label class="control-label">Comments</label>
                                                <textarea name="comments" cols="3" class="form-control box-shadow-1"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <button type="submit" class="btn btn-black float-right"> Send</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="box border-radius-1">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            Individual Promotial Mail
                        </h3>

                    </div>
                    <div class="box-body">
                        <form action="" method="POST" name="mail_form" id="mail_form">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="form-group col-lg-6">
                                                <label class="control-label">Username</label>
                                                <input type="text" name="username" class="form-control box-shadow-1 required" required>
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <label class="control-label">Date</label>
                                                <input type="text" name="date" class="form-control box-shadow-1 maildate required" required readonly>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-lg-6">
                                                <label class="control-label">Total Referral</label>
                                                <input type="number" name="total" min="0" class="form-control box-shadow-1">
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <label class="control-label">Active Referral</label>
                                                <input type="number" name="active" class="form-control box-shadow-1">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-lg-6">
                                                <label class="control-label">pwt</label>
                                                <input type="number" name="pwt" class="form-control box-shadow-1">
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <label class="control-label">USDT</label>
                                                <input type="number" name="usdt" class="form-control box-shadow-1">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="form-actions">
                                    <button type="submit" class="btn btn-black float-right"> Send</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="box border-radius-1">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            CSV Promotional Mail
                        </h3>

                    </div>
                    <div class="box-body">
                        <form action="" method="POST" name="csv_form" id="csv_form">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label">Select CSV</label>
                                    <input type="file" name="csvfile" id="csvfile" class="form-control box-shadow-1 required" required>
                                </div>
                                <div class="form-actions">
                                    <button type="submit" id="send_btn" class="btn btn-black float-right"> Send</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
            <div class="col-md-6">
                <div class="box border-radius-1">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            News
                        </h3>

                    </div>
                    <div class="box-body">
                        <form action="" method="POST" name="data_form" id="data_form">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label">Select Photo</label>
                                    <input type="file" name="file" id="file" class="form-control box-shadow-1 required" required>
                                </div>
                                <div class="form-actions">
                                    <button type="submit" id="news_btn" class="btn btn-black float-right"> Upload</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6"></div>
            <div class="col-md-6">
                <div class="box border-radius-1">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            News
                        </h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body ">
                        <div class="table-responsive">
                            <table class="display table table-hover table-striped" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Sr.No</th>
                                        <th>Image</th>
                                        <th>Date</th>
                                        <th>Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $count = 1;
                                    foreach ($news as $row) { ?>
                                        <tr>
                                            <td><?= $count++ ?></td>
                                            <td><img class="notify-image" src="<?= base_url('uploads/news/' . $row['document']) ?>" alt=""></td>
                                            <td><?= date('d M, Y', strtotime($row['created_time']))  ?></td>
                                            <td class="v-align-inherit"><button onclick="remove_news('<?= $row['id'] ?>')" class="btn btn-black">Delete</button></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<?php $this->load->view('admin/common/footer'); ?>
<script type="text/javascript">
    $(document).ready(function() {
        $("#news_li").addClass("active");
        var table = $('.display').DataTable()
        $("#data_form").validate();
        $("#data_form").submit(function(e) {
            var form = $("#data_form");
            if (form.valid() == false) {
                return false;
            } else {
                e.preventDefault();
                var file_size = $('#file')[0].files[0].size;
                const file = Math.round((file_size / 1024));
                if (file >= 4096) {
                    alert("File too Big, please select a file less than 4mb");
                    return false;
                }
                var fileInput = $('#file').val();
                // Allowing file type
                var allowedExtensions = /(\.jpg|\.jpeg|\.png)$/i;
                if (!allowedExtensions.exec(fileInput)) {
                    alert('Invalid file type');
                    $('#file').val('');
                    return false;
                }
                var url = '<?php echo base_url(); ?>admin/news/add_news';
                $.ajax({
                    type: "post",
                    url: url,
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function(response) {
                        e = JSON.parse(response);
                        BootstrapDialog.show({
                            title: "Message",
                            message: e.message,
                        });
                        if (e.success) {
                            window.location.reload(true);
                        }
                    }
                });
            }
        })

        $("#mail_form").validate();
        $("#mail_form").submit(function(e) {
            var form = $("#mail_form");
            if (form.valid() == false) {
                return false;
            } else {
                e.preventDefault();
                $.ajax({
                    type: "post",
                    url: "<?= base_url('admin/news/promotional_mail') ?>",
                    data: $("#mail_form").serialize(),
                    // dataType: "json",
                    success: function(response) {
                        e = JSON.parse(response);
                        if (e.success == true) {
                            BootstrapDialog.show({
                                title: "Message",
                                message: e.message,
                            });
                            // window.location.reload(true);
                        } else {
                            BootstrapDialog.show({
                                title: "Message",
                                message: e.message,
                            });
                        }
                    }
                });
            }
        })

        $("#promo_form").validate();
        $("#promo_form").submit(function(e) {
            var form = $("#promo_form");
            if (form.valid() == false) {
                return false;
            } else {
                e.preventDefault();
                $.ajax({
                    type: "post",
                    url: "<?= base_url('admin/news/common_promo_mail') ?>",
                    data: $("#promo_form").serialize(),
                    // dataType: "json",
                    success: function(response) {
                        e = JSON.parse(response);
                        if (e.success == true) {
                            BootstrapDialog.show({
                                title: "Message",
                                message: e.message,
                            });
                            // window.location.reload(true);
                        } else {
                            BootstrapDialog.show({
                                title: "Message",
                                message: e.message,
                            });
                        }
                    }
                });
            }
        })

        $("#csv_form").validate();
        $("#csv_form").submit(function(e) {
            var form = $("#csv_form");
            if (form.valid() == false) {
                return false;
            } else {
                e.preventDefault();
                var fileInput = $('#csvfile').val();
                // Allowing file type
                var allowedExtensions = /(\.csv|\.xlsx)$/i;
                if (!allowedExtensions.exec(fileInput)) {
                    alert('Invalid file type');
                    $('#csvfile').val('');
                    return false;
                }
                var url = '<?php echo base_url(); ?>admin/news/csv_promo_mail';
                $.ajax({
                    type: "post",
                    url: url,
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function(response) {
                        e = JSON.parse(response);
                        BootstrapDialog.show({
                            title: "Message",
                            message: e.message,
                        });
                        // if (e.success) {
                        //     window.location.reload(true);
                        // }
                    }
                });
            }
        })
    });

    function remove_news(id) {
        BootstrapDialog.show({
            title: "Confirm",
            message: "Do you really want to delete .?",
            buttons: [{
                    label: 'Yes',
                    cssClass: 'btn-primary ',
                    action: function(dialogItself) {
                        var url = '<?php echo base_url(); ?>admin/news/delete_news';
                        $.ajax({
                            type: "post",
                            url: url,
                            data: {
                                'id': id
                            },
                            // dataType: "json",
                            success: function(response) {
                                dialogItself.close();
                                e = JSON.parse(response);
                                BootstrapDialog.show({
                                    title: "Message",
                                    message: e.message,
                                });
                                window.location.reload(true);
                            }
                        });
                    }
                },
                {
                    label: 'No',
                    cssClass: 'btn-warning',
                    action: function(dialogItself) {
                        dialogItself.close();
                    }
                }
            ]
        });
    }
</script>