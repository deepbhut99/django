<?php $this->load->view('admin/common/header'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php if (!empty($title)) echo $title; ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">My Account</a></li>
            <li class="active"><?php if (!empty($title)) echo $title; ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box border-radius-1">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            <?php if (!empty($title)) echo $title; ?>
                        </h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php echo validation_errors(); ?>
                        <?php
                        if ($this->session->flashdata('error_message')) {
                        ?>
                            <div class="alert alert-warning alert-dismissible">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <?php echo $this->session->flashdata('error_message'); ?>
                            </div>
                        <?php
                        }
                        ?>
                        <?php
                        if ($this->session->flashdata('success_message')) {
                        ?>
                            <div class="alert alert-success alert-dismissible">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <?php echo $this->session->flashdata('success_message'); ?>
                            </div>
                        <?php
                        }
                        ?>
                        <form id="data_form" name="data_form" action="" method="POST" enctype="multipart/form-data">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Old Password</label>
                                            <input type="password" class="form-control required box-shadow-1" id="old_password" name="old_password" value="">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">New Password</label>
                                            <input type="password" class="form-control required box-shadow-1" id="new_password" name="new_password" value="">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Confirm Password</label>
                                            <input type="password" class="form-control required box-shadow-1" id="confirm_password" name="confirm_password" value="" equalTo="#new_password">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-black float-right"> Update</button>
                                <!-- <a href="#" class="btn btn-inverse">Cancel</a> -->
                            </div>
                        </form>
                    </div>
                    <!-- /.box-body -->

                </div>
                <!-- /.box -->


            </div>
            <!-- /.col -->

        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php $this->load->view('admin/common/footer'); ?>

<script type="text/javascript">
    $("#data_form").validate({
        rules: {
            new_password: {
                minlength: 6,
            },
            confirm_password: {
                minlength: 6,
            },
        },
    });
</script>