<?php $this->load->view('admin/common/header'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php if (!empty($title)) echo $title; ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Users Management</a></li>
      <li class="active"><?php if (!empty($title)) echo $title; ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box border-radius-1">
          <div class="box-header with-border">
            <h3 class="box-title">
              <?php if (!empty($title)) echo $title; ?>
            </h3>
            <a href="<?php echo base_url(); ?>admin/ticket" class="btn btn-black float-right">Back</a>
          </div>
          <!-- /.box-header -->
          <div class="box-body ">
            <?php
            if ($this->session->flashdata('success_message')) {
            ?>
              <div class="alert alert-success alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <?php echo $this->session->flashdata('success_message'); ?>
              </div>

            <?php
            }
            ?>

            <div class="table-responsive">

              <button class="btn btn-black float-left" onclick="close_all('<?= $offset ?>')">Close All Ticket</button>
              <p class="pull-right"><strong>Total Records:</strong> <?php if (!empty($total_records)) echo $total_records; ?></p>

              <div class="clearfix"></div>



              <table id="example" class="display table table-hover table-striped" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>S.No.</th>
                    <th>Login</th>
                    <th>Ticket ID</th>
                    <th>Member ID</th>
                    <th>Subject</th>
                    <th>Email</th>
                    <th>KYC</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if (!empty($data_result)) {

                    if (!empty($_GET['offset'])) {
                      $i = ($_GET['offset'] * 10) - 9;
                    } else {
                      $i = 1;
                    }

                    foreach ($data_result as $row) {

                  ?>
                      <tr id="tr_<?php echo $row->id; ?>">
                        <td><?php echo $i; ?>.</td>
                        <td><a target="_blank" href="<?= site_url('admin/login/default_login?id=' . base64_encode($row->user_id)) ?>" class="btn btn-e5bc08 btn-sm">Login</a></td>
                        <td><?php echo $row->ticket_id; ?></td>
                        <td><?php echo $row->username; ?></td>
                        <td><?php echo $row->subject; ?></td>
                        <td><?php echo $row->email; ?></td>
                        <td><?= $row->is_profile_update == 1 ? 'Active' : 'Inactive' ?></td>
                        <td id="status_area_<?php echo $row->ticket_id; ?>">
                          <?php
                          if ($row->status == 'open') {
                          ?>
                            <a href="javascript:void(0);" class="label label-default font-14">Open</a>
                          <?php
                          } else if ($row->status == 'process') {
                          ?>
                            <a href="javascript:void(0);" class="label label-danger font-14">In Process</a>
                          <?php
                          } else if ($row->status == 'close') {
                          ?>
                            <a href="javascript:void(0);" class="label label-success font-14">Closed</a>
                          <?php
                          }
                          ?>
                        </td>

                        <td class="text-center">
                          <a href="<?php echo base_url(); ?>admin/ticket/view_ticket?ticket_id=<?php echo $row->ticket_id; ?>&vt=o" class="btn btn-e5bc08" target="_blank"> View Details</a>
                          <a href="<?php echo base_url(); ?>admin/ticket/view_chat?ticket_id=<?php echo $row->ticket_id; ?>&vt=o" class="btn btn-black" target="_blank"> Conversation</a>
                          <a href="javascript:void(0);" class="btn btn-e6e6e6" onclick="close_ticket('<?php echo $row->ticket_id; ?>');"> Close ticket</a>
                        </td>

                      </tr>
                    <?php
                      $i++;
                    }
                  } else {
                    ?>
                    <tr>
                      <td colspan="9" class="text-center">No Data Found !</td>
                    </tr>
                  <?php
                  }
                  ?>
                </tbody>
              </table>

              <?php
              if (!empty($links)) {
              ?>
                <nav aria-label="Page navigation example">
                  <ul class="pagination">
                    <?php echo $links; ?>
                  </ul>
                </nav>
              <?php
              }
              ?>

            </div>
          </div>
          <!-- /.box-body -->

        </div>
        <!-- /.box -->


      </div>
      <!-- /.col -->

    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php $this->load->view('admin/common/footer'); ?>


<script type="text/javascript">
  function close_ticket(id) {
    BootstrapDialog.show({
      title: "Confirm",
      message: "Do you really want to perform this action ?",
      buttons: [{
          label: 'Yes',
          cssClass: 'btn-primary ',
          action: function(dialogItself) {

            var url = '<?php echo base_url(); ?>admin/ticket/close_ticket';
            var dataString = 'id=' + id;

            $.ajax({
              type: "POST",
              data: dataString,
              url: url,
              dataType: "json",
              success: function(response) {
                dialogItself.close();

                $("#status_area_" + id).html('<span class="label label-danger font-14">close</span>');

                BootstrapDialog.show({
                  title: "Message",
                  message: "Status Changed Successfully !",
                });
              }

            });

          }
        },
        {
          label: 'No',
          cssClass: 'btn-warning',
          action: function(dialogItself) {
            dialogItself.close();
          }
        }
      ]
    });

  }

  function close_all(offset) {
    BootstrapDialog.show({
      title: "Confirm",
      message: "Do you really want to close all it ?",
      buttons: [{
          label: 'Yes',
          cssClass: 'btn-primary ',
          action: function(dialogItself) {

            var url = '<?php echo base_url(); ?>admin/ticket/close_all_ticket';
            $.ajax({
              type: "post",
              url: url,
              data: {
                offset: offset,
              },
              // dataType: "json",
              success: function(response) {
                dialogItself.close();
                location.reload();
                // BootstrapDialog.show({
                //   title: "Message",
                //   message: "Deleted successfully!",
                // });

              }

            });

          }
        },
        {
          label: 'No',
          cssClass: 'btn-warning',
          action: function(dialogItself) {
            dialogItself.close();
          }
        }
      ]
    });

  }
</script>