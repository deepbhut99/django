<?php $this->load->view('admin/common/header'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Ticket
      <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Ticket</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="row">
      <div class="col-lg-3 col-sm-6 col-xsm-12 cursor-pointer" data-toggle="tooltip" title="Click Here !!!" id="vn-click-ticket">
        <div class="small-box bg-theme-card border-radius-1 card-dash card-ticket">
          <div class="inner">
            <p class="font-22 mb-0 font-weight-700">Tickets</p>
          </div>
          <div class="icon">
            <img class="dashboard-icon" src="<?= base_url('assets/admin_panel/images/icons/tckt.svg')?>" alt="Ticket">
          </div>
          <a class="small-box-footer border-radius-2">&nbsp;</a>
        </div>
      </div>
    </div>


    <div class="row" id="vn-info-ticket">
      <div class="col-lg-4 col-sm-6 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-theme-card border-radius-1">
          <div class="inner">
            <h3><?php if (!empty($total_pending_ticket)) echo $total_pending_ticket;
                else echo 0; ?> </h3>

            <p>Open Tickets</p>
          </div>
          <div class="icon">
            <i class="ion ion-email"></i>
          </div>
          <a href="<?php echo base_url(); ?>admin/ticket/tickets_list?type=open" class="small-box-footer border-radius-2">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-4 col-sm-6 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-theme-card border-radius-1">
          <div class="inner">
            <h3><?php if (!empty($total_inprocess_ticket)) echo $total_inprocess_ticket;
                else echo 0; ?></h3>

            <p>In-process Tickets</p>
          </div>
          <div class="icon">
            <i class="ion ion-email"></i>
          </div>
          <a href="<?php echo base_url(); ?>admin/ticket/tickets_list?type=process" class="small-box-footer border-radius-2">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-4 col-sm-6 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-theme-card border-radius-1">
          <div class="inner">
            <h3><?php if (!empty($total_close_ticket)) echo $total_close_ticket;
                else echo 0; ?></h3>

            <p>Closed Tickets</p>
          </div>
          <div class="icon">
            <i class="ion ion-email"></i>
          </div>
          <a href="<?php echo base_url(); ?>admin/ticket/tickets_list?type=close" class="small-box-footer border-radius-2">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <!-- ./col -->
    </div>
    <!-- /.row -->



  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php $this->load->view('admin/common/footer'); ?>
<script type="text/javascript">
  $(document).ready(function() {
    $("#ticket_li").addClass("active");
  });
</script>