<?php $this->load->view('admin/common/header'); ?>
<link rel='stylesheet' href='<?php echo base_url(); ?>assets/admin_panel/dist/css/ticket-chat.css'>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php if (!empty($title)) echo $title; ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">My Account</a></li>
            <li class="active"><?php if (!empty($title)) echo $title; ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            <?php if (!empty($title)) echo $title; ?>

                        </h3>

                        <?php
                        if (!empty($_GET['vt']) && ($_GET['vt'] == 'o')) {
                        ?>
                            <a href="<?php echo base_url(); ?>admin/ticket/tickets_list?type=open" class="btn btn-primary pull-right">Back</a>
                        <?php
                        } else if (!empty($_GET['vt']) && ($_GET['vt'] == 'pr')) {
                        ?>
                            <a href="<?php echo base_url(); ?>admin/ticket/tickets_list?type=process" class="btn btn-primary pull-right">Back</a>
                        <?php
                        } else if (!empty($_GET['vt']) && ($_GET['vt'] == 'c')) {
                        ?>
                            <a href="<?php echo base_url(); ?>admin/ticket/tickets_list?type=close" class="btn btn-primary pull-right">Back</a>
                        <?php
                        }
                        ?>

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">



                        <div id="frame">
                            <div class="content" style="width: 100%">
                                <div class="contact-profile font-24">
                                    <img src="<?php echo base_url(); ?>assets/user_panel/images/user.jpg" alt="" />
                                    <p>Admin</p>

                                </div>

                                <div class="clearfix"></div>
                                <div class="messages font-24">
                                    <ul>
                                        <?php
                                        if (!empty($data_result)) {
                                            foreach ($data_result as $row) {
                                                if ($row->from == 'admin') {
                                        ?>
                                                    <li class="sent pull-left">
                                                        <img src="<?php echo base_url(); ?>assets/user_panel/images/user.jpg" alt="" />
                                                        <p><?php echo nl2br($row->description); ?>
                                                            <?php if (!empty($row->image)) { ?> <a href="<?= base_url() . '/uploads/ticket_attachment/' . $row->image ?> " target="_blank">View Attechment</a> <?php } ?></p>
                                                    </li>
                                                <?php
                                                } else {
                                                ?>
                                                    <li class="replies pull-right">
                                                        <?php
                                                        if (!empty($row->profile_pic)) {
                                                        ?>
                                                            <img src="<?php echo base_url(); ?>uploads/user_images/<?php echo $row->profile_pic; ?>" alt="" />
                                                        <?php
                                                        } else {
                                                        ?>
                                                            <img src="<?php echo base_url(); ?>assets/user_panel/images/user.jpg" alt="" />
                                                        <?php
                                                        }
                                                        ?>
                                                        <p><?php echo nl2br($row->description); ?>
                                                            <?php if (!empty($row->image)) { ?> <a href="<?= base_url() . '/uploads/ticket_attachment/' . $row->image ?> " target="_blank">View Attechment</a> <?php } ?></p>
                                                    </li>
                                        <?php
                                                }
                                            }
                                        }
                                        ?>


                                    </ul>
                                </div>
                                <div class="message-input">
                                    <div class="wrap">
                                        <form id="data_form" name="data_form" action="<?php echo base_url(); ?>admin/ticket/submit_ticket_reply" method="POST" enctype="multipart/form-data">

                                            <input type="hidden" id="ticket_id" name="ticket_id" value="<?php echo $this->input->get('ticket_id'); ?>">

                                            <textarea class="font-24" placeholder="Write your message..." rows="5" class="" id="description" name="description" required></textarea>
                                            <input type="file" name="image" class="">
                                            <!-- <i class="fa fa-paperclip attachment" aria-hidden="true"></i> -->
                                            <button class="submit pull-left" type="submit">Reply</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>



                    </div>
                    <!-- /.box-body -->

                </div>
                <!-- /.box -->


            </div>
            <!-- /.col -->

        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php $this->load->view('admin/common/footer'); ?>


<!-- <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css'> -->
<?php
if ($this->session->flashdata("error_message")) {
?>
    <script type="text/javascript">
        BootstrapDialog.show({
            title: "Message",
            message: "<?php echo $this->session->flashdata("error_message"); ?>",
        });
    </script>
<?php
}
?>

<script type="text/javascript">
    $(document).ready(function() {
        $(".messages").animate({
            scrollTop: $(document).height()
        }, "fast");
    });
</script>