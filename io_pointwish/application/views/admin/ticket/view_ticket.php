<?php $this->load->view('admin/common/header'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php if(!empty($title)) echo $title; ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">My Account</a></li>
        <li class="active"><?php if(!empty($title)) echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">
              <?php if(!empty($title)) echo $title; ?>
                
              </h3>

                <?php
                if(!empty($_GET['vt']) && ($_GET['vt'] == 'o'))
                {
                ?>
                    <a href="<?php echo base_url(); ?>admin/ticket/tickets_list?type=open" class="btn btn-primary pull-right">Back</a>
                <?php
                }
                else if(!empty($_GET['vt']) && ($_GET['vt'] == 'pr'))
                {
                ?>
                    <a href="<?php echo base_url(); ?>admin/ticket/tickets_list?type=process" class="btn btn-primary pull-right">Back</a>
                <?php
                }
                else if(!empty($_GET['vt']) && ($_GET['vt'] == 'c'))
                {
                ?>
                    <a href="<?php echo base_url(); ?>admin/ticket/tickets_list?type=close" class="btn btn-primary pull-right">Back</a>
                <?php
                }
                ?>

            </div>
            <!-- /.box-header -->
            <div class="box-body">


        <table class="table table-hover">

            <tr>
                <td><strong>Ticket ID:</strong></td>
                <td>
                <?php echo $data_result->ticket_id; ?>
                </td>
            </tr>

            <tr>
                <td ><strong>First Name:</strong></td>
                <td ><?php echo $data_result->first_name; ?></td>
            </tr>

            <tr>
                <td ><strong>Member Id:</strong></td>
                <td><?php echo $data_result->last_name; ?></td>
            </tr>

            <tr>
                <td><strong>Email:</strong></td>
                <td><?php echo $data_result->email; ?></td>
            </tr>

            <tr>
                <td><strong>Attachment:</strong></td>
                <td>
                    <?php
                    if(!empty($data_result->attachment))
                    {
                    ?>
                        <a href="<?php echo base_url(); ?>uploads/ticket_attachment/<?php echo $data_result->attachment; ?>" class="btn btn-primary" download="">Download</a>
                    <?php
                    }
                    ?>            
                </td>
            </tr>

            <tr>
                <td><strong>Subject:</strong></td>
                <td><?php echo $data_result->subject; ?></td>
            </tr>

             <tr>
                <td><strong>Description:</strong></td>
                <td><?php echo nl2br($data_result->description); ?></td>
            </tr>

             <tr>
                <td><strong>status:</strong></td>
                <td>
                    <?php
                    if($data_result->status == 'open')
                    {
                    ?>
                        <a href="javascript:void(0);" class="label label-default">Open</a>
                    <?php 
                    } 
                    else if($data_result->status == 'process')
                    {
                    ?>
                        <a href="javascript:void(0);" class="label label-danger">In Process</a>
                    <?php 
                    }
                    else if($data_result->status == 'close')
                    {
                    ?>
                        <a href="javascript:void(0);" class="label label-success">Closed</a>
                    <?php
                    }
                    ?>                          
                </td>
            </tr>


             <tr>
                <td><strong>Created Date:</strong></td>
                <td><?php echo date('d-m-Y H:i A', strtotime($data_result->modified_datetime)); ?></td>
            </tr>

        </tbody>
        </table>

            </div>
            <!-- /.box-body -->

          </div>
          <!-- /.box -->


        </div>
        <!-- /.col -->

      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('admin/common/footer'); ?>