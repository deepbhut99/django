<?php $this->load->view('admin/common/header'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" ng-controller="Users_Controller" ng-cloak>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php if (!empty($title)) echo $title; ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Trading</a></li>
            <li class="active"><?php if (!empty($title)) echo $title; ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box border-radius-1">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            <?php if (!empty($title)) echo $title; ?>
                        </h3>
                    </div>
                    <!-- <div class="fa fa-3x fa-spin fa-spinner mailbox-attachment-icon" ng-show="matrixLoader">
                        <div class="spinner-border" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                    </div> -->

                    <div>
                        <table id="agencytable" class="display table table-hover table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>S.No.</th>
                                    <th>Member ID</th>
                                    <th>Total Users</th>
                                    <th>Active Agency</th>
                                    <th>Current Level</th>
                                    <th>Business</th>
                                    <th>Commission</th>
                                    <th>Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                // var_dump($users);
                                // exit(0);
                                $count = 1;
                                foreach ($users as $row) { ?>
                                    <tr>
                                        <td><?= $count++ ?></td>
                                        <td><?= $row['username'] ?></td>
                                        <td><?= $row['total_users'] ? $row['total_users'] : 0 ?></td>
                                        <td><?= $row['total_agency'] ? $row['total_agency'] : 0 ?></td>
                                        <td><?= $row['level'] ?></td>
                                        <td><?= $row['business'] ? number_format($row['business'], 2)  : 0 ?></td>
                                        <td><?= $row['commission'] ? number_format($row['commission'], 2) : 0 ?></td>
                                        <td><?= $row['date'] ? date('d m, Y', strtotime($row['date'])) : '--' ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->

                </div>
                <!-- /.box -->


            </div>
            <!-- /.col -->

        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php $this->load->view('admin/common/footer'); ?>
<script>
    $(document).ready(function() {
        $("#trading_li").addClass("active menu-open");
        $("#trading_child_li").show();
        $("#agency_li").addClass("active");
        var table = $('#agencytable').DataTable({
            select: true,
            dom: 'Blfrtip',
            lengthMenu: [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, "All"]
            ],
            paging: true,
            dom: 'Bfrtip',
            pageLength: 100,
            buttons: [{
                extend: 'csv',
                text: ' Export a CSV'
            }, {
                extend: 'excel',
                text: ' Export a EXCEL'
            }, 'pageLength']
        });
        table.buttons().container().appendTo('#datatable_wrapper .col-md-6:eq(0)');
    });
</script>