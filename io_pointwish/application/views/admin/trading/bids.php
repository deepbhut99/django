<?php if ($settings->auto_refresh == 1) { ?>
    <meta http-equiv="refresh" content="30">
<?php } ?>
<?php $this->load->view('admin/common/header'); ?>
<style>
    .dt-buttons {
        margin-top: .5rem !important;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php if (!empty($title)) echo $title; ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Trading</a></li>
            <li class="active"><?php if (!empty($title)) echo $title; ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box border-radius-1">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            <?php if (!empty($title)) echo $title; ?>
                        </h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body ">
                        <div class="table-responsive">
                            <div class="d-md-flex justify-content-between">
                                <div>
                                    <p><strong>Total Bids: </strong> <?= count($result); ?></p>
                                    <p><strong>Total Trade Amount: </strong><label for="" id="total_trade"></label></p>
                                    <p><strong>Total Profit Given: </strong><label for="" id="total_profit"></label></p>
                                    <div class="clearfix"></div>
                                </div>
                                <div>
                                    <div class="box border-3 pl-3 pr-3">
                                        <h4>Total User : <span><b>5210</b></span></h4>
                                        <h4>Total Amount : <span><b>$15,460</b></span></h4>
                                    </div>
                                </div>
                                <div></div>
                                <div></div>
                                <div></div>
                            </div>
                            <?php
                            if ($this->session->flashdata('error_message')) {
                            ?>
                                <div class="alert alert-success alert-dismissible">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <?php echo $this->session->flashdata('error_message'); ?>
                                </div>

                            <?php
                            }
                            ?>
                            <label class="switch-user">
                                <input type="checkbox" id="refresh-toggle" <?= $settings->auto_refresh == 1 ? 'checked' : '' ?>>
                                <span class="slider-user round-user"></span>
                            </label>
                            <form id="export_form" name="export_form" class="mb-6" action="#" novalidate="">
                                <div class="col-lg-3 pr-0" style="padding-left: 0">
                                    <div class="d-flex">
                                        <!-- <input type="text" class="form-control ml-2 mr-2" placeholder="Start Date" onfocus="(this.type='date')" onblur="(this.type='text')">
                                        <input type="text" name="" id="" class="form-control ml-2 mr-2" placeholder="End Date" onfocus="(this.type='date')" onblur="(this.type='text')"> -->
                                        <input ng-model="startdate" id="startdate" type="text" class="form-control mr-2 required" placeholder="Start Date" required readonly>
                                        <input ng-model="enddate" id="enddate" type="text" class="form-control mr-2 required" placeholder="End Date" required readonly>
                                        <button type="button" ng-click="exportData(export_form)" id="history" class="btn btn-black" style="height: 34px; width:41px !important"><i class="fa fa-search" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                                <div class="col-lg-9"></div>
                            </form>
                            <table id="bidsTable" class="display table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Coin</th>
                                        <th>User ID</th>
                                        <th>Device</th>
                                        <th>Country</th>
                                        <th>Win Ratio</th>
                                        <th>LastWin Ratio</th>
                                        <th>Date Time</th>
                                        <th>Low</th>
                                        <th>High</th>
                                        <th>Base Amount</th>
                                        <th>Closing Value</th>
                                        <th>M-Closing Value</th>
                                        <th>M Value</th>
                                        <th>Prediction</th>
                                        <th>Trade Amount</th>
                                        <th>Profit</th>
                                        <th>B Value</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $count = 1;
                                    $amount = 0;
                                    $profit = 0;
                                    foreach ($result as $row) {
                                        $amount += $row['total_amount'];
                                        $profit += ($row['result'] == 1 ? $row['total_amount'] + ($row['total_amount'] * 95 / 100) : 0);
                                        $win = ($row['result'] == 1 ? $row['total_amount'] + ($row['total_amount'] * 95 / 100) : 0);
                                        $mvalue = ($row['closeVal'] != $row['mCloseVal'] ? abs((float)$row['value'] - (float)$row['mCloseVal']) : 0);
                                    ?>
                                        <tr id="tr_<?php echo $row->id; ?>" class="<?= $win > 0 ? 'bg-td-green' : '' ?> <?= $mvalue > 0 ? 'bg-td-yellow' : ''?>">
                                            <td><?= $count++ ?></td>
                                            <td><?= $row['coin'] ?></td>
                                            <td><?= $row['username'] ?></td>
                                            <td><?= $row['my_os'] ?></td>
                                            <td><?= $row['login_country'] ?></td>
                                            <td><?= $row['winRatio'] ?></td>
                                            <td><?= $row['lastWinRatio'] ?></td>
                                            <td><?= date('d-m-Y h:i:s', $row['timestamp'] / 1000)  ?></td>
                                            <td><?= $row['low'] ?></td>
                                            <td><?= $row['high'] ?></td>
                                            <td><?= $row['value'] ?></td>
                                            <td><?= $row['closeVal'] ?></td>
                                            <td><?= $row['mCloseVal'] ?></td>
                                            <td><?= number_format($mvalue, 2) ?></td>
                                            <td><?= $row['growth'] == '1' ? 'Up' : 'Down' ?></td>
                                            <td><?= $row['total_amount'] ?></td>
                                            <td><?= $win ?></td>
                                            <td><?= $row['bootCloseval'] ?></td>

                                        </tr>
                                    <?php } ?>
                                    <script>
                                        $('#total_trade').html("<?= round($amount, 2) ?>");
                                        $('#total_profit').html("<?= round($profit, 2) . ' (' . round(($profit / $amount * 100), 2) . '%)' ?>");
                                    </script>

                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->
<?php $this->load->view('admin/common/footer'); ?>
<script>
    $(document).ready(function() {
        $("#export_form").validate();
        $("#trading_li").addClass("active menu-open");
        $("#trading_child_li").show();
        $("#bids_li").addClass("active");
        var table = $('#bidsTable').DataTable({
            select: true,
            dom: 'Blfrtip',
            lengthMenu: [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, "All"]
            ],
            dom: 'Bfrtip',
            pageLength: 100,
            buttons: [{
                extend: 'csv',
                text: ' Export a CSV'
            }, {
                extend: 'excel',
                text: ' Export a EXCEL'
            }, 'pageLength']
        });
        table.buttons().container().appendTo('#datatable_wrapper .col-md-6:eq(0)');
        
        //jQuery listen for checkbox change
        $("#refresh-toggle").change(function() {
            var refresh;
            if (this.checked) {
                refresh = 1;
            } else {
                refresh = 0;
            }
            $.ajax({
                type: "post",
                url: "<?= base_url('admin/trading/change_status') ?>",
                data: {
                    status: refresh
                },
                // dataType: "json",
                success: function(response) {
                    BootstrapDialog.show({
                        title: "Message",
                        message: "Changed successfully !",
                    });
                    location.reload();
                }
            });
        });
    });

    function get_range_bids() {
        l = $('#startdate').val();
        console.log(l);
        var data_form = $("#export_form");
        if (data_form.valid() == false) {
            return;
        } else {
            // $("#history").html('<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>');
            // $("#history").attr('disabled', true);
            var url = '<?php echo base_url(); ?>admin/trading/export_bids';
            $.ajax({
                type: "POST",
                data: {
                    'startdate': $('#startdate').val(),
                    'enddate': $('#enddate').val(),
                },
                url: url,
                dataType: "json",
                success: function(response) {                    
                    window.location.reload(true);

                }

            });
        }
    }
</script>