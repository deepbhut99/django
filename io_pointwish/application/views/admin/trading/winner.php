<?php $this->load->view('admin/common/header'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php if (!empty($title)) echo $title; ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Users Management</a></li>
            <li class="active"><?php if (!empty($title)) echo $title; ?></li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-6">
                <div class="box border-radius-1">
                    <div class="box-body">
                        <h3 class="mt-0 mb-5">Daily Winner</h3>
                        <table id="dailywinner" class="display table table-hover table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Sr.NO</th>
                                    <th>Date</th>
                                    <th>Username</th>
                                    <th>Profit</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $count = 1;
                                foreach ($daily as $row) { ?>
                                    <tr>
                                        <td><?= $count++ ?></td>
                                        <td><?= date('d M, Y') ?></td>
                                        <td class="text-capitalize"><?= $row['username'] ?></td>
                                        <td>$ <?= number_format($row['totalprofit'], 2) ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box border-radius-1">
                    <div class="box-body">
                        <h3 class="mt-0 mb-5">Weekly Winner</h3>
                        <table id="weeklywinner" class="display table table-hover table-striped table-borderd">
                            <thead>
                                <tr>
                                    <th>Sr.NO</th>
                                    <th>Date</th>
                                    <th>Username</th>
                                    <th>Profit</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $count = 1;
                                foreach ($weekly as $row) { ?>
                                    <tr>
                                        <td><?= $count++ ?></td>
                                        <td><?= date('d M, Y', strtotime("saturday last week")) ?></td>
                                        <td class="text-capitalize"><?= $row['username'] ?></td>
                                        <td>$ <?= number_format($row['totalprofit'], 2) ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="box border-radius-1">
                    <div class="box-body">
                        <h3 class="mt-0 mb-5">Monthly Winner</h3>
                        <table id="monthlywinner" class="display table table-hover table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Sr.NO</th>
                                    <th>Date</th>
                                    <th>Username</th>
                                    <th>Profit</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $count = 1;
                                foreach ($monthly as $row) { ?>
                                    <tr>
                                        <td><?= $count++ ?></td>
                                        <td><?= date("d M, Y", strtotime("last day of last month")) ?></td>
                                        <td class="text-capitalize"><?= $row['username'] ?></td>
                                        <td>$ <?= number_format($row['totalprofit'], 2) ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box border-radius-1">
                    <div class="box-body">
                        <h3 class="mt-0 mb-5">Popular</h3>
                        <table id="popularwinner" class="display table table-hover table-striped table-borderd">
                            <thead>
                                <tr>
                                    <th>Sr.NO</th>
                                    <th>Overall Profit</th>
                                    <th>Username</th>
                                    <th>Profit</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $count = 1;
                                foreach ($populars as $row) { ?>
                                    <tr>
                                        <td><?= $count++ ?></td>
                                        <td>Overall Profit</td>
                                        <td class="text-capitalize"><?= $row['username'] ?></td>
                                        <td>$ <?= number_format($row['totalprofit'], 2) ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<?php $this->load->view('admin/common/footer'); ?>
<script>
    $(document).ready(function() {
        var table = $('#dailywinner').DataTable({
            paging: false
        });
        var table = $('#weeklywinner').DataTable({
            paging: false
        });
        var table = $('#monthlywinner').DataTable({
            paging: false
        });
        var table = $('#popularwinner').DataTable({
            paging: false
        });

        $("#trading_li").addClass("active menu-open");
        $("#trading_child_li").show();
        $("#winner_li").addClass("active");
    });
</script>