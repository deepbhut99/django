<?php $this->load->view('admin/common/header'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php if (!empty($title)) echo $title; ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">My Account</a></li>
      <li class="active"><?php if (!empty($title)) echo $title; ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box border-radius-1">
          <div class="box-header with-border">
            <h3 class="box-title">
              <?php if (!empty($title)) echo $title; ?>

            </h3>
            <a href="<?php echo base_url(); ?>admin/user/total_users" class="btn btn-black float-right">Back</a>
          </div>

          <!-- /.box-header -->
          <div class="box-body">
            <?php echo validation_errors(); ?>

            <form id="data_form" name="data_form" action="" method="POST" enctype="multipart/form-data">

              <?php
              if ($this->session->flashdata('error_message')) {
              ?>
                <div class="alert alert-success alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <?php echo $this->session->flashdata('error_message'); ?>
                </div>

              <?php
              }
              ?>

              <div class="form-body">

                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label">Full Name</label>
                      <input type="text" class="form-control required box-shadow-1" id="full_name" name="full_name" value="<?php if (!empty($user_data->full_name)) echo $user_data->full_name; ?>">
                    </div>
                  </div>
                  <!--/span-->

                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label">Username</label>
                      <input type="text" class="form-control required box-shadow-1" value="<?php if (!empty($user_data->full_name)) echo $user_data->full_name; ?>" readonly="">
                    </div>
                  </div>
                  <!--/span-->

                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label">Email</label>
                      <input type="text" name="email" class="form-control required box-shadow-1" value="<?php if (!empty($user_data->email)) echo $user_data->email; ?>">
                    </div>
                  </div>
                  <!--/span-->

                </div>
                <!--/row-->

                <div class="row">
                  <div class="col-md-3">
                    <div class="form-group">
                      <label class="control-label">Country</label>
                      <select class="form-control required box-shadow-1" id="country" name="country">
                        <option value="">Select</option>
                        <?php
                        $country_list = getCountryList();
                        if (!empty($country_list)) {
                          foreach ($country_list as $row) {
                        ?>
                            <option value="<?php echo $row->id; ?>" <?php if (!empty($user_data) && ($user_data->country == $row->id)) echo "selected"; ?>><?php echo $row->name; ?></option>
                        <?php
                          }
                        }
                        ?>
                      </select>

                    </div>
                  </div>
                  <!--/span-->

                  <div class="col-md-3">
                    <div class="form-group">
                      <label class="control-label">Code</label>

                      <select class="form-control required box-shadow-1" id="country_code" name="country_code">
                        <option value="">Select</option>
                        <?php
                        $country_list = getCountryList();
                        if (!empty($country_list)) {
                          foreach ($country_list as $row) {
                        ?>
                            <option value="<?php echo $row->phonecode; ?>" <?php if (!empty($user_data) && ($user_data->country_code == $row->phonecode)) echo "selected"; ?>><?php echo $row->phonecode; ?></option>
                        <?php
                          }
                        }
                        ?>
                      </select>

                    </div>
                  </div>

                  <div class="col-md-3">                  
                    <div class="form-group">
                      <label class="control-label">Mobile</label>
                      <input type="text" class="form-control required box-shadow-1" id="mobile" name="mobile" value="<?php if (!empty($user_data->mobile)) echo $user_data->mobile; ?>">
                    </div>
                  </div>

                  <div class="col-md-3">
                    <div class="form-group">
                      <label class="control-label">Address</label>
                      <input type="text" class="form-control box-shadow-1" id="address" name="address" value="<?php if (!empty($user_data->address)) echo $user_data->address; ?>">
                    </div>
                  </div>

                  <!--/span-->

                </div>

              </div>
              <div class="form-actions">
                <button type="submit" class="btn btn-black float-right"> Update</button>
                <!-- <a href="#" class="btn btn-inverse">Cancel</a> -->
              </div>
            </form>
          </div>
          <!-- /.box-body -->

        </div>
        <!-- /.box -->


      </div>
      <!-- /.col -->

    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php $this->load->view('admin/common/footer'); ?>

<script type="text/javascript">
  $("#data_form").validate();
</script>