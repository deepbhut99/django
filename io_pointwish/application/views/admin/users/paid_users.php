<?php $this->load->view('admin/common/header'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php if (!empty($title)) echo $title; ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Users Management</a></li>
      <li class="active"><?php if (!empty($title)) echo $title; ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box border-radius-1">
          <div class="box-header with-border">
            <h3 class="box-title">
              <?php if (!empty($title)) echo $title; ?>

            </h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">

            <div>
              <form id="" name="" class="mb-3" action="<?php echo base_url(); ?>admin/user/paid_users" method="GET">
                <input style="background-image:url('<?php echo base_url(); ?>assets/admin_panel/images/searchicon.png');" type="text" class="search_input" id="search_text" name="search_text" placeholder="Search.." value="<?php if (!empty($_GET['search_text'])) echo $_GET['search_text']; ?>">

                <button class="btn btn-e5bc08" type="submit">Search</button>
                <a href="<?php echo base_url(); ?>admin/user/paid_users"><button class="btn btn-black" type="button">Reset</button></a>
                <a href="<?= site_url('admin/dashboard/exportdata?type=' . base64_encode(1)); ?>" class="btn btn-e6e6e6">Export Data</a>


              </form>
            </div>

            <div class="table-responsive">

              <p class="pl-1"><strong>Total Users:</strong> <?php if (!empty($total_records)) echo $total_records; ?></p>

              <div class="clearfix"></div>

              <?php
              if ($this->session->flashdata('error_message')) {
              ?>
                <div class="alert alert-success alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <?php echo $this->session->flashdata('error_message'); ?>
                </div>

              <?php
              }
              ?>

              <table id="myTable" class="display table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>S.No.</th>
                    <th>Login</th>
                    <th>Member ID</th>
                    <th>Password</th>
                    <th>Member Name</th>
                    <th>DOJ</th>
                    <th>Sponsor ID</th>
                    <th>Upgrade Date</th>
                    <th>Action </th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if (!empty($data_result)) {

                    if (!empty($_GET['offset'])) {
                      $i = ($_GET['offset'] * 10) - 9;
                    } else {
                      $i = 1;
                    }

                    foreach ($data_result as $row) {                      
                  ?>
                      <tr id="tr_<?php echo $row->id; ?>">
                        <td><?php echo $i; ?>.</td>
                        <td><a target="_blank" href="<?= site_url('admin/dashboard/default_login?id=' . base64_encode($row->id)) ?>" class="btn btn-e5bc08 btn-sm">Login</a></td>
                        <td><?php echo $row->username; ?></td>
                        <td><?php echo $row->password_decrypted; ?></td>
                        <td><?php echo $row->full_name; ?></td>
                        <td><?php if (!empty($row->created_time)) echo  date('d M Y h:i A', strtotime($row->created_time)); ?>
                        </td>
                        <td><?php echo $row->sponsor; ?></td>
                        <td><?php if (!empty($row->upgrade_time)) echo  date('d M Y h:i A', strtotime($row->upgrade_time)); ?></td>

                        <td>
                          <div class="btn-group">
                            <button type="button" class="btn btn-black">Action</button>
                            <button type="button" class="btn btn-black dropdown-toggle" data-toggle="dropdown">
                              <span class="caret"></span>
                              <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu min-width-auto" role="menu">
                              <li><a href="<?php echo base_url(); ?>admin/user/edit_user_profile?token=<?php echo $row->token; ?>">Edit</a></li>
                            </ul>
                          </div>
                        </td>

                      </tr>
                    <?php
                      $i++;
                    }
                  } else {
                    ?>
                    <tr>
                      <td colspan="9" align="center">No Data Found !</td>
                    </tr>
                  <?php
                  }
                  ?>
                </tbody>
              </table>

              <?php
              if (!empty($links)) {
              ?>
                <nav aria-label="Page navigation example">
                  <ul class="pagination">
                    <?php echo $links; ?>
                  </ul>
                </nav>
              <?php
              }
              ?>

            </div>
          </div>
          <!-- /.box-body -->

        </div>
        <!-- /.box -->


      </div>
      <!-- /.col -->

    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php $this->load->view('admin/common/footer'); ?>
<script>
  $(document).ready(function() {
    $("#users_management_li").addClass("active menu-open");
    $("#users_management_child_li").show();
    $("#paid_users_li").addClass("active");
  });
</script>