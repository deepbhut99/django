<?php $this->load->view('admin/common/header'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php if (!empty($title)) echo $title; ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Users Management</a></li>
      <li class="active"><?php if (!empty($title)) echo $title; ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box border-radius-1">
          <div class="box-header with-border">
            <h3 class="box-title">
              <?php if (!empty($title)) echo $title; ?>
            </h3>
            <button class="btn btn-e5bc08 float-right" id="btnConvert">Convert USDT to pwt</button>
          </div>
          <!-- /.box-header -->
          <div class="box-body ">

            <div class="d-flex">
              <div class="mr-2">
                <form id="export_form" name="export_form" novalidate="" method="POST">                  
                    <div class="d-flex">
                      <input ng-model="startdate" id="startdate" type="text" class="form-control mr-2" placeholder="Start Date" readonly>
                      <input ng-model="enddate" id="enddate" type="text" class="form-control mr-2" placeholder="End Date" readonly>
                      <button type="button" ng-click="export_user(export_form)" id="history" class="btn btn-black"><i class="fa fa-search" aria-hidden="true"></i></button>
                    </div>                           
                </form>
              </div>
              <div>
                <form id="" name="" class="mb-3" action="<?php echo base_url(); ?>admin/user/total_users" method="GET">
                  <input style="background-image:url('<?php echo base_url(); ?>assets/admin_panel/images/searchicon.png');" type="text" class="search_input" id="search_text" name="search_text" placeholder="Search.." value="<?php if (!empty($_GET['search_text'])) echo $_GET['search_text']; ?>">

                  <button class="btn btn-e5bc08" type="submit">Search</button>
                  <a href="<?php echo base_url(); ?>admin/user/total_users"><button class="btn btn-black" type="button">Reset</button></a>
                </form>
              </div>
            </div>

            <div class="table-responsive">

              <p class="pl-1"><strong>Total Users:</strong> <?php if (!empty($total_records)) echo $total_records; ?></p>

              <div class="clearfix"></div>

              <?php
              if ($this->session->flashdata('error_message')) {
              ?>
                <div class="alert alert-success alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <?php echo $this->session->flashdata('error_message'); ?>
                </div>

              <?php
              }
              ?>

              <table id="example" class="display table table-hover table-striped" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>S.No.</th>
                    <th>Login</th>
                    <th>Member ID</th>
                    <th>Country</th>
                    <th>Password</th>
                    <th>Email</th>
                    <th>USDT Wallet</th>
                    <th>Locked pwt</th>
                    <th>DOJ</th>
                    <th>Sponsor ID</th>
                    <th>Status</th>
                    <th>Action </th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if (!empty($data_result)) {
                    $CI = &get_instance();
                    $obj = $CI->load->model('admin/Users_model', 'users');
                    if (!empty($_GET['offset'])) {
                      $i = ($_GET['offset'] * 10) - 9;
                    } else {
                      $i = 1;
                    }
                    $count = 1;
                    foreach ($data_result as $row) {
                      //  $cityName = $row->city!=0 ? $obj->users->getcityname($row->city) : '';


                  ?>
                      <tr id="tr_<?php echo $row->id; ?>">
                        <td><?= $count++; ?>.</td>
                        <td><a target="_blank" href="<?= site_url('admin/login/default_login?id=' . base64_encode($row->id)) ?>" class="btn btn-e5bc08 btn-sm">Login</a></td>
                        <td><?php echo $row->username; ?></td>
                        <td><?= ($row->country ? $row->country : '--') ?></td>
                        <td>
                          <?php echo $row->password_decrypted; ?>
                        </td>
                        <td><?php echo $row->email; ?></td>
                        <td><?= number_format($row->wallet_amount, 2) ?></td>
                        <td><?= number_format($row->pwt_wallet + $row->unlocked_pwt, 2) ?></td>
                        <td><?php if (!empty($row->created_time)) echo  date('d M Y h:i A', strtotime($row->created_time)); ?>
                        </td>
                        <td><?php echo $row->sponsor; ?></td>
                        <td>
                          <label class="switch-user">
                            <input type="checkbox" id="change_<?= $row->id ?>" onclick="change_status('<?= $row->id ?>')" <?= $row->is_delete == 1 ? 'checked' : '' ?>>
                            <span class="slider-user round-user"></span>
                          </label>
                        </td>

                        <td>
                          <div class="btn-group">
                            <button type="button" class="btn btn-black">Action</button>
                            <button type="button" class="btn btn-black dropdown-toggle" data-toggle="dropdown">
                              <span class="caret"></span>
                              <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu min-width-auto" role="menu">
                              <li><a href="<?php echo base_url(); ?>admin/user/edit_user_profile?token=<?php echo $row->token; ?>">Edit</a></li>
                            </ul>
                          </div>
                        </td>

                      </tr>
                    <?php
                      $i++;
                    }
                  } else {
                    ?>
                    <tr>
                      <td colspan="9" align="center">No Data Found !</td>
                    </tr>
                  <?php
                  }
                  ?>
                </tbody>
              </table>

              <?php
              if (!empty($links)) {
              ?>
                <nav aria-label="Page navigation example">
                  <ul class="pagination">
                    <?php echo $links; ?>
                  </ul>
                </nav>
              <?php
              }
              ?>

            </div>
          </div>
          <!-- /.box-body -->

        </div>
        <!-- /.box -->


      </div>
      <!-- /.col -->

    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php $this->load->view('admin/common/footer'); ?>
<script>
  $(document).ready(function() {
    $("#users_management_li").addClass("active menu-open");
    $("#users_management_child_li").show();
    $("#total_users_li").addClass("active");
    
    $('#btnConvert').on('click', () => {
      BootstrapDialog.show({
        title: "Confirm",
        message: "Do you really want to approve it ?",
        buttons: [{
            label: 'Yes',
            cssClass: 'btn-primary ',
            action: function(dialogItself) {

              var url = '<?php echo base_url(); ?>admin/user/convert_wallet';
              $.ajax({
                type: "post",
                url: url,                
                success: function(response) {
                  dialogItself.close();                  
                  BootstrapDialog.show({
                    title: "Message",
                    message: "Converted successfully !",
                  });
                  window.location.reload(true);
                }
              });

            }
          },
          {
            label: 'No',
            cssClass: 'btn-warning',
            action: function(dialogItself) {
              dialogItself.close();
            }
          }
        ]
      });
    })
  });

  function change_status(userid) {
    var status;
    var user = $('#change_' + userid);
    if (user.prop("checked") == true) {
      status = 1;
    } else {
      status = 0;
    }
    $.ajax({
      type: "post",
      url: "<?= base_url('admin/user/change_status') ?>",
      data: {
        status: status,
        id: userid
      },
      // dataType: "json",
      success: function(response) {
        BootstrapDialog.show({
          title: "Message",
          message: "Changed successfully !",
        });
        location.reload();
      }
    });
  }
</script>