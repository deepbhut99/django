<?php $this->load->view('admin/common/header'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php if(!empty($title)) echo $title; ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">User Management</a></li>
        <li class="active"><?php if(!empty($title)) echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">
              <?php if(!empty($title)) echo $title; ?>
              </h3>

            <?php
            if(!empty($_GET['b']) && ($_GET['b'] == 't'))
            {
            ?>
                <a href="<?php echo base_url(); ?>admin/user/total_users" class="btn btn-primary pull-right"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
            <?php
            }
            else if(!empty($_GET['b']) && ($_GET['b'] == 'p'))
            {
            ?>
                <a href="<?php echo base_url(); ?>admin/user/pending_users" class="btn btn-primary pull-right"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
            <?php
            }
            ?>

            </div>
            <!-- /.box-header -->
            <div class="box-body">

<?php
if($this->session->flashdata('error_message'))
{
?>
<div class="alert alert-success alert-dismissible">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <?php echo $this->session->flashdata('error_message'); ?>
  </div>

<?php
}
?>

              <table class="table table-hover">

                <tr>
                    <td style="width: 30%"><strong>Image:</strong></td>
                    <td>
                    <?php
                    if(!empty($user_data->profile_image))
                    {
                    ?>
                    <img src="<?php echo base_url(); ?>uploads/user_images/<?php echo $user_data->profile_image; ?>" height="200" width="200">
                    <?php
                    }
                    else
                    {
                        echo '--';
                    }
                    ?>
                    </td>
                </tr>

                <tr>
                    <td style="width: 30%"><strong>Login Type:</strong></td>
                    <td><?php if(!empty($user_data->oauth_provider)) echo ucfirst($user_data->oauth_provider); ?></td>
                </tr>

                <tr>
                    <td style="width: 30%"><strong>User Type:</strong></td>
                    <td><?php if(!empty($user_data->user_type)) echo getUserTypeNamebyID($user_data->user_type); ?></td>
                </tr>

                <tr>
                    <td style="width: 30%"><strong>First Name:</strong></td>
                    <td><?php if(!empty($user_data->first_name))  echo ucfirst($user_data->first_name); ?></td>
                </tr>

                <tr>
                    <td style="width: 30%"><strong>Last Name:</strong></td>
                    <td><?php if(!empty($user_data->last_name))  echo ucfirst($user_data->last_name); ?></td>
                </tr>

                <tr>
                    <td style="width: 30%"><strong>Last Login:</strong></td>
                    <td><?php if(!empty($user_data->last_login)) echo date('d F Y H:i A', strtotime($user_data->last_login)); ?></td>
                </tr>

                <tr>
                    <td style="width: 30%"><strong>Created Login:</strong></td>
                    <td><?php if(!empty($user_data->created_datetime)) echo date('d F Y H:i A', strtotime($user_data->created_datetime)); ?></td>
                </tr>

                <tr>
                    <td style="width: 30%"><strong>Modified Date:</strong></td>
                    <td><?php if(!empty($user_data->modified_datetime)) echo date('d F Y H:i A', strtotime($user_data->modified_datetime)); ?></td>
                </tr>


              </table>
            </div>
            <!-- /.box-body -->

          </div>
          <!-- /.box -->


        </div>
        <!-- /.col -->

      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('admin/common/footer'); ?>