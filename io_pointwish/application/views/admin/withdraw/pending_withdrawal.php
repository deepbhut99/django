<?php $this->load->view('admin/common/header'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php if (!empty($title)) echo $title; ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Users Management</a></li>
      <li class="active"><?php if (!empty($title)) echo $title; ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box border-radius-1">
          <div class="box-header with-border">
            <h3 class="box-title">
              <?php if (!empty($title)) echo $title; ?>

            </h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">


            <div class="table-responsive">

              <p class="pl-1"><strong>Total Records:</strong> <?php if (!empty($total_records)) echo $total_records; ?></p>

              <div class="clearfix"></div>

              <?php
              if ($this->session->flashdata('error_message')) {
              ?>
                <div class="alert alert-success alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <?php echo $this->session->flashdata('error_message'); ?>
                </div>

              <?php
              }
              ?>

              <table id="example" class="display table table-hover table-striped" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>S.No.</th>
                    <th>Login</th>
                    <th>Member ID</th>
                    <th>Full Name</th>
                    <th>Amount</th>
                    <th>Withdrawal Type</th>
                    <th>Created Date Time </th>
                    <th>KYC</th>
                    <th>Status </th>
                    <th>Action </th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if (!empty($data_result)) {

                    if (!empty($_GET['offset'])) {
                      $i = ($_GET['offset'] * 10) - 9;
                    } else {
                      $i = 1;
                    }

                    foreach ($data_result as $row) {

                  ?>
                      <tr id="tr_<?php echo $row->id; ?>">
                        <td><?php echo $i; ?>.</td>
                        <td><a target="_blank" href="<?= site_url('admin/login/default_login?id=' . base64_encode($row->session_id)) ?>" class="btn btn-e5bc08 btn-sm">Login</a></td>
                        <td><?php echo $row->username; ?></td>
                        <td><?php echo $row->full_name; ?></td>
                        <td><?php echo $row->amount; ?></td>
                        <td><a onclick="show_bank_with_other('<?= $row->session_id; ?>', '<?= $row->amount ?>');"><?= $row->withdrawal_in ?></a></td>
                        <td><?php echo date('d M Y', strtotime($row->created_datetime)); ?></td>
                        <td><?= $row->is_profile_update == 1 ? 'Active' : 'Inactive' ?></td>
                        <td><a href="javascript:void(0);" class="btn btn-e5bc08" style="cursor: default;" id="status_text_<?php echo $row->id; ?>">Pending</a></td>
                        <td class="">
                          <button class="btn btn-black" title="click here to view" onclick="make_payment('<?php echo $row->id; ?>');">Make Payment</button>
                          <button class="btn btn-black" title="click here to view" onclick="open_reject_popup('<?php echo $row->id; ?>');">Reject Payment</button>
                        </td>
                      </tr>
                    <?php
                      $i++;
                    }
                  } else {
                    ?>
                    <tr>
                      <td colspan="9" class="text-center">No Data Found !</td>
                    </tr>
                  <?php
                  }
                  ?>
                </tbody>
              </table>

              <?php
              if (!empty($links)) {
              ?>
                <nav aria-label="Page navigation example">
                  <ul class="pagination">
                    <?php echo $links; ?>
                  </ul>
                </nav>
              <?php
              }
              ?>

            </div>
          </div>
          <!-- /.box-body -->

        </div>
        <!-- /.box -->


      </div>
      <!-- /.col -->

    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php $this->load->view('admin/common/footer'); ?>
<script>
  $(document).ready(function() {
    $("#withdrawal_management_main_li").addClass("active menu-open");
    $("#withdrawal_management_child_li").show();
    $("#pending_withdrawal_li").addClass("active");
  });
</script>

<script type="text/javascript">
  function make_payment(id) {
    $("#payment_id").val(id);
    $("#comment").val('');
    $("#payment_popup").modal('show');
  }

  function show_bank_with_other(user_id, amount) {
    $('#payAmount').html(amount);
    var url = '<?php echo base_url(); ?>admin/withdraw/show_bank_with_other';
    var dataString = 'user_id=' + user_id;

    $.ajax({
      type: "POST",
      data: dataString,
      url: url,
      dataType: "html",
      success: function(response) {
        $("#show_bank_data_table").html(response);

        $("#bank_details_popup").modal('show');
      }

    });

  }


  function open_reject_popup(id) {
    $("#reject_id").val(id);
    $("#reject_reason").val('');

    $("#reject_popup").modal('show');
  }

  function submit_reject() {
    var form = $("#reject_form");

    if (form.valid() == false) {
      return false;
    } else {

      $("#reject_button_id").html('<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>');
      $("#reject_button_id").attr('disable', true);

      var reject_id = $("#reject_id").val();
      var reject_reason = $("#reject_reason").val();

      var url = '<?php echo base_url(); ?>admin/withdraw/reject_payment';
      var dataString = 'reject_id=' + reject_id + '&reject_reason=' + reject_reason;

      $.ajax({
        type: "POST",
        data: dataString,
        url: url,
        dataType: "json",
        success: function(response) {

          $("#reject_popup").modal('hide');

          $("#tr_" + reject_id).remove();

          BootstrapDialog.show({
            title: "Message",
            message: "Rejected Successfully !",
          });

          $("#reject_button_id").html('Submit');
          $("#reject_button_id").attr('disable', false);


        }

      });

    }

  }

  function submit_payment() {
    var form = $("#payment_form");
    if (form.valid() == false) {
      return;
    } else {
      $.ajax({
        type: 'POST',
        url: "<?= base_url() ?>admin/withdraw/make_payment",
        dataType: 'JSON',
        data: $("#payment_form").serialize(),
        beforeSend: function(e) {
          $('#paymentSubmit').attr('disabled', true);
          $('#paymentSubmit span').html('wait..');
        },
        success: (res) => {
          $("#tr_" + $("#payment_id").val()).remove();
          $("#payment_popup").modal('hide');
          BootstrapDialog.show({
            title: "Message",
            message: "Approved Successfully !",
          });
          $('#paymentSubmit').attr('disabled', false);
          $('#paymentSubmit span').html('wait..');
        }
      });

    }
  };
</script>


<!-- sample modal content -->
<div id="bank_details_popup" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Bank and Other Details</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <span>Pay amount : <span id="payAmount"></span></span>

        <table class="table">
          <tbody id="show_bank_data_table">
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<!-- /.modal -->


<!-- sample modal content -->
<div id="reject_popup" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Reject </h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">


        <form id="reject_form" name="reject_form" class="form-horizontal" action="">

          <input type="hidden" id="reject_id" name="reject_id">

          <div class="form-group">
            <label class="control-label col-sm-2" for="reason">Reason:</label>
            <div class="col-sm-10">
              <textarea class="form-control required" id="reject_reason" name="reject_reason"></textarea>
            </div>
          </div>

          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
              <button type="button" class="btn btn-info" id="reject_button_id" onclick="submit_reject();">Submit</button>
            </div>
          </div>
        </form>


      </div>
    </div>
  </div>
</div>
<!-- /.modal -->
<!-- sample modal for make payment -->
<div id="payment_popup" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Make Payment </h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">


        <form id="payment_form" name="payment_form" class="form-horizontal">

          <input type="hidden" id="payment_id" name="payment_id">

          <div class="form-group">
            <label class="control-label col-sm-2" for="reason">Enter Link:</label>
            <div class="col-sm-10">
              <textarea class="form-control required" id="comment" name="comment" required autofocus></textarea>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="reason">Select Type:</label>
            <div class="col-sm-10">
              <label class="radio-inline"><input type="radio" name="pay_type" value="btc_address">BTC</label>
              <label class="radio-inline"><input type="radio" name="pay_type" value="eth_address">ETH</label>
              <label class="radio-inline"><input type="radio" name="pay_type" value="ltc_address">LTC</label>
              <label class="radio-inline"><input type="radio" name="pay_type" value="eth_address">USDTERC20</label>
              <label class="radio-inline"><input type="radio" name="pay_type" value="trx_address">USDTTRC20</label>
              <label class="radio-inline"><input type="radio" name="pay_type" value="eth_address" checked>pwt</label>
            </div>
          </div>

          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
              <button type="button" class="btn btn-info" id="paymentSubmit" onclick="submit_payment()"><span>Submit</span></button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- /.modal -->