<?php $this->load->view('admin/common/header'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php if (!empty($title)) echo $title; ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Users Management</a></li>
      <li class="active"><?php if (!empty($title)) echo $title; ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box border-radius-1">
          <div class="box-header with-border">
            <h3 class="box-title">
              <?php if (!empty($title)) echo $title; ?>

            </h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">


            <div class="table-responsive">

              <p class="p-1"><strong>Total Records:</strong> <?php if (!empty($total_records)) echo $total_records; ?></p>

              <div class="clearfix"></div>

              <?php
              if ($this->session->flashdata('error_message')) {
              ?>
                <div class="alert alert-success alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <?php echo $this->session->flashdata('error_message'); ?>
                </div>

              <?php
              }
              ?>

              <table id="example" class="display table table-hover table-striped" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>S.No.</th>
                    <th>Member ID</th>
                    <th>Full Name</th>
                    <th>Admin Charges</th>
                    <th>Bank and Other Details</th>
                    <th>Amount</th>
                    <th>Withdrawal Type</th>
                    <th>Created Date Time </th>
                    <th>Reject Reason</th>
                    <th>Status </th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if (!empty($data_result)) {

                    if (!empty($_GET['offset'])) {
                      $i = ($_GET['offset'] * 10) - 9;
                    } else {
                      $i = 1;
                    }

                    foreach ($data_result as $row) {

                  ?>
                      <tr id="tr_<?php echo $row->id; ?>">
                        <td><?php echo $i; ?>.</td>
                        <td><?php echo $row->username; ?></td>
                        <td><?php echo $row->full_name; ?></td>

                        <td><?php echo $row->admin_charge; ?> %</td>

                        <td>
                          <button class="btn btn-e5bc08" title="click here to view" onclick="show_bank_with_other('<?php echo $row->session_id; ?>');">Show</button>
                        </td>

                        <td><?php echo $row->amount; ?> $</td>
                        <td><?= $row->withdrawal_in ?></td>
                        <td><?php echo date('d M Y', strtotime($row->created_datetime)); ?></td>

                        <td><?php echo $row->reject_reason; ?></td>

                        <td><a href="javascript:void(0);" class="btn btn-danger" style="cursor: default;" id="status_text_<?php echo $row->id; ?>">Rejected</a></td>

                      </tr>
                    <?php
                      $i++;
                    }
                  } else {
                    ?>
                    <tr>
                      <td colspan="9" class="text-center">No Data Found !</td>
                    </tr>
                  <?php
                  }
                  ?>
                </tbody>
              </table>

              <?php
              if (!empty($links)) {
              ?>
                <nav aria-label="Page navigation example">
                  <ul class="pagination">
                    <?php echo $links; ?>
                  </ul>
                </nav>
              <?php
              }
              ?>

            </div>
          </div>
          <!-- /.box-body -->

        </div>
        <!-- /.box -->


      </div>
      <!-- /.col -->

    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php $this->load->view('admin/common/footer'); ?>
<script>
  $(document).ready(function() {
    $("#withdrawal_management_main_li").addClass("active menu-open");
    $("#withdrawal_management_child_li").show();
    $("#rejected_withdrawal_li").addClass("active");
  });
</script>

<script type="text/javascript">
  function show_bank_with_other(user_id) {
    var url = '<?php echo base_url(); ?>admin/withdraw/show_bank_with_other';
    var dataString = 'user_id=' + user_id;

    $.ajax({
      type: "POST",
      data: dataString,
      url: url,
      dataType: "html",
      success: function(response) {
        $("#show_bank_data_table").html(response);

        $("#bank_details_popup").modal('show');
      }

    });

  }
</script>

<!-- sample modal content -->
<div id="bank_details_popup" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Bank and Other Details</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">


        <table class="table">
          <tbody id="show_bank_data_table">


          </tbody>
        </table>


      </div>
    </div>
  </div>
</div>
<!-- /.modal -->