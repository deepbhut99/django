<!-- Start Siter Footer -->
<footer class="tm-site-footer footer-bg" id="footer">
    <div class="empty-space col-md-b90 col-xs-b55"></div>
    <section class="tm-subscribe-wrap" id="tm-subscribe">
        <div class="empty-space col-md-b90 col-xs-b55"></div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="Box__BoxWrapper-sc-9qo6sy-0 box hHLgFu subscribe-box-bg">
                        <div class="rowstyle__RowWrapper-sc-1nreibv-0 ljhoKP row">
                            <div class="colstyle__ColWrapper-sc-1oqivd3-0 cSKItV col lg-6 offset-lg-3 xs-12">
                                <div class="sectionTitlestyle__SectionTitleWrapper-lzdnsw-0 fjPqRn title__wrapper">
                                    <h1 class="footer-box-head"> Don’t miss out, Stay updated </h1>
                                    <p class="footer-box-text">Sign up for updates and market news. Subscribe to our newsletter and receive update about products and crypto trading tips.</p>
                                </div>
                            </div>
                        </div>
                        <div class="rowstyle__RowWrapper-sc-1nreibv-0 ljhoKP row">
                            <div class="colstyle__ColWrapper-sc-1oqivd3-0 cSKItV col lg-8 offset-lg-2 xs-12 mrper-lg-2">
                                <div class="form-box">
                                    <form id="subscrib_data_form" autocomplete="off" method="POST" class="account-form">
                                        <div class="inputstyle__InputWrapper-x4q24z-0 eBaPLn input__wrapper">
                                            <input class="email-input" type="email" id="email1" name="email1" required placeholder="Enter your email">
                                        </div>
                                        <button class="buttonstyle__ButtonWrapper-vboqqy-0 khusAO btn" type="submit" id="submit1" name="submit1">Subscribe</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="empty-space col-md-b95 col-xs-b60"></div>
    </section>
    <div class="tm-top-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <h2 class="tm-footer-bar col-xs-b2 tm-light-blue-c col-xs-t-1 tm-md-f28 tm-md-lh34">Send a message</h2>
                    <div class="empty-space col-md-b55 col-xs-b30"></div>
                    <form id="data_form1" autocomplete="off" method="POST" class="account-form">
                        <div class="tm-form-field">
                            <input type="text" id="name2" name="name2" required>
                            <span class="bar"></span>
                            <label>Full Name</label>
                        </div>
                        <div class="tm-form-field">
                            <input type="email" id="email2" name="email2" required>
                            <span class="bar"></span>
                            <label>Email Address</label>
                        </div>
                        <div class="tm-form-field mb-3">
                            <textarea class="h-auto" cols="30" rows="6" id="msg" name="msg" required></textarea>
                            <span class="bar"></span>
                            <label>Your Message</label>
                        </div>
                        <button class="tm-btn mb-2" type="submit" id="submit" name="submit">
                            <span><i></i> Send Message</span>
                        </button>
                    </form>
                    <div id="tm-alert"></div>
                </div>
                <div class="col-lg-7">
                    <h2 class="tm-footer-bar col-xs-b2 tm-light-blue-c text-center col-xs-t-1 tm-md-f28 tm-md-lh34">Join us in the community</h2>
                    <div class="empty-space col-md-b55 col-xs-b40"></div>
                    <div class="tm-social-btn-wrap tm-style1 col-xs-b1">
                        <div class="tm-socil-bar-wrap">
                            <div class="tm-socil-bar bar1"></div>
                            <div class="tm-socil-bar bar2"></div>
                            <div class="tm-socil-bar bar3"></div>
                            <div class="tm-socil-bar bar4"></div>
                            <div class="tm-socil-bar bar5"></div>
                            <div class="tm-socil-bar bar6"></div>
                            <div class="tm-socil-bar bar7"></div>
                            <div class="tm-socil-bar bar8"></div>
                        </div>
                        <div class="tm-single-social-btn tm-active">
                            <a href="https://bitcointalk.org/index.php?topic=5382800.msg59062695#msg59062695" target="_blank" class="tm-email-btn"><span><i class="fa fa-btc"></i></span></a>
                            <div class="tm-social-btn-text">Bitcointalk</div>
                        </div>
                        <div class="tm-single-social-btn">
                            <a href="https://www.facebook.com/pointwishtoken/?ref=pages_you_manage" target="_blank" class="tm-social-btn tm-facebook"><span><i class="fa fa-facebook"></i></span></a>
                            <div class="tm-social-btn-text">Facebook</div>
                        </div>
                        <div class="tm-single-social-btn">
                            <a href="https://in.pinterest.com/pwtofficial/" target="_blank" class="tm-social-btn tm-linkedin"><span><i class="fa fa-pinterest"></i></span></a>
                            <div class="tm-social-btn-text">Pinterest</div>
                        </div>
                        <div class="tm-single-social-btn">
                            <a href="https://twitter.com/pwtofficial1" target="_blank" class="tm-social-btn tm-twitter"><span><i class="fa fa-twitter"></i></span></a>
                            <div class="tm-social-btn-text">Twitter</div>
                        </div>
                        <div class="tm-single-social-btn">
                            <a href="https://www.reddit.com/user/pwtofficial" target="_blank" class="tm-social-btn tm-reddit"><span><i class="fa fa-reddit-alien"></i></span></a>
                            <div class="tm-social-btn-text">Reddit</div>
                        </div>
                        <div class="tm-single-social-btn">
                            <a href="https://t.me/pointwishtoken" target="_blank" class="tm-social-btn tm-telegram"><span><i class="fa fa-telegram"></i></span></a>
                            <div class="tm-social-btn-text">Telegram</div>
                        </div>
                        <div class="tm-single-social-btn">
                            <a href="https://medium.com/@pwtofficial" target="_blank" class="tm-social-btn tm-medium"><span><i class="fa fa-medium"></i></span></a>
                            <div class="tm-social-btn-text">Medium</div>
                        </div>
                        <div class="tm-single-social-btn">
                            <a href="https://www.instagram.com/pwtofficial1/" target="_blank" class="tm-social-btn tm-github"><span><i class="fa fa-instagram"></i></span></a>
                            <div class="tm-social-btn-text">Instagram</div>
                        </div>
                        <div class="tm-single-social-btn">
                            <a href="https://www.youtube.com/channel/UCfbIPzb3IL7wgnBYI-6qLOA" target="_blank" class="tm-social-btn tm-youtube"><span><i class="fa fa-youtube-play"></i></span></a>
                            <div class="tm-social-btn-text">Youtube</div>
                        </div>
                    </div>
                    <div class="empty-space col-md-b60 col-xs-b60"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="empty-space col-md-b95 col-xs-b55"></div>
    <div class="tm-copyright tm-white-c tm-f20 tm-md-f15">
        <div class="container tm-f14">© 2022 POINTWISH | ALL RIGHTS RESERVED.</div>
    </div>
</footer>
<!-- End Siter Footer -->

<!-- The Modal -->
<div class="modal fade" id="tm-signup">
    <div class="modal-dialog">
        <div class="modal-content bg-transparent">
            <button type="button" class="close" data-dismiss="modal">x</button>
            <div class="tm-modal tm-color1" id="signup-details-modal">
                <h2 class="tm-modal-title">Sign up Here</h2>
                <form class="tm-modal-form" id="signUpForm" name="signUpForm" autocomplete="off" method="POST">
                    <?php
                    if ($this->session->flashdata('error_session')) {
                    ?>
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <?php echo $this->session->flashdata('error_session'); ?>
                        </div>
                    <?php } ?>

                    <?php
                    if ($this->session->flashdata('success_message')) {
                    ?>
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <span style="color: white;"><?php echo $this->session->flashdata('success_message'); ?></span>
                        </div>
                    <?php } ?>
                    <div class="intro-x mt-8">
                        <div class="tm-modal-input">
                            <input type="text" name="sponsor_id" id="sponsor_id" value="<?php if (!empty($_GET['user_id'])) echo $_GET['user_id']; ?>" autocomplete="off" placeholder="Sponsor ID">
                            <i class="fa fa-user-o"></i>
                        </div>
                        <span id="member_name_section" class="successSpan"></span>

                        <div class="tm-modal-input">
                            <input type="text" name="fullname" id="fullname" onblur="is_unique_user()" autocomplete="off" placeholder="Enter Username" required>
                            <i class="fa fa-user-o"></i>
                        </div>
                        <span id="error_full_name" class="errorSpan"></span>

                        <div class="tm-modal-input">
                            <input type="text" name="email" id="email" onblur="is_unique_email()" placeholder="Enter Email" autocomplete="off" required>
                            <i class="fa fa-envelope-o"></i>
                        </div>
                        <span id="error_email" class="errorSpan"></span>

                        <div class="tm-modal-input">
                            <input name="password" id="password" type="password" placeholder="Enter Your Password" autocomplete="off" required>
                            <i class="fa fa-lock"></i>
                        </div>
                        <span id="error_passwordUp" class="errorSpan"></span>

                        <div class="tm-modal-input">
                            <input type="password" name="confirm_password" id="confirm_password" placeholder="<?= lang('cnfm_pwd') ?>" autocomplete="off" equalTo="#password" required>
                            <i class="fa fa-lock"></i>
                        </div>
                        <span id="error_cpassword" class="errorSpan"></span>

                    </div>
                    <div class="empty-space col-md-b15 col-xs-b15"></div>
                    <button class="tm-btn tm-style1 w-100" type="submit" id="signUpSubmit" onclick="submit_data();"><span><b><i></i> Sign Up</b></span></button>
                    <h6 class="click-text" data-toggle="modal" data-target="#tm-login" data-dismiss="modal">CLICK HERE TO SIGNUP</h6>
                    <h4 class="text-center mt-3 font-weight-bold" id="register-success"></h4>
                </form>
            </div><!-- .tm-modal -->
        </div>
    </div>
</div>
<div class="modal fade" id="tm-login">
    <div class="modal-dialog">
        <div class="modal-content bg-transparent">
            <button type="button" class="close" data-dismiss="modal">x</button>
            <div class="tm-modal tm-color1">
                <h2 class="tm-modal-title">Welcome Back</h2>
                <form id="data_form" autocomplete="off" method="POST" class="tm-modal-form">
                    <?php
                    if ($this->session->flashdata('error_session')) {
                    ?>
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <?php echo $this->session->flashdata('error_session'); ?>
                        </div>
                    <?php } ?>

                    <?php
                    if ($this->session->flashdata('success_message')) {
                    ?>
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <span><?php echo $this->session->flashdata('success_message'); ?></span>
                        </div>
                    <?php } ?>
                    <span id="errorMessageOnline"></span>
                    <div class="tm-modal-input">
                        <input name="username" type="text" id="username" value="<?= $this->session->flashdata("username") ?>" placeholder="User Name">
                        <i class="fa fa-user-o"></i>
                    </div>
                    <span id="error_user" class="errorSpan text-danger"></span>

                    <div class="tm-modal-input">
                        <input placeholder="Enter Your Password" value="<?= $this->session->flashdata("password") ?>" autocomplete="new-password" type="password" id="lpassword" name="password">
                        <i class="fa fa-lock"></i>
                    </div>
                    <span id="error_pass" class="errorSpan text-danger"></span>

                    <div class="modal-ftr">
                        <input type="checkbox" class="input border mr-2" id="remember-me">
                        <label class="cursor-pointer select-none" for="remember-me">Remember me</label>
                        <span class="forgot_password float-right cursor-pointer" data-toggle="modal" data-dismiss="modal" data-target="#tm-forget-pwd">Forgot Password?</span>
                    </div>

                    <div class="empty-space col-md-b10 col-xs-b10"></div>
                    <button class="tm-btn tm-style1 w-100" id="login_btn_Online" type="submit"><span><i></i> LOG IN</span></button>
                    <h6 class="click-text" data-toggle="modal" data-target="#tm-signup" data-dismiss="modal">CLICK HERE TO SIGNUP</h6>
                </form>
            </div><!-- .tm-modal -->
        </div>
    </div>
</div>
<div class="modal fade" id="verification_model">
    <div class="modal-dialog">
        <div class="modal-content bg-transparent">
            <button type="button" class="close" data-dismiss="modal">x</button>
            <div class="tm-modal tm-color1">
                <h2 class="tm-modal-title">Verification</h2>
                <form class="tm-modal-form reset-password-form" id="authForm">
                    <div class="form-group">
                        <div class="tm-modal-input">
                            <i class="fa fa-key"></i>
                            <input class="otp-inpt" name="code" id="code" type="text" placeholder="o-o-o-o-o-o" data-inputmask="'mask': '9-9-9-9-9-9'" required />
                        </div>
                        <span id="error_user" class="errorSpan text-danger font-weight-bold"></span>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="tm-btn tm-style1 w-100" id="auth_btn_Online">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="tm-forget-pwd">
    <div class="modal-dialog">
        <div class="modal-content bg-transparent">
            <button type="button" class="close" data-dismiss="modal">x</button>
            <div class="tm-modal tm-color1">
                <h2 class="tm-modal-title">Reset Password</h2>
                <div id="reset-form">
                    <form id="form_forgot" name="form_forgot" action="#">
                        <div class="form-group" id="form-grp-femail">
                            <div class="tm-modal-input">
                                <input type="email" placeholder="<?= lang('enter_email') ?> " id="forgetemail" name="email" autocomplete="off" required>
                                <i class="fa fa-envelope-o"></i>
                            </div>
                            <span id="error_forgotU" class="errorSpan"></span>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <button class="tm-btn tm-style1 w-100" type="submit" id="forgot_btn_Online"><i></i> Send Reset Link</button>
                                <!-- <button style="background: #c99728; outline: none !important;" type="submit" id="forgot_btn_Online" class="mt-2 mb-2 text-white"><span><b><?= lang('snd_instruction') ?></b></span></button> -->
                            </div>
                        </div>
                    </form>
                </div>
                <div class="reset-confirmation d-none" id="reset-confirmation">
                    <h4 class="text-center">Link was sent</h4>
                    <div id="successForget" class="text-center">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Scroll Up -->
<div id='scrollup'><i class="fa fa-angle-double-up"></i></div>

<!-- Scripts -->
<script src="<?= base_url('assets/front/newthem/js/vendor/modernizr-3.5.0.min.js') ?>"></script>
<script src="<?= base_url('assets/front/newthem/js/vendor/jquery-1.12.4.min.js') ?>"></script>
<script src="<?= base_url('assets/front/newthem/js/bootstrap.min.js') ?>"></script>
<!-- <script src="<?= base_url('assets/front/newthem/js/smoothScroll.min.js') ?>"></script> -->
<script src="<?= base_url('assets/user_panel/libs/toastr/toastr.min.js') ?>"></script>
<script src="<?= base_url('assets/front/newthem/js/jQuery.easing.min.js') ?>"></script>
<script src="<?= base_url('assets/front/newthem/js/wow.min.js') ?>"></script>
<script src="<?= base_url('assets/front/newthem/js/particles.min.js') ?>"></script>
<script src="<?= base_url('assets/front/newthem/js/mailchimp.min.js') ?>"></script>
<script src="<?= base_url('assets/front/newthem/js/slick.min.js') ?>"></script>
<script src="<?= base_url('assets/front/newthem/js/modal.video.min.js') ?>"></script>
<script src="<?= base_url('assets/front/newthem/js/particles.min.js') ?>"></script>
<script src="<?= base_url('assets/front/newthem/js/main.js') ?>"></script>
<script src="<?= base_url('assets/user_panel/js/jquery.validate.js') ?>"></script>
<script src="<?= base_url('assets/front/newthem/js/common.js?v=' . rand()) ?>"></script>
<script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/3/jquery.inputmask.bundle.js"></script>
</body>

<script>
    $("#code").inputmask();

    if (window.location.href.indexOf("user_id") > -1) {
        $(window).on('load', function() {
            $('#tm-signup').modal('show');
        });
    }
    if (window.location.href.indexOf("resetsuccess") > -1) {
        $(window).on('load', function() {
            $("#tm-login").modal('show');
        });
    }
    
</script>

<script>
    $(window).load(function() {
        $('#myModal').modal('show');
    });

    const daysEl = document.getElementById('days');
    const hoursEl = document.getElementById('hours');
    const minsEL = document.getElementById('mins');
    const secondsEL = document.getElementById('seconds');

    const newYears = '03 Feb 2022 15:39';

    function countdown() {
        const newYearsDate = new Date(newYears);
        const currentDate = new Date();

        const totalSeconds = (newYearsDate - currentDate) / 1000;
        const minutes = Math.floor(totalSeconds / 60) % 60;
        const hours = Math.floor(totalSeconds / 3600) % 24;
        const days = Math.floor(totalSeconds / 3600 / 24);
        const seconds = Math.floor(totalSeconds) % 60;


        if (days < 10) {
            daysEl.innerText = "0" + days;
        } else {
            daysEl.innerText = days;
        }
        if (hours < 10) {
            hoursEl.innerText = "0" + hours;
        } else {
            hoursEl.innerText = hours;
        }
        if (minutes < 10) {
            minsEL.innerText = "0" + minutes;
        } else {
            minsEL.innerText = minutes;
        }
        if (seconds < 10) {
            secondsEL.innerText = "0" + seconds;
        } else {
            secondsEL.innerText = seconds;
        }
    }
    setInterval(countdown, 1000);
</script>

<script>
    var errorMsg = '',
        $valid = false;
    $.validator.addMethod("is_sponser_exist", function(val, elem) {
        var len = $("#sponsor_id").val();
        if (len.length >= 5) {
            $.ajax({
                async: false,
                url: '<?php echo base_url(); ?>login/is_sponser_exist',
                type: "get",
                dataType: "json",
                data: {
                    sponsor_id: $("#sponsor_id").val()
                },
                success: function(response) {
                    if (response.status == 1) {
                        errorMsg = 'Sponsor Available !';
                        $valid = true;
                        $("#member_name_section").html('User Name: ' + response.member_name);
                        $("#member_name_section").addClass("text-success");
                        $("#member_name_section").removeClass("text-danger");

                    } else if (response.status == 2) {
                        errorMsg = 'User not exist !';
                        $valid = false;
                        $("#member_name_section").html(errorMsg);
                        $("#sponsor_id").addClass("error");
                        $("#member_name_section").addClass("text-danger");
                    }
                }
            });
        }

        $.validator.messages["is_sponser_exist"] = errorMsg;
        return $valid;
    }, '');

    function is_unique_user() {
        if (/^[a-zA-Z0-9](?!.*?[^\na-zA-Z0-9]{2}).*?[a-zA-Z0-9]$/.test($("#fullname").val())) {
            var len = $("#fullname").val();
            if (len.length > 9) {
                $isUser = false;
                $("#error_full_name").removeClass("text-success");
                $("#fullname").addClass("error");
                $("#error_full_name").text('Username should be maximum 9 letter.');
                $("#error_full_name").addClass("error");
            } else if (len.length >= 5) {
                $.ajax({
                    async: false,
                    url: '<?php echo base_url(); ?>login/is_unique_user',
                    type: "get",
                    dataType: "json",
                    data: {
                        username: $("#fullname").val()
                    },
                    success: function(response) {
                        if (response.status == 1) {
                            $isUser = false;
                            $("#error_full_name").removeClass("text-success");
                            $("#fullname").addClass("error");
                            $("#error_full_name").text('Already Exist');
                            $("#error_full_name").addClass("error");


                        } else if (response.status == 2) {
                            $isUser = true;
                            $("#error_full_name").text('Proceed with name');
                            $("#error_full_name").removeClass("error");
                            $("#error_full_name").addClass("text-success");
                            // $("#newusername").removeClass("error");
                        }
                    }
                });
            } else {
                $isUser = false;
                $("#error_full_name").removeClass("text-success");
                $("#fullname").addClass("error");
                $("#error_full_name").text('Username should be minimum 5 letter.');
                $("#error_full_name").addClass("error");
            }
        } else {
            $isUser = false;
            $("#error_full_name").removeClass("text-success");
            $("#fullname").addClass("error");
            $("#error_full_name").text('Special chracter not allowed.');
            $("#error_full_name").addClass("error");
        }
    }

    function is_unique_email() {
        var len = $("#email").val();
        if (len.length >= 10) {
            $.ajax({
                async: false,
                url: '<?php echo base_url(); ?>login/is_email_exist',
                type: "post",
                dataType: "json",
                data: {
                    email: $("#email").val()
                },
                success: function(response) {
                    if (response.status == 2) {
                        isEmail = false;
                        $("#error_email").removeClass("text-success");
                        $("#email").addClass("error");
                        $("#error_email").text('Already Exist');
                        $("#error_email").addClass("error");
                    } else if (response.status == 1) {
                        isEmail = true;
                        $("#error_email").text('Proceed with mail');
                        $("#error_email").removeClass("error");
                        $("#error_email").addClass("text-success");
                        // $("#newusername").removeClass("error");
                    }
                }
            });
        } else {
            isEmail = false;
            $("#error_email").removeClass("text-success");
            $("#email").addClass("error");
            $("#error_email").text('Email should be minimum 10 letter.');
            $("#error_email").addClass("error");
        }
    }

    $("#signUpForm").validate({
        rules: {
            sponsor_id: {
                is_sponser_exist: true,
            },
            fullname: {
                required: true,
                minlength: 5,
                maxlength: 9
            },
            email: {
                required: true,
                minlength: 10
            },
            password: {
                minlength: 6,
            },
        },
        errorPlacement: function(error, element) {
            element.parents('div.input-group').after(error);
        }
    });

    function submit_data() {
        var form = $("#signUpForm");
        if (form.valid() == false) {
            return false;
        } else {
            $(".submit_button_class").attr('disabled', true);
            // $("#signUpForm").submit();
        }
    }
</script>
<script>
    $(document).ready(function() {
        $("#data_form1").submit(function(e) {
            e.preventDefault();
            var n = $("#name2").val(),
                e = $("#email2").val(),
                m = $("#msg").val(),
                a = 0;
            $.ajax({
                url: "home/contact_us",
                type: "POST",
                data: $("#data_form1").serialize(),
                beforeSend: function() {
                    $("#submit span i").addClass("fa fa-refresh fa-spin fa-fw")
                },
                success: function(e) {

                    e = JSON.parse(e);
                    if (e.success = '1') {
                        $("#submit span i").removeClass("fa fa-refresh fa-spin fa-fw");
                        $("#data_form1").trigger('reset');
                        toastr.success('Success');
                    }

                }
            })
        });
        $("#subscrib_data_form").submit(function(e) {
            e.preventDefault();
            var e = $("#email1").val(),
                a = 0;
            $.ajax({
                url: "home/contact_us_subscribe",
                type: "POST",
                data: $("#subscrib_data_form").serialize(),
                beforeSend: function() {
                    $("#submit1 span i").addClass("fa fa-refresh fa-spin fa-fw")
                },
                success: function(e) {

                    e = JSON.parse(e);
                    if (e.success = '1') {
                        $("#submit1 span i").removeClass("fa fa-refresh fa-spin fa-fw");
                        $("#subscrib_data_form").trigger('reset');
                        toastr.success('Success');
                    }

                }
            })
        });
    });
</script>
<!-- Mirrored from thememarch.com/demo/html/ocher/home3(purple).html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 14 Jan 2022 07:15:25 GMT -->

</html>