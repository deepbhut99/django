<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?= base_url('assets/front/newthem/img/logo/pointwish.png') ?> ">

    <title>Point Wish</title>

    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/front/newthem/css/plugins.css') ?>">
    <link href="<?= base_url('assets/user_panel/libs/toastr/toastr.min.css') ?>" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?= base_url('assets/front/newthem/css/bootstrap.min.css') ?> ">
    <link rel="stylesheet" href="<?= base_url('assets/front/newthem/css/animate.min.css') ?> ">
    <link rel="stylesheet" href="<?= base_url('assets/front/newthem/css/icofont.css') ?> ">
    <link rel="stylesheet" href="<?= base_url('assets/front/newthem/css/flaticon.css') ?> ">
    <link rel="stylesheet" href="<?= base_url('assets/front/newthem/css/fontawesome.min.css') ?> ">
    <link rel="stylesheet" href="<?= base_url('assets/front/newthem/css/slick.min.css') ?> ">
    <link rel="stylesheet" href="<?= base_url('assets/front/newthem/css/modal.video.min.css') ?> ">
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/front/newthem/css/style.css?v= ' . rand()) ?> ">

    <script>
        var BASE_URL = "<?php echo base_url(); ?>";
    </script>

</head>

<body data-spy="scroll" data-target=".tm-section-active" class="tm-purple-color">
    <!-- Start Preloader -->
    <div id="tm-preloader">
        <div class="tm-preloader-in">
            <div class="bar bar-top"></div>
            <div class="bar bar-right"></div>
            <div class="bar bar-bottom"></div>
            <div class="bar bar-left"></div>
        </div>
    </div>
    <!-- End Preloader -->
    <!-- Start Site Header Wrap -->
    <header class="tm-site-header">
        <div class="tm-header-menu">
            <div class="container tm-header-menu-container">
                <div class="tm-site-branding">
                    <!-- For Image Logo -->
                    <a href="<?= base_url('home') ?>" class="tm-logo-link">
                        <img style="height: 60px;" src="<?= base_url('assets/front/newthem/img/logo/logo-web.png?v=' . rand()) ?>" alt="Pointwish" class="tm-logo">
                        <img src="<?= base_url('assets/front/newthem/img/logo/logo-mobile.png') ?>" alt="Pointwish" class="tm-mobile-logo">
                    </a>
                    <!-- For Site Title -->
                </div>
                <nav class="tm-primary-nav tm-section-active">
                    <ul class="tm-primary-nav-list">
                        <li class="menu-item"><a href="#about" class="nav-link">About</a></li>
                        <li class="menu-item"><a href="#howitworks" class="nav-link">How It Works</a></li>
                        <li class="menu-item"><a href="#tokensale" class="nav-link">Token Sale</a></li>
                        <li class="menu-item"><a href="#bounty" class="nav-link">Bounty</a></li>
                        <li class="menu-item"><a href="#roadmap" class="nav-link">Roadmap</a></li>
                        <li class="menu-item"><a href="#team" class="nav-link">Team</a></li>
                        <li class="menu-item"><a href="#faq" class="nav-link">FAQ</a></li>
                    </ul>
                    <!-- <div class="tm-language">
                        <div class="tm-active-language">EN</div>
                        <div class="tm-lenguage-dropdown">
                            <a href="#">FR</a>
                            <a href="#">CH</a>
                            <a href="#">HI</a>
                        </div>
                    </div> -->
                    <div class="tm-menu-meta">
                        <?php if ($this->session->userdata('user_id')) { ?>
                            <a href="<?= base_url('affiliate') ?>" class="tm-btn tm-style1 tm-with-border">Dashboard</a>
                        <?php } else { ?>
                            <a class="tm-btn tm-style1 tm-with-border" data-toggle="modal" data-target="#tm-login">LOGIN</a>
                        <?php } ?>
                        <button class="tm-btn tm-style1 tm-with-border" data-toggle="modal" data-target="#tm-signup">SIGNUP</button>
                    </div>
                </nav>
            </div><!-- .tm-header-menu-container -->
        </div><!-- .tm-header-menu -->
    </header><!-- .tm-site-header -->
    <!-- End Site Header Wrap -->
    <div class="tm-section-active tm-fix-bar">
        <ul>
            <li><a href="#home" class="nav-link"></a></li>
            <li><a href="#about" class="nav-link"></a></li>
            <li><a href="#howitworks" class="nav-link"></a></li>
            <li><a href="#tokensale" class="nav-link"></a></li>
            <li><a href="#bounty" class="nav-link"></a></li>
            <li><a href="#roadmap" class="nav-link"></a></li>
            <li><a href="#team" class="nav-link"></a></li>
            <li><a href="#featured-company" class="nav-link"></a></li>
            <li><a href="#faq" class="nav-link"></a></li>
            <li><a href="#tm-subscribe" class="nav-link"></a></li>
        </ul>
    </div>