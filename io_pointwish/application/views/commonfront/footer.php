<div section-scroll='7' class="wd_scroll_wrap">
    <!--Footer area start here-->
    <footer class="top-foo">
        <div id="footer-js">
            <div class="footer-top section">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="con">
                                <h2 class="wow fadeInUp animated" data-wow-duration="1.0s" style="visibility: visible; animation-duration: 1.0s; animation-name: fadeInUp;">Subscribe to us!</h2>
                                <ul>
                                    <!-- <li class="wow fadeInUp animated animated-icon" data-wow-duration="1.3s"><a class="footer-icons" href="#"><i class="far fa-newspaper"></i></a></li> -->
                                    <li class="wow fadeInUp animated animated-icon" data-wow-duration="1.5s"><a class="footer-icons" href="https://twitter.com/pwtio" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                    <li class="wow fadeInUp animated animated-icon" data-wow-duration="1.9s"><a class="footer-icons" href="https://t.me/pwtio" target="_blank"><i class="fab fa-telegram-plane"></i></a></li>
                                    <li class="wow fadeInUp animated animated-icon" data-wow-duration="1.3s"><a class="footer-icons" href="https://www.facebook.com/pwtio" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                    <li class="wow fadeInUp animated animated-icon" data-wow-duration="1.3s"><a class="footer-icons" href="https://www.reddit.com/user/pwtio" target="_blank"><i class="fab fa-reddit-alien"></i></a></li>
                                    <!-- <li class="wow fadeInUp animated animated-icon" data-wow-duration="2.1s"><a class="footer-icons" href="#"><i class="fab fa-btc"></i></a></li> -->
                                    <li class="wow fadeInUp animated animated-icon" data-wow-duration="2.1s"><a class="footer-icons" href="https://pwtio.medium.com/" target="_blank"><i class="fab fa-medium-m"></i></a></li>
                                    <li class="wow fadeInUp animated animated-icon" data-wow-duration="2.1s"><a class="footer-icons" href="https://www.instagram.com/pwtio/" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                    <li class="wow fadeInUp animated animated-icon" data-wow-duration="2.1s"><a class="footer-icons" href="https://in.pinterest.com/pwtio/" target="_blank"><i class="fab fa-pinterest-p"></i></a></li>
                                    <li class="wow fadeInUp animated animated-icon" data-wow-duration="2.1s"><a class="footer-icons" href="https://www.youtube.com/channel/UCxNBlIwB4snBH4aRJHDMkOg" target="_blank"><i class="fab fa-youtube"></i></a></li>
                                    <li class="wow fadeInUp animated animated-icon" data-wow-duration="2.1s"><a class="footer-icons" href="https://github.com/pwtio" target="_blank"><i class="fab fa-github"></i></a></li>
                                    <!-- <li class="wow fadeInUp animated animated-icon" data-wow-duration="2.1s"><a class="footer-icons" href="#"><i class="fab fa-vimeo-v"></i></a></li>
                                        <li class="wow fadeInUp animated animated-icon" data-wow-duration="2.4s"><a class="footer-icons" href="#"><i class="fab fa-discord"></i></a></li> -->
                                </ul>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="search_btm wow fadeInUp animated" data-wow-duration="2.6s" style="visibility: visible; animation-duration: 2.6s; animation-name: fadeInUp;">
                                            <div class="search_main">
                                                <input type="text" placeholder="enter your email address">
                                                <button type="submit">subscribe</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <footer class="foo-bot">
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 pt-sm-10px">
                        <marquee behavior="" direction="">Trading and investing involves significant level of risk and is not suitable and/or appropriate for all clients. Please make sure you carefully consider your investment objectives, level of experience and risk appetite before buying or selling. Buying or selling entails financial risks and could result in a partial or complete loss of your funds, therefore, you should not invest funds you cannot afford to lose. You should be aware of and fully understand all the risks associated with trading and investing, and seek advice from an independent financial advisor if you have any doubts. You are granted limited non-exclusive rights to use the IP contained in this site for personal, non-commercial, non-transferable use only in relation to the services offered on the site.</marquee>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 pt-sm-20px">
                        <div class="copyright wow  fadeInUp animated" data-wow-duration="1.9s" style="visibility: visible; animation-duration: 1.0s; animation-name: fadeInUp;">
                            <p>© 2021-2022 <a href="#"><span>XEN BOT</span></a> | All rights reserved.
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 pt-sm-20px pb-xsm-15px">
                        <div class="foo-link wow  fadeInUp animated" data-wow-duration="1.0s" style="visibility: visible; animation-duration: 1.0s; animation-name: fadeInUp;">
                            <ul>
                                <!-- <li><a href="#">WhitePaper</a></li> -->
                                <li><a href="<?= base_url('terms-condition')?>">Terms & Conditions</a></li>
                                <li><a href="<?= base_url('privacy-policy')?>">Privacy Policy</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
<!--Footer area end here-->
<!-- all js here -->
<!-- jquery latest version -->
<script src="<?= base_url('assets/frontforpwt/js/vendor/jquery-3.2.1.min.js') ?>"></script>
<!-- tether js -->
<script src="<?= base_url('assets/frontforpwt/js/tether.min.js') ?>"></script>
<!-- bootstrap js -->
<script src="<?= base_url('assets/frontforpwt/js/bootstrap.min.js') ?>"></script>
<!-- owl.carousel js -->
<script src="<?= base_url('assets/frontforpwt/js/owl.carousel.min.js') ?>"></script>
<!-- meanmenu js -->
<script src="<?= base_url('assets/frontforpwt/js/jquery.meanmenu.js') ?>"></script>
<!-- jquery-ui js -->
<script src="<?= base_url('assets/frontforpwt/js/jquery-ui.min.js') ?>"></script>
<!-- easypiechart js -->
<script src="<?= base_url('assets/frontforpwt/js/jquery.easypiechart.min.js') ?>"></script>
<!-- particles js -->
<!-- wow js -->
<script src="<?= base_url('assets/frontforpwt/js/wow.min.js') ?>"></script>
<!-- smooth-scroll js -->
<script src="<?= base_url('assets/frontforpwt/js/smooth-scroll.min.js') ?>"></script>
<!-- plugins js -->
<script src="<?= base_url('assets/frontforpwt/js/plugins.js') ?>"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>

<!-- EChartJS JavaScript -->
<script src="<?= base_url('assets/frontforpwt/js/echarts-en.min.js') ?>"></script>
<script src="<?= base_url('assets/frontforpwt/js/echarts-liquidfill.min.js') ?>"></script>
<script src="<?= base_url('assets/frontforpwt/js/vc_round_chart.min.js') ?>"></script>
<script src="<?= base_url('assets/frontforpwt/js/videojs-ie8.min.js') ?>"></script>
<script src="<?= base_url('assets/frontforpwt/js/video.js') ?>"></script>
<script src="<?= base_url('assets/frontforpwt/js/Youtube.min.js') ?>"></script>
<!-- main js -->
<script src="<?= base_url('assets/frontforpwt/js/main.js?v=' . rand()) ?>"></script>



<div class="modal fade" id="featureModal">
    <div class="modal-dialog center-modal">
        <div class="modal-content">

            <!-- Modal body -->
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 pr-s-0">
                        <iframe class="modal-youtube" src="https://www.youtube.com/embed/tgbNymZ7vqY"></iframe>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 hide-xs">
                        <p>sdjkfgjkchbkk hadfgidagoaidgoiaduoigo iadgoidfgo adgodfgodifhbiudhiudfh</p>
                        <p>adjklfhgjkdfbadf'gbml dfhdfkjdjfgjkdsg kjhdfkjah qksgjk sgkq dfklgjqdf</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>

</html>