<?php
$session_data = getSessionData();
?>
<!doctype html>
<html class="no-js" lang="en">


<!-- Mirrored from www.webstrot.com/html/bitmoney/landing_page/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 25 Oct 2021 05:54:16 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<!-- /Added by HTTrack -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Analytical Bot Trading Platform</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="../../../../webstrot.com/html/bitmoney/landing_page/apple-touch-icon.html">
    <!-- Place favicon.ico in the root directory -->
    <link rel="shortcut icon" href="<?= base_url('assets/frontforpwt/images/icons/faviconn.svg') ?>">


    <!-- all css here -->
    <!-- bootstrap v3.3.6 css -->
    <link rel="stylesheet" href="<?= base_url('assets/frontforpwt/css/bootstrap.min.css') ?>">
    <!-- animate css -->
    <link rel="stylesheet" href="<?= base_url('assets/frontforpwt/css/animate.css') ?>">
    <!-- jquery-ui.min css -->
    <link rel="stylesheet" href="<?= base_url('assets/frontforpwt/css/jquery-ui.min.css') ?>">
    <!-- meanmenu css -->
    <link rel="stylesheet" href="<?= base_url('assets/frontforpwt/css/meanmenu.min.css') ?>">
    <!-- owl.carousel css -->
    <link rel="stylesheet" href="<?= base_url('assets/frontforpwt/css/owl.carousel.min.css') ?>">
    <!-- bxslider css -->
    <!--flaticon css -->
    <link rel="stylesheet" href="<?= base_url('assets/frontforpwt/css/flaticon.css') ?>">
    <!-- font-awesome css -->
    <link rel="stylesheet" href="<?= base_url('assets/frontforpwt/css/font-awesome.min.css') ?>">
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> -->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css">
    <link href="<?= base_url('assets/frontforpwt/css/video-js.css') ?>" rel="stylesheet">
    <!-- style css -->
    <link rel="stylesheet" href="<?= base_url('assets/frontforpwt/style.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/frontforpwt/app.css?v=' . rand()) ?>">
    <!-- responsive css -->
    <link rel="stylesheet" href="<?= base_url('assets/frontforpwt/css/responsive.css') ?>">
    <!-- modernizr css -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">

    <script src="<?= base_url('assets/frontforpwt/js/vendor/modernizr-2.8.3.min.js') ?>"></script>
    <!-- <script src='../../../google_analytics_auto.js'></script> -->




    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
    <script>
        window.OneSignal = window.OneSignal || [];
        OneSignal.push(function() {
            OneSignal.init({
                appId: "2c5c4c22-03b4-48d3-aad9-54e0ba5dcd83",
            });
        });
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-C64PGT75XG"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'G-C64PGT75XG');
    </script>



</head>

<body>
    <a href="javascript:" id="return-to-top"><i class="fa fa-angle-up"></i></a>
    <!-- preloader Start -->
    <div id="preloader">
        <div id="status"><img src="<?= base_url('assets/frontforpwt/images/banner/loader.gif') ?>" id="preloader_image" alt="loader">
        </div>
    </div>
    <!--Header area start here-->
    <div section-scroll='0' class="wd_scroll_wrap">
        <header class="gc_main_menu_wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-6">
                        <div class="logo-area">
                            <a href="<?= base_url('home')?>"><img class="logo-head" src="<?= base_url('assets/frontforpwt/images/logo/logo.png') ?>" alt="logo" /></a>
                        </div>
                    </div>
                    <!-- Mobile Menu  Start -->
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-6">
                        <div class="menu-area hidden-xs">
                            <nav class="wd_single_index_menu btc_main_menu">
                                <ul>
                                    <li><a href="#home">Home</a></li>
                                    <li><a href="#about">About</a></li>
                                    <li><a href="#features">Features</a></li>
                                    <li><a href="#project">Project</a></li>
                                    <li><a href="#steps">Steps</a></li>
                                    <li><a href="#roadmap">Road Map</a></li>
                                    <li><a href="#token">Token</a></li>
                                </ul>
                            </nav>
                            <!-- <div class="language">
                                <span class="lng-in"><img src="<?= base_url('assets/frontforpwt/images/icons/12.png') ?>" alt="" /></span>
                                <ul class="lng-out">
                                    <li><img src="<?= base_url('assets/frontforpwt/images/icons/12.png') ?>" alt="" /></li>
                                    <li><img src="<?= base_url('assets/frontforpwt/images/icons/12.png') ?>" alt="" /></li>
                                </ul>
                            </div> -->

                            <div class="login-btn">
                                <?php if ($this->session->userdata('user_id')) { ?>
                                    <a href="<?= base_url('affiliate'); ?>" class="btn1"><i class="fa fa-user"></i><span><?= lang('dashboard') ?></span></a>

                                <?php } else { ?>
                                    <a href="<?= base_url('login') ?>" class="btn1"><i class="fa fa-user"></i><span>Login</span></a>

                                <?php } ?>

                            </div>
                        </div>
                        <!-- mobile menu area start -->
                        <!-- Mobile Menu  End -->
                    </div>
                    <div class="rp_mobail_menu_main_wrapper visible-xs">
                        <div id="togglemenu" onclick="togglemenu()">
                            <i class="menubar fas fa-bars"></i>
                        </div>
                    </div>
                    <div class="visible-xs m-sidebar-parent">
                        <div class="mob-sidebar">
                            <div id="myLinks">
                                <ul class="navigation">
                                    <li><a href="#home">Home</a></li>
                                    <li><a href="#about">About</a></li>
                                    <li><a href="#features">Features</a></li>
                                    <li><a href="#project">Project</a></li>
                                    <li><a href="#steps">Steps</a></li>
                                    <li><a href="#roadmap">Road Map</a></li>
                                    <li><a href="#token">Token</a></li>
                                    <li><a href="<?= base_url('login') ?>" class="btn1" data-animation="animated bounceInUp">Sign In</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!--Header area end here-->

        <script>
            function togglemenu() {
                $("#myLinks").slideToggle(500);
            }
        </script>