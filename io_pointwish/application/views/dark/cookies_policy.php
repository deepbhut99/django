<?php $this->load->view('common/header_dark');  ?>

<!--============= Header Section Ends Here =============-->
<section class="page-header bg_img oh" data-background="<?= base_url('assets/front/dark/images/page-header.png') ?>">
    <div class="bottom-shape d-none d-md-block">
        <img src="<?= base_url('assets/front/dark/css/img/page-header.png') ?>" alt="css">
    </div>
    <div class="page-left-thumb">
        <!-- <img src="<?= base_url('assets/front/images/bg/privacy-header.png') ?>" alt="bg"> -->
    </div>
    <div class="container">
        <div class="page-header-content cl-white">
            <h1 class="title">Cookies Policy</h1>
        </div>
    </div>
</section>
<!--============= Header Section Ends Here =============-->



<!--============= Privacy Section Starts Here =============-->
<section class="privacy-section padding-top padding-bottom">
    <div class="container">
        <div class="row justify-content-center">            
        </div>
<?php $this->load->view('common/sidebar_dark');  ?>
            <div class="col-lg-9 col-xl-9 col-md-8">
                <form class="hdmob">
                    <select class="fa form-control " onchange="window.location='<?php echo base_url(); ?>page/rules/'+this.value" style="background-color: #ebecec; color: black; font-size: 16px; border: 1px solid rgba(255, 255, 255, 0.3); border-radius: 1.25rem; height: calc(2.25rem + 20px); font-family: 'Poppins', sans-serif; ">

                        <!--<option class="fa" style="font-family: 'Poppins', sans-serif;">Select</option>-->
                        <option value="terms" style="font-family: 'Poppins', sans-serif;">Terms & Condition</option>
                        <option value="privacy_policy" style="font-family: 'Poppins', sans-serif;"> Privcy Policy</option>
                        <option value="risk_disclosure" style="font-family: 'Poppins', sans-serif;"> Disclaimer</option>
                        <option value="return_refund" style="font-family: 'Poppins', sans-serif;"> Return & Refund Policy</option>
                        <option selected value="cookies_policy" style="font-family: 'Poppins', sans-serif;"> Cookies Policy</option>
                        <option value="AML_KYC" style="font-family: 'Poppins', sans-serif;"> AML & KYC Policy</option>
                        <option value="demo_trading" style="font-family: 'Poppins', sans-serif;"> Demo & Trading Account</option>
                        <option value="payment_policy" style="font-family: 'Poppins', sans-serif;"> Payment Policy</option>
                    </select>
                    <br><br>
                </form>
                <article class="mt-70 mt-lg-0">                    
                    <h4 class="title text-uppercase pb-30" style="text-align: left;"><b>INTRODUCTION</b></h4>
                    <p class="pb-20 text-justify">Earnfinex (“we” or “us” or “our”) may use cookies, web beacons, tracking pixels, and other tracking technologies when you visit our website [Name of Website.com], including any other media form, media channel, mobile website, or mobile application related or connected thereto (collectively, the “Site”) to help customize the Site and improve your experience.
                    </p>
                    <p class="pb-20 text-justify">We reserve the right to make changes to this Cookie Policy at any time and for any reason. We will alert you about any changes by updating the “Last Updated” date of this Cookie Policy. Any changes or modifications will be effective immediately upon posting the updated Cookie Policy on the Site, and you waive the right to receive specific notice of each such change or modification.
                    </p>
                    <p class="pb-20 text-justify">You are encouraged to periodically review this Cookie Policy to stay informed of updates. You will be deemed to have been made aware of, will be subject to, and will be deemed to have accepted the changes in any revised Cookie Policy by your continued use of the Site after the date such revised Cookie Policy is posted.
                    </p>
                    <p class="pb-20 text-justify">This cookie policy was created using Termly.
                    </p>
                    <h4 class="title text-uppercase pb-30" style="text-align: left;"><b>USE OF COOKIES</b></h4>
                    <p class="pb-20 text-justify">A “cookie” is a string of information which assigns you a unique identifier that we store on your computer. Your browser then provides that unique identifier to use each time you submit a query to the Site. We use cookies on the Site to, among other things, keep track of services you have used, record registration information, record your user preferences, keep you logged into the Site, facilitate purchase procedures, and track the pages you visit. Cookies help us understand how the Site is being used and improve your user experience.
                    </p>
                    <h4 class="title text-uppercase pb-30" style="text-align: left;"><b>TYPES OF COOKIES</b></h4>
                    <p class="pb-20 text-justify">The following types of cookies may be used when you visit the Site:
                    </p>
                    <h4 class="title text-uppercase pb-30" style="text-align: left;"><b>Advertising Cookies</b></h4>
                    <p class="pb-20 text-justify">Advertising cookies are placed on your computer by advertisers and ad servers in order to display advertisements that are most likely to be of interest to you. These cookies allow advertisers and ad servers to gather information about your visits to the Site and other websites, alternate the ads sent to a specific computer, and track how often an ad has been viewed and by whom. These cookies are linked to a computer and do not gather any personal information about you.
                    </p>
                    <h4 class="title text-uppercase pb-30" style="text-align: left;"><b>Analytics Cookies</b></h4>
                    <p class="pb-20 text-justify">Analytics cookies monitor how users reached the Site, and how they interact with and move around once on the Site. These cookies let us know what features on the Site are working the best and what features on the Site can be improved.
                    </p>
                    <h4 class="title text-uppercase pb-30" style="text-align: left;"><b>Our Cookies</b></h4>
                    <p class="pb-20 text-justify">Our cookies are “first-party cookies”, and can be either permanent or temporary. These are necessary cookies, without which the Site won't work properly or be able to provide certain features and functionalities. Some of these may be manually disabled in your browser, but may affect the functionality of the Site.
                    </p>
                    <h4 class="title text-uppercase pb-30" style="text-align: left;"><b>Personalization Cookies</b></h4>
                    <p class="pb-20 text-justify">Personalization cookies are used to recognize repeat visitors to the Site. We use these cookies to record your browsing history, the pages you have visited, and your settings and preferences each time you visit the Site.
                    </p>
                    <h4 class="title text-uppercase pb-30" style="text-align: left;"><b>Security Cookies</b></h4>
                    <p class="pb-20 text-justify">Security cookies help identify and prevent security risks. We use these cookies to authenticate users and protect user data from unauthorized parties.
                    </p>
                    <h4 class="title text-uppercase pb-30" style="text-align: left;"><b>Site Management Cookies</b></h4>
                    <p class="pb-20 text-justify">Site management cookies are used to maintain your identity or session on the Site so that you are not logged off unexpectedly, and any information you enter is retained from page to page. These cookies cannot be turned off individually, but you can disable all cookies in your browser.
                    </p>
                    <h4 class="title text-uppercase pb-30" style="text-align: left;"><b>Third-Party Cookies</b></h4>
                    <p class="pb-20 text-justify">Third-party cookies may be place on your computer when you visit the Site by companies that run certain services we offer. These cookies allow the third parties to gather and track certain information about you. These cookies can be manually disabled in your browser.
                    </p>
                    <h4 class="title text-uppercase pb-30" style="text-align: left;"><b>CONTROL OF COOKIES</b></h4>
                    <p class="pb-20 text-justify">Most browsers are set to accept cookies by default. However, you can remove or reject cookies in your browser’s settings. Please be aware that such action could affect the availability and functionality of the Site.
                    </p>
                    <p class="pb-20 text-justify">For more information on how to control cookies, check your browser or device’s settings for how you can control or reject cookies, or visit the following links:
                        <br>• <a href="https://support.apple.com/en-in/guide/safari/sfri11471/mac" target="_blank">Apple Safari</a>
                        <br>• <a href="https://support.google.com/chrome/answer/95647?co=GENIE.Platform%3DDesktop&hl=en" target="_blank">Google Chrome</a>
                        <br>• <a href="https://support.microsoft.com/en-us/windows/microsoft-edge-browsing-data-and-privacy-bb8174ba-9d73-dcf2-9b4a-c582b4e640dd" target="_blank">Microsoft Edge</a>
                        <br>• <a href="https://support.microsoft.com/en-us/topic/delete-and-manage-cookies-168dab11-0753-043d-7c16-ede5947fc64d" target="_blank">Microsoft Internet Explorer</a>
                        <br>• <a href="https://support.mozilla.org/en-US/kb/enhanced-tracking-protection-firefox-desktop?redirectslug=enable-and-disable-cookies-website-preferences&redirectlocale=en-US" target="_blank">Mozilla Firefox</a>
                        <br>• <a href="https://help.opera.com/en/latest/web-preferences/" target="_blank">Opera</a>
                        <br>• <a href="https://support.google.com/chrome/answer/95647?co=GENIE.Platform%3DAndroid&hl=en&oco=1" target="_blank">Android (Chrome)</a>
                        <br>• <a href="https://docs.blackberry.com/content/dam/docs-blackberry-com/release-pdfs/en/device-user-guides/BlackBerry-Classic-Smartphone-10.3.3-User-Guide-en.pdf" target="_blank">Blackberry</a>
                        <br>• <a href="https://support.google.com/chrome/answer/95647?co=GENIE.Platform%3DiOS&hl=en&oco=1" target="_blank">iPhone or iPad (Chrome)</a>
                        <br>• <a href="https://support.google.com/chrome/answer/95647?co=GENIE.Platform%3DAndroid&hl=en&oco=1" target="_blank">iPhone or iPad (Safari)</a>
                    </p>
                    <h4 class="title text-uppercase pb-30" style="text-align: left;"><b>OTHER TRACKING TECHNOLOGIES</b></h4>
                    <p class="pb-20 text-justify">In addition to cookies, we may use web beacons, pixel tags, and other tracking technologies on the Site to help customize the Site and improve your experience. A “web beacon” or “pixel tag” is tiny object or image embedded in a web page or email. They are used to track the number of users who have visited particular pages and viewed emails, and acquire other statistical data. They collect only a limited set of data, such as a cookie number, time and date of page or email view, and a description of the page or email on which they reside. Web beacons and pixel tags cannot be declined. However, you can limit their use by controlling the cookies that interact with them.
                    </p>
                    <h4 class="title text-uppercase pb-30" style="text-align: left;"><b>PRIVACY POLICY</b></h4>
                    <p class="pb-20 text-justify">For more information about how we use information collected by cookies and other tracking technologies. This Cookie Policy is part of and is incorporated into our Privacy Policy. By using the Site, you agree to be bound by this Cookie Policy and our Privacy Policy.
                    </p>
                </article>
            </div>
        </div>
    </div>
</section>
<!--============= Privacy Section Ends Here =============-->
<?php $this->load->view('common/footer_dark');  ?>