<?php $this->load->view('common/header_dark');  ?>

<!--============= Header Section Ends Here =============-->
<section class="page-header bg_img oh" data-background="<?= base_url('assets/front/dark/images/page-header.png') ?>">
    <div class="bottom-shape d-none d-md-block">
        <img src="<?= base_url('assets/front/dark/css/img/page-header.png') ?>" alt="css">
    </div>
    <div class="page-left-thumb">
        <!-- <img src="<?= base_url('assets/front/images/bg/privacy-header.png') ?>" alt="bg"> -->
    </div>
    <div class="container">
        <div class="page-header-content cl-white">
            <h1 class="title">Demo & Trading Account</h1>
        </div>
    </div>
</section>
<!--============= Header Section Ends Here =============-->



<!--============= Privacy Section Starts Here =============-->
<section class="privacy-section padding-top padding-bottom">
    <div class="container">
        <div class="row justify-content-center">           
        </div>
        <?php $this->load->view('common/sidebar_dark');  ?>
        <div class="col-lg-9 col-xl-9 col-md-8">
            <form class="hdmob">
                <select class="fa form-control " onchange="window.location='<?php echo base_url(); ?>page/rules/'+this.value" style="background-color: #ebecec; color: black; font-size: 16px; border: 1px solid rgba(255, 255, 255, 0.3); border-radius: 1.25rem; height: calc(2.25rem + 20px); font-family: 'Poppins', sans-serif; ">

                    <!--<option class="fa" style="font-family: 'Poppins', sans-serif;">Select</option>-->
                    <option value="terms" style="font-family: 'Poppins', sans-serif;">Terms & Condition</option>
                    <option value="privacy_policy" style="font-family: 'Poppins', sans-serif;"> Privcy Policy</option>
                    <option value="risk_disclosure" style="font-family: 'Poppins', sans-serif;"> Disclaimer</option>
                    <option value="return_refund" style="font-family: 'Poppins', sans-serif;"> Return & Refund Policy</option>
                    <option value="cookies_policy" style="font-family: 'Poppins', sans-serif;"> Cookies Policy</option>
                    <option value="AML_KYC" style="font-family: 'Poppins', sans-serif;"> AML & KYC Policy</option>
                    <option selected value="demo_trading" style="font-family: 'Poppins', sans-serif;"> Demo & Trading Account</option>
                    <option value="payment_policy" style="font-family: 'Poppins', sans-serif;"> Payment Policy</option>
                </select>
                <br><br>
            </form>
            <article class="mt-70 mt-lg-0">                
                <p class="pb-20 text-justify">Agreement for the provision of practice account to a retail person.</p>
                <p class="pb-20 text-justify">This Agreement is entered into by and between:</p>
                <p class="pb-20 text-justify">Earnfinex with company No. 13069879, and registered at United Kingdom (hereinafter referred to as the Company) on the one part and the individual client who has registered for a Demo Account (as defined below) with the Company, on the other part (hereinafter referred to as the Client)(collectively the Company and the Client hereinafter referred to as the Parties).
                </p>
                <h4 class="title text-uppercase pb-30" style="text-align: left;"><b>Application of these Terms & Conditions</b></h4>
                <p class="pb-20 text-justify">By the creation and registration of a Demo Account by a client, the Client agrees to and accepts:
                    <br>a) These Terms & Conditions of use of his Demo Account which constitute a legal agreement between the Client and the Company and will bind the Client.
                    <br><br>b) The Company does not provide to the Client any investment services until the client registers a trading account and accepts the terms and conditions for the investment services.
                    <br><br>c) To have acknowledged that he/she has read, understood and accepted the provisions of the Privacy Policy on the Company’s website www.iqoption.com (the Website) in which, among others, the terms of use by the Company of any data and/or information of the Client (personal or sensitive), is described and/or stated. The Privacy Policy should be read in conjunction with the provisions of paragraph 4 of these Terms & Conditions.
                </p>

                <h4 class="title text-uppercase pb-30" style="text-align: left;"><b>Provision of a Free Account</b></h4>
                <p class="pb-20 text-justify">
                    a) The Company will provide a Free Practice Account to the Client and credit virtual funds to the balance of such account (Demo Account). The virtual funds are NOT considered real money and have no value. Virtual funds may not be withdrawn or transferred to other accounts you hold with the Company or with third parties.
                    <br><br>b) The Company agrees to provide the Client with the Demo Account subject to the Client:
                    <br>• Being over 18 years old and of legal competence and sound mind;
                    <br>• Being a resident of a country in which the Company provides its services, in accordance with its General Terms and Conditions;
                    <br>• Not residing in and/or being a citizen of any country where distribution or provision of financial products or services offered by the Company would be contrary to local laws or regulations;
                    <br>• Not being a US Reportable Person;
                    <br><br>c) The Client by registering for a Demo Account declares and confirms all the above representations.
                    <br><br>d) Without derogation from the above, the Company reserves the right, acting reasonably, to suspend or refuse access to and use of, the Company’s trading platform to anyone at its sole and absolute discretion.
                    <br><br>e) The Demo Account is provided to give Clients a demonstration of the Company’s trading platform and to offer Clients the opportunity to trade on simulated real trading conditions. This further allows the Client to gain more experience prior to trading with real funds and facing actual risk. Clients may test their trading strategies and become familiar with the trading platform at their own pace.
                    <br><br>f) The Client is able to place trades and open positions (demo deals) in the Company’s trading platform and these positions will be kept open for a maximum period of one (1) month, except where the number of open positions exceeds the Company’s internal threshold. However, it should be noted that these trades are not executed in the market.
                    <br><br>g) The Client is able to use charts, follow market trends, utilize and familiarize themselves with all the tools offered on the Company’s trading platform and participate in free of charge trading Tournaments offered by the Company (“Tournaments”). In case the Client participates in Tournaments, the provision of section 6 and the Terms and Conditions of the Agreement for the Provision of real accounts shall apply.
                    <br><br>h) The Client is able to request additional virtual funds through the Demo Account and trade for an unlimited period of time with no obligation to deposit real funds.
                    <br><br>i) The Client is able to place trades and open positions (demo deals) in the Company’s trading platform and these positions will be kept open for a maximum period of one (1) month, except where the number of open positions exceeds the Company’s internal threshold. However, it should be noted that these trades are not executed in the market.
                    <br><br>j) The Company reserves the right to make changes, amend, alter, modify, delete or add, at any time and without notice, to these Terms & Conditions and to the content and availability of the Demo Account (the Changes). The Company shall post such Changes and/or add such Changes to the Terms & Conditions on the Company’s Website, and such notification shall be deemed as sufficient notice to the Client and the Changes shall be deemed as accepted by the Client upon the next time the Client shall login to his Demo Account or following three days from the date of the Changes, whichever occurs earlier.
                </p>
                <h4 class="title text-uppercase pb-30" style="text-align: left;"><b>Client Warranties and Representations</b></h4>
                <p class="pb-20 text-justify">The Client, upon his/her registration and/or creation of a Demo Account, hereby warrants and represents to the Company the following:
                    <br><br>a) The Client has the authority to enter into this Agreement and to execute the provisions thereof;
                    <br><br>b) The Client is not under any legal disability with respect to, and is not subject to any law or regulation which prevents his performance of this Agreement;
                    <br><br>c) The Client acts as principal and not as an authorized representative/attorney or trustee of any third party.
                    <br><br>d) The Client certifies that he has provided accurate, complete and true information about himself upon registration and will maintain the accuracy of the provided information by promptly updating any registration information that may have changed. Failure to do so may result in Demo Account closure without notice by the Company;
                    <br><br>e) The Client confirms that he/she is not a US Reportable Person.
                    <br><br>f) The Client confirms that he has reached the age of maturity in the country of his/her residency.
                    <br><br>g) The Client confirms that the purpose and reason for registering and operating a Demo Account is to practice trading, on their own behalf, in the financial products offered by the Company and to take advantage of the services offered by the Company. The Client warrants that should the reason for operating an IQ Option Trading account change, they will inform the Company immediately.
                    <br><br>h) The Client acknowledges that the Company relies and takes into consideration the Client’s above warranties and representations in the offering of the services connected to a Demo Account, to the Client.
                </p>
                <h4 class="title text-uppercase pb-30" style="text-align: left;"><b>Confidentiality</b></h4>
                <p class="pb-20 text-justify">
                    a) The Parties agree to keep confidential and not to disclose to any third party any confidential information given by the other Party under this Agreement, including without limitation all the communication, documentation or other information exchanged between them, both during the term of the Agreement as well as after its termination.
                    <br><br>b) By registering and/or creating a Demo Account, the Client acknowledges and consents to the processing of any personal data provided by him/her to the Company in the manner as described herein and at all times as permitted by applicable laws.
                    <br><br>c) The Company has the right, without prior notice to the Client, to disclose personal data or details of the transactions of the Client in order to comply with the requirements of regulatory authorities. The Company may also disclose such information to its auditors/consultants provided if they are informed and committed to the confidentiality of the information communicated.
                    <br><br>d) The Company will handle all Clients’ personal data according to the relevant laws and regulations for the protection of personal data as this may be amended from time to time.
                </p>
                <h4 class="title text-uppercase pb-30" style="text-align: left;"><b>Warning</b></h4>
                <p class="pb-20 text-justify">Trading using a Demo Account simulates real trading conditions but does not expose you to risk of loss of real funds. Trading in a Demo Account exposes you to virtual risk and cannot give you an indication of the emotions linked to trading with real funds and real losses. If you feel you are ready to move onto a real trading account, understand that you may not have developed proper risk management techniques and/or market awareness and/or trading skills, by using the Demo Account.
                </p>
                <h4 class="title text-uppercase pb-30" style="text-align: left;"><b>Tournaments</b></h4>
                <p class="pb-20 text-justify">Clients are given the opportunity to enter into Tournaments in which virtual funds are traded. The top traders in the tournaments are awarded real funds into their trading accounts. They accept that the Company cannot guarantee fair play amongst all participants in the tournaments and enters them at their own risk.
                </p>
                <p class="pb-20 text-justify">Clients shall accept the Terms and Conditions for the Provision of Investment Services, Activities, and Ancillary Services for real accounts by clicking the acceptance box upon registering for a tournament account.
                </p>
                <p class="pb-20 text-justify">The Client acknowledges and agrees that the Company will have the right, at any time and for any reason and without justification, at its sole discretion, to cease offering Tournaments.
                </p>
                <p class="pb-20 text-justify">The Company reserves the right at its discretion to terminate the Client’s trading account where it has identified that the Client has performed an act with the intention and/or effect of manipulating and/or abusing the Company’s Tournament scheme. The Company shall have no liability towards the Client and no obligation to pay the profit of the Client for such reasons.
                </p>
            </article>
        </div>
    </div>
    </div>
</section>
<!--============= Privacy Section Ends Here =============-->
<?php $this->load->view('common/footer_dark');  ?>