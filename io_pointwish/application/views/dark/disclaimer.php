<?php $this->load->view('common/header_dark');  ?>

<!--============= Header Section Ends Here =============-->
<section class="page-header bg_img oh" data-background="<?= base_url('assets/front/dark/images/page-header.png') ?>">
    <div class="bottom-shape d-none d-md-block">
        <img src="<?= base_url('assets/front/dark/css/img/page-header.png') ?>" alt="css">
    </div>
    <div class="page-left-thumb">
        <!-- <img src="<?= base_url('assets/front/images/bg/privacy-header.png') ?>" alt="bg"> -->
    </div>
    <div class="container">
        <div class="page-header-content cl-white">
            <h1 class="title">Disclaimer</h1>
        </div>
    </div>
</section>
<!--============= Header Section Ends Here =============-->



<!--============= Privacy Section Starts Here =============-->
<section class="privacy-section padding-top padding-bottom">
    <div class="container">
        <div class="row justify-content-center">            
        </div>
<?php $this->load->view('common/sidebar_dark');  ?>
            <div class="col-lg-9 col-xl-9 col-md-8">
                <form class="hdmob">
                    <select class="fa form-control " onchange="window.location='<?php echo base_url(); ?>page/rules/'+this.value" style="background-color: #ebecec; color: black; font-size: 16px; border: 1px solid rgba(255, 255, 255, 0.3); border-radius: 1.25rem; height: calc(2.25rem + 20px); font-family: 'Poppins', sans-serif; ">

                        <!--<option class="fa" style="font-family: 'Poppins', sans-serif;">Select</option>-->
                        <option value="terms" style="font-family: 'Poppins', sans-serif;">Terms & Condition</option>
                        <option value="privacy_policy" style="font-family: 'Poppins', sans-serif;"> Privcy Policy</option>
                        <option selected value="risk_disclosure" style="font-family: 'Poppins', sans-serif;"> Disclaimer</option>
                        <option value="return_refund" style="font-family: 'Poppins', sans-serif;"> Return & Refund Policy</option>
                        <option value="cookies_policy" style="font-family: 'Poppins', sans-serif;"> Cookies Policy</option>
                        <option value="AML_KYC" style="font-family: 'Poppins', sans-serif;"> AML & KYC Policy</option>
                        <option value="demo_trading" style="font-family: 'Poppins', sans-serif;"> Demo & Trading Account</option>
                        <option value="payment_policy" style="font-family: 'Poppins', sans-serif;"> Payment Policy</option>
                    </select>
                    <br><br>
                </form>
                <article class="mt-70 mt-lg-0">                
                    <p class="pb-20 text-justify">By accessing earnfinex.com or any of its associate/group sites,
                        you have read, understood and agree to be legally bound by the terms of the following
                        disclaimer and user agreement.
                    </p>
                    <p class="pb-20 text-justify">earnfinex.com has taken due care and caution in compilation of
                        data for its web site. The views and trading tips expressed by trading experts on
                        earnfinex.com are their own, and not that of the website or its management. earnfinex.com
                        advises users to check with certified experts before taking any investment decision.
                        However, earnfinex.com does not guarantee the accuracy, adequacy or completeness of any
                        information and is not responsible for any errors or omissions or for the results obtained
                        from the use of such information. earnfinex.com especially states that it has no financial
                        liability whatsoever to any user on account of the use of information provided on its
                        website.
                    </p>
                    <p class="pb-20 text-justify">The information on this website is updated from time to time.
                        earnfinex.com however excludes any warranties (whether expressed or implied), as to the
                        quality, accuracy, efficacy, completeness, performance, fitness or any of the contents of
                        the website, including (but not limited) to any comments, feedback and advertisements
                        contained within the Site.
                    </p>
                    <p class="pb-20 text-justify">This website contains material in the form of inputs submitted
                        by users and earnfinex.com accepts no responsibility for the content or accuracy of such
                        content nor does earnfinex.com make any representations by virtue of the contents of this
                        website in respect of the existence or availability of any goods and services advertised in
                        the contributory sections. earnfinex.com makes no warranty that the contents of the website
                        are free from infection by viruses or anything else which has contaminating or destructive
                        properties and shall have no liability in respect thereof.
                    </p>
                    <p class="pb-20 text-justify">This website will contain articles contributed by several
                        individuals. The views are exclusively their own and do not necessarily represent the views
                        of the website or its management. The linked sites are not under our control and we are not
                        responsible for the contents of any linked site or any link contained in a linked site, or
                        any changes or updates to such sites. news.earnfinex.com is providing these links to you
                        only as a convenience, and the inclusion of any link does not imply endorsement by us of the
                        site.
                    </p>
                    <p class="pb-20 text-justify">Crypto trading is inherently risky and you agree to assume
                        complete and full responsibility for the outcomes of all trading decisions that you make,
                        including but not limited to loss of capital. None of the crypto trading calls made by us
                        and group companies associated with it should be construed as an offer to buy or sell
                        securities, nor advice to do so. All comments and posts made by us, group companies
                        associated with it and employees/owners are for information purposes only and under no
                        circumstances should be used for actual trading. Under no circumstances should any person at
                        this site make trading decisions based solely on the information discussed herein. We are
                        not a qualified crypto advisor and you should not construe any information discussed herein
                        to constitute investment advice. It is informational in nature.
                    </p>
                    <p class="pb-20 text-justify">You should consult a qualified trader or other financial advisor
                        prior to making any actual investment or trading decisions. You agree to not make actual
                        crypto trades based on comments on the site, nor on any techniques presented nor discussed
                        in this site or any other form of information presentation. All information is for
                        educational and informational use only. You agree to consult with an investment advisor,
                        which we are not, prior to making any trading decision of any kind. Hypothetical or
                        simulated performance results have certain inherent limitations. Unlike an actual
                        performance record, simulated results do not represent actual trading. No representation is
                        being made that any account will or is likely to achieve profits or losses similar to those
                        shown.
                    </p>
                    <p class="pb-20 text-justify">We encourage all investors to use the information on the site as
                        a resource only to further their own research on all featured markets and information
                        presented on the site. Nothing published on this site should be considered as investment
                        advice.
                    </p>
                    <p class="pb-20 text-justify">Earnfinex, its management, its associate companies and/or their
                        employees take no responsibility for the veracity, validity and the correctness of the
                        expert recommendations or other information or research. Although we attempt to research
                        thoroughly on information provided herein, there are no guarantees in accuracy. The
                        information presented on the site has been gathered from various sources believed to be
                        providing correct information. group, companies, associates and/or employees are not
                        responsible for errors, inaccuracies if any in the content provided on the site. Any
                        prediction made on the direction of the stock market or on the direction of individual
                        stocks may prove to be incorrect. Users/visitors are expected to refer to other investment
                        resources to verify the accuracy of the data posted on this site on their own.
                    </p>
                    <p class="pb-20 text-justify">earnfinex.com does not represent or endorse the accuracy or
                        reliability of any of the information, conversation, or content contained on, distributed
                        through, or linked, downloaded or accessed from any of the services contained on this
                        website (hereinafter, the "Service"), nor the quality of any products, information or other
                        materials displayed, purchased, or obtained by you as a result of any other information or
                        offer by or in connection with the Service.
                    </p>
                    <p class="pb-20 text-justify">Neither earnfinex.com nor its principals, franchise owner,
                        associates or employees, are licensed to provide investment advice. No materials in
                        earnfinex.com , either on behalf of earnfinex.com or any site host, or any participant in
                        earnfinex.com or any of its associated sites should be taken as investment advice directly,
                        indirectly, implicitly, or in any manner whatsoever, including but not limited to trading of
                        stocks or investing in mutual funds on a short term or long term basis, or trading of any
                        financial instruments whatsoever. Past Performance Is Not Indicative of Future Returns. All
                        analyst commentary, mutual fund ratings and rankings provided on earnfinex.com is provided
                        for information purposes only. This information is NOT a recommendation or solicitation to
                        buy or sell any crypto. Your use of this and all information contained on earnfinex.com is
                        governed by this Terms and Conditions of Use. This material is based upon information that
                        we consider reliable, but we do not represent that it is accurate or complete, and that it
                        should be relied upon, as such. You should not rely solely on the Information in making any
                        investment. Rather, you should use the Information only as a starting point for doing
                        additional independent research in order to allow you to form your own opinion regarding
                        investments.
                    </p>
                    <p class="pb-20 text-justify">By using earnfinex.com including any software and content
                        contained therein, you agree that use of the Service is entirely at your own risk.
                        earnfinex.com is not a registered investment advisor or a broker dealer. You understand and
                        acknowledge that there is a very high degree of risk involved in trading securities. Past
                        results of any trader published on this website are not indicative of future returns by that
                        trader, and are not indicative of future returns which be realized by you. Any information,
                        opinions, advice or offers posted by any person or entity logged in to earnfinex.com or any
                        of its associated sites is to be construed as public conversation only. earnfinex.com makes
                        no warranties and gives no assurances regarding the truth, timeliness, reliability, or good
                        faith of any material posted at earnfinex.com. earnfinex.com does not warrant that trading
                        methods or systems presented in their services or the information herein, or obtained from
                        advertisers or members will result in profits or losses.
                    </p>

                </article>
            </div>
        </div>
    </div>
</section>
<!--============= Privacy Section Ends Here =============-->
<?php $this->load->view('common/footer_dark');  ?>