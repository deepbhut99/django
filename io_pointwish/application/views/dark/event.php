<?php $this->load->view('commonfront/header') ?>
<link rel="stylesheet" href="<?= base_url('assets/frontforpwt/css/gallery.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/frontforpwt/css/faq.css')?>">
<div class="wd_scroll_wrap">
    <section class="about-area privacy-section">
        <div class="container">
            <div>
                <h1 class="privacy-heading-text"><?php echo ($title) ?></h1>
            </div>
        </div>
    </section>
    <!--About area end here-->
    <div class="sud">
        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1920 181.1" style="enable-background:new 0 0 1920 181.1;" xml:space="preserve">
            <style type="text/css">
                .st0 {
                    fill-rule: evenodd;
                    clip-rule: evenodd;
                    fill: #10122d;
                }
            </style>
            <g>
                <path class="st0" d="M0,80c0,0,28.9-4.2,43-13c14.3-9,71-35.7,137,5c17.3,7.7,33.3,13,48,11c17.3,0.3,50.3,4.7,66,23
                     c20.3,9.7,68,40.3,134-12c24-11,59-16.3,104,2c21,7.3,85,27.7,117-14c24-30.7,62.7-55,141-12c26,10.3,72,14.7,110-14
                     c37.7-19,89.7-29,122,53c23,32.7,47.7,66.3,97,26c24-22.7,51-78.3,137-38c0,0,28.3,15.7,52,15c23.7-0.7,50.7,4.3,76,41
                     c19.7,19.7,71,36.7,121-2c0,0,22.3-16,55-12c0,0,32.7,6.7,56-71c23.3-76,79-92,122-29c9.3,13.7,25,42,62,43c37,1,51.7,25.3,67,48
                     c15.3,22.7,51,22.7,53,23v28.1H0V80z" />
            </g>
        </svg>
    </div>
    <div class="wd_scroll_wrap">
        <section class="features-area section">
            <div class="container">
                <div class="row d-flex justify-center">
                    <!-- <div class="col-lg-8 col-md-7">
                        <div class="accordion">
                            <div class="accordion-item">
                                <button id="accordion-button-1" aria-expanded="false">
                                    <span class="event-title">May 2022</span>                                    
                                </button>
                                <div class="accordion-content">
                                    <div class="row mx-0">
                                        <div class="col-lg-3 event-box">
                                            <div class="magnific-img">
                                                <a class="image-popup-vertical-fit" href="https://unsplash.it/974/?random" title="9.jpg">
                                                    <img class="event-img" src="https://unsplash.it/974/?random" alt="9.jpg" />
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 event-box">
                                            <div class="magnific-img">
                                                <a class="image-popup-vertical-fit" href="https://unsplash.it/900/?random" title="10.jpg">
                                                    <img class="event-img" src="https://unsplash.it/900/?random" alt="10.jpg" />
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 event-box">
                                            <div class="magnific-img">
                                                <a class="image-popup-vertical-fit" href="https://unsplash.it/902" title="3.jpg">
                                                    <img class="event-img" src="https://unsplash.it/902/" alt="3.jpg" />
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 event-box">
                                            <div class="magnific-img">
                                                <a class="image-popup-vertical-fit" href="https://unsplash.it/901" title="4.jpg">
                                                    <img class="event-img" src="https://unsplash.it/901" alt="4.jpg" />
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 event-box">
                                            <div class="magnific-img">
                                                <a class="image-popup-vertical-fit" href="https://unsplash.it/888/?random" title="1.jpg">
                                                    <img class="event-img" src="https://unsplash.it/888/?random" alt="1.jpg" />
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 event-box">
                                            <div class="magnific-img">
                                                <a class="image-popup-vertical-fit" href="https://unsplash.it/931/?random" title="2.jpg">
                                                    <img class="event-img" src="https://unsplash.it/931/?random" alt="2.jpg" />
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 event-box">
                                            <div class="magnific-img">
                                                <a class="image-popup-vertical-fit" href="https://unsplash.it/908/?random" title="5.jpg">
                                                    <img class="event-img" src="https://unsplash.it/908/?random" alt="5.jpg" />
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 event-box">
                                            <div class="magnific-img">
                                                <a class="image-popup-vertical-fit" href="https://unsplash.it/978/?random" title="6.jpg">
                                                    <img class="event-img" src="https://unsplash.it/978/?random" alt="6.jpg" />
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 event-box">
                                            <div class="magnific-img">
                                                <a class="image-popup-vertical-fit" href="https://unsplash.it/857/?random" title="7.jpg">
                                                    <img class="event-img" src="https://unsplash.it/857/?random" alt="7.jpg" />
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 event-box">
                                            <div class="magnific-img">
                                                <a class="image-popup-vertical-fit" href="https://unsplash.it/905/?random" title="8.jpg">
                                                    <img class="event-img" src="https://unsplash.it/905/?random" alt="8.jpg" />
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 event-box">
                                            <div class="magnific-img">
                                                <a class="image-popup-vertical-fit" href="https://unsplash.it/1230/?random" title="12.jpg">
                                                    <img class="event-img" src="https://unsplash.it/1230/?random" alt="12.jpg" />
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 event-box">
                                            <div class="magnific-img">
                                                <a class="image-popup-vertical-fit" href="https://unsplash.it/800/?random" title="13.jpg">
                                                    <img class="event-img" src="https://unsplash.it/800/?random" alt="13.jpg" />
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 event-box">
                                            <div class="magnific-img">
                                                <a class="image-popup-vertical-fit" href="https://unsplash.it/930/?random" title="14.jpg">
                                                    <img class="event-img" src="https://unsplash.it/930/?random" alt="14.jpg" />
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 event-box">
                                            <div class="magnific-img">
                                                <a class="image-popup-vertical-fit" href="https://unsplash.it/999/?random" title="15.jpg">
                                                    <img class="event-img" src="https://unsplash.it/999/?random" alt="15.jpg" />
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 event-box">
                                            <div class="magnific-img">
                                                <a class="image-popup-vertical-fit" href="https://unsplash.it/897/?random" title="16.jpg">
                                                    <img class="event-img" src="https://unsplash.it/897/?random" alt="16.jpg" />
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <div class="col-lg-5 col-md-6">
                        <div class="event-card">
                            <div class="event-card-body">
                                <div class="event-card-heading">
                                    <h3 class="text-white mb-3">Our Upcoming Events</h3>
                                </div>
                                <div class="event-info">
                                    <div class="date-time-sec">
                                        <span class="date-head">Date : </span>
                                        <span class="date-text">05/01/2022 <small>[03:34 PM]</small></span>
                                        <!-- <span class="time-text">3:34 PM</span> -->
                                    </div>
                                    <div class="event-link">
                                        <span class="link-head">Platform&nbsp;:&nbsp;</span>
                                        <a class="link-text">https://pwt.io/givenlinkhere123456798</a>
                                    </div>
                                    <div class="event-disc">
                                        <p><span class="disc-head">Discription : </span>The discription of event here.The discription of event here.The discription of event here.The discription of event here.</p>
                                    </div>
                                </div>
                                <div class="event-info">
                                    <div class="date-time-sec">
                                        <span class="date-head">Date : </span>
                                        <span class="date-text">05/01/2022 <small>[05:32 PM]</small></span>
                                        <!-- <span class="time-text">3:34 PM</span> -->
                                    </div>
                                    <div class="event-link">
                                        <span class="link-head">Platform&nbsp;:&nbsp;</span>
                                        <a class="link-text">https://pwt.io/givenlinkhere123456798</a>
                                    </div>
                                    <div class="event-disc">
                                        <p><span class="disc-head">Discription : </span>The discription of event here.The discription of event here.The discription of event here.The discription of event here.</p>
                                    </div>
                                </div>
                                <!-- <div class="event-info">
                                    <div class="date-time-sec">
                                        <span>5/1/2022</span>
                                        <span class="float-right">3:34 PM</span>
                                    </div>
                                    <div class="event-link">
                                        <h4><a class="link-text">https://pwt.io/givenlinkhere132465798</a></h4>
                                    </div>
                                    <div class="event-disc">
                                        <p>The discription of event here.The discription of event here.The discription of event here.The discription of event here.</p>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<script src="<?= base_url('assets/frontforpwt/js/faq.js')?>"></script>
<?php $this->load->view('commonfront/footer') ?>
<script src="<?= base_url('assets/frontforpwt/js/jquery.magnific-popup.min.js') ?>"></script>

<script>
    $('.image-popup-vertical-fit').magnificPopup({
    type: 'image',
    mainClass: 'mfp-with-zoom',
    gallery: {
        enabled: true
    },

    zoom: {
        enabled: true,

        duration: 300, // duration of the effect, in milliseconds
        easing: 'ease-in-out', // CSS transition easing function

        opener: function(openerElement) {

            return openerElement.is('img') ? openerElement : openerElement.find('img');
        }
    }

});
</script>