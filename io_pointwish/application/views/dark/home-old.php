<?php $this->load->view('commonfront/header');  ?>
<!--Slider area start here-->
<section class="slider-area">
    <canvas>
        <script src="<?= base_url('assets/frontforpwt/js/three.js') ?>"></script>
        <script src="<?= base_url('assets/frontforpwt/js/stats.min.js') ?>"></script>
        <script src="<?= base_url('assets/frontforpwt/js/Projector.js') ?>"></script>
        <script src="<?= base_url('assets/frontforpwt/js/CanvasRenderer.js') ?>"></script>
    </canvas>
    <div id="particles-js">
        <div id="carousel-example-generic" class="carousel slide backgifanimation" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <div class="carousel-captions caption-1">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="slider-content py-sm-150">
                                        <!-- <ul>
                                            <li data-animation="animated bounceInDown" class="slider_social_icon1"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                            <li data-animation="animated bounceInDown" class="slider_social_icon2"><a href="#"><i class="fab fa-twitter"></i></a></li>
                                            <li data-animation="animated bounceInDown" class="slider_social_icon3"><a href="#"><i class="fab fa-telegram-plane"></i></a></li>
                                            <li data-animation="animated bounceInDown" class="slider_social_icon4"><a href="#"><i class="fab fa-btc"></i></a></li>
                                            <li data-animation="animated bounceInDown" class="slider_social_icon5"><a href="#"><i class="fas fa-envelope"></i></a></li>
                                        </ul> -->
                                        <h2 data-animation="animated bounceInLeft">Artificial Intelligence & Machine learning<br>based Analytical Bot </h2>
                                        <div class="buttons">
                                            <a href="<?= base_url('login') ?>" class="btn1" data-animation="animated bounceInUp">Sign Up</a>
                                            <a class="btn2" data-animation="animated bounceInUp">Join on<i class="fab fa-telegram-plane f-18 pd-l10"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <img class="mobile-screen" src="<?= base_url('assets/frontforpwt/images/banner/xtree.png') ?>" alt="">
                                    <!-- <div class="dm-width">
                                        <div class="dm-device">
                                            <div class="device">
                                                <div class="screen">
                                                    <div class="slider">
                                                        <div class="slider__item slider__item--1">
                                                            <img src="<?= base_url('assets/frontforpwt/images/slider/slide-1.png') ?>">
                                                        </div>
                                                        <div class="slider__item slider__item--2">
                                                            <img src="<?= base_url('assets/frontforpwt/images/slider/slide-2.png') ?>">
                                                        </div>
                                                        <div class="slider__item slider__item--3">
                                                            <img src="<?= base_url('assets/frontforpwt/images/slider/slide-3.png') ?>">
                                                        </div>
                                                        <div class="slider__item slider__item--4">
                                                            <img src="<?= base_url('assets/frontforpwt/images/slider/slide-4.png') ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->
                                </div>
                                <!-- <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 hidden-xs hidden-sm">
                                            <div class="btc_timer_section_wrapper">
                                                <div id="clockdiv">
                                                    <div>
                                                        <span class="days"></span>
                                                        <div class="smalltext">Days</div>
                                                    </div>
                                                    <div>
                                                        <span class="hours"></span>
                                                        <div class="smalltext">Hours</div>
                                                    </div>
                                                    <div>
                                                        <span class="minutes"></span>
                                                        <div class="smalltext">Minutes</div>
                                                    </div>
                                                    <div>
                                                        <span class="seconds"></span>
                                                        <div class="smalltext">Seconds</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="slider-content">
                                    <ul>
                                        <li data-animation="animated bounceInDown" class="slider_social_icon1"><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li data-animation="animated bounceInDown" class="slider_social_icon2"><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li data-animation="animated bounceInDown" class="slider_social_icon3"><a href="#"><i class="fa fa-send-o"></i></a></li>
                                        <li data-animation="animated bounceInDown" class="slider_social_icon4"><a href="#"><i class="fa fa-bitcoin"></i></a></li>
                                        <li data-animation="animated bounceInDown" class="slider_social_icon5"><a href="#"><i class="fa fa-envelope"></i></a></li>
                                    </ul>
                                    <h2 data-animation="animated bounceInLeft">Artificial Intelligence & Machine learning <br>based Analytical Bot</h2>
                                    <div class="buttons">
                                        <a href="#" class="btn1" data-animation="animated bounceInUp">WHITEPAPER</a>
                                        <a href="#" class="btn2" data-animation="animated bounceInUp">Buy Tokens Now!</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12  hidden-xs hidden-sm">
                                <div class="btc_timer_section_wrapper">
                                    <div id="clockdiv2">
                                        <div>
                                            <span class="days"></span>
                                            <div class="smalltext">Days</div>
                                        </div>
                                        <div>
                                            <span class="hours"></span>
                                            <div class="smalltext">Hours</div>
                                        </div>
                                        <div>
                                            <span class="minutes"></span>
                                            <div class="smalltext">Minutes</div>
                                        </div>
                                        <div>
                                            <span class="seconds"></span>
                                            <div class="smalltext">Seconds</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Slider area end here-->
<section class="currency-area">
    <div class="container-fliud">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="rete-list bt_title wow  fadeInUp animated" data-wow-duration="1.0s" style="visibility: visible; animation-duration: 1.0s; animation-name: fadeInUp;">
                    <div class="content">
                        <div class="con">
                            <h2>
                                <img src="<?= base_url('assets/frontforpwt/images/features/option_trading.svg') ?>" alt="">
                                <span>Options Trading</span>
                            </h2>
                            <button class="btn3" data-toggle="modal" data-target="#featureModal">Read More</button>
                        </div>
                    </div>
                </div>
                <div class="rete-list bt_title wow  fadeInUp animated" data-wow-duration="1.3s" style="visibility: visible; animation-duration: 1.3s; animation-name: fadeInUp;">
                    <div class="content">
                        <div class="con">
                            <h2><img src="<?= base_url('assets/frontforpwt/images/features/xbot.svg') ?>" alt="">
                                <span>pwt</span>
                            </h2>
                            <button class="btn3" data-toggle="modal" data-target="#featureModal">Read More</button>
                        </div>
                    </div>
                </div>
                <div class="rete-list bt_title wow  fadeInUp animated" data-wow-duration="1.6s" style="visibility: visible; animation-duration: 1.6s; animation-name: fadeInUp;">
                    <div class="content">
                        <div class="con">
                            <h2><img src="<?= base_url('assets/frontforpwt/images/features/copytrade.svg') ?>" alt=""><span>Copy Trade</span></h2>
                            <button class="btn3" data-toggle="modal" data-target="#featureModal">Read More</button>
                        </div>
                    </div>
                </div>
                <div class="rete-list bt_title wow  fadeInUp animated" data-wow-duration="1.9s" style="visibility: visible; animation-duration: 1.9s; animation-name: fadeInUp;">
                    <div class="content">
                        <div class="con">
                            <h2><img src="<?= base_url('assets/frontforpwt/images/features/agency.svg') ?>" alt=""><span>Agency</span></h2>
                            <button class="btn3" data-toggle="modal" data-target="#featureModal">Read More</button>
                        </div>
                    </div>
                </div>
                <div class="rete-list hidden-md   wow  fadeInUp animated" data-wow-duration="2.1s" style="visibility: visible; animation-duration: 2.1s; animation-name: fadeInUp;">
                    <div class="content">
                        <div class="con">
                            <h2><img src="<?= base_url('assets/frontforpwt/images/features/multiple.svg') ?>" alt=""><span>Arbitrage</span></h2>
                            <button class="btn3" data-toggle="modal" data-target="#featureModal">Read More</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
<!--About area start here-->
<div id="about" class="wd_scroll_wrap">
    <section class="about-area pd-t70 pd-b100 jarallax bg-img">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="about-content">
                        <h2 class="f-40 fw-400 wow  fadeInUp animated" data-wow-duration="1.0s" style="visibility: visible; animation-duration: 1.0s; animation-name: fadeInUp;">About The Project</h2>
                        <p class="wow  fadeInUp animated" data-wow-duration="1.3s" style="visibility: visible; animation-duration: 1.3s; animation-name: fadeInUp;">Our team is working on a bot that will make a profitable trade and learn by itself to improve profit percentage every time</p>
                        <p class="wow  fadeInUp animated" data-wow-duration="1.6s" style="visibility: visible; animation-duration: 1.6s; animation-name: fadeInUp;">We concluded with two different technologies that are used by industry experts and found a solution for traders</p>
                        <p class="wow  fadeInUp animated" data-wow-duration="1.9s" style="visibility: visible; animation-duration: 1.9s; animation-name: fadeInUp;">Artificial Intelligence and Machine learning is the key difference between us and our competitors, our bot is learning after each trade and increase your profit</p>
                        <div class="buttons hidden-sm">
                            <a>
                                <img class="h-50px" src="<?= base_url('assets/frontforpwt/images/icons/android.svg') ?>" alt="">
                            </a>
                            <a>
                                <img class="h-50px" src="<?= base_url('assets/frontforpwt/images/icons/apple.svg') ?>" alt="">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="about-img wow  fadeInUp animated mr-t0" data-wow-duration="1.0s" style="visibility: visible; animation-duration: 1.0s; animation-name: fadeInUp;">
                        <img class="max-h-360" src="<?= base_url('assets/frontforpwt/images/about/ai.png') ?>" alt="" />
                    </div>
                </div>
                <div class="buttons visible-sm text-center">
                    <a>
                        <img class="h-50px mr-t30" src="<?= base_url('assets/frontforpwt/images/icons/android.svg') ?>" alt="">
                    </a>
                    <a>
                        <img class="h-50px mr-t30" src="<?= base_url('assets/frontforpwt/images/icons/apple.svg') ?>" alt="">
                    </a>
                </div>
            </div>
        </div>
        <div class="container mr-t100">
            <div class="row">
                <h2 class="head-visible-md txt-center-sm wow fadeInUp animated" data-wow-duration="1.0s">Options Trading</h2>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="about-img wow mr-r0 mr-t0 fadeInUp animated" data-wow-duration="1.0s" style="visibility: visible; animation-duration: 1.0s; animation-name: fadeInUp;">
                        <img class="h-md-350px" src="<?= base_url('assets/frontforpwt/images/about/options_trading.jpg') ?>" alt="" />
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="about-content">
                        <h2 class="head-hide-md wow fadeInUp animated" data-wow-duration="1.0s">Options Trading</h2>
                        <p class="wow mt-md-20px fadeInUp animated" data-wow-duration="1.3s" style="visibility: visible; animation-duration: 1.3s; animation-name: fadeInUp;">On our platform traders can trade with BTC/USDT and ETH/USDT pair for options trading and it will take only 1 minute to win 95% profit</p>
                        <p class="wow fadeInUp animated" data-wow-duration="1.6s" style="visibility: visible; animation-duration: 1.6s; animation-name: fadeInUp;">Traders have 30 seconds for trade and can predict the coin's trend. After that 30 seconds is the wait time for the result </p>
                        <p class="wow fadeInUp animated" data-wow-duration="1.9s" style="visibility: visible; animation-duration: 1.9s; animation-name: fadeInUp;">If the prediction is correct, traders will instantly get 95% profit with their bid amount. The trader will play this game at a live price of the crypto market</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--About area end here-->
    <div class="sud">
        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1920 181.1" style="enable-background:new 0 0 1920 181.1;" xml:space="preserve">
            <style type="text/css">
                .st0 {
                    fill-rule: evenodd;
                    clip-rule: evenodd;
                    fill: #10122d;
                }
            </style>
            <g>
                <path class="st0" d="M0,80c0,0,28.9-4.2,43-13c14.3-9,71-35.7,137,5c17.3,7.7,33.3,13,48,11c17.3,0.3,50.3,4.7,66,23
                     c20.3,9.7,68,40.3,134-12c24-11,59-16.3,104,2c21,7.3,85,27.7,117-14c24-30.7,62.7-55,141-12c26,10.3,72,14.7,110-14
                     c37.7-19,89.7-29,122,53c23,32.7,47.7,66.3,97,26c24-22.7,51-78.3,137-38c0,0,28.3,15.7,52,15c23.7-0.7,50.7,4.3,76,41
                     c19.7,19.7,71,36.7,121-2c0,0,22.3-16,55-12c0,0,32.7,6.7,56-71c23.3-76,79-92,122-29c9.3,13.7,25,42,62,43c37,1,51.7,25.3,67,48
                     c15.3,22.7,51,22.7,53,23v28.1H0V80z" />
            </g>
        </svg>
    </div>
</div>
<div id="features" class="wd_scroll_wrap">
    <section class="features-area section">
        <div id="features-js">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="section-heading2 wow  fadeInUp animated" data-wow-duration="1.0s" style="visibility: visible; animation-duration: 1.0s; animation-name: fadeInUp;">
                            <h2>Our Features</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="featured-lists">
                            <ul>
                                <li>
                                    <div class="col-sm-6 pd-0 hidden-xs wow  fadeInUp animated" data-wow-duration="1.3s" style="visibility: visible; animation-duration: 1.3s; animation-name: fadeInUp;">
                                        <div class="contents-l mr-b30">
                                            <h2>Annual Fee</h2>
                                            <p>We are charging an $ 100 bot maintenance fee and a $ 20 trading fee,
                                                <br>user can't do trading without trading fee
                                            </p>
                                            <a>
                                                <!-- <i class="fab fa-youtube"></i> -->
                                                <img class="h-25px" src="<?= base_url('assets/frontforpwt/images/icons/youtube.svg') ?>" alt="">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 pd-0 wow  fadeInUp animated" data-wow-duration="1.3s" style="visibility: visible; animation-duration: 1.3s; animation-name: fadeInUp;">
                                        <div class="imgs-l">
                                            <figure><img src="<?= base_url('assets/frontforpwt/images/features/1.png') ?>" alt="" /></figure>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 pd-0 visible-xs  wow  fadeInUp animated" data-wow-duration="1.3s" style="visibility: visible; animation-duration: 1.3s; animation-name: fadeInUp;">
                                        <div class="contents-l mr-b30">
                                            <h2>Annual Fee</h2>
                                            <p>We are charging an $ 100 bot maintenance fee and a $ 20 trading fee,
                                                <br>user can't do trading without trading fee
                                            </p>
                                            <a>
                                                <!-- <i class="fab fa-youtube"></i> -->
                                                <img class="h-25px" src="<?= base_url('assets/frontforpwt/images/icons/youtube.svg') ?>" alt="">
                                            </a>
                                        </div>
                                    </div>
                                </li>
                                <li class="rl">
                                    <div class="col-sm-6 pd-0 floatright mr-b30 hidden-xs wow  fadeInUp animated" data-wow-duration="1.6s" style="visibility: visible; animation-duration: 1.6s; animation-name: fadeInUp;">
                                        <div class="contents-r">
                                            <h2>Multicurrency Trading</h2>
                                            <p>Exchange between all popular currencies with a couple of clicks.
                                                <br>Instant send from one currency to another.
                                            </p>
                                            <a>
                                                <!-- <i class="fab fa-youtube"></i> -->
                                                <img class="h-25px" src="<?= base_url('assets/frontforpwt/images/icons/youtube.svg') ?>" alt="">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 pd-0 wow  fadeInUp animated" data-wow-duration="1.6s" style="visibility: visible; animation-duration: 1.6s; animation-name: fadeInUp;">
                                        <div class="imgs-r">
                                            <figure><img src="<?= base_url('assets/frontforpwt/images/features/2.png') ?>" alt="" /></figure>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 pd-0 floatright mr-b30 visible-xs wow  fadeInUp animated" data-wow-duration="1.6s" style="visibility: visible; animation-duration: 1.6s; animation-name: fadeInUp;">
                                        <div class="contents-r">
                                            <h2>Multicurrency Trading</h2>
                                            <p>Exchange between all popular currencies with a couple of clicks.
                                                <br>Instant send from one currency to another.
                                            </p>
                                            <a>
                                                <!-- <i class="fab fa-youtube"></i> -->
                                                <img class="h-25px" src="<?= base_url('assets/frontforpwt/images/icons/youtube.svg') ?>" alt="">
                                            </a>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="col-sm-6 pd-0 hidden-xs wow  fadeInUp animated" data-wow-duration="1.9s" style="visibility: visible; animation-duration: 1.9s; animation-name: fadeInUp;">
                                        <div class="contents-l mr-b30">
                                            <h2>Zero Investment</h2>
                                            <p>We don't have any investment plan or fixed income plan,
                                                <br>a trading bot will help you to generate a regular income by trading
                                            </p>
                                            <a>
                                                <!-- <i class="fab fa-youtube"></i> -->
                                                <img class="h-25px" src="<?= base_url('assets/frontforpwt/images/icons/youtube.svg') ?>" alt="">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 pd-0 wow  fadeInUp animated" data-wow-duration="1.9s" style="visibility: visible; animation-duration: 1.9s; animation-name: fadeInUp;">
                                        <div class="imgs-l">
                                            <figure><img src="<?= base_url('assets/frontforpwt/images/features/3.png') ?>" alt="" /></figure>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 pd-0 visible-xs wow  fadeInUp animated" data-wow-duration="1.9s" style="visibility: visible; animation-duration: 1.9s; animation-name: fadeInUp;">
                                        <div class="contents-l mr-b30">
                                            <h2>Zero Investment</h2>
                                            <p>We don't have any investment plan or fixed income plan,
                                                <br>a trading bot will help you to generate a regular income by trading
                                            </p>
                                            <a>
                                                <!-- <i class="fab fa-youtube"></i> -->
                                                <img class="h-25px" src="<?= base_url('assets/frontforpwt/images/icons/youtube.svg') ?>" alt="">
                                            </a>
                                        </div>
                                    </div>
                                </li>
                                <li class="rl">
                                    <div class="col-sm-6 pd-0 floatright mr-b30 hidden-xs wow  fadeInUp animated" data-wow-duration="2.1s" style="visibility: visible; animation-duration: 2.1s; animation-name: fadeInUp;">
                                        <div class="contents-r">
                                            <h2>Exchnage Wallet</h2>
                                            <p>Users can trade directly from their exchange wallet through API
                                                <br>Users can use Binance, Coinbase exchange, FTX, Kraken, Kucoin
                                            </p>
                                            <a>
                                                <!-- <i class="fab fa-youtube"></i> -->
                                                <img class="h-25px" src="<?= base_url('assets/frontforpwt/images/icons/youtube.svg') ?>" alt="">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 pd-0 wow  fadeInUp animated" data-wow-duration="2.1s" style="visibility: visible; animation-duration: 2.1s; animation-name: fadeInUp;">
                                        <div class="imgs-r">
                                            <figure><img src="<?= base_url('assets/frontforpwt/images/features/4.png') ?>" alt="" /></figure>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 pd-0 floatright mr-b30 visible-xs wow  fadeInUp animated" data-wow-duration="2.1s" style="visibility: visible; animation-duration: 2.1s; animation-name: fadeInUp;">
                                        <div class="contents-r">
                                            <h2>Exchnage Wallet</h2>
                                            <p>Users can trade directly from their exchange wallet through API
                                                <br>Users can use Binance, Coinbase exchange, FTX, Kraken, Kucoin
                                            </p>
                                            <a>
                                                <!-- <i class="fab fa-youtube"></i> -->
                                                <img class="h-25px" src="<?= base_url('assets/frontforpwt/images/icons/youtube.svg') ?>" alt="">
                                            </a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div id="project" class="wd_scroll_wrap">
    <section class="projects bg-img pd-t100 pd-b70 jarallax">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                    <div class="section-heading">
                        <h2 class="wow  fadeInUp animated" data-wow-duration="1.0s" style="visibility: visible; animation-duration: 1.0s; animation-name: fadeInUp;">Advantage of the Project</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="project-list wow  fadeInUp animated" data-wow-duration="1.0s" style="visibility: visible; animation-duration: 1.0s; animation-name: fadeInUp;">
                        <div class="content">
                            <span class="icons"><img src="<?= base_url('assets/frontforpwt/images/icons/6.png') ?>" alt="" /></span>
                            <h3>Global<br>Exchanges</h3>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="project-list wow  fadeInUp animated" data-wow-duration="1.3s" style="visibility: visible; animation-duration: 1.3s; animation-name: fadeInUp;">
                        <div class="content">
                            <span class="icons"><img src="<?= base_url('assets/frontforpwt/images/icons/7.png') ?>" alt="" /></span>
                            <h3>Advanced <br>Security System</h3>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="project-list wow  fadeInUp animated" data-wow-duration="1.6s" style="visibility: visible; animation-duration: 1.6s; animation-name: fadeInUp;">
                        <div class="content">
                            <span class="icons"><img class="h-85px" src="<?= base_url('assets/frontforpwt/images/icons/arbitrade.svg') ?>" alt="" /></span>
                            <h3>Arbitrage <br> &nbsp; </h3>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="project-list wow  fadeInUp animated" data-wow-duration="1.9s" style="visibility: visible; animation-duration: 1.9s; animation-name: fadeInUp;">
                        <div class="content">
                            <span class="icons"><img src="<?= base_url('assets/frontforpwt/images/icons/9.png') ?>" alt="" /></span>
                            <h3>P2P <br>Exchange</h3>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="project-list wow  fadeInUp animated" data-wow-duration="2.1s" style="visibility: visible; animation-duration: 2.1s; animation-name: fadeInUp;">
                        <div class="content">
                            <span class="icons"><img src="<?= base_url('assets/frontforpwt/images/icons/10.png') ?>" alt="" /></span>
                            <h3>Secured <br>Wallet</h3>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="project-list wow  fadeInUp animated" data-wow-duration="2.4s" style="visibility: visible; animation-duration: 2.4s; animation-name: fadeInUp;">
                        <div class="content">
                            <span class="icons"><img src="<?= base_url('assets/frontforpwt/images/icons/11.png') ?>" alt="" /></span>
                            <h3>Cooperation <br>With Arbitration</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div id="steps" class="wd_scroll_wrap">
    <section class="steps-area section">
        <div id="steps-js">
            <div class="container">
                <div class="row">
                    <div class="col-lg-11 col-md-11 col-sm-12 col-xs-12">
                        <div class="steps-heading">
                            <h2 class="wow  fadeInUp animated" data-wow-duration="1.0s" style="visibility: visible; animation-duration: 1.0s; animation-name: fadeInUp;">Few Steps To Start Bot</h2>
                            <div class="right-con">
                                <span>Scroll Down</span>
                                <a data-scroll data-options='{ "speed": 100 }' href="#steps"><i class="fa fa-angle-down"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="steps" class="steps-details bg-mg">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 col-md-offset-1 col-lg-offset-1">
                    <div class="steps-cont">
                        <ul>
                            <li class="l-con">
                                <div class="cont wow  fadeInUp animated" data-wow-duration="1.0s" style="visibility: visible; animation-duration: 1.0s; animation-name: fadeInUp;">
                                    <h2>01. Registration On The Platform</h2>
                                    <p>Complete the signup process and verifiy your mail account
                                        <br>Please activate 2FA code for security
                                    </p>
                                </div>
                                <span><i class="fa fa-users"></i></span>
                            </li>
                            <li class="r-con">
                                <div class="cont wow  fadeInUp animated" data-wow-duration="1.3s" style="visibility: visible; animation-duration: 1.3s; animation-name: fadeInUp;">
                                    <h2>02. Add API </h2>
                                    <p>User API from your selected exchange and put paste it
                                        <br>Never share API key with anyone users or support staff
                                    </p>
                                </div>
                                <span><img class="iconapi" src="<?= base_url('assets/frontforpwt/images/icons/api.svg') ?>" alt=""></span>
                            </li>
                            <li class="l-con">
                                <div class="cont wow  fadeInUp animated" data-wow-duration="1.6s" style="visibility: visible; animation-duration: 1.6s; animation-name: fadeInUp;">
                                    <h2>03. Create a Bot</h2>
                                    <p>Use predfine stratagey or use your own one
                                        <br>Easy to build or copy from the expert
                                    </p>
                                </div>
                                <span><i class="fas fa-edit"></i></span>
                                <div class="mid-icons"></div>
                            </li>
                            <li class="r-con">
                                <div class="cont wow  fadeInUp animated" data-wow-duration="1.9s" style="visibility: visible; animation-duration: 1.9s; animation-name: fadeInUp;">
                                    <h2>04. Trade on Exchanges</h2>
                                    <p>Select your faviourite exchange and activate API
                                        <br>Allow access only for trading
                                    </p>
                                </div>
                                <span><i class="fas fa-chart-line"></i></span>
                            </li>
                            <li class="l-con">
                                <div class="cont wow  fadeInUp animated" data-wow-duration="2.1s" style="visibility: visible; animation-duration: 2.1s; animation-name: fadeInUp;">
                                    <h2>05. Start Bot Trading</h2>
                                    <p>Bot will use your API and do trading
                                        <br>You can use our copy trade option and follow the experts
                                    </p>
                                </div>
                                <span><i class="fas fa-robot"></i></span>
                            </li>
                            <li class="r-con">
                                <div class="cont wow  fadeInUp animated" data-wow-duration="2.4s" style="visibility: visible; animation-duration:2.4s; animation-name: fadeInUp;">
                                    <h2>06. Collect Profit</h2>
                                    <p>Trading will be done on the exchange and you can withdraw it
                                        <br>Select a best stratagey and book your profit
                                    </p>
                                </div>
                                <span><i class="fas fa-dollar-sign"></i></span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="sud">
        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1920 181.1" style="enable-background:new 0 0 1920 181.1;" xml:space="preserve">
            <style type="text/css">
                .st0 {
                    fill-rule: evenodd;
                    clip-rule: evenodd;
                    fill: #10122d;
                }
            </style>
            <g>
                <path class="st0" d="M0,80c0,0,28.9-4.2,43-13c14.3-9,71-35.7,137,5c17.3,7.7,33.3,13,48,11c17.3,0.3,50.3,4.7,66,23
                     c20.3,9.7,68,40.3,134-12c24-11,59-16.3,104,2c21,7.3,85,27.7,117-14c24-30.7,62.7-55,141-12c26,10.3,72,14.7,110-14
                     c37.7-19,89.7-29,122,53c23,32.7,47.7,66.3,97,26c24-22.7,51-78.3,137-38c0,0,28.3,15.7,52,15c23.7-0.7,50.7,4.3,76,41
                     c19.7,19.7,71,36.7,121-2c0,0,22.3-16,55-12c0,0,32.7,6.7,56-71c23.3-76,79-92,122-29c9.3,13.7,25,42,62,43c37,1,51.7,25.3,67,48
                     c15.3,22.7,51,22.7,53,23v28.1H0V80z" />
            </g>
        </svg>
    </div>
</div>
<div class="wd_scroll_wrap">
    <section class="video-area section">
        <div id="video-js">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="videos-heding">
                            <!-- <h2></h2> -->
                            <div class="videos-box">
                                <video id="my-video" class="video-js" controls preload="auto" width="770" height="400" poster="<?= base_url('assets/frontforpwt/images/video/1.png') ?>" data-setup='{ "techOrder": ["youtube"], "sources": [{ "type": "video/youtube", "src": "https://youtu.be/Gc2en3nHxA4"}], "youtube": { "customVars": { "wmode": "transparent" } } }'>
                                </video>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="wd_scroll_wrap">
        <section id="roadmap" class="video-des bg-img pd-t100">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="deslidt">
                            <h2 class="roadmap-heading">Development of Analytical Bot<br>(Road Map)</h2>
                            <ul>
                                <li>
                                    <span class="years"><span class="con">2021</span></span>
                                    <div class="serials">
                                        <span class="cre"></span>
                                        <div class="col-sm-6 col-xs-12 left-con wow  fadeInUp animated" data-wow-duration="1.0s" style="visibility: visible; animation-duration: 1.0s; animation-name: fadeInUp;">
                                            <div class="box-con">
                                                <div class="dbox">
                                                    <div class="dleft">
                                                        <div class="imgs"><img src="<?= base_url('assets/frontforpwt/images/video/2.png') ?>" alt="" /></div>
                                                    </div>
                                                    <div class="dright">
                                                        <div class="content">
                                                            <p>(3Q,2021)</p>
                                                            <h3>Project Started</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xs-12 right-con  wow  fadeInUp animated" data-wow-duration="1.3s" style="visibility: visible; animation-duration: 1.3s; animation-name: fadeInUp;">
                                            <div class="box-con">
                                                <div class="dbox">
                                                    <div class="dleft">
                                                        <div class="content">
                                                            <p>(4Q,2021)</p>
                                                            <h3>Website Launch</h3>
                                                        </div>
                                                    </div>
                                                    <div class="dright">
                                                        <div class="imgs"><img src="<?= base_url('assets/frontforpwt/images/video/3.png') ?>" alt="" /></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="years"><span class="con">2022</span></span>
                                    <div class="serials">
                                        <span class="cre"></span>
                                        <div class="col-sm-6 col-xs-12 left-con wow  fadeInUp animated" data-wow-duration="1.6s" style="visibility: visible; animation-duration: 1.6s; animation-name: fadeInUp;">
                                            <div class="box-con">
                                                <div class="dbox">
                                                    <div class="dleft">
                                                        <div class="imgs"><img class="mt-n1" src="<?= base_url('assets/frontforpwt/images/video/14.png') ?>" alt="" /></div>
                                                    </div>
                                                    <div class="dright">
                                                        <div class="content">
                                                            <p>(1Q,2022)</p>
                                                            <h3>Mobile Application</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xs-12 right-con wow  fadeInUp animated" data-wow-duration="1.9s" style="visibility: visible; animation-duration: 1.9s; animation-name: fadeInUp;">
                                            <div class="box-con">
                                                <div class="dbox">
                                                    <div class="dleft">
                                                        <div class="content">
                                                            <p>(1Q,2022)</p>
                                                            <h3>API for 15 <br> Exchanges</h3>
                                                        </div>
                                                    </div>
                                                    <div class="dright">
                                                        <div class="imgs"><img src="<?= base_url('assets/frontforpwt/images/video/11.png') ?>" alt="" /></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="serials">
                                        <span class="cre"></span>
                                        <div class="col-sm-6 col-xs-12 left-con wow  fadeInUp animated" data-wow-duration="2.1s" style="visibility: visible; animation-duration: 2.1s; animation-name: fadeInUp;">
                                            <div class="box-con">
                                                <div class="dbox">
                                                    <div class="dleft">
                                                        <div class="imgs"><img src="<?= base_url('assets/frontforpwt/images/video/13.png') ?>" alt="" /></div>
                                                    </div>
                                                    <div class="dright">
                                                        <div class="content">
                                                            <p>(1Q,2022)</p>
                                                            <h3>Memes Token</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xs-12 right-con wow  fadeInUp animated" data-wow-duration="2.1s" style="visibility: visible; animation-duration: 2.1s; animation-name: fadeInUp;">
                                            <div class="box-con">
                                                <div class="dbox">
                                                    <div class="dleft">
                                                        <div class="content">
                                                            <p>(2Q,2022)</p>
                                                            <h3>Listing on top<br>50 Exchanges<br></h3>
                                                        </div>
                                                    </div>
                                                    <div class="dright">
                                                        <div class="imgs"><img src="<?= base_url('assets/frontforpwt/images/video/12.png') ?>" alt="" /></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="years"><span class="con">2022</span></span>
                                    <div class="serials">
                                        <span class="cre"></span>
                                        <div class="col-sm-6 col-xs-12 left-con  wow  fadeInUp animated" data-wow-duration="2.3s" style="visibility: visible; animation-duration: 2.3s; animation-name: fadeInUp;">
                                            <div class="box-con">
                                                <div class="dbox">
                                                    <div class="dleft">
                                                        <div class="imgs"><img src="<?= base_url('assets/frontforpwt/images/video/7.png') ?>" alt="" /></div>
                                                    </div>
                                                    <div class="dright">
                                                        <div class="content">
                                                            <p>(2Q,2022)</p>
                                                            <h3>Indicators and <br>Value Addition</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xs-12 right-con wow  fadeInUp animated" data-wow-duration="2.3s" style="visibility: visible; animation-duration: 2.3s; animation-name: fadeInUp;">
                                            <div class="box-con">
                                                <div class="dbox">
                                                    <div class="dleft">
                                                        <div class="content">
                                                            <p>(3Q,2022)</p>
                                                            <h3>P2P Platform</h3>
                                                        </div>
                                                    </div>
                                                    <div class="dright">
                                                        <div class="imgs"><img src="<?= base_url('assets/frontforpwt/images/video/10.png') ?>" alt="" /></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="serials">
                                        <div class="col-sm-12 col-xs-12 center-con wow  fadeInUp animated" data-wow-duration="2.5s" style="visibility: visible; animation-duration: 2.5s; animation-name: fadeInUp;">
                                            <div class="box-con">
                                                <div class="dbox">
                                                    <div class="dleft">
                                                        <div class="imgs"><img src="<?= base_url('assets/frontforpwt/images/video/15.png') ?>" alt="" /></div>
                                                    </div>
                                                    <div class="dright">
                                                        <div class="content">
                                                            <p>(4Q,2022)</p>
                                                            <h3>8 New Pairs<br>for O.T</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                        </div>
                    </div>
                    </ul>
                </div>
            </div>
    </div>
    </section>
    <div class="sud">
        <svg version="1.1" id="Layer_2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1920 181.1" style="enable-background:new 0 0 1920 181.1;" xml:space="preserve">
            <style type="text/css">
                .st0 {
                    fill-rule: evenodd;
                    clip-rule: evenodd;
                    fill: #10122d;
                }
            </style>
            <g>
                <path class="st0" d="M0,80c0,0,28.9-4.2,43-13c14.3-9,71-35.7,137,5c17.3,7.7,33.3,13,48,11c17.3,0.3,50.3,4.7,66,23
                        c20.3,9.7,68,40.3,134-12c24-11,59-16.3,104,2c21,7.3,85,27.7,117-14c24-30.7,62.7-55,141-12c26,10.3,72,14.7,110-14
                        c37.7-19,89.7-29,122,53c23,32.7,47.7,66.3,97,26c24-22.7,51-78.3,137-38c0,0,28.3,15.7,52,15c23.7-0.7,50.7,4.3,76,41
                        c19.7,19.7,71,36.7,121-2c0,0,22.3-16,55-12c0,0,32.7,6.7,56-71c23.3-76,79-92,122-29c9.3,13.7,25,42,62,43c37,1,51.7,25.3,67,48
                        c15.3,22.7,51,22.7,53,23v28.1H0V80z" />
            </g>
        </svg>
    </div>
</div>
</div>
<div id="token" class="wd_scroll_wrap">
    <section class="tokens-area section">
        <div id="token-js">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="section-heading2">
                            <h2 class="wow  fadeInUp animated" data-wow-duration="1.0s" style="visibility: visible; animation-duration: 1.0s; animation-name: fadeInUp;">Token Distribution</h2>
                        </div>
                    </div>
                    <div class="col-lg-10 col-md-9 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-right wow  fadeInUp animated" data-wow-duration="1.3s" style="visibility: visible; animation-duration: 1.3s; animation-name: fadeInUp;">
                            <div class="tokens mr-lg-l50">
                                <div class="token-name">Total Supply</div>
                                <div class="token-body">
                                    <p>100,000,000,0000,000,000</p>
                                    <button class="left-btn">$0.0.00000000001 </button>
                                    <span class="easypiechart skill-circle">
                                        <span class="percent head-font">80</span>
                                        <br>
                                        <span class="con">Circulation</span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-right wow  fadeInUp animated" data-wow-duration="1.6s" style="visibility: visible; animation-duration: 1.6s; animation-name: fadeInUp;">
                            <div class="tokens mr-lg-r50">
                                <div class="token-name">Distribution</div>
                                <div class="token-body">
                                    <p>10% Burn & 10% Usage Reward</p>
                                    <button class="right-btn">70% burn</button>
                                    <div class="prices">
                                        <h3 class="f-20">Burn Period :</h3>
                                        <table>
                                            <thead>
                                                <tr>
                                                    <th>Time</th>
                                                    <th>Burn</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>60 days</td>
                                                    <td>5%</td>
                                                </tr>
                                                <tr>
                                                    <td>180 Days </td>
                                                    <td>10%</td>
                                                </tr>
                                                <tr>
                                                    <td>365 Days</td>
                                                    <td>15%</td>
                                                </tr>
                                                <tr>
                                                    <td>Each year</td>
                                                    <td>15%</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- <section class="token-details section bg-img">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="section-heading">
                            <h2 class="wow  fadeInUp animated" data-wow-duration="1.0s" style="visibility: visible; animation-duration: 1.0s; animation-name: fadeInUp;">Distribution of Tokens</h2>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2">
                        <div class="tokes-chart-slider wow animate fadeIn" data-wow-duration="1s">
                            <div class="cart-list">
                                <div class="chart col-sm-6">
                                    <div id="e_chart_3" class="" style="height:400px;"></div>
                                    <div class="imgs"><img src="<?= base_url('assets/frontforpwt/images/icons/l.png') ?>" alt="" /></div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="label-chatrs">
                                        <div class="lsits">
                                            <span class="clabels-text">85%</span>
                                            <p>For participants of Pre-Sale and ICO</p>
                                        </div>
                                        <div class="lsits">
                                            <span class="clabels-text">8%</span>
                                            <p>Reserved for the team.</p>
                                        </div>
                                        <div class="lsits">
                                            <span class="clabels-text">4%</span>
                                            <p>Reserved for the consultants.</p>
                                        </div>
                                        <div class="lsits">
                                            <span class="clabels-text">3%</span>
                                            <p>Bounty campaign</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cart-list">
                                <div class="chart col-sm-6">
                                    <div id="e_chart_2" class="" style="height:400px;"></div>
                                    <div class="imgs"><img src="<?= base_url('assets/frontforpwt/images/icons/l.png') ?>" alt="" /></div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="label-chatrs">
                                        <div class="lsits">
                                            <span class="clabels-text">85%</span>
                                            <p>For participants of Pre-Sale and ICO</p>
                                        </div>
                                        <div class="lsits">
                                            <span class="clabels-text">8%</span>
                                            <p>Reserved for the team.</p>
                                        </div>
                                        <div class="lsits">
                                            <span class="clabels-text">4%</span>
                                            <p>Reserved for the consultants.</p>
                                        </div>
                                        <div class="lsits">
                                            <span class="clabels-text">3%</span>
                                            <p>Bounty campaign</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="contents text-center">
                            <h2 class="wow  fadeInUp animated" data-wow-duration="1.3s" style="visibility: visible; animation-duration: 1.3s; animation-name: fadeInUp;">The issued tokens will be distributed as follows:</h2>
                            <a href="#" class="btn1 wow  fadeInUp animated" data-wow-duration="1.6s" style="visibility: visible; animation-duration: 1.6s; animation-name: fadeInUp;">Download WhitePaper</a>
                        </div>
                    </div>
                </div>
            </div>
        </section> -->
    <!-- <div class="sud">
            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1920 181.1" style="enable-background:new 0 0 1920 181.1;" xml:space="preserve">
                <style type="text/css">
                    .st0 {
                        fill-rule: evenodd;
                        clip-rule: evenodd;
                        fill: #10122d;
                    }
                </style>
                <g>
                    <path class="st0" d="M0,80c0,0,28.9-4.2,43-13c14.3-9,71-35.7,137,5c17.3,7.7,33.3,13,48,11c17.3,0.3,50.3,4.7,66,23
                     c20.3,9.7,68,40.3,134-12c24-11,59-16.3,104,2c21,7.3,85,27.7,117-14c24-30.7,62.7-55,141-12c26,10.3,72,14.7,110-14
                     c37.7-19,89.7-29,122,53c23,32.7,47.7,66.3,97,26c24-22.7,51-78.3,137-38c0,0,28.3,15.7,52,15c23.7-0.7,50.7,4.3,76,41
                     c19.7,19.7,71,36.7,121-2c0,0,22.3-16,55-12c0,0,32.7,6.7,56-71c23.3-76,79-92,122-29c9.3,13.7,25,42,62,43c37,1,51.7,25.3,67,48
                     c15.3,22.7,51,22.7,53,23v28.1H0V80z" />
                </g>
            </svg>
        </div> -->
    <section class="team2-area pd-t100 pd-b50 bg-img">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="section-heading">
                        <h2 class="wow  fadeInUp animated" data-wow-duration="1.0s" style="visibility: visible; animation-duration: 1.0s; animation-name: fadeInUp;">Core Team</h2>
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="team-box col-lg-6 col-md-6 col-sm-6 col-xs-12 wow fadeInUp animated" data-wow-duration="1.3s" style="visibility: visible; animation-duration: 1.3s; animation-name: fadeInUp;">
                    <div class="team-list">
                        <div class="conte">
                            <div class="dbox">
                                <div class="dleft">
                                    <img src="<?= base_url('assets/frontforpwt/images/team/1.png') ?>" alt="" />
                                </div>
                                <div class="dright">
                                    <div class="content">
                                        <h3>Ajay Suryavanshi</h3>
                                        <span>(Co-Founder & CEO)</span>
                                        <ul>
                                            <li><a><i class="fab fa-twitter"></i></a></li>
                                            <li><a><i class="fab fa-linkedin"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="con">
                                <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="team-box col-lg-6 col-md-6 col-sm-6 col-xs-12 wow fadeInUp animated" data-wow-duration="1.3s" style="visibility: visible; animation-duration: 1.3s; animation-name: fadeInUp;">
                    <div class="team-list">
                        <div class="conte conte-team">
                            <iframe class="team-youtube" src="https://www.youtube.com/embed/tgbNymZ7vqY" width="100%" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="sud">
        <svg version="1.1" id="Layer_3" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1920 181.1" style="enable-background:new 0 0 1920 181.1;" xml:space="preserve">
            <style type="text/css">
                .st0 {
                    fill-rule: evenodd;
                    clip-rule: evenodd;
                    fill: #10122d;
                }
            </style>
            <g>
                <path class="st0" d="M0,80c0,0,28.9-4.2,43-13c14.3-9,71-35.7,137,5c17.3,7.7,33.3,13,48,11c17.3,0.3,50.3,4.7,66,23
                     c20.3,9.7,68,40.3,134-12c24-11,59-16.3,104,2c21,7.3,85,27.7,117-14c24-30.7,62.7-55,141-12c26,10.3,72,14.7,110-14
                     c37.7-19,89.7-29,122,53c23,32.7,47.7,66.3,97,26c24-22.7,51-78.3,137-38c0,0,28.3,15.7,52,15c23.7-0.7,50.7,4.3,76,41
                     c19.7,19.7,71,36.7,121-2c0,0,22.3-16,55-12c0,0,32.7,6.7,56-71c23.3-76,79-92,122-29c9.3,13.7,25,42,62,43c37,1,51.7,25.3,67,48
                     c15.3,22.7,51,22.7,53,23v28.1H0V80z" />
            </g>
        </svg>
    </div>
</div>

<?php $this->load->view('commonfront/footer');  ?>

<script>
    $('.slider').owlCarousel({
        items: 3,
        singleItem: true,
        nav: true,
        dots: false,
        loop: true,
        autoPlay: false
    });
</script>