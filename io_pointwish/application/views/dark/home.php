<?php $this->load->view('common/header'); ?>
<link rel="stylesheet" href="<?= base_url('assets/front/newthem/css/owl.carousel.min.css') ?>">

<div class="particle_sec">
    <img src="<?= base_url('assets/front/newthem/img/particle/1.png') ?>" class="section__particle top-left" alt="cryptik particles">
    <img src="<?= base_url('assets/front/newthem/img/particle/2.png') ?>" class="section__particle two" alt="cryptik particles">
    <img src="<?= base_url('assets/front/newthem/img/particle/3.png') ?>" class="section__particle top-right" alt="cryptik particles">
    <img src="<?= base_url('assets/front/newthem/img/particle/4.png') ?>" class="section__particle bottom-left" alt="cryptik particles">
    <img src="<?= base_url('assets/front/newthem/img/particle/5.png') ?>" class="section__particle bottom-right" alt="cryptik particles">
</div>

<!-- Start Hero Section -->
<section class="tm-hero-section tm-style1 home-sec tm-bg" id="home">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="tm-hero-text">
                    <h1 class="tm-hero-title tm-f40 tm-lg-f38 mb-2 fw-700 tm-fw-medium tm-m0 col-xs-t-14 tm-md-f30 tm-md-lh40">Point Wish Token is an<br>Amazing Social Trading<br>Platform</h1>
                    <div class="empty-space col-xs-b10"></div>
                    <h2 class="tm-hero-sub-title tm-white-c tm-f18 tm-m0 tm-md-f18 tm-md-lh26">Suitable For Everey Tokens and Coins<br>Of Binance</h2>
                    <div class="empty-space col-xs-b25"></div>
                    <div class="tm-btn-group">
                        <a href="<?= base_url('assets/front/newthem/Pointwish-whitepaper.pdf') ?>" class="btn-home tm-btn tm-style1 pt-08rem" target="_blank">White Paper</a>
                        <!-- <a href="https://www.pinksale.finance/#/launchpad/0xEd2C8897372A6f69737739c63e81156Fbde0b796?chain=BSC" target="_blank" class="pt-1"> -->
                            <!-- <img style="height: 40px;" src="<?= base_url('assets/front/newthem/img/logo/pinksale-white.png') ?>" alt=""> -->
                        </a>
                        <!-- <a href="#" class="btn-home tm-btn tm-style1 tm-with-border" data-toggle="modal" data-target="#tm-login">Trade With Us</a> -->
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="tm-about-img-wrap">
                    <div class="tm-about-img">
                        <img src="<?= base_url('assets/front/newthem/img/home.png') ?>" alt="">
                        <img style="height: 100px" src="<?= base_url('assets/front/newthem/img/logo/favicon.svg?v= ' . rand()) ?>" alt="" class="tm-ethereum">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Hero Section -->

<!-- Trading Partner Section Start -->
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="tm-single-work h-auto box box">
                <a href="https://www.binance.com/en-IN/support/faq/360002502072" target="_blank">
                    <img style="width: 320px;" src="<?= base_url('assets/front/newthem/img/logo/bnb-name.svg?v=' . rand()) ?>" alt="bnb">
                </a>
            </div>
        </div>
    </div>
</div>
<!-- Trading Partner Section End -->

<!-- Media Partner section start -->
<section class="tm-about-wrap" id="media">
    <div class="container">
        <h2 class="text-center mt-5 mb-4">Media Partners</h2>
        <div id="media-partner1" class="owl-carousel owl-theme mb-3">
            <div class="tm-single-work box-media-slider">
                <a>
                    <img src="<?= base_url('assets/front/newthem/img/media1/coinalpha.svg') ?>" alt="media-1">
                </a>
            </div>
            <div class="tm-single-work box-media-slider">
                <a>
                    <img src="<?= base_url('assets/front/newthem/img/media1/coincatapult.svg') ?>" alt="media-2">
                </a>
            </div>
            <div class="tm-single-work box-media-slider">
                <a>
                    <img src="<?= base_url('assets/front/newthem/img/media1/coindiscovery.svg') ?>" alt="media-3">
                </a>
            </div>
            <div class="tm-single-work box-media-slider">
                <a>
                    <img src="<?= base_url('assets/front/newthem/img/media1/coinfair.svg') ?>" alt="media-4">
                </a>
            </div>
            <div class="tm-single-work box-media-slider">
                <a>
                    <img src="<?= base_url('assets/front/newthem/img/media1/coinhunter.svg') ?>" alt="media-5">
                </a>
            </div>
            <div class="tm-single-work box-media-slider">
                <a>
                    <img src="<?= base_url('assets/front/newthem/img/media1/coinscope.svg') ?>" alt="media-6">
                </a>
            </div>
            <div class="tm-single-work box-media-slider">
                <a>
                    <img src="<?= base_url('assets/front/newthem/img/media1/coin-seek.svg') ?>" alt="media-7">
                </a>
            </div>
            <div class="tm-single-work box-media-slider">
                <a>
                    <img src="<?= base_url('assets/front/newthem/img/media1/coinsnipper.svg') ?>" alt="media-8">
                </a>
            </div>
            <div class="tm-single-work box-media-slider">
                <a>
                    <img src="<?= base_url('assets/front/newthem/img/media1/cointoplist.svg') ?>" alt="media-9">
                </a>
            </div>
            <div class="tm-single-work box-media-slider">
                <a>
                    <img src="<?= base_url('assets/front/newthem/img/media1/freshcoins.svg') ?>" alt="media-10">
                </a>
            </div>
            <div class="tm-single-work box-media-slider">
                <a>
                    <img src="<?= base_url('assets/front/newthem/img/media1/gemfinder.svg') ?>" alt="media-11">
                </a>
            </div>
            <div class="tm-single-work box-media-slider">
                <a>
                    <img src="<?= base_url('assets/front/newthem/img/media1/100xcoinhunt.svg') ?>" alt="media-12">
                </a>
            </div>
            <div class="tm-single-work box-media-slider">
                <a>
                    <img src="<?= base_url('assets/front/newthem/img/media1/rugfreecoins.svg') ?>" alt="media-13">
                </a>
            </div>
            <div class="tm-single-work box-media-slider">
                <a>
                    <img src="<?= base_url('assets/front/newthem/img/media1/memecoins.svg') ?>" alt="media-14">
                </a>
            </div>
            <div class="tm-single-work box-media-slider">
                <a>
                    <img src="<?= base_url('assets/front/newthem/img/media1/coinfind.svg') ?>" alt="media-15">
                </a>
            </div>
        </div>
        <div id="media-partner2" class="owl-carousel owl-theme">
            <div class="tm-single-work box-media-slider">
                <a>
                    <img src="<?= base_url('assets/front/newthem/img/media1/cntoken.svg') ?>" alt="media-16">
                </a>
            </div>
            <div class="tm-single-work box-media-slider">
                <a>
                    <img src="<?= base_url('assets/front/newthem/img/media1/watcher-guru.svg') ?>" alt="media-17">
                </a>
            </div>
            <div class="tm-single-work box-media-slider">
                <a>
                    <img src="<?= base_url('assets/front/newthem/img/media1/coinboom.svg') ?>" alt="media-18">
                </a>
            </div>
            <div class="tm-single-work box-media-slider">
                <a>
                    <img src="<?= base_url('assets/front/newthem/img/media1/coindom.svg') ?>" alt="media-19">
                </a>
            </div>
            <div class="tm-single-work box-media-slider">
                <a>
                    <img src="<?= base_url('assets/front/newthem/img/media1/nextcoin.svg') ?>" alt="media-20">
                </a>
            </div>
            <div class="tm-single-work box-media-slider">
                <a>
                    <img src="<?= base_url('assets/front/newthem/img/media1/coinhot.svg') ?>" alt="media-21">
                </a>
            </div>
            <div class="tm-single-work box-media-slider">
                <a>
                    <img src="<?= base_url('assets/front/newthem/img/media1/nomics.svg') ?>" alt="media-22">
                </a>
            </div>
            <div class="tm-single-work box-media-slider">
                <a>
                    <img src="<?= base_url('assets/front/newthem/img/media1/coinsgod.png') ?>" alt="media-23">
                </a>
            </div>
            <div class="tm-single-work box-media-slider">
                <a>
                    <img src="<?= base_url('assets/front/newthem/img/media1/coin-vote.svg') ?>" alt="media-24">
                </a>
            </div>
            <div class="tm-single-work box-media-slider">
                <a>
                    <img src="<?= base_url('assets/front/newthem/img/media1/coinmoonshot.svg') ?>" alt="media-25">
                </a>
            </div>
            <div class="tm-single-work box-media-slider">
                <a>
                    <img src="<?= base_url('assets/front/newthem/img/media1/coin-buster.svg') ?>" alt="media-26">
                </a>
            </div>
            <div class="tm-single-work box-media-slider">
                <a>
                    <img src="<?= base_url('assets/front/newthem/img/media1/coinscout.svg') ?>" alt="media-27">
                </a>
            </div>
            <div class="tm-single-work box-media-slider">
                <a>
                    <img src="<?= base_url('assets/front/newthem/img/media1/coincatch.svg') ?>" alt="media-28">
                </a>
            </div>
            <div class="tm-single-work box-media-slider">
                <a>
                    <img src="<?= base_url('assets/front/newthem/img/media1/coinxhigh.svg') ?>" alt="media-29">
                </a>
            </div>
        </div>
    </div>
</section>
<!-- Media Partner section end -->

<!-- Start About Section -->
<section class="tm-about-wrap" id="about">
    <div class="empty-space col-md-b90 col-xs-b55"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 bg-timer">
                <div class="timer-half-sec">
                    <div class="timer-sec">
                        <!-- <p class="text-center">Pre-Sale will be start on PinkSale,</p> -->
                        <!-- <div class="text-center">
                            <a href="https://www.pinksale.finance/#/launchpad/0xEd2C8897372A6f69737739c63e81156Fbde0b796?chain=BSC" target="_blank" class="pt-1">
                                <img style="height: 55px;" src="<?= base_url('assets/front/newthem/img/logo/pinksale-white.png') ?>" alt="">
                            </a>
                        </div> -->
                        <!-- <div id="countdown-timer">
                            <ul class="timer-ul">
                                <li class="timer-li"><span id="days"></span><span class="timer-text">days</span></li>
                                <li><span class="split">:</span></li>
                                <li class="timer-li"><span id="hours"></span><span class="timer-text">Hours</span></li>
                                <li><span class="split">:</span></li>
                                <li class="timer-li"><span id="mins"></span><span class="timer-text">Minutes</span></li>
                                <li><span class="split">:</span></li>
                                <li class="timer-li"><span id="seconds"></span><span class="timer-text">Seconds</span></li>
                            </ul>
                        </div> -->
                        <!-- <h2 class="text-center mt-3">Pre-Sale is Started <br> on PinkSale</h2> -->
                        <!-- <div class="Box__BoxWrapper-sc-9qo6sy-0 hHLgFu box progressbar-wrapper">
                            <div class="Box__BoxWrapper-sc-9qo6sy-0 hHLgFu box progressbar"><span class="Text__TextWrapper-sc-5ha4qu-0 cOKoiv text">$5,097</span></div><span class="Text__TextWrapper-sc-5ha4qu-0 cOKoiv text">$11,931</span>
                        </div> -->
                    </div>
                </div>
            </div><!-- .col -->

            <div class="col-lg-6">
                <div class="tm-about">
                    <h2 class="tm-m0 tm-lh40 col-xs-t-1 tm-md-f28 tm-md-lh35">What is Point Wish Token Platform?</h2>
                    <div class="empty-space col-md-b20 col-xs-b10"></div>
                    <div class="tm-f18 tm-md-f15 tm-m0 tm-lh28 tm-md-lh20">It is a bot trdaing platform which can be used with Binance exchange API and user can do trading on the exhcnage. Traders must have 20 PWT tokens in their wallet so bot can be active </div>
                    <div class="empty-space col-md-b10 col-xs-b15"></div>
                    <h2 class="tm-m0 tm-lh60 tm-md-f28 tm-md-lh34">Why Bot trading?</h2>
                    <div class="empty-space col-md-b10 col-xs-b10"></div>
                    <div class="tm-about-text tm-f18 tm-md-f15 tm-lh28 tm-md-lh20 col-xs-t-1">
                        <p>Crypto market is working round the clock without holidays,</p>
                        <p>Bot can do better trading then human in all the circumtances and has a good chance to book profit.</p>
                        <p>Algorithm, Indicators and Price based trdaing bot will be delivered best result for the traders.</p>
                    </div>
                </div>
            </div><!-- .col -->
        </div>
    </div>
    <div class="empty-space col-md-b85 col-xs-b55"></div>
</section>
<!-- End About Section -->


<!-- Start How it Works -->
<section class="tm-ocher-work" id="howitworks">
    <div class="empty-space col-md-b85 col-xs-b50"></div>
    <div class="container">
        <div class="tm-section-heading tm-style1">
            <div class="tm-section-heading-left">
                <h2 class="tm-m0 tm-md-f28 tm-md-lh34">Point Wish Token <br>
                    is the game changer</h2>
            </div>
            <div class="tm-section-heading-right">
                <h2 class="col-xs-b20 tm-md-f28 tm-md-lh34">Why?</h2>
                <div class="tm-f18 tm-md-f15 tm-lh28 tm-md-lh24">Traders can use algorithm and set up to 25 different buying and selling point for single coins or tokens, they can also set different strategy for each coins or tokens </div>
            </div>
        </div>
        <div class="empty-space col-md-b75 col-xs-b50"></div>
        <div class="tm-section-heading tm-style2">
            <h2 class="text-center col-xs-b2 col-xs-t-2 tm-md-f28 tm-md-lh34">Key Benifits</h2>
        </div>
        <div class="empty-space col-md-b55 col-xs-b35"></div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-sm-6">
                <div class="tm-single-work">
                    <div class="tm-single-work-icon">
                        <!-- <i class="flaticon-ethereum"></i> -->
                        <img class="bnb-icon" src="<?= base_url('assets/front/newthem/img/bnb/bnb-icon.png') ?>" alt="">
                    </div>
                    <div class="tm-single-work-text tm-f20 tm-md-f18 tm-md-lh28 tm-lh30">BNBBSC <br> Blockchain</div>
                </div>
                <div class="empty-space col-md-b30 col-xs-b30"></div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="tm-single-work">
                    <div class="tm-single-work-icon">
                        <i class="flaticon-strategy"></i>
                    </div>
                    <div class="tm-single-work-text tm-f20 tm-md-f18 tm-md-lh28 tm-lh30">Copy Trade <br> Buy-Sell strategy</div>
                </div>
                <div class="empty-space col-md-b30 col-xs-b30"></div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="tm-single-work">
                    <div class="tm-single-work-icon">
                        <i class="flaticon-block"></i>
                    </div>
                    <div class="tm-single-work-text tm-f20 tm-md-f18 tm-md-lh28 tm-lh30">Email Security <br> For every traders</div>
                </div>
                <div class="empty-space col-md-b30 col-xs-b30"></div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="tm-single-work">
                    <div class="tm-single-work-icon">
                        <i class="flaticon-decentralized"></i>
                    </div>
                    <div class="tm-single-work-text tm-f20 tm-md-f18 tm-md-lh28 tm-lh30">Binance API <br> Trading with bot</div>
                </div>
                <div class="empty-space col-md-b30 col-xs-b30"></div>
            </div>
        </div>
    </div>
    <div class="empty-space col-md-b60 col-xs-b30"></div>
    <div class="tm-section-heading tm-style2">
        <h2 class="text-center col-xs-b2 col-xs-t-1 tm-md-f28 tm-md-lh34">Comparision with others</h2>
    </div>
    <div class="empty-space col-md-b55 col-xs-b35"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="tm-comparison-table text-center">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Traditional</th>
                                    <th>Cryptohopper</th>
                                    <th>PWT</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Fees</td>
                                    <td>$10</td>
                                    <td>$19</td>
                                    <td>Free</td>

                                </tr>
                                <tr>
                                    <td>Strategy</td>
                                    <td>5</td>
                                    <td>20</td>
                                    <td>44</td>
                                </tr>
                                <tr>
                                    <td>Copy trading</td>
                                    <td>No</td>
                                    <td>No</td>
                                    <td>Yes</td>
                                </tr>
                                <tr>
                                    <td>Team trading reward</td>
                                    <td>No</td>
                                    <td>No</td>
                                    <td>Yes</td>
                                </tr>
                                <tr>
                                    <td>Mobile apllication</td>
                                    <td>No</td>
                                    <td>Yes</td>
                                    <td>Yes</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div><!-- .col -->
        </div>
    </div><!-- .container -->
    <div class="empty-space col-md-b95 col-xs-b60"></div>
</section>
<!-- End How it Works -->

<!-- Start Token Sale -->
<section class="tm-token-sale tm-light-blue-bg" id="tokensale">
    <div class="empty-space col-md-b85 col-xs-b45"></div>
    <div class="container">
        <div class="tm-section-heading tm-style1 tm-color2">
            <div class="tm-section-heading-left">
                <h2 class="tm-m0 tm-light-blue-c tm-md-f28 tm-md-lh34">What <br>
                    is a token sale?</h2>
            </div>
            <div class="tm-section-heading-right">
                <div class="tm-f18 tm-md-f15 tm-lh28 tm-white-c tm-md-lh24">A token sale, sometimes referred to as an IDO, is form of crowd funding for digital currency-related projects. In token sales, purch - asers buy units of a new currency in exchange for legal tender or other digital currencies, such as BNB.</div>
            </div>
        </div>
        <div class="empty-space col-md-b75 col-xs-b50"></div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xl-6">
                <h2 class="text-center col-xs-b2 col-xs-t-2 tm-light-blue-c tm-md-f28 tm-md-lh34">Token Distribution</h2>
                <div class="empty-space col-md-b55 col-xs-b35"></div>
                <div class="tm-token-distribution col-xs-b1">
                    <div class="tm-token-distribution-left">
                        <ul class="tm-token-distribution-list tm-f20 tm-md-f15 tm-white-c">
                            <li><span class="tm-blank tm-token-color1"></span><span class="tm-percentage tm-light-blue-c">50%</span> <span class="flex-xsm-100">IDO supporters</span></li>
                            <li><span class="tm-blank tm-token-color2"></span><span class="tm-percentage tm-light-blue-c">20%</span> <span class="flex-xsm-100">Team members</span></li>
                            <li><span class="tm-blank tm-token-color3"></span><span class="tm-percentage tm-light-blue-c">15%</span> <span class="flex-xsm-100">Marketing</span></li>
                            <li><span class="tm-blank tm-token-color4"></span><span class="tm-percentage tm-light-blue-c">10%</span> <span class="flex-xsm-100">Advisors</span></li>
                            <li><span class="tm-blank tm-token-color5"></span><span class="tm-percentage tm-light-blue-c">05%</span> <span class="flex-xsm-100">Bounty Fund</span></li>
                        </ul>
                    </div>
                    <div class="tm-token-distribution-right tm-sp-token-distribution">
                        <img src="<?= base_url('assets/front/newthem/img/token-percentage01.png') ?> " alt="" class="tm-token-distribution-img">
                    </div>
                </div>
                <div class="empty-space col-lg-b0 col-xs-b55"></div>
            </div><!-- .col -->
            <div class="col-xl-6">
                <h2 class="text-center col-xs-b2 col-xs-t-2 tm-light-blue-c tm-md-f28 tm-md-lh34">Token Fund Usage</h2>
                <div class="empty-space col-md-b55 col-xs-b35"></div>
                <div class="tm-token-distribution col-xs-b1">
                    <div class="tm-token-distribution-left">
                        <ul class="tm-token-distribution-list tm-f20 tm-md-f15 tm-white-c">
                            <li><span class="tm-blank tm-token-color1"></span><span class="tm-percentage tm-light-blue-c">40%</span> <span class="flex-xsm-100">Platform development</span></li>
                            <li><span class="tm-blank tm-token-color5"></span><span class="tm-percentage tm-light-blue-c">25%</span> <span class="flex-xsm-100">Sales & marketing</span></li>
                            <li><span class="tm-blank tm-token-color4"></span><span class="tm-percentage tm-light-blue-c">20%</span> <span class="flex-xsm-100">Operational costs</span></li>
                            <li><span class="tm-blank tm-token-color2"></span><span class="tm-percentage tm-light-blue-c">10%</span> <span class="flex-xsm-100">Legal expenses</span></li>
                            <li><span class="tm-blank tm-token-color3"></span><span class="tm-percentage tm-light-blue-c">05%</span> <span class="flex-xsm-100">Strategy development</span></li>
                        </ul>
                    </div>
                    <div class="tm-token-distribution-right">
                        <img src="<?= base_url('assets/front/newthem/img/token-percentage02.png') ?> " alt="" class="tm-token-distribution-img">
                    </div>
                </div>
            </div><!-- .col -->
        </div>
    </div>
    <div class="empty-space col-md-b95 col-xs-b60"></div>
    <div class="tm-dark-blue-bg tm-bounty" id="bounty">
        <div class="empty-space col-md-b90 col-xs-b55"></div>
        <div class="container">
            <h2 class="text-center col-xs-b2 tm-light-blue-c col-xs-t-1 tm-md-f28 tm-md-lh34 d-xsm-grid">Bounty Fund Distribution <span>(<span class="tm-white-c">5%</span> of the total)</span></h2>
            <div class="empty-space col-md-b55 col-xs-b30"></div>
            <div class="tm-token-distribution">
                <div class="tm-token-distribution-left">
                    <ul class="tm-token-distribution-list tm-f20 tm-md-f15 tm-white-c">
                        <li><span class="tm-blank tm-token-color1"></span><span class="tm-percentage tm-light-blue-c">25%</span> Twitter bounty campaing</li>
                        <li><span class="tm-blank tm-token-color6"></span><span class="tm-percentage tm-light-blue-c">25%</span> Telegram bounty campaing</li>
                        <li><span class="tm-blank tm-token-color5"></span><span class="tm-percentage tm-light-blue-c">20%</span> Facebook bounty campaing</li>
                        <li><span class="tm-blank tm-token-color2"></span><span class="tm-percentage tm-light-blue-c">15%</span> Membership bounty campaing</li>
                        <li><span class="tm-blank tm-token-color7"></span><span class="tm-percentage tm-light-blue-c">10%</span> Authors bounty campaing</li>
                        <li><span class="tm-blank tm-token-color8"></span><span class="tm-percentage tm-light-blue-c">05%</span> Reserve</li>
                    </ul>
                    <div class="empty-space col-lg-b0 col-xs-b40"></div>
                </div>
                <div class="tm-token-distribution-right tm-sp-token-distribution col-xs-b1">
                    <img src="<?= base_url('assets/front/newthem/img/banner/status_bar.png') ?>" alt="" class="tm-token-distribution-img">
                </div>
            </div>
        </div><!-- .container -->
        <div class="empty-space col-md-b95 col-xs-b60"></div>
    </div>
</section>
<!-- End Token Sale -->

<!-- Start Road Map Section -->
<section class="tm-roadmap-wrap tm-gray-bg tm-bg" id="roadmap">
    <div class="empty-space col-md-b90 col-xs-b55"></div>
    <div class='particle-network-animation'></div>
    <div class="container">
        <div class="tm-section-heading tm-style2 text-center col-xs-t-1">
            <h2 class="tm-f34 col-xs-b2 tm-md-f28 tm-md-lh34">Roadmap</h2>
            <div class="empty-space col-md-b25 col-xs-b10"></div>
            <div class="tm-f20 tm-md-f15 col-xs-t-4 col-xs-b1">We have big plans for the future of Request.</div>
        </div>
    </div>
    <div class="empty-space col-md-b85 col-xs-b45"></div>
    <div class="container">
        <div class="tm-green-roadmap">
            <div class="green-slider-nav-wrap">
                <div class="slider green-slider-nav">
                    <div>
                        <div class="tm-green-year"><span>2022</span></div>
                    </div>
                    <div>
                        <div class="tm-green-year"><span>2023</span></div>
                    </div>
                    <div>
                        <div class="tm-green-year"><span>2024</span></div>
                    </div>
                </div>
            </div>
            <div class="slider green-slider-for">
                <div>
                    <div class="tm-green-roadmap-text">
                        <div class="tm-green-roadmap-text-in">
                            <div class="tm-gr-single-text">
                                <div class="tm-gr-yr">January<span class="tm-gr-ball"></span></div>
                                <div class="tm-gr-text">Launch of the project</div>
                            </div>
                            <div class="tm-gr-single-text">
                                <div class="tm-gr-yr">March<span class="tm-gr-ball"></span></div>
                                <div class="tm-gr-text">BOT 2.0 with additional features</div>
                            </div>
                            <div class="tm-gr-single-text">
                                <div class="tm-gr-yr">June<span class="tm-gr-ball"></span></div>
                                <div class="tm-gr-text">3 new exchanges for trading</div>
                            </div>
                            <div class="tm-gr-single-text">
                                <div class="tm-gr-yr">September<span class="tm-gr-ball"></span></div>
                                <div class="tm-gr-text">BTC trading pair will be added</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="tm-green-roadmap-text">
                        <div class="tm-green-roadmap-text-in">
                            <div class="tm-gr-single-text">
                                <div class="tm-gr-yr">March<span class="tm-gr-ball"></span></div>
                                <div class="tm-gr-text">15 new exchanges for trading</div>
                            </div>
                            <div class="tm-gr-single-text">
                                <div class="tm-gr-yr">June<span class="tm-gr-ball"></span></div>
                                <div class="tm-gr-text">PWT will be on one of the top 25 exchange</div>
                            </div>
                            <div class="tm-gr-single-text">
                                <div class="tm-gr-yr">September<span class="tm-gr-ball"></span></div>
                                <div class="tm-gr-text">Bot will be work with DEX</div>
                            </div>
                            <div class="tm-gr-single-text">
                                <div class="tm-gr-yr">Octobor<span class="tm-gr-ball"></span></div>
                                <div class="tm-gr-text">Payment Gateway system will be announced</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="tm-green-roadmap-text">
                        <div class="tm-green-roadmap-text-in">
                            <div class="tm-gr-single-text">
                                <div class="tm-gr-yr">June<span class="tm-gr-ball"></span></div>
                                <div class="tm-gr-text">ALFA version of the Blockchain</div>
                            </div>
                            <div class="tm-gr-single-text">
                                <div class="tm-gr-yr">September<span class="tm-gr-ball"></span></div>
                                <div class="tm-gr-text">Multichain NFT platform</div>
                            </div>
                            <div class="tm-gr-single-text">
                                <div class="tm-gr-yr">Octobor<span class="tm-gr-ball"></span></div>
                                <div class="tm-gr-text">Online e-commerce platform</div>
                            </div>
                            <div class="tm-gr-single-text">
                                <div class="tm-gr-yr">November<span class="tm-gr-ball"></span></div>
                                <div class="tm-gr-text">Major partnerships in blockchain industry</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="empty-space col-md-b95 col-xs-b60"></div>
</section>
<!-- End Road Map Section -->

<!-- Start Team Section -->
<section class="tm-member team-bg tm-bg" id="team">
    <div class="tm-team-wrap">
        <div class="empty-space col-md-b90 col-xs-b55"></div>
        <h2 class="text-center col-xs-b2 tm-light-blue-c col-xs-t-1 tm-md-f28 tm-md-lh34">Meet Our Team</h2>
        <div class="empty-space col-md-b55 col-xs-b35"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="tm-member-card text-center">
                        <div class="tm-member-card-in">
                            <div class="tm-member-thumb"><img src="<?= base_url('assets/front/newthem/img/team/02.png') ?>" alt=""></div>
                            <div class="tm-member-desc">
                                <h3 class="tm-member-name tm-f20 tm-md-f15 col-xs-b5">Lasse Clausen</h3>
                                <div class="tm-member-designation tm-f18">Founder & CEO</div>
                            </div>
                        </div>
                        <div class="empty-space col-md-b35 col-xs-b25"></div>
                        <ul class="tm-member-social-btn">
                            <li><a href="mailto: support@pointwish.io" class="tm-social-btn"><span><i class="fa fa-envelope"></i></span></a></li>
                        </ul>
                    </div>
                    <div class="empty-space col-md-b30 col-xs-b30"></div>
                </div><!-- .col -->
                <div class="col-lg-4 col-md-6">
                    <div class="tm-member-card text-center">
                        <div class="tm-member-card-in">
                            <div class="tm-member-thumb"><img src="<?= base_url('assets/front/newthem/img/team/01.png') ?>" alt=""></div>
                            <div class="tm-member-desc">
                                <h3 class="tm-member-name tm-f20 tm-md-f15 col-xs-b5">George Alexiev</h3>
                                <div class="tm-member-designation tm-f18">Senior Developer</div>
                            </div>
                        </div>
                        <div class="empty-space col-md-b35 col-xs-b25"></div>
                        <ul class="tm-member-social-btn">
                            <li><a href="http://www.linkedin.com/in/george-alexiev-355036232" target="_blank" class="tm-social-btn"><span><i class="fa fa-linkedin"></i></span></a></li>
                        </ul>
                    </div>
                    <div class="empty-space col-md-b30 col-xs-b30"></div>
                </div><!-- .col -->
                <div class="col-lg-4 col-md-6">
                    <div class="tm-member-card text-center">
                        <div class="tm-member-card-in">
                            <div class="tm-member-thumb"><img src="<?= base_url('assets/front/newthem/img/team/03.png') ?>" alt=""></div>
                            <div class="tm-member-desc">
                                <h3 class="tm-member-name tm-f20 tm-md-f15 col-xs-b5">Oscar Machado</h3>
                                <div class="tm-member-designation tm-f18">CMO</div>
                            </div>
                        </div>
                        <div class="empty-space col-md-b35 col-xs-b25"></div>
                        <ul class="tm-member-social-btn">
                            <li><a href="https://t.me/pointwishtoken" target="_blank" class="tm-social-btn"><span><i class="fa fa-telegram"></i></span></a></li>
                        </ul>
                    </div>
                    <div class="empty-space col-md-b30 col-xs-b30"></div>
                </div><!-- .col -->
            </div>
        </div>
    </div><!-- .tm-team-wrap -->
</section>
<!-- End Team Section -->

<!-- Start Token Seciton -->
<div class="tm-token-wrap token-bg tm-bg" id="featured-company">
    <div class="empty-space col-md-b95 col-xs-b60"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="tm-token">
                    <h2 class="tm-token-heading tm-m0 tm-md-f28 tm-md-lh34">Token Listed</h2>
                    <div class="tm-token-list">
                        <div class="tm-single-token">
                            <a>
                                <!-- <img class="w-175" src="<?= base_url('assets/front/newthem/img/logo/pinksale.png') ?>" alt=""> -->
                            </a>
                        </div>
                    </div>
                </div>
                <div class="tm-token-bar">
                    <span class="tm-bar-circle"></span>
                    <span class="tm-bar-circle"></span>
                </div>
                <div class="tm-token">
                    <h2 class="tm-token-heading tm-m0 tm-md-f28 tm-md-lh34">Featured On</h2>
                    <div class="tm-token-list">
                        <div class="tm-single-token">
                            <a href="https://coinmarketcap.com/" target="_blank">
                                <img class="w-185" src="<?= base_url('assets/front/newthem/img/logo/coinmarket.svg') ?>" alt="">
                            </a>
                        </div>
                        <div class="tm-single-token">
                            <a href="https://www.coingecko.com/en" target="_blank">
                                <img class="w-185" src="<?= base_url('assets/front/newthem/img/logo/coingecko.svg') ?>" alt="">
                            </a>
                        </div>
                        <div class="tm-single-token">
                            <a href="https://bscscan.com/" target="_blank">
                                <img class="w-185" src="<?= base_url('assets/front/newthem/img/logo/bsc-scan.svg') ?>" alt="">
                            </a>
                        </div>
                    </div>
                </div><!-- .tm-token -->
            </div>
        </div>
    </div>
    <div class="empty-space col-md-b95 col-xs-b60"></div>
</div>
<!-- End Token Seciton -->

<!-- Start FAQ Section -->
<section class="tm-faq-wrap tm-bg faq-bg" id="faq">
    <div class="empty-space col-md-b90 col-xs-b55"></div>
    <h2 class="text-center col-xs-b2 tm-light-blue-c col-xs-t-1 tm-md-f28 tm-md-lh34">Frequently Asked Questions</h2>
    <div class="empty-space col-md-b55 col-xs-b35"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="tabs animated-fade">
                    <ul class="tab-links">
                        <li class="active"><a href="#top-question">Top Questions</a></li>
                        <li><a href="#company">Point Wish Bot</a></li>
                        <li><a href="#product">Trading Rewards</a></li>
                    </ul>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="tab-content">
                                <div id="top-question" class="tab active">
                                    <div class="tm-faq">
                                        <div class="accordian-wrapper">
                                            <div class="single-accordian">
                                                <h3 class="accordian-head">What is Point Wish bot ?</h3>
                                                <div class="accordian-body">Point Wish Bot is a trading bot which is working with Binance and do trading 24x7 for the traders. Coins or Tokens can be slected by the traders along with self made strategy.</div>
                                            </div>
                                            <div class="single-accordian">
                                                <h3 class="accordian-head">What is bot activation fees ?</h3>
                                                <div class="accordian-body">We are not chargeing any yearly or mmonthly fees to activate bot but users must have 20 PWT tokens in their account to start trading on our platform.</div>
                                            </div>
                                            <div class="single-accordian">
                                                <h3 class="accordian-head">How can I share my stratagy with other users ?</h3>
                                                <div class="accordian-body">Copy trade function will help you in that and to become an "Expert" trader must have 100 trades on the platform using Point Wish Bot. Anyone can sell thir strategy on our platform.</div>
                                            </div>
                                            <div class="single-accordian">
                                                <h3 class="accordian-head">How to secure my account ?</h3>
                                                <div class="accordian-body">We are providing 2FA security for all of our users and anyone can activate "Email Verification" to secure their account with the system.</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="company" class="tab">
                                    <div class="tm-faq">
                                        <div class="accordian-wrapper">
                                            <div class="single-accordian">
                                                <h3 class="accordian-head">How it can work for me ?</h3>
                                                <div class="accordian-body">Point Wish Bot is a algorithm based trading bot who will do trading on Binance exchange 24x7, user can set up to 20 unique buy and sell points for each coins or tokens.</div>
                                            </div>
                                            <div class="single-accordian">
                                                <h3 class="accordian-head">How to earn maximum profit ?</h3>
                                                <div class="accordian-body">Trades can do trading as per their skill and knowledge or they can follow "Expert" traders and copy their trading stratgey to earn more. Also professional; traders can earn extra with selling their strategy.</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div id="product" class="tab">
                                    <div class="tm-faq">
                                        <div class="accordian-wrapper">
                                            <div class="single-accordian">
                                                <h3 class="accordian-head">What is team trading reward ?</h3>
                                                <div class="accordian-body">Any users who invite other users on our platform will join under affiliate programe. User will get trdaing profit % from the all user who join under him/her.</div>
                                            </div>
                                            <div class="single-accordian">
                                                <h3 class="accordian-head">How much team trdaing reward a user can earn ?</h3>
                                                <div class="accordian-body">User can earn from 4% to 16% as per their team size more users will bring more profit.</div>
                                            </div>
                                            <div class="single-accordian">
                                                <h3 class="accordian-head">How can I join others under my team ?</h3>
                                                <div class="accordian-body">Users have touse their sponserid at the time of signup to add others as a team.</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="text-center">
                                <img class="img-faq-sec" src="<?= base_url('assets/front/newthem/img/banner/faq-img.png?v= ' . rand()) ?>" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- .col -->
        </div>
    </div>
    <div class="empty-space col-md-b85 col-xs-b50"></div>
</section>
<!-- End FAQ Section -->
<?php $this->load->view('common/footer'); ?>
<script src="<?= base_url('assets/front/newthem/js/owl.carousel.js') ?>"></script>

<script>
    $('#media-partner1').owlCarousel({
        loop: true,
        margin: 30,
        dots: false,
        nav: false,
        responsive: {
            0: {
                items: 1,
            },
            768: {
                items: 2,
            },
            992: {
                items: 3,
            },
            1200: {
                items: 4,
            }
        },
        slideBy: 4,
        autoplay: true,
        autoplayTimeout: 3000
    });
    $('#media-partner2').owlCarousel({
        loop: true,
        margin: 30,
        dots: false,
        nav: false,
        responsive: {
            0: {
                items: 1,
            },
            768: {
                items: 2,
            },
            992: {
                items: 3,
            },
            1200: {
                items: 4,
            }
        },
        slideBy: 4,
        autoplay: true,
        autoplayTimeout: 3000
    })
</script>