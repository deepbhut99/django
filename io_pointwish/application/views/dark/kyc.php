<?php $this->load->view('common/header_dark');  ?>

<!--============= Header Section Ends Here =============-->
<section class="page-header bg_img oh" data-background="<?= base_url('assets/front/dark/images/page-header.png') ?>">
    <div class="bottom-shape d-none d-md-block">
        <img src="<?= base_url('assets/front/dark/css/img/page-header.png') ?>" alt="css">
    </div>
    <div class="page-left-thumb">
        <!-- <img src="<?= base_url('assets/front/images/bg/privacy-header.png') ?>" alt="bg"> -->
    </div>
    <div class="container">
        <div class="page-header-content cl-white">
            <h1 class="title">AML & KYC Policy</h1>
        </div>
    </div>
</section>
<!--============= Header Section Ends Here =============-->



<!--============= Privacy Section Starts Here =============-->
<section class="privacy-section padding-top padding-bottom">
    <div class="container">
        <div class="row justify-content-center">            
        </div>
<?php $this->load->view('common/sidebar_dark');  ?>
            <div class="col-lg-9 col-xl-9 col-md-8">
                <form class="hdmob">
                    <select class="fa form-control " onchange="window.location='<?php echo base_url(); ?>page/rules/'+this.value" style="background-color: #ebecec; color: black; font-size: 16px; border: 1px solid rgba(255, 255, 255, 0.3); border-radius: 1.25rem; height: calc(2.25rem + 20px); font-family: 'Poppins', sans-serif; ">

                        <!--<option class="fa" style="font-family: 'Poppins', sans-serif;">Select</option>-->
                        <option value="terms" style="font-family: 'Poppins', sans-serif;">Terms & Condition</option>
                        <option value="privacy_policy" style="font-family: 'Poppins', sans-serif;"> Privcy Policy</option>
                        <option value="risk_disclosure" style="font-family: 'Poppins', sans-serif;"> Disclaimer</option>
                        <option value="return_refund" style="font-family: 'Poppins', sans-serif;"> Return & Refund Policy</option>
                        <option value="cookies_policy" style="font-family: 'Poppins', sans-serif;"> Cookies Policy</option>
                        <option selected value="AML_KYC" style="font-family: 'Poppins', sans-serif;"> AML & KYC Policy</option>
                        <option value="demo_trading" style="font-family: 'Poppins', sans-serif;"> Demo & Trading Account</option>
                        <option value="payment_policy" style="font-family: 'Poppins', sans-serif;"> Payment Policy</option>
                    </select>
                    <br><br>
                </form>
                <article class="mt-70 mt-lg-0">                    
                    <p class="pb-20 text-justify">It is the Policy of earnfinex.com and its affiliates (hereinafter referred to as “the Company”) to prohibit and actively pursue the prevention of money laundering and any activity that facilitates money laundering or the funding of terrorist or criminal activities. The Company requires its officers, employees, and affiliates to adhere to these standards in preventing the use of its products and services for money laundering purposes.
                    </p>
                    <p class="pb-20 text-justify">For the purposes of this Policy, money laundering is generally defined as engaging in acts designed to conceal or disguise the true origins of criminally derived proceeds so that the unlawful proceeds appear to have been derived from legitimate origins or constitute legitimate assets.
                    </p>
                    <p class="pb-20 text-justify">Generally, money laundering occurs in three stages. Cash first enters the financial system at the “placement” stage, where the cash generated from criminal activities is converted into monetary instruments, such as money orders or traveler’s checks, or deposited into accounts at financial institutions. At the “layering” stage, the funds are transferred or moved into other accounts or other financial institutions to further separate the money from its criminal origin. At the “integration” stage, the funds are reintroduced into the economy and used to purchase legitimate assets or to fund other criminal activities or legitimate businesses. Terrorist financing may not involve the proceeds of criminal conduct, but rather an attempt to conceal the origin or intended use of the funds, which will later be used for criminal purposes.
                    </p>
                    <p class="pb-20 text-justify">Each employee of the Company, whose duties are associated with the provision of products and services of the Company and who directly or indirectly deals with the clientele of the Company, is expected to know the requirements of the applicable laws and regulations which affect his or her job responsibilities, and it shall be the affirmative duty of such employee to carry out these responsibilities at all times in a manner that complies with the requirements of the relevant laws and regulations.
                    </p>
                    <p class="pb-20 text-justify">The laws and regulations include, but not limited to: “Customer Due Diligence for Banks” (2001) and “General Guide to Account Opening and Customer Identification” (2003) of Basel Committee on Banking Supervision, Forty + nine Recommendations for Money Laundering of FATF, USA Patriot Act (2001), Prevention and Suppression of Money Laundering Activities Law (1996).
                    </p>
                    <p class="pb-20 text-justify">To ensure that this general policy is carried out, management of the Company has established and maintains an ongoing program for the purpose of assuring compliance with the relevant laws and regulations and the prevention of money laundering. This program seeks to coordinate the specific regulatory requirements throughout the group within a consolidated framework in order to effectively manage the group’s risk of exposure to money laundering and terrorist financing across all business units, functions, and legal entities.
                    </p>
                    <p class="pb-20 text-justify">Each of the affiliates of the Company is required to comply with AML and KYC policies.
                    </p>
                    <p class="pb-20 text-justify">All identification documents and services records shall be kept for the minimum period of time required by local law.
                    </p>
                    <p class="pb-20 text-justify">All new employees shall receive anti money laundering training as part of the mandatory new-hire training program. All applicable employees are also required to complete AML and KYC training annually. Participation in additional targeted training programs is required for all employees with day-to-day AML and KYC responsibilities.
                    </p>
                </article>
            </div>
        </div>
    </div>
</section>
<!--============= Privacy Section Ends Here =============-->
<?php $this->load->view('common/footer_dark');  ?>