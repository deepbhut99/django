<?php $this->load->view('common/header_dark');  ?>

<!--============= Header Section Ends Here =============-->
<section class="page-header single-header bg_img oh" data-background="<?= base_url('assets/front/dark/images/bg/bg-social-1.jpg') ?>">
    <div class="bottom-shape d-none d-md-block">
        <img src="<?= base_url('assets/front/dark/css/img/page-header.png') ?>" alt="css">
    </div>
</section>
<!--============= Header Section Ends Here =============-->



<!--============= Contact Section Starts Here =============-->
<section class="contact-section padding-top padding-bottom">
    <div class="container">
        <div class="section-header mw-100 cl-white">
            <h1 class="title text-black">&nbsp;</h1>
            <!-- <p>Please check our article on the following news channels.</p> -->
        </div>
    </div>
</section>
<section class="partner-section padding-top padding-bottom oh">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-8 col-lg-9">
                <div class="section-header mw-100 mt-5 mt-xsm-n6">
                    <h5 class="cate">Our Media Partners</h5>
                    <p>Please check our article on the following news channels.</p>
                    <!-- <h2 class="title">They trust us... would you?</h2> -->
                    <!-- <p>
                        Our partners are some of the most reputable lenders on the market. Join their ranks and cooperate with us to increase your profit.
                    </p> -->
                </div>
            </div>
        </div>
        
        <div class="row justify-content-center mb-5">
            <div class="col-sm-6 col-md-3 col-lg-2 pr-sm-0">
                <div class="partner-item bg-white">
                    <a href="https://apnews.com/press-release/globe-newswire/be63b203c6c5416629861fb328c821b8" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/ap_news.svg') ?>" alt="AP News">
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-2 pr-sm-0">
                <div class="partner-item-2 bg-white">
                    <a href="https://www.streetinsider.com/Globe+Newswire/Earnfinex+%E2%80%93+the+Smart+A.I-based+trading+platform+where+you+earn+while+playing./18634194.html" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/streetinsider.svg') ?>" alt="Street Insider">
                    </a>
                </div>
                <div class="partner-item-2 bg-white">
                    <a href="https://www.globenewswire.com/news-release/2021/07/01/2256811/0/en/Earnfinex-the-Smart-A-I-based-trading-platform-where-you-earn-while-playing.html" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/intrado.svg') ?>" alt="Intrado">
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-2 pr-sm-0">
                <div class="partner-item-2 bg-white">
                    <a href="https://www.marketwatch.com/press-release/earnfinex---the-smart-ai-based-trading-platform-where-you-earn-while-playing-2021-07-01" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/market_watch.svg') ?>" alt="Market Watch">
                    </a>
                </div>
                <div class="partner-item-2 bg-white">
                    <a href="https://www.morningstar.com/news/globe-newswire/8274183/earnfinex-the-smart-ai-based-trading-platform-where-you-earn-while-playing" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/morningstar.svg') ?>" alt="Morningstar">
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-2 pr-sm-0">
                <div class="partner-item-2 bg-white">
                    <a href="https://menafn.com/1102377987/Earnfinex-the-Smart-AI-based-trading-platform-where-you-earn-while-playing" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/menafn.svg') ?>" alt="Menafn">
                    </a>
                </div>
                <div class="partner-item-2 bg-white">
                    <a href="https://www.benzinga.com/pressreleases/21/07/g21816100/earnfinex-the-smart-a-i-based-trading-platform-where-you-earn-while-playings" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/benzinga.svg') ?>" alt="Benzinga">
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-2 pr-sm-0">
                <div class="partner-item-2 bg-white">
                    <a href="https://www.canadianinsider.com/earnfinex-the-smart-a-i-based-trading-platform-where-you-earn-while-playing-" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/canadian_insider.svg') ?>" alt="Canadian Insider">
                    </a>
                </div>
                <div class="partner-item-2 bg-white">
                    <a href="https://m.insidertracking.com/earnfinex-the-smart-a-i-based-trading-platform-where-you-earn-while-playing-" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/insider_tracking.svg') ?>" alt="Insider Tracking">
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-2 pr-sm-0">
                <div class="partner-item bg-white">
                    <a href="https://www.marketscreener.com/news/latest/Earnfinex-ndash-the-Smart-A-I-based-trading-platform-where-you-earn-while-playing--35770858/" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/market_screener.svg') ?>" alt="Market Screener">
                    </a>
                </div>
            </div>
        </div>

        <div class="row justify-content-center mb-5">
            <div class="col-sm-12 col-md-12 col-lg-2 pr-sm-0">
            </div>
            <div class="col-sm-6 col-md-3 col-lg-2 pr-sm-0">
                <div class="partner-item-2 bg-white">
                    <a href="https://finance.yahoo.com/finance/earnfinex-smart-based-trading-platform-192000137.html" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/yahoo_finance.svg') ?>" alt="Yahoo Finance">
                    </a>
                </div>
                <div class="partner-item-2 bg-white">
                    <a href="https://ca.finance.yahoo.com/earnfinex-smart-based-trading-platform-192000137.html" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/finance_ca.svg') ?>" alt="Yahoo Finance CA">
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-2 pr-sm-0">
                <div class="partner-item-2 bg-white">
                    <a href="https://uk.finance.yahoo.com/news/earnfinex-smart-based-trading-platform-192000137.html" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/finance_uk.svg') ?>" alt="Yahoo Finance UK">
                    </a>
                </div>
                <div class="partner-item-2 bg-white">
                    <a href="https://nz.finance.yahoo.com/earnfinex-smart-based-trading-platform-192000137.html" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/finance_nz.svg') ?>" alt="Yahoo Finance NZ">
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-2 pr-sm-0">
                <div class="partner-item-2 bg-white">
                    <a href="https://au.news.yahoo.com/earnfinex-smart-based-trading-platform-192000137.html" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/yahoo_au.svg') ?>" alt="Yahoo News AU">
                    </a>
                </div>
                <div class="partner-item-2 bg-white">
                    <a href="https://nz.news.yahoo.com/earnfinex-smart-based-trading-platform-192000137.html" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/yahoo_nz.svg') ?>" alt="Yahoo News NZ">
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-2 pr-sm-0">
                <div class="partner-item-2 bg-white">
                    <a href="https://www.yahoo.com/entertainment/earnfinex-smart-based-trading-platform-192000137.html" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/yahoo_entertainment.svg') ?>" alt="Yahoo Entertainment">
                    </a>
                </div>
                <div class="partner-item-2 bg-white">
                    <a href="https://www.yahoo.com/lifestyle/earnfinex-smart-based-trading-platform-192000137.html" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/yahoo-life.svg') ?>" alt="Yahoo Life">
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-2 pr-sm-0">
            </div>
        </div>

        <div class="row justify-content-center mb-5">
            <div class="col-sm-6 col-md-3 col-lg-2 pr-sm-0">
                <div class="partner-item bg-white">
                    <a href="http://quotes.fatpitchfinancials.com/fatpitch.financials/news/read/41534753" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/fat_pitch.svg') ?>" alt="Fat Pitch Financials">
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-2 pr-sm-0">
                <div class="partner-item-2 bg-white">
                    <a href="http://finance.azcentral.com/azcentral/news/read/41534753/" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/az_central.svg') ?>" alt="AZ Centtral">
                    </a>
                </div>
                <div class="partner-item-2 bg-white">
                    <a href="http://stocks.newsok.com/newsok/news/read/41534753" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/newsok.svg') ?>" alt="News Ok">
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-2 pr-sm-0">
                <div class="partner-item-2 bg-white">
                    <a href="http://finance.dailyherald.com/dailyherald/news/read/41534753/" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/daily_herald.svg') ?>" alt="Daily Herald">
                    </a>
                </div>
                <div class="partner-item-2 bg-white">
                    <a href="http://finance.minyanville.com/minyanville/news/read/41534753" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/minyanviile.svg') ?>" alt="Minyanville">
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-2 pr-sm-0">
                <div class="partner-item-2 bg-white">
                    <a href="http://money.mymotherlode.com/clarkebroadcasting.mymotherlode/news/read/41534753/" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/mother_lode.svg') ?>" alt="Mother Lode">
                    </a>
                </div>
                <div class="partner-item-2 bg-white">
                    <a href="http://business.starkvilledailynews.com/starkvilledailynews/markets/news/read/41534753/" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/starkville.svg') ?>" alt="Stark Ville">
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-2 pr-sm-0">
                <div class="partner-item-2 bg-white">
                    <a href="http://business.bigspringherald.com/bigspringherald/markets/news/read/41534753/" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/herald.svg') ?>" alt="Herald">
                    </a>
                </div>
                <div class="partner-item-2 bg-white pl-1">
                    <a href="http://business.bentoncourier.com/bentoncourier/markets/news/read/41534753/" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/saline_courier.svg') ?>" alt="Saline Courier">
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-2 pr-sm-0">
                <div class="partner-item bg-white">
                    <a href="https://markets.financialcontent.com/ask/news/read/41534732">
                        <img src="<?= base_url('assets/front/dark/images/media/ask.svg') ?>" alt="Ask">
                    </a>
                </div>
            </div>
        </div>

        <div class="row justify-content-center mb-5">
            <div class="col-sm-12 col-md-12 col-lg-2 pr-sm-0">
            </div>
            <div class="col-sm-6 col-md-3 col-lg-2 pr-sm-0">
                <div class="partner-item-2 bg-white">
                    <a href="http://business.times-online.com/times-online/markets/news/read/41534753/" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/times_record.svg') ?>" alt="Times Record">
                    </a>
                </div>
                <div class="partner-item-2 bg-white">
                    <a href="http://business.guymondailyherald.com/guymondailyherald/markets/news/read/41534753/" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/guymen_herald.svg') ?>" alt="Guymen Herald">
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-2 pr-sm-0">
                <div class="partner-item-2 bg-white">
                    <a href="http://business.theantlersamerican.com/theantlersamerican/markets/news/read/41534753/" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/antlers_american.svg') ?>" alt="Antlers American">
                    </a>
                </div>
                <div class="partner-item-2 bg-white">
                    <a href="http://business.minstercommunitypost.com/minstercommunitypost/markets/news/read/41534753/" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/community_post.svg') ?>" alt="Community Post">
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-2 pr-sm-0">
                <div class="partner-item-2 bg-white">
                    <a href="http://business.decaturdailydemocrat.com/decaturdailydemocrat/markets/news/read/41534753/" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/daily_democrat.svg') ?>" alt="Daily Democrat">
                    </a>
                </div>
                <div class="partner-item-2 bg-white">
                    <a href="http://business.dptribune.com/dptribune/news/read/41534753" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/deer_park.png') ?>" alt="Deer Park Tribune">
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-2 pr-sm-0">
                <div class="partner-item-2 bg-white">
                    <a href="http://business.thepostandmail.com/thepostandmail/markets/news/read/41534753/" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/post_mail.svg') ?>" alt="Post Mail">
                    </a>
                </div>
                <div class="partner-item-2 bg-white">
                    <a href="http://markets.chroniclejournal.com/chroniclejournal/news/read/41534753" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/chronical_journal.svg') ?>" alt="chronical journal">
                    </a>
                </div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-2 pr-sm-0">
            </div>
        </div>

        <div class="row justify-content-center mb-5">
            <div class="col-sm-6 col-md-3 col-lg-2 pr-sm-0">
                <div class="partner-item-2 bg-white">
                    <a href="https://www.fox21delmarva.com/story/44227866/earnfinex-the-smart-ai-based-trading-platform-where-you-earn-while-playing" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/04/fox21.png') ?>" alt="Fox 21">
                    </a>
                </div>
                <div class="partner-item-2 bg-white">
                    <a href="https://www.rfdtv.com/story/44227866/earnfinex-the-smart-ai-based-trading-platform-where-you-earn-while-playing" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/04/rfd_tv.svg') ?>" alt="RFD TV">
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-2 pr-sm-0">
                <div class="partner-item-2 bg-white">
                    <a href="https://www.wboc.com/story/44227866/earnfinex-the-smart-ai-based-trading-platform-where-you-earn-while-playing" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/04/wboc.svg') ?>" alt="WBOC">
                    </a>
                </div>
                <div class="partner-item-2 bg-white">
                    <a href="https://www.wfxg.com/story/44227866/earnfinex-the-smart-ai-based-trading-platform-where-you-earn-while-playing" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/04/fox54.png') ?>" alt="FOX 54" style="max-height: 50px">
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-2 pr-sm-0">
                <div class="partner-item-2 bg-white">
                    <a href="https://www.wicz.com/story/44227866/earnfinex-the-smart-ai-based-trading-platform-where-you-earn-while-playing" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/04/fox40.png') ?>" alt="FOX 40">
                    </a>
                </div>
                <div class="partner-item-2 bg-white">
                    <a href="https://www.snntv.com/story/44227866/earnfinex-the-smart-ai-based-trading-platform-where-you-earn-while-playing" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/04/snn_news.png') ?>" alt="SNN News">
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-2 pr-sm-0">
                <div class="partner-item-2 bg-white">
                    <a href="https://www.wtnzfox43.com/story/44227866/earnfinex-the-smart-ai-based-trading-platform-where-you-earn-while-playing" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/04/fox43.png') ?>" alt="FOX 43" style="max-height: 48px;">
                    </a>
                </div>
                <div class="partner-item-2 bg-white">
                    <a href="https://www.wdfxfox34.com/story/44227866/earnfinex-the-smart-ai-based-trading-platform-where-you-earn-while-playing" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/04/fox34.png') ?>" alt="FOX 34">
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-2 pr-sm-0">
                <div class="partner-item-2 bg-white">
                    <a href="https://www.wpgxfox28.com/story/44227866/earnfinex-the-smart-ai-based-trading-platform-where-you-earn-while-playing" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/04/fox28.png') ?>" alt="FOX 28">
                    </a>
                </div>
                <div class="partner-item-2 bg-white">
                    <a href="https://www.wrde.com/story/44227866/earnfinex-the-smart-ai-based-trading-platform-where-you-earn-while-playing" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/04/wrde.png') ?>" alt="WRDE Cost" style="max-height: 50px;">
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-2 pr-sm-0">
                <div class="partner-item bg-white">
                    <a href="https://www.htv10.tv/story/44227866/earnfinex-the-smart-ai-based-trading-platform-where-you-earn-while-playing" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/04/htv_10.png') ?>" alt="HTV 10" style="max-height: 50px">
                    </a>
                </div>
            </div>
        </div>

        <div class="row justify-content-center mb-5">
            <div class="col-sm-6 col-md-3 col-lg-2 pr-sm-0">
                <div class="partner-item-2 bg-white">
                    <a href="https://markets.financialcontent.com/bostonherald/news/read/41534732" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/05/boston_herald.svg') ?>" alt="boston herald">
                    </a>
                </div>
                <div class="partner-item-2 bg-white">
                    <a href="https://www.thecowboychannel.com/story/44228135/earnfinex-the-smart-ai-based-trading-platform-where-you-earn-while-playing" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/05/cowboy.png') ?>" alt="Cowboy Channel">
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-2 pr-sm-0">
                <div class="partner-item-2 bg-white">
                    <a href="https://markets.financialcontent.com/kelownadailycourier/news/read/41534732" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/05/daily_courier.svg') ?>" alt="daily courier">
                    </a>
                </div>
                <div class="partner-item-2 bg-white">
                    <a href="http://business.dailytimesleader.com/dailytimesleader/news/read/41534732/Earnfinex_" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/05/daily_times_leader.svg') ?>" alt="daily times leader">
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-2 pr-sm-0">
                <div class="partner-item-2 bg-white">
                    <a href="https://www.einpresswire.com/article/545274574/earnfinex-the-smart-a-i-based-trading-platform-where-you-earn-while-playing" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/05/ein_presswire.svg') ?>" alt="ein presswire">
                    </a>
                </div>
                <div class="partner-item-2 bg-white">
                    <a href="https://markets.financialcontent.com/jsonline/news/read/41534732/earnfinex_" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/05/milwaukee.svg') ?>" alt="milwaukee">
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-2 pr-sm-0">
                <div class="partner-item-2 bg-white">
                    <a href="https://panhandle.newschannelnebraska.com/story/44228135/earnfinex-the-smart-ai-ba" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/05/ncn_news.svg') ?>" alt="ncn news">
                    </a>
                </div>
                <div class="partner-item-2 bg-white">
                    <a href="https://markets.financialcontent.com/pentictonherald/news/read/41534732" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/05/penticton_herald.svg') ?>" alt="penticton herald">
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-2 pr-sm-0">
                <div class="partner-item-2 bg-white">
                    <a href="https://markets.financialcontent.com/gatehouse.rrstar/news/read/41534732/earnfinex_" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/05/rrstar.svg') ?>" alt="rrstar">
                    </a>
                </div>
                <div class="partner-item-2 bg-white">
                    <a href="https://markets.financialcontent.com/sandiego/news/read/41534732" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/05/san_union_tribune.svg') ?>" alt="San Union Tribune">
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-2 pr-sm-0">
                <div class="partner-item-2 bg-white">
                    <a href="https://markets.financialcontent.com/startribune/news/read/41534732/earnfinex_" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/05/star_tribune.svg') ?>" alt="Star Tribune">
                    </a>
                </div>
                <div class="partner-item-2 bg-white">
                    <a href="https://markets.financialcontent.com/townhall/news/read/41534732" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/05/townhall.svg') ?>" alt="Townhall">
                    </a>
                </div>
            </div>
        </div>
        
        <div class="row justify-content-center">
            <div class="col-sm-6 col-md-3 col-lg-2 pr-sm-0">
                <div class="partner-item bg-white">
                    <a href="https://centurylink.net/finance/category/press/article/prdistribution-2021-7-1-earnfinex-the-smart-ai-based-trading-platform-where-you-earn-while-playing" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/03/century_link.svg') ?>" alt="Century Link">
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-2 pr-sm-0">
                <div class="partner-item-2 bg-white">
                    <a href="https://portal.truvista.net/finance/category/press/article/prdistribution-2021-7-1-earnfinex-the-smart-ai-based-trading-platform-where-you-earn-while-playing" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/03/truvista.svg') ?>" alt="truvista">
                    </a>
                </div>
                <div class="partner-item-2 bg-white">
                    <a href="https://www.armstrongmywire.com/finance/category/press/article/prdistribution-2021-7-1-earnfinex-the-smart-ai-based-trading-platform-where-you-earn-while-playing" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/03/armstrong.svg') ?>" alt="armstrong">
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-2 pr-sm-0">
                <div class="partner-item-2 bg-white">
                    <a href="https://home.sparklight.com/finance/category/press/article/prdistribution-2021-7-1-earnfinex-the-smart-ai-based-trading-platform-where-you-earn-while-playing" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/03/sparklight.svg') ?>" alt="sparklight">
                    </a>
                </div>
                <div class="partner-item-2 bg-white">
                    <a href="https://hawaiiantel.net/finance/category/press/article/prdistribution-2021-7-1-earnfinex-the-smart-ai-based-trading-platform-where-you-earn-while-playing" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/03/hawaiian_telcom.svg') ?>" alt="hawaiian telcom">
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-2 pr-sm-0">
                <div class="partner-item-2 bg-white">
                    <a href="https://mediacomtoday.com/finance/category/press/article/prdistribution-2021-7-1-earnfinex-the-smart-ai-based-trading-platform-where-you-earn-while-playing" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/03/media.svg') ?>" alt="Media Com">
                    </a>
                </div>
                <div class="partner-item-2 bg-white">
                    <a href="https://mybendbroadband.com/finance/category/press/article/prdistribution-2021-7-1-earnfinex-the-smart-ai-based-trading-platform-where-you-earn-while-playing" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/03/bendbroadband.svg') ?>" alt="bendbroadband">
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-2 pr-sm-0">
                <div class="partner-item-2 bg-white">
                    <a href="https://home.suddenlink.net/finance/category/press/article/prdistribution-2021-7-1-earnfinex-the-smart-ai-based-trading-platform-where-you-earn-while-playing" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/03/suddenlink.svg') ?>" alt="Sudden Link">
                    </a>
                </div>
                <div class="partner-item-2 bg-white">
                    <a href="https://aperture.synacor.net/finance/category/press/article/prdistribution-2021-7-1-earnfinex-the-smart-ai-based-trading-platform-where-you-earn-while-playing" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/03/synacor.svg') ?>" alt="synacor">
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-2 pr-sm-0">
                <div class="partner-item bg-white">
                    <a href="https://myworld.atlanticbb.com/finance/category/press/article/prdistribution-2021-7-1-earnfinex-the-smart-ai-based-trading-platform-where-you-earn-while-playing" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/03/altantic_broadband.svg') ?>" alt="altantic broadband">
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-2 pr-sm-0">
                <div class="partner-item bg-white">
                    <a href="https://portal.tds.net/finance/category/press/article/prdistribution-2021-6-4-mare-island-dock-of-bay-music-festival" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/03/tds_telcom.svg') ?>" alt="TDS Telecom">
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-2 pr-sm-0">
                <div class="partner-item bg-white">
                    <a href="https://wowway.net/finance/category/press/article/prdistribution-2021-7-1-earnfinex-the-smart-ai-based-trading-platform-where-you-earn-while-playing" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/03/wow.svg') ?>" alt="WOW">
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-2 pr-sm-0">
                <div class="partner-item bg-white">
                    <a href="https://my.gvtc.com/finance/category/press/article/prdistribution-2021-7-1-earnfinex-the-smart-ai-based-trading-platform-where-you-earn-while-playing" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/03/gvtc.svg') ?>" alt="GVTC" style="max-height: 100px;">
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-2 pr-sm-0">
                <div class="partner-item bg-white">
                    <a href="https://portal.mygrande.com/finance/category/press/article/prdistribution-2021-7-1-earnfinex-the-smart-ai-based-trading-platform-where-you-earn-while-playing" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/03/grande.svg') ?>" alt="Grande">
                    </a>
                </div>
            </div>
        </div>
        
        <div class="row justify-content-center mt-xsm-70">
            <div class="col-xl-8 col-lg-9">
                <div class="section-header mw-100 mt-5 mt-xsm-n6">
                    <h5 class="cate">Our Advertisement Partners</h5>
                </div>
            </div>
        </div>
        
        <div class="row justify-content-center mb-5">
            <div class="col-sm-6 col-md-3 col-lg-2 pr-sm-0">
                <div class="partner-item bg-white">
                    <a href="https://coinzilla.com/advertisers/" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/coinzilla.svg') ?>" alt="Coinzilla">
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-2 pr-sm-0">
                <div class="partner-item bg-white">
                    <a href="https://www.coinpayu.com/" target="_blank">
                        <img src="<?= base_url('assets/front/dark/images/media/coinpayu.svg') ?>" alt="Coin Payu" style="max-height: 100px;">
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!--============= Contact Section Ends Here =============-->

<?php $this->load->view('common/footer_dark');  ?>

<script>
    // Lazy Load
    (function(a) {
        a.fn.lazyload = function(b) {
            var c = {
                threshold: 0,
                failurelimit: 0,
                event: "scroll",
                effect: "show",
                container: window
            };
            if (b) {
                a.extend(c, b)
            }
            var d = this;
            if ("scroll" == c.event) {
                a(c.container).bind("scroll", function(b) {
                    var e = 0;
                    d.each(function() {
                        if (a.abovethetop(this, c) || a.leftofbegin(this, c)) {} else if (!a.belowthefold(this, c) && !a.rightoffold(this, c)) {
                            a(this).trigger("appear")
                        } else {
                            if (e++ > c.failurelimit) {
                                return false
                            }
                        }
                    });
                    var f = a.grep(d, function(a) {
                        return !a.loaded
                    });
                    d = a(f)
                })
            }
            this.each(function() {
                var b = this;
                if (undefined == a(b).attr("original")) {
                    a(b).attr("original", a(b).attr("src"))
                }
                if ("scroll" != c.event || undefined == a(b).attr("src") || c.placeholder == a(b).attr("src") || a.abovethetop(b, c) || a.leftofbegin(b, c) || a.belowthefold(b, c) || a.rightoffold(b, c)) {
                    if (c.placeholder) {
                        a(b).attr("src", c.placeholder)
                    } else {
                        a(b).removeAttr("src")
                    }
                    b.loaded = false
                } else {
                    b.loaded = true
                }
                a(b).one("appear", function() {
                    if (!this.loaded) {
                        a("<img />").bind("load", function() {
                            a(b).hide().attr("src", a(b).attr("original"))[c.effect](c.effectspeed);
                            b.loaded = true
                        }).attr("src", a(b).attr("original"))
                    }
                });
                if ("scroll" != c.event) {
                    a(b).bind(c.event, function(c) {
                        if (!b.loaded) {
                            a(b).trigger("appear")
                        }
                    })
                }
            });
            a(c.container).trigger(c.event);
            return this
        };
        a.belowthefold = function(b, c) {
            if (c.container === undefined || c.container === window) {
                var d = a(window).height() + a(window).scrollTop()
            } else {
                var d = a(c.container).offset().top + a(c.container).height()
            }
            return d <= a(b).offset().top - c.threshold
        };
        a.rightoffold = function(b, c) {
            if (c.container === undefined || c.container === window) {
                var d = a(window).width() + a(window).scrollLeft()
            } else {
                var d = a(c.container).offset().left + a(c.container).width()
            }
            return d <= a(b).offset().left - c.threshold
        };
        a.abovethetop = function(b, c) {
            if (c.container === undefined || c.container === window) {
                var d = a(window).scrollTop()
            } else {
                var d = a(c.container).offset().top
            }
            return d >= a(b).offset().top + c.threshold + a(b).height()
        };
        a.leftofbegin = function(b, c) {
            if (c.container === undefined || c.container === window) {
                var d = a(window).scrollLeft()
            } else {
                var d = a(c.container).offset().left
            }
            return d >= a(b).offset().left + c.threshold + a(b).width()
        };
        a.extend(a.expr[":"], {
            "below-the-fold": "$.belowthefold(a, {threshold : 0, container: window})",
            "above-the-fold": "!$.belowthefold(a, {threshold : 0, container: window})",
            "right-of-fold": "$.rightoffold(a, {threshold : 0, container: window})",
            "left-of-fold": "!$.rightoffold(a, {threshold : 0, container: window})"
        })
    })(jQuery);
    $(function() {
        $("img").lazyload({
            placeholder: "https://lh5.googleusercontent.com/-NkR_NSweopU/V8OktAG8VxI/AAAAAAAAD2c/xPXdM_PM-OA9VBqDK7pf0qgV1FMHJpsqQCK4B/s1600/startuan.gif",
            effect: "fadeIn",
            threshold: "-50"
        })
    });
</script>