<?php $this->load->view('common/header_dark');  ?>

<!--============= Header Section Ends Here =============-->
<section class="page-header bg_img oh" data-background="<?= base_url('assets/front/dark/images/page-header.png') ?>">
    <div class="bottom-shape d-none d-md-block">
        <img src="<?= base_url('assets/front/dark/css/img/page-header.png') ?>" alt="css">
    </div>
    <div class="page-left-thumb">
        <!-- <img src="<?= base_url('assets/front/images/bg/privacy-header.png') ?>" alt="bg"> -->
    </div>
    <div class="container">
        <div class="page-header-content cl-white">
            <h1 class="title">Payment Policy</h1>
        </div>
    </div>
</section>
<!--============= Header Section Ends Here =============-->



<!--============= Privacy Section Starts Here =============-->
<section class="privacy-section padding-top padding-bottom">
    <div class="container">
        <div class="row justify-content-center">            
        </div>
        <?php $this->load->view('common/sidebar_dark');  ?>
        <div class="col-lg-9 col-xl-9 col-md-8">
            <form class="hdmob">
                <select class="fa form-control " onchange="window.location='<?php echo base_url(); ?>page/rules/'+this.value" style="background-color: #ebecec; color: black; font-size: 16px; border: 1px solid rgba(255, 255, 255, 0.3); border-radius: 1.25rem; height: calc(2.25rem + 20px); font-family: 'Poppins', sans-serif; ">

                    <!--<option class="fa" style="font-family: 'Poppins', sans-serif;">Select</option>-->
                    <option value="terms" style="font-family: 'Poppins', sans-serif;">Terms & Condition</option>
                    <option value="privacy_policy" style="font-family: 'Poppins', sans-serif;"> Privcy Policy</option>
                    <option value="risk_disclosure" style="font-family: 'Poppins', sans-serif;"> Disclaimer</option>
                    <option value="return_refund" style="font-family: 'Poppins', sans-serif;"> Return & Refund Policy</option>
                    <option value="cookies_policy" style="font-family: 'Poppins', sans-serif;"> Cookies Policy</option>
                    <option value="AML_KYC" style="font-family: 'Poppins', sans-serif;"> AML & KYC Policy</option>
                    <option value="demo_trading" style="font-family: 'Poppins', sans-serif;"> Demo & Trading Account</option>
                    <option selected value="payment_policy" style="font-family: 'Poppins', sans-serif;"> Payment Policy</option>
                </select>
                <br><br>
            </form>
            <article class="mt-70 mt-lg-0">               
                <h4 class="title text-uppercase pb-30" style="text-align: left;"><b>Payment Policy</b></h4>
                <p class="pb-20 text-justify">The Company is financially responsible for the Client's account balance at any moment.
                </p>
                <p class="pb-20 text-justify">The Company’s financial responsibility starts with the first record about the Client’s deposit and continues up to the full withdrawal of the funds.
                </p>
                <p class="pb-20 text-justify">The Client has the right to demand from the Company any amount of funds which is available in their account at the time of the inquiry.
                </p>
                <p class="pb-20 text-justify">The only official deposit/withdrawal methods are the methods that appear on the Company’s official Website. The Client is taking all the risks related to the use of the payment methods unless these payment methods are provided by the
                    Company’s partners or are the Company’s responsibility. The Company isn’t responsible for any delay or cancellation of financial transactions that may be caused by the selected payment system. In case where the Client has any claims
                    related to any of the payment systems, it’s their responsibility to contact the support service of the payment system and to notify the Company of their claims.
                </p>
                <p class="pb-20 text-justify">The Company shall not be responsible for the activities of any third-party service providers that the Client may use in order to make any deposit/withdrawal. The Company’s financial responsibility for the Client’s funds starts when
                    the funds arrive to the Company’s bank account or any other account related to the Company and this fact appears on the Payment Methods page of the Website. In case any fraud appears during the financial transaction or after it,
                    the Company reserves the right to cancel the transaction and to freeze the Client's account.
                </p>
                <p class="pb-20 text-justify">The Company’s responsibility as to the Client’s funds ends when the funds leave the Company’s bank account or any other account related to the Company and this fact appears on the Payment Methods page of the Website.
                </p>
                <p class="pb-20 text-justify">In case of any technical errors that may appear when processing the financial transaction, the Company reserves the right to cancel that transaction and all other Client’s financial activity on the Company’s Website.
                </p>
                <h4 class="title text-uppercase pb-30" style="text-align: left;"><b>Client’s Registration</b></h4>
                <p class="pb-20 text-justify">Client’s registration procedure is based on two main steps:
                    <br>• Client’s web registration.
                    <br>• Client’s identity verification.
                </p>
                <p class="pb-20 text-justify">To complete the first step, the Client must:
                    <br>• Provide the Company their real identity and contact details.
                    <br>• Accept the Company’s agreement and submit an application.
                </p>
                <p class="pb-20 text-justify">To complete the second step, the Client must:
                    <br>• Provide a full copy of their Passport and/or (if available) ID card with a photo and personal details.
                </p>
                <p class="pb-20 text-justify">The Company reserves the right to demand from the Client other documents, such as paying bills, bank confirmation, bank card scans or any other document that may be necessary during the verification process.
                </p>
                <p class="pb-20 text-justify">The identification process shall be completed in 10 business days since the Company's request to identify the Client. In some cases, the Company may increase the identification process up to 30 working days.
                </p>
                <h4 class="title text-uppercase pb-30" style="text-align: left;"><b>Depositing Procedure</b></h4>
                <p class="pb-20 text-justify">To make a deposit, the Client shall make an inquiry from their personal profile. To complete the inquiry, the Client shall choose any of the payment methods from the list, fill in all the required details and proceed to the payment
                    page.
                </p>
                <p class="pb-20 text-justify">The processing time of the inquiry depends on the selected payment method and may vary from one method to another. The Company cannot regulate the processing time. In case of using electronic payment methods, the transaction time can
                    vary from seconds to days. In case of using direct bank wire, the transaction time can be up to 45 business days.
                </p>
                <h4 class="title text-uppercase pb-30" style="text-align: left;"><b>Taxes</b></h4>
                <p class="pb-20 text-justify">The Company is not a tax agent and therefore shall not provide financial information on the Client to any third parties. This information shall not be disclosed unless officially requested by government authorities.
                </p>
                <h4 class="title text-uppercase pb-30" style="text-align: left;"><b>One-Click Payments</b></h4>
                <p class="pb-20 text-justify">You agree to pay for all goods and/or services or other additional services you ordered on the Website, as well as for any additional expenses (if any), including, but not limited to, all possible taxes, charges, etc. You bear full
                    responsibility for timely payments on the Website. The Payment Service Provider only facilitates payments for the amounts indicated on the Website and it is not responsible for the payment of any additional fees/expenses by the
                    Website user. After you click the “Pay” button, the transaction is deemed to be irrevocably processed and executed. After clicking the “Pay” button, you agree that you will not be eligible to cancel the payment or request to cancel
                    it.
                </p>
                <p class="pb-20 text-justify">By placing the order on the Website, you confirm and state that you do not violate the laws of any country. Also, by accepting these Regulations (and/or Terms & Conditions), you, as a cardholder, confirm that you are entitled to use
                    the Services offered on the Website. When using the Website services, you make a legally binding declaration that you have reached or surpassed the legal age that is considered as such by your jurisdiction for using the Services
                    provided on the Website. By starting to use the Website Services, including those of specific nature, you bear legal responsibility for violating the legislation of any country where these Services are being used and confirm that
                    the Payment Service Provider is not responsible for any such unlawful or unauthorized violation. By agreeing to use the Website Services, you understand and accept that processing of any of your payments is executed by the Payment
                    Service Provider and there is no statutory right of revocation of already purchased goods and/or services or any other opportunities to cancel the payment.
                </p>
                <p class="pb-20 text-justify">If you wish to reject to use Services for your next purchases of goods and/or services or other facilities on the Website, you can do that by using your personal account/profile on the Website. The Payment Service Provider is not responsible
                    for any failure to process the data related to your payment card, or for the issuing bank’s refusal to provide authorization for the payment with your payment card.
                </p>
                <p class="pb-20 text-justify">The Payment Service Provider is not responsible for the quality, quantity, price, terms or conditions of any goods and/or services or other facilities offered to you or purchased by you from the Website by using your payment card.
                    When you pay for any of the Website’s goods and/or services, you are primarily bound by the Website Terms and Conditions. Please note that only you, as the cardholder, are responsible for paying for all goods and/or services you
                    have ordered on the Website and for any additional expenses/fees that can be applied to this payment. The Payment Service Provider acts only as the executor of the payment in the amount stated on the Website and it is not responsible
                    for pricing, total prices and/or total sums. In case there is a situation when you do not agree with the terms and conditions and/or other reasons, we ask you not to proceed with the payment and, if necessary, contact directly
                    the Administration/Support Team of the Website.
                </p>
            </article>
        </div>
    </div>
    </div>
</section>
<!--============= Privacy Section Ends Here =============-->
<?php $this->load->view('common/footer_dark');  ?>