<?php $this->load->view('commonfront/header') ?>
<div class="wd_scroll_wrap">
    <section class="about-area privacy-section">
        <div class="container">
            <div>
                <h1 class="privacy-heading-text"><?php echo ($title) ?></h1>
            </div>
        </div>
    </section>
    <!--About area end here-->
    <div class="sud">
        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1920 181.1" style="enable-background:new 0 0 1920 181.1;" xml:space="preserve">
            <style type="text/css">
                .st0 {
                    fill-rule: evenodd;
                    clip-rule: evenodd;
                    fill: #10122d;
                }
            </style>
            <g>
                <path class="st0" d="M0,80c0,0,28.9-4.2,43-13c14.3-9,71-35.7,137,5c17.3,7.7,33.3,13,48,11c17.3,0.3,50.3,4.7,66,23
                     c20.3,9.7,68,40.3,134-12c24-11,59-16.3,104,2c21,7.3,85,27.7,117-14c24-30.7,62.7-55,141-12c26,10.3,72,14.7,110-14
                     c37.7-19,89.7-29,122,53c23,32.7,47.7,66.3,97,26c24-22.7,51-78.3,137-38c0,0,28.3,15.7,52,15c23.7-0.7,50.7,4.3,76,41
                     c19.7,19.7,71,36.7,121-2c0,0,22.3-16,55-12c0,0,32.7,6.7,56-71c23.3-76,79-92,122-29c9.3,13.7,25,42,62,43c37,1,51.7,25.3,67,48
                     c15.3,22.7,51,22.7,53,23v28.1H0V80z" />
            </g>
        </svg>
    </div>
    <div class="wd_scroll_wrap">
        <section class="features-area section">
            <div class="container">
                <h4 class="title text-uppercase pb-30 text-white">general rules</h4>
                <p class="pb-20 text-justify">pwt values customers’ personal information highly and complies with laws, including the Act on Promotion of Information and Communications Network Utilization and Information Protection, and other relative laws regarding the Personal Information Protection Act, and take such action to protect customer’s personal information.</p>
                <p class="pb-20 text-justify">“Processing Personal Information” means a guideline that the Company needs to comply with in order to let the customers use the services with confidence by protecting the customer’s personal information, which the company highly values.</p>
                <p class="pb-20 text-justify">This processing of personal information is applied to the company’s website <a href="https://www.pwt.io">(https://www.pwt.io)</a> service. Separate privacy policies may apply to services provided on other websites.</p>
                <p class="pb-20 text-justify">The company will notify you through the website announcement (or individual notice) when revising the personal information processing policy. Users are urged to check back frequently when visiting our site.</p>

                <h4 class="title text-uppercase pb-30 text-white pt-10">PURPOSE OF COLLECTING AND USING PERSONAL INFORMATION</h4>
                <p class="pb-20 text-justify">The company may process personal information for the following purposes. The processed personal information will not be used for any purpose other than the following purposes and will be subject to a prior agreement if the purpose
                    of the use is changed.
                </p>
                <p class="pb-20 text-justify">• Verification of a user’s identity, his/her willingness to obtain membership, his/her willingness to withdrawal membership and managing members.
                </p>
                <p class="pb-20 text-justify">• Preventing and sanctioning acts that interfere with the smooth operation of the service including restrictions on use and illegal use of members in violation of laws and regulations and Company’s Terms of Use, preventing theft of
                    accounts and fraudulent transactions.
                </p>
                <p class="pb-20 text-justify">• Provide notices such as amendment of the terms of use, preserve records for dispute settlement, process complaint, protect user and service operation.
                </p>
                <p class="pb-20 text-justify">• Authentication for payment and withdrawal.
                </p>
                <p class="pb-20 text-justify">• Providing services, providing content, and providing personalized services.
                </p>
                <p class="pb-20 text-justify">• Establishing a service utilization environment that users can use with confidence in terms of security, privacy, and safety.
                </p>
                <p class="pb-20 text-justify">• Data to provide personalized services such as service usage record and access frequency analysis, statistics on service utilization, customized service according to service analysis and statistics, and advertisement.
                </p>
                <p class="pb-20 text-justify">• Inform any upcoming events, marketing, advertisement, etc.
                </p>
                <p class="pb-20 text-justify">• Providing information upon a request made by proper and legal investigation on hacking/fraud and other performance of duty by law.
                </p>
                <h4 class="title text-uppercase pb-30 text-white" style="text-align: left;"><b>List of collecting
                        personal information and method of collecting</b></h4>
                <p class="pb-20 text-justify">The Company collects minimum personal information that customer needs to use the services of the Company. Customer may refuse to consent to collection or use of personal information if one does not wish to do so. However, if you do
                    not agree to the collection and use of essential items in the collected personal information items, there may be restrictions on the use of some functions.
                </p>
                <p class="pb-20 text-justify">The personal information that the company collects from the user is as follows.
                </p>
                <p class="pb-20 text-justify"><b>1 . Registration</b>
                    <br>&nbsp;&nbsp;&nbsp;&nbsp;• E-mail, Password
                    <br>&nbsp;&nbsp;&nbsp;&nbsp;• Providing exchange/wallet service
                    <br>&nbsp;&nbsp;&nbsp;&nbsp;• Duplicated registration information (DI), and identification information of the same person(CI)
                </p>
                <p class="pb-20 text-justify">Information that is automatically generated during the process of using the service or processing the business: service usage history, access logs, cookies, access IP information, payment history.
                </p>
                <p class="pb-20 text-justify"><b>2 . Confirmation identity</b>
                    <br>&nbsp;&nbsp;&nbsp;&nbsp;• Copy of identification (make blank all other information except date of birth)
                </p>
                <p class="pb-20 text-justify"><b>3 . Others</b>
                </p>
                <p class="pb-20 text-justify">Company will not collect sensitive personal information that may infringe human rights (such as race, ethnicity, ideology, creed, place of birth, homeland, political orientation and criminal record, health status and sex life), and
                    Company will obtain prior consent when the Company has to collect above information.
                </p>
                <p class="pb-20 text-justify">In any case, the company will not use the information that the customer input for any other purpose other than the purpose stated above, or leak.
                </p>
                <p class="pb-20 text-justify">The Company collects personal information through the following methods:
                </p>
                <p class="pb-20 text-justify">The company collects personal information which the customer, themselves, input and consent to when signing up and using services (Consent of legal representative is necessary when collecting personal information of children under
                    14 years old).
                </p>
                <p class="pb-20 text-justify">Personal information of users can be collected through web pages, e-mail, fax, and telephone during the consultation process through the Customer Centre.
                </p>
                <p class="pb-20 text-justify">Personal information can be collected in written form at events, seminars, etc. held offline.
                </p>
                <p class="pb-20 text-justify">Generation information such as device information can be automatically generated and collected during the process of using PC web or mobile web (access IP information, cookie, service usage log, access log).
                </p>
                <h4 class="title text-uppercase pb-30 text-white" style="text-align: left;"><b>Retention and usage
                        period of the collected personal information</b></h4>
                <p class="pb-20 text-justify">If the Company collects personal information of the user, the retention period is until the cancellation (including withdrawal application, withdrawal of directorship) after signing up. Also, at the time of termination, the Company
                    will destroy the personal information of the user and instruct the third party to destroy the personal information that is provided to the third party.
                </p>
                <p class="pb-20 text-justify">However, if there is a need to preserve by the laws and regulations such as commercial law, Company may retain transaction details and minimum basic information during the preservation period prescribed by laws and regulations and
                    notify customers regarding this period in advance, and the company may preserve the customer’s personal information if the period of retention has not elapsed or if the company receive customer’s consent individually.
                </p>
                <p class="pb-20 text-justify">1) Records of consumer complaints or disputes
                    <br>• Retention reason: consumer protection act in electronic commerce
                    <br>• Retention period: 3 years
                </p>
                <p class="pb-20 text-justify">2) Records on the collection, processing and use of credit information
                    <br>• Retention reason: Act on the Use and Protection of Credit Information
                    <br>• Retention period: 3 years
                </p>
                <p class="pb-20 text-justify">3) Record of payment and goods supply
                    <br>• Retention reason: Consumer Protection Act in Electronic Commerce
                    <br>• Retention period: 5 years
                </p>
                <p class="pb-20 text-justify">4) Record of Log In
                    <br>• Retention reason: Protection of communications secrets Act
                    <br>• Retention period: 3 months
                </p>
                <p class="pb-20 text-justify">5) Company internal policy to prevent the illegal use
                    <br>• Record period of illegal use: 5 years
                </p>
                <h4 class="title text-uppercase pb-30 text-white" style="text-align: left;"><b>Procedures and methods
                        of personal information destruction</b></h4>
                <p class="pb-20 text-justify">In principle, the company destroys customer’s personal information without any delay when the purpose of collecting personal information is achieved. The procedure and method of destroying personal information of the company are as
                    follows.
                </p>
                <p class="pb-20 text-justify"><b>1) Destruction procedure :</b><br> The information entered by the user for registration is transferred to a separate DB (retained in a separate cabinet in case of the written form) after the purpose is achieved and is stored for
                    a certain period of time and destroyed after the period according to internal policy and other relevant laws and regulations(refer to retention and usage period).
                </p>
                <p class="pb-20 text-justify"><b>2) Destruction period :</b><br> In the case where the personal information of the user has elapsed, destruction should be taken place within 5 days from the end of the period of holding the personal information. If the personal
                    information such as accomplishing the purpose of processing personal information, abolishing the service, the destruction should be taken place within 5 days from the day when it is deemed unnecessary to process the personal information.
                </p>
                <p class="pb-20 text-justify"><b>3) Destruction method:</b>
                    <br>• Personal information printed on paper shall be destroyed by being shredded through shredders or burnt
                    <br>• Personal information stored in an electronic file is deleted using a technical method that can’t play the record
                </p>
                <h4 class="title text-uppercase pb-30 text-white" style="text-align: left;"><b>Transfer of personal
                        information abroad</b></h4>
                <p class="pb-20 text-justify">The company uses overseas could services to provide smooth services and enhance user convenience. In this regard, we inform you about the transfer of personal information abroad as follows.
                </p>
                <p class="pb-20 text-justify">The company entrusts the following personal information processing tasks for the smooth handling of personal information business, and both regulates and supervises the entrusted company to process personal information safely in accordance
                    with the Information and Communications Network Act.
                </p>
                <p class="pb-20 text-justify">Customers have the right to refuse consent to the consignment (provision) of personal information. However, customer may be restricted from using the services when refused to consent.
                </p>
                <h4 class="title text-uppercase pb-30 text-white" style="text-align: left;"><b>Customer’s rights and
                        how to exercise the rights</b></h4>
                <p class="pb-20 text-justify">1) The subject of the information can exercise the right of personal information protection at any time with respect to the company
                    <br>• Personal information request
                    <br>• Request revision when error is found
                    <br>• Request destruction
                    <br>• Requests stop the procedure
                </p>
                <p class="pb-20 text-justify">2) The exercise of rights under (1) may be done in writing an e-mail to the person in charge of personal information protection and the department in charge. And the Company take action without any delay.
                </p>
                <p class="pb-20 text-justify">3) If the subject requests correction or deletion of personal
                    information, the company will not use or provide the personal information until the
                    correction or deletion is completed.
                </p>
                <p class="pb-20 text-justify">4) Users can view or modify their registered personal
                    information at any time and may request withdrawal of consent and termination of
                    membership.
                </p>
                <p class="pb-20 text-justify">5) The company does not accept membership of children under the
                    age of 14 who need the consent of a legal representative.
                </p>
                <h4 class="title text-uppercase pb-30 text-white" style="text-align: left;"><b>Issues regarding Cookie
                        operation</b></h4>
                <p class="pb-20 text-justify">Cookie means “contact information file” and is used by the company to provide members with faster and more convenient use of the website and to provide specialized customized services. The Company identifies your computer with
                    regard to cookie management but does not identify you individually.
                </p>
                <p class="pb-20 text-justify">Customers have the option of installing cookies. By setting options in the web browser, members can accept all cookies, check each time a cookie is saved, or refuse to save all cookies. However, if you refuse to install cookies,
                    you will not be able to use the web conveniently, and you may have difficulty using some services that require login.
                </p>
                <h4 class="title text-uppercase pb-30 text-white" style="text-align: left;"><b>Measures to ensure the
                        safety of personal information</b></h4>
                <p class="pb-20 text-justify"><b>1) Technical measures against hacking: </b> <br> The Company installs security programs to prevent leakage and damage of personal information caused by hacking and computer viruses, and the Company periodically updates and checks,
                    installs the system in an area where the place is separated from outside and technically and physically monitors and blocks them.
                </p>
                <p class="pb-20 text-justify"><b>2) Minimize the staff and educate them:</b> <br> The Company limits the personal information processing staff to the person in charge and implements measures to manage personal information. In addition, we constantly emphasize
                    compliance with the company's personal information processing policy through on-the-job training to the person in charge.
                </p>
                <p class="pb-20 text-justify"><b>3) Encryption of personal information: </b> <br> The personal information of the user is encrypted, stored and managed so that only the user can know it, and the important data is using the separate security function such as
                    encrypting the file and transmission data or using the file lock function.
                </p>
                <p class="pb-20 text-justify"><b>4) Restricted access to personal information: </b> <br> The company takes necessary measures to control access to personal information through granting, modifying, and deleting access rights to the database system that processes
                    personal information. We also control unauthorized access from outside by using an intrusion prevention system.
                </p>
                <p class="pb-20 text-justify"><b>5) Using locks for document security: </b> <br> We keep documents with personal information and auxiliary storage media in safe place with lock.
                </p>
                <p class="pb-20 text-justify"><b>6) Access control to unauthorized persons:</b> <br> The company has setup a separate physical storage place for storing personal information and both setup and operates access control procedures on this place.
                </p>
                <h4 class="title text-uppercase pb-30 text-white" style="text-align: left;"><b>Duty of Notification</b>
                </h4>
                <p class="pb-20 text-justify">In the case of addition, deletion or modification of this Privacy Policy, we will announce amendment and contents through announcement at least 7 days prior to the enforcement date.
                </p>
            </div>
        </section>
    </div>
</div>
<?php $this->load->view('commonfront/footer') ?>