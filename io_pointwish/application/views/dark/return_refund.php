<?php $this->load->view('common/header_dark');  ?>

<!--============= Header Section Ends Here =============-->
<section class="page-header bg_img oh" data-background="<?= base_url('assets/front/dark/images/page-header.png') ?>">
    <div class="bottom-shape d-none d-md-block">
        <img src="<?= base_url('assets/front/dark/css/img/page-header.png') ?>" alt="css">
    </div>
    <div class="page-left-thumb">
        <!-- <img src="<?= base_url('assets/dark/front/images/bg/privacy-header.png') ?>" alt="bg"> -->
    </div>
    <div class="container">
        <div class="page-header-content cl-white">
            <h1 class="title">Return & Refund Policy</h1>
        </div>
    </div>
</section>
<!--============= Header Section Ends Here =============-->



<!--============= Privacy Section Starts Here =============-->
<section class="privacy-section padding-top padding-bottom">
    <div class="container">
        <div class="row justify-content-center">           
        </div>
        <?php $this->load->view('common/sidebar_dark');  ?>
        <div class="col-lg-9 col-xl-9 col-md-8">
            <form class="hdmob">
                <select class="fa form-control " onchange="window.location='<?php echo base_url(); ?>page/rules/'+this.value" style="background-color: #ebecec; color: black; font-size: 16px; border: 1px solid rgba(255, 255, 255, 0.3); border-radius: 1.25rem; height: calc(2.25rem + 20px); font-family: 'Poppins', sans-serif; ">

                    <!--<option class="fa" style="font-family: 'Poppins', sans-serif;">Select</option>-->
                    <option value="terms" style="font-family: 'Poppins', sans-serif;">Terms & Condition</option>
                    <option value="privacy_policy" style="font-family: 'Poppins', sans-serif;"> Privcy Policy</option>
                    <option value="risk_disclosure" style="font-family: 'Poppins', sans-serif;"> Disclaimer</option>
                    <option selected value="return_refund" style="font-family: 'Poppins', sans-serif;"> Return & Refund Policy</option>
                    <option value="cookies_policy" style="font-family: 'Poppins', sans-serif;"> Cookies Policy</option>
                    <option value="AML_KYC" style="font-family: 'Poppins', sans-serif;"> AML & KYC Policy</option>
                    <option value="demo_trading" style="font-family: 'Poppins', sans-serif;"> Demo & Trading Account</option>
                    <option value="payment_policy" style="font-family: 'Poppins', sans-serif;"> Payment Policy</option>
                </select>
                <br><br>
            </form>
            <article class="mt-70 mt-lg-0">                
                <p class="pb-20 text-justify"><b class="parabold">1 . 1&nbsp;&nbsp;</b>The company is
                    financially responsible for the client's account balance in any particular moment.
                </p>
                <p class="pb-20 text-justify"><b class="parabold">1 . 2&nbsp;&nbsp;</b>Company's financial
                    responsibility starts with the first record about the customer's deposit and continues up to
                    a full withdrawal of funds.
                </p>
                <p class="pb-20 text-justify"><b class="parabold">1 . 3&nbsp;&nbsp;</b>The client has the right to demand from the Company any amount of funds which is available in his/her account at the time of the inquiry.
                </p>
                <p class="pb-20 text-justify"><b class="parabold">1 . 4&nbsp;&nbsp;</b>The only official methods of deposits/withdrawals are the methods which appear on the company's official website. The client assumes all the risks related to the usage of these payment methods since the payment methods are not the company's partners and not the company's responsibility. The company is not responsible for any delay or cancelation of a transaction which was caused by the payment method. In case the client has any claims related to any of the payment methods, it is his/her responsibility to contact the support service of the particular payment method and to notify the company about those claims.
                </p>
                <p class="pb-20 text-justify"><b class="parabold">1 . 5&nbsp;&nbsp;</b>The company does not assume any responsibility for the activity of any third-party service providers which the customer may use in order to make a deposit/withdrawal. The company's financial responsibility for the client's funds starts when the funds have been loaded to the user's wallet account related to the payment methods which appear on the company's website. In case any fraud is detected during or after a financial transaction, the company reserves the right to cancel such transaction and freeze the client's account.
                </p>
                <p class="pb-20 text-justify">The Company's responsibility for the clients' funds ends when the funds are withdrawn from the user's wallet account or any other account related to the user.
                </p>
                <p class="pb-20 text-justify"><b class="parabold">1 . 6&nbsp;&nbsp;</b>In case of any technical mistakes related to financial transactions, the company reserves the right to cancel such transactions and their results.
                </p>
                <p class="pb-20 text-justify"><b class="parabold">1 . 7&nbsp;&nbsp;</b>The client may have only one registered account on the company's website. In case the company detects any duplication of the customer's accounts, the company reserves the right to freeze the customer's accounts and funds without the right of withdrawal.
                </p>
                <h4 class="title text-uppercase pb-30" style="text-align: left;"><b>Client's registration</b></h4>
                <p class="pb-20 text-justify"><b>2 . 1 Client's registration is based on two main steps:&nbsp;&nbsp;</b>
                    <br>• Client's web registration
                    <br>• Client's identity verification
                </p>
                <p class="pb-20 text-justify"><b>In order to complete the first step the client needs to:&nbsp;&nbsp;</b>
                    <br>• Provide the company with his/her real identity and contact details
                    <br>• To accept the company's agreements and their appendices
                </p>
                <p class="pb-20 text-justify"><b>2 . 2 In order to complete the second step the company needs to request and the client needs to provide&nbsp;&nbsp;</b>
                    <br>• a scan or digital photo of his/her identification document
                    <br>• full copy of all the pages of his/her ID document with the photo and personal details
                </p>
                <p class="pb-20 text-justify">The company reserves the right to demand from the client any other documents, such as payment bills, bank confirmation, bank card scans or any other document that may be necessary during the identification process.
                </p>
                <p class="pb-20 text-justify">The identification process must be completed in 10 business days since the company`s request. In some cases, the company may increase the identification period up to 30 working days.
                </p>
                <h4 class="title text-uppercase pb-30" style="text-align: left;"><b>Deposit process</b></h4>
                <p class="pb-20 text-justify">In order to make a deposit, the client shall to make an inquiry from his Personal Cabinet. In order to complete the inquiry, the client needs to choose any of the payment methods from the list, fill in all the necessary details and continue with the payment.
                </p>
                <p class="pb-20 text-justify">The following currencies are available for deposit: USDT, BTC, ETH
                </p>
                <p class="pb-20 text-justify">Withdrawal request processing time depends on the payment method and may vary from one method to another. The company cannot regulate the processing time. In case of using electronic payment methods, the transaction time can vary from seconds to days. In case of using blockchain, the transaction time can be from 3 up to 45 business days.
                </p>
                <p class="pb-20 text-justify">Any transactions made by the Client must be executed through the determined source of the transaction, belonging exclusively to the Client, who carries out the payment by his/her own funds. The withdrawal, refund, compensation, and other payments carried out from the Client’s account. Withdrawal from the Account may be carried out only in the same currency in which the corresponding deposit was made.
                </p>
                <h4 class="title text-uppercase pb-30" style="text-align: left;"><b>Taxes</b></h4>
                <p class="pb-20 text-justify">The company is not a tax agent and does not provide the clients' financial information to any third parties. This information can only be provided in case of an official demand from government agencies.
                </p>
                <h4 class="title text-uppercase pb-30" style="text-align: left;"><b>Refund policy</b></h4>
                <p class="pb-20 text-justify"><b class="parabold">5 . 1&nbsp;&nbsp;</b>At any time a Client can withdraw a part or all funds from his/her Account by sending the Company a Request for Withdrawal containing the Client’s order to withdraw money from the Client’s Account, which complies with the following terms:
                </p>
                <p class="pb-20 text-justify">• The Company will execute the order for withdrawal from the Client’s trading account, which will be limited by the remaining balance of the Client’s Account at the time of order execution. If the amount withdrawn by the Client (including commissions and other expenses as per this Regulation) exceeds the balance of the Client's Account, the Company may reject the order after explaining the reason for the rejection;
                </p>
                <p class="pb-20 text-justify">• The Client's order to withdraw money from the Client's Account must comply with the requirements and restrictions set forth by current legislation and other provisions of the countries in the jurisdiction of which such transaction is made;
                </p>
                <p class="pb-20 text-justify">• Money from the Client's Account must be withdrawn to the same payment system with the same purse ID that was previously used by the Client to deposit funds to the Account. The Company may limit the amount of the withdrawal to a payment system with amount of the deposits came on the Client's account from that payment system. The Company may, at its discretion, make exceptions to this rule and withdraw Client money to other payment systems, but the Company may at any time ask the Client for the payment information for the other payment systems, and the Client must provide the Company with that payment information;
                </p>
                <p class="pb-20 text-justify"><b class="parabold">5 . 2&nbsp;&nbsp;</b>A Request for Withdrawal is executed by transferring the funds to the Client’s External Account by an Agent authorized by the Company.
                </p>
                <p class="pb-20 text-justify"><b class="parabold">5 . 3&nbsp;&nbsp;</b>The Client shall make a Request for Withdrawal in the currency of the deposit. If the deposit currency is different from the transfer currency, the Company will convert transfer amount into the transfer currency at the exchange rate established by the Company as of the time when the funds are debited from the Client’s Account.
                </p>
                <p class="pb-20 text-justify"><b class="parabold">5 . 4&nbsp;&nbsp;</b>The currency in which the Company makes transfers to the Client’s External Account may be displayed in the Client’s Dashboard, depending on the currency of the Client’s Account and the withdrawal method.
                </p>
                <p class="pb-20 text-justify"><b class="parabold">5 . 5&nbsp;&nbsp;</b>The conversion rate, commission and other expenses related to each withdrawal method are set by the Company and may be changed at any time at the Company’s sole discretion. The exchange rate may differ from the currency exchange rate set by the authorities of a particular country and from the current market exchange rate for the relevant currencies. In the cases established by Payment Service Providers, funds may be withdrawn from the Client’s Account in a currency that is different from the currency of the Client’s External Account.
                </p>
                <p class="pb-20 text-justify"><b class="parabold">5 . 6&nbsp;&nbsp;</b>The Company reserves the right to set minimum and maximum withdrawal amounts depending on the withdrawal method. These restrictions will be set out in the Client's Dashboard.
                </p>
                <p class="pb-20 text-justify"><b class="parabold">5 . 7&nbsp;&nbsp;</b>The withdrawal order is deemed accepted by the Company if it is created in the Client’s Dashboard, and is displayed in the Balance History section and in the Company’s system for accounting clients’ requests. An order created in any manner other than that specified in this clause will not be accepted and executed by the Company.
                </p>
                <p class="pb-20 text-justify"><b class="parabold">5 . 8&nbsp;&nbsp;</b>The funds will be withdrawn from the Client's account within five (5) business days.
                </p>
                <p class="pb-20 text-justify"><b class="parabold">5 . 9&nbsp;&nbsp;</b>If the funds sent by the Company pursuant to a Request for Withdrawal have not arrived in the Client's External Account after five (5) business days, the Client may ask the Company to investigate this transfer.
                </p>
                <p class="pb-20 text-justify"><b class="parabold">5 . 10&nbsp;&nbsp;</b>If the Client has made an error in the payment information when drawing up a Request for Withdrawal that resulted in a failure to transfer money to the Client's External Account, the Client will pay a commission for resolving the situation.
                </p>
                <p class="pb-20 text-justify"><b class="parabold">5 . 11&nbsp;&nbsp;</b>The Client's profit in excess of the funds deposited by the Client may be transferred to the Client's External Account only by a method agreed by the Company and Client, and if the Client made a deposit to his/her account by a certain method, the Company has the right to withdraw a previous deposit of the Client by the same method.
                </p>
                <h4 class="title text-uppercase pb-30" style="text-align: left;"><b>Payment methods for withdrawals</b></h4>
                <p class="pb-20 text-justify"><b class="parabold">6 . 1&nbsp;&nbsp;</b>Crypto withdrawals.
                </p>
                <p class="pb-20 text-justify"><b class="parabold">6 . 1 . 1&nbsp;&nbsp;</b>The Client may send a Request for Withdrawal by crypto at any time if the Company accepts this method at the time of funds transfer.
                </p>
                <p class="pb-20 text-justify"><b class="parabold">6 . 1 . 2&nbsp;&nbsp;</b>The Client may make a Request for Withdrawal only to a crypto account opened by his/her. The Company will not accept and execute orders to transfer money to a crypto account of a third party.
                </p>
                <p class="pb-20 text-justify"><b class="parabold">6 . 1 . 3&nbsp;&nbsp;</b>The Company must send the money to the Client's crypto account in accordance with the information in the Request for Withdrawal if the conditions of clause 7.1.2. of this Regulation are met.
                </p>
                <p class="pb-20 text-justify">The Client understands and agrees that the Company assumes no liability for the time a crypto transfer take.
                </p>
            </article>
        </div>
    </div>
    </div>
</section>
<!--============= Privacy Section Ends Here =============-->
<?php $this->load->view('common/footer_dark');  ?>