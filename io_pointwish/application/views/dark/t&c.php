<?php $this->load->view('common/header_dark');  ?>

<!--============= Header Section Ends Here =============-->
<section class="page-header bg_img oh" data-background="<?= base_url('assets/front/dark/images/page-header.png') ?>">
    <div class="bottom-shape d-none d-md-block">
        <img src="<?=base_url('assets/front/dark/css/img/page-header.png') ?>" alt="css">
    </div>
    <div class="page-left-thumb">
        <!-- <img src="<?=base_url('assets/front/dark/images/bg/privacy-header.png') ?>" alt="bg"> -->
    </div>
    <div class="container">
        <div class="page-header-content cl-white">
            <h1 class="title">Terms & Conditions</h1>
        </div>
    </div>
</section>
<!--============= Header Section Ends Here =============-->



<!--============= Privacy Section Starts Here =============-->
<section class="privacy-section padding-top padding-bottom">
    <div class="container">
        <div class="row justify-content-center">
           
        </div>
        <?php $this->load->view('common/sidebar_dark');  ?>
        <div class="col-lg-9 col-xl-9 col-md-8">
            <form class="hdmob">
                <select class="fa form-control " onchange="window.location='<?php echo base_url(); ?>page/rules/'+this.value" style="background-color: #ebecec; color: black; font-size: 16px; border: 1px solid rgba(255, 255, 255, 0.3); border-radius: 1.25rem; height: calc(2.25rem + 20px); font-family: 'Poppins', sans-serif; ">

                    <!--<option class="fa" style="font-family: 'Poppins', sans-serif;">Select</option>-->
                    <option selected value="page/terms" style="font-family: 'Poppins', sans-serif;">Terms & Condition</option>
                    <option value="privacy_policy" style="font-family: 'Poppins', sans-serif;"> Privcy Policy</option>
                    <option value="risk_disclosure" style="font-family: 'Poppins', sans-serif;"> Disclaimer</option>
                    <option value="return_refund" style="font-family: 'Poppins', sans-serif;"> Return & Refund Policy</option>
                    <option value="cookies_policy" style="font-family: 'Poppins', sans-serif;"> Cookies Policy</option>
                    <option value="AML_KYC" style="font-family: 'Poppins', sans-serif;"> AML & KYC Policy</option>
                    <option value="demo_trading" style="font-family: 'Poppins', sans-serif;"> Demo & Trading Account</option>
                    <option value="payment_policy" style="font-family: 'Poppins', sans-serif;"> Payment Policy</option>
                </select>
                <br><br>
            </form>
            <article class="mt-70 mt-lg-0">                
                <p class="pb-20 text-justify">This Public offer agreement (hereinafter referred to as
                        Agreement) governs the terms
                        and conditions for the services of “Guessing Game” (hereinafter referred to as
                        Company) provided online at: www.earnfinex.com. This Agreement is accepted as a
                        web-based document and does not require signing by parties.
                        The Client automatically affirms the full acceptance of the Agreement by registering a
                        Client's Profile at the Company's website. The Agreement remains valid until it is
                        terminated by either parties.
                    </p>

                    <h4 class="title text-uppercase pb-30" style="text-align: left;"><b>Terms and
                            Definitions</b></h4>
                    <p class="pb-20 text-justify"><b class="parabold">Client’s Area – </b>a workspace created in
                        the web-interface, used by the Client
                        for performing
                        Trading and Non-Trading Operations and entering personal information.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">Client – </b>any person over 18 years of
                        age, using the services of “Guessing Game” in accordance with this Agreement.
                    </p>

                    <p class="pb-20 text-justify"><b class="parabold">Company – </b>a legal entity, referred to as
                        “Guessing Game”,
                        which provides, in
                        accordance with the provisions of this Agreement, the conduct of arbitrage
                        operations for the purchase and sale of CFD contracts.
                    </p>

                    <p class="pb-20 text-justify"><b class="parabold">Non-Trading Operation – </b>any operation
                        related to a top-up of the Client’s Trading
                        An account with necessary funds or withdrawal of funds from the Trading Account. For
                        NonTrading Operations, the Company uses electronic payment systems selected at its
                        discretion and tied to the appropriate interface in the Client’s Area.
                    </p>

                    <p class="pb-20 text-justify"><b class="parabold">Client’s Profile – </b>a set of personal
                        data about the Client, provided by himself/
                        herself during registration and verification process within the Client’s Area, and
                        stored on the Company’s secure server.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">Trading Account – </b>a specialized account
                        on the Company’s server that enables the Client
                        to conduct Trading Operations.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">Trading Operation – </b>an arbitration
                        operation for the purchase and sale of trade contracts
                        performed by the Client using the Trading Terminal available in the Client’s Area.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">Trading Server – </b>a server owned by the
                        Company with specialized software installed on it,
                        which serves for conducting Trading and Non-Trading Operations of Clients and tracking
                        the statistics of these operations.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">Trading Terminal – </b>a specialized
                        interface located in the Client’s Area, connected to the
                        Company’s Trading Server, and allowing the Client to perform Trading Operations.
                    </p>

                    <h4 class="title text-uppercase pb-30" style="text-align: left;"><b>General Provisions</b></h4>
                    <p class="pb-20 text-justify"><b class="parabold">2 . 1&nbsp;&nbsp;</b>The service provided by the Company is an Internet service that uses the official website
                        of the Company and its Trading Server to carry out Trading Operations. The use of
                        the service implies the availability of sustainable high-speed Internet connection on the
                        Client’s device.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">2 . 2&nbsp;&nbsp;</b>In its activities, the Company is guided by existing Legislation on anti-money
                        laundering and terrorist financing. The Company requires the Client to correctly
                        enter personal data, and reserves the right to verify the Client’s identity, using the
                        necessary methods:
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">2 . 2 . 1&nbsp;&nbsp;</b>In its activities, the Company is guided by existing Legislation on anti-money
                        laundering and terrorist financing. The Company requires the Client to correctly
                        enter personal data, and reserves the right to verify the Client’s identity, using the
                        necessary methods:
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">2 . 2 . 2&nbsp;&nbsp;</b>A phone call to the Client at the specified phone number if necessary.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">2 . 2 . 3&nbsp;&nbsp;</b>Other means necessary at the discretion of the Company to confirm the Client’s
                        identity and financial activity.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">2 . 3&nbsp;&nbsp;</b>A Client, regardless of the legal status (legal or natural person), is prohibited to have more
                        than one Trading Account with the Company. The Company reserves the right to terminate
                        this Agreement or reset the results of Trading Operations in the event of re-registration of the
                        Client Profile or in case of multiple Trading Accounts usage by the same Client.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">2 . 4&nbsp;&nbsp;</b>A Client’s Profile is registered in the secured space of the Client’s Area on the
                        Company’s official website. The Company guarantees the confidentiality of the Client’s
                        data in accordance with the provisions of the Section 8 of this Agreement.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">2 . 5&nbsp;&nbsp;</b>The Client is responsible for the safety of the Client’s Area authentication data received
                        from the Company, in case of loss of access to the Client’s Area, Client must immediately
                        notify the Company to block access to the funds in the Trading Account.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">2 . 6&nbsp;&nbsp;</b>Upon registration, the Company automatically provides the Client with a Trading
                        Account where the Client performs all Trading and Non-Trading Operations.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">2 . 7&nbsp;&nbsp;</b>The Company carries quoting of the Clients by using its own paid sources of quotations,
                        applying to process of the quote flow in accordance with the needs of ensuring the liquidity of
                        contracts opened by Clients. Quotes of any other companies, and/or quotes taken from other
                        paid sources, cannot be taken into account when considering disputes.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">2 . 8&nbsp;&nbsp;</b>The Company provides the Client with a specially prepared web interface (Trading
                        Terminal) to carry out Trading Operations within the Client’s Area.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">2 . 9&nbsp;&nbsp;</b>The Company prohibits the Client to resort to any type of fraudulent activity that
                        may be considered by the Company in the Client’s actions aimed at gaining
                        profit using actions or operations not instructed by the Company,
                        vulnerabilities in the Company’s official website, bonus speculation, and trading
                        with a group of persons, including but not limited to hedging transactions from
                        different accounts. In this case, the Company reserves the right to terminate this
                        Agreement or to reset the results of Trading Operations.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">2 . 10&nbsp;&nbsp;</b>The Company reserves the right to terminate this Agreement or to reset the results of
                        Trading Operations in cases of detecting an unfair attitude towards the Company as a whole
                        and to the products and services provided, including (but not limited to) insulting employees
                        and partners of the Company, slandering, publishing unreliable information about the Company, negative reviews, attempted blackmail or extortion by the Client.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">2 . 11&nbsp;&nbsp;</b>The Company reserves the right to prohibit the Client from copying the Trading
                        Operations of other traders in case of speculation on copying in small volumes (simultaneous
                        copying of bets with the resulting sum less than $1) or in other cases, when the Company
                        considers that the Client is abusing this feature.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">2 . 12&nbsp;&nbsp;</b>The Client shall ensure that his/her activities fully comply with the legislation of the country where they are conducted.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">2 . 13&nbsp;&nbsp;</b>The Client acknowledges and accepts the responsibility for the payment of all taxes and
                        fees which may arise from the performance of Trading Operations.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">2 . 14&nbsp;&nbsp;</b>This trading platform is for entertainment purposes only and based on the guessing ability or trading skills of the client.
                    </p>
                    <h4 class="title text-uppercase pb-30" style="text-align: left;"><b>Procedure of Non-Trading Operations Execution</b></h4>
                    <p class="pb-20 text-justify"><b class="parabold">3 . 1&nbsp;&nbsp;</b>Non-Trading Operations include operations performed by the Client to top-up the
                        Trading Account as well as withdraw funds from it (deposit and withdrawal of funds).
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">3 . 2&nbsp;&nbsp;</b>Non-Trading Operations are performed by the Client with the help of the Client’s
                        Area functionality. The Company does not carry out Non-Trading Operations
                        requested using conventional means of communication (Email, ICQ, Live-chat, etc.).
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">3 . 3&nbsp;&nbsp;</b>While performing Non-Trading Operations, the Client is only allowed to use personal
                        funds held in wallet accounts owned by the Client.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">3 . 4&nbsp;&nbsp;</b>The currency of the Trading Account is the US dollar. The currency is used to display
                        Client’s Trading Account balance. Trading Account currency cannot be changed by the
                        Client. Automatic recalculation of the deposited amount from the currency used by the Client
                        to the currency of the Trading Account is applied when the Client deposits funds into the
                        Wallet. The same process occurs during withdrawal procedures.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">3 . 5&nbsp;&nbsp;</b>In case of currency conversion, the Company uses an exchange rate in accordance with the
                        quotes that are received from supported electronic payment providers at the time of the Nontrading Operation execution.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">3 . 6&nbsp;&nbsp;</b>The Company sets the following minimum amounts for Non-Trading Operations (unless
                        specified otherwise):
                        <br>• Deposit – 10 USDT;<br>• Withdrawal – 100 USDT.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">3 . 7&nbsp;&nbsp;</b>If the Client uses different wallets for a Trading Account top-up, the withdrawal of
                        funds to these wallets will be carried out in the same proportion in which the deposit was
                        made. If the Company is not able to process the withdrawal of funds to the wallets indicated
                        by the Client, the Company is obliged to contact the Client in order to change the selected
                        payment systems or wallets. <br>
                        (3.8. If the Client uses bank cards to replenish the Trading Account, the Client agrees that the Company can save the bank card payment details in order to implement the "quick top-up"
                        function of the Trading Account in one click, when the Client uses the appropriate
                        functionality in the Client's Area. The Client can disable this service upon request, by
                        contacting the Company's support service).
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">3 . 8&nbsp;&nbsp;</b>In order to ensure compliance with the requirements of generally accepted
                        Legislative standards, as well as to protect Clients’ funds, the withdrawal of funds shall be
                        performed using the same payment method that was previously used for depositing, and by
                        using the same payment details.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">3 . 9&nbsp;&nbsp;</b>The Company does not allow the use of the provided services as a mean to extract profits
                        from Non-Trading Operations, or in any way other than its intended purpose.
                    </p>
                    <h4 class="title text-uppercase pb-30" style="text-align: left;"><b>Procedure of Trading Operations Execution</b></h4>
                    <p class="pb-20 text-justify"><b class="parabold">4 . 1&nbsp;&nbsp;</b>Trading Operations include arbitrage operations for sale and purchase of trade contracts
                        with the trading instruments provided by the Company. These operations are executed via
                        Trading Terminal provided by the Company within the Client’s Area. The processing of all
                        Clients’ Trading Operations is carried out by the Company using the Trading Server at its
                        disposal with the appropriate software.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">4 . 2&nbsp;&nbsp;</b>The Company uses the «Market Execution» quotation technology for the Trading
                        Operations execution and performs a transaction at the price that exists at the time of the
                        Client’s request processing in the queue of Clients’ requests on the Company’s Trading
                        Server. The maximum deviation of the price indicated in the Client’s Trading Terminal from
                        the price existing on the Company’s Trading Server does not exceed the value of the two
                        average spreads for this trading instrument in the periods corresponding to the average
                        volatility of this instrument.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">4 . 3&nbsp;&nbsp;</b>The Company reserves the right to refuse the Client to conduct a Trade
                        Operation if, at the moment the Client makes a request to open a contract, the
                        Company does not have enough liquidity in the trading instrument chosen by the
                        Client by the time the contract expires. In this case, upon clicking the
                        corresponding button in the Trading Terminal, the Client receives a notification.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">4 . 4&nbsp;&nbsp;</b>The amount of funds paid to the Client in the event of a positive outcome of the
                        trade contract concluded by him/her is determined by the Company as a
                        percentage of the amount of collateral determined by the Client at the time of the
                        execution of the trade contract using the corresponding interface element of the Trading
                        Terminal.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">4 . 5&nbsp;&nbsp;</b>As a part of services provided by the Company, the Client is offered to
                        purchase, sell trade contracts or not to participate in operations. The trade
                        contracts come in a variety of classes, depending on the purchase method.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">4 . 6&nbsp;&nbsp;</b>The Client has the possibility to keep any number of
                        simultaneously opened Trading Operations on his Trading Account for any
                        expiration date of any class of trade contracts available. At the same time, the
                        total volume of all newly opened Trading Operations cannot exceed the amount of
                        the Client’s balance in the Trading Terminal.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">4 . 7&nbsp;&nbsp;</b>The Company reserves the right to cancel or revise the results of the Client’s Trading
                        Operation in the following cases:
                        <br>• The Trading Operation is opened/closed at a non-market quotation;
                        <br>• The Trading Operation is performed with the help of unauthorized bot software;
                        <br>• In case of software failures or other malfunction on the Trading Server;
                        <br>• Synthetic Trading Operations (locks) on trade contracts may be invalidated in the
                        event of revealing obvious signs of abuse.
                    </p>
                    <h4 class="title text-uppercase pb-30" style="text-align: left;"><b>Quotes and Information</b></h4>
                    <p class="pb-20 text-justify"><b class="parabold">5 . 1&nbsp;&nbsp;</b>The price offered in the Company’s Trading Terminal is used for Trading Operations.
                        Trading conditions for instruments are specified in the contract specifications. All issues related
                        to determining the current price level in the market are in the sole competence of the
                        Company, their values are the same for all Clients of the Company.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">5 . 2&nbsp;&nbsp;</b>In the event of an unplanned interruption in the flow of server quotes caused by a hardware
                        or software failure, the Company reserves the right to synchronize the base of Public offer
                        quotations on the Trading Server with other sources. Such sources may be:
                        <br>A. the quotes base of the liquidity provider;
                        <br>B. the quotes base of a news agency.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">5 . 3&nbsp;&nbsp;</b>In the event of a failure in profit calculation by the type of trade contract/
                        instrument as a result of incorrect response of the software and/or hardware of the
                        Trading Server, the Company reserves the right to:
                        <br>A. Cancel a mistakenly opened position;
                        <br>B. Adjust a mistakenly executed Trading Operation according to the current values.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">5 . 4&nbsp;&nbsp;</b>The method of adjusting or changing the volume, price and/or number of Trading
                        Operations (and/or the level or volume of any order) is determined by the Company and is
                        final and binding on the Client. The Company undertakes to inform the Client of any
                        adjustment or such change as soon as this becomes possible.
                    </p>
                    <h4 class="title text-uppercase pb-30" style="text-align: left;"><b>Authorities and Responsibilities of the Company and the Client</b></h4>
                    <p class="pb-20 text-justify"><b class="parabold">6 . 1&nbsp;&nbsp;</b>The Client is not entitled to request any trading recommendations or other
                        information that motivates to commit Trading Operations from the Company
                        representatives. The Company undertakes not to give the Client any recommendations
                        directly motivating the Client to perform any Trading Operations. This provision does not
                        apply to the issuance of general recommendations by the Company on the use of CFD
                        trading strategies.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">6  .  2&nbsp;&nbsp;</b>The Client guarantees the Company protection against any obligations, expenses, claims,
                        damages that the Company may incur both directly and indirectly due to the inability of the
                        Client to fulfill its obligations to third parties both in connection with its activities in the
                        Company and outside it.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">6 . 3&nbsp;&nbsp;</b>The Company is not a provider of communication (Internet connection) services and is
                        not liable for non-fulfillment of obligations due to failure in communication channels.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">6 . 4&nbsp;&nbsp;</b>The Client is obliged to provide copies of the identification and residence address
                        confirmation documents, as well as comply with any other verification steps as determined by
                        the Company.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">6 . 5&nbsp;&nbsp;</b>The Client undertakes not to distribute in any media (social media, forums,
                        blogs, newspapers, radio, television, including but not limited to the abovementioned) any information about the Company without prior approval of the content
                        with its official representative.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">6 . 6&nbsp;&nbsp;</b>The Company reserves the right to amend this Agreement in whole or in part
                        without notifying the Client. The current Agreement can be found on the official
                        website of the Company, the revision date is indicated in the appropriate section.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">6 . 7&nbsp;&nbsp;</b>The Company is not liable to the Client for any losses incurred as a result of using
                        the service provided by the Company; the Company does not compensate for moral
                        damage or loss of profits, unless otherwise specified in this Agreement or other legal
                        documents of the Company.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">6 . 8&nbsp;&nbsp;</b>The main communication method between the Company and the Client is
                        email correspondence, which does not cancel the Company’s obligation to provide the
                        Client with the necessary support using other means and methods of communication
                        available on its official website.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">6 . 9&nbsp;&nbsp;</b>The Company provides the following procedure for settlements with Clients:
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">6 . 9 .1&nbsp;&nbsp;</b>Client’s Trading Accounts top-up is performed automatically in most cases, without the
                        participation of the Company’s staff. In exceptional cases, in the event of malfunctions in the
                        software of intermediaries involved in payments processing, the Company at its discretion may
                        process the accrual of funds on Trading Account manually. If the deposit is processed
                        manually, the Client must specify transfer id number, date & time, payment method used,
                        sender and recipient wallet details when contacting the Company’s support service.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">6 . 9 .2&nbsp;&nbsp;</b>Withdrawal of funds from the Trading Accounts of the Clients is carried out only in
                        manual mode after the Client submits the relevant form in the Client Area. The Client cannot
                        withdraw an amount that exceeds the amount of funds displayed in his/her Trading Account as
                        the available balance. When the Client submits withdrawal form, the corresponding amount is
                        debited from the available funds on the Client’s Trading Account. The withdrawal
                        request processing is executed within three business days. In certain
                        cases, the Company reserves the right to extend the period required for applications
                        processing up to 14 business days, having notified the Client in advance.
                    </p>
                    <h4 class="title text-uppercase pb-30" style="text-align: left;"><b>Risk Disclosure</b></h4>
                    <p class="pb-20 text-justify"><b class="parabold">7 . 1&nbsp;&nbsp;</b>The Client assumes the risks of the following types:
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">7 . 1 . 1&nbsp;&nbsp;</b>General risks in investing associated with the possible loss of invested funds as a result of
                        committed Trading Operations. Such risks are not subject to state insurance and are
                        not protected by any legislative acts.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">7 . 1 . 2&nbsp;&nbsp;</b>Risks associated with the provision of online trading. The Client is aware
                        that the Trading Operations are secured using the electronic trading system and are
                        not directly connected with any existing global trading platform. All communications
                        are carried out via communication channels.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">7 . 1 . 3&nbsp;&nbsp;</b>Risks associated with the use of third party electronic payment systems.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">7 . 2&nbsp;&nbsp;</b>The Client is aware that he/she cannot invest funds in his/her Trading Account, the loss of
                        which will significantly impair the quality of his life or create problems for the client in
                        relations with third parties.
                    </p>
                    <h4 class="title text-uppercase pb-30" style="text-align: left;"><b>The Processing of Personal Data</b></h4>
                    <p class="pb-20 text-justify"><b class="parabold">8 . 1&nbsp;&nbsp;</b>The Client is aware that he/she cannot invest funds in his/her Trading Account, the loss of
                        which will significantly impair the quality of his life or create problems for the client in
                        relations with third parties.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">8 . 2&nbsp;&nbsp;</b>The Company ensures the safety of the Clients’ data in the form in which they are
                        entered by the Client during registration on the official website of the Company and within the
                        Client’s Profile.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">8 . 3&nbsp;&nbsp;</b>The Client has the right to change personal data in his/her Client’s Area, except for the
                        email address. The data can be changed only when the Client personally contacts the support
                        service of the Company after proper identification.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">8 . 4&nbsp;&nbsp;</b>The Company uses «cookies» technology on its website, in order to provide statistically
                        information storage.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">8 . 5&nbsp;&nbsp;</b>The Company has an affiliate program but does not provide partners with any personal
                        data about their referrals.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">8 . 6&nbsp;&nbsp;</b>The Company’s mobile application can gather anonymized stats on the installed
                        applications.
                    </p>
                    <h4 class="title text-uppercase pb-30" style="text-align: left;"><b>Procedure for Handling Claims and Disputes</b></h4>
                    <p class="pb-20 text-justify"><b class="parabold">9 . 1&nbsp;&nbsp;</b>All disputes between the Company and the Client are resolved in a complaint procedure
                        by negotiation and correspondence.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">9 . 2&nbsp;&nbsp;</b>The Company accepts claims arising under this Agreement only by
                        email support@earnfinex.com and not later than seven business days from the
                        date (day) of a disputed case.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">9 . 3&nbsp;&nbsp;</b>The Company is obliged to review the claim of the Client in a period not exceeding 14
                        business days upon receiving a written complaint from the Client, and to notify the Client
                        about the outcome of the complaint by email.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">9 . 4&nbsp;&nbsp;</b>The Company does not compensate the Clients for any loss of profit or moral damage in
                        the event of a positive decision on the Client’s claim. The Company makes a compensation
                        payment to the Client’s Trading Account or cancels the result of the disputed Trading
                        Operation, bringing the balance of the Client’s Trading Account back the way it was in the
                        case of the disputed Trading Operation would not have been carried out. The results of other
                        Trading Operations on the Client’s Trading Account are not affected.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">9 . 5&nbsp;&nbsp;</b>The compensation payment is credited to the Client’s Trading Account within one
                        business day after a positive decision has been taken on the Client’s claim.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">9 . 6&nbsp;&nbsp;</b>In the event of a dispute that is not described in this Agreement, the Company, when
                        making a final decision is guided by the norms of generally accepted international
                        practices and ideas about a fair settlement of the dispute.
                    </p>
                    <h4 class="title text-uppercase pb-30" style="text-align: left;"><b>Term and Termination of the Agreement</b></h4>
                    <p class="pb-20 text-justify"><b class="parabold">10 . 1&nbsp;&nbsp;</b>This Agreement becomes effective from the moment the Client logs into his
                        Client’s Area for the first time at https://earnfinex.com/profile/</a> (Client’s Profile
                        registration) and will be valid in perpetuity.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">10 . 2&nbsp;&nbsp;</b>Either Party may terminate this Agreement unilaterally:
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">10 . 2 . 1&nbsp;&nbsp;</b>The Agreement shall be considered terminated at the initiative of the Client
                        an initiative within seven business days from the moment of closing the
                        Client’s Profile in the Client’s Area or receiving the written notification from
                        the Client containing the request for termination of the Agreement provided that the
                        Client has no unfulfilled obligations hereunder. Notice of termination must be sent by
                        the Client to the Company’s email: <a href="mailto:support@earnfinex.com" target="_blank">support@earnfinex.com</a>
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">10 . 2 . 2&nbsp;&nbsp;</b>The Company has the right to unilaterally, without explanation, terminate
                        the Agreement with the Client. However, the Company undertakes to comply with its
                        financial obligations to the Client at the time of termination of the Agreement within 30
                        business days, provided that the Client has no unfulfilled obligations hereunder.

                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">10 . 2 . 3&nbsp;&nbsp;</b>The Company has the right to unilaterally terminate the Agreement without prior
                        notice to the Client in the event of a violation of one or several provisions of the following
                        Agreement.
                    </p>
                    <p class="pb-20 text-justify"><b class="parabold">10 . 3&nbsp;&nbsp;</b>This Agreement is considered terminated with respect to the Parties when the mutual
                        obligations of the Client and the Company with respect to previously made Non-Trading
                        Operations are fulfilled and all debts of each Party are repaid, provided that the Client has no
                        unfulfilled obligations hereunder.
                    </p>
            </article>
        </div>
    </div>
    </div>
</section>
<!--============= Privacy Section Ends Here =============-->
<?php $this->load->view('common/footer_dark');  ?>