<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="Access Denied">
    <title>pwt | 404</title>
    <link rel="canonical" href="<?= base_url($this->uri->uri_string()) ?>" />
    <link rel="shortcut icon" href="<?= base_url('assets/front/images/favicon.ico') ?>" type="image/x-icon">

    <link href="<?= base_url('errors/minified.css') ?>" rel="stylesheet" type="text/css" />
</head>

<body>
    <!--============= Error In Section Starts Here =============-->
    <div class="error-section bg_img" data-background="<?= base_url('errors/404-bg.jpg') ?>">
        <div class="container">
            <div class="man1">
                <img src="<?= base_url('errors/man_01.png') ?>" alt="404" class="wow bounceInUp" data-wow-duration=".5s" data-wow-delay=".5s">
            </div>
            <div class="man2">
                <img src="<?= base_url('errors/man_02.png') ?>" alt="404" class="wow bounceInUp" data-wow-duration=".5s">
            </div>
            <div class="error-wrapper wow bounceInDown" data-wow-duration=".7s" data-wow-delay="1s">
                <h1 class="title">404</h1>
                <h3 class="subtitle">Page Not Found</h3>
                <a href="<?= base_url() ?>" style="background: #c99728; outline: none !important;" class="button-5 shadow">Go Back</a>
            </div>
        </div>
    </div>
    <!--============= Error In Section Ends Here =============-->
    <script src="<?= base_url('errors/minified.js') ?>"></script>
    <script src="<?= base_url('errors/main.js') ?>"></script>
    <script>
        document.onkeydown = function(e) {
            if (e.keyCode == 123) {
                return false;
            }
            if (e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)) {
                return false;
            }
            if (e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)) {
                return false;
            }
            if (e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)) {
                return false;
            }

            if (e.ctrlKey && e.shiftKey && e.keyCode == 'C'.charCodeAt(0)) {
                return false;
            }
        }

        $("html").on("contextmenu", function() {
            return false;
        });
    </script>
</body>

</html>
