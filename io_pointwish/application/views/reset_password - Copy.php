<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from pixner.net/mosto/demo/ltr/light/reset-password.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 11 Dec 2020 12:44:26 GMT -->

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Reset Password">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>pwt | Reset Password</title>
    <link rel="stylesheet" href="<?= base_url('assets/front/dark/css/login.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/front/dark/css/minified.css') ?>">

    <link rel="shortcut icon" href="<?= base_url('assets/front/dark/images/favicon.ico') ?>" type="image/x-icon">
    
    <script>
        var BASE_URL = "<?= base_url() ?>";
    </script>


</head>

<body>
    <!--<div class="preloader">-->
    <!--    <div class="preloader-inner">-->
    <!--        <div class="preloader-icon">-->
    <!--            <span></span>-->
    <!--            <span></span>-->
    <!--        </div>-->
    <!--    </div>-->
    <!--</div>-->
    <!--============= Sign In Section Starts Here =============-->
    <div class="account-section bg_img py-2" data-background="<?= base_url('assets/front/dark/images/account-bg.jpg') ?>">
        <div class="container mt-auto mb-auto">
            <div class="account-wrapper pt-3 bg-white pb-3">
                <div class="account-body pt-0">
                    <div class="form-head">
                        <!--<h1 class="title fa-2x mb-20"><?= lang('rst_yr_pwd') ?></h1>-->
                        <a class="logo">
                            <img height="75px" src="<?= base_url('assets/front/dark/images/logo/logo-1.svg') ?>" alt="logo">
                        </a>
                        
                    </div>
                    <span id="errorMessage"></span>
                    <form id="form_reset" name="form_reset" action="#">
                        <div class="form-group">
                            <input class="inputhght required" name="password" id="password" type="password" placeholder="New Password" autocomplete="off" required style="border: 1px solid #c99728 !important">
                            <span id="error_passwordUp" class="errorSpan"></span>
                        </div>
                        <div class="form-group pt-3">
                            <input class="inputhght required" type="password" name="confirm_password" id="confirm_password" placeholder="<?= lang('cnfm_pwd') ?>" autocomplete="off" equalTo="#password" required style="border: 1px solid #c99728 !important">
                            <span id="error_cpassword" class="errorSpan"></span>
                        </div>
                        <span id="error_ResetC" class="errorSpan"></span>                        
                        <div class="form-group text-center mb-0">
                            <button style="background: #c99728 !important; outline: none !important;" type="submit" class="text-white">
                                <span id="reset_btn_Online">Reset Now</span>
                            </button>
                        </div>
                        <a href="<?= base_url('home')?>" class="text-3b368c mb-1 hide-mobile">Back to Home</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--============= Sign In Section Ends Here =============-->
    <script src="<?= base_url('assets/front/dark/js/minified.js') ?>"></script>
    <script src="<?= base_url('assets/user_panel/js/jquery.validate.js') ?>"></script>
    <script src="<?= base_url('assets/front/dark/js/main.js') ?>"></script>
    <script src="<?= base_url('assets/front/dark/js/common.js') ?>"></script>

</body>

</html>
