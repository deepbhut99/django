<!doctype html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Authentication forms">
    <meta name="author" content="Arasari Studio">
    <title>pwt Login & Registration</title>
    <link href="<?= base_url('assets/frontforpwtlogin/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('assets/frontforpwtlogin/css/common.css') ?>" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700&amp;display=swap" rel="stylesheet">
    <link href="<?= base_url('assets/frontforpwtlogin/css/theme-07.css') ?>" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css">

    <link rel="shortcut icon" href="<?= base_url('assets/frontforpwt/images/icons/faviconn.svg') ?>">

    <script>
        var BASE_URL = "<?php echo base_url(); ?>";
    </script>

    <style>
        .forny-two-pane>div:last-child {
            background-image: none !important;
        }

        .forny-container {
            background: linear-gradient(108deg, rgb(40, 13, 87) 0%, rgb(54, 63, 159) 100%) center center / cover no-repeat !important;
        }

        .forny-two-pane>div:last-child {
            display: block !important;
        }

        .forny-form {
            margin-top: 7%;
        }

        .input-reset-pwd {
            background-color: transparent !important;
            border-bottom: 1px solid #d9d9d9 !important;
            padding-bottom: 7px;
        }

        .input-reset-pwd .form-control {
            color: #fff !important;
        }

        .forny-container .is-valid,
        .forny-container .is-valid input:-webkit-autofill,
        .forny-container .is-valid input:-webkit-autofill:hover,
        .forny-container .is-valid input:-webkit-autofill:focus,
        .forny-container .is-valid input:-webkit-autofill:active {
            box-shadow: none !important;
        }
    </style>
</head>

<body>
    <div class="forny-container">
        <div class="forny-inner">
            <div class="forny-two-pane">
                <div>
                    <div class="forny-form">
                        <div class="mb-8 forny-logo">
                            <div class="hidden-xs text-center">
                                <a href="<?= base_url('home') ?>">
                                    <img width="250px" src="<?= base_url('assets/front/newthem/img/logo/logo-web.png') ?>">
                                </a>
                            </div>
                            <div class="visible-xs text-center">
                                <img width="200px" src="<?= base_url('assets/front/newthem/img/logo/logo-web.png') ?>">
                            </div>
                        </div>
                        <div class="tab-content">
                            <p class="mt-6 mb-6 text-white">
                                Reset your password.
                            </p>
                            <span id="errorMessage"></span>
                            <form id="form_reset" name="form_reset" action="#" class="account-form">
                                <div class="form-group input-reset-pwd">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-lock text-white"></i>
                                            </span>
                                        </div>
                                        <input class="form-control required" type="password" name="password" id="password" placeholder="New Password" autocomplete="off" required>
                                        <span id="error_passwordUp" class="errorSpan"></span>
                                    </div>
                                </div>
                                <div class="form-group input-reset-pwd">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-lock text-white"></i>
                                            </span>
                                        </div>
                                        <input class="form-control required" type="password" name="confirm_password" id="confirm_password" placeholder="Confirm Password" autocomplete="off" equalTo="#password" required>
                                        <span id="error_ResetC" class="errorSpan"></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <button class="btn btn-primary btn-block" type="submit">
                                            <span id="reset_btn_Online">Reset Now</span>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="<?php echo base_url(); ?>assets/frontforpwtlogin/js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/frontforpwtlogin/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/frontforpwtlogin/js/main.js"></script>
    <script src="<?php echo base_url(); ?>assets/frontforpwtlogin/js/demo.js"></script>

    <script src="<?= base_url('assets/front/dark/js/minified.js') ?>"></script>
    <script src="<?= base_url('assets/front/dark/js/main.js') ?>"></script>
    <script src="<?= base_url('assets/user_panel/js/jquery.validate.js') ?>"></script>
    <script src="<?= base_url('assets/front/newthem/js/common.js?v=' . rand()) ?>"></script>
</body>

</html>