<!doctype html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Authentication forms">
    <meta name="author" content="Arasari Studio">
    <title>pwt Login & Registration</title>
    <link href="<?= base_url('assets/frontforpwtlogin/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('assets/frontforpwtlogin/css/common.css') ?>" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700&amp;display=swap" rel="stylesheet">
    <link href="<?= base_url('assets/frontforpwtlogin/css/theme-07.css') ?>" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css">

    <link rel="shortcut icon" href="<?= base_url('assets/frontforpwt/images/icons/faviconn.svg') ?>">

    <script>
        var BASE_URL = "<?php echo base_url(); ?>";
    </script>
</head>

<body>
    <div class="forny-container">
        <div class="forny-inner">
            <div class="forny-two-pane">
                <div>
                    <div class="forny-form">
                        <div class="mb-8 forny-logo">
                            <div class="hidden-xs">
                                <a href="<?= base_url('home') ?>">
                                    <img src="<?php echo base_url(); ?>assets/frontforpwtlogin/img/logo-07.svg">
                                </a>
                            </div>
                            <div class="visible-xs">
                                <img src="<?php echo base_url(); ?>assets/frontforpwtlogin/img/logo-07.svg">
                            </div>
                        </div>
                        <div class="tab-content">
                            <p class="mt-6 mb-6">
                                Enter your information to setup a new account.
                            </p>
                            <form class="account-form" id="signUpForm" name="signUpForm" autocomplete="off" method="POST">
                                <?php
                                if ($this->session->flashdata('error_session')) {
                                ?>
                                    <div class="alert alert-danger alert-dismissible">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <?php echo $this->session->flashdata('error_session'); ?>
                                    </div>
                                <?php } ?>

                                <?php
                                if ($this->session->flashdata('success_message')) {
                                ?>
                                    <div class="alert alert-success alert-dismissible">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <span style="color: white;"><?php echo $this->session->flashdata('success_message'); ?></span>
                                    </div>
                                <?php } ?>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="18" height="24" viewBox="0 0 18 24">
                                                    <g transform="translate(-61.127)">
                                                        <g transform="translate(61.127)">
                                                            <path d="M75.6,15.584A3.128,3.128,0,0,1,72.452,12.7a5.374,5.374,0,0,0,1.229-1.234,7.564,7.564,0,0,0,1.331-3.524.537.537,0,0,0,.134-.191,5.891,5.891,0,0,0,.445-2.264A5.275,5.275,0,0,0,70.574,0a4.6,4.6,0,0,0-2.088.5,3.62,3.62,0,0,0-.738.134,4.171,4.171,0,0,0-2.6,2.407,6.062,6.062,0,0,0-.292,3.924,6.386,6.386,0,0,0,.27.831.537.537,0,0,0,.125.185A6.772,6.772,0,0,0,67.8,12.7a3.129,3.129,0,0,1-3.146,2.885,3.689,3.689,0,0,0-3.53,3.706V23.46a.536.536,0,0,0,.532.54H78.595a.536.536,0,0,0,.532-.54V19.291A3.688,3.688,0,0,0,75.6,15.584ZM68.044,1.676a2.588,2.588,0,0,1,.61-.1.526.526,0,0,0,.224-.061,3.576,3.576,0,0,1,1.7-.433,4.2,4.2,0,0,1,3.951,4.41c0,.073,0,.146-.005.218A2.3,2.3,0,0,0,72.862,5H69.234a.974.974,0,0,1-.593-.2,1.006,1.006,0,0,1-.328-.432.649.649,0,0,0-.645-.413.656.656,0,0,0-.592.5,5.033,5.033,0,0,1-1.2,2.188C65.336,4.406,66.3,2.187,68.044,1.676Zm-.463,9.346a6.408,6.408,0,0,1-1.29-3.289,6.123,6.123,0,0,0,1.549-2.2A2.083,2.083,0,0,0,68,5.669a2.021,2.021,0,0,0,1.23.414h3.629a1.264,1.264,0,0,1,1.153.76s0,.008,0,.011c0,3.051-1.744,5.532-3.887,5.532A3.315,3.315,0,0,1,67.581,11.022ZM68.8,13.23a3.821,3.821,0,0,0,2.647,0A4.241,4.241,0,0,0,73,15.78l-2.8,4.041a.091.091,0,0,1-.151,0l-2.8-4.042A4.242,4.242,0,0,0,68.8,13.23Zm9.258,9.69H62.192V19.29a2.612,2.612,0,0,1,2.59-2.629,4.5,4.5,0,0,0,1.553-.333l2.846,4.114a1.153,1.153,0,0,0,.947.5h0a1.153,1.153,0,0,0,.947-.5l2.846-4.113a4.326,4.326,0,0,0,1.552.333,2.612,2.612,0,0,1,2.59,2.629Z" transform="translate(-61.127)" />
                                                        </g>
                                                    </g>
                                                </svg>
                                            </span>
                                        </div>
                                        <input class="form-control" type="text" name="sponsor_id" id="sponsor_id" value="<?php if (!empty($_GET['user_id'])) echo $_GET['user_id']; ?>" autocomplete="off" placeholder="<?= lang('sponser_id') ?>">
                                        <span id="member_name_section" class="successSpan"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="18" height="24" viewBox="0 0 18 24">
                                                    <g transform="translate(-61.127)">
                                                        <g transform="translate(61.127)">
                                                            <path d="M75.6,15.584A3.128,3.128,0,0,1,72.452,12.7a5.374,5.374,0,0,0,1.229-1.234,7.564,7.564,0,0,0,1.331-3.524.537.537,0,0,0,.134-.191,5.891,5.891,0,0,0,.445-2.264A5.275,5.275,0,0,0,70.574,0a4.6,4.6,0,0,0-2.088.5,3.62,3.62,0,0,0-.738.134,4.171,4.171,0,0,0-2.6,2.407,6.062,6.062,0,0,0-.292,3.924,6.386,6.386,0,0,0,.27.831.537.537,0,0,0,.125.185A6.772,6.772,0,0,0,67.8,12.7a3.129,3.129,0,0,1-3.146,2.885,3.689,3.689,0,0,0-3.53,3.706V23.46a.536.536,0,0,0,.532.54H78.595a.536.536,0,0,0,.532-.54V19.291A3.688,3.688,0,0,0,75.6,15.584ZM68.044,1.676a2.588,2.588,0,0,1,.61-.1.526.526,0,0,0,.224-.061,3.576,3.576,0,0,1,1.7-.433,4.2,4.2,0,0,1,3.951,4.41c0,.073,0,.146-.005.218A2.3,2.3,0,0,0,72.862,5H69.234a.974.974,0,0,1-.593-.2,1.006,1.006,0,0,1-.328-.432.649.649,0,0,0-.645-.413.656.656,0,0,0-.592.5,5.033,5.033,0,0,1-1.2,2.188C65.336,4.406,66.3,2.187,68.044,1.676Zm-.463,9.346a6.408,6.408,0,0,1-1.29-3.289,6.123,6.123,0,0,0,1.549-2.2A2.083,2.083,0,0,0,68,5.669a2.021,2.021,0,0,0,1.23.414h3.629a1.264,1.264,0,0,1,1.153.76s0,.008,0,.011c0,3.051-1.744,5.532-3.887,5.532A3.315,3.315,0,0,1,67.581,11.022ZM68.8,13.23a3.821,3.821,0,0,0,2.647,0A4.241,4.241,0,0,0,73,15.78l-2.8,4.041a.091.091,0,0,1-.151,0l-2.8-4.042A4.242,4.242,0,0,0,68.8,13.23Zm9.258,9.69H62.192V19.29a2.612,2.612,0,0,1,2.59-2.629,4.5,4.5,0,0,0,1.553-.333l2.846,4.114a1.153,1.153,0,0,0,.947.5h0a1.153,1.153,0,0,0,.947-.5l2.846-4.113a4.326,4.326,0,0,0,1.552.333,2.612,2.612,0,0,1,2.59,2.629Z" transform="translate(-61.127)" />
                                                        </g>
                                                    </g>
                                                </svg>
                                            </span>
                                        </div>
                                        <input class="form-control required" type="text" name="fullname" id="fullname" onblur="is_unique_user()" autocomplete="off" placeholder="<?= lang('uname') ?>" required>
                                        <span id="error_full_name" class="errorSpan"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="16" viewBox="0 0 24 16">
                                                    <g transform="translate(0)">
                                                        <path d="M23.983,101.792a1.3,1.3,0,0,0-1.229-1.347h0l-21.525.032a1.169,1.169,0,0,0-.869.4,1.41,1.41,0,0,0-.359.954L.017,115.1a1.408,1.408,0,0,0,.361.953,1.169,1.169,0,0,0,.868.394h0l21.525-.032A1.3,1.3,0,0,0,24,115.062Zm-2.58,0L12,108.967,2.58,101.824Zm-5.427,8.525,5.577,4.745-19.124.029,5.611-4.774a.719.719,0,0,0,.109-.946.579.579,0,0,0-.862-.12L1.245,114.4,1.23,102.44l10.422,7.9a.57.57,0,0,0,.7,0l10.4-7.934.016,11.986-6.04-5.139a.579.579,0,0,0-.862.12A.719.719,0,0,0,15.977,110.321Z" transform="translate(0 -100.445)" />
                                                    </g>
                                                </svg>
                                            </span>
                                        </div>
                                        <input class="form-control required" type="text" name="email" id="email" onblur="is_unique_email()" placeholder="<?= lang('enter_email') ?>" autocomplete="off" required>
                                        <span id="error_email" class="errorSpan"></span>
                                    </div>
                                </div>
                                <div class="form-group password-field">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="24" viewBox="0 0 16 24">
                                                    <g transform="translate(0)">
                                                        <g transform="translate(5.457 12.224)">
                                                            <path d="M207.734,276.673a2.543,2.543,0,0,0-1.749,4.389v2.3a1.749,1.749,0,1,0,3.5,0v-2.3a2.543,2.543,0,0,0-1.749-4.389Zm.9,3.5a1.212,1.212,0,0,0-.382.877v2.31a.518.518,0,1,1-1.035,0v-2.31a1.212,1.212,0,0,0-.382-.877,1.3,1.3,0,0,1-.412-.955,1.311,1.311,0,1,1,2.622,0A1.3,1.3,0,0,1,208.633,280.17Z" transform="translate(-205.191 -276.673)" />
                                                        </g>
                                                        <path d="M84.521,9.838H82.933V5.253a4.841,4.841,0,1,0-9.646,0V9.838H71.7a1.666,1.666,0,0,0-1.589,1.73v10.7A1.666,1.666,0,0,0,71.7,24H84.521a1.666,1.666,0,0,0,1.589-1.73v-10.7A1.666,1.666,0,0,0,84.521,9.838ZM74.346,5.253a3.778,3.778,0,1,1,7.528,0V9.838H74.346ZM85.051,22.27h0a.555.555,0,0,1-.53.577H71.7a.555.555,0,0,1-.53-.577v-10.7a.555.555,0,0,1,.53-.577H84.521a.555.555,0,0,1,.53.577Z" transform="translate(-70.11)" />
                                                    </g>
                                                </svg>
                                            </span>
                                        </div>
                                        <input style="height: 45px;" class="form-control required" name="password" id="password" type="password" placeholder="<?= lang('ntr_y_pawd') ?>" autocomplete="off" required>
                                        <div class="regeye">
                                            <i id="eyer1" class="fas fa-eye-slash" onclick="hideshowpwdreg()"></i>
                                        </div>
                                        <span id="error_passwordUp" class="errorSpan"></span>
                                    </div>
                                </div>
                                <div class="form-group password-field">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="24" viewBox="0 0 16 24">
                                                    <g transform="translate(0)">
                                                        <g transform="translate(5.457 12.224)">
                                                            <path d="M207.734,276.673a2.543,2.543,0,0,0-1.749,4.389v2.3a1.749,1.749,0,1,0,3.5,0v-2.3a2.543,2.543,0,0,0-1.749-4.389Zm.9,3.5a1.212,1.212,0,0,0-.382.877v2.31a.518.518,0,1,1-1.035,0v-2.31a1.212,1.212,0,0,0-.382-.877,1.3,1.3,0,0,1-.412-.955,1.311,1.311,0,1,1,2.622,0A1.3,1.3,0,0,1,208.633,280.17Z" transform="translate(-205.191 -276.673)" />
                                                        </g>
                                                        <path d="M84.521,9.838H82.933V5.253a4.841,4.841,0,1,0-9.646,0V9.838H71.7a1.666,1.666,0,0,0-1.589,1.73v10.7A1.666,1.666,0,0,0,71.7,24H84.521a1.666,1.666,0,0,0,1.589-1.73v-10.7A1.666,1.666,0,0,0,84.521,9.838ZM74.346,5.253a3.778,3.778,0,1,1,7.528,0V9.838H74.346ZM85.051,22.27h0a.555.555,0,0,1-.53.577H71.7a.555.555,0,0,1-.53-.577v-10.7a.555.555,0,0,1,.53-.577H84.521a.555.555,0,0,1,.53.577Z" transform="translate(-70.11)" />
                                                    </g>
                                                </svg>
                                            </span>
                                        </div>
                                        <input style="height:45px;" class="form-control required" type="password" name="confirm_password" id="confirm_password" placeholder="<?= lang('cnfm_pwd') ?>" autocomplete="off" equalTo="#password" required>
                                        <div class="regeye">
                                            <i id="eyer2" class="fas fa-eye-slash" onclick="hideshowcpwdreg()"></i>
                                        </div>
                                        <span id="error_cpassword" class="errorSpan"></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <button class="btn btn-primary btn-block" type="submit" id="signUpSubmit" onclick="submit_data();"><span><b><i>0</i><?= lang('sign_upp') ?></b></span></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade successful-form-sn" id="login_detail_modal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">    
                <div class="modal-body">
                    <div class="text-center">
                        <img style="width:30%" alt="logo" src="<?= base_url('assets/frontforpwtlogin/img/logo-07.svg')  ?>">
                    </div>
                    <h2 class="mt-2 mb-2 text-center text-5b5b5b"><?= lang('sucesss') ?></h2>

                    <h4 class="modal-title text-center text-5b5b5b"><?= lang('pls_chck_email') ?></h4>
                    <table class="ml-auto mr-auto">
                        <tr>
                            <td class="text-5b5b5b"><strong><?= lang('uunme') ?></strong> <span id="username_span">PW10576</span></td>
                        </tr>
                        <tr>
                            <td class="text-5b5b5b"><strong><?= lang('l_password') ?>:</strong> <span id="password_span">123456</span></td>
                        </tr>
                    </table>                    
                    <div class="text-center mt-2">
                        <a href="<?php echo base_url(); ?>login"><button type="button" style="box-shadow:none" class="btn btn-primary border-0"><?= lang('okay') ?></button></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="<?php echo base_url(); ?>assets/frontforpwtlogin/js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/frontforpwtlogin/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/frontforpwtlogin/js/main.js"></script>
    <script src="<?php echo base_url(); ?>assets/frontforpwtlogin/js/demo.js"></script>

    <script src="<?= base_url('assets/front/dark/js/minified.js') ?>"></script>
    <script src="<?= base_url('assets/front/dark/js/main.js') ?>"></script>
    <script src="<?= base_url('assets/user_panel/js/jquery.validate.js') ?>"></script>
    <script src="<?= base_url('assets/front/dark/js/common.js?v=' . rand()) ?>"></script>
    <script>
        $("#data_form").validate({
            errorPlacement: function(error, element) {
                //element.before(error);

                element.parents('div.login-error').after(error);
            }
        });
    </script>

    <script>
        function hideshowpwd() {
            if ($("#eyeicon").hasClass("fa-eye-slash")) {
                $("#eyeicon").removeClass("fa-eye-slash");
                $("#eyeicon").addClass("fa-eye");
                $("#lpassword").removeAttr('type', 'password');
                $("#lpassword").attr('type', 'text');
            } else {
                $("#eyeicon").removeClass("fa-eye");
                $("#eyeicon").addClass("fa-eye-slash");
                $("#lpassword").removeAttr('type', 'text');
                $("#lpassword").attr('type', 'password');
            }
        }

        function hideshowpwdreg() {
            if ($("#eyer1").hasClass("fa-eye-slash")) {
                $("#eyer1").removeClass("fa-eye-slash");
                $("#eyer1").addClass("fa-eye");
                $("#password").removeAttr('type', 'password');
                $("#password").attr('type', 'text');
            } else {
                $("#eyer1").removeClass("fa-eye");
                $("#eyer1").addClass("fa-eye-slash");
                $("#password").removeAttr('type', 'text');
                $("#password").attr('type', 'password');
            }
        }

        function hideshowcpwdreg() {
            if ($("#eyer2").hasClass("fa-eye-slash")) {
                $("#eyer2").removeClass("fa-eye-slash");
                $("#eyer2").addClass("fa-eye");
                $("#confirm_password").removeAttr('type', 'password');
                $("#confirm_password").attr('type', 'text');
            } else {
                $("#eyer2").removeClass("fa-eye");
                $("#eyer2").addClass("fa-eye-slash");
                $("#confirm_password").removeAttr('type', 'text');
                $("#confirm_password").attr('type', 'password');
            }
        }
    </script>


    <script>
        var errorMsg = '',
            $valid = false;
        $.validator.addMethod("is_sponser_exist", function(val, elem) {
            var len = $("#sponsor_id").val();
            if (len.length >= 5) {
                $.ajax({
                    async: false,
                    url: '<?php echo base_url(); ?>login/is_sponser_exist',
                    type: "get",
                    dataType: "json",
                    data: {
                        sponsor_id: $("#sponsor_id").val()
                    },
                    success: function(response) {
                        if (response.status == 1) {
                            errorMsg = 'Sponsor Available !';
                            $valid = true;
                            $("#member_name_section").html('User Name: ' + response.member_name);
                            $("#member_name_section").addClass("text-success");
                            $("#member_name_section").removeClass("text-danger");

                        } else if (response.status == 2) {
                            errorMsg = 'User not exist !';
                            $valid = false;
                            $("#member_name_section").html(errorMsg);
                            $("#sponsor_id").addClass("error");
                            $("#member_name_section").addClass("text-danger");
                        }
                    }
                });
            }

            $.validator.messages["is_sponser_exist"] = errorMsg;
            return $valid;
        }, '');

        function is_unique_user() {
            if (/^[a-zA-Z0-9](?!.*?[^\na-zA-Z0-9]{2}).*?[a-zA-Z0-9]$/.test($("#fullname").val())) {
                var len = $("#fullname").val();
                if (len.length > 9) {
                    $isUser = false;
                    $("#error_full_name").removeClass("text-success");
                    $("#fullname").addClass("error");
                    $("#error_full_name").text('Username should be maximum 9 letter.');
                    $("#error_full_name").addClass("error");
                } else if (len.length >= 5) {
                    $.ajax({
                        async: false,
                        url: '<?php echo base_url(); ?>login/is_unique_user',
                        type: "get",
                        dataType: "json",
                        data: {
                            username: $("#fullname").val()
                        },
                        success: function(response) {
                            if (response.status == 1) {
                                $isUser = false;
                                $("#error_full_name").removeClass("text-success");
                                $("#fullname").addClass("error");
                                $("#error_full_name").text('Already Exist');
                                $("#error_full_name").addClass("error");


                            } else if (response.status == 2) {
                                $isUser = true;
                                $("#error_full_name").text('Proceed with name');
                                $("#error_full_name").removeClass("error");
                                $("#error_full_name").addClass("text-success");
                                // $("#newusername").removeClass("error");
                            }
                        }
                    });
                } else {
                    $isUser = false;
                    $("#error_full_name").removeClass("text-success");
                    $("#fullname").addClass("error");
                    $("#error_full_name").text('Username should be minimum 5 letter.');
                    $("#error_full_name").addClass("error");
                }
            } else {
                $isUser = false;
                $("#error_full_name").removeClass("text-success");
                $("#fullname").addClass("error");
                $("#error_full_name").text('Special chracter not allowed.');
                $("#error_full_name").addClass("error");
            }
        }

        function is_unique_email() {
            var len = $("#email").val();
            if (len.length >= 10) {
                $.ajax({
                    async: false,
                    url: '<?php echo base_url(); ?>login/is_email_exist',
                    type: "post",
                    dataType: "json",
                    data: {
                        email: $("#email").val()
                    },
                    success: function(response) {
                        if (response.status == 2) {
                            isEmail = false;
                            $("#error_email").removeClass("text-success");
                            $("#email").addClass("error");
                            $("#error_email").text('Already Exist');
                            $("#error_email").addClass("error");
                        } else if (response.status == 1) {
                            isEmail = true;
                            $("#error_email").text('Proceed with mail');
                            $("#error_email").removeClass("error");
                            $("#error_email").addClass("text-success");
                            // $("#newusername").removeClass("error");
                        }
                    }
                });
            } else {
                isEmail = false;
                $("#error_email").removeClass("text-success");
                $("#email").addClass("error");
                $("#error_email").text('Email should be minimum 10 letter.');
                $("#error_email").addClass("error");
            }
        }

        $("#signUpForm").validate({
            rules: {
                sponsor_id: {
                    is_sponser_exist: true,
                },
                fullname: {
                    required: true,
                    minlength: 5,
                    maxlength: 9
                },
                email: {
                    required: true,
                    minlength: 10
                },
                password: {
                    minlength: 6,
                },
            },
            errorPlacement: function(error, element) {
                element.parents('div.input-group').after(error);
            }
        });

        function submit_data() {
            var form = $("#signUpForm");
            if (form.valid() == false) {
                return false;
            } else {
                $(".submit_button_class").attr('disabled', true);
                // $("#signUpForm").submit();
            }
        }
    </script>
    </script>

</body>

</html>