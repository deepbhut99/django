<?php $this->load->view('trading/common/header'); ?>
<div class="page-content" ng-controller="Tradingbot_Controller" ng-init="get_user_info()">
    <div class="container-fluid mt-12px">
        <div>
            <div class="ml-auto mr-auto mt-lg-10 text-center" ng-show="topLoader">
                <img src="<?= base_url('assets/user_panel/images/preloader.gif') ?>" alt="loader">
            </div>
            <div class="row pl-3 mb-2">
                <div>
                    <!-- <h3 ng-show="user.binance_apikey === ''" class="mb-0 text-new-red mt-n2">Add api key on Profile Page</h3> -->
                </div>
            </div>
            <div class="row mb-14px">
                <div class="ml-auto mr-2">
                    <input type="text" ng-model="top_search" id="filter" name="top_search" class="form-control bg-input2" placeholder="Search Here.." />
                </div>
            </div>

            <div class="row">
                <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 px-3px results" id="boxfor_ticker" ng-repeat="coinname in coin_name | filter:top_search">
                    <div id="{{coinname}}">

                        <a href="<?= base_url('coin-status/{{coinname}}') ?>">

                            <div class="card directory-card border-radius-copy mb-6px">
                                <div class="card-body pb-12px">
                                    <div class="mb-2">
                                        <img class="h-50px" src="<?= base_url('assets/user_panel/images/coins/{{coinname}}.png') ?>" alt="">
                                        <ul class="list-unstyled social-links float-right">
                                            <li class="text-right mb-2">
                                                <small class="opacity-half text-white"><?= lang('profit') ?></small>
                                                <div ng-cloak ng-show="gainersforbot.{{coinname}}['buysell'] == -1 || gainersforbot.{{coinname}}['buysell'] == 1">
                                                    <h3 class="mb-0 text-theme-green mt-n2" id="{{coinname}}-h"></h3>
                                                </div>
                                                <div ng-cloak ng-hide="gainersforbot.{{coinname}}['buysell'] == -1 || gainersforbot.{{coinname}}['buysell'] == 1">
                                                    <h3 class="mb-0 text-new-red mt-n2" id="coin2"><?php echo ("{{coinname}}"); ?></h3>
                                                </div>
                                            </li>
                                            <li class="text-right mb-2">
                                                <small class="opacity-half"></small>
                                                <h3 class="mb-0 mt-n2 text-white" id="{{coinname}}-p"></h3>
                                            </li>
                                            <li class="text-right mb-2">
                                                <small class="opacity-half"></small>
                                                <h3 class="mb-0 mt-n2 text-white" id="{{coinname}}-c"></h3>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('trading/common/footer') ?>