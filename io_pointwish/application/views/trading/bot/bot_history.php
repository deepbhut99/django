<?php $this->load->view('trading/common/header'); ?>
<div class="page-content" ng-controller="Tradingbot_Controller" ng-init="get_user_info()">
    <div class="container-fluid p-0 mt-12px">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="ml-auto mr-auto text-center d-none" id="historyLoader">
                            <img src="<?= base_url('assets/user_panel/images/preloader.gif') ?>" alt="loader">
                        </div>
                        <table id="datatable-bot" class="table display dt-responsive nowrap table-striped table-borderless" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>OrderId</th>
                                    <th>Date</th>
                                    <th>Buy|Sell</th>
                                    <th>Symbol</th>
                                    <th>Price</th>
                                    <th>Qunatity</th>
                                    <th>Total</th>
                                    <th>Profit</th>
                                    <th>fees</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- <tr>
                                    <td>Sr.No</td>
                                    <td>OrderId</td>
                                    <td>Date</td>
                                    <td>Buy|Sell</td>
                                    <td>Symbol</td>
                                    <td>Price</td>
                                    <td>Qunatity</td>
                                    <td>Total</td>
                                    <td>Profit</td>
                                    <td>fees</td>
                                </tr> -->
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('trading/common/footer') ?>

<script>
    $(document).ready(function() {
        $('#datatable-bot').DataTable();
    });
    // var tableContainer = $(table_history.table().container());
    // tableContainer.css('display', 'none');
    // $('#historyLoader').addClass('d-block');
    // console.log($('#startdate').val(), $('#enddate').val());
    // $.ajax({
    //     type: 'post',
    //     url: '<?= base_url("trade/trading/bot_history") ?>',
    //     data: {
    //         start: $('#startdate').val(),
    //         end: $('#enddate').val()
    //     },
    //     success: function(res) {

    //         res = JSON.parse(res);
    //         count = 1;
    //         res.forEach(function(bid2) {
    //             var totalt = bid.price * bid.origQty;

    //             table_history.row.add([count++, bid.id_whichsellorbuy, moment(bid.created_datetime * 1000).format('DD-MM-YYYY hh:mm:ss A'), bid.buy_or_sell == 1 ? '<span class="text-02C076">Buy</span>' : '<span class="text-danger">Sell</span>', bid.symbol, bid.price, bid.origQty, totalt.toFixed(2), bid.price_with_commision, bid.qty_with_commision, ]).draw(false);
    //         })
    //         $('#historyLoader').removeClass('d-block');
    //         tableContainer.css('display', 'block');
    //     }
    // })
</script>

<script>
    var table_history3 = $('#datatable-bot').DataTable({
        destroy: true,
        pagingType: "simple",
        order: [
            [0, "asc"]
        ],
        columnDefs: def_columns
    });
    var bids1 =  <?= json_encode($res) ?>;
    count = 1;

    bids1.forEach(function(bid2) {
        var totalt = bid2.price * bid2.origQty;
        table_history3.row.add([count++, bid2.id_whichsellorbuy, moment(bid2.created_datetime * 1000).format('DD-MM-YYYY hh:mm:ss A'), bid2.buy_or_sell == 1 ? '<span class="text-02C076">Buy</span>' : '<span class="text-danger">Sell</span>', bid2.symbol, bid2.price, bid2.origQty, totalt.toFixed(2), bid2.price_with_commision, bid2.qty_with_commision, ]).draw(false);
    });
</script>