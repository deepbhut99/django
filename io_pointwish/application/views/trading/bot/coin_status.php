<?php $this->load->view('trading/common/header'); ?>
<?php
$prise = $type . '_p';
$qty = $type . '_qty';

?>
<div class="page-content" ng-controller="Tradingbot_Controller2" ng-init="get_user_info('<?php echo ($type) ?>')">
    <div class="container-fluid px-9px mt-12px">
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-md-6 px-3px">
                        <div class="card mb-6px">
                            <div class="card-body">
                                <div class="d-flex">
                                    <img class="h-35" src="<?= base_url('assets/user_panel/images/coins/BNBUSDT.png') ?>">
                                    <h3 class="pl-3 pt-1"><?php echo ($type) ?></h3>
                                </div>
                                <div class="opacity-0.85">
                                    <div class="row mt-2">
                                        <div class="col-md-4 col-sm-6 col-xsm-6">                                            
                                            <h4 class="mb-0"><?php if(empty($balancebinance->$prise)) { echo("0"); } else { echo ($balancebinance->$prise); } ?></h4> 
                                            <h6 class="text-light">Position<br>Amount(USDT)</h6>
                                        </div>
                                        <div class="col-md-4 col-sm-6 col-xsm-6">
                                            <h4 class="mb-0">0.0</h4>
                                            <h6 class="text-light">Avg Price</h6>
                                        </div>
                                        <div class="col-md-4 col-sm-6 col-xsm-6">
                                            <h4 class="mb-0">
                                                <?php if ($botbid->total_stratage == "") {
                                                    echo ("0");
                                                } else {
                                                    echo ($botbid->total_stratage);
                                                } ?>
                                            </h4>
                                            <h6 class="text-light">Numbers of<br>call margin</h6>
                                        </div>
                                        <div class="col-md-4 col-sm-6 col-xsm-6">
                                            <h4 class="mb-0"><?php if(empty($balancebinance->$qty)) { echo("0"); }else { echo($balancebinance->$qty); } ?></h4>
                                            <h6 class="text-light">Position<br>quantity</h6>
                                        </div>
                                        <div class="col-md-4 col-sm-6 col-xsm-6">
                                            <h4 class="mb-0" id="<?php echo ($type) ?>-c"></h4>
                                            <h6 class="text-light">Current Price</h6>
                                        </div>
                                        <div class="col-md-4 col-sm-6 col-xsm-6">
                                            <h4 class="mb-0" id="<?php echo ($type) ?>-p"></h4>
                                            <h6 class="text-light">Change</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 px-3px">
                        <div class="card mb-6px">
                            <div class="card-body">
                                <div class="d-flex">
                                    <h3 class="pt-1">Select your option</h3>
                                </div>
                                <div>
                                    <div class="row mt-2">
                                        <div class="col-sm-4 col-xsm-4" ng-if="gainersforbot.<?php echo ($type) ?>['buysell'] == 1 || gainersforbot.<?php echo ($type) ?>['buysell'] == -1">
                                            <div class="text-center" ng-cloak ng-show="user.cycle_status == 0">
                                                <div id="one-shot" ng-click="cyclestart('<?php echo ($type) ?>',user.id)">
                                                    <i class="fas fa-crosshairs text-46f2d4 fa-2x one-shot"></i>
                                                    <h6 class="text-light one-shot-txt">One-Shot</h6>
                                                </div>
                                            </div>
                                            <div class="text-center" ng-cloak ng-show="user.cycle_status == 1">
                                                <div id="one-shot" ng-click="cyclestop('<?php echo ($type) ?>',user.id)">
                                                    <i class="fas text-46f2d4 fa-2x one-shot fa-recycle"></i>
                                                    <h6 class="text-light one-shot-txt">Cycle</h6>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 col-xsm-4" ng-if="gainersforbot.<?php echo ($type) ?> == '' || gainersforbot.<?php echo ($type) ?>['buysell'] == 3 || gainersforbot.<?php echo ($type) ?>['buysell'] == 4 || gainersforbot.<?php echo ($type) ?>['buysell'] == 5">
                                            <div class="text-center">
                                                <div id="one-shot" ng-click="">
                                                    <i class="fas fa-crosshairs text-46f2d4 fa-2x one-shot"></i>
                                                    <h6 class="text-light one-shot-txt">One-Shot</h6>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 col-xsm-4" ng-click="">
                                            <div class="text-center">
                                                <i class="fab fa-sellsy text-46f2d4 fa-2x"></i>
                                                <h6 class="text-light">Sell</h6>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 col-xsm-4" ng-click="">
                                            <div class="text-center">
                                                <i class="far fa-hand-pointer text-46f2d4 fa-2x"></i>
                                                <h6 class="text-light">Buy</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mt-4">
                                        <!-- <div class="col-sm-4 col-xsm-4">
                                            <div class="text-center">
                                                <div id="stop-margin">
                                                    <i class="fas fa-pause-circle text-danger fa-2x stop-margin"></i>
                                                    <h6 class="text-light stop-margin-txt">Stop margin call</h6>
                                                </div>
                                            </div>
                                        </div> -->
                                        <div class="col-sm-4 col-xsm-4">
                                            <div class="text-center">
                                                <i class="fas fa-box-open text-46f2d4 fa-2x"></i>
                                                <h6 class="text-light">Stratagy Mode</h6>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 col-xsm-4">
                                            <a>
                                                <div class="text-center">
                                                    <i class="fas fa-save text-46f2d4 fa-2x"></i>
                                                    <h6 class="text-light">Saved Strategy</h6>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 px-3px">
                        <div class="card mb-6px min-h-174px">
                            <div class="card-body">
                                <div class="d-flex">
                                    <h3 class="pt-1">Operation Reminder</h3>
                                </div>
                                <div>
                                    <div class="row mt-2">
                                        <div class="col-12">
                                            <div class="text-light font-lg-12">pwt is operating, please do not operate the currency account by yourself, and check whether there is a fixed deposite freezing and other related settings, so as to avoid abnormal judgements cause by the system and affect your rights and interests.</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 px-3px">
                        <div class="row">
                            <div class="col-sm-6 col-xsm-6 pr-3px">
                                <div class="card mb-6px">
                                    <div class="card-body p-coin-stat">
                                        <div class="d-flex justify-content-between">
                                            <i class="fas fa-chart-bar f-20px icon-hide-sm"></i>
                                            <h6 class="mb-0 d-sm-grid">First Buy in <span>Amount</span></h6>
                                            <h6 class="mb-0">
                                                <?php if ($botbid == "defaultval") {
                                                    echo ("15");
                                                } else {
                                                    echo ($botbid->firstbuyinamout);
                                                } ?>
                                            </h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xsm-6 pl-3px">
                                <div class="card mb-6px">
                                    <div class="card-body p-coin-stat">
                                        <div class="d-flex justify-content-between">
                                            <i class="fas fa-chart-line f-20px icon-hide-sm"></i>
                                            <h6 class="mb-0 d-sm-grid">Margin call <span>limit</span></h6>
                                            <h6 class="mb-0">
                                                <?php if ($botbid == "defaultval") {
                                                    echo ("1");
                                                } else {
                                                    echo ($botbid->margincalllimit);
                                                }
                                                ?>
                                            </h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-xsm-6 pr-3px">
                                <div class="card mb-6px">
                                    <div class="card-body p-coin-stat">
                                        <div class="d-flex justify-content-between">
                                            <i class="fas fa-dollar-sign f-20px icon-hide-sm"></i>
                                            <h6 class="mb-0 d-sm-grid">Margin call <span>drop</span></h6>
                                            <!-- <h6 class="mb-0">-
                                                <?php if ($multibid1 == "defaultval2") {
                                                    echo ("1.5");
                                                } else {
                                                    echo ($multibid1->buypoint);
                                                }
                                                ?>
                                                %</h6> -->
                                            <h6 class="mb-0" ng-cloak ng-show="gainersforbot.<?php echo ($type) ?>['buysell'] == -1">
                                                <?php echo ($multibid3->takieprofitratio); ?>%
                                            </h6>
                                            <h6 class="mb-0" ng-cloak ng-hide="gainersforbot.<?php echo ($type) ?>['buysell'] == -1">
                                                <?php if ($multibid2->buysell == -1) {
                                                    echo ($multibid1->takieprofitratio);
                                                } elseif ($multibid2 == "defaultval3") {
                                                    echo ("0.3");
                                                } else {
                                                    echo ($multibid2->takieprofitratio);
                                                } ?>%
                                            </h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xsm-6 pl-3px">
                                <div class="card mb-6px">
                                    <div class="card-body p-coin-stat">
                                        <div class="d-flex justify-content-between">
                                            <i class="fas fa-hand-holding-usd f-20px icon-hide-sm"></i>
                                            <h6 class="mb-0 d-sm-grid">Multiple buy <span>in ratio</span></h6>
                                            <!-- <h6 class="mb-0">
                                                <?php if ($multibid1 == "defaultval2") {
                                                    echo ("1");
                                                } else {
                                                    echo ($multibid1->buyamountmul);
                                                }
                                                ?>x
                                            </h6> -->
                                            <h6 class="mb-0" ng-cloak ng-show="gainersforbot.<?php echo ($type) ?>['buysell'] == -1">
                                                <?php echo ($multibid3->multiplebuyinratio); ?>x
                                            </h6>
                                            <h6 class="mb-0" ng-cloak ng-hide="gainersforbot.<?php echo ($type) ?>['buysell'] == -1">
                                                <?php if ($multibid2->buysell == -1) {
                                                    echo ($multibid1->multiplebuyinratio);
                                                } elseif ($multibid2 == "defaultval3") {
                                                    echo ("0.3");
                                                } else {
                                                    echo ($multibid2->multiplebuyinratio);
                                                } ?>x
                                            </h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-xsm-6 pr-3px">
                                <div class="card mb-6px">
                                    <div class="card-body p-coin-stat">
                                        <div class="d-flex justify-content-between">
                                            <i class="fas fa-percent f-20px icon-hide-sm"></i>
                                            <h6 class="mb-0 d-sm-grid">Profit <span>ratio</span></h6>
                                            <!-- <h6 class="mb-0">
                                                <?php if ($multibid1 == "defaultval2") {
                                                    echo ("1.3");
                                                } else {
                                                    echo ($multibid1->sell);
                                                }
                                                ?>%
                                            </h6> -->
                                            <h6 class="mb-0" ng-cloak ng-show="gainersforbot.<?php echo ($type) ?>['buysell'] == -1">
                                                <?php echo ($multibid3->sell_point); ?>%
                                            </h6>
                                            <h6 class="mb-0" ng-cloak ng-hide="gainersforbot.<?php echo ($type) ?>['buysell'] == -1">
                                                <?php if ($multibid2->buysell == -1) {
                                                    echo ($multibid1->sell_point);
                                                } elseif ($multibid2 == "defaultval3") {
                                                    echo ("0.3");
                                                } else {
                                                    echo ($multibid2->sell_point);
                                                } ?>%
                                            </h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xsm-6 pl-3px">
                                <div class="card mb-6px">
                                    <div class="card-body p-coin-stat">
                                        <div class="d-flex justify-content-between">
                                            <i class="fas fa-hand-pointer f-20px icon-hide-sm"></i>
                                            <h6 class="mb-0 d-sm-grid">Profit <span>callback</span></h6>
                                            <!-- <h6 class="mb-0">
                                                <?php if ($multibid1 == "defaultval2") {
                                                    echo ("0.3");
                                                } else {
                                                    echo ($multibid1->sellstop);
                                                }
                                                ?>%
                                            </h6> -->
                                            <h6 class="mb-0" ng-cloak ng-show="gainersforbot.<?php echo ($type) ?>['buysell'] == -1">
                                                <?php echo ($multibid3->sell_stop_limit); ?>%
                                            </h6>
                                            <h6 class="mb-0" ng-cloak ng-hide="gainersforbot.<?php echo ($type) ?>['buysell'] == -1">
                                                <?php if ($multibid2->buysell == -1) {
                                                    echo ($multibid1->sell_stop_limit);
                                                } elseif ($multibid2 == "defaultval3") {
                                                    echo ("0.3");
                                                } else {
                                                    echo ($multibid2->sell_stop_limit);
                                                } ?>%
                                            </h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-4">
                    <div class="col-6 px-4px">
                        <div class="text-right">
                        
                            <div>
                                <a class="btn btn-dark btn-block" href="<?= base_url() ?>trade-setting/<?php echo ($type) ?>">Trade Setting</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 px-4px">
                        <!-- <div class="ml-auto mr-auto mt-lg-10 text-center" ng-show="topLoader">
                            <img class="mt-md-n8" src="<?= base_url('assets/user_panel/images/preloader2.gif') ?>" alt="loader">
                        </div> -->
                        <button  type="button" class="btn btn-primary start-btn btn-block pushme2 mt-0" ng-click="">&nbsp;&nbsp;Start</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<?php $this->load->view('trading/common/footer') ?>

<script>
    function botnotpurchase() {
        toastr.error('Error', '', toast);
    }

    function stratsaved() {
        toastr.error("Please stop the bot");
    }
</script>

<script>
    // $(".pushme2").click(function() {
    //     $(this).text(function(i, v) {
    //         return v === 'Start' ? 'Stop' : 'Start'
    //     });
    //     if ($(this).hasClass("btn-primary")) {
    //         $(this).removeClass("btn-primary");
    //         $(this).addClass("btn-danger");
    //         $(this).removeClass("start-btn");
    //         $(this).addClass("stop-btn");
    //     } else {
    //         $(this).removeClass("btn-danger");
    //         $(this).addClass("btn-primary");
    //         $(this).addClass("start-btn");
    //         $(this).removeClass("stop-btn")
    //     }
    // });

    // $("#one-shot").click(function() {
    //     if ($(".one-shot").hasClass("fa-bars")) {
    //         $(".one-shot").removeClass("fa-bars");
    //         $(".one-shot").addClass("fa-recycle");
    //         $(".one-shot-txt").text("Cycle");
    //     } else {
    //         $(".one-shot").removeClass("fa-recycle");
    //         $(".one-shot").addClass("fa-bars");
    //         $(".one-shot-txt").text("One-Shot");
    //     }
    // });

    $("#stop-margin").click(function() {
        if ($(".stop-margin").hasClass("fa-stop-circle")) {
            $(".stop-margin").removeClass("fa-stop-circle");
            $(".stop-margin").addClass("fa-play-circle");
            $(".stop-margin").addClass("text-info");
            $(".stop-margin").removeClass("text-danger");
            $(".stop-margin-txt").text("Play margin call");
        } else {
            $(".stop-margin").removeClass("fa-play-circle");
            $(".stop-margin").addClass("fa-stop-circle");
            $(".stop-margin").addClass("text-danger");
            $(".stop-margin").removeClass("text-info");
            $(".stop-margin-txt").text("Stop margin call");
        }
    })
</script>