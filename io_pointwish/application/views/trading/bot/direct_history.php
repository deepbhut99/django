<?php $this->load->view('trading/common/header') ?>
<div class="page-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <div class="web-hidden">
                    <a class="btn btn-primary" href="<?= base_url('bot-trading') ?>">Back</a>
                </div>
                <div class="card w-100 theme-border-web mt-20px">
                    <div class="card-body p-table-0">
                        <table id="datatable" class="table dt-responsive table-striped table-borderless table-team-income" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Profit</th>
                                    <th>Details</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('trading/common/footer') ?>
<script>
    let isMobile = window.matchMedia("only screen and (max-width: 760px)").matches;
    var def_columns;
    if (isMobile) {
        def_columns = [{
                "targets": [2],
                "visible": false,

            }
            // More than 2 column
        ];
    } else {

    }
    var table_history = $('#datatable').DataTable({
        destroy: true,
        pagingType: "simple",
        order: [
            [0, "asc"]
        ],
        columnDefs: def_columns
    });
    var bids = <?= json_encode($directref) ?>;
    count = 1;
    bids.forEach(function(bid) {
        table_history.row.add(['<span class="table-date"><span class="table-date-text">' + moment(bid.date).format('D-MMMM') + '</span><span class="table-dash">-</span><span class="table-year-text">' + moment(bid.date).format('YYYY') + '</span></span>', '<span class="profit-text">$35</span>', bid.detail, ]).draw(false);
    });
</script>