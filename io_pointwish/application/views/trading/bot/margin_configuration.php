<?php $this->load->view('trading/common/header'); ?>
<div class="page-content" ng-controller="Tradingbot_Controller_Margin">
    <div class="container-fluid">
        <h6><?php echo ($type) ?></h6>
        <div class="row">
            <div class="col-xsm-6">
                <a href="<?= base_url('trade-setting') ?>" class="btn btn-primary start-btn ml-3 visible-xs">Back</a>
            </div>
            <div class="col-xsm-6 col-md-12">
                <div class="text-center text-xsm-right">
                    <a class="btn btn-primary start-btn save-btn mt-sm-3">Save</a>
                </div>
            </div>
        </div>
        <div class="row mt-14px">
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 px-2">
                <div class="card theme-border mb-14px">
                    <div class="card-body">
                        <div class="card bg-6083d9 border-radius-2 mb-3">
                            <h4 class="text-center pt-1">1st Call</h4>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div>
                                    <h6 class="text-gold-new d-xsm-grid"><span>Margin call</span> <span>drop</span></h6>
                                    <div class="d-flex min-w-40px">
                                        <input value="3.5" type="number" class="form-control w-70px px-2">
                                        <div class="mt-auto mb-auto text-center">
                                            <span class="ml-2">%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div>
                                    <h6 class="text-gold-new d-xsm-grid"><span>Multiple Buy in</span> <span>ratio</span></h6>
                                    <div class="d-flex min-w-40px">
                                        <input value="2" type="number" class="form-control w-70px px-2">
                                        <div class="mt-auto mb-auto text-center">
                                            <span class="ml-2">Times</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 px-2">
                <div class="card theme-border mb-14px">
                    <div class="card-body">
                        <div class="card bg-6083d9 border-radius-2 mb-3">
                            <h4 class="text-center pt-1">2nd Call</h4>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div>
                                    <h6 class="text-gold-new d-xsm-grid"><span>Margin call</span> <span>drop</span></h6>
                                    <div class="d-flex min-w-40px">
                                        <input value="4" type="number" class="form-control w-70px px-2">
                                        <div class="mt-auto mb-auto text-center">
                                            <span class="ml-2">%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div>
                                    <h6 class="text-gold-new d-xsm-grid"><span>Multiple Buy in</span> <span>ratio</span></h6>
                                    <div class="d-flex min-w-40px">
                                        <input value="4" type="number" class="form-control w-70px px-2">
                                        <div class="mt-auto mb-auto text-center">
                                            <span class="ml-2">Times</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 px-2">
                <div class="card theme-border mb-14px">
                    <div class="card-body">
                        <div class="card bg-6083d9 border-radius-2 mb-3">
                            <h4 class="text-center pt-1">3rd Call</h4>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div>
                                    <h6 class="text-gold-new d-xsm-grid"><span>Margin call</span> <span>drop</span></h6>
                                    <div class="d-flex min-w-40px">
                                        <input value="4.5" type="number" class="form-control w-70px px-2">
                                        <div class="mt-auto mb-auto text-center">
                                            <span class="ml-2">%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div>
                                    <h6 class="text-gold-new d-xsm-grid"><span>Multiple Buy in</span> <span>ratio</span></h6>
                                    <div class="d-flex min-w-40px">
                                        <input value="8" type="number" class="form-control w-70px px-2">
                                        <div class="mt-auto mb-auto text-center">
                                            <span class="ml-2">Times</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 px-2">
                <div class="card theme-border mb-14px">
                    <div class="card-body">
                        <div class="card bg-6083d9 border-radius-2 mb-3">
                            <h4 class="text-center pt-1">4th Call</h4>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div>
                                    <h6 class="text-gold-new d-xsm-grid"><span>Margin call</span> <span>drop</span></h6>
                                    <div class="d-flex min-w-40px">
                                        <input value="5.2" type="number" class="form-control w-70px px-2">
                                        <div class="mt-auto mb-auto text-center">
                                            <span class="ml-2">%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div>
                                    <h6 class="text-gold-new d-xsm-grid"><span>Multiple Buy in</span> <span>ratio</span></h6>
                                    <div class="d-flex min-w-40px">
                                        <input value="16" type="number" class="form-control w-70px px-2">
                                        <div class="mt-auto mb-auto text-center">
                                            <span class="ml-2">Times</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 px-2">
                <div class="card theme-border mb-14px">
                    <div class="card-body">
                        <div class="card bg-6083d9 border-radius-2 mb-3">
                            <h4 class="text-center pt-1">5th Call</h4>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div>
                                    <h6 class="text-gold-new d-xsm-grid"><span>Margin call</span> <span>drop</span></h6>
                                    <div class="d-flex min-w-40px">
                                        <input value="8" type="number" class="form-control w-70px px-2">
                                        <div class="mt-auto mb-auto text-center">
                                            <span class="ml-2">%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div>
                                    <h6 class="text-gold-new d-xsm-grid"><span>Multiple Buy in</span> <span>ratio</span></h6>
                                    <div class="d-flex min-w-40px">
                                        <input value="32" type="number" class="form-control w-70px px-2">
                                        <div class="mt-auto mb-auto text-center">
                                            <span class="ml-2">Times</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 px-2">
                <div class="card theme-border mb-14px">
                    <div class="card-body">
                        <div class="card bg-6083d9 border-radius-2 mb-3">
                            <h4 class="text-center pt-1">6th Call</h4>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div>
                                    <h6 class="text-gold-new d-xsm-grid"><span>Margin call</span> <span>drop</span></h6>
                                    <div class="d-flex min-w-40px">
                                        <input value="10" type="number" class="form-control w-70px px-2">
                                        <div class="mt-auto mb-auto text-center">
                                            <span class="ml-2">%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div>
                                    <h6 class="text-gold-new d-xsm-grid"><span>Multiple Buy in</span> <span>ratio</span></h6>
                                    <div class="d-flex min-w-40px">
                                        <input value="64" type="number" class="form-control w-70px px-2">
                                        <div class="mt-auto mb-auto text-center">
                                            <span class="ml-2">Times</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 px-2">
                <div class="card theme-border mb-14px">
                    <div class="card-body">
                        <div class="card bg-6083d9 border-radius-2 mb-3">
                            <h4 class="text-center pt-1">7th Call</h4>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div>
                                    <h6 class="text-gold-new d-xsm-grid"><span>Margin call</span> <span>drop</span></h6>
                                    <div class="d-flex min-w-40px">
                                        <input value="12" type="number" class="form-control w-70px px-2">
                                        <div class="mt-auto mb-auto text-center">
                                            <span class="ml-2">%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div>
                                    <h6 class="text-gold-new d-xsm-grid"><span>Multiple Buy in</span> <span>ratio</span></h6>
                                    <div class="d-flex min-w-40px">
                                        <input value="128" type="number" class="form-control w-70px px-2">
                                        <div class="mt-auto mb-auto text-center">
                                            <span class="ml-2">Times</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('trading/common/footer'); ?>

<script>
    $('.margin-card').click(function() {
        $(this).find('input').focus();
    });
</script>