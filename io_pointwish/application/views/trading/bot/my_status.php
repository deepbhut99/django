<?php $this->load->view('trading/common/header'); ?>
<link rel="stylesheet" href="<?= base_url('assets/user_panel/css/tabs.css') ?>">
<div class="page-content mx-cryp" ng-controller="Tradingbot_Controller" ng-init="get_user_info()">
    <div class="container-fluid p-0 mt-12px">
        <div class="row pl-0.75rem show-mobile">
            <div class="col-xl-4 px-3px">
                <a>
                    <div class="header-strip header-strip-mobile">
                        Total Earning
                    </div>
                    <div class="icon-circle icon-circle-mobile">
                        <img height="40px" src="<?= base_url('assets/user_panel/images/icon/total-profit') ?>" alt="">
                    </div>
                    <div class="card my-status-card ml-stat-mobile">
                        <div class="card-body">
                            <div class="row p-mystat-card">
                                <div class="col-6 text-center">
                                    <h6 class="my-stat-text">Today's<br>Profit</h6>
                                    <h2 class="my-stat-amt"><?php if (isset($totalprofittoday)) {
                                                                echo (number_format($totalprofittoday, 3));
                                                            } else {
                                                                echo (0);
                                                            } ?></h2>
                                    </h2>
                                </div>
                                <div class="col-6 text-center">
                                    <h6 class="my-stat-text">Total<br>Profit</h6>
                                    <h2 class="my-stat-amt"><?php if (isset($totalprofit)) {
                                                                echo (number_format($totalprofit, 3));
                                                            } else {
                                                                echo (0);
                                                            } ?></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-xl-4 px-3px">
                <a href="<?= base_url('team-reward') ?>">
                    <div class="header-strip header-strip-mobile">
                        Team Reward
                    </div>
                    <div class="icon-circle icon-circle-mobile">
                        <img height="40px" src="<?= base_url('assets/user_panel/images/icon/total-profit') ?>" alt="">
                    </div>
                    <div class="card my-status-card ml-stat-mobile">
                        <div class="card-body">
                            <div class="row p-mystat-card">
                                <div class="col-6 text-center">
                                    <h6 class="my-stat-text">Today's<br>Profit</h6>
                                    <h2 class="my-stat-amt"><?= isset($todayTeamTradingIncome) ? number_format($todayTeamTradingIncome, 6) : 0 ?></h2>
                                </div>
                                <div class="col-6 text-center">
                                    <h6 class="my-stat-text">Total<br>Profit</h6>
                                    <h2 class="my-stat-amt"><?= isset($totalTeamTtradingIncome) ? number_format($totalTeamTtradingIncome, 6) : 0 ?></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-xl-4 px-3px">
                <a href="<?= base_url('bot-profit') ?>">
                    <div class="header-strip header-strip-mobile">
                        Bot Profit
                    </div>
                    <div class="icon-circle icon-circle-mobile">
                        <img height="40px" src="<?= base_url('assets/user_panel/images/icon/total-profit') ?>" alt="">
                    </div>
                    <div class="card my-status-card ml-stat-mobile">
                        <div class="card-body">
                            <div class="row p-mystat-card">
                                <div class="col-6 text-center">
                                    <h6 class="my-stat-text">Today's<br>Profit</h6>
                                    <h2 class="my-stat-amt"><?php if (isset($todasprofitforbot)) {
                                                                echo ($todasprofitforbot);
                                                            } else {
                                                                echo (0);
                                                            } ?></h2>
                                </div>
                                <div class="col-6 text-center">
                                    <h6 class="my-stat-text">Total<br>Profit</h6>
                                    <h2 class="my-stat-amt"><?php if (isset($tptalprofitforbot)) {
                                                                echo ($tptalprofitforbot);
                                                            } else {
                                                                echo (0);
                                                            } ?></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>

        <div class="show-web">
            <ul class="nav nav-tabs justify-content-center row py-0" role="tablist">
                <li class="nav-item active col-xl-4">
                    <a class="nav-link active tab-mystat" data-toggle="tab" href="#earning" role="tab">
                        <div class="header-strip">
                            Total Earning
                        </div>
                        <div class="icon-circle">
                            <img height="40px" src="<?= base_url('assets/user_panel/images/icon/total-profit') ?>" alt="">
                        </div>
                        <div class="card my-status-card">
                            <div class="card-body">
                                <div class="row p-mystat-card">
                                    <div class="col-6">
                                        <h6 class="my-stat-text">Today's<br>Profit</h6>
                                        <h2 class="my-stat-amt"><?php if (isset($totalprofittoday)) {
                                                                    echo (number_format($totalprofit, 3));
                                                                } else {
                                                                    echo (0);
                                                                } ?></h2>
                                        </h2>
                                    </div>
                                    <div class="col-6">
                                        <h6 class="my-stat-text">Total<br>Profit</h6>
                                        <h2 class="my-stat-amt"><?php if (isset($totalprofit)) {
                                                                    echo (number_format($totalprofit, 3));
                                                                } else {
                                                                    echo (0);
                                                                } ?></h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="nav-item col-xl-4">
                    <a class="nav-link tab-mystat" data-toggle="tab" href="#reward" role="tab">
                        <div class="header-strip">
                            Team Reward
                        </div>
                        <div class="icon-circle">
                            <img height="40px" src="<?= base_url('assets/user_panel/images/icon/total-profit') ?>" alt="">
                        </div>
                        <div class="card my-status-card">
                            <div class="card-body">
                                <div class="row p-mystat-card">
                                    <div class="col-6">
                                        <h6 class="my-stat-text">Today's<br>Profit</h6>
                                        <h2 class="my-stat-amt"><?= isset($todayTeamTradingIncome) ? number_format($todayTeamTradingIncome, 4) : 0 ?></h2>
                                    </div>
                                    <div class="col-6">
                                        <h6 class="my-stat-text">Total<br>Profit</h6>
                                        <h2 class="my-stat-amt"><?= isset($totalTeamTtradingIncome) ? number_format($totalTeamTtradingIncome, 4) : 0 ?></h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="nav-item col-xl-4">
                    <a class="nav-link tab-mystat" data-toggle="tab" href="#bot-profit" role="tab">
                        <div class="header-strip">
                            Bot Profit
                        </div>
                        <div class="icon-circle">
                            <img height="40px" src="<?= base_url('assets/user_panel/images/icon/total-profit') ?>" alt="">
                        </div>
                        <div class="card my-status-card">
                            <div class="card-body">
                                <div class="row p-mystat-card">
                                    <div class="col-6">
                                        <h6 class="my-stat-text">Today's<br>Profit</h6>
                                        <h2 class="my-stat-amt"><?php if (isset($todasprofitforbot)) {
                                                                    echo (number_format($todasprofitforbot, 4));
                                                                } else {
                                                                    echo (0);
                                                                } ?></h2>
                                    </div>
                                    <div class="col-6">
                                        <h6 class="my-stat-text">Total<br>Profit</h6>
                                        <h2 class="my-stat-amt"><?php if (isset($tptalprofitforbot)) {
                                                                    echo (number_format($tptalprofitforbot, 4));
                                                                } else {
                                                                    echo (0);
                                                                } ?></h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active p-3" id="earning" role="tabpanel">

                </div>
                <div class="tab-pane p-3" id="reward" role="tabpanel">
                    <h3 class="font-size-22 text-center">Team Reward</h3>
                    <div class="card w-100 mt-20px">
                        <div class="card-body p-table-0">
                            <table id="datatable" class="table display dt-responsive table-striped table-borderless table-team-income" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Profit</th>
                                        <th>Details</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="tab-pane p-3" id="bot-profit" role="tabpanel">
                    <h3 class="font-size-22 text-center">Bot Profit</h3>
                    <div class="card w-100 mt-20px">
                        <div class="card-body p-table-0">
                            <table id="datatable2" class="table display dt-responsive table-striped table-borderless table-team-income" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>OrderId</th>
                                        <th>Date</th>
                                        <th>Buy|Sell</th>
                                        <th>Symbol</th>
                                        <th>Price</th>
                                        <th>Qunatity</th>
                                        <th>Total</th>
                                        <th>Profit</th>
                                        <th>fees</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            <!-- <table id="datatable1" class="table display dt-responsive table-striped table-borderless table-team-income" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Profit</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<?php $this->load->view('trading/common/footer') ?>

<script>
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust()
            .responsive.recalc();
    });
</script>

<script>
    $(document).ready(function() {
        $('table.display').DataTable();
    });
</script>

<!-- Total Earning Script Start -->

<script>
    var table_history = $('#datatable').DataTable({
        destroy: true,
        pagingType: "simple",
        order: [
            [0, "asc"]
        ],
        columnDefs: def_columns
    });
    var bids = <?= json_encode($table1) ?>;

    count = 1;
    bids.forEach(function(bid) {
        table_history.row.add(['<span class="table-date"><span class="table-date-text">' + moment(bid.date).format('D-MMMM') + '</span><span class="table-dash">-</span><span class="table-year-text">' + moment(bid.date).format('YYYY') + '</span></span>', '<span class="profit-text">$35</span>', bid.detail, ]).draw(false);
    });


    var table_history2 = $('#datatable2').DataTable({
        destroy: true,
        pagingType: "simple",
        order: [
            [0, "asc"]
        ],
        columnDefs: def_columns
    });
    var bids1 = <?= json_encode($table2) ?>;
    count = 1;

    bids1.forEach(function(bid1) {
        var totalt = bid1.price * bid1.origQty;
        table_history2.row.add([bid1.id_whichsellorbuy, moment(bid1.created_datetime * 1000).format('DD-MM-YYYY hh:mm:ss A'), bid1.buy_or_sell == 1 ? '<span class="text-02C076">Buy</span>' : '<span class="text-danger">Sell</span>', bid1.symbol, bid1.price, bid1.origQty, totalt.toFixed(2), bid1.price_with_commision, bid1.qty_with_commision, ]).draw(false);
    });
</script>
<script>

</script>