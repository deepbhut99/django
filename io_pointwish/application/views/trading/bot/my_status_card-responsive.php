<?php $this->load->view('trading/common/header'); ?>
<link rel="stylesheet" href="<?= base_url('assets/user_panel/css/tabs.css') ?>">
<div class="page-content mx-cryp" ng-controller="Tradingbot_Controller" ng-init="get_user_info()">
    <div class="container-fluid p-0 mt-12px">
        <!-- <div class="row">
            <div class="col-xl-4 px-3px">
                <div class="header-strip">
                    Total Earning
                </div>
                <div class="icon-circle">
                    <img height="40px" src="<?= base_url('assets/user_panel/images/icon/total-profit') ?>" alt="">
                </div>
                <div class="card my-status-card">
                    <div class="card-body">
                        <div class="row p-mystat-card">
                            <div class="col-6">
                                <h6 class="my-stat-text">Today's Profit(USDT)</h6>
                                <h2 class="my-stat-amt"><?php if (isset($totalprofittoday)) {
                                                            echo (number_format($totalprofit, 3));
                                                        } else {
                                                            echo (0);
                                                        } ?></h2>
                                </h2>
                            </div>
                            <div class="col-6">
                                <h6 class="my-stat-text">Total Profit(USDT)</h6>
                                <h2 class="my-stat-amt"><?php if (isset($totalprofit)) {
                                                            echo (number_format($totalprofit, 3));
                                                        } else {
                                                            echo (0);
                                                        } ?></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 px-3px">
                <div class="header-strip">
                    Team Reward
                </div>
                <div class="icon-circle">
                    <img height="40px" src="<?= base_url('assets/user_panel/images/icon/total-profit') ?>" alt="">
                </div>
                <div class="card my-status-card">
                    <div class="card-body">
                        <div class="row p-mystat-card">
                            <div class="col-6">
                                <h6 class="my-stat-text">Today's Profit(USDT)</h6>
                                <h2 class="my-stat-amt"><?= isset($todayTeamTradingIncome) ? number_format($todayTeamTradingIncome, 6) : 0 ?></h2>
                            </div>
                            <div class="col-6">
                                <h6 class="my-stat-text">Total Profit(USDT)</h6>
                                <h2 class="my-stat-amt"><?= isset($totalTeamTtradingIncome) ? number_format($totalTeamTtradingIncome, 6) : 0 ?></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 px-3px">
                <div class="header-strip">
                    Bot Profit
                </div>
                <div class="icon-circle">
                    <img height="40px" src="<?= base_url('assets/user_panel/images/icon/total-profit') ?>" alt="">
                </div>
                <div class="card my-status-card">
                    <div class="card-body">
                        <div class="row p-mystat-card">
                            <div class="col-6">
                                <h6 class="my-stat-text">Today's Profit(USDT)</h6>
                                <h2 class="my-stat-amt"><?php if (isset($todasprofitforbot)) {
                                                            echo ($todasprofitforbot);
                                                        } else {
                                                            echo (0);
                                                        } ?></h2>
                            </div>
                            <div class="col-6">
                                <h6 class="my-stat-text">Total Profit(USDT)</h6>
                                <h2 class="my-stat-amt"><?php if (isset($tptalprofitforbot)) {
                                                            echo ($tptalprofitforbot);
                                                        } else {
                                                            echo (0);
                                                        } ?></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
    </div>
    <ul class="nav nav-tabs justify-content-center row" role="tablist">
        <li class="nav-item col-xl-4">
            <a class="nav-link active tab-mystat" data-toggle="tab" href="#topgainers" role="tab">
                <div class="header-strip">
                    Total Earning
                </div>
                <div class="icon-circle">
                    <img height="40px" src="<?= base_url('assets/user_panel/images/icon/total-profit') ?>" alt="">
                </div>
                <div class="card my-status-card">
                    <div class="card-body">
                        <div class="row p-mystat-card">
                            <div class="col-6">
                                <h6 class="my-stat-text">Today's Profit(USDT)</h6>
                                <h2 class="my-stat-amt"><?php if (isset($totalprofittoday)) {
                                                            echo (number_format($totalprofit, 3));
                                                        } else {
                                                            echo (0);
                                                        } ?></h2>
                                </h2>
                            </div>
                            <div class="col-6">
                                <h6 class="my-stat-text">Total Profit(USDT)</h6>
                                <h2 class="my-stat-amt"><?php if (isset($totalprofit)) {
                                                            echo (number_format($totalprofit, 3));
                                                        } else {
                                                            echo (0);
                                                        } ?></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </li>
        <li class="nav-item col-xl-4">
            <a class="nav-link tab-mystat" data-toggle="tab" href="#mostpopular" role="tab">
                <div class="header-strip">
                    Team Reward
                </div>
                <div class="icon-circle">
                    <img height="40px" src="<?= base_url('assets/user_panel/images/icon/total-profit') ?>" alt="">
                </div>
                <div class="card my-status-card">
                    <div class="card-body">
                        <div class="row p-mystat-card">
                            <div class="col-6">
                                <h6 class="my-stat-text">Today's Profit(USDT)</h6>
                                <h2 class="my-stat-amt"><?= isset($todayTeamTradingIncome) ? number_format($todayTeamTradingIncome, 6) : 0 ?></h2>
                            </div>
                            <div class="col-6">
                                <h6 class="my-stat-text">Total Profit(USDT)</h6>
                                <h2 class="my-stat-amt"><?= isset($totalTeamTtradingIncome) ? number_format($totalTeamTtradingIncome, 6) : 0 ?></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </li>
        <li class="nav-item col-xl-4">
            <a class="nav-link tab-mystat" data-toggle="tab" href="#mostpopular" role="tab">
                <div class="header-strip">
                    Bot Profit
                </div>
                <div class="icon-circle">
                    <img height="40px" src="<?= base_url('assets/user_panel/images/icon/total-profit') ?>" alt="">
                </div>
                <div class="card my-status-card">
                    <div class="card-body">
                        <div class="row p-mystat-card">
                            <div class="col-6">
                                <h6 class="my-stat-text">Today's Profit(USDT)</h6>
                                <h2 class="my-stat-amt"><?php if (isset($todasprofitforbot)) {
                                                            echo ($todasprofitforbot);
                                                        } else {
                                                            echo (0);
                                                        } ?></h2>
                            </div>
                            <div class="col-6">
                                <h6 class="my-stat-text">Total Profit(USDT)</h6>
                                <h2 class="my-stat-amt"><?php if (isset($tptalprofitforbot)) {
                                                            echo ($tptalprofitforbot);
                                                        } else {
                                                            echo (0);
                                                        } ?></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </li>
    </ul>
</div>
<?php $this->load->view('trading/common/footer') ?>