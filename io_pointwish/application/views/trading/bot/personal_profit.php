<?php $this->load->view('trading/common/header') ?>
<div class="page-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <div class="web-hidden">
                    <a class="btn btn-dark" href="<?= base_url('my-status') ?>">Back</a>
                </div>
                <div class="card w-100 theme-border-web mt-20px">
                    <div class="card-body">
                        <table id="datatable1" class="table dt-responsive table-striped table-borderless table-team-income" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Profit</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('trading/common/footer') ?>
<script>
    var def_columns;
    var table_history = $('#datatable1').DataTable({
        destroy: true,
        pagingType: "simple",
        order: [
            [0, "asc"]
        ]
    }).clear().draw();
    var tableContainer = $(table_history.table().container());
    tableContainer.css('display', 'none');
    $('#historyLoader').addClass('d-block');
    console.log($('#startdate').val(), $('#enddate').val());
    $.ajax({
        type: 'post',
        url: '<?= base_url("trade/trading/bot_history") ?>',
        data: {
            start: $('#startdate').val(),
            end: $('#enddate').val()
        },
        success: function(res) {
            res = JSON.parse(res);
            count = 1;
            res.forEach(function(bid) {
                var totalt = bid.price * bid.origQty;
                if (bid.buy_or_sell == 0) {
                    table_history.row.add(['<span class="table-date"><span class="table-date-text">' + moment(bid.created_datetime * 1000).format('D-MMMM') + '</span><span class="table-dash">-</span><span class="table-year-text">' + moment(bid.created_datetime * 1000).format('YYYY') + '</span></span>', '<span class="profit-text">' + bid.price_with_commision + '</span>']).draw(false);
                }
            })
            $('#historyLoader').removeClass('d-block');
            tableContainer.css('display', 'block');
        }
    })
</script>