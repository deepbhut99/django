<?php $this->load->view('trading/common/header') ?>
<div class="page-content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xsm-6">
                <a href="<?= base_url('trade-setting') ?>" class="btn btn-primary start-btn ml-3 visible-xs">Back</a>
            </div>
            <div class="col-xsm-6 col-md-12">
                <div class="text-center text-xsm-right">
                    <a class="btn btn-primary start-btn save-btn mt-sm-3">Save</a>
                </div>
            </div>
        </div>
        <div class="row mt-14px">
            <div class="col-xl-3 col-lg-2"></div>
            <div class="col-xl-6 col-lg-8">
                <div class="card theme-border w-100 mb-3">
                    <div class="card-body p-trade-card">
                        <div class="row border-bottom-trade py-1 input-focus">
                            <div class="col-6 mt-auto mb-auto">
                                <h5>5 sub-position</h5>
                            </div>
                            <div class="col-6 py-1">
                                <div class="d-flex">
                                    <input class="form-control ml-auto w-70px text-right px-2" type="number" value="1.3">
                                    <div class="min-w-40px text-center mt-auto mb-auto">
                                        <span class="mt-auto mb-auto ml-2">%</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row border-bottom-trade py-1 input-focus">
                            <div class="col-6 mt-auto mb-auto">
                                <h5>6 sub-position</h5>
                            </div>
                            <div class="col-6 py-1">
                                <div class="d-flex">
                                    <input class="form-control ml-auto w-70px text-right px-2" type="number" value="1.3">
                                    <div class="min-w-40px text-center mt-auto mb-auto">
                                        <span class="mt-auto mb-auto ml-2">%</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row py-1 input-focus">
                            <div class="col-6 mt-auto mb-auto">
                                <h5>7 sub-position</h5>
                            </div>
                            <div class="col-6 py-1">
                                <div class="d-flex">
                                    <input class="form-control ml-auto w-70px text-right px-2" type="number" value="1.3">
                                    <div class="min-w-40px text-center mt-auto mb-auto">
                                        <span class="mt-auto mb-auto ml-2">%</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-2"></div>
        </div>
    </div>
</div>
<?php $this->load->view('trading/common/footer') ?>