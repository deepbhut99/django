<?php $this->load->view('trading/common/header') ?>
<div class="page-content">
    <div class="container-fluid">
        <div class="card border-radius-0 h-45px mb-14px">
            <div class="card-body p-0">
                <ul class="nav nav-tabs nav-tabs-custom border-bottom-0" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link pt-12px pb-12px active bg-transparent p-community" data-toggle="tab" href="#private" role="tab">
                            <span class="d-block d-sm-none"><i class="fa fa-search-plus"></i></span>
                            <span class="d-none d-sm-block">Private</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link pt-12px pb-12px bg-transparent p-portfolio" data-toggle="tab" href="#public" role="tab">
                            <span class="d-block d-sm-none"><i class="fa fa-money"></i></span>
                            <span class="d-none d-sm-block">Public</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="tab-content">
            <div class="tab-pane active" id="private" role="tabpanel">
                <div class="row">
                    <div class="col-xl-3 col-lg-2"></div>
                    <div class="col-xl-6 col-lg-8">
                        <div class="card theme-border w-100">
                            <div class="card-body">
                                <div class="row border-bottom-trade py-1 input-focus">
                                    <div class="col-6 mt-auto mb-auto">
                                        <h5>Abccc</h5>
                                    </div>
                                    <div class="col-6 py-1">
                                        <div class="d-flex justify-content-end">
                                            <a href="<?= base_url('trade-setting') ?>" class="btn btn-dark px-0.65rem"><i class="fas fa-edit font-size-16"></i></a>
                                            <button class="btn btn-primary ml-2 start-btn push-btn min-w-57px">Start</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="row border-bottom-trade py-1 input-focus">
                                    <div class="col-6 mt-auto mb-auto">
                                        <h5>Abccc</h5>
                                    </div>
                                    <div class="col-6 py-1">
                                        <div class="d-flex justify-content-end">
                                            <a href="<?= base_url('trade-setting') ?>" class="btn btn-dark px-0.65rem"><i class="fas fa-edit font-size-16"></i></a>
                                            <button class="btn btn-primary ml-2 start-btn push-btn min-w-57px">Start</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="row border-bottom-trade py-1 input-focus">
                                    <div class="col-6 mt-auto mb-auto">
                                        <h5>Abccc</h5>
                                    </div>
                                    <div class="col-6 py-1">
                                        <div class="d-flex justify-content-end">
                                            <a href="<?= base_url('trade-setting') ?>" class="btn btn-dark px-0.65rem"><i class="fas fa-edit font-size-16"></i></a>
                                            <button class="btn btn-primary ml-2 start-btn push-btn min-w-57px">Start</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="row border-bottom-trade py-1 input-focus">
                                    <div class="col-6 mt-auto mb-auto">
                                        <h5>Abccc</h5>
                                    </div>
                                    <div class="col-6 py-1">
                                        <div class="d-flex justify-content-end">
                                            <a href="<?= base_url('trade-setting') ?>" class="btn btn-dark px-0.65rem"><i class="fas fa-edit font-size-16"></i></a>
                                            <button class="btn btn-primary ml-2 start-btn push-btn min-w-57px">Start</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="row py-1 input-focus">
                                    <div class="col-6 mt-auto mb-auto">
                                        <h5>Abccc</h5>
                                    </div>
                                    <div class="col-6 py-1">
                                        <div class="d-flex justify-content-end">
                                            <a href="<?= base_url('trade-setting') ?>" class="btn btn-dark px-0.65rem"><i class="fas fa-edit font-size-16"></i></a>
                                            <button class="btn btn-primary ml-2 start-btn push-btn min-w-57px">Start</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-2"></div>
                </div>
            </div>
            <div class="tab-pane" id="public" role="tabpanel">
                <div class="row">
                    <div class="col-xl-3 col-lg-2"></div>
                    <div class="col-xl-6 col-lg-8">
                        <div class="row mb-14px">
                            <div class="ml-auto mr-4">
                                <input type="text" class="form-control input-lg ng-pristine ng-untouched ng-valid ng-empty" placeholder="Search Here..">
                            </div>
                        </div>
                        <div class="card theme-border w-100">
                            <div class="card-body">
                                <div class="d-flex justify-content-end">
                                    <h6 class="font-size-16">Profit</h6>
                                    <h6 class="px-0.65rem ml-1 font-size-16">Copy</h6>
                                    <h6 class="text-center start-btn push-btn min-w-57px font-size-16">Action</h6>
                                </div>
                                <div class="row border-bottom-trade py-1 input-focus">
                                    <div class="col-6 mt-auto mb-auto">
                                        <h5 class="strategy-name">Abccc</h5>
                                    </div>
                                    <div class="col-6 py-1">
                                        <div class="d-flex justify-content-end">
                                            <h6 class="mt-auto mb-auto">3.5%</h6>
                                            <a class="btn btn-copy btn-dark px-0.65rem ml-2"><i class="far fa-copy font-size-16"></i></a>
                                            <button class="btn btn-primary ml-2 start-btn push-btn min-w-57px">Start</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="row border-bottom-trade py-1 input-focus">
                                    <div class="col-6 mt-auto mb-auto">
                                        <h5 class="strategy-name">Abccc</h5>
                                    </div>
                                    <div class="col-6 py-1">
                                        <div class="d-flex justify-content-end">
                                            <h6 class="mt-auto mb-auto">5.2%</h6>
                                            <a class="btn btn-copy btn-dark px-0.65rem ml-2"><i class="far fa-copy font-size-16"></i></a>
                                            <button class="btn btn-primary ml-2 start-btn push-btn min-w-57px">Start</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="row border-bottom-trade py-1 input-focus">
                                    <div class="col-6 mt-auto mb-auto">
                                        <h5 class="strategy-name">Abccc</h5>
                                    </div>
                                    <div class="col-6 py-1">
                                        <div class="d-flex justify-content-end">
                                            <h6 class="mt-auto mb-auto">10.5%</h6>
                                            <a class="btn btn-copy btn-dark px-0.65rem ml-2"><i class="far fa-copy font-size-16"></i></a>
                                            <button class="btn btn-primary ml-2 start-btn push-btn min-w-57px">Start</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="row border-bottom-trade py-1 input-focus">
                                    <div class="col-6 mt-auto mb-auto">
                                        <h5 class="strategy-name">Abccc</h5>
                                    </div>
                                    <div class="col-6 py-1">
                                        <div class="d-flex justify-content-end">
                                            <h6 class="mt-auto mb-auto">56.5%</h6>
                                            <a class="btn btn-copy btn-dark px-0.65rem ml-2"><i class="far fa-copy font-size-16"></i></a>
                                            <button class="btn btn-primary ml-2 start-btn push-btn min-w-57px">Start</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="row py-1 input-focus">
                                    <div class="col-6 mt-auto mb-auto">
                                        <h5 class="strategy-name">Abccc</h5>
                                    </div>
                                    <div class="col-6 py-1">
                                        <div class="d-flex justify-content-end">
                                            <h6 class="mt-auto mb-auto">99.99%</h6>
                                            <a class="btn btn-copy btn-dark px-0.65rem ml-2"><i class="far fa-copy font-size-16"></i></a>
                                            <button class="btn btn-primary ml-2 start-btn push-btn min-w-57px">Start</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-2"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('trading/common/footer') ?>

<script>
    $(".push-btn").click(function() {
        $(this).text(function(i, v) {
            return v === 'Start' ? 'Stop' : "Start"
        });
        if ($(this).hasClass("btn-primary")) {
            $(this).removeClass("btn-primary");
            $(this).addClass("btn-danger");
            $(this).removeClass("start-btn");
            $(this).addClass("stop-btn");
        } else {
            $(this).removeClass("btn-danger");
            $(this).addClass("btn-primary");
            $(this).removeClass("stop-btn");
            $(this).addClass("start-btn");
        }
    });
</script>