<?php $this->load->view('trading/common/header'); ?>
<div class="page-content" ng-controller="Tradingbot_TradeSetting">
    <div class="container-fluid">
        <div class="row">
            <div>
                <a href="<?= base_url() ?>coin-status/<?php echo ($type) ?>" class="btn btn-dark start-btn visible-xs ml-1 mb-1">Back</a>
            </div>
            <div class="col-12">
                <form method="post">
                    <div class="row mt-14px">
                        <!-- <div class="card theme-border w-100 mb-3">
                            <div class="card-body py-3">
                                <div class="row input-focus">
                                    <div class="col-6 pt-2 mt-auto mb-auto">
                                        <h5>Name</h5>
                                    </div>
                                    <div class="col-6 mt-auto mb-auto">
                                        <input class="form-control ml-auto w-50 text-right px-2" type="text" placeholder="Edit your name here..." maxlength="20">
                                    </div>
                                </div>
                            </div>
                        </div> -->
                        <div class="card w-100 mb-6px">
                            <div class="card-body py-3">
                                <div class="row input-focus">
                                    <div class="col-6 pt-2 mt-auto mb-auto">
                                        <h5>First Buy in Amount</h5>
                                    </div>
                                    <div class="col-6 mt-auto mb-auto">
                                        <div class="d-flex">
                                            <input class="form-control ml-auto w-70px text-right px-2" name="tradamount" step="any" id="tradamount" ng-init="tradamount = 15" type="number" ng-model="tradamount" placeholder="Trade Amount" required="">
                                            <div class="min-w-40px text-center mt-auto mb-auto">
                                                <span class="mt-auto mb-auto ml-2">USDT</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="card theme-border w-100 mb-3">
                            <div class="card-body py-3">
                                <div class="row">
                                    <div class="col-6">
                                        <h5 class="mb-0">Strategy Type</h5>
                                    </div>
                                    <div class="col-6 text-right">
                                        <form class="font-size-16">
                                            <input class="cursor-pointer" type="radio" id="public" name="status" value="Public">
                                            <label class="mb-0" for="Public">Public</label>
                                            <input class="cursor-pointer" type="radio" id="private" name="status" value="Private">
                                            <label class="mb-0" for="Private">Private</label>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                        <!-- <div class="card theme-border w-100 mb-3">
                            <div class="card-body p-trade-card">
                                <div class="row">
                                    <div class="col-6 mt-auto mb-auto">
                                        <h5>Open Position doubled</h5>
                                    </div>
                                    <div class="col-6 mt-auto mb-auto">
                                        <div class="text-right mt-1 pt-2">
                                            <input type="checkbox" id="switch3" switch="bool" />
                                            <label for="switch3" data-on-label="On" data-off-label="Off"></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->

                        <div class="card w-100 mb-6px">
                            <div class="card-body p-trade-card">
                                <div class="row border-bottom-trade py-1 input-focus">
                                    <div class="col-6 mt-auto mb-auto">
                                        <h5>Coin</h5>
                                    </div>
                                    <div class="col-6 py-1">
                                        <div class="d-flex">
                                            <input class="form-control ml-auto w-93px text-right px-2" name="odertype" id="odertype" readonly="readonly" value="<?php echo ($type) ?>" required="">
                                            <div class="min-w-40px text-center mt-auto mb-auto">
                                                <span class="mt-auto mb-auto ml-2">Coin</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row border-bottom-trade py-1 input-focus">
                                    <div class="col-6 mt-auto mb-auto">
                                        <h5>Margin call limit</h5>
                                    </div>
                                    <div class="col-6 py-1">
                                        <div class="d-flex">
                                            <input class="form-control ml-auto w-70px text-right px-2" type="number" oninput="myfunction()" id="margincall" ng-model="margincall">
                                            <div class="min-w-40px text-center mt-auto mb-auto">
                                                <span class="mt-auto mb-auto ml-2">Time</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="row border-bottom-trade py-1 input-focus">
                                    <div class="col-6 mt-auto mb-auto">
                                        <h5>Whole position take profit ratio</h5>
                                    </div>
                                    <div class="col-6 py-1">
                                        <div class="d-flex">
                                            <input class="form-control ml-auto w-70px text-right px-2" name="sell" id="sell" step="any" ng-model="sell" type="number" placeholder="" required="">
                                            <div class="min-w-40px text-center mt-auto mb-auto">
                                                <span class="mt-auto mb-auto ml-2">%</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row border-bottom-trade py-1 input-focus">
                                    <div class="col-6 mt-auto mb-auto">
                                        <h5>Whole position take profit callback</h5>
                                    </div>
                                    <div class="col-6 py-1">
                                        <div class="d-flex">
                                            <input class="form-control ml-auto w-70px text-right px-2" name="sellstoplimit" step="any" id="sellstoplimit" ng-model="sellstoplimit" type="number" placeholder="Sell Limite" required="">
                                            <div class="min-w-40px text-center mt-auto mb-auto">
                                                <span class="mt-auto mb-auto ml-2">%</span>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->

                                <div class="cursor-pointer" >
                                    <div class="row border-bottom-trade py-1 text-white" title="click here">
                                        <div class="col-6 mt-auto mb-auto">
                                            <h5>Margin Configuration</h5>
                                        </div>
                                        <div class="col-6 mt-auto mb-auto py-1">
                                            <div class="text-right">
                                                <button class="btn p-0"><i class="py-0.65rem fas fa-chevron-right pr-10px"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="cursor-pointer" id="inputbox">

                                </div>

                                <!-- <div class="row border-bottom-trade py-1 input-focus">
                                    <div class="col-6 mt-auto mb-auto">
                                        <h5>Buy in Callback</h5>
                                    </div>
                                    <div class="col-6 py-1">
                                        <div class="d-flex">
                                            <input class="form-control ml-auto w-70px text-right px-2" name="buyincallback" step="any" id="buyincallback" ng-model="buyincallback" type="number" placeholder="" required="">
                                            <div class="min-w-40px text-center mt-auto mb-auto">
                                                <span class="mt-auto mb-auto ml-2">%</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <a data-toggle="modal" data-target="#dist-profit">
                                    <div class="row border-bottom-trade py-1 text-white" title="click here">
                                        <div class="col-6 mt-auto mb-auto">
                                            <h5>Distributed and Take Profit Allocation</h5>
                                        </div>
                                        <div class="col-6 py-1">
                                            <div class="text-right">
                                                <i class="py-0.65rem fas fa-chevron-right pr-10px"></i>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                                <div class="row py-1 take-profit input-focus">
                                    <div class="col-6 mt-auto mb-auto">
                                        <h5>Sub-position take-profit callback</h5>
                                    </div>
                                    <div class="col-6 py-1">
                                        <div class="d-flex">
                                            <input class="form-control ml-auto w-70px text-right px-2 input-take-profit" step="any" name="takeprofitcall" id="takeprofitcall" ng-model="takeprofitcall" type="number" placeholder="" required="">
                                            <div class="min-w-40px text-center mt-auto mb-auto">
                                                <span class="mt-auto mb-auto ml-2">%</span>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                        <div class="col-6 px-3px">
                            <button id="resetbtn" class="btn btn-dark btn-block">Default</button>
                        </div>
                        <div class="col-6 px-3px">
                            <button class="btn start-btn btn-primary btn-block" ng-click="savedata()">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>





<div class="modal fade" id="profit-percentage">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content bg-transparent border-0">
            <div class="modal-body">
                <div class="row" style="padding-left: 10px; padding-right:10px;">
                    <div class="card" style="width:100%;">
                        <div class="card-body pb-4">
                            <button type="button" class="close text-d9d9d9" ng-click="hide_modal()" data-dismiss="modal" aria-hidden="true">x</button>
                            <h4 class="card-title text-left" style="border-bottom: 1px solid #343b51; padding-bottom:20px;">Profit Percentage</h4>
                            <form id="data_form" name="data_form" method="POST">
                                <div class="row">
                                    <div class="col-md-10 pr-0">
                                        <label class="font-14 bold mb-2">Profit</label>
                                        <input required type="number" class="theme-input-style form-control required" placeholder="Enter Profit Percentage">
                                    </div>
                                    <div class="col-md-2 text-center mt-auto">
                                        <button type="submit" class="btn btn-primary mt-1-3rem waves-effect waves-light mr-1 start-btn">Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>



<div class="modal fade" id="dist-profit">
    <div class="modal-dialog modal-dialog-centered">
        <!-- <div> -->
        <div class="modal-content bg-transparent border-0">
            <div class="modal-body px-2">
                <div class="card w-100">
                    <div class="card-body pb-4">
                        <button type="button" class="close text-d9d9d9" ng-click="hide_modal()" data-dismiss="modal" aria-hidden="true">x</button>
                        <h4 class="card-title mb-0 text-left" style="border-bottom: 1px solid #343b51; padding-bottom:20px;">Distributed and Take Profit Allocation</h4>
                        <div class="row border-bottom-trade py-1 input-focus">
                            <div class="col-6 mt-auto mb-auto">
                                <h5>5 sub-position</h5>
                            </div>
                            <div class="col-6 py-1">
                                <div class="d-flex">
                                    <input class="form-control ml-auto w-70px text-right px-2" type="number" value="1.3">
                                    <div class="min-w-40px text-center mt-auto mb-auto">
                                        <span class="mt-auto mb-auto ml-2">%</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row border-bottom-trade py-1 input-focus">
                            <div class="col-6 mt-auto mb-auto">
                                <h5>6 sub-position</h5>
                            </div>
                            <div class="col-6 py-1">
                                <div class="d-flex">
                                    <input class="form-control ml-auto w-70px text-right px-2" type="number" value="1.3">
                                    <div class="min-w-40px text-center mt-auto mb-auto">
                                        <span class="mt-auto mb-auto ml-2">%</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row py-1 input-focus">
                            <div class="col-6 mt-auto mb-auto">
                                <h5>7 sub-position</h5>
                            </div>
                            <div class="col-6 py-1">
                                <div class="d-flex">
                                    <input class="form-control ml-auto w-70px text-right px-2" type="number" value="1.3">
                                    <div class="min-w-40px text-center mt-auto mb-auto">
                                        <span class="mt-auto mb-auto ml-2">%</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <button class="btn btn-primary start-btn">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>




<?php $this->load->view('trading/common/footer'); ?>

<script>
    $(document).ready(function() {
        $('input:radio[id=public]').change(function() {
            if (this.value == 'Public') {
                $('#profit-percentage').modal('show');
            }
        });

    });

    $('.input-focus').click(function() {
        $(this).find('input').focus();
    });

    $("#resetbtn").click(function() {
        $("#tradamount").val("15");
        $("#margincall").val("1");
        $("#sell").val("1.3");
        $("#sellstoplimit").val("0.3")
    });

    function myfunction() {
        var calllimite = document.getElementById('margincall').value;
        var newdata = Number(calllimite);
        var setnumber = 20;
        if (newdata == 0) {
            toastr.error("Please Correct value insert Margin call limit")
        } else if (newdata > Number(setnumber)) {
            toastr.error("Margin call limit is greater than 20")
        } else {
            const numberofbox = [];
            const element = document.getElementById("inputbox");
            var mylist = '<div class="row mt-3"><div class="col-6"><h5 class="text-center text-theme-green mr-21px">Margin call drop</h5><h5 class="text-center text-danger mr-21px">Position profit ratio</h5></div><div class="col-6"><h5 class="text-center text-theme-green mr-17px">Multiple Buy in ratio</h5><h5 class="text-center text-danger mr-17px">Profit callback</h5></div></div><div class="margin-scroll">';
            for (let i = 0; i < newdata; i++) {
                numberofbox.push(i)
                var id = i + 1;
                mylist += '<div class="model-margin"> <div class="row"> <div class="col-12 mt-2"> <h6 class="text-theme-green">' + id + 'st Call Buy</h6> </div> <div class="col-6"> <div class="d-flex min-w-40px"><input value="' + (id + 0.5) + '" placeholder="' + (id + 0.5) + '" step="any" id="buy' + id + '" type="number" class="form-control px-2"> <div class="mt-auto mb-auto text-center"><span class="ml-2">%</span></div> </div> </div> <div class="col-6"> <div class="d-flex min-w-40px"><input value="1" placeholder="1" step="any" id="buyratio' + id + '" type="number" class="form-control px-2"> <div class="mt-auto mb-auto text-center"><span class="ml-2">x</span></div> </div> </div> </div> <div class="row"> <div class="col-12 mt-2"> <h6 class="text-danger">' + id + 'st Call Sell</h6> </div> <div class="col-6"> <div class="d-flex min-w-40px"><input value="1.3" placeholder="1.3" step="any" id="sellprofit' + id + '" type="number" class="form-control px-2"> <div class="mt-auto mb-auto text-center"><span class="ml-2">%</span></div> </div> </div> <div class="col-6"> <div class="d-flex min-w-40px"><input placeholder="0.3" value="0.3" step="any" id="sellstoploss' + id + '" type="number" class="form-control px-2"> <div class="mt-auto mb-auto text-center"><span class="ml-2">%</span></div> </div> </div> </div> </div>'
            }

            mylist += '</div>';
            element.innerHTML = mylist;
            document.getElementById("numberofbid").value = newdata;

        }
    }
</script>