</div>
</div>
<!-- end main content-->
</div>
<!-- END layout-wrapper -->



<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>

<!-- JAVASCRIPT -->
<script src="<?= base_url('assets/user_panel/js/jquery.validate.js'); ?>"></script>
<script src="<?= base_url('assets/user_panel/libs/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>
<script src="<?= base_url('assets/user_panel/libs/metismenu/metisMenu.min.js') ?>"></script>
<script src="<?= base_url('assets/user_panel/libs/simplebar/simplebar.min.js') ?>"></script>
<script src="<?= base_url('assets/user_panel/libs/node-waves/waves.min.js') ?>"></script>


<!-- Required datatable js -->
<script src="<?= base_url('assets/user_panel/libs/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('assets/user_panel/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js') ?>"></script>

<!-- Responsive examples -->
<script src="<?= base_url('assets/user_panel/libs/datatables.net-responsive/js/dataTables.responsive.min.js') ?>"></script>
<script src="<?= base_url('assets/user_panel/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') ?>"></script>

<!-- Sweet Alerts js -->
<script src="<?= base_url('assets/user_panel/libs/sweetalert2/sweetalert2.min.js') ?>"></script>

<!-- owlcarousal js -->
<script src="<?= base_url('assets/user_panel/js/owl.carousel.js') ?>"></script>

<!-- Sweet alert init js-->
<script src="<?= base_url('assets/user_panel/js/pages/sweet-alerts.init.js') ?>"></script>

<?php if ($title != 'Trading') { ?>
    <!-- Plugins js -->
    <script src="<?= base_url('assets/user_panel/libs/dropzone/min/dropzone.min.js') ?>"></script>
<?php } ?>

<script src="<?= base_url('assets/user_panel/js/multislider.min.js') ?>"></script>


<script src="<?= base_url('assets/user_panel/libs/morris.js/morris.min.js') ?>"></script>
<script src="<?= base_url('assets/user_panel/libs/raphael/raphael.min.js') ?>"></script>

<script src="<?= base_url('assets/user_panel/libs/toastr/toastr.min.js') ?>"></script>

<script src="<?= base_url('assets/user_panel/js/app.js') ?>"></script>
<script src="<?= base_url('assets/user_panel/js/mask.js') ?>"></script>
<script src="<?= base_url('assets/user_panel/js/myapp.js?v=' . rand()) ?>"></script>

</body>

</html>
<script>
    let isMobile = window.matchMedia("only screen and (max-width: 760px)").matches;
    var def_columns;
    if (isMobile) {

    } else {
        $("#menumain").addClass("d-none");
    }
</script>
<script>
    // INCLUDE JQUERY & JQUERY UI 1.12.1
    $(function() {
        $("#startdate").datepicker({
            dateFormat: "yy-mm-dd",
            duration: "fast"
        });
        $("#enddate").datepicker({
            dateFormat: "yy-mm-dd",
            duration: "fast"
        });
        $("#start_date").datepicker({
            dateFormat: "yy-mm-dd",
            duration: "fast"
        });
        $("#end_date").datepicker({
            dateFormat: "yy-mm-dd",
            duration: "fast"
        });
        $("#earn_start").datepicker({
            dateFormat: "yy-mm-dd",
            duration: "fast"
        });
        $("#earn_end").datepicker({
            dateFormat: "yy-mm-dd",
            duration: "fast"
        });
    });
</script>




<?php
if ($this->session->flashdata('error_message')) {
?>
    <script type="text/javascript">
        $(document).ready(function() {
            Swal.fire({
                title: "Error!",
                text: "<?php echo $this->session->flashdata('error_message'); ?>",
                type: "error",
                confirmButtonClass: "btn btn-success mt-2",
                buttonsStyling: !1
            });

        });
    </script>
<?php
}
?>

<?php
if ($this->session->flashdata('warning_message')) {
?>
    <script type="text/javascript">
        $(document).ready(function() {
            Swal.fire({
                title: "Warning!",
                text: "<?php echo $this->session->flashdata('warning_message'); ?>",
                type: "warning",
                confirmButtonClass: "btn btn-success mt-2",
                buttonsStyling: !1
            });
        });
    </script>
<?php
}
?>

<?php
if ($this->session->flashdata('success_message')) {
?>
    <script type="text/javascript">
        $(document).ready(function() {
            Swal.fire({
                title: "Done!",
                text: "<?php echo $this->session->flashdata('success_message'); ?>",
                type: "success",
                confirmButtonClass: "btn btn-success mt-2",
                buttonsStyling: !1
            });
        });
    </script>
<?php
}
?>
<script>
    if ($(window).width() < 992) {
        $("#vertical-menu-btn").click(function() {
            if ($("#main-head").hasClass("active")) {
                $("#main-head").removeClass("active");
            } else {
                $("#main-head").addClass("active");
            }
        })
    }

    $(".sub-menu-sec").click(function() {
        var target = $(this).parent().children(".sub-menu-section");
        $(target).slideToggle();
    });
</script>
<script>
    $(function() {
        $('#picture').on('click', function() {
            $('#fileinput').trigger('click');
        });
    });
</script>

<!-- <script>
    $("#profile-menu").click(function() {
        
    })
</script> -->