<!doctype html>
<html lang="en" ng-app="pwt">

<head>
    <meta charset="utf-8" />
    <title>PointWish: <?php if (!empty($title)) echo $title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta content="<?= $title ?>" name="description" />
    <meta http-equiv="Content-Security-Policy" content="default-src data: gap://* file://* https://ssl.gstatic.com *; img-src 'self' * data:; style-src 'self' 'unsafe-inline' *; script-src 'self' 'unsafe-eval' 'unsafe-inline' *; connect-src 'self' * ws://* wss://*;">


    <script src="<?= base_url('assets/user_panel/libs/jquery/jquery.min.js') ?>"></script>
    <link href="<?= base_url('assets/user_panel/css/multi-form.css') ?>" rel="stylesheet" type="text/css" />
    <script src="<?= base_url('assets/user_panel/js/multi-form.js') ?>"></script>
    <script>
        let user = <?= $this->session->logsid ?>;
        window.addEventListener("focus", function(event) {
            $.ajax({
                type: "POST",
                data: {
                    userid: user,
                },
                url: '<?php echo base_url(); ?>login/is_activeUser',
                dataType: "json",
                success: function(response) {
                    if (response.is_active == 0) {
                        window.location = "<?= base_url('logout'); ?>"
                    }
                }

            });
        }, true);
    </script>

    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
    <script>
        window.OneSignal = window.OneSignal || [];
        OneSignal.push(function() {
            OneSignal.init({
                appId: "2c5c4c22-03b4-48d3-aad9-54e0ba5dcd83",
            });
        });
    </script>



    <?php $session_data = getSessionData(); ?>

    <!-- App favicon -->
    <link rel="shortcut icon" href="<?= base_url('assets/front/newthem/img/logo/pointwish.png') ?>">

    <link href="<?= base_url('assets/user_panel/libs/chartist/chartist.min.css') ?>" rel="stylesheet">

    <!-- DataTables -->
    <link href="<?= base_url('assets/user_panel/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css') ?>" rel="stylesheet" type="text/css" />
    <link href="<?= base_url('assets/user_panel/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css') ?>" rel="stylesheet" type="text/css" />

    <!-- Responsive datatable examples -->
    <link href="<?= base_url('assets/user_panel/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') ?>" rel="stylesheet" type="text/css" />

    <!-- Sweet Alert-->
    <link href="<?= base_url('assets/user_panel/libs/sweetalert2/sweetalert2.min.css') ?>" rel="stylesheet" type="text/css" />

    <!-- Plugins css -->
    <link href="<?= base_url('assets/user_panel/libs/dropzone/min/dropzone.min.css') ?>" rel="stylesheet" type="text/css" />

    <!-- Bootstrap Css -->
    <?php
    if ($title != 'Tradding') {
    ?>
        <link href="<?= base_url('assets/user_panel/css/bootstrap.min.css') ?>" id="bootstrap-style" rel="stylesheet" type="text/css" />
    <?php
    }
    ?>

    <!-- owlCarousel css -->
    <link rel="stylesheet" href="<?= base_url('assets/user_panel/css/owl.carousel.min.css') ?>">

    <!-- Icons Css -->
    <link href="<?= base_url('assets/user_panel/css/icons.min.css') ?>" rel="stylesheet" type="text/css" />

    <!-- Pricing table scss-->
    <link href="<?= base_url('assets/user_panel/pricing-tables.css') ?>" rel="stylesheet" type="text/css" />

    <!-- App Css-->
    <link href="<?= base_url('assets/user_panel/css/datepicker.css') ?>" id="app-style" rel="stylesheet" type="text/css" />
    <link href="<?= base_url('assets/user_panel/css/app.min.css?v=2') ?>" rel="stylesheet" type="text/css" />

    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tailwindcss/2.2.19/tailwind.min.css"> -->
    <link rel="stylesheet" href="<?= base_url('assets/user_panel/css/sidebar.css?v=' . rand()) ?>">
    <link rel="stylesheet" href="<?= base_url('assets/user_panel/css/style.css?v=' . rand()) ?>">

    <!-- Winer page css -->
    <link href="<?= base_url('assets/user_panel/css/winner.css') ?>" rel="stylesheet" type="text/css" />

    <!-- Toastr css -->
    <link href="<?= base_url('assets/user_panel/libs/toastr/toastr.min.css') ?>" rel="stylesheet" type="text/css" />

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css">


    <script src="<?= base_url('assets/user_panel/js/jquery-ui.min.js') ?>"></script>

    <script src="<?= base_url('assets/user_panel/libs/angular/angular.min.js') ?>"></script>
    <script src="<?= base_url('assets/user_panel/js/angular-datatables.min.js') ?>"></script>

    <script src="<?= base_url('assets/user_panel/js/moment.min.js') ?>"></script>

    <script>
        var BASE_URL = "<?php echo base_url(); ?>";
    </script>
    <?php
    // $session_data = getSessionData();
    $amount = getWalletAmountByUserID($this->session->user_id);
    $live_amount = $amount[3];
    $demo_amount = $amount[1];
    ?>
    <script>
        sessionStorage.clear();
        sessionStorage.setItem('live-amount', <?= $live_amount; ?>);
        sessionStorage.setItem('demo-amount', <?= $demo_amount; ?>);

        var toast = {
            timeOut: 1e3,
            closeButton: !0,
            debug: !1,
            newestOnTop: !0,
            progressBar: !0,
            positionClass: "toast-top-right",
            preventDuplicates: !0,
            onclick: null,
            showDuration: "300",
            hideDuration: "1000",
            extendedTimeOut: "1000",
            showEasing: "swing",
            hideEasing: "linear",
            showMethod: "fadeIn",
            hideMethod: "fadeOut",
            tapToDismiss: !1
        }
    </script>
</head>
<div id="preloader"></div>

<body data-sidebar="dark" ng-controller="App_Controller" class="sidebar-close vertical-collpsed">
    <!-- Begin page -->
    <div id="layout-wrapper">
        <header id="page-topbar">
            <div class="preload-stopper navbar-header h-55px">
                <div class="d-flex w-100 justify-content-between">
                    <button type="button" class="btn btn-sm px-3 font-size-24 header-item" id="vertical-menu-btn">
                        <i class="mdi mdi-menu" id="menumain"></i>
                    </button>
                    <?php if ($title != "Trading") { ?>
                        <h4 class="header-page-name" style="color: white!important;"><?php echo $title ?></h4>
                    <?php } else { ?>
                        <h4 class="header-page-name title-trade" style="color: white!important;"><?php echo $title1 ?></h4>
                    <?php } ?>
                </div>
            </div>
        </header>
        <!-- ========== Left Sidebar Start ========== -->

        <div class="main-d-flex">

            <div id="main-head" class="main-sidebar">
                <!--- more free and premium Tailwind CSS components at https://tailwinduikit.com/ --->
                <div class="sidebar-section">
                    <div class="sidebar-sec-2">
                        <div class="sidebar-item sub-menu-sec">
                            <div class="d-flex">
                                <i class="sidebar-icon fas fa-user"></i>
                                <p class="sidebar-text">Profile</p>
                            </div>
                            <i class="sidebar-dropdown-icon fas fa-chevron-down"></i>
                        </div>
                        <div class="sub-menu-section <?php if ($activetitle == "kyc" || $activetitle == "crypto-address" || $activetitle == "change-pwd" || $activetitle == "2fa" || $activetitle == "apikey") { ?>active<?php } ?>">
                            <div class="sub-menu <?php if ($activetitle == "kyc") { ?>activate<?php } ?>">
                                <a href="<?= base_url('kyc') ?>">
                                    <div class="sidebar-item">
                                        <i class="sidebar-icon far fa-user-check"></i>
                                        <p class="sidebar-text">Kyc Verification</p>
                                    </div>
                                </a>
                            </div>
                            <div class="sub-menu <?php if ($activetitle == "crypto-address") { ?>activate<?php } ?>">
                                <a href="<?= base_url('crypto-address') ?>">
                                    <div class="sidebar-item">
                                        <i class="sidebar-icon far fa-user-clock"></i>
                                        <p class="sidebar-text">Crypto Address</p>
                                    </div>
                                </a>
                            </div>
                            <div class="sub-menu <?php if ($activetitle == "change-pwd") { ?>activate<?php } ?>">
                                <a href="<?= base_url('change-password') ?>">
                                    <div class="sidebar-item">
                                        <i class="sidebar-icon far fa-lock-alt"></i>
                                        <p class="sidebar-text">Change Password</p>
                                    </div>
                                </a>
                            </div>
                            <div class="sub-menu <?php if ($activetitle == "2fa") { ?>activate<?php } ?>">
                                <a href="<?= base_url('2fa') ?>">
                                    <div class="sidebar-item">
                                        <i class="sidebar-icon far fa-user-shield"></i>
                                        <p class="sidebar-text">2FA</p>
                                    </div>
                                </a>
                            </div>
                            <div class="sub-menu <?php if ($activetitle == "apikey") { ?>activate<?php } ?>">
                                <a href="<?= base_url('apikey') ?>">
                                    <div class="sidebar-item">
                                        <i class="sidebar-icon fas fa-key"></i>
                                        <p class="sidebar-text">API Key</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="sidebar-sec-2">
                        <div class="sidebar-item sub-menu-sec">
                            <div class="d-flex">
                                <i class="sidebar-icon fas fa-robot"></i>
                                <p class="sidebar-text">Bot</p>
                            </div>
                            <i class="sidebar-dropdown-icon fas fa-chevron-down"></i>
                        </div>
                        <div class="sub-menu-section <?php if ($activetitle == "my-status" || $activetitle == "binance" || $activetitle == "wazirx" || $activetitle == "binance-future" || $activetitle == "bot-history") { ?>active<?php } ?>">
                            <div class="sub-menu <?php if ($activetitle == "my-status") { ?>activate<?php } ?>">
                                <a href="<?= base_url('my-status') ?>">
                                    <div class="sidebar-item">
                                        <i class="sidebar-icon far fa-user-clock"></i>
                                        <p class="sidebar-text">My Status</p>
                                    </div>
                                </a>
                            </div>
                            <div class="sub-menu <?php if ($activetitle == "binance") { ?>activate<?php } ?>">
                                <a href="<?= base_url('binance') ?>">
                                    <div class="sidebar-item">
                                        <img height="20px" class="sidebar-icon binance-icon" src="<?= base_url('assets/user_panel/images/icon/binance.svg') ?>" alt="">
                                        <p class="sidebar-text">Binance Spot</p>
                                    </div>
                                </a>
                            </div>
                            <!-- <div class="sub-menu <?php if ($activetitle == "wazirx") { ?>activate<?php } ?>">
                                <div class="sidebar-item">
                                    <img height="20px" class="sidebar-icon wazirx-icon" src="<?= base_url('assets/user_panel/images/icon/wazirx.svg') ?>" alt="">
                                    <a href="<?= base_url('wazirx') ?>">
                                        <p class="sidebar-text">WazirX</p>
                                    </a>
                                </div>
                            </div>
                            <div class="sub-menu <?php if ($activetitle == "binance-future") { ?>activate<?php } ?>">
                                <div class="sidebar-item">
                                    <img height="20px" class="sidebar-icon binance-icon" src="<?= base_url('assets/user_panel/images/icon/binance.svg') ?>" alt="">
                                    <a href="<?= base_url('binance-future') ?>">
                                        <p class="sidebar-text">Binance Future</p>
                                    </a>
                                </div>
                            </div> -->
                            <div class="sub-menu <?php if ($activetitle == "bot-history") { ?>activate<?php } ?>">
                                <a href="<?= base_url('bot-history') ?>">
                                    <div class="sidebar-item">
                                        <i class="sidebar-icon fas fa-history"></i>
                                        <p class="sidebar-text">Trade History</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="sidebar-sec-2">
                        <div class="sidebar-item sub-menu-sec">
                            <div class="d-flex">
                                <i class="sidebar-icon fas fa-copy"></i>
                                <p class="sidebar-text">Copy Trade</p>
                            </div>
                            <i class="sidebar-dropdown-icon fas fa-chevron-down"></i>
                        </div>
                        <div class="sub-menu-section <?php if ($activetitle == "community" || $activetitle == "portfolio" || $activetitle == "expert-area") { ?>active<?php } ?>">
                            <div class="sub-menu <?php if ($activetitle == "community") { ?>activate<?php } ?>">
                                <a href="<?= base_url('community') ?>">
                                    <div class="sidebar-item">
                                        <i class="sidebar-icon far fa-users"></i>
                                        <p class="sidebar-text">Community</p>
                                    </div>
                                </a>
                            </div>
                            <div class="sub-menu <?php if ($activetitle == "portfolio") { ?>activate<?php } ?>">
                                <a href="<?= base_url('portfolio') ?>">
                                    <div class="sidebar-item">
                                        <i class="sidebar-icon far fa-folder-open"></i>
                                        <p class="sidebar-text">Portfolio</p>
                                    </div>
                                </a>
                            </div>
                            <div class="sub-menu <?php if ($activetitle == "expert-area") { ?>activate<?php } ?>">
                                <a href="<?= base_url('expert-area') ?>">
                                    <div class="sidebar-item">
                                        <i class="sidebar-icon far fa-user-cog"></i>
                                        <p class="sidebar-text">Expert Area</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="sidebar-sec-2">
                        <div class="sidebar-item sub-menu-sec">
                            <div class="d-flex">
                                <i class="sidebar-icon fas fa-wallet"></i>
                                <p class="sidebar-text">Wallet</p>
                            </div>
                            <i class="sidebar-dropdown-icon fas fa-chevron-down"></i>
                        </div>
                        <div class="sub-menu-section <?php if ($activetitle == "deposite-wallet" || $activetitle == "withdraw-wallet" || $activetitle == "transaction-wallet") { ?>active<?php } ?>">
                            <div class="sub-menu <?php if ($activetitle == "deposite-wallet") { ?>activate<?php } ?>">
                                <a href="<?= base_url('deposite') ?>">
                                    <div class="sidebar-item">
                                        <i class="sidebar-icon far fa-piggy-bank"></i>
                                        <p class="sidebar-text">Deposite</p>
                                    </div>
                                </a>
                            </div>
                            <div class="sub-menu <?php if ($activetitle == "withdraw-wallet") { ?>activate<?php } ?>">
                                <a href="<?= base_url('withdraw') ?>">
                                    <div class="sidebar-item">
                                        <i class="sidebar-icon far fa-hand-holding-usd"></i>
                                        <p class="sidebar-text">Withdraw</p>
                                    </div>
                                </a>
                            </div>
                            <div class="sub-menu <?php if ($activetitle == "transaction-wallet") { ?>activate<?php } ?>">
                                <a href="<?= base_url('wallet-transaction') ?>">
                                    <div class="sidebar-item">
                                        <i class="sidebar-icon far fa-history"></i>
                                        <p class="sidebar-text">Wallet Transaction</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="sidebar-sec-3">
                        <a href="<?= base_url('affiliate') ?>">
                            <div class="sidebar-item <?php if ($activetitle == "affiliatepage") { ?>activate<?php } ?>">
                                <i class="sidebar-icon far fa-share-square"></i>
                                <p class="sidebar-text">Affiliate</p>
                            </div>
                        </a>
                        <!-- <a href="<?= base_url('exchange') ?>">
                            <div class="sidebar-item <?php if($activetitle == "exchangepwt") { ?>activate<?php } ?>">
                                <i class="sidebar-icon far fa-share-square"></i>
                                <p class="sidebar-text">Exchange</p>
                            </div>
                        </a> -->
                        <div>
                            <a href="<?= base_url('history') ?>">
                                <div class="sidebar-item <?php if ($activetitle == "main-history") { ?>activate<?php } ?>">
                                    <i class="sidebar-icon far fa-history"></i>
                                    <p class="sidebar-text">History</p>
                                </div>
                            </a>
                        </div>
                        <a href="<?= base_url('logout') ?>">
                            <div class="sidebar-item">
                                <i class="sidebar-icon far fa-sign-out-alt"></i>
                                <p class="sidebar-text">Logout</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>

            <!-- Left Sidebar End -->
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">