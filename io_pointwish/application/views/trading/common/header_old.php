<!doctype html>
<html lang="en" ng-app="pwt">

<head>
    <meta charset="utf-8" />
    <title>pwt: <?php if (!empty($title)) echo $title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta content="<?= $title ?>" name="description" />
    <meta http-equiv="Content-Security-Policy" content="default-src data: gap://* file://* https://ssl.gstatic.com *; img-src 'self' * data:; style-src 'self' 'unsafe-inline' *; script-src 'self' 'unsafe-eval' 'unsafe-inline' *; connect-src 'self' * ws://* wss://*;">


    <script src="<?= base_url('assets/user_panel/libs/jquery/jquery.min.js') ?>"></script>
    <link href="<?= base_url('assets/user_panel/css/multi-form.css?v=' . rand()) ?>" rel="stylesheet" type="text/css" />
    <script src="<?= base_url('assets/user_panel/js/multi-form.js') ?>"></script>
    <script>
        let user = <?= $this->session->logsid ?>;
        window.addEventListener("focus", function(event) {
            $.ajax({
                type: "POST",
                data: {
                    userid: user,
                },
                url: '<?php echo base_url(); ?>login/is_activeUser',
                dataType: "json",
                success: function(response) {
                    if (response.is_active == 0) {
                        window.location = "<?= base_url('logout'); ?>"
                    }
                }

            });
        }, true);
    </script>
    <!--<script src="<?= base_url('assets/user_panel/js/duplicate.js?v=' . rand()) ?>"></script>-->
    <script>
        // $(document).ready(function() {
        //     if (window.IsDuplicate()) {
        //         document.write('<h1>Duplicate window detected close other one and refresh the page</h1>');
        //         window.stop();
        //     }
        // });
    </script>
    <!--<script src="<?= base_url('assets/front/dark/js/devtools.js?v=' . rand()) ?>"></script>-->
    <script>
        // var www = $(window).width();
        // if (www > 600) {
        //     window.addEventListener('devtoolschange', function(e) {
        //         if (e.detail.open == true) {
        //             document.write('<h1>Developer tools is opened. Please close <span style="color:red">(Press F12)</span> and refresh the page</h1>');
        //             window.stop();
        //         }
        //     });
        // }
    </script>
    
    <?php $session_data = getSessionData(); ?>

    <!-- App favicon -->
    <link rel="shortcut icon" href="<?= base_url('assets/user_panel/images/logo/favicon.ico') ?>">

    <link href="<?= base_url('assets/user_panel/libs/chartist/chartist.min.css') ?>" rel="stylesheet">

    <!-- DataTables -->
    <link href="<?= base_url('assets/user_panel/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css') ?>" rel="stylesheet" type="text/css" />
    <link href="<?= base_url('assets/user_panel/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css') ?>" rel="stylesheet" type="text/css" />

    <!-- Responsive datatable examples -->
    <!--<link href="<?= base_url('assets/user_panel/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') ?>" rel="stylesheet" type="text/css" />-->
    <link href="<?= base_url('assets/user_panel/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') ?>" rel="stylesheet" type="text/css" />

    <!-- Sweet Alert-->
    <link href="<?= base_url('assets/user_panel/libs/sweetalert2/sweetalert2.min.css') ?>" rel="stylesheet" type="text/css" />

    <!-- Plugins css -->
    <link href="<?= base_url('assets/user_panel/libs/dropzone/min/dropzone.min.css') ?>" rel="stylesheet" type="text/css" />

    <!-- Bootstrap Css -->
    <?php
    if ($title != 'Tradding') {
    ?>
        <link href="<?= base_url('assets/user_panel/css/bootstrap.min.css?v=' . rand()) ?>" id="bootstrap-style" rel="stylesheet" type="text/css" />
    <?php
    }
    ?>

    <!-- owlCarousel css -->
    <link rel="stylesheet" href="<?= base_url('assets/user_panel/css/owl.carousel.min.css') ?>">

    <!-- Icons Css -->
    <link href="<?= base_url('assets/user_panel/css/icons.min.css') ?>" rel="stylesheet" type="text/css" />

    <!-- Pricing table scss-->
    <link href="<?= base_url('assets/user_panel/pricing-tables.css?v=' . rand()) ?>" rel="stylesheet" type="text/css" />

    <!-- App Css-->
    <link href="<?= base_url('assets/user_panel/css/datepicker.css?v=' . rand()) ?>" id="app-style" rel="stylesheet" type="text/css" />
    <link href="<?= base_url('assets/user_panel/css/app.min.css?v=' . rand()) ?>" rel="stylesheet" type="text/css" />

    <!-- Winer page css -->
    <link href="<?= base_url('assets/user_panel/css/winner.css?v=' . rand()) ?>" rel="stylesheet" type="text/css" />

    <!-- Toastr css -->
    <link href="<?= base_url('assets/user_panel/libs/toastr/toastr.min.css?v=' . rand()) ?>" rel="stylesheet" type="text/css" />

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">


    <script src="<?= base_url('assets/user_panel/js/jquery-ui.min.js') ?>"></script>

    <script src="<?= base_url('assets/user_panel/libs/angular/angular.min.js') ?>"></script>
    <script src="<?= base_url('assets/user_panel/js/angular-datatables.min.js') ?>"></script>

    <script src="<?= base_url('assets/user_panel/js/moment.min.js') ?>"></script>

    <script>
        var BASE_URL = "<?php echo base_url(); ?>";
    </script>
    <?php
    // $session_data = getSessionData();
    $amount = getWalletAmountByUserID($this->session->user_id);
    $live_amount = $amount[3];
    $demo_amount = $amount[1];
    ?>
    <script>
        sessionStorage.clear();
        sessionStorage.setItem('live-amount', <?= $live_amount; ?>);
        sessionStorage.setItem('demo-amount', <?= $demo_amount; ?>);

        var toast = {
            timeOut: 1e3,
            closeButton: !0,
            debug: !1,
            newestOnTop: !0,
            progressBar: !0,
            positionClass: "toast-top-right",
            preventDuplicates: !0,
            onclick: null,
            showDuration: "300",
            hideDuration: "1000",
            extendedTimeOut: "1000",
            showEasing: "swing",
            hideEasing: "linear",
            showMethod: "fadeIn",
            hideMethod: "fadeOut",
            tapToDismiss: !1
        }
    </script>
</head>
<div id="preloader"></div>

<body data-sidebar="dark" ng-controller="App_Controller" class="sidebar-close vertical-collpsed">
    <script>
        // var ww = document.body.clientWidth;
        // if (ww > 991) {
        //     $('body').addClass('');
        // }
    </script>

    <!-- Begin page -->
    <div id="layout-wrapper">
        <header id="page-topbar" class="<?= ($title == 'Trading' ? 'bg-202305' : 'bg-12161c') ?>">
            <!-- <div id="preloaderh"></div> -->

            <div class="preload-stopper navbar-header h-55px">
                <div class="d-flex">
                    <!-- LOGO -->
                    <div class="navbar-brand-box <?= ($title == 'Trading' ? 'bg-202305' : 'bg-12161c') ?>">
                        <a class="logo logo-light">
                            <span class="logo-sm">
                                <img src="<?= base_url('assets/user_panel/images/logo/favicon.ico') ?>" alt="" height="30">
                            </span>
                            <!--<span class="logo-lg">-->
                            <!--    <img src="<?= base_url('assets/user_panel/images/logo/logo.svg?v=' . rand()) ?>" alt="" class="header-item ml-lg-n5 mt-3" height="18">-->
                            <!--</span>-->
                        </a>
                    </div>

                    <button type="button" class="btn btn-sm px-3 font-size-24 header-item" id="vertical-menu-btn">
                        <i class="mdi mdi-menu"></i>
                    </button>
                    <?php if ($title != "Trading") { ?>
                        <h4 class="header-page-name"><?php echo $title ?></h4>
                    <?php } else { ?>
                        <h4 class="header-page-name title-trade"><?php echo $title1 ?></h4>
                    <?php } ?>
                </div>

                <div class="d-flex">
                    <?php if ($title == 'Trading') { ?>
                        <div class="dropdown d-inline-block">
                            <div class="btn-group">
                                <!--<button type="button" class="btn btn-primary btn-demo mt-xsm-1 mt-2 w-115" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">-->
                                <button type="button" class="btn btn-primary btn-demo mt-xsm-1 mt-2 w-auto text-nowrap" id="btn-amount" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <div class="media-body">
                                        <div class="font-size-12 text-muted">
                                            <p class="mb-0 text-white text-left"><span class='account-label text-nowrap'><?= ($this->session->trade_type == 0 ? 'Live Account' : 'Demo Account'); ?></span>&nbsp;&nbsp;&nbsp;<i class="mdi mdi-chevron-down float-right"></i></p>
                                        </div>
                                        <h5 class="mt-0 mb-0 h-demo text-nowrap"><span class='account-amount'></span> pwt</h5>

                                    </div>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right pl-3 pr-3 text-nowrap">
                                    <div class="d-flex">
                                        <div class="mb-auto mt-auto"><input class="hw-20 account-radio" type="radio" id="live" name="acc" value="live" data-label='Live Account' <?= ($this->session->trade_type == 0 ? 'checked' : ''); ?>></div>
                                        <div class="pl-3"><label for="live"><span class="font-size-12 text-light">Live Account</span><br><span class="font-size-18" id='live-amount'></span> pwt</label></div>
                                    </div>
                                    <div class="d-flex">
                                        <div class="mb-auto mt-auto"><input class="hw-20 account-radio" type="radio" id="demo" name="acc" value="demo" data-label='Demo Account' <?= ($this->session->trade_type == 1 ? 'checked' : ''); ?>></div>
                                        <div class="pl-3"><label for="demo"><span class="font-size-12 text-light">Demo Account</span><br><span class="font-size-18" id='demo-amount'></span> pwt</label><i class="fa fa-refresh text-white font-size-18 refresh-demo-wallet" aria-hidden="true"></i></div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    <?php } ?>

                    <?php if ($title == "Wallet") { ?>
                        <div>
                            <a href="<?= base_url('assets/user_panel/web-tutorial.pdf?v=' . rand()) ?>" download class="btn btn-gold mr-header mt-14px">Web Tutorial</a>
                        </div>
                    <?php } ?>
                    <?php if ($this->uri->segment(1) == "affiliate") { ?>
                        <div>
                            <a href="<?= base_url('assets/user_panel/affiliate.pdf?v=' . rand()) ?>" download class="btn btn-gold mr-header mt-14px">Agency guide</a>
                        </div>
                    <?php } ?>
                    <div class="dropdown d-none d-lg-inline-block">
                        <button type="button" class="btn header-item noti-icon" data-toggle="fullscreen">
                            <i class="mdi mdi-fullscreen"></i>
                        </button>
                    </div>
                    <div id="main" ng-cloak>
                        <?php if ($title == "Trading") { ?>
                            <span class="pl-0 pr-2" onclick="openNav()">
                                <i class="fa fa-ellipsis-v header-more-btn" aria-hidden="true"></i>
                            </span>
                        <?php } ?>
                        <div class="dropdown d-inline-block">
                            <button type="button" class="btn header-item noti-icon pl-0" id="page-header-notifications-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="mdi mdi-bell-outline"></i>
                                <span ng-show="notifications.length > '0'" class="badge bg-goldenrod text-black line-h-1 font-size-75 badge-pill">{{notifications.length}}</span>
                            </button>
                            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right p-0 min-w-18rem" aria-labelledby="page-header-notifications-dropdown">
                                <div class="p-3">
                                    <div class="row align-items-center">
                                        <div class="col">
                                            <h5 class="m-0 font-size-16"> Notifications ({{notifications.length}}) </h5>
                                        </div>
                                    </div>
                                </div>
                                <div ng-if='!loader' data-simplebar style="max-height: 230px;">
                                    <a ng-repeat="notification in notifications" ng-click="mark_read(notification.id)" class="text-reset notification-item cursor-pointer">
                                        <div class="media">
                                            <div class="avatar-xs mr-3">
                                                <span class="avatar-title w-100 h-100 bg-warning rounded-circle font-size-16">
                                                    <i class="mdi mdi-message-text-outline"></i>
                                                </span>
                                            </div>
                                            <div class="media-body">
                                                <h6 class="mt-0 mb-1 text-capitalize">{{notification.type}}
                                                    <small class="float-sm-right">{{notification.date | date: "mediumDate"}}</small>
                                                </h6>
                                                <div class="font-size-12 text-muted">
                                                    <p class="mb-1">{{notification.detail}}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                    <?php //} 
                                    ?>
                                </div>
                                <div ng-show="notifications.length > 0" class="p-2 border-top">
                                    <a ng-click="mark_all_read()" class="btn btn-sm btn-link font-size-14 btn-block text-center">
                                        Read all
                                    </a>
                                </div>
                            </div>
                        </div>

                        
                        <div class="dropdown d-inline-block">
                            <button type="button" class="btn header-item p-0" id="page-header-user-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <!--<img class="rounded-circle header-profile-user" src="<?= base_url('assets/user_panel/images/users/user-4.jpg') ?>" alt="Header Avatar">-->
                                <h4 class="profile-font text-uppercase" style="color: gold !important;"><?= substr($session_data->username, 0, 1) ?></h4>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right min-width-8rem">
                                <!-- item-->
                                <h5 class="px-3 text-ffba00 text-capitalize" style="color: #ffba00 !important;"><?= $session_data->username ?></h5>
                                <a class="dropdown-item" style="color: #9ca8b3 !important;" href="<?= base_url('account') ?>"><i style="color: #9ca8b3 !important;" class="mdi mdi-account-circle font-size-17 align-middle mr-1"></i> Profile</a>
                                <a class="dropdown-item" style="color: #9ca8b3 !important;" href="<?= base_url('logout') ?>"><i style="color: #9ca8b3 !important;" class="fa fa-power-off font-size-17 align-middle mr-1"></i> Logout</a>
                            </div>
                        </div>
                    </div>
                    <div id="mySidenav" class="sidenav d-none">
                        <!--<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>-->

                        <?php if ($title == "Trading") { ?>
                            <div class="card bg-12161c mb-0 border-none">
                                <div class="card-body pt-0 pl-3 pr-3">
                                    <ul class="nav nav-tabs nav-tabs-custom" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-toggle="tab" href="#open" role="tab" aria-selected="true">
                                                <!--<span class="d-block d-sm-none"><i class="fas fa-home"></i></span>-->
                                                <span style="color: #02C076 !important">Open</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#close" role="tab" aria-selected="false">
                                                <!--<span class="d-block d-sm-none">-->
                                                <!--    <i class="far fa-user"></i>-->
                                                <!--</span>-->
                                                <span style="color :#D9304E !important">Close</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                                        </li>
                                    </ul>

                                    <div class="tab-content">
                                        <div class="tab-pane pt-2 active" id="open" role="tabpanel"></div>
                                        <div class="tab-pane pt-2" id="close" role="tabpanel"></div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </header>
        <!-- ========== Left Sidebar Start ========== -->
        <div class="preload-stopper position-fixed vertical-menu mt-n16px <?= ($title == 'Trading' ? 'bg-grdnt-1' : 'bg-12161c') ?>">
            <div data-simplebar class="h-100">
                <!--- Sidemenu -->
                <div id="sidebar-menu">
                    <!-- Left Menu Start -->
                    <ul class="metismenu list-unstyled mt-16px" id="side-menu">
                        <li>
                            <a href="<?= base_url('trading') ?>" data-id="trade" id="trade" class="waves-effect">
                                <i class="fas fa-chart-bar"></i>
                                <span>Trading</span>
                            </a>
                        </li>

                        <li>
                            <a href="<?= base_url('trade-dashboard') ?>" class="waves-effect">
                                <i class="fa fa-tachometer"></i>
                                <span><?= lang('dashboard') ?></span>
                            </a>
                        </li>

                        <li>
                            <a href="<?= base_url('copytrade') ?>" class="waves-effect">
                                <i class="fa fa-clone" aria-hidden="true"></i>
                                <span>Copy Trade</span>
                            </a>
                        </li>

                        <!--<li>-->
                        <!--    <a href="<?= base_url('trading-winner') ?>" data-id="winner" id="winner" class="waves-effect">-->
                        <!--        <i class="fas fa-trophy"></i>-->
                        <!--        <span><?= lang('trading_winner') ?></span>-->
                        <!--    </a>-->
                        <!--</li>-->

                        <!--<li>-->
                        <!--    <a href="<?= base_url('trade/draw') ?>" data-id="draw" id="draw" class="waves-effect">-->
                        <!--        <i class="dripicons-ticket"></i>-->
                        <!--        <span><?= lang('lucky_draw') ?></span>-->
                        <!--    </a>-->
                        <!--</li>-->

                        <li>
                            <a href="<?= base_url('wallet') ?>" data-id="wallet" id="wallet" class="waves-effect">
                                <i class="ti-wallet"></i>
                                <span><?= lang('wallet') ?></span>
                            </a>
                        </li>

                        <li>
                            <a ng-click="get_auth_status()" href="<?= base_url('account') ?>" data-id="profile" id="profile" class="waves-effect">
                                <i class="fas fa-user-tie"></i>
                                <span><?= lang('profile') ?></span>
                            </a>
                        </li>

                        <li>
                            <a href="<?= base_url('affiliate') ?>" data-id="affiliate" id="affiliate" class="waves-effect">
                                <i class="fas fa-share-alt"></i>
                                <span><?= lang('affiliate') ?></span>
                            </a>
                        </li>

                        <?php if ($session_data->is_agency == 1) { ?>
                            <li>
                                <a href="<?= base_url('agency') ?>" data-id="agency" id="agency" class="waves-effect">
                                    <i class="fa fa-building-o" aria-hidden="true"></i>
                                    <span>Agency</span>
                                </a>
                            </li>

                            <li>
                                <a href="<?= base_url('exchange') ?>" data-id="exchange" id="exchange" class="waves-effect">
                                    <i class="fas fa-exchange-alt"></i>
                                    <span><?= lang('exchange') ?></span>
                                </a>
                            </li>
                        <?php } ?>


                        <li>
                            <a href="<?= base_url('history') ?>" data-id="history" id="history" class="waves-effect">
                                <i class="fas fa-history"></i>
                                <span><?= lang('history') ?></span>
                            </a>
                        </li>

                        <li>
                            <a href="<?= base_url('ticket') ?>" data-id="ticket" id="ticket" class="waves-effect">
                                <i class="fas fa-ticket-alt"></i>
                                <span><?= lang('ticket') ?></span>
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- Sidebar -->
            </div>
        </div>
        <!-- Left Sidebar End -->
        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="main-content">
