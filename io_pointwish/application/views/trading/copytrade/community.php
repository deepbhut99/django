<?php $this->load->view('trading/common/header'); ?>
<link rel="stylesheet" href="<?= base_url('assets/user_panel/css/tabs.css?v=' .rand()) ?>">
<div class="page-content" ng-controller="CopyTrade_Controller" ng-cloak>
    <div class="container-fluid p-0 mt-12px">
        <div>
            <!-- Nav tabs -->

            <ul class="nav nav-tabs justify-content-center" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#topgainers" role="tab">
                        <?= lang('top_gainers') ?>
                    </a>
                </li>
                <li class="nav-item" ng-click="most_populars()">
                    <a class="nav-link" data-toggle="tab" href="#mostpopular" role="tab">
                        <?= lang('most_popular') ?>
                    </a>
                </li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane active p-3" id="topgainers" role="tabpanel">
                    <div class="ml-auto mr-auto mt-lg-10 text-center" ng-show="topLoader">
                        <img src="<?= base_url('assets/user_panel/images/preloader.gif') ?>" alt="loader">
                    </div>
                    <div class="row mb-14px" ng-show="gainers.length">
                        <div class="col-lg-9 col-md-8 col-sm-12"></div>
                        <div class="col-lg-3 col-md-4 col-sm-12">
                            <div class="form-group mb-0 mr-2 ml-2">
                                <div class="input-group colorpicker-default w-50 float-md-right" title="Using format option">
                                    <input type="text" ng-model="top_search" name="top_search" class="form-control input-lg" placeholder="Search Here.." />
                                </div>
                            </div>
                        </div>
                    </div>                    
                    <div class="row" ng-show="!topLoader">
                        <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 px-3px" ng-repeat="user in gainers | filter:top_search | pagination : currentPage*itemsPerPage | limitTo: 6">
                            <div class="card directory-card border-radius-copy mb-6px">
                                <div class="card-body pb-12px">
                                    <div class="media mb-2">
                                        <span class="avatar-lg mt-16px ng-binding profile-font-2 text-center text-uppercase">{{user.full_name.substr(0, 1)}}</span>
                                        <div class="media-body ml-3 mt-3">
                                            <h5 class="font-size-18 mt-0 mb-1">{{user.full_name}}</h5>
                                            <span class="badge badge-theme"><i class="far fa-star" aria-hidden="true"></i>&nbsp;<?= lang('experts') ?></span>
                                        </div>

                                        <ul class="list-unstyled social-links float-right">
                                            <li class="text-right mb-0.3">
                                                <small class="opacity-half"><?= lang('profit') ?></small>
                                                <h3 class="mb-0 text-46f2d4 mt-n2">{{user.win_ratio}}%</h3>
                                            </li>
                                            <li class="text-right mb-0.3">
                                                <small class="opacity-half"><?= lang('copiers') ?></small>
                                                <h3 class="mb-0 mt-n2">{{user.copiers}}</h3>
                                            </li>
                                            <li class="text-right mb-0.3">
                                                <small class="opacity-half"><?= lang('profit_sharing') ?></small>
                                                <h3 class="mb-0 mt-n2">{{user.profit_share}}%</h3>
                                            </li>
                                        </ul>
                                    </div>
                                    <div ng-hide="user.userid == <?= $this->session->user_id ?>">
                                        <button ng-show="user.status == false" ng-click="start_copy(user.userid, user.profit_share, user.min_invest)" class="btn btn-secondary waves-effect bg-img-btn border-0 w-50 mt-n110px"><?= lang('start_copying') ?></button>
                                        <button ng-show="user.status == true" ng-click="stop_copy(user.userid)" class="btn btn-secondary waves-effect bg-danger border-0 w-50 mt-n110px">Stop Copying</button>
                                    </div>
                                    <div ng-show="user.userid == <?= $this->session->user_id ?>">
                                        <button class="btn btn-secondary waves-effect bg-img-btn border-0 w-50 mt-n110px">Your Self</button>
                                    </div>
                                    <hr class="mt-n4">
                                    <p class="text-center font-18 pt-1 mb-0 mt-n7px"><span class="opacity-half">Min. Trade Amount</span> <span class="font-weight-bold text-46f2d4">{{user.min_invest}} USDT</span></p>
                                </div>
                            </div>
                        </div>
                        <h3 class="ml-auto mr-auto mt-1-3rem" ng-show="!gainers.length">
                            No Data Found
                        </h3>
                    </div>
                    <div class="pagination-copy" ng-show="gainers.length > 6">
                        <span ng-class="DisablePrevPage()"><a class="prev-page" ng-click="prevPage()">prev</a></span>
                        <span ng-repeat="n in range()" ng-click="setPage(n)"><a class="pagination-item-copy" ng-class="{active: n == currentPage}" href="#" ng-bind="n+1"></a></span>
                        <span ng-class="DisableNextPage()"><a href ng-click="nextPage()" class="prev-page">next</a></span>
                    </div>
                </div>
                <div class="tab-pane p-3" id="mostpopular" role="tabpanel">
                    <div class="ml-auto mr-auto mt-lg-10 text-center" ng-show="dataLoader">
                        <img src="<?= base_url('assets/user_panel/images/preloader.gif') ?>" alt="loader">
                    </div>
                    <div class="row mb-14px" ng-show="populars.length && !dataLoader">
                        <div class="col-lg-9 col-md-8 col-sm-12"></div>
                        <div class="col-lg-3 col-md-4 col-sm-12">
                            <div class="form-group mb-0 mr-2 ml-2">
                                <div class="input-group colorpicker-default w-50 float-md-right" title="Using format option">
                                    <input ng-model="most_search" type="text" class="form-control input-lg" placeholder="Search Here.." />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" ng-show="!dataLoader">
                        <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 px-3px" ng-repeat="plr in populars | filter:most_search | pagination : activePage*usersPerPage | limitTo: 6">
                            <div class="card directory-card border-radius-copy mb-6px">
                                <div class="card-body pb-12px">
                                    <div class="media mb-2">
                                        <span class="avatar-lg mt-16px ng-binding profile-font-2 text-center text-uppercase">{{plr.full_name.substr(0, 1)}}</span>
                                        <div class="media-body ml-3 mt-3">
                                            <h5 class="font-size-18 mt-0 mb-1">{{plr.full_name}}</h5>
                                            <span class="badge badge-theme"><i class="far fa-star" aria-hidden="true"></i>&nbsp;<?= lang('experts') ?></span>
                                        </div>
                                        <ul class="list-unstyled social-links float-right">
                                            <li class="text-right mb-0.3">
                                                <small class="opacity-half"><?= lang('profit') ?></small>
                                                <h3 class="mb-0 text-46f2d4 mt-n2">{{plr.win_ratio}}%</h3>
                                            </li>
                                            <li class="text-right mb-0.3">
                                                <small class="opacity-half"><?= lang('copiers') ?></small>
                                                <h3 class="mb-0 mt-n2">{{plr.copiers}}</h3>
                                            </li>
                                            <li class="text-right mb-0.3">
                                                <small class="opacity-half"><?= lang('profit_sharing') ?></small>
                                                <h3 class="mb-0 mt-n2">{{plr.profit_share}}%</h3>
                                            </li>
                                        </ul>
                                    </div>
                                    <div ng-hide="plr.userid == <?= $this->session->user_id ?>">
                                        <button ng-show="plr.status == false" ng-click="start_copy(plr.userid, plr.profit_share, plr.min_invest)" class="btn btn-secondary waves-effect bg-img-btn border-0 w-50 mt-n110px"><?= lang('start_copying') ?></button>
                                        <button ng-show="plr.status == true" ng-click="stop_copy(plr.userid)" class="btn btn-secondary waves-effect bg-danger border-0 w-50 mt-n110px">Stop Copying</button>
                                    </div>
                                    <div ng-show="plr.userid == <?= $this->session->user_id ?>">
                                        <button class="btn btn-secondary waves-effect bg-img-btn border-0 w-50 mt-n110px">Your Self</button>
                                    </div>
                                    <hr class="mt-n4">
                                    <p class="text-center pt-1 mb-0 font-18 mt-n7px"><span class="opacity-half">Min. Trade Amount</span> <span class="font-weight-bold text-46f2d4">{{plr.min_invest}} USDT</span></p>
                                </div>
                            </div>
                        </div>
                        <h3 class="ml-auto mr-auto mt-1-3rem" ng-show="!populars.length">
                            No Data Found
                        </h3>
                    </div>
                    <div class="pagination-copy" ng-show="populars.length > 6">
                        <span ng-class="DisableBackPage()"><a class="prev-page" ng-click="backPage()">prev</a></span>
                        <span ng-repeat="n in usersrange()" ng-click="setActivePage(n)"><a class="pagination-item-copy" ng-class="{active: n == activePage}" href="#" ng-bind="n+1"></a></span>
                        <span ng-class="DisableNxtPage()"><a href ng-click="nxtPage()" class="prev-page">next</a></span>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<?php $this->load->view('trading/common/footer'); ?>
<script>
    $(document).ready(function() {
        $('.myslider').owlCarousel({
            items: 1,
            nav: false,
            dots: true,
            dotsData: true,
            autoplay: true,
            autoplayTimeout: 3000,
            loop: true,
            autoplayHoverPause: true
        });
    });
</script>