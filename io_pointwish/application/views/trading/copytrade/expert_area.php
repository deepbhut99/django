<?php $this->load->view('trading/common/header'); ?>
<div class="page-content mx-cryp" ng-controller="CopyTrade_Controller" ng-cloak>
    <div class="container-fluid p-0 mt-12px">
        <?php if ($user_data->copy_trade == '0') { ?>
            <h1 class="text-center mt-3 mb-3">Trade as an Expert</h1>

            <h4 class="text-center mb-4"><?= lang('min_rqurmnt') ?></h4>
            <div>
                <div class="card ml-auto mr-auto card-radio card-expert mb-6px">
                    <div class="card-body pl-xsm-2 pr-xsm-2 w-max-width">
                        <form class="ng-pristine ng-valid pr-2 pt-2">
                            <div>
                                <input type="radio" class="input-radio mr-2" name="trade" value="Total bid count atleast 100" <?= $total_bot_play->totalbidforbot >= 100? 'checked' : 'disabled'?>>
                                <label class="expert-text" for="Total bid count atleast 100">Total bid count atleast 100: <small>(current <?php echo($total_bot_play->totalbidforbot); ?>)</small></label><br>
                            </div>
                        </form>
                    </div>
                </div>
                <?php
                if ($total_bot_play->totalbidforbot >= 16) {
                    $result = true;
                }
                ?>
                <div class="card ml-auto mr-auto card-expert bg-transparent">
                    <div ng-show="<?= !$result ?>" class="card-body text-center bg-danger border-radius-0">
                        <i class="fa fa-window-close pr-2" aria-hidden="true"></i><span><?= lang('you_are_not_eble_to_reg') ?></span>
                    </div>
                    <button data-toggle="modal" data-target=".bs-example-modal-center" ng-show="<?= $result ?>" class="card-body border-0 text-center bg-success border-radius-0">
                        <i class="fa fa-check-square pr-2" aria-hidden="true"></i><span>Register</span>
                    </button>
                </div>
            </div>
        <?php } else { ?>
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 px-3px">
                    <div class="card mb-6px">
                        <div class="card-body">
                            <div class="pl-2 text-black">
                                <h3><?= $earn_commission ? number_format($earn_commission, 2) : 0 ?> USDT</h3>
                                <h6>Total Commission</h6>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 px-3px">
                    <div class="card mb-6px">
                        <div class="card-body py-2">
                            <div class="pl-2 text-black">
                                <h5 class="text-center pt-0.18">Current Condition</h5>
                                <div class="d-flex justify-content-between">
                                    <div>
                                        <h5 class="font-size-14 mb-1">Profit Share : <span class="font-weight-900"><?= $user_data->profit_share ?>%</span></h5>
                                        <h5 class="font-size-14 pb-1">Min. Investment : <span class="font-weight-900"><?= $user_data->min_invest ?> USDT</span></h5>
                                    </div>
                                    <div class="mt-6">
                                        <button class="btn btn-theme-red" onclick="disable_expert()">Disable</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 px-3px">
                    <div class="card mb-6px">
                        <div class="card-body">
                            <div class="pl-2 text-black">
                                <h3><?= $win ? $win : 0 ?>%</h3>
                                <h6>Winning Ratio</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mt-3">
                <h2 class="font-size-22 text-center">History</h2>
            </div>
            <div class="card mt-3">
                <div class="card-body">
                    <div class="ml-auto mr-auto text-center d-none" id="earnLoader">
                        <img src="<?= base_url('assets/user_panel/images/preloader.gif') ?>" alt="loader">
                    </div>
                    <table id="earnDatatable" class="table display dt-responsive nowrap table-striped table-borderless" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                            <tr>
                                <th>Sr.No</th>
                                <th><?= lang('time') ?></th>
                                <th><?= lang('type') ?></th>
                                <th>User</th>
                                <th><?= lang('amount') ?></th>
                                <th><?= lang('profit') ?></th>
                                <th>Commission</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
<?php $this->load->view('trading/common/footer'); ?>
<div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content bg-0C1A32 border-radius-0 border-0">
            <div class="modal-header">
                <h5 class="modal-title mt-0">Register</h5>
                <button type="button" class="close text-d9d9d9" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="card mb-0">
                    <div class="card-body">
                        <form id="data-form" action="#" novalidate="">
                            <div class="form-group">
                                <label>Profit Share <small class="text-danger font-weight-bold">(min. 1%)</small></label>
                                <input type="number" min="1" max="25" id="profit" name="profit" class="form-control required" required="" placeholder="Enter sharing profit">
                            </div>
                            <div class="form-group">
                                <label>Options Trading <small class="text-danger font-weight-bold">(min. $15)</small></label>
                                <input type="number" min="1" id="invest" class="form-control required" required="" placeholder="Enter Amount">
                            </div>
                            <div class="form-group d-flex justify-content-between">
                                <label>PointWish Strategy</label>
                                <div>
                                    <input type="radio" id="pwtstrategy" name="pwtstrategy" value="Yes">
                                    <label for="yes">Yes</label>
                                    <input type="radio" id="pwtstrategy" name="pwtstrategy" value="No">
                                    <label for="no">No</label>
                                </div>
                            </div>
                            <div class="form-group mb-0 text-right">
                                <div>
                                    <button type="submit" class="btn btn-dark waves-effect waves-light mr-1">
                                        <?= lang('submit') ?>
                                    </button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<script>
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust()
            .responsive.recalc();
    });

    $(document).ready(function() {
        $('table.display').DataTable();
    });
    $("#data-form").validate();
    $('#data-form').submit(function(e) {
        e.preventDefault();
        var form = $("#data-form");
        if (form.valid() == false) {
            return;
        } else {
            Swal.fire({
                title: "Are you sure?",
                text: "",
                type: "warning",
                showCancelButton: !0,
                confirmButtonColor: "#34c38f",
                cancelButtonColor: "#f46a6a",
                confirmButtonText: "Yes",
                confirmButtonClass: "btn btn-success mt-2",
                cancelButtonClass: "btn btn-danger ml-2 mt-2",
                buttonsStyling: !1,
            }).then(function(t) {
                if (t.value == true) {
                    $.ajax({
                        type: "POST",
                        url: "<?= base_url('trade/trading/register_expert') ?>",
                        data: {
                            profit: $('#profit').val(),
                            invest: $('#invest').val(),
                            pwt: $('#pwtstrategy:checked').val()
                        },
                        success: function(res) {
                            res = JSON.parse(res);
                            if (res.success == true) {
                                location.reload();
                            } else {
                                toastr.error(res.message);
                            }
                        }
                    })
                } else {

                }
            });
        }
    });

    var earn_table = $("#earnDatatable").DataTable({
        "pagingType": "simple",
        "order": [
            [0, "asc"]
        ]
    });
    var peers = <?= json_encode($peers) ?>;
    count = 1;
    peers.forEach(function(bid) {
        // profit = bid.profit == 1 ? (+bid.trade_amount + +bid.trade_amount * 95 / 100) * (+bid.commission / 100) : 0;
        profit = bid.profit == "1" ? "<span class='text-success'>" + (+bid.trade_amount + (+bid.trade_amount * 95 / 100)) + "</span>" : "<span class='text-danger'>0</span>";
        commission = bid.profit == "1" ? "<span class='text-success'>" + ((+bid.trade_amount * 95 / 100) * (+bid.commission / 100)).toFixed(2) + ' ( ' + bid.commission + '% )' : "<span class='text-danger'>0</span>";
        earn_table.row.add([count++, moment(+bid.trade_date).format('DD-MM-YYYY hh:mm:ss A'), bid.type == 1 ? 'Up' : 'Down', bid.full_name, bid.trade_amount, profit, commission]).draw(false);
    });

    function disable_expert() {
        Swal.fire({
            title: "Are you sure?",
            text: "",
            type: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#34c38f",
            cancelButtonColor: "#f46a6a",
            confirmButtonText: "Yes",
            confirmButtonClass: "btn btn-success mt-2",
            cancelButtonClass: "btn btn-danger ml-2 mt-2",
            buttonsStyling: !1,
        }).then(function(t) {
            if (t.value == true) {
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('trade/trading/disable_expert') ?>",
                    success: function(res) {
                        location.reload();
                    }
                })
            } else {

            }
        });
    }

    var earn_history = $('#earnDatatable').DataTable({
        destroy: true,
        pagingType: "simple",
        order: [
            [0, "asc"]
        ]
    }).clear().draw();
    var tableContainer = $(earn_history.table().container());
    tableContainer.css('display', 'none');
    $('#earnLoader').addClass('d-block');
    $.ajax({
        type: 'post',
        url: '<?= base_url("trade/trading/get_earntrade_history") ?>',
        data: {
            start: $('#earn_start').val(),
            end: $('#earn_end').val()
        },
        success: function(res) {
            res = JSON.parse(res);
            count = 1;
            res.forEach(function(bid) {
                profit = bid.profit == "1" ? "<span class='text-success'>" + (+bid.trade_amount + (+bid.trade_amount * 95 / 100)) + "</span>" : "<span class='text-danger'>0</span>";
                commission = bid.profit == "1" ? "<span class='text-success'>" + ((+bid.trade_amount * 95 / 100) * (+bid.commission / 100)).toFixed(2) + ' ( ' + bid.commission + '% )' : "<span class='text-danger'>0</span>";
                earn_history.row.add([count++, moment(+bid.trade_date).format('DD-MM-YYYY hh:mm:ss A'), bid.type == 1 ? 'Up' : 'Down', bid.full_name, bid.trade_amount, profit, commission]).draw(false);
            })
            $('#earnLoader').removeClass('d-block');
            tableContainer.css('display', 'block');
        }
    })
</script>