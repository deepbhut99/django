<?php $this->load->view('trading/common/header'); ?>
<link rel="stylesheet" href="<?= base_url('assets/user_panel/css/tabs.css') ?>">
<div class="page-content" ng-controller="CopyTrade_Controller1" ng-cloak>
    <div class="container-fluid mt-12px">
        <div class="row">
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 px-3px">
                <div class="card mb-6px">
                    <div class="card-body">
                        <div class="pl-2 text-black">
                            <h3><?= $total_trade ? $total_trade : 0 ?> USDT</h3>
                            <h6>Investment Amount</h6>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 px-3px">
                <div class="card mb-6px">
                    <div class="card-body">
                        <div class="pl-2 text-black">
                            <h3><?= $total_profit ? number_format($total_profit, 2) : 0 ?> USDT</h3>
                            <h6>Total Profit</h6>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 px-3px">
                <div class="card mb-6px">
                    <div class="card-body">
                        <div class="pl-2 text-black">
                            <h3><?= $total_trade ? round($total_profit * 100 / $total_trade, 2) : 0 ?>%</h3>
                            <h6>Profit(%)</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div>
            <h3 class="font-size-22 text-center">Portfolio List</h3>
        </div>

        <div class="card bg-transparent">
            <div class="card-body pl-0 pr-0">
                <!-- <ul class="nav nav-tabs nav-tabs-custom border-bottom-0 pl-3" role="tablist">
                    <li class="nav-item mr-2">
                        <a class="nav-link active pl-md-5 pr-md-5 bg-2b2f36" data-toggle="tab" href="#copying" role="tab">
                            <span class="d-block d-sm-none"><i class="fa fa-clone"></i></span>
                            <span class="d-none d-sm-block">Copying</span>
                        </a>
                    </li>
                    <li class="nav-item mr-2">
                        <a class="nav-link pl-md-5 pr-md-5 bg-2b2f36" data-toggle="tab" href="#copiers" role="tab">
                            <span class="d-block d-sm-none"><i class="fa fa-user-plus"></i></span>
                            <span class="d-none d-sm-block">Copiers</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link pl-md-5 pr-md-5 bg-2b2f36 abc" data-toggle="tab" href="#copyhistory" role="tab">
                            <span class="d-block d-sm-none"><i class="fa fa-history"></i></span>
                            <span class="d-none d-sm-block">History</span>
                        </a>
                    </li>
                </ul> -->

                <ul class="nav nav-tabs justify-content-center py-0" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#copying" role="tab">
                            Copying
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#copiers" role="tab">
                            Copiers
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#copyhistory" role="tab">
                            History
                        </a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active pt-3 p-mobile" id="copying" role="tabpanel">
                        <div class="ml-auto mr-auto mt-lg-10 text-center" ng-show="listLoader">
                            <img src="<?= base_url('assets/user_panel/images/preloader.gif') ?>" alt="loader">
                        </div>
                        <div class="row mb-14px" ng-show="lists.length">
                            <div class="col-lg-9 col-md-8 col-sm-12"></div>
                            <div class="col-lg-3 col-md-4 col-sm-12">
                                <div class="form-group mb-0 mr-2 ml-2">
                                    <div class="input-group colorpicker-default w-50 float-md-right" title="Using format option">
                                        <input type="text" ng-model="listsearch" class="form-control input-lg" placeholder="Name..." />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" ng-show="!listLoader">
                            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 pr-5px" ng-repeat="list in lists | filter:listsearch | pagination : currentPage*itemsPerPage | limitTo: 6">
                                <div class="card directory-card border-radius-copy mb-14px">
                                    <div class="card-body pb-12px">
                                        <div class="media mb-2">
                                            <span class="avatar-lg mt-16px ng-binding profile-font-2 text-center text-uppercase">{{list.full_name.substr(0, 1)}}</span>
                                            <div class="media-body ml-3 mt-3">
                                                <h5 class="font-size-18 mt-0 mb-1">{{list.full_name}}</h5>
                                                <span class="badge badge-theme"><i class="far fa-star" aria-hidden="true"></i>&nbsp;<?= lang('experts') ?></span>
                                            </div>
                                            <ul class="list-unstyled social-links float-right">
                                                <li class="text-right mb-0.3">
                                                    <small class="opacity-half"><?= lang('profit') ?></small>
                                                    <h3 class="mb-0 text-46f2d4 mt-n2">{{list.win_ratio}}%</h3>
                                                </li>
                                                <li class="text-right mb-0.3">
                                                    <small class="opacity-half"><?= lang('copiers') ?></small>
                                                    <h3 class="mb-0 mt-n2">{{list.copiers}}</h3>
                                                </li>
                                                <li class="text-right mb-0.3">
                                                    <small class="opacity-half"><?= lang('profit_sharing') ?></small>
                                                    <h3 class="mb-0 mt-n2">{{list.profit_share}}%</h3>
                                                </li>
                                            </ul>
                                        </div>
                                        <button ng-show="list.status == true" ng-click="stop_copy(user.userid)" class="btn btn-secondary waves-effect bg-danger border-0 w-50 mt-n110px">Stop Copying</button>
                                        <hr class="mt-n4">
                                        <p class="text-center font-18 pt-1 mb-0 mt-n7px"><span class="opacity-half">Min. Trade Amount</span> <span class="font-weight-bold text-46f2d4">{{list.min_invest}} USDT</span></p>
                                    </div>
                                </div>
                            </div>
                            <h3 class="ml-auto mr-auto mt-1-3rem" ng-show="!lists.length">
                                No Data Found
                            </h3>
                        </div>


                        <div class="pagination-copy" ng-show="lists.length > 6">
                            <span ng-class="DisablePrevPage()"><a class="prev-page" ng-click="prevPage()">prev</a></span>
                            <span ng-repeat="n in range()" ng-click="setPage(n)"><a class="pagination-item-copy" ng-class="{active: n == currentPage}" href="#" ng-bind="n+1"></a> </span>
                            <span ng-class="DisableNextPage()"><a href ng-click="nextPage()" class="prev-page">next</a></span>
                        </div>
                    </div>
                    <div class="tab-pane pt-3" id="copiers" role="tabpanel">
                        <div class="card">
                            <div class="card-body">
                                <div class="ml-auto mr-auto text-center d-none" id="copierLoader">
                                    <img src="<?= base_url('assets/user_panel/images/preloader.gif') ?>" alt="loader">
                                </div>
                                <table id="copiers_datatable" class="table dt-responsive nowrap table-striped table-borderless" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>Sr.No</th>
                                            <th>Username</th>
                                            <th>Profit Share</th>
                                            <th>Min Investment</th>
                                            <th>Bot Purchase</th>
                                            <th>Start Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane pt-3" id="copyhistory" role="tabpanel">
                        <div class="card">
                            <div class="card-body">
                                <div class="ml-auto mr-auto text-center d-none" id="historyLoader">
                                    <img src="<?= base_url('assets/user_panel/images/preloader.gif') ?>" alt="loader">
                                </div>
                                <table id="historycopytrade" class="table display dt-responsive nowrap table-striped table-borderless" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>Sr.No</th>
                                            <th><?= lang('time') ?></th>
                                            <th><?= lang('type') ?></th>
                                            <th><?= lang('experts') ?></th>
                                            <th><?= lang('amount') ?></th>
                                            <th><?= lang('profit') ?></th>
                                            <th>Commission</th>
                                            <th>Actual Profit</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('trading/common/footer'); ?>

<script>
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust()
            .responsive.recalc();
    });
</script>

<script>
    var table_copiers = $("#copiers_datatable").DataTable({
        "pagingType": "simple"
    });
    var copiers = <?= json_encode($copiers) ?>;

    count = 1;
    copiers.forEach(function(row) {
        table_copiers.row.add([count++, row.username, row.profit_sharing, row.minimum_invest, row.is_bottrade == 1 ? '<span class="text-success">YES</span>' : '<span class="text-danger">NO</span>', moment(row.start_date).format('DD-MM-YYYY')]).draw(false);
    });
    var table = $("#historycopytrade").DataTable({
        "pagingType": "simple",
        "order": [
            [0, "asc"]
        ]
    });
    var bidcopys = <?= json_encode($history) ?>;

    count = 1;
    bidcopys.forEach(function(bidcopy) {
        //  profit = bidcopy.profit == 1 ? (+bidcopy.trade_amount + +bidcopy.trade_amount * 95 / 100) * (+bidcopy.commission / 100) : 0;
        profit = bidcopy.profit == 1 ? +bidcopy.trade_amount + (+bidcopy.trade_amount * 95 / 100) : 0;
        commission = bidcopy.profit == 1 ? (+bidcopy.trade_amount * 95 / 100) * (+bidcopy.commission / 100) : 0;
        //   table.row.add([count++, moment(+bidcopy.trade_date).format('DD-MM-YYYY hh:mm:ss A'), bidcopy.type == 1 ? 'Up' : 'Down', bidcopy.full_name, bidcopy.trade_amount, profit, bidcopy.profit == "1" ? commission.toFixed(3) + ' ( ' + bidcopy.commission + '% )' : 0, (profit - commission).toFixed(3)]).draw(false);
        table.row.add([count++, moment(+bidcopy.trade_date).format('DD-MM-YYYY hh:mm:ss A'), bidcopy.type == 1 ? 'Up' : 'Down', bidcopy.full_name, bidcopy.trade_amount, bidcopy.profit == 1 ? "<span class='text-success'>" + profit + '</span>' : "<span class='text-danger'>0</span>", bidcopy.profit == "1" ? "<span class='text-success'>" + commission.toFixed(2) + ' ( ' + bidcopy.commission + '% )' + '</span>' : "<span class='text-danger'>0</span>", bidcopy.profit == "1" ? "<span class='text-success'>" + (profit - commission).toFixed(2) + '</span>' : "<span class='text-danger'>0</span>"]).draw(false);
    });
</script>