<?php $this->load->view('trading/common/header'); ?>
<link href="<?= base_url('assets/user_panel/css/exchange.css') ?>" rel="stylesheet" type="text/css" />
<div class="page-content" ng-controller="Dashboard_Controller">
    <div class="container-fluid p-24">
        <div class="row">
            <!-- <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 pr-8px">
                <div class="card bg-0d0f15 mb-14-px mb-sm-14px">
                    <div class="card-body p-1.4">
                        <div id="myfirstchart" class="morris-charts morris-chart-height mb-2" dir="ltr" style="stroke: gold !important"></div>
                    </div>
                </div>
            </div> -->
            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="card mb-14-px mb-sm-14px gradient-bg-1">
                    <div class="card-body">
                        <div class="media">
                            <div class="media-body ml-3">
                                <input type="hidden" id="winbids">
                                <h2><?= $winbids ?>%</h2>
                                <h6><?= lang('win_rate') ?></h6>
                            </div>
                            <img height="62px" width="60px" src="<?= base_url('assets/user_panel/images/dashboard/win_rate.svg') ?>" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="card mb-14-px mb-sm-14px gradient-bg-2">
                    <div class="card-body">
                        <div class="media">
                            <div class="media-body ml-3">
                                <h2><?= $copytrade ? number_format($copytrade, 2) : 0 ?> USDT</h2>
                                <h6><?= lang('copy_trade_profit') ?></h6>
                            </div>
                            <img height="60px" width="60px" src="<?= base_url('assets/user_panel/images/dashboard/trade_profit.svg') ?>" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="card mb-14-px mb-sm-14px gradient-bg-3">
                    <div class="card-body">
                        <div class="media">
                            <div class="media-body ml-3">
                                <h2><?= number_format($total_trade, 2) ?> USDT</h2>
                                <h6><?= lang('total_trade_amt') ?></h6>
                            </div>
                            <img height="60px" width="60px" src="<?= base_url('assets/user_panel/images/dashboard/total_trade_amt.svg') ?>" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="card mb-14-px mb-sm-14px gradient-bg-4">
                    <div class="card-body">
                        <div class="media">
                            <div class="media-body ml-3">
                                <h2><?= number_format($total_profit, 2) ?> USDT</h2>
                                <h6><?= lang('total_revenue') ?></h6>
                            </div>
                            <img height="60px" width="60px" src="<?= base_url('assets/user_panel/images/dashboard/total_revenue.svg') ?>" alt="">
                        </div>
                    </div>
                </div>
            </div>


            <!-- <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 pr-18px">
                <div class="row">
                    <div class="col-md-6 col-sm-12 pr-lg-4px pr-md-4px pl-md-20px pl-sm-20px pr-sm-20px">
                        <div class="card mb-14-px mb-sm-14px">
                            <div class="card-body">
                                <div class="media">
                                    <div class="media-body ml-3">
                                        <input type="hidden" id="winbids">
                                        <h2><?= $winbids ?>%</h2>
                                        <h6><?= lang('win_rate') ?></h6>
                                    </div>
                                    <img height="62px" width="60px" src="<?= base_url('assets/user_panel/images/dashboard/win_rate.svg') ?>" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 pr-md-20px pl-sm-20px pr-sm-20px">
                        <div class="card mb-14-px mb-sm-14px">
                            <div class="card-body">
                                <div class="media">
                                    <div class="media-body ml-3">
                                        <h2><?= $copytrade ? number_format($copytrade, 2) : 0 ?> USDT</h2>
                                        <h6><?= lang('copy_trade_profit') ?></h6>
                                    </div>
                                    <img height="60px" width="60px" src="<?= base_url('assets/user_panel/images/dashboard/trade_profit.svg') ?>" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-12 pr-lg-4px pr-md-4px pl-md-20px pl-sm-20px pr-sm-20px">
                        <div class="card mb-14-px mb-sm-14px">
                            <div class="card-body">
                                <div class="media">
                                    <div class="media-body ml-3">
                                        <h2><?= number_format($total_trade, 2) ?> USDT</h2>
                                        <h6><?= lang('total_trade_amt') ?></h6>
                                    </div>
                                    <img height="60px" width="60px" src="<?= base_url('assets/user_panel/images/dashboard/total_trade_amt.svg') ?>" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 pr-md-20px pl-sm-20px pr-sm-20px">
                        <div class="card mb-14-px mb-sm-14px">
                            <div class="card-body">
                                <div class="media">
                                    <div class="media-body ml-3">
                                        <h2><?= number_format($total_profit, 2) ?> USDT</h2>
                                        <h6><?= lang('total_revenue') ?></h6>
                                    </div>
                                    <img height="60px" width="60px" src="<?= base_url('assets/user_panel/images/dashboard/total_revenue.svg') ?>" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 pl-md-20px pr-md-20px pl-sm-20px pr-sm-20px">
                        <div class="card bg-0d0f15 mb-sm-14px">
                            <div class="card-body">
                                <div>
                                    <h4 class="text-center mb-4 mt-2"><?= lang('trades_summary') ?></h4>
                                    <div class="progress dash-bar mb-3">
                                        <div class="progress-bar bg-danger" role="progressbar" style="width:<?= $downbids ?>%" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
                                        <div class="progress-bar bg-success" role="progressbar" style="width: <?= $upbids ?>%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <div class="progressbar-text">
                                        <span><?= lang('down') ?> <span class="text-danger"><?= $downbids ?>%</span></span>
                                        <span><span class="text-success"><?= $upbids ?>%</span> <?= lang('up') ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->

        </div>

        <!-- <div class="row mt-n10px">
            <div class="col-12">
                <div>
                    <form>
                        <div class="row">
                            <div class="col-xl-7 col-lg-7 col-md-6 col-sm-4"></div>
                            <div class="col-xl-5 col-lg-5 col-md-6 col-sm-8">
                                <div class="wrapper-new d-flex">
                                    <input ng-model="startdate" id="startdate" type="text" class="form-control ml-3 mr-3" placeholder="<?= lang('start_date') ?>" readonly>
                                    <input ng-model="enddate" id="enddate" type="text" class="form-control mr-3" placeholder="<?= lang('end_date') ?>" readonly>
                                    <button type="button" onclick="get_history()" id="history" class="btn btn-primary waves-effect waves-light mr-1 pr-4 mr-3 srchbtn" style="height: 38px; width:41px !important"><i class="fa fa-search" aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="card mt-n20px mb-14-px mb-sm-14px">
                    <div class="card-body">
                        <div class="ml-auto mr-auto text-center d-none" id="dataloader">
                            <img src="<?= base_url('assets/user_panel/images/preloader.gif') ?>" alt="loader">
                        </div>
                        <table id="datatable" class="table dt-responsive nowrap table-striped table-borderless" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <tr>
                                    <th><?= lang('sr_no') ?></th>
                                    <th><?= lang('date_time') ?></th>
                                    <th><?= lang('base_amt') ?></th>
                                    <th><?= lang('closing_val') ?></th>
                                    <th><?= lang('prediction') ?></th>
                                    <th><?= lang('trade_amt') ?></th>
                                    <th><?= lang('profit') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div> -->
    </div> <!-- container-fluid -->
</div>
<!--Morris Chart-->
<?php $this->load->view('trading/common/footer'); ?>
<script>
    var dataLists = "<?= $winbids ?>";
    new Morris.Donut({
        // ID of the element in which to draw the chart.
        element: 'myfirstchart',
        // Chart data records -- each entry in this array corresponds to a point on
        // the chart.
        data: dataLists == 0 ? [{
            label: "No Trade",
            value: 100
        }] : [{
                label: "Win-Round",
                value: <?= $winbids ?>
            },
            {
                label: "Loss-Round",
                value: <?= $lossbids ?>
            }
        ],
        // The name of the data record attribute that contains x-values.
        xkey: '',
        // A list of names of data record attributes that contain y-values.
        ykeys: [''],
        // Labels for the ykeys -- will be displayed when you hover over the
        // chart.
        labels: [''],
        colors: ["#02C076", "#D9304E"],
    });
</script>
<script>
    $(document).ready(function() {
        // $('#dataloader').addClass('d-block');
        var table = $("#datatable").DataTable({
            "pagingType": "simple"
        });
        var bids = <?= json_encode($bids) ?>;
        count = 1;
        bids.forEach(function(bid) {
            table.row.add([count++, moment(+bid.timestamp).format('DD-MM-YYYY HH:mm:ss'), bid.value, bid.mCloseVal, bid.growth == 1 ? 'Up' : 'Down', bid.amount, bid.result == 1 ? '<span class="text-success">' + (+bid.amount + (+bid.amount * 95 / 100)).toFixed(2) + '</span>' : '<span class="text-danger">' + 0 + '</span>']).draw(false);
        });
        setTimeout(function() {
            // $('#dataloader').removeClass('d-block');
            // $('.table-dash').removeClass('d-none');
        }, 1000);
    });

    function get_history() {
        var table = $('#datatable').DataTable({
            destroy: true,
            pagingType: "simple"
        }).clear().draw();
        var tableContainer = $(table.table().container());
        tableContainer.css('display', 'none');
        $('#dataloader').addClass('d-block');
        $.ajax({
            type: 'post',
            url: '<?= base_url("trade/dashboard/get_all_bids") ?>',
            data: {
                start: $('#startdate').val(),
                end: $('#enddate').val(),
            },
            success: function(res) {
                res = JSON.parse(res);
                count = 1;
                res.forEach(function(bid) {
                    table.row.add([count++, moment(+bid.timestamp).format('DD-MM-YYYY HH:mm:ss'), bid.value, bid.mCloseVal, bid.growth == 1 ? 'Up' : 'Down', bid.amount, bid.result == 1 ? '<span class="text-success">' + (+bid.amount + (+bid.amount * 95 / 100)).toFixed(2) + '</span>' : '<span class="text-danger">' + 0 + '</span>']).draw(false);
                })
                $('#dataloader').removeClass('d-block');
                tableContainer.css('display', 'block');
            }
        })
    }
</script>