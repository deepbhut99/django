<?php $this->load->view('trading/common/header'); ?>
<style>
    .btnradius {
        border-radius: 1rem;
    }
</style>
<div class="page-content">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row align-items-center">
            <div class="col-sm-6">
                <div class="page-title-box">


                </div>
            </div>
        </div>
        <!-- end page title -->


        <h4 class="font-size-18">Team Bonus</h4>
        <div class="row">
            <div class="col-xl-4 col-md-6">
                <div class="card mini-stat text-white" style="background: linear-gradient(90deg, #00B4DB 0%, #0083B0 100%);">
                    <div class="card-body">
                        <div class="mb-4">
                            <div class="float-left mini-stat-img mr-4">
                                <img src="<?= base_url('assets/user_panel/images/services-icon/wallet.svg'); ?>" alt="">
                            </div>
                            <h5 class="font-size-16  mt-0 text-white-50" style="color: white !important;">Wallet</h5>
                            <h4 class="font-weight-medium font-size-24">$ 1,685</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="card mini-stat text-white" style="background: linear-gradient(90deg, #00B4DB 0%, #0083B0 100%);">
                    <div class="card-body">
                        <div class="mb-4">
                            <div class="float-left mini-stat-img mr-4">
                                <img src="<?= base_url('assets/user_panel/images/services-icon/withdraw.svg'); ?>" alt="">
                            </div>
                            <h5 class="font-size-16  mt-0 text-white-50" style="color: white !important;">Withdrawal</h5>
                            <h4 class="font-weight-medium font-size-24">$ 1,685</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="card mini-stat text-white" style="background: linear-gradient(90deg, #00B4DB 0%, #0083B0 100%);">
                    <div class="card-body">
                        <div class="mb-4">
                            <div class="float-left mini-stat-img mr-4" >
                                <img src="<?= base_url('assets/user_panel/images/services-icon/balance.svg'); ?>" alt="">
                            </div>
                            <h5 class="font-size-16  mt-0 text-white-50" style="color: white !important;">Balance</h5>
                            <h4 class="font-weight-medium font-size-24">$ 1,685
                                <span>
                                    <a class="btn btn-primary waves-effect waves-light btnradius" href="#" role="button" style="float: right; margin-top:-6px; background-color: #3a49c9">Withdraw</a>
                                </span>
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <br>
        <h4 class="font-size-18">Fixed Bonus</h4>
        <div class="row">
            <div class="col-xl-4 col-md-6">
                <div class="card mini-stat text-white" style="background: linear-gradient(90deg, #00B4DB 0%, #0083B0 100%);">
                    <div class="card-body">
                        <div class="mb-4">
                            <div class="float-left mini-stat-img mr-4">
                                <img src="<?= base_url('assets/user_panel/images/services-icon/wallet.svg'); ?>" alt="">
                            </div>
                            <h5 class="font-size-16  mt-0 text-white-50" style="color: white !important;">Wallet</h5>
                            <h4 class="font-weight-medium font-size-24">$ 1,685</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="card mini-stat text-white" style="background: linear-gradient(90deg, #00B4DB 0%, #0083B0 100%);">
                    <div class="card-body">
                        <div class="mb-4">
                            <div class="float-left mini-stat-img mr-4">
                                <img src="<?= base_url('assets/user_panel/images/services-icon/withdraw.svg'); ?>" alt="">
                            </div>
                            <h5 class="font-size-16  mt-0 text-white-50" style="color: white !important;">Withdrawal</h5>
                            <h4 class="font-weight-medium font-size-24">$ 1,685</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="card mini-stat text-white" style="background: linear-gradient(90deg, #00B4DB 0%, #0083B0 100%);">
                    <div class="card-body">
                        <div class="mb-4">
                            <div class="float-left mini-stat-img mr-4">
                                <img src="<?= base_url('assets/user_panel/images/services-icon/balance.svg'); ?>" alt="">
                            </div>
                            <h5 class="font-size-16  mt-0 text-white-50" style="color: white !important;">Balance</h5>
                            <h4 class="font-weight-medium font-size-24">$ 1,685
                                <span>
                                    <a class="btn btn-primary waves-effect waves-light btnradius" href="#" role="button" style="float: right; margin-top:-6px; background-color: #3a49c9">Withdraw</a>
                                </span>
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- 
        <br>
        <h4 class="font-size-18">Crusader</h4>
        <div class="row">  
            <div class="col-xl-4 col-md-6">
                <div class="card mini-stat text-white" style="background: linear-gradient(90deg, #00B4DB 0%, #0083B0 100%);">
                    <div class="card-body">
                        <div class="mb-4">
                            <div class="float-left mini-stat-img mr-4">
                                <img src="<?= base_url('assets/user_panel/images/services-icon/02.png'); ?>" alt="">
                            </div>
                            <h5 class="font-size-16  mt-0 text-white-50" style="color: white !important;">Wallet</h5>
                            <h4 class="font-weight-medium font-size-24">$ 1,685</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="card mini-stat text-white" style="background: linear-gradient(90deg, #00B4DB 0%, #0083B0 100%);">
                    <div class="card-body">
                        <div class="mb-4">
                            <div class="float-left mini-stat-img mr-4">
                                <img src="<?= base_url('assets/user_panel/images/services-icon/02.png'); ?>" alt="">
                            </div>
                            <h5 class="font-size-16  mt-0 text-white-50" style="color: white !important;">Withdrawal</h5>
                            <h4 class="font-weight-medium font-size-24">$ 1,685</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="card mini-stat text-white" style="background: linear-gradient(90deg, #00B4DB 0%, #0083B0 100%);">
                    <div class="card-body">
                        <div class="mb-4">
                            <div class="float-left mini-stat-img mr-4">
                                <img src="<?= base_url('assets/user_panel/images/services-icon/02.png'); ?>" alt="">
                            </div>
                            <h5 class="font-size-16  mt-0 text-white-50" style="color: white !important;">Balance</h5>
                            <h4 class="font-weight-medium font-size-24">$ 1,685
                                <span>
                                    <a class="btn btn-primary waves-effect waves-light" href="#" role="button" style="float: right; margin-top:-6px; background-color: #3a49c9">Withdraw</a>
                                </span>
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->


        <br>
        <h4 class="font-size-18">Herald</h4>
        <div class="row">

            <div class="col-xl-4 col-md-6">
                <div class="card mini-stat text-white" style="background: linear-gradient(90deg, #00B4DB 0%, #0083B0 100%);">
                    <div class="card-body">
                        <div class="mb-4">
                            <div class="float-left mini-stat-img mr-4">
                                <img src="<?= base_url('assets/user_panel/images/services-icon/wallet.svg'); ?>" alt="">
                            </div>
                            <h5 class="font-size-16  mt-0 text-white-50" style="color: white !important;">Wallet</h5>
                            <h4 class="font-weight-medium font-size-24">$ 1,685</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="card mini-stat text-white" style="background: linear-gradient(90deg, #00B4DB 0%, #0083B0 100%);">
                    <div class="card-body">
                        <div class="mb-4">
                            <div class="float-left mini-stat-img mr-4">
                                <img src="<?= base_url('assets/user_panel/images/services-icon/withdraw.svg'); ?>" alt="">
                            </div>
                            <h5 class="font-size-16  mt-0 text-white-50" style="color: white !important;">Withdrawal</h5>
                            <h4 class="font-weight-medium font-size-24">$ 1,685</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="card mini-stat text-white" style="background: linear-gradient(90deg, #00B4DB 0%, #0083B0 100%);">
                    <div class="card-body">
                        <div class="mb-4">
                            <div class="float-left mini-stat-img mr-4">
                                <img src="<?= base_url('assets/user_panel/images/services-icon/balance.svg'); ?>" alt="">
                            </div>
                            <h5 class="font-size-16  mt-0 text-white-50" style="color: white !important;">Balance</h5>
                            <h4 class="font-weight-medium font-size-24">$ 1,685
                                <span>
                                    <a class="btn btn-primary waves-effect waves-light btnradius" href="#" role="button" style="float: right; margin-top:-6px; background-color: #3a49c9">Withdraw</a>
                                </span>
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->

        <br>
        <h4 class="font-size-18">Guardian</h4>
        <div class="row">

            <div class="col-xl-4 col-md-6">
                <div class="card mini-stat text-white" style="background: linear-gradient(90deg, #00B4DB 0%, #0083B0 100%);">
                    <div class="card-body">
                        <div class="mb-4">
                            <div class="float-left mini-stat-img mr-4">
                                <img src="<?= base_url('assets/user_panel/images/services-icon/wallet.svg'); ?>" alt="">
                            </div>
                            <h5 class="font-size-16  mt-0 text-white-50" style="color: white !important;">Wallet</h5>
                            <h4 class="font-weight-medium font-size-24">$ 1,685</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="card mini-stat text-white" style="background: linear-gradient(90deg, #00B4DB 0%, #0083B0 100%);">
                    <div class="card-body">
                        <div class="mb-4">
                            <div class="float-left mini-stat-img mr-4">
                                <img src="<?= base_url('assets/user_panel/images/services-icon/withdraw.svg'); ?>" alt="">
                            </div>
                            <h5 class="font-size-16  mt-0 text-white-50" style="color: white !important;">Withdrawal</h5>
                            <h4 class="font-weight-medium font-size-24">$ 1,685</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="card mini-stat text-white" style="background: linear-gradient(90deg, #00B4DB 0%, #0083B0 100%);">
                    <div class="card-body">
                        <div class="mb-4">
                            <div class="float-left mini-stat-img mr-4">
                                <img src="<?= base_url('assets/user_panel/images/services-icon/balance.svg'); ?>" alt="">
                            </div>
                            <h5 class="font-size-16  mt-0 text-white-50" style="color: white !important;">Balance</h5>
                            <h4 class="font-weight-medium font-size-24">$ 1,685
                                <span>
                                    <a class="btn btn-primary waves-effect waves-light btnradius" href="#" role="button" style="float: right; margin-top:-6px; background-color: #3a49c9">Withdraw</a>
                                </span>
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <br>
        <h4 class="font-size-18">Crusader</h4>
        <div class="row">
            <div class="col-xl-4 col-md-6">
                <div class="card mini-stat text-white" style="background: linear-gradient(90deg, #00B4DB 0%, #0083B0 100%);">
                    <div class="card-body">
                        <div class="mb-4">
                            <div class="float-left mini-stat-img mr-4">
                                <img src="<?= base_url('assets/user_panel/images/services-icon/wallet.svg'); ?>" alt="">
                            </div>
                            <h5 class="font-size-16  mt-0 text-white-50" style="color: white !important;">Wallet</h5>
                            <h4 class="font-weight-medium font-size-24">$ 1,685</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="card mini-stat text-white" style="background: linear-gradient(90deg, #00B4DB 0%, #0083B0 100%);">
                    <div class="card-body">
                        <div class="mb-4">
                            <div class="float-left mini-stat-img mr-4">
                                <img src="<?= base_url('assets/user_panel/images/services-icon/withdraw.svg'); ?>" alt="">
                            </div>
                            <h5 class="font-size-16  mt-0 text-white-50" style="color: white !important;">Withdrawal</h5>
                            <h4 class="font-weight-medium font-size-24">$ 1,685</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="card mini-stat text-white" style="background: linear-gradient(90deg, #00B4DB 0%, #0083B0 100%);">
                    <div class="card-body">
                        <div class="mb-4">
                            <div class="float-left mini-stat-img mr-4">
                                <img src="<?= base_url('assets/user_panel/images/services-icon/balance.svg'); ?>" alt="">
                            </div>
                            <h5 class="font-size-16  mt-0 text-white-50" style="color: white !important;">Balance</h5>
                            <h4 class="font-weight-medium font-size-24">$ 1,685
                                <span>
                                    <a class="btn btn-primary waves-effect waves-light btnradius" href="#" role="button" style="float: right; margin-top:-6px; background-color: #3a49c9">Withdraw</a>
                                </span>
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <br>
        <h4 class="font-size-18">Legend</h4>
        <div class="row">
            <div class="col-xl-4 col-md-6">
                <div class="card mini-stat text-white" style="background: linear-gradient(90deg, #00B4DB 0%, #0083B0 100%);">
                    <div class="card-body">
                        <div class="mb-4">
                            <div class="float-left mini-stat-img mr-4">
                                <img src="<?= base_url('assets/user_panel/images/services-icon/wallet.svg'); ?>" alt="">
                            </div>
                            <h5 class="font-size-16  mt-0 text-white-50" style="color: white !important;">Wallet</h5>
                            <h4 class="font-weight-medium font-size-24">$ 1,685</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="card mini-stat text-white" style="background: linear-gradient(90deg, #00B4DB 0%, #0083B0 100%);">
                    <div class="card-body">
                        <div class="mb-4">
                            <div class="float-left mini-stat-img mr-4">
                                <img src="<?= base_url('assets/user_panel/images/services-icon/withdraw.svg'); ?>" alt="">
                            </div>
                            <h5 class="font-size-16  mt-0 text-white-50" style="color: white !important;">Withdrawal</h5>
                            <h4 class="font-weight-medium font-size-24">$ 1,685</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="card mini-stat text-white" style="background: linear-gradient(90deg, #00B4DB 0%, #0083B0 100%);">
                    <div class="card-body">
                        <div class="mb-4">
                            <div class="float-left mini-stat-img mr-4">
                                <img src="<?= base_url('assets/user_panel/images/services-icon/balance.svg'); ?>" alt="">
                            </div>
                            <h5 class="font-size-16  mt-0 text-white-50" style="color: white !important;">Balance</h5>
                            <h4 class="font-weight-medium font-size-24">$ 1,685
                                <span>
                                    <a class="btn btn-primary waves-effect waves-light btnradius" href="#" role="button" style="float: right; margin-top:-6px; background-color: #3a49c9">Withdraw</a>
                                </span>
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <br>
        <h4 class="font-size-18">Ancient</h4>
        <div class="row">
            <div class="col-xl-4 col-md-6">
                <div class="card mini-stat text-white" style="background: linear-gradient(90deg, #00B4DB 0%, #0083B0 100%);">
                    <div class="card-body">
                        <div class="mb-4">
                            <div class="float-left mini-stat-img mr-4">
                                <img src="<?= base_url('assets/user_panel/images/services-icon/wallet.svg'); ?>" alt="">
                            </div>
                            <h5 class="font-size-16  mt-0 text-white-50" style="color: white !important;">Wallet</h5>
                            <h4 class="font-weight-medium font-size-24">$ 1,685</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="card mini-stat text-white" style="background: linear-gradient(90deg, #00B4DB 0%, #0083B0 100%);">
                    <div class="card-body">
                        <div class="mb-4">
                            <div class="float-left mini-stat-img mr-4">
                                <img src="<?= base_url('assets/user_panel/images/services-icon/withdraw.svg'); ?>" alt="">
                            </div>
                            <h5 class="font-size-16  mt-0 text-white-50" style="color: white !important;">Withdrawal</h5>
                            <h4 class="font-weight-medium font-size-24">$ 1,685</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="card mini-stat text-white" style="background: linear-gradient(90deg, #00B4DB 0%, #0083B0 100%);">
                    <div class="card-body">
                        <div class="mb-4">
                            <div class="float-left mini-stat-img mr-4">
                                <img src="<?= base_url('assets/user_panel/images/services-icon/balance.svg'); ?>" alt="">
                            </div>
                            <h5 class="font-size-16  mt-0 text-white-50" style="color: white !important;">Balance</h5>
                            <h4 class="font-weight-medium font-size-24">$ 1,685
                                <span>
                                    <a class="btn btn-primary waves-effect waves-light btnradius" href="#" role="button" style="float: right; margin-top:-6px; background-color: #3a49c9">Withdraw</a>
                                </span>
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <br>
        <h4 class="font-size-18">Immortal</h4>
        <div class="row">
            <div class="col-xl-4 col-md-6">
                <div class="card mini-stat text-white" style="background: linear-gradient(90deg, #00B4DB 0%, #0083B0 100%);">
                    <div class="card-body">
                        <div class="mb-4">
                            <div class="float-left mini-stat-img mr-4">
                                <img src="<?= base_url('assets/user_panel/images/services-icon/wallet.svg'); ?>" alt="">
                            </div>
                            <h5 class="font-size-16  mt-0 text-white-50" style="color: white !important;">Wallet</h5>
                            <h4 class="font-weight-medium font-size-24">$ 1,685</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="card mini-stat text-white" style="background: linear-gradient(90deg, #00B4DB 0%, #0083B0 100%);">
                    <div class="card-body">
                        <div class="mb-4">
                            <div class="float-left mini-stat-img mr-4">
                                <img src="<?= base_url('assets/user_panel/images/services-icon/withdraw.svg'); ?>" alt="">
                            </div>
                            <h5 class="font-size-16  mt-0 text-white-50" style="color: white !important;">Withdrawal</h5>
                            <h4 class="font-weight-medium font-size-24">$ 1,685</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="card mini-stat text-white" style="background: linear-gradient(90deg, #00B4DB 0%, #0083B0 100%);">
                    <div class="card-body">
                        <div class="mb-4">
                            <div class="float-left mini-stat-img mr-4">
                                <img src="<?= base_url('assets/user_panel/images/services-icon/balance.svg'); ?>" alt="">
                            </div>
                            <h5 class="font-size-16  mt-0 text-white-50" style="color: white !important;">Balance</h5>
                            <h4 class="font-weight-medium font-size-24">$ 1,685
                                <span>
                                    <a class="btn btn-primary waves-effect waves-light btnradius" href="#" role="button" style="float: right; margin-top:-6px; background-color: #3a49c9">Withdraw</a>
                                </span>
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div> <!-- container-fluid -->
</div>
<?php $this->load->view('trading/common/footer'); ?>