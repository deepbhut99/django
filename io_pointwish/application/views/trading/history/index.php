<?php $this->load->view('trading/common/header'); ?>
<div class="page-content">
    <div class="container-fluid p-0">
        <div class="row mt-14px">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="ml-auto mr-auto text-center d-none" id="dataloader">
                            <img src="<?= base_url('assets/user_panel/images/preloader.gif') ?>" alt="loader">
                        </div>
                        <table id="datatable" class="table dt-responsive nowrap table-striped table-borderless" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <tr>
                                    <th><?= lang('sr_no') ?></th>
                                    <th><?= lang('message') ?></th>
                                    <th><?= lang('credit_debit') ?></th>
                                    <th><?= lang('amount') ?></th>
                                    <th><?= lang('balance') ?></th>
                                    <th><?= lang('date') ?></th>
                                    <th><?= lang('status') ?></th>
                                    <th><?= lang('transaction_id') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>



    </div> <!-- container-fluid -->
</div>
<?php $this->load->view('trading/common/footer'); ?>
<script>
    $(document).ready(function() {
        // $('#dataloader').addClass('d-block');
        var table = $("#datatable").DataTable({
            "pagingType": "simple"
        });
        var bids = <?= json_encode($data_result) ?>;
        count = 1;
        // console.log(bids);
        bids.forEach(function(bid) {
            txn_id = bid.txn_id;
            table.row.add([
                count++,
                bid.type == 'withdrawal' && bid.message == 'pending' ? "<span class='text-capitalize cursor-pointer font-weight-bold text-warning' onclick=remove_withdrawal(\'" + bid.txn_id + "\')>" + bid.message + "<br>( " + bid.username + " )</span>" : "<span class='text-capitalize font-weight-bold'>" + bid.message + "<br>( " + bid.username + " )</span>",
                "<span class='text-capitalize'>" + bid.log + "</span>",
                bid.payment_type == 'pwt' ? 'USDT <br>' + (+bid.amount).toFixed(2) : 'USDT <br> ' + (+bid.amount).toFixed(6),
                bid.balance != 'NA' ? (bid.payment_type == 'pwt' ? 'USDT <br>' + (+bid.balance).toFixed(2) : (bid.payment_type == 'USDTTRC20' || bid.payment_type == 'USDTERC20' ? 'USDT <br>' : 'USDT <br>') + (+bid.balance).toFixed(6)) : 'N/A',
                moment(bid.created_datetime).format('D MMMM, YYYY hh:mm'),
                bid.status == '1' || bid.status == '100' ? "<span class='badge text-capitalize badge-success'>Success</span>" : bid.status == '2' ? "<span class='badge badge-warning'>Pending</span>" : bid.status == '3' ? "<span class='badge badge-danger'>Rejected</span>" : "<span class='badge badge-warning'>In-Progress</span>",
                "<span>" + txn_id.substr(0, 5) + '...' + txn_id.substr(-5) + "</span>"
            ]).draw(false);
        });
        setTimeout(function() {
            // $('#dataloader').removeClass('d-block');
            // $('.table-wrapper').removeClass('d-none');
        }, 1000);


        var base = "<?= base_url() ?>";
        // $.post(base + "trade/payment/check", {
        //         token: true
        //     },
        //     function(data, status) {
        //         console.log(data);
        //     });
    });


    function remove_withdrawal(id) {

        Swal.fire({
            title: "Are you sure want to delete?",
            text: "",
            type: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#34c38f",
            cancelButtonColor: "#f46a6a",
            confirmButtonText: "Yes",
            confirmButtonClass: "btn btn-success mt-2",
            cancelButtonClass: "btn btn-danger ml-2 mt-2",
            buttonsStyling: !1,
        }).then(function(t) {
            if (t.value == true) {
                var url = '<?php echo base_url(); ?>trade/account/remove_withdrawal';
                $.ajax({
                    type: "POST",
                    data: {
                        withdrawal_id: id,
                    },
                    url: url,
                    dataType: "json",
                    success: function(response) {
                        window.location.reload(true);
                    }

                });

            }
        });
    }
</script>