<?php $this->load->view('trading/common/header'); ?>
<div class="page-content" ng-controller="Profile_Controller" ng-cloak>
    <div class="container-fluid p-0 mt-12px">
        <div class="card mb-14px min-h-189">
            <div class="card-body pb-12px">
                <div class="">
                    <h3 class="d-sm-grid"><?= lang('email_verification') ?> <small class="font-size-16 text-46f2d4">(<a class="text-#0b65c6 cursor-pointer" onclick="submit_mail()"><?= lang('click_here_for_verification') ?></a>)</small></h3>
                    <div class="auth-switch">
                        <input disabled type="checkbox" id="switch3" ng-model="google_auth_status" switch="bool" checked />
                        <label for="switch3" data-on-label="On" data-off-label="Off"></label>
                        <br>
                    </div>
                    <div class="text-center" style="padding-bottom: 7px;">
                        <form id="form">
                            <div class="row ml-auto mr-auto justify-content-center mb-2">
                                <label class="mt-3 mb-3 col-12"><?= lang('enter_six_digit_code') ?></label><br>
                                <input type="text" class="input-bordered col-5 text-sm-center fa-22 required form-control text-d9d9d9 mr-2" required maxlengh="6" ng-model="code" placeholder="o-o-o-o-o-o" ui-mask="9-9-9-9-9-9" name="code" id="code">
                                <button type="submit" ng-hide="authsuccess" ng-click="check_auth()" class="btn btn-dark"><?= lang('submit') ?></button>
                                <button type="submit" ng-show="authsuccess" ng-click="off_auth()" class="btn btn-dark"><?= lang('submit') ?></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div></div>
        </div>
    </div>
</div>
<?php $this->load->view('trading/common/footer'); ?>
<script>
    function submit_mail() {
        Swal.fire({
            title: "Are you sure?",
            text: "",
            type: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#34c38f",
            cancelButtonColor: "#f46a6a",
            confirmButtonText: "Yes",
            confirmButtonClass: "btn btn-success mt-2",
            cancelButtonClass: "btn btn-danger ml-2 mt-2",
            buttonsStyling: !1,
        }).then(function(t) {
            if (t.value == true) {
                var url = '<?php echo base_url(); ?>trade/account/send_mail';
                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: "json",
                    success: function(response) {
                        window.location.reload(true);
                    }
                });
            }
        });
    }
</script>