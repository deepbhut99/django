<?php $this->load->view('trading/common/header'); ?>
<div ng-controller="Agency_Controller" ng-init="get_user_info()">
    <div ng-cloak class="page-content">
        <div class="container-fluid p-0">
            <!-- start page title -->
            <div class="row mt-14px">
                <div class="col-xl-2 col-lg-1"></div>
                <div class="col-xl-8 col-lg-10">
                    <div class="row">
                        <div class="row">
                            <!-- <div class="col-xl-6 col-md-6 pr-md-0">
                                <div class="card mb-14px card-hght left-card theme-border">
                                    <div class="card-body pb-4">
                                        <div class="ml-auto mr-auto text-center" ng-show="userLoader">
                                            <img src="<?= base_url('assets/user_panel/images/preloader.gif') ?>" alt="loader">
                                        </div>
                                        <div ng-show="!userLoader">
                                            <div class="row affiliate-row" ng-show="user.is_agency == 0">
                                                <div class="col-md-8">
                                                    <h6 class="text-aff"><?= lang('you_need_buy_agency_licence') ?></h6>
                                                </div>
                                                <div class="col-md-4 text-md-right text-center">
                                                    <button type="button" ng-click="buy_agency('USDT')" class="btn  waves-effect bg-goldenrod text-black border-0">Buy with USDT</button>
                                                </div>
                                            </div>
                                            <div class="text-center view-agency-btn" ng-hide="user.is_agency == 0">
                                                <a role="button" href="<?= base_url('trade/agency') ?>" class="btn btn-secondary waves-effect bg-goldenrod text-black border-0"><?= lang('view_agency') ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                            <div class="col-xl-12 col-md-12">
                                <div class="card mb-px">
                                    <div class="card-body">
                                            <div class="ml-auto mr-auto text-center" ng-show="userLoader">
                                                <img src="<?= base_url('assets/user_panel/images/preloader.gif') ?>" alt="loader">
                                            </div>
                                            <div ng-show="!userLoader">
                                            <div class="row affiliate-row" ng-show="user.is_bottrade == 0">
                                                    <div class="col-md-6">
                                                        <h6 class="text-aff"><?= lang('you_need_buy_agency_licence') ?></h6>
                                                    </div>
                                                    <div class="col-md-6 text-center">
                                                    <button type="button" ng-click="buy_bottrading('USDT')" class="btn  waves-effect bg-goldenrod text-black border-0">Buy with USDT</button>
                                                        <button type="button" ng-click="buy_bottrading('PWT')" class="btn  waves-effect bg-goldenrod text-black border-0">Buy with PWT</button>
                                                    </div>
                                                </div>                       
                                            <div class="" ng-hide="user.is_bottrade == 0">
                                                <div class="row">
                                                    <div class="col-8">
                                                        <h5 class="pb-0.3">Your Refferal Level is</h5>
                                                    </div>
                                                    <div class="col-4">
                                                        <h1 class="fa-4x text-right"><?= isset($bot_trade) ? 'X' . $bot_trade->level : 'X' ?></h1>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="col-xl-12 col-md-12">
                                <div class="card mb-14px right-card theme-border">
                                    <div class="card-body">
                                        <div class="row aff-mt mt-xl-3">
                                            <div class="col-xl-3">
                                                <h4 class="card-title mt-xl-1 mt-lg-3 mt-sm-3 mt-xsm-3"><?= lang('reff_link') ?></h4>
                                            </div>
                                            <div class="col-xl-9">
                                                <div class="form-group">
                                                    <div id="colorpicker-color-pattern" class="input-group colorpicker-component colorpicker-element" data-colorpicker-id="5">
                                                        <input type="text" id="share_link" class="form-control input-lg" value="<?= base_url('signup?user_id=' . $this->session->user_name) ?>" readonly="">
                                                        <span class="input-group-append">
                                                            <span onclick="copy_text();" class="input-group-text colorpicker-input-addon" data-original-title="" title="" tabindex="0"><i class="fas fa-copy"></i></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                            <!-- <div class="col-xl-6 col-md-6">
                            <div class="card theme-border">
                                <div class="card-body pb-3 my-3">
                                    <div class="contain">
                                        <div id="owl-carousel" class="owl-carousel owl-theme">
                                            <div class="affiliate-item">
                                                <div class="mt-2">
                                                    <div class="d-flex justify-content-between">
                                                        <h5>Direct Referral</h5>
                                                        <h6><?= $bottrading1 . ' / ' . $total_downline ?></h6>
                                                    </div>
                                                    <div class="progress h-7px">
                                                        <?php
                                                        $avg_downline = round($mydownline * 100 / $total_downline, 2);
                                                        ?>
                                                        <div class="progress-bar bg-info" role="progressbar" style="width: <?= $avg_downline ?>%" aria-valuenow="<?= $avg_downline ?>" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                            </div>                                           
                                            <div class="affiliate-item">                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-1"></div>
            </div>

            <div class="row mt-14px">
                <div class="col-xl-12 col-md-12">
                    <!-- <div class="card mb-14px right-card">
                        <div class="card-body">
                            <div class="row aff-mt mt-xl-3">
                                <div class="col-xl-3">
                                    <h4 class="card-title mt-xl-1 mt-lg-3 mt-sm-3 mt-xsm-3"><?= lang('reff_link') ?></h4>
                                </div>
                                <div class="col-xl-9">
                                    <div class="form-group">
                                        <div id="colorpicker-color-pattern" class="input-group colorpicker-component colorpicker-element" data-colorpicker-id="5">
                                            <input type="text" id="share_link" class="form-control input-lg" value="<?= base_url('signup?user_id=' . $this->session->user_name) ?>" readonly="">
                                            <span class="input-group-append">
                                                <span onclick="copy_text();" class="input-group-text colorpicker-input-addon" data-original-title="" title="" tabindex="0"><i class="fas fa-copy"></i></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <div class="card mb-6px">
                        <!-- <div class="invite-card">
                            <h2 class="ref-card-head">Invite Your Friends</h2>
                            <div class="ref-icon-sec">
                                <i class="fas fa-share-alt ref-icon"></i>
                            </div>
                        </div> -->
                        <div class="card-body">
                            <div class="ref-content">
                                <h4>Referral Link</h4>
                                <div class="d-flex w-100">
                                    <input type="text" id="share_link" class="form-control input-lg" value="<?= base_url('?user_id=' . $this->session->user_name) ?>" readonly="">
                                    <button onclick="copy_text();" title="click here to copy" class="btn btn-dark"><i class="fas fa-copy"></i></button>
                                </div>
                            </div>
                            <!-- <div class="mt-3">
                                <h5 class="text-center">Share On Social Media</h5>
                            </div> -->
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <table class="table display table-striped table-bordered dt-responsive nowrap table-borderless" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                    <tr>
                                        <th><?= lang('sr_no') ?></th>
                                        <th><?= lang('date') ?></th>
                                        <th><?= lang('uunme') ?></th>
                                        <th><?= lang('email') ?></th>
                                        <th>XBOT</th>
                                        <th>Agency</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $count = 1;
                                    $active = 0;
                                    foreach ($users as $user) {
                                        $active += $user['is_status'] == '1' ? 1 : 0;
                                    ?>
                                        <tr>
                                            <td><?= $count++ ?></td>
                                            <td><?= date('d M, Y', strtotime($user['created_time']))  ?></td>
                                            <td class="text-capitalize"><?= $user['username'] ?></td>
                                            <td><?= $user['email'] ?></td>
                                            <td><span class="<?= $user['is_bottrade'] == '1' ? 'text-success' : 'text-danger' ?>"><?= $user['is_bottrade'] == '1' ? 'Active' : 'Inactive'  ?></span></td>
                                            <td><span class="<?= $user['is_agency'] == '1' ? 'text-success' : 'text-danger' ?>"><?= $user['is_agency'] == '1' ? 'Active' : 'Inactive'  ?></span></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade buy-bot" tabindex="-1" role="dialog" id="buybotModal" aria-labelledby="buy-bot" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0 ml-auto"><?= lang('confirm_your_particiaption') ?></h5>
                    <button type="button" class="close text-d9d9d9" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body text-center">
                    <img class="pt-3 pb-3" src="<?= base_url('assets/user_panel/images/dashboard/hand-shake.png') ?>" alt="modal-image">
                    <h6 class="pl-5 pr-5 pb-3">You need to pay {{buyType == 'EFX' ? 200 : 100}} {{buyType}} to join in Bot License. Do you want to pay it?</h6>
                    <div>
                        <input class="mr-1" type="checkbox" checked>
                        <label for="vehicle1"> <?= lang('i_confirm_accept_tc') ?> <a href=""><?= lang('terms_of_service') ?></a></label><br>
                        <button type="button" ng-click="make_bot_payment(buyType)" class="btn btn-secondary waves-effect border-0 w-75 mt-1"><?= lang('confirm') ?></button>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
</div>

<?php $this->load->view('trading/common/footer'); ?>
<script>
    $(document).ready(function() {
        $('table.display').DataTable({
            order: [
                [0, "asc"]
            ]
        });
        $('#activeUsers').html(<?= $active ? $active : 0 ?>);
        $('#inactiveUsers').html(<?= count($users) - ($active ? $active : 0) ?>);
    });

    function copy_text() {
        var copyText = document.getElementById("share_link");
        copyText.select();
        document.execCommand("copy");
        toastr.success('Copied Successfully', '', toast);
        $("#copy_button_id").html('Copied');
        setTimeout(function() {
            $("#copy_button_id").html('Copy');
        }, 3000);
    }

    $('#btn-confirm').click(function() {
        $('#modal1').modal('hide');
    });

    $('#owl-carousel').owlCarousel({
        loop: true,
        margin: 30,
        dots: false,
        nav: false,
        items: 1,
        autoplay: false,
        autoplayTimeout: 2500,
    })
</script>