<?php $this->load->view('trading/common/header'); ?>
<div class="page-content p-mobile pl-0 pr-0" ng-controller="Agency_Controller">
    <div class="container-fluid p-mobile pl-0 pr-0">
        <div class="card border-radius-0 h-45px mb-14px">
            <div class="card-body pb-0 pt-0 pl-0">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-tabs-custom border-bottom-0" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link pt-12px pb-12px active bg-transparent p-community" data-toggle="tab" href="#overview" role="tab">
                            <span class="d-block d-sm-none"><i class="fas fa-search-plus"></i></span>
                            <span class="d-none d-sm-block"><?= lang('overview') ?></span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link pt-12px pb-12px bg-transparent p-portfolio" data-toggle="tab" href="#commission" role="tab">
                            <span class="d-block d-sm-none"><i class="fas fa-dollar-sign"></i></span>
                            <span class="d-none d-sm-block"><?= lang('commission') ?></span>
                        </a>
                    </li>
                    <li class="nav-item bg-transparent">
                        <a class="nav-link pt-12px pb-12px p-expert" data-toggle="tab" href="#network" role="tab">
                            <span class="d-block d-sm-none"><i class="fa fa-link"></i></span>
                            <span class="d-none d-sm-block"><?= lang('network_management') ?></span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- Tab panes -->
        <div class="tab-content">
            <div class="tab-pane active" id="overview" role="tabpanel">
                <div class="row">
                    <div class="col-lg-3 col-md-6 pr-lg-0 pr-md-0">
                        <div class="card mb-14px theme-border">
                            <div class="card-body">
                                <h6 class="opacity-half mb-0"><?= lang('total_refferrals') ?></h6>
                                <h2 class="mb-0"><?= $referrals ?></h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 pr-lg-0">
                        <div class="card mb-14px theme-border">
                            <div class="card-body">
                                <h6 class="opacity-half mb-0"><?= lang('total_agencies') ?></h6>
                                <h2 class="mb-0"><?= $agencies ?></h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 pr-lg-0 pr-md-0">
                        <div class="card mb-14px theme-border">
                            <div class="card-body">
                                <h6 class="opacity-half mb-0"><?= lang('trading_commission') ?></h6>
                                <h2 class="mb-0">$ <?= $trading_commission ? $trading_commission : 0 ?></h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="card mb-14px theme-border">
                            <div class="card-body">
                                <h6 class="opacity-half mb-0"><?= lang('agency_commission') ?></h6>
                                <h2 class="mb-0">$ <?= $agency_commission ? $agency_commission : 0 ?></h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-9 col-lg-8 col-md-12 pr-lg-1">
                        <div class="card theme-border">
                            <div class="card-body pb-1">
                                <h5 class="pb-3"><?= lang('invite_now') ?></h5>
                                <div class="row">
                                    <div class="col-md-6">
                                        <h4 class="card-title"><?= lang('reff_link') ?></h4>
                                        <div class="form-group">
                                            <div id="colorpicker-color-pattern" class="input-group colorpicker-component colorpicker-element" data-colorpicker-id="5">
                                                <input type="text" id="share_link" class="form-control input-lg " value="<?= base_url('signup?user_id=' . $this->session->user_name) ?>" readonly="">
                                                <span class="input-group-append">
                                                    <span role="button" onclick="copy_text();" class="input-group-text colorpicker-input-addon" data-original-title="" title="" tabindex="0"><i class="fas fa-copy"></i></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xl-4 col-lg-12 pr-xl-0">
                                        <div class="card mb-sm-14px" style="background-color: rgba(255, 255, 255, 0.05);">
                                            <div class="card-body pb-3">
                                                <div class="row">
                                                    <div class="col-xxl-2 col-xl-3 col-lg-1 col-md-1 pb-3">
                                                        <div class="mn-stat">
                                                            <i class="fa fa-envelope icon-agency font-size-26" aria-hidden="true"></i>
                                                        </div>
                                                    </div>
                                                    <div class="col-xxl-10 col-xl-9 col-lg-11 col-md-11">
                                                        <div class="pl-agency">
                                                            <h5><?= lang('invite_others') ?></h5>
                                                            <h6 class="opacity-half font-size-12"><?= lang('invite_yr_frnds_to_reg') ?></h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-4 col-lg-12 pr-xl-0">
                                        <div class="card mb-sm-14px" style="background-color: rgba(255, 255, 255, 0.05);">
                                            <div class="card-body pb-3">
                                                <div class="row">
                                                    <div class="col-xxl-2 col-xl-3 col-lg-1 col-md-1 pb-3">
                                                        <div class="mn-stat">
                                                            <i class="fas mt-3 fa-user-circle icon-agency font-size-26" aria-hidden="true"></i>
                                                        </div>
                                                    </div>
                                                    <div class="col-xxl-10 col-xl-9 col-lg-11 col-md-11">
                                                        <div class="pl-agency">
                                                            <h5><?= lang('register_successfully') ?></h5>
                                                            <h6 class="opacity-half font-size-12"><?= lang('the_others_sign_up') ?></h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-4 col-lg-12">
                                        <div class="card" style="background-color: rgba(255, 255, 255, 0.05);">
                                            <div class="card-body pb-3">
                                                <div class="row">
                                                    <div class="col-xxl-2 col-xl-3 col-lg-1 col-md-1 pb-3">
                                                        <div class="mn-stat">
                                                            <!-- <img height="40px" src="<?= base_url('assets/user_panel/images/icon/agency01.png') ?>" alt=""> -->
                                                            <i class="fas mt-3 fa-money-bill-alt icon-agency font-size-26" aria-hidden="true"></i>
                                                        </div>
                                                    </div>
                                                    <div class="col-xxl-10 col-xl-9 col-lg-11 col-md-11">
                                                        <div class="pl-agency">
                                                            <h5><?= lang('earn_commission') ?></h5>
                                                            <h6 class="opacity-half font-size-12"><?= lang('rcv_up_to_fifty') ?></h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-12">
                        <div class="card bg-6083d9 mb-14px">
                            <div class="card-body text-black">
                                <h5 class="mb-0"><?= lang('your_ref_level') ?></h5>
                                <h2 class=" mb-0"><?= lang('rank') ?> F<?= $mylevel ?></h2>
                                <h6 class="mb-1"><?= lang('reff_requrmnrt') ?></h6>
                            </div>
                        </div>
                        <div class="card theme-border">
                            <div class="card-body pt-2rem">
                                <div>
                                    <div class="d-flex justify-content-between">
                                        <h6>F<?= $nextLevel ?> <?= lang('agency') ?></h6>
                                        <h6><?= $mydownline . ' / ' . $total_downline ?></h6>
                                    </div>
                                    <div class="progress mb-4 h-7px">
                                        <?php
                                        $avg_downline = round($mydownline * 100 / $total_downline, 2);
                                        ?>
                                        <div class="progress-bar bg-info" role="progressbar" style="width: <?= $avg_downline ?>%" aria-valuenow="<?= $avg_downline ?>" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-between">
                                    <h6>F<?= $nextLevel ?> <?= lang('volume') ?></h6>
                                    <h6>$<?= $myvolume . ' / $' . $total_volume ?></h6>
                                </div>
                                <div class="progress mb-4 h-7px">
                                    <?php
                                    $avg_volume = round($myvolume * 100 / $total_volume, 2);
                                    ?>
                                    <div class="progress-bar bg-info" role="progressbar" style="width: <?= $avg_volume ?>%" aria-valuenow="<?= $avg_volume ?>" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="commission" role="tabpanel">
                <div>
                    <form>
                        <div class="row">
                            <div class="col-xl-7 col-lg-7 col-md-6 col-sm-4"></div>
                            <div class="col-xl-5 col-lg-5 col-md-6 col-sm-8">
                                <div class="wrapper-new d-flex">
                                    <input ng-model="startdate" id="startdate" type="text" class="form-control ml-3 mr-3" placeholder="Start Date" readonly>
                                    <input ng-model="enddate" id="enddate" type="text" class="form-control mr-3" placeholder="End Date" readonly>
                                    <button type="button" onclick="get_history()" id="history" class="btn btn-primary waves-effect waves-light mr-1 pr-4 mr-3 srchbtn" style="height: 38px; width:41px !important"><i class="fa fa-search" aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="card theme-border">
                    <div class="card-body">
                        <div class="ml-auto mr-auto text-center d-none" id="dataloader">
                            <img src="<?= base_url('assets/user_panel/images/preloader.gif') ?>" alt="loader">
                        </div>
                        <table id="datatable" class="table dt-responsive nowrap table-striped table-borderless" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <tr>
                                    <th><?= lang('sr_no')?></th>
                                    <th><?= lang('time') ?></th>
                                    <th><?= lang('uunme') ?></th>
                                    <th><?= lang('level') ?></th>
                                    <th><?= lang('type') ?></th>
                                    <th><?= lang('amount') ?></th>
                                    <th><?= lang('earned') ?></th>
                                </tr>
                            </thead>
                            <tbody></tbody>


                        </table>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="network" role="tabpanel">
                <div class="card theme-border">
                    <div class="card-body">
                        <div>
                            <table class="table display dt-responsive nowrap table-striped table-borderless" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                    <tr>
                                        <th><?= lang('sr_no')?></th>
                                        <th><?= lang('uunme') ?></th>
                                        <th><?= lang('sponser') ?></th>
                                        <th><?= lang('agency_licenece') ?></th>
                                        <th><?= lang('agencies') ?></th>
                                        <th><?= lang('total_vol') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $count = 1;
                                    foreach ($networks as $row) { ?>
                                        <tr>
                                            <td><?= $count++ ?></td>
                                            <td><?= $row['username'] ?></td>
                                            <td><?= $row['sponsor'] ?></td>
                                            <td><i class="fa <?= $row['is_agency'] == 1 ? 'fa-check-circle text-success' : 'fa-times-circle text-danger' ?>" aria-hidden="true"></i></td>
                                            <td><?= $row['tree_count'] ?></td>
                                            <td>$ <?= $row['total_trade'] ? number_format($row['total_trade'], 2) : 0 ?></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- container-fluid -->
</div>

<?php $this->load->view('trading/common/footer'); ?>

<script>
    $(".pagination-item-copy").on("click", function() {
        $(".pagination-item-copy").removeClass("active");
        $(this).addClass("active");
    });
</script>
<script>
    $(document).ready(function() {
        $('table.display').DataTable();
    });
</script>
<script>
    $(document).ready(function() {
        var table = $("#datatable").DataTable({
            'pagingType': "simple",
            'order': [
                [0, "asc"]
            ]
        });
        var history = <?= json_encode($history) ?>;
        count = 1;
        history.forEach((row) => {
            table.row.add(
                [
                    count++,
                    moment(row.created_date).format('Do MMM, YYYY'),
                    row.sponsor_user,
                    row.level,
                    row.commission_type == 1 ? "Agency Commission" : 'Trading Commission',
                    row.amount,
                    row.earned
                ]
            ).draw(false);
        })

        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
            $($.fn.dataTable.tables(true)).DataTable()
                .columns.adjust()
                .responsive.recalc();
        });
    });

    function get_history() {
        var end = $('#enddate').val();
        var start = $('#startdate').val();
        var table = $('#datatable').DataTable({
            destroy: true,
            pagingType: "simple",
            order: [
                [0, "asc"]
            ]
        }).clear().draw();
        $.ajax({
            type: 'post',
            url: '<?= base_url("trade/agency/get_range_history") ?>',
            data: {
                start: $('#startdate').val(),
                end: $('#enddate').val()
            },
            success: function(res) {
                res = JSON.parse(res);
                count = 1;
                res.forEach((row) => {
                    table.row.add(
                        [
                            count++,
                            moment(row.created_date).format('Do MMM, YYYY'),
                            row.sponsor_user,
                            row.level,
                            row.commission_type == 1 ? "Agency Commission" : 'Trading Commission',
                            row.amount,
                            row.earned
                        ]
                    ).draw(false);
                })
            }
        })
    }
    function copy_text() {
        var copyText = document.getElementById("share_link");
        copyText.select();
        document.execCommand("copy");
        toastr.success('Copied Successfully');
    }
</script>