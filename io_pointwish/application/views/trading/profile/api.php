<?php $this->load->view('trading/common/header'); ?>
<div class="page-content mx-cryp    " ng-controller="Profile_Controller" ng-cloak>
    <div class="container-fluid p-0 mt-12px">
        <div class="row">
            <div class="col-sm-6 px-3px">
                <div class="card h-md-225px mb-6px">
                    <div class="card-body">
                        <h3 data-toggle="modal"><?= lang('binapi') ?></h3>

                        <div class="mt-20px">
                            <?php if ($user_data->binance_apikey_active_check == 0) { ?>

                                <form data-single="true" id="form3" name="form3" action="<?php echo base_url('account/binanceapikey'); ?>" method="POST" enctype="multipart/form-data" class="px-1">
                                    <div class="tab my-1">
                                        <div class="form-group">
                                            <input class="form-control required" name="apikey" id="apikey" placeholder="Api Key" required>
                                        </div>
                                    </div>
                                    <div class="tab">
                                        <div class="form-group">
                                            <input class="form-control required" name="securitykey" id="securitykey" placeholder="Security Key" required>
                                        </div>
                                    </div>

                                    <div style="overflow:auto;">
                                        <div>
                                            <button type="button" class="previous btn btn-dark waves-effect waves-light">Previous</button>
                                            <button type="button" class="next btn btn-dark waves-effect waves-light">Next</button>
                                            <button type="button" class="submit btn btn-dark waves-effect waves-light">Submit</button>
                                        </div>
                                    </div>
                                    <!-- Circles which indicates the steps of the form: -->
                                    <div class="d-none">
                                        <span class="step">1</span>
                                        <span class="step">2</span>

                                    </div>
                                </form>
                            <?php } else { ?>

                                <form data-single="true" id="form4" name="form4" action="<?php echo base_url('account/reset_binance_address'); ?>" method="POST" enctype="multipart/form-data" class="px-1">
                                    <div class="tab my-1">
                                        <div class="form-group">
                                            <input class="form-control required" name="apikey1" id="apikey1" placeholder="<?php echo ($user_data->binance_apikey) ?>" readonly>
                                        </div>
                                    </div>
                                    <div style="overflow:auto;">
                                        <div style="margin-top: 0.7rem;">
                                            <button type="button" class="submit btn btn-primary waves-effect waves-light"><?= lang('reset') ?></button>

                                        </div>
                                    </div>
                                </form>

                            <?php } ?>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-sm-6 px-3px mb-6px">
                <div class="card h-md-225px">
                    <div class="card-body">
                        <div>
                            <h3><?= lang('hubapi') ?></h3>
                            <!-- <div style="margin-top: -5px;">
                                        <small class="cursor-pointer" role="button" class="text-theme-green">*View sample document</small> <br>
                                    </div> -->
                        </div>
                        <div class="mt-5">
                            <h2 class="text-center">Coming Soon</h2>
                        </div>
                        <!-- <div class="mt-20px">
                                    <?php if (empty($id_data) || (!empty($id_data) && $id_data->status == 3)) { ?>
                                        <form data-single="true" id="form2" name="form2" action="<?php echo base_url('account/upload_document'); ?>" method="POST" enctype="multipart/form-data" class="px-1">                                    
                                            <div class="form-group">
                                                <input ng-model="trx_address" ng-disabled="address.trx_address" type="text" id="trx_address" name="trx_address" class="form-control required" required="" placeholder="TRX Address">
                                            </div>
                                            <div class="tab">
                                                <span>Upload ID Proof</span>
                                                <div class="fallback mt-1 w-75" id="idproof">
                                                    <input class="dropzone w-100" name="idproof" type="file">
                                                </div>
                                            </div>
                                            <div class="tab">
                                                <span class="pwt-price-web">Upload Sample Document</span>
                                                <img class="float-sm-right my-xl-n5 w-115 cursor-pointer" src="<?= base_url('assets/user_panel/images/banner/sample.svg') ?>" alt="">
                                                <div class="fallback mt-1 w-75" id="file">
                                                    <input class="dropzone w-100" name="file" type="file">
                                                </div>
                                            </div>
                                            <div style="overflow:auto;">
                                                <div style="margin-top: 0.7rem;">
                                                    <button type="button" class="previous btn btn-primary waves-effect waves-light">Previous</button>
                                                    <button type="button" class="next btn btn-primary waves-effect waves-light">Next</button>
                                                    <button type="button" class="submit btn btn-primary waves-effect waves-light">Submit</button>
                                                </div>
                                            </div>
                                            <div class="d-none">
                                                <span class="step">1</span>
                                                <span class="step">2</span>
                                                <span class="step">3</span>
                                            </div>
                                        </form>
                                    <?php } else if (!empty($id_data) && ($id_data->status == 0 || $id_data->status == 1 || $id_data->status == 2)) { ?>
                                        <?php
                                        if (!empty($id_data) && ($id_data->status == 0)) {
                                        ?>
                                            <p class="text-center mt-lg-4"><span class="fa-2x font-weight-bold text-warning"><?= lang('pending') ?></span></p>
                                        <?php
                                        } else if (!empty($id_data) && ($id_data->status == 1)) {
                                        ?>
                                            <p class="text-center mt-lg-4"><span class="fa-2x font-weight-bold text-02C076"><?= lang('approve') ?></span></p>
                                        <?php
                                        } else if (!empty($id_data) && ($id_data->status == 2)) {
                                        ?>
                                            <p class="text-center mb-0"><span class="fa-2x font-weight-bold text-danger"><?= lang('reject') ?></span></p>
                                            <p class="text-center mb-2">(Reason: <?= $id_data->reject_reason ?>)</p class="text-center">
                                            <p class="text-center"><button type="button" onclick="location.href='<?= base_url('reupload/' . $id_data->id) ?>'" class="submit btn btn-primary waves-effect waves-light">Click here to upload again</button></p>
                                        <?php
                                        }
                                        ?>
                                    <?php } ?>
                                </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('trading/common/footer'); ?>

<script>
    $(document).ready(function() {
        $("#apikey").focusin(function() {
            $(this).removeClass("error1")
        });
        $("#securitykey").focusin(function() {
            $(this).removeClass("error1")
        });

        var val = {
            errorPlacement: function(error, element) {
                // name attrib of the field
                var n = element.attr("name");
                if (n == "apikey")
                    element.attr("placeholder", "Api Key");
                else if (n == "securitykey")
                    element.attr("placeholder", "Security Key");

            },
            rules: {
                apikey: "required",
                securitykey: "required",

            },
            highlight: function(element) {
                $(element).addClass('error1');
            },
            unhighlight: function(element) {
                $(element).removeClass('error1');
            },
            messages: {}
        }
        $("#form3").multiStepForm({
            // defaultStep:0,
            beforeSubmit: function(form, submit) {
                console.log("called before submiting the form");
                console.log(form);
                console.log(submit);
            },
            validations: val,
        }).navigateTo(0);

        $("#form4").multiStepForm({
            // defaultStep:0,
            beforeSubmit: function(form, submit) {
                console.log("called before submiting the form");
                console.log(form);
                console.log(submit);
            },
            validations: val,
        }).navigateTo(0);

    });

    function submit_mail() {

        Swal.fire({
            title: "Are you sure?",
            text: "",
            type: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#34c38f",
            cancelButtonColor: "#f46a6a",
            confirmButtonText: "Yes",
            confirmButtonClass: "btn btn-success mt-2",
            cancelButtonClass: "btn btn-danger ml-2 mt-2",
            buttonsStyling: !1,
        }).then(function(t) {
            if (t.value == true) {
                var url = '<?php echo base_url(); ?>trade/account/send_mail';
                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: "json",
                    success: function(response) {
                        window.location.reload(true);
                    }

                });

            }
        });
    }
</script>