<?php $this->load->view('trading/common/header'); ?>
<div class="page-content" ng-controller="Profile_Controller" ng-cloak>
    <div class="container-fluid p-0 mt-12px">
        <div class="card mb-14px">
            <div class="card-body">
                <div>
                    <h3><?= lang('change_password') ?> </h3>
                    <div style="margin-top: -5px;">
                        <small class="text-46f2d4">*<?= lang('you_must_enable') ?></small> <br>
                    </div>
                </div>
                <div class="row mt-1">
                    <div class="col-12">
                        <form id="change_password_form" name="change_password_form" class="custom-validation" method="POST">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="ml-1"><?= lang('current_password') ?></label>
                                        <input type="password" ng-model="old_password" id="old_password" name="old_password" class="form-control required" required="" placeholder="<?= lang('current_password') ?>">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="ml-1"><?= lang('new_password') ?></label>
                                        <input type="password" ng-model="new_password" id="new_password" name="new_password" class="form-control required" required="" placeholder="<?= lang('new_password') ?>">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="ml-1"><?= lang('confirm_password') ?></label>
                                        <input type="password" ng-model="confirm_password" id="confirm_password" name="confirm_password" class="form-control required" required="" placeholder="<?= lang('confirm_password') ?>" equalTo="#new_password">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group mb-0">
                                <div class="ml-1 text-center">
                                    <button type="submit" ng-click="change_password();" class="btn btn-dark waves-effect waves-light mr-1" ng-disabled="!authsuccess">
                                        <?= lang('submit') ?>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('trading/common/footer'); ?>