<?php $this->load->view('trading/common/header'); ?>
<div class="page-content mx-cryp" ng-controller="Profile_Controller" ng-cloak>
    <div class="container-fluid p-0 mt-12px">
        <div class="row">
            <div class="col-md-6 col-sm-6 px-3px">
                <div class="card mb-6px">
                    <div class="card-body">
                        <div>
                            <h3><?= lang('eth_usdt_pwt_address') ?></h3>
                            <div style="margin-top: -5px;">
                                <small class="text-46f2d4">*<?= lang('you_must_enable_eth') ?></small> <br>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-12">
                                <form class="custom-validation" method="POST">
                                    <div class="form-group">
                                        <input ng-model="eth_address" ng-disabled="address.eth_address" type="text" id="eth_address" name="eth_address" class="form-control required" required="" placeholder="<?= lang('eth_address') ?>">
                                    </div>
                                    <div class="form-group mb-0">
                                        <div class="text-center">
                                            <button ng-hide="address.eth_address" ng-click="set_eth_address()" type="submit" class="btn btn-dark waves-effect waves-light mr-1" ng-disabled="!authsuccess">
                                                <?= lang('submit') ?>
                                            </button>
                                            <button ng-show="address.eth_address" ng-click="reset_wallet_address('eth_address')" type="submit" class="btn btn-secondary waves-effect waves-light mr-1" ng-disabled="!authsuccess">
                                                <?= lang('reset') ?>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 px-3px">
                <div class="card mb-6px">
                    <div class="card-body">
                        <div>
                            <h3><?= lang('btc_address') ?></h3>
                            <div style="margin-top: -5px;">
                                <small class="text-46f2d4">*<?= lang('you_must_enable_btc') ?></small> <br>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-12">
                                <form class="custom-validation" method="POST">
                                    <div class="form-group">
                                        <input ng-model="btc_address" ng-disabled="address.btc_address" type="text" id="btc_address" name="btc_address" class="form-control required" required="" placeholder="<?= lang('btc_address') ?>">
                                    </div>
                                    <div class="form-group mb-0">
                                        <div class="text-center">
                                            <button ng-hide="address.btc_address" ng-click="set_btc_address()" type="submit" class="btn btn-dark waves-effect waves-light mr-1" ng-disabled="!authsuccess">
                                                <?= lang('submit') ?>
                                            </button>
                                            <button ng-show="address.btc_address" ng-click="reset_wallet_address('btc_address')" type="submit" class="btn btn-secondary waves-effect waves-light mr-1" ng-disabled="!authsuccess">
                                                <?= lang('reset') ?>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 px-3px">
                <div class="card mb-6px">
                    <div class="card-body">
                        <div>
                            <h3>TRX Address</h3>
                            <div style="margin-top: -5px;">
                                <small class="text-46f2d4">*You must enable 2FA to edit TRX Address</small> <br>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-12">
                                <form class="custom-validation" method="POST">
                                    <div class="form-group">
                                        <input ng-model="trx_address" ng-disabled="address.trx_address" type="text" id="trx_address" name="trx_address" class="form-control required" required="" placeholder="TRX Address">
                                    </div>
                                    <div class="form-group mb-0">
                                        <div class="text-center">
                                            <button ng-hide="address.trx_address" ng-click="set_trx_address()" type="submit" class="btn btn-dark waves-effect waves-light mr-1" ng-disabled="!authsuccess">
                                                <?= lang('submit') ?>
                                            </button>
                                            <button ng-show="address.trx_address" ng-click="reset_wallet_address('trx_address')" type="submit" class="btn btn-secondary waves-effect waves-light mr-1" ng-disabled="!authsuccess">
                                                <?= lang('reset') ?>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 px-3px">
                <div class="card mb-6px">
                    <div class="card-body">
                        <div>
                            <h3><?= lang('ltc_address') ?></h3>
                            <div style="margin-top: -5px;">
                                <small class="text-46f2d4">*<?= lang('you_must_enable_ltc') ?></small> <br>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-12">
                                <form class="custom-validation" method="POST">
                                    <div class="form-group">
                                        <input ng-model="ltcaddress" ng-disabled="address.ltc_address" type="text" id="ltc_address" name="ltc_address" class="form-control required" required placeholder="<?= lang('ltc_address') ?>">
                                    </div>
                                    <div class="form-group mb-0">
                                        <div class="text-center">
                                            <button ng-hide="address.ltc_address" ng-click="set_ltc_address()" type="submit" class="btn btn-dark waves-effect waves-light mr-1" ng-disabled="!authsuccess">
                                                <?= lang('submit') ?>
                                            </button>
                                            <button ng-show="address.ltc_address" ng-click="reset_wallet_address('ltc_address')" type="submit" class="btn btn-secondary waves-effect waves-light mr-1" ng-disabled="!authsuccess">
                                                <?= lang('reset') ?>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<?php $this->load->view('trading/common/footer'); ?>