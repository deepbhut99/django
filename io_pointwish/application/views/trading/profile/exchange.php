<?php $this->load->view('trading/common/header'); ?>
<!-- Wallet Page Css -->
<link href="<?= base_url('assets/user_panel/css/exchange.css?v=' . rand()) ?>" rel="stylesheet" type="text/css" />
<?php $session_data = getSessionData(); ?>
<div class="page-content">
    <div class="container-fluid px-9px">
        <section class="mt-14px">
            <div class="p-0">
                <div class="row">
                    <div class="col-12">
                        <div class="card mb-6px">
                            <div class="card-body">
                                <h3><?= lang('last_pwt_price') ?></h3>
                                <div class="">
                                    <span class="badge badge-theme mr-2 mb-2 pwt-price-first"><?= number_format(get_pwt()['current_price'], 2) ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card mb-6px">
                            <div class="card-body">
                                <h3 class="ml-n2">1 PWT = <?= number_format(get_pwt()['current_price'], 2) ?> USDT</h3>
                                <div class="result-crypto"></div>
                                <form id="convertForm" name="convertForm" class="" method="post">
                                    <div class="row">
                                        <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-xs-12 ">
                                            <div class="mb-2">
                                                <span class="badge badge-theme active" id="fromCurrecy">PWT</span>
                                            </div>
                                            <input required class="input--squared parent-balance m-0 required bg-input3" placeholder="0.00" id='value' name="name" type="number" min="10" oninput="calc_usdt_price()">
                                            <span>
                                                <h6 class="balance top-53"><?= lang('balance') ?> <br> <span id="fromWallet" class="float-right"><?= number_format($session_data->unlocked_pwt, 2) ?> PWT</span></h6>
                                            </span>
                                        </div>
                                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 col-xs-12 text-center">
                                            <i class="fa fa-arrow-circle-right arow-right" aria-hidden="true" id="go" onclick="swapValues()"></i>
                                        </div>
                                        <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                            <div class="mb-2">
                                                <span class="badge badge-theme active" id="toCurrecy">USDT</span>
                                            </div>
                                            <input class="input--squared m-0 bg-input3" id='value1' placeholder="0.00" name="name" type="number" readonly="">
                                            <span id="getAmount">&nbsp;</span>
                                            <span>
                                                <h6 class="balance"><?= lang('balance') ?><br> <span id="toWallet" class="float-right">$<?= number_format($session_data->wallet_amount, 2) ?></span></h6>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="row pt-3">
                                        <div class="col-12">
                                            <button type="submit" class="btn btn-dark btn-block">
                                                <?= lang('convert') ?>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <table id="exchangeData" class="table display dt-responsive nowrap table-striped table-borderless" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th><?= lang('sr_no') ?></th>
                                            <th><?= lang('date') ?></th>
                                            <th><?= lang('from') ?></th>
                                            <th><?= lang('amount') ?></th>
                                            <th><?= lang('to') ?></th>
                                            <th><?= lang('amount') ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $count = 1;
                                        foreach ($history as $row) { ?>
                                            <tr>
                                                <td><?= $count++ ?></td>
                                                <td><?= date('M d, Y', strtotime($row['date'])); ?></td>
                                                <td><?= $row['from_currency'] ?></td>
                                                <td><?= number_format($row['amount'], 2) ?></td>
                                                <td><?= $row['to_currency'] ?></td>
                                                <td><?= number_format($row['conv_amount'], 2) ?></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<div class="modal fade" id="exchange-modal" style="overflow:hidden">
    <div class="modal-dialog modal-dialog-centered max-width-700">
        <div class="modal-content bg-transparent max-width-0">
            <img src="<?= base_url('assets/user_panel/images/banner/73.jpg') ?>" class="ml-1 border-grey modal-banner" alt="">
        </div>
    </div>
</div>
<?php $this->load->view('trading/common/footer'); ?>

<script>
    $(document).ready(function() {
        $('#convertForm').validate();
        var table = $("#exchangeData").DataTable({
            "pagingType": "simple",
        });
    });
    // $('#go').click(function() {
    //     if ($('#go').hasClass('fa-arrow-circle-right')) {
    //         $('#go').removeClass('fa-arrow-circle-right');
    //         $('#go').addClass('fa-arrow-circle-left');
    //     } else {
    //         $('#go').addClass('fa-arrow-circle-right');
    //         $('#go').removeClass('fa-arrow-circle-left');
    //     }
    // });
</script>

<script>
    function swapValues() {
        var val = $('#value').val();
        var val1 = $('#value1').val();
        var lbl = $('#fromCurrecy').text();
        var lbl1 = $('#toCurrecy').text();
        var wallet = $('#fromWallet').text();
        var wallet1 = $('#toWallet').text();
        $('#value').val(val1);
        $('#value1').val(val);
        $('#fromCurrecy').text(lbl1);
        $('#toCurrecy').text(lbl);
        $('#fromWallet').text(wallet1);
        $('#toWallet').text(wallet);
        calc_usdt_price();
    }

    function calc_usdt_price() {
        let pwtPrice = "<?= get_pwt()['current_price'] ?>";
        let agency = "<?= $session_data->is_agency ?>";
        let amount = $('#value').val();
        let convertPrice = 0;
        var calc = 0,
            getAmount = 0;
        if ($('#fromCurrecy').text() == "PWT") {
            calc = +amount * +pwtPrice;
        } else {
            calc = +amount / +pwtPrice;
        }
        convertPrice = calc;
        getAmount = agency == 1 ? calc : calc - calc * 0.001; //Others 0.1% charge;
        let p = +convertPrice;
        $('#value1').val(p.toFixed(2));
        $('#getAmount').text('You will get ' + getAmount.toLocaleString());
    }

    $("#convertForm").submit(function(e) {
        e.preventDefault();
        var form = $("#convertForm");
        if (form.valid() == false) {
            return;
        } else {
            Swal.fire({
                title: "Are you sure?",
                text: "",
                type: "warning",
                showCancelButton: !0,
                confirmButtonColor: "#34c38f",
                cancelButtonColor: "#f46a6a",
                confirmButtonText: "Yes",
                confirmButtonClass: "btn btn-success mt-2",
                cancelButtonClass: "btn btn-danger ml-2 mt-2",
                buttonsStyling: !1,
            }).then(function(t) {
                if (t.value == true) {
                    var url = '<?php echo base_url(); ?>trade/account/convert_price';
                    $.ajax({
                        type: "POST",
                        data: {
                            amount: $('#value').val(),
                            convert: $('#value1').val(),
                            fromCurrecy: $('#fromCurrecy').text(),
                            toCurrecy: $('#toCurrecy').text(),
                        },
                        url: url,
                        dataType: "json",
                        success: function(response) {
                            if (response.success == true) {
                                toastr.success(response.message);
                                window.location.reload(true);
                            } else {
                                toastr.error(response.message);
                            }
                        }

                    });

                }
            });
        }
    })

    // $(".badge-1").on("click", function() {
    //     $(".badge-1").removeClass("active");
    //     $(this).addClass("active");
    // });

    // $(".badge-2").on("click", function() {
    //     $(".badge-2").removeClass("active-1");
    //     $(this).addClass("active-1");
    // });

    // $(".badge-txt-pwt").on("click", function() {
    //     // $(".badge-disabled").css("pointer-events", "none");
    //     // $(".badge-disabled").off('click');
    //     $(".badge-pwt").addClass("badge-disabled");
    //     $(".badge-btc").removeClass("badge-disabled");
    //     $(".badge-eth").removeClass("badge-disabled");
    //     $(".badge-usdt").removeClass("badge-disabled");
    // });

    // $(".badge-txt-btc").on("click", function() {
    //     // $(".badge-disabled").css("pointer-events", "none");
    //     // $(".badge-disabled").off('click');
    //     $(".badge-pwt").removeClass("badge-disabled");
    //     $(".badge-btc").addClass("badge-disabled");
    //     $(".badge-eth").removeClass("badge-disabled");
    //     $(".badge-usdt").removeClass("badge-disabled");
    // });

    // $(".badge-txt-eth").on("click", function() {
    //     $(".badge-pwt").removeClass("badge-disabled");
    //     $(".badge-btc").removeClass("badge-disabled");
    //     $(".badge-eth").addClass("badge-disabled");
    //     $(".badge-usdt").removeClass("badge-disabled");
    // });

    // $(".badge-txt-usdt").on("click", function() {
    //     $(".badge-pwt").removeClass("badge-disabled");
    //     $(".badge-btc").removeClass("badge-disabled");
    //     $(".badge-eth").removeClass("badge-disabled");
    //     $(".badge-usdt").addClass("badge-disabled");
    // });
</script>