<?php $this->load->view('trading/common/header'); ?>
<div class="page-content" ng-controller="Profile_Controller" ng-cloak>
    <div class="container-fluid p-0">
        <!-- start page title -->
        <!-- end page title -->
        <div class="row mt-14px">
            <div class="col-xl-1 col-lg-12 col-md-12"></div>
            <div class="col-xl-5 col-lg-6 col-md-6 pr-md-0">

            </div>
            <div class="col-xl-5 col-lg-6 col-md-6">

            </div>
            <div class="col-xl-1"></div>
        </div>
        <div class="row">
            <div class="col-xl-1 col-lg-12 col-md-12"></div>
            <div class="col-xl-5 col-lg-6 col-md-6 pr-md-0">

            </div>
            <div class="col-xl-5 col-lg-6 col-md-6">

            </div>
            <div class="col-xl-1 col-lg-12 col-md-12"></div>
        </div>
        <div class="row">
            <div class="col-xl-1 col-lg-12 col-md-12"></div>
            <div class="col-xl-5 col-lg-6 col-md-6 pr-md-0">

            </div>
            <div class="col-xl-5 col-lg-6 col-md-6 pr-md-2">

            </div>
            <div class="col-xl-1 col-lg-12 col-md-12"></div>
        </div>
        <div class="row">
            <div class="col-xl-1 col-lg-12 col-md-12">
            </div>
            <div class="col-xl-5 col-lg-6 col-md-6 pr-md-0">

            </div>
            <div class="col-xl-5 col-lg-6 col-md-6 pr-md-1">


            </div>
            <div class="col-xl-1 col-lg-12 col-md-12"></div>
        </div>
    </div>
    <!-- container-fluid -->
</div>
<div class="modal fade" id="smaple-document" style="overflow:hidden">
    <div class="modal-dialog modal-dialog-centered max-width-700">
        <div class="modal-content bg-transparent max-width-0">
            <img src="<?= base_url('assets/user_panel/images/banner/document_demo.jpg?v=' . rand()) ?>" class="ml-1 border-grey modal-banner" alt="">
        </div>
    </div>
</div>
<?php $this->load->view('trading/common/footer'); ?>
<script>
    $(document).ready(function() {
        $("#fname").focusin(function() {
            $(this).removeClass("error1")
        });
        $("#lname").focusin(function() {
            $(this).removeClass("error1")
        });
        $("#idnumber").focusin(function() {
            $(this).removeClass("error1")
        });
        $("#idproof").focusin(function() {
            $(this).removeClass("error1")
        });
        $("#file").focusin(function() {
            $(this).removeClass("error1")
        });
        var val = {
            errorPlacement: function(error, element) {
                // name attrib of the field
                var n = element.attr("name");
                if (n == "fname")
                    element.attr("placeholder", "First name");
                else if (n == "lname")
                    element.attr("placeholder", "Last name");
                else if (n == "idnumber")
                    element.attr("placeholder", "Id number");
            },
            rules: {
                fname: "required",
                lname: "required",
                idnumber: "required",
                idproof: {
                    required: true
                },
                file: {
                    required: true,
                    // extension: "jpg|png|gif"
                },
            },
            highlight: function(element) {
                $(element).addClass('error1');
            },
            unhighlight: function(element) {
                $(element).removeClass('error1');
            },
            messages: {}
        }
        $("#form2").multiStepForm({
            // defaultStep:0,
            beforeSubmit: function(form, submit) {
                console.log("called before submiting the form");
                console.log(form);
                console.log(submit);
            },
            validations: val,
        }).navigateTo(0);

        $("#change_password_form").validate({
            rules: {
                new_password: {
                    required: true,
                    minlength: 6
                },
            },
        });
    });
    $(document).ready(function() {
        $("#apikey").focusin(function() {
            $(this).removeClass("error1")
        });
        $("#securitykey").focusin(function() {
            $(this).removeClass("error1")
        });

        var val = {
            errorPlacement: function(error, element) {
                // name attrib of the field
                var n = element.attr("name");
                if (n == "apikey")
                    element.attr("placeholder", "Api Key");
                else if (n == "securitykey")
                    element.attr("placeholder", "Security Key");

            },
            rules: {
                apikey: "required",
                securitykey: "required",

            },
            highlight: function(element) {
                $(element).addClass('error1');
            },
            unhighlight: function(element) {
                $(element).removeClass('error1');
            },
            messages: {}
        }
        $("#form3").multiStepForm({
            // defaultStep:0,
            beforeSubmit: function(form, submit) {
                console.log("called before submiting the form");
                console.log(form);
                console.log(submit);
            },
            validations: val,
        }).navigateTo(0);
        $("#form4").multiStepForm({
            // defaultStep:0,
            beforeSubmit: function(form, submit) {
                console.log("called before submiting the form");
                console.log(form);
                console.log(submit);
            },
            validations: val,
        }).navigateTo(0);
    });
    function submit_mail() {
        Swal.fire({
            title: "Are you sure?",
            text: "",
            type: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#34c38f",
            cancelButtonColor: "#f46a6a",
            confirmButtonText: "Yes",
            confirmButtonClass: "btn btn-success mt-2",
            cancelButtonClass: "btn btn-danger ml-2 mt-2",
            buttonsStyling: !1,
        }).then(function(t) {
            if (t.value == true) {
                var url = '<?php echo base_url(); ?>trade/account/send_mail';
                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: "json",
                    success: function(response) {
                        window.location.reload(true);
                    }
                });
            }
        });
    }
</script>