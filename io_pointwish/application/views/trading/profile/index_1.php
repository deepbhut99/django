<?php $this->load->view('trading/common/header');
?>

<div class="page-content" ng-controller="Profile_Controller" ng-cloak>
    <div class="container-fluid p-0">
        <!-- start page title -->

        <!-- end page title -->

        <div class="row mt-14px">
            <div class="col-xl-1 col-lg-12 col-md-12">
            </div>
            <div class="col-xl-5 col-lg-6 col-md-6 pr-md-0">
                <div class="card mb-14px h-md-225px">
                    <div class="card-body">
                        <h3><?= lang('verification') ?></h3>
                        <?php if (empty($id_data) || (!empty($id_data) && $id_data->status == 2)) { ?>
                            <div class="my-4">
                                <form data-single="true" id="form2" name="form2" action="<?php echo base_url(); ?>trade/account/upload_document" method="POST" enctype="multipart/form-data" class="dropzone">
                                    <div class="fallback">
                                        <input name="file" type="file" multiple="multiple">
                                    </div>
                                </form>
                            </div>
                        <?php } else if (!empty($id_data) && ($id_data->status == 0 || $id_data->status == 1)) { ?>
                            <?php
                            if (!empty($id_data) && ($id_data->status == 0)) {
                            ?>
                                <p class="text-center mt-md-5"><span class="fa-2x font-weight-bold text-warning">Pending</span></p>
                            <?php
                            } else if (!empty($id_data) && ($id_data->status == 1)) {
                            ?>
                                <p class="text-center mt-md-5"><span class="fa-2x font-weight-bold text-warning">Approved</span></p>
                            <?php
                            } else if (!empty($id_data) && ($id_data->status == 2)) {
                            ?>
                                <p class="text-center mt-md-5"><span class="fa-2x font-weight-bold text-warning">Rejected</span></p>
                            <?php
                            }
                            ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="col-xl-5 col-lg-6 col-md-6">
                <div class="card mb-14px">
                    <div class="card-body">
                        <div>
                            <h3><?= lang('eth_usdt_pwt_address') ?></h3>
                            <div style="margin-top: -5px;">
                                <small style="color: #0b65c6;">*<?= lang('you_must_enable') ?></small> <br>
                            </div>
                        </div>
                        <div class="row">

                            <div class="card-body">

                                <form class="custom-validation" method="POST">
                                    <div class="form-group">
                                        <input type="text" ng-model="eth_address" ng-disabled="address.eth_address" id="eth_address" name="eth_address" class="form-control required" required="" placeholder="ETH Address">
                                    </div>
                                    <div class="form-group mb-2">
                                        <div>
                                            <button ng-hide="address.eth_address" ng-click="set_eth_address()" type="submit" class="btn btn-primary waves-effect waves-light mr-1" >
                                                <?= lang('submit') ?>
                                            </button>
                                            <button ng-show="address.eth_address" ng-click="reset_eth_address()" type="submit" class="btn btn-secondary waves-effect waves-light mr-1" >
                                                Reset
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-1">
            </div>
        </div>
        <div class="row">
            <div class="col-xl-1 col-lg-12 col-md-12">
            </div>
            <div class="col-xl-5 col-lg-6 col-md-6 pr-md-0">
                <div class="row">
                    <div class="col-md-6 pr-md-0">
                        <div class="card mb-14px">
                            <div class="card-body">
                                <div>
                                    <h3>Twitter</h3>
                                    <div style="margin-top: -5px;">
                                        <small style="color: #0b65c6;">*Please enter correct username</small> <br>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="card-body px-3">
                                        <form class="custom-validation" id="twitter_form" method="post" enctype="multipart/form-data">
                                            <div class="form-group">
                                                <div class="d-flex">
                                                    <input type="text" name="tusername" id="tusername" class="form-control mr-2 required" ng-disabled="<?= $user_data->twitter_username != '' ?>" value="<?= $user_data->twitter_username ?>" placeholder="Twitter Username" required>
                                                    <div>
                                                        <span ng-show="<?= $user_data->twitter_status == '0' ?>" class="btn btn-primary py-1 font-size-18 btn-twitter" id="twitter-pin">
                                                            <i class="fa fa-paperclip attch-twitter" aria-hidden="true"></i>
                                                        </span>
                                                        <span ng-show="<?= $user_data->twitter_status == '2' || $user_data->twitter_status == '1' ?>" class="btn btn-primary py-1 font-size-18">
                                                            <i class="fa fa-check-square-o" aria-hidden="true"></i>
                                                        </span>
                                                        <input type="file" name="file_twitter" id="file_twitter" class="d-none required" accept="image/png, image/jpg, image/jpeg" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group mb-0">
                                                <div>
                                                    <button ng-show="<?= $user_data->twitter_status == '0' ?>" type="submit" class="btn btn-primary waves-effect waves-light mr-1 submitBtn">
                                                        <?= lang('submit') ?>
                                                    </button>
                                                    <label ng-show="<?= $user_data->twitter_status == '2' || $user_data->twitter_status == '1' ?>" class="btn waves-effect waves-light mr-1 mb-0 btn-<?= $twitter->status == 0 ? 'warning' : 'success' ?>">
                                                        <?= $twitter->status == 0 ? 'Pending' : 'Approve' ?>
                                                    </label>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card mb-14px">
                            <div class="card-body">
                                <div>
                                    <h3>Telegram</h3>
                                    <div style="margin-top: -5px;">
                                        <small style="color: #0b65c6;">*Please enter correct username</small> <br>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="card-body px-3">
                                        <form class="custom-validation" id="telegram_form" method="post" enctype="multipart/form-data">
                                            <div class="form-group">
                                                <div class="d-flex">
                                                    <input type="text" name="tgusername" id="tgusername" class="form-control mr-2 required" placeholder="Telegram Username" ng-disabled="<?= $user_data->telegram_username != '' ?>" value="<?= $user_data->telegram_username ?>" required>
                                                    <div>
                                                        <span ng-show="<?= $user_data->telegram_status == '0' ?>" class="btn btn-primary py-1 font-size-18 btn-telegram" id="telegram-pin">
                                                            <i class="fa fa-paperclip attch-telegram" aria-hidden="true"></i>
                                                        </span>
                                                        <span ng-show="<?= $user_data->telegram_status == '2' || $user_data->telegram_status == '1' ?>" class="btn btn-primary py-1 font-size-18">
                                                            <i class="fa fa-check-square-o" aria-hidden="true"></i>
                                                        </span>
                                                        <input type="file" name="file_telegram" id="file_telegram" class="d-none" accept="image/png, image/jpg, image/jpeg">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group mb-0">
                                                <div>
                                                    <button ng-show="<?= $user_data->telegram_status == '0' ?>" type="submit" class="btn btn-primary waves-effect waves-light mr-1 btn-tele">
                                                        <?= lang('submit') ?>
                                                    </button>
                                                    <label ng-show="<?= $user_data->telegram_status == '2' || $user_data->telegram_status == '1' ?>" class="btn waves-effect waves-light mr-1 mb-0 btn-<?= $telegram->status == 0 ? 'warning' : 'success' ?>">
                                                        <?= $telegram->status == 0 ? 'Pending' : 'Approve' ?>
                                                    </label>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-5 col-lg-6 col-md-6">
                <div class="card mb-14px">
                    <div class="card-body">
                        <div>
                            <h3><?= lang('btc_address') ?></h3>
                            <div style="margin-top: -5px;">
                                <small style="color: #0b65c6;">*<?= lang('you_must_enable') ?></small> <br>
                            </div>
                        </div>
                        <div class="row">

                            <div class="card-body">

                                <form class="custom-validation" method="POST">
                                    <div class="form-group">
                                        <input ng-model="btc_address" ng-disabled="address.btc_address" type="text" id="btc_address" name="btc_address" class="form-control required" required="" placeholder="BTC Address">
                                    </div>
                                    <div class="form-group mb-0">
                                        <div>
                                            <button ng-hide="address.btc_address" ng-click="set_btc_address()" type="submit" class="btn btn-primary waves-effect waves-light mr-1" >
                                                <?= lang('submit') ?>
                                            </button>
                                            <button ng-show="address.btc_address" ng-click="reset_btc_address()" type="submit" class="btn btn-secondary waves-effect waves-light mr-1" >
                                                Reset
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-1 col-lg-12 col-md-12">
            </div>
        </div>
        <div class="row">
            <div class="col-xl-1 col-lg-12 col-md-12">
            </div>
            <div class="col-xl-5 col-lg-6 col-md-6 pr-md-0">
                <div class="card mb-14px">
                    <div class="card-body">
                        <div>
                            <h3><?= lang('change_password') ?> </h3>
                            <div style="margin-top: -5px;">
                                <small style="color: #0b65c6;">*<?= lang('you_must_enable') ?></small> <br>
                            </div>
                        </div>
                        <div class="row">

                            <div class="card-body pb-2">

                                <form id="change_password_form" name="change_password_form" class="custom-validation" method="POST">
                                    <div class="form-group">
                                        <label><?= lang('current_password') ?></label>
                                        <input type="password" ng-model="old_password" id="old_password" name="old_password" class="form-control required" required="" placeholder="<?= lang('current_password') ?>">
                                    </div>

                                    <div class="form-group">
                                        <label><?= lang('new_password') ?></label>
                                        <input type="password" ng-model="new_password" id="new_password" name="new_password" class="form-control required" required="" placeholder="<?= lang('new_password') ?>">
                                    </div>

                                    <div class="form-group">
                                        <label><?= lang('confirm_password') ?></label>
                                        <input type="password" ng-model="confirm_password" id="confirm_password" name="confirm_password" class="form-control required" required="" placeholder="<?= lang('confirm_password') ?>" equalTo="#new_password">
                                    </div>
                                    <div class="form-group mb-0">
                                        <div>
                                            <button type="submit" ng-click="change_password();" class="btn btn-primary waves-effect waves-light mr-1" >
                                                <?= lang('submit') ?>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-xl-5 col-lg-6 col-md-6">
                <div class="card mb-14px">
                    <div class="card-body">
                        <div>
                            <h3>BNB/pwt Address</h3>
                            <div style="margin-top: -5px;">
                                <small style="color: #0b65c6;">*<?= lang('you_must_enable') ?></small> <br>
                            </div>
                        </div>
                        <div class="row">

                            <div class="card-body pb-0">

                                <form class="custom-validation" method="POST">
                                    <div class="form-group">
                                        <input ng-model="ltcaddress" ng-disabled="address.ltc_address" type="text" id="ltc_address" name="ltc_address" class="form-control required" required placeholder="LTC Address">
                                    </div>
                                    <div class="form-group mb-0">
                                        <div>
                                            <button ng-hide="address.ltc_address" ng-click="set_ltc_address()" type="submit" class="btn btn-primary waves-effect waves-light mr-1" >
                                                <?= lang('submit') ?>
                                            </button>
                                            <button ng-show="address.ltc_address" ng-click="reset_ltc_address()" type="submit" class="btn btn-secondary waves-effect waves-light mr-1" >
                                                Reset
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-1 col-lg-12 col-md-12">
            </div>
        </div>
    </div>
    <!-- container-fluid -->
</div>
<?php $this->load->view('trading/common/footer'); ?>
<script>
    $("#change_password_form").validate({
        rules: {
            new_password: {
                required: true,
                minlength: 6
            },
        },

    });
</script>

<script>
    $(document).ready(function() {
        $("#twitter_form").validate();
        $("#telegram_form").validate();
        $('#file_twitter').on('change', function() {
            $('.attch-twitter').removeClass('fa-paperclip');
            $('.btn-twitter').removeClass('py-1');
            $('.attch-twitter').addClass('fa-check-square-o');
            $('.btn-twitter').addClass('pt-6px');
            $('.btn-twitter').addClass('pb-3px');
        });
        $('#file_telegram').on('change', function() {
            $('.attch-telegram').removeClass('fa-paperclip');
            $('.btn-telegram').removeClass('py-1');
            $('.attch-telegram').addClass('fa-check-square-o');
            $('.btn-telegram').addClass('pt-6px');
            $('.btn-telegram').addClass('pb-3px');
        });

        $("#twitter_form").on('submit', function(e) {
            e.preventDefault();
            var form = $("#twitter_form");
            if (form.valid() == false) {
                return;
            } else {
                var files = $('#file_twitter').val();
                if (files) {
                    $.ajax({
                        type: 'POST',
                        url: '<?php echo base_url(); ?>trade/account/upload_twitter_account',
                        data: new FormData(this),
                        dataType: 'json',
                        contentType: false,
                        cache: false,
                        processData: false,
                        beforeSend: function() {
                            $('.submitBtn').attr("disabled", "disabled");
                        },
                        success: function(response) { //console.log(response);
                            if (response.success == true) {
                                window.location.reload(true);
                            } else {
                                $('.submitBtn').attr("disabled", false)
                                toastr.error(response.message);
                            }
                        }
                    });
                } else {
                    toastr.error('Please attech file');
                }
            }
        });

        $("#telegram_form").on('submit', function(e) {
            e.preventDefault();
            var form = $("#telegram_form");
            if (form.valid() == false) {
                return;
            } else {
                var files = $('#file_telegram').val();
                if (files) {
                    $.ajax({
                        type: 'POST',
                        url: '<?php echo base_url(); ?>trade/account/upload_telegram_account',
                        data: new FormData(this),
                        dataType: 'json',
                        contentType: false,
                        cache: false,
                        processData: false,
                        beforeSend: function() {
                            $('.btn-tele').attr("disabled", "disabled");
                        },
                        success: function(response) { //console.log(response);
                            if (response.success == true) {
                                window.location.reload(true);
                            } else {
                                $('.btn-tele').attr("disabled", "");
                                toastr.error(response.message);
                            }
                        }
                    });
                } else {
                    toastr.error('Please attech file');
                }
            }
        });
    });
    // ======= TWITTER =======
    $('#twitter-pin').on('click', function() {
        $('#file_twitter').trigger('click');
    });
    // ======= TELEGRAM =======
    $('#telegram-pin').on('click', function() {
        $('#file_telegram').trigger('click');
    });
</script>