<?php $this->load->view('trading/common/header'); ?>
<div class="page-content" ng-controller="Profile_Controller" ng-cloak>
    <div class="container-fluid p-0 mt-12px">
        <div class="card mb-14px">
            <div class="card-body">
                <h3 data-toggle="modal" data-target="#smaple-document" class="cursor-pointer"><?= lang('verification') ?></h3>
                <!-- <div style="margin-top: -5px;">
                    <small class="cursor-pointer text-46f2d4" role="button" data-toggle="modal" data-target="#smaple-document">*View sample document</small> <br>
                </div> -->
                <?php if (empty($id_data) || (!empty($id_data) && $id_data->status == 3)) { ?>
                    <form data-single="true" id="form2" name="form2" action="<?php echo base_url('account/upload_document'); ?>" method="POST" enctype="multipart/form-data" class="px-1">
                        <div class="tab my-1">
                            <div class="row pt-1">
                                <div class="col-md-4 pr-md-0">
                                    <input class="form-control required" name="fname" id="fname" placeholder="First name" required>
                                </div>
                                <div class="col-md-4 pr-md-0 mt-validate-3">
                                    <input class="form-control required" name="lname" id="lname" placeholder="Last name" required>
                                </div>
                                <div class="col-md-4 mt-validate-3">
                                    <input class="form-control required" name="idnumber" id="idnumber" placeholder="Id Number" required>
                                </div>
                            </div>
                        </div>
                        <div class="tab">
                            <span>Upload ID Proof</span>
                            <div class="fallback mt-1" id="idproof">
                                <input class="dropzone w-100" name="idproof" type="file">
                            </div>
                        </div>
                        <div class="tab">
                            <span class="pwt-price-web">Upload Sample Document</span>
                            <img data-toggle="modal" data-target="#smaple-document" class="float-sm-right my-xl-n5 w-115 cursor-pointer" src="<?= base_url('assets/user_panel/images/banner/sample.svg') ?>" alt="">
                            <div class="fallback mt-1" id="file">
                                <input class="dropzone w-100" name="file" type="file">
                            </div>
                        </div>
                        <div class="mt-12px text-center">
                            <button type="button" class="previous btn btn-dark waves-effect waves-light">Previous</button>
                            <button type="button" class="next btn btn-dark waves-effect waves-light">Next</button>
                            <button type="button" class="submit btn btn-dark waves-effect waves-light">Submit</button>
                        </div>
                        <!-- Circles which indicates the steps of the form: -->
                        <div class="d-none">
                            <span class="step">1</span>
                            <span class="step">2</span>
                            <span class="step">3</span>
                        </div>
                    </form>
                <?php } else if (!empty($id_data) && ($id_data->status == 0 || $id_data->status == 1 || $id_data->status == 2)) { ?>
                    <?php
                    if (!empty($id_data) && ($id_data->status == 0)) {
                    ?>
                        <p class="text-center mt-lg-4"><span class="fa-2x font-weight-bold text-warning"><?= lang('pending') ?></span></p>
                    <?php
                    } else if (!empty($id_data) && ($id_data->status == 1)) {
                    ?>
                        <p class="text-center mt-lg-4"><span class="fa-2x font-weight-bold text-02C076"><?= lang('approve') ?></span></p>
                    <?php
                    } else if (!empty($id_data) && ($id_data->status == 2)) {
                    ?>
                        <p class="text-center mb-0"><span class="fa-2x font-weight-bold text-danger"><?= lang('reject') ?></span></p>
                        <p class="text-center mb-2">(Reason: <?= $id_data->reject_reason ?>)</p class="text-center">
                        <p class="text-center"><button type="button" onclick="location.href='<?= base_url('reupload/' . $id_data->id) ?>'" class="submit btn btn-dark waves-effect waves-light">Click here to upload again</button></p>
                    <?php
                    }
                    ?>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('trading/common/footer'); ?>
<script>
    $(document).ready(function() {
        $("#fname").focusin(function() {
            $(this).removeClass("error1")
        });
        $("#lname").focusin(function() {
            $(this).removeClass("error1")
        });
        $("#idnumber").focusin(function() {
            $(this).removeClass("error1")
        });
        $("#idproof").focusin(function() {
            $(this).removeClass("error1")
        });
        $("#file").focusin(function() {
            $(this).removeClass("error1")
        });
        var val = {
            errorPlacement: function(error, element) {
                // name attrib of the field
                var n = element.attr("name");
                if (n == "fname")
                    element.attr("placeholder", "First name");
                else if (n == "lname")
                    element.attr("placeholder", "Last name");
                else if (n == "idnumber")
                    element.attr("placeholder", "Id number");
            },
            rules: {
                fname: "required",
                lname: "required",
                idnumber: "required",
                idproof: {
                    required: true
                },
                file: {
                    required: true,
                    // extension: "jpg|png|gif"
                },
            },
            highlight: function(element) {
                $(element).addClass('error1');
            },
            unhighlight: function(element) {
                $(element).removeClass('error1');
            },
            messages: {}
        }
        $("#form2").multiStepForm({
            // defaultStep:0,
            beforeSubmit: function(form, submit) {
                console.log("called before submiting the form");
                console.log(form);
                console.log(submit);
            },
            validations: val,
        }).navigateTo(0);

        $("#change_password_form").validate({
            rules: {
                new_password: {
                    required: true,
                    minlength: 6
                },
            },
        });
    });
</script>