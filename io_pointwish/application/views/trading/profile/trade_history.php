<?php $this->load->view('trading/common/header'); ?>
<div class="page-content">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row align-items-center">
            <div class="col-sm-6">
                <div class="page-title-box">
                    <!-- <h4 class="font-size-18">History</h4> -->
                </div>
            </div>

            <div class="col-sm-6">
                <div class="float-right d-none d-md-block">

                </div>
            </div>
        </div>
        <!-- end page title -->


        <div class="row">
            <div class="col-1"></div>
            <div class="col-10">
                <div class="card" style="border: 1px solid rgba(255, 255, 255, 0.3);">
                    <div class="card-body">

                        <h4 class="card-title">Trade History</h4><br>

                        <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <tr>
                                    <th>Sr. No</th>
                                    <th>Currency</th>
                                    <th>Trade Time</th>
                                    <th>Bid Type</th>
                                    <th>Base Price</th>
                                    <th>Closed Price </th>
                                    <th>Trade Amount</th>
                                    <th>Result</th>
                                    <th>Earning</th>
                                </tr>
                            </thead>

                            <tbody>
                                <td>01</td>
                                <td>USDT</td>
                                <td>1 : 30</td>
                                <td>Bid Type</td>
                                <td>$50</td>
                                <td>$20</td>
                                <td>$400</td>
                                <td>WIN</td>
                                <td>$210</td>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-1"></div>
            <!-- end col -->
        </div><!-- end row -->



    </div> <!-- container-fluid -->
</div>
<?php $this->load->view('trading/common/footer'); ?>