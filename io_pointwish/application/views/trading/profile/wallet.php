<?php $this->load->view('trading/common/header'); ?>
<!-- Wallet Page Css -->
<link href="<?= base_url('assets/user_panel/css/wallet.css') ?>" id="app-style" rel="stylesheet" type="text/css" />
<div ng-controller="Wallet_Controller">
    <div class="page-content">
        <div class="container-fluid p-0">
            <section class="mt-14px">
                <div class="container p-0">
                    <div class="row">
                        <div class="col-xl-8 col-lg-6 col-md-12 col-sm-12 col-xsm-12">
                            <div class="row">
                                <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-xsm-12 pr-lg-0">
                                    <div class="crumina-module  pricing-table--small mb-14px p-lg-4 p-3">
                                        <div class="d-flex justify-content-between">
                                            <div class="pricing-thumb">
                                                <img height="80px" src="<?= base_url('assets/user_panel/images/icon/usdt.svg') ?>" class="woox-icon" alt="dash">
                                                <h5 class="pricing-title">USDT <small class="font-size-14">(TRC20)</small><br><span><?= number_format($user_data->wallet_amount, 2) ?></span></h5>
                                            </div>
                                            <div class="mt-11px mt-sm-25px icon-wallet-dw">
                                                <div class="text-center waves-effect mr-sm-4" title="Deposit" ng-click="show_deposit('USDTTRC20')">
                                                    <img class="text-center" height="30px" src="<?= base_url('assets/user_panel/images/wallet_background/deposit.svg') ?>" alt="">
                                                </div>
                                                <div class="text-center waves-effect" title="Withdrwal" ng-click="show_withdrawal('USDTTRC20')">
                                                    <img class="text-center" height="30px" src="<?= base_url('assets/user_panel/images/wallet_background/withdrawal.svg') ?>" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-xsm-12 pr-lg-0">
                                    <div class="crumina-module  pricing-table--small p-lg-4 p-3 mb-14px">
                                        <div class="d-flex justify-content-between">
                                            <div class="pricing-thumb">
                                                <img height="80px" src="<?= base_url('assets/user_panel/images/icon/eth.png') ?>" class="woox-icon" alt="ethereum">
                                                <h5 class="pricing-title"><?= lang('ethereum') ?> <br><span>ETH</span></h5>
                                            </div>
                                            <div class="mt-11px mt-sm-25px icon-wallet-dw">
                                                <div class="text-center waves-effect" title="Deposit" ng-click="show_deposit('ETH')">
                                                    <img class="text-center" height="30px" src="<?= base_url('assets/user_panel/images/wallet_background/deposit.svg') ?>" alt="">
                                                </div>
                                                <!-- <div class="text-center waves-effect" title="Withdrwal" ng-click="show_withdrawal('ETH')">
                                                    <img class="text-center" height="30px" src="<?= base_url('assets/user_panel/images/wallet_background/withdrawal.svg') ?>" alt="">
                                                </div> -->
                                            </div>
                                        </div>
                                        <!--<div class="text-center waves-effect" title="Transfer" ng-click="show_transfer('ETH')">-->
                                        <!--    <img class="text-center" height="30px" src="<?= base_url('assets/user_panel/images/wallet_background/exchange.svg') ?>" alt="">-->
                                        <!--</div>-->
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-xsm-12 pr-lg-0">
                                    <div class="crumina-module  pricing-table--small mb-14px p-lg-4 p-3">
                                        <div class="d-flex justify-content-between">
                                            <div class="pricing-thumb">
                                                <img height="80px" src="<?= base_url('assets/user_panel/images/icon/bnb1.png') ?>" class="woox-icon" alt="dash">
                                                <h5 class="pricing-title">Binance<br><span>BNB</span></h5>
                                            </div>
                                            <div class="mt-11px mt-sm-25px icon-wallet-dw">
                                                <div class="text-center waves-effect" title="Deposit" ng-click="show_deposit('BNB')">
                                                    <img class="text-center" height="30px" src="<?= base_url('assets/user_panel/images/wallet_background/deposit.svg') ?>" alt="">
                                                </div>
                                                <!-- <div class="text-center waves-effect" title="Withdrwal" ng-click="show_withdrawal('BNB')">
                                                    <img class="text-center" height="30px" src="<?= base_url('assets/user_panel/images/wallet_background/withdrawal.svg') ?>" alt="">
                                                </div> -->
                                            </div>
                                        </div>
                                        <!--<div class="text-center waves-effect" title="Transfer" ng-click="show_transfer('LTC')">-->
                                        <!--    <img class="text-center" height="30px" src="<?= base_url('assets/user_panel/images/wallet_background/exchange.svg') ?>" alt="">-->
                                        <!--</div>-->
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-xsm-12 pr-lg-0">
                                    <div class="crumina-module  pricing-table--small mb-14px p-lg-4 p-3">
                                        <div class="d-flex justify-content-between">
                                            <div class="pricing-thumb">
                                                <img height="80px" src="<?= base_url('assets/user_panel/images/icon/xbot.svg') ?>" class="woox-icon" alt="litecoin">
                                                <h5 class="pricing-title">XBOT<br>
                                                    <span>Coming soon</span>
                                                    <!-- <span><?= number_format($user_data->unlocked_pwt, 2) ?> XBOT</span> -->
                                                </h5>
                                            </div>
                                            <!-- <div class="mt-11px mt-sm-25px icon-wallet-dw">
                                                <div class="text-center waves-effect mr-sm-4" title="Deposit" ng-click="show_deposit('pwt')">
                                                    <img class="text-center" height="30px" src="<?= base_url('assets/user_panel/images/wallet_background/deposit.svg') ?>" alt="">
                                                </div>
                                                <div class="text-center waves-effect" title="Withdrwal" ng-click="show_withdrawal('pwt')">
                                                    <img class="text-center" height="30px" src="<?= base_url('assets/user_panel/images/wallet_background/withdrawal.svg') ?>" alt="">
                                                </div>
                                            </div> -->
                                        </div>
                                        <!--<div class="text-center waves-effect" title="Transfer">-->
                                        <!--    <img class="text-center" height="30px" src="<?= base_url('assets/user_panel/images/wallet_background/exchange.svg') ?>" alt="">-->
                                        <!--</div>-->
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-xsm-12 pr-lg-0">
                                    <div class="crumina-module  pricing-table--small mb-14px p-lg-4 p-3">
                                        <div class="d-flex justify-content-between">
                                            <div class="pricing-thumb">
                                                <img height="80px" src="<?= base_url('assets/user_panel/images/icon/btc.png') ?>" class="woox-icon" alt="bitcoin">
                                                <h5 class="pricing-title"><?= lang('bitcoin') ?> <br><span>BTC</span></h5>
                                            </div>
                                            <div class="mt-11px mt-sm-25px icon-wallet-dw">
                                                <div class="text-center waves-effect" title="Deposit" ng-click="show_deposit('BTC')">
                                                    <img class="text-center" height="30px" src="<?= base_url('assets/user_panel/images/wallet_background/deposit.svg') ?>" alt="">
                                                </div>
                                                <!-- <div class="text-center waves-effect" title="Withdrwal" ng-click="show_withdrawal('BTC')">
                                                    <img class="text-center" height="30px" src="<?= base_url('assets/user_panel/images/wallet_background/withdrawal.svg') ?>" alt="">
                                                </div> -->
                                            </div>
                                        </div>
                                        <!--<div class="text-center waves-effect" title="Transfer" ng-click="show_transfer('BTC')">-->
                                        <!--    <img class="text-center" height="30px" src="<?= base_url('assets/user_panel/images/wallet_background/exchange.svg') ?>" alt="">-->
                                        <!--</div>-->
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-xsm-12 pr-lg-0">
                                    <div class="crumina-module  pricing-table--small mb-14px p-lg-4 p-3">
                                        <div class="d-flex justify-content-between">
                                            <div class="pricing-thumb">
                                                <img height="80px" src="<?= base_url('assets/user_panel/images/icon/usdt.svg') ?>" class="woox-icon" alt="litecoin">
                                                <h5 class="pricing-title">USDT <small class="font-size-14">(ERC20)</small> <br><span><?= number_format($user_data->wallet_amount, 2) ?></span></h5>
                                            </div>
                                            <div class="mt-11px mt-sm-25px icon-wallet-dw">
                                                <div class="text-center waves-effect" title="Deposit" ng-click="show_deposit('USDTERC20')">
                                                    <img class="text-center" height="30px" src="<?= base_url('assets/user_panel/images/wallet_background/deposit.svg') ?>" alt="">
                                                </div>
                                                <!-- <div class="text-center waves-effect" title="Withdrwal" ng-click="show_withdrawal('USDTERC20')">
                                                    <img class="text-center" height="30px" src="<?= base_url('assets/user_panel/images/wallet_background/withdrawal.svg') ?>" alt="">
                                                </div> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6 col-md-12 col-sm-12 col-xsm-12 pr-lg-0">
                            <div class="theme-border min-h-323 mb-activity-12px">
                                <div class="crumina-module pricing-table--small mb-0 border-bottom-radius-0 activity-card">
                                    <div class="card-body pb-0">
                                        <h4 class="pb-15px border-bottom-1"><?= lang('activity') ?></h4>
                                    </div>
                                </div>
                                <div class="crumina-module pricing-table--small pl-2em mb-14px pt-3 pb-0 min-h-325 activity-card border-top-radius-0">
                                    <div class="card-body p-0">
                                        <ol class="activity-feed">
                                            <?php
                                            if ($history) {
                                                foreach ($history as $row) {
                                            ?>
                                                    <li class="feed-item <?= $row['type'] == 'transfer' ? 'ftransfer' : ($row['type'] == 'fund' ? 'fdeposit' : 'fwithdraw') ?>">
                                                        <div class="feed-item-list">
                                                            <span class="date text-gry"><?= date('d M Y H:i', strtotime($row['created_datetime'])); ?></span>
                                                            <span class="activity-text"><span class="text-ylw">$ <?= number_format($row['amount'], 2) ?></span></span>
                                                        </div>
                                                    </li>
                                                <?php }
                                            } else { ?>
                                                <li class="feed-item text-center">
                                                    <div class="feed-item-list">
                                                        <span class="date text-gry"></span>
                                                        <span class="activity-text ml-n5"><?= lang('no_record_found') ?></span>
                                                    </div>
                                                </li>
                                            <?php } ?>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xsm-12 pr-lg-0">
                            <div class="crumina-module  pricing-table--small mb-14px p-lg-4 p-3">
                                <div class="row">
                                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xsm-12 border-lg-right">
                                        <div class="text-center border-xsm-bottom">
                                            <h3 class="mb-1"><?= lang('total_pwt') ?></h3>
                                            <h6 class="text-666d7a font-size-20 mb-xsm-3"><?= number_format($user_data->pwt_wallet, 2) ?></h6>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xsm-12">
                                        <div class="text-center mt-xsm-3">
                                            <h3 class="mb-1"><?= lang('total_unlocked_pwt') ?></h3>
                                            <h6 class="text-666d7a font-size-20"><?= number_format($user_data->unlocked_pwt, 2) ?></h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xsm-12 pr-lg-0">
                            <div class="crumina-module  pricing-table--small mb-14px p-lg-4 p-3">
                                <div class="row">
                                    <div class="col-12">
                                        <table class="table text-center table-borderless table-sm-responsive">
                                            <thead>
                                                <tr class="border-bottom-1">
                                                    <th class="w-33.33 font-size-18 text-uppercase"><?= lang('Date') ?></th>
                                                    <th class="w-33.33 font-size-18 text-uppercase"><?= lang('locked_pwt') ?></th>
                                                    <th class="w-33.33 font-size-18 text-uppercase"><?= lang('unlocked_pwt') ?></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $total = count($unlocks);
                                                $pwt = $user_data->pwt_wallet / $total;
                                                foreach ($unlocks as $row) {
                                                ?>
                                                    <tr class="wallet-table">
                                                        <td><?= date('d M, Y', strtotime($row->date))  ?></td>
                                                        <td><?= number_format($pwt, 2) ?></td>
                                                        <td><?= $row->status == 1 ? number_format($pwt, 2) : 0 ?></td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> -->
                </div>
            </section>
        </div>
    </div>
    <div class="modal fade deposit" id="modal_deposit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content bg-transparent border-0">
                <div class="modal-body px-2">
                    <div class="row" style="padding-left: 10px; padding-right:10px;">
                        <div class="card" style="width:100%;">
                            <div class="card-body">
                                <button type="button" class="close text-d9d9d9" ng-click="hide_modal()" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="card-title text-left" style="border-bottom: 1px solid #343b51; padding-bottom:20px;">{{type}}&nbsp;&nbsp;<small class="text-danger font-weight-bold">(min. $<?php echo ("{{minDeposit}}"); ?>)</small></h4>
                                <form id="data_form" name="data_form" method="POST">
                                    <div class="row">
                                        <div class="col-md-10 pr-0">
                                            <label class="font-14 bold mb-2"><?= lang('deposit') ?></label>
                                            <input required type="number" min="{{minDeposit}}" class="theme-input-style form-control required" ng-model="amountf" id="amount" name="amountf" placeholder="Enter {{wallettype}} Amount">
                                            <input type="hidden" id="token" name="token" value="<?= $user_data->id ?>">
                                            <input type="hidden" name="payment_type" id="payment_type" value="{{payment_type}}">
                                        </div>
                                        <div class="col-md-2" style="margin-top: 8px;" ng-disabled="amountf < minDeposit">
                                            <!-- <label class="font-14 bold mb-2">Deposit <small class="bold">(USD)</small></label> -->
                                            <button id="submit_button_id" type="submit" class="btn btn-primary mt-1-3rem waves-effect waves-light mr-1"><?= lang('pay') ?></button>
                                        </div>
                                    </div>
                                </form>
                                <div id="payment_details" class="d-none">
                                    <div class="text-center">
                                        <h4 style="text-align: center;"> <strong style="color: #31BAA0;"></strong> <?= lang('deposit_address') ?></h4>
                                    </div>
                                    <br>
                                    <div class="text-center">
                                        <h4 style="text-align: center;"><span id="amountc"></span> <span id="currency"></span></h4>
                                    </div>
                                    <div class="justify-content-center" style="display: flex;">
                                        <div id="qrcode" class="bg-white waves-button-input"></div>
                                    </div>
                                    <br>
                                    <div class="text-center">
                                        <input type="text" class="form-control text-center" id="address" readonly>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-12 mt-2 text-center">
                                            <button id="copy_button_id" onclick="copy_text();" type="button" class="btn btn-primary waves-effect waves-light mr-1"><?= lang('copy') ?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <div class="modal fade Withdraw" id="modal_withdrawal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content bg-transparent border-0">
                <div class="modal-body px-2">
                    <div class="row" style="padding-left: 10px; padding-right:10px;">

                        <div class="card" style="width: 100%;">
                            <div class="ml-auto mr-auto text-center" ng-show="addressLoader">
                                <img src="<?= base_url('assets/user_panel/images/preloader.gif') ?>" alt="loader">
                            </div>
                            <div class="card-body" ng-show="!addressLoader">
                                <button type="button" class="close text-d9d9d9" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="card-title text-left" style="border-bottom: 1px solid #343b51; padding-bottom:20px;"><?= lang('withdrawal') ?> <small class="text-danger font-weight-bold">( Fee {{ withdraw_in == 'usdt' ? '$'+fee : fee + ' pwt'   }} )</small></h4>
                                <form name="withdraw_form" id="withdraw_form" class="custom-validation" novalidate="" method="POST">
                                    <div class="form-group">
                                        <label><?= lang('amount') ?>
                                            <small class="text-danger font-weight-bold">{{minamount ? '(min '+minamount+')' : '' }}</small>
                                        </label>
                                        <small class="float-right" ng-show="withdraw_in == 'usdt'">(Wallet: $<?= number_format($user_data->wallet_amount, 2) ?>)</small>
                                        <small class="float-right" ng-show="withdraw_in == 'pwt'">(Wallet: <?= number_format($user_data->unlocked_pwt, 2) ?> USDT)</small>
                                        <div>
                                            <input ng-model="withdraw_amount" name="withdraw_amount" id="withdraw_amount" type="number" min="{{minamount}}" class="form-control required" required="" data-parsley-minlength="6" placeholder="<?= lang('enter_amount') ?>">
                                            <input type="hidden" name="withdraw_type" id="withdraw_type" value="{{type}}">
                                            <input type="hidden" name="withdraw_fee" id="withdraw_fee" value="{{fee}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label><?= lang('receiving_address') ?></label>
                                        <div>
                                            <input name="withdraw_address" id="withdraw_address" readonly="" type="text" class="form-control required" required="" data-parsley-maxlength="6" ng-model="address" placeholder="<?= lang('enter_rcvng_address') ?>">
                                        </div>
                                    </div>
                                    <div class="text-white" ng-show="withdraw_amount">
                                        <label><?= lang('you_will_get') ?> {{ withdraw_in == 'usdt' ? '$' : 'pwt '   }}{{withdraw_amount - fee > 0 ? withdraw_amount - fee : 0}}</label>
                                    </div>
                                    <div class="form-group mb-0">
                                        <div>
                                            <button type="submit" ng-disabled="withdraw_amount - fee < 0" ng-click="make_withdrawal(withdraw_form)" class="btn btn-primary waves-effect waves-light mr-1">
                                                <?= lang('submit') ?>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="model_transfer" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content bg-transparent border-0">
                <div class="modal-body">
                    <div class="row" style="padding-left: 10px; padding-right:10px;">
                        <div class="card" style="width: 100%;">
                            <div class="card-body">
                                <button type="button" class="close text-d9d9d9" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="card-title text-left" style="border-bottom: 1px solid #343b51; padding-bottom:20px;"><?= lang('transfer') ?></h4>
                                <form class="custom-validation" id="transfer_form" name="transfer_form" action="#" novalidate="">
                                    <div class="form-group">
                                        <label><?= lang('user_name') ?></label>
                                        <div>
                                            <input ng-model="ts_member" type="text" id="member_id" name="member_id" class="form-control required" required="" placeholder="<?= lang('enter_username') ?>">
                                            <span ng-model="member_section_name" id="member_section_name"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label><?= lang('earnfinex_amount') ?>
                                            <small class="text-danger font-weight-bold">(min {{mintransfer}})</small>
                                        </label>
                                        <small class="float-right">(Wallet: {{transfer_type == 'pwt' ? <?= $user_data->pwt_wallet ?> + ' pwt' : '$ ' +<?= $user_data->wallet_amount ?>}})</small>
                                        <div>
                                            <input ng-model="ts_amount" min="{{mintransfer}}" type="number" id="ts_amount" name="ts_amount" class="form-control required" required="" placeholder="<?= lang('enter_amount') ?>">
                                            <input type="hidden" name="transfer_type" id="transfer_type" value="{{transfer_type}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label><?= lang('memo') ?> <small>(<?= lang('optional') ?>)</small></label>
                                        <div>
                                            <input type="text" class="form-control" required="" placeholder="<?= lang('enter_msg') ?>">
                                        </div>
                                    </div>
                                    <div class="form-group mb-0">
                                        <div>
                                            <button type="button" onclick="transfer_submit();" class="btn btn-primary waves-effect waves-light mr-1">
                                                <?= lang('submit') ?>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="android-modal" style="overflow:hidden">
        <div class="modal-dialog modal-dialog-centered max-width-700">
            <div class="modal-content bg-transparent max-width-0">
                <img src="<?= base_url('assets/user_panel/images/banner/72.jpg') ?>" class="ml-1 border-grey modal-banner" alt="">
            </div>
        </div>
    </div>
    <div class="modal fade" id="otp_model" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content bg-transparent border-0">
                <div class="modal-body">
                    <div class="row" style="padding-left: 10px; padding-right:10px;">
                        <div class="card" style="width: 100%;">
                            <div class="card-body">
                                <button type="button" class="close text-d9d9d9" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="card-title text-left" style="border-bottom: 1px solid #343b51; padding-bottom:20px;">Authentication</h4>
                                <form class="custom-validation" id="otp_form" name="otp_form">
                                    <div class="form-group text-center">
                                        <label>Enter 6-digit code from your email</label>
                                        <div class="justify-content-center ml-auto mr-auto row text-center">
                                            <input type="text" class="input-bordered col-6 text-sm-center fa-22 required form-control text-d9d9d9 mr-2" required maxlengh="6" ng-model="code" placeholder="o-o-o-o-o-o" ui-mask="9-9-9-9-9-9" name="code" id="code">
                                            <button type="submit" ng-click="check_auth()" class="btn btn-primary"><?= lang('submit') ?></button>
                                        </div>
                                    </div>
                                    <!-- <div class="form-group mb-0">
                                        <div>
                                            <button type="button"  ng-click="check_auth()" class="btn btn-primary waves-effect waves-light mr-1">
                                                <?= lang('submit') ?>
                                            </button>
                                        </div>
                                    </div> -->
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->

    </div>
</div>
<?php $this->load->view('trading/common/footer'); ?>
<!-- <script src="https://cdn.jsdelivr.net/npm/davidshimjs-qrcodejs@0.0.2/qrcode.js"></script> -->
<script>
    // $(window).ready(function() {
    //     setTimeout(function() {
    //         $('#android-modal').modal("show")
    //     }, 100)
    // })
</script>
<script type="text/javascript">
    var base = "<?= base_url() ?>";
    $("#data_form").validate();
    $("#withdraw_form").validate();
    $("#otp_form").validate();
    $("#data_form").submit(function(e) {
        e.preventDefault();
        var data_form = $("#data_form");
        if (data_form.valid() == false) {
            return;
        } else {
            $('#data_form').addClass('d-none');
            // $('#myModal').modal('show');
            console.log($('#address').val());
            if ($('#address').val() == '') {
                show_deposit_address();

            } else {
                $('#payment_details').removeClass('d-none');

            }
        }
    })

    function copy_text() {
        var copyText = document.getElementById("address");
        copyText.select();
        document.execCommand("copy");
        toastr.success('Copied Successfully', '', toast);
        $("#copy_button_id").html('Copied');

        setTimeout(function() {
            $("#copy_button_id").html('Copy');
        }, 3000);

        //alert("Copied the text: " + copyText.value);
    }

    let utoken = $("#token").val();
    // $.post(base + "trade/payment/check", {},
    //     function(data, status) {
    //         console.log(data);
    //     });

    function show_deposit_address() {

        let amount = $("#amount").val();
        let type = $("#payment_type").val();
        let token = $("#token").val();

        if (type == 'USDTERC20' || type == 'USDTTRC20' || type == "BTC" || type == "ETH" || type == "BNB") {
            url = 'trade/payment/nowpayment';
        } else {
            url = 'trade/payment/coinbase';
        }
        $.post(base + url, {
                amount: amount,
                type: type,
                token: token
            },
            function(data) {
                if (data.payments == 'coinbase') {
                    if (data.address) {
                        $('#payment_details').removeClass('d-none').addClass('d-block');
                        // $("#payment_details").show();
                        // $("#data_form").hide();
                        $("#amountc").text(data.amount);
                        $("#currency").text(data.currency);
                        $("#address").val(data.address);
                        var qrcode = new QRCode("qrcode", {
                            text: data.address,
                            width: 128,
                            height: 128,
                            colorDark: "#000000",
                            colorLight: "#ffffff",
                            correctLevel: QRCode.CorrectLevel.H
                        });
                        // console.log(data.address);
                        // alert("Data: " + data + "\nStatus: " + status);
                    }
                } else {
                    window.location.href = data.url;
                }
            });
    }

    $('#amount').bind("paste", function(e) {
        e.preventDefault();
    });

    var errorMsg = '',
        $valid = false;
    $.validator.addMethod("isMemberAvailabe", function(val, elem) {
        var len = $("#member_id").val();
        if (len.length >= 3) {
            $.ajax({
                async: false,
                url: '<?php echo base_url(); ?>trade/account/isMemberAvailabe',
                type: "POST",
                dataType: "json",
                data: {
                    member_id: $("#member_id").val()
                },
                success: function(response) {
                    if (response.status == 1) {
                        errorMsg = 'Member Available !';
                        $valid = true;
                        $("#member_section_name").html('User Name: ' + response.member_name);
                        $("#member_section_name").removeClass("text-danger");
                        $("#member_section_name").addClass("text-success");

                    } else if (response.status == 2) {
                        errorMsg = 'User not exist !';
                        $valid = false;
                        $("#member_id").addClass("error");
                        $("#member_section_name").addClass("text-danger");
                        $("#member_section_name").html(errorMsg);
                    } else if (response.status == 3) {
                        errorMsg = "You can't transfer funds to yourself !";
                        $valid = false;
                        $("#member_id").addClass("error");
                        $("#member_section_name").addClass("text-danger");
                        $("#member_section_name").html(errorMsg);
                    }
                }
            });
        }

        $.validator.messages["is_sponser_exist"] = errorMsg;
        return $valid;
    }, '');


    $("#transfer_form").validate({
        rules: {
            member_id: {
                required: true,
                isMemberAvailabe: true,
            },
        },
    });

    function transfer_submit() {
        var form = $("#transfer_form");

        if (form.valid() == false) {
            return;
        } else {
            Swal.fire({
                title: "Are you sure?",
                text: "",
                type: "warning",
                showCancelButton: !0,
                confirmButtonColor: "#34c38f",
                cancelButtonColor: "#f46a6a",
                confirmButtonText: "Yes",
                confirmButtonClass: "btn btn-success mt-2",
                cancelButtonClass: "btn btn-danger ml-2 mt-2",
                buttonsStyling: !1,
            }).then(function(t) {
                if (t.value == true) {
                    var url = '<?php echo base_url(); ?>trade/account/transfer_fund';
                    $.ajax({
                        type: "POST",
                        data: {
                            member_id: $('#member_id').val(),
                            amount: $('#ts_amount').val(),
                            type: $('#transfer_type').val(),
                        },
                        url: url,
                        dataType: "json",
                        success: function(response) {
                            window.location.reload(true);
                        }

                    });

                }
            });

        }
    }
</script>