<?php $this->load->view('trading/common/header'); ?>
<div class="page-content">
    <div class="container-fluid p-0">
        <div class="row mt-14px">
            <div class="col-lg-1"></div>
            <div class="col-lg-10">
                <div>
                    <div class="row">
                        <div class="col-xl-7 col-lg-7 col-md-6 col-sm-4"></div>
                        <div class="col-xl-5 col-lg-5 col-md-6 col-sm-8">
                            <div class="wrapper-new d-flex">
                                <input ng-model="startdate" id="startdate" type="text" class="form-control ml-3 mr-3" placeholder="<?= lang('start_date')?>" readonly>
                                <input ng-model="enddate" id="enddate" type="text" class="form-control mr-3" placeholder="<?= lang('end_date')?>" readonly>
                                <button type="button" onclick="get_history()" id="history" class="btn btn-primary waves-effect waves-light mr-1 pr-4 mr-3 srchbtn" style="height: 38px; width:41px !important"><i class="fa fa-search" aria-hidden="true"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card theme-border">
                    <div class="card-body">
                        <div class="text-right">
                            <button type="button" class="btn btn-info waves-effect waves-light mr-21px" data-toggle="modal" data-target=".bs-example-modal-center"><i class="fas fa-plus"></i>&nbsp;&nbsp;<?= lang('create_ticket') ?></button>
                        </div><br>
                        <div class="ml-auto mr-auto text-center d-none" id="dataloader" >
                            <img src="<?= base_url('assets/user_panel/images/preloader.gif') ?>" alt="loader">
                        </div>
                        <table id="datatable" class="table dt-responsive nowrap table-striped table-borderless" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <tr>
                                    <th>Sr</th>
                                    <th>Created Date</th>
                                    <th>Ticket ID</th>
                                    <th>Subject</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-lg-1"></div>
        </div>
    </div>
</div>
<?php $this->load->view('trading/common/footer'); ?>

<div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content bg-0e0031">
            <div class="modal-header">
                <h5 class="modal-title mt-0"><?= lang('create_ticket') ?></h5>
                <button type="button" class="close text-d9d9d9" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="card mb-0">
                    <div class="card-body">
                        <form class="custom-validation" id="data_form" name="data_form" method="POST" action="<?= base_url('trade/ticket/create_ticket') ?>" novalidate="">
                            <div class="form-group">
                                <label><?= lang('subject') ?></label>
                                <input type="text" id="subject" name="subject" class="form-control required" required="" placeholder="<?= lang('enter_subject')?>">
                            </div>
                            <div class="form-group">
                                <label><?= lang('description') ?></label>
                                <div>
                                    <textarea required="" name="description" class="form-control required" rows="3" placeholder="<?= lang('description')?>"></textarea>
                                </div>
                            </div>
                            <div class="form-group mb-0 text-right">
                                <div>
                                    <button type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                                        <?= lang('submit') ?>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<div class="modal fade" id="conversionModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header border-0 pb-0">
                <h5 class="modal-title mt-0"><?= lang('ticket_id') ?> : <span id="ticket_id_name"></span></h5>
                <button type="button" class="close text-d9d9d9" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <p class="px-3 py-1 border-bottom">Sub : <span id="ticket_subject"></span></p>
            <div class="modal-body">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title mb-4"><?= lang('chat') ?></h4>
                        <div class="chat-conversation">
                            <ul class="conversation-list" data-simplebar="init" style="max-height: 367px;">
                                <div class="simplebar-wrapper" style="margin: 0px -10px;">
                                    <div class="simplebar-height-auto-observer-wrapper">
                                        <div class="simplebar-height-auto-observer"></div>
                                    </div>
                                    <div class="simplebar-mask">
                                        <div class="simplebar-offset" style="right: -17px; bottom: 0px;">
                                            <div class="simplebar-content-wrapper" style="height: auto; overflow: hidden scroll;">
                                                <div class="simplebar-content" style="padding: 0px 10px;" id="message_chat_body">
                                                    <div style="text-align: center;padding: 150px;" id="crypto_send_loader">
                                                        <i class="fa fa-spinner fa-spin" style="font-size: 50px;"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="simplebar-placeholder" style="width: auto; height: 699px;"></div>
                                </div>
                                <div class="simplebar-track simplebar-horizontal" style="visibility: hidden;">
                                    <div class="simplebar-scrollbar" style="transform: translate3d(0px, 0px, 0px); display: none;"></div>
                                </div>
                                <div class="simplebar-track simplebar-vertical" style="visibility: visible;">
                                    <div class="simplebar-scrollbar" style="height: 192px; transform: translate3d(0px, 47px, 0px); display: block;"></div>
                                </div>
                            </ul>
                            <form id="ticket_reply" name="ticket_reply" method="POST" class="">
                                <div class="row reply-block">
                                    <input type="hidden" id="ticket_id" name="ticket_id">
                                    <div class="col-sm-9 col-8 chat-inputbar">
                                        <input type="text" class="form-control chat-input required" id="description" name="description" placeholder="Enter your text" required>
                                    </div>
                                    <div class="col-sm-3 col-4 chat-send">
                                        <button type="submit" class="btn btn-success btn-block"><?= lang('send') ?></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        // $('#dataloader').addClass('d-block');
        var table = $("#datatable").DataTable({
            "pagingType": "simple"
        });
        var bids = <?= json_encode($tickets) ?>;
        count = 1;
        bids.forEach(function(bid) {
            // table.row.add([count++, "<a class='cursor-pointer text-primary font-weight-bold' onclick=open_ticket_form(\'" + bid.ticket_id + "\')>#TC10000" + bid.id + "</a>", "<span id='" + bid.ticket_id + "' data-value='" + bid.subject + "'>" + bid.subject.substr(0, 3) + "...</span>", moment(bid.created_datetime).format('DD-MM-YYYY hh:mm:ss'), "<span class='badge badge-success text-capitalize'>" + bid.status + "</span>"]).draw(false);
            // table.row.add([count++, moment(bid.created_datetime).format('DD-MM-YYYY HH:mm'), "<a class='cursor-pointer text-primary font-weight-bold' onclick=open_ticket_form(\'" + bid.ticket_id + "\')>#TC10000" + bid.id + "</a>", "<span id='" + bid.ticket_id + "' data-value='" + bid.subject + "'>" + bid.subject.substr(0, 5) + "</span>", "<span class='badge badge-success text-capitalize'>" + bid.status + "</span>"]).draw(false);
            table.row.add([count++, moment(bid.created_datetime).format('DD-MM-YYYY HH:mm'), "<a class='cursor-pointer text-primary font-weight-bold' onclick=open_ticket_form(\'" + bid.ticket_id + "\')>#TC10" + bid.id + "</a>", "<span id='" + bid.ticket_id + "' data-value='" + bid.subject + "'>" + bid.subject.substr(0, 5) + "</span>", bid.status == 'close' ? "<span class='badge badge-danger text-capitalize' id='" + bid.ticket_id + "Status'>" + bid.status + "</span>" : "<button type='button' onclick=close_ticket(\'" + bid.ticket_id + "\') class='btn btn-danger p-2 fa fa-check'>Mark as close</button>"]).draw(false);
        });
        setTimeout(function() {
            // $('#dataloader').removeClass('d-block');
            // $('.table-wrapper').removeClass('d-none');
        }, 1000);
        
        $("#ticket_reply").submit(function(e) {
            e.preventDefault();
            var ticket_reply = $("#ticket_reply");
            if (ticket_reply.valid() == false) {
                return;
            } else {
                var description = $("#description").val();
                var ticket_id = $("#ticket_id").val();
                var postData = "ticket_id=" + ticket_id + "&description=" + description;
                var url = '<?php echo base_url(); ?>trade/ticket/submit_ticket_reply';
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: postData,
                    dataType: 'json',
                    success: function(data) {
                        var message_html = '';

                        var base_url_n = '<?php echo base_url(); ?>';
                        message_html += '<li class="clearfix odd">';
                        message_html += '<div class="chat-avatar">';
                        message_html += '<img src="' + base_url_n + 'assets/user_panel/images/users/user-3.jpg" class="avatar-xs rounded-circle" alt="avatar">';
                        message_html += '<span class="time">' + data.time + '</span>';
                        message_html += '</div>';
                        message_html += '<div class="conversation-text">';
                        message_html += '<div class="ctext-wrap">';
                        message_html += '<p>' + data.message + '</p>';
                        message_html += '</div>';
                        message_html += '</div>';
                        message_html += '</li>';
                        $("#message_chat_body").append(message_html);
                        $("#description").val('');
                    }
                });
            }
        });
    });

    function open_ticket_form(ticket_id) {
        $("#ticket_id_name").html(ticket_id);
        $("#ticket_id").val(ticket_id);
        $('#ticket_reply .reply-block').prop('id', 'reply_' + ticket_id);
        $("#description").val('');
        $('#ticket_subject').text($('#' + ticket_id).data('value'))
        let status = $('#' + ticket_id + 'Status').text();
        if (status == 'close') {
            $('#reply_' + ticket_id).parents('form').removeClass('d-block').addClass('d-none');
        } else {
            $('#reply_' + ticket_id).parents('form').removeClass('d-none').addClass('d-block');
        }
        $("#conversionModal").modal('show');

        setTimeout(function() {
            getMessageByTicketID(ticket_id);
        }, 600);

    }

    function getMessageByTicketID(ticket_id) {
        var ticket_id = $("#ticket_id").val();
        var postData = "ticket_id=" + ticket_id;
        var url = '<?php echo base_url(); ?>trade/ticket/getMessageByTicketID';
        $.ajax({
            async: false,
            type: 'POST',
            url: url,
            data: postData,
            dataType: 'json',
            success: function(data) {
                $("#message_chat_body").html(data.message_html);
            }
        });
    }
    
    function get_history() {
        var table = $('#datatable').DataTable({
            destroy: true,
            pagingType: "simple"
        }).clear().draw();
        var tableContainer = $(table.table().container());
        tableContainer.css('display', 'none');
        $('#dataloader').addClass('d-block');
        $.ajax({
            type: 'post',
            url: '<?= base_url("trade/ticket/get_range_tickets") ?>',
            data: {
                start: $('#startdate').val(),
                end: $('#enddate').val(),
            },
            success: function(res) {
                res = JSON.parse(res);
                count = 1;
                res.forEach(function(bid) {
                    // table.row.add([count++, "<a class='cursor-pointer text-primary font-weight-bold' onclick=open_ticket_form(\'" + bid.ticket_id + "\')>#TC10000" + bid.id + "</a>", moment(bid.created_datetime).format('DD-MM-YYYY hh:mm:ss'), "<span class='badge badge-success text-capitalize'>" + bid.status + "</span>"]).draw(false);
                    table.row.add([count++, moment(bid.created_datetime).format('DD-MM-YYYY hh:mm:ss'), "<a class='cursor-pointer text-primary font-weight-bold' onclick=open_ticket_form(\'" + bid.ticket_id + "\')>#TC10000" + bid.id + "</a>", "<span id='" + bid.ticket_id + "' data-value='" + bid.subject + "'>" + bid.subject.substr(0, 5) + "</span>", bid.status == 'close' ? "<span class='badge badge-success text-capitalize' id='" + bid.ticket_id + "Status'>" + bid.status + "</span>" : '<button class="btn btn-danger p-2 fa fa-check">Mark as close</button>']).draw(false);
                });
                $('#dataloader').removeClass('d-block');
                tableContainer.css('display', 'block');
            }
        })
    }
    
    function close_ticket(ticket_id) {
        Swal.fire({
            title: "Are you sure?",
            text: "",
            type: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#34c38f",
            cancelButtonColor: "#f46a6a",
            confirmButtonText: "Yes",
            confirmButtonClass: "btn btn-success mt-2",
            cancelButtonClass: "btn btn-danger ml-2 mt-2",
            buttonsStyling: !1,
        }).then(function(t) {
            if (t.value == true) {
                $.ajax({
                    url: "<?= base_url('ticket/close_ticket') ?>",
                    type: 'POST',
                    data: {
                        ticket_id: ticket_id
                    },
                    dataType: "json",
                    success: (res) => {
                        // res = JSON.parse(res);
                        if (res.success) {
                            location.reload(true);
                        } else {
                            toastr.error(res.data.message, '', toast);
                        }
                    }
                })
            } else {

            }
        });
    }
</script>
