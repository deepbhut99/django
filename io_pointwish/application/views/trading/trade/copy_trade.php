<?php $this->load->view('trading/common/header'); ?>
<link rel="stylesheet" href="<?= base_url('assets/user_panel/css/pagination.css') ?>">
<div class="page-content p-mobile pl-0 pr-0" ng-controller="CopyTrade_Controller" ng-cloak>
  <div class="container-fluid p-mobile pl-0 pr-0">
    <div class="card border-radius-0 h-45px">
      <div class="card-body pb-0 pt-0 pl-0">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs nav-tabs-custom border-bottom-0" role="tablist">
          <li class="nav-item">
            <a class="nav-link pt-12px pb-12px active bg-transparent p-community" data-toggle="tab" href="#community" role="tab">
              <span class="d-block d-sm-none"><i class="fa fa-users"></i></span>
              <span class="d-none d-sm-block"><?= lang('community') ?></span>
            </a>
          </li>
          <li class="nav-item" ng-click="get_portfolio_lists()">
            <a class="nav-link pt-12px pb-12px bg-transparent p-portfolio" data-toggle="tab" href="#portfolio" role="tab">
              <span class="d-block d-sm-none"><i class="fa fa-folder-open"></i></span>
              <span class="d-none d-sm-block"><?= lang('portfolio') ?></span>
            </a>
          </li>
          <li class="nav-item bg-transparent">
            <a class="nav-link pt-12px pb-12px p-expert" data-toggle="tab" href="#expert_area" role="tab">
              <span class="d-block d-sm-none"><i class="fa fa-star"></i></span>
              <span class="d-none d-sm-block"><?= lang('expert_area') ?></span>
            </a>
          </li>
        </ul>
      </div>
    </div>


    <!-- Tab panes -->
    <div class="tab-content">
      <div class="tab-pane active" id="community" role="tabpanel">

      </div>
      <div class="tab-pane p-0" id="portfolio" role="tabpanel">

      </div>
      <div class="tab-pane p-0" id="expert_area" role="tabpanel">

      </div>
    </div>
  </div> <!-- container-fluid -->
</div>
<?php $this->load->view('trading/common/footer'); ?>