<?php $this->load->view('trading/common/header'); ?>
<div class="page-content">
    <div class="container-fluid">

        <!-- start page title -->

        <!-- end page title -->

        <div class="card card-timer ml-auto mr-auto mt-14px mb-0">
            <div class="card-body crd-body-timer ml-auto mr-auto" style="padding-bottom: 35px;">
                <div class="text-center">
                    <h4><?= lang('next_draw') ?></h4>
                    <!-- <p class="opacity-half">Time left to end</p> -->
                </div>
                <div class="display-flex" id="timer">
                    <div class="card-count" id="hours"></div>
                    <p class="opacity-half p-time1"><?= lang('hour') ?></p>
                    <div class="card-count" id="minutes"></div>
                    <p class="opacity-half p-time2"><?= lang('min') ?></p>
                    <div class="card-count" id="seconds"></div>
                    <p class="opacity-half p-time3"><?= lang('sec') ?></p>



                </div>
                <!-- <div class="d-flex">
                    <p class=""><?= lang('hour') ?></p>
                    <p class=""><?= lang('min') ?></p>
                    <p class=""><?= lang('sec') ?></p>
                </div> -->

            </div>
        </div>

        <div class="row">
            <div class="col-xl-1 col-md-1">
            </div>
            <div class="col-xl-10 col-md-10">
                <div>
                    <h5 class="dt text-center text-media">1st<span>
                            <!-- <small style="vertical-align: super;">st</small></span>  -->
                            December, 2020</h5>
                </div>
            </div>
            <div class="col-xl-1 col-md-1">
            </div>
        </div><br>

        <div class="row">

            <div class="col-xl-1 col-md-1">
            </div>
            <div class="col-xl-10 col-md-10">
                <div class="col-xl-12 col-md-12">
                    <div class="card directory-card winerweb mb-14px">
                        <div class="card-body" style="padding-bottom: 10px;">

                            <div class="row">
                                <div class="col-xl-1 col-md-1">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">1</h5>
                                </div>
                                <div class="col-xl-2 col-md-2">

                                    <img src="<?= base_url('assets/user_panel/images/users/user-2.jpg') ?>" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg" style="margin-top: -12px; vertical-align: bottom;">
                                    <!-- <span class="badge badge-danger badge-pill1">1</span> -->
                                </div>

                                <div class="col-xl-2 col-md-2">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">Sophia</h5>
                                </div>
                                <div class="col-xl-3 col-md-3">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">sophia@gmail.com</h5>
                                </div>
                                <div class="col-xl-2 col-md-2">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">95%</h5>
                                </div>

                                <div class="col-xl-2 col-md-2">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">Terms</h5>
                                </div>

                            </div>


                        </div>
                    </div>
                </div>
                <div class="col-xl-12 col-md-12">
                    <div class="card directory-card winerweb mb-14px">
                        <div class="card-body" style="padding-bottom: 10px;">

                            <div class="row">
                                <div class="col-xl-1 col-md-1">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">2</h5>
                                </div>
                                <div class="col-xl-2 col-md-2">

                                    <img src="<?= base_url('assets/user_panel/images/users/user-4.jpg') ?>" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg" style="margin-top: -12px; vertical-align: bottom;">
                                    <!-- <span class="badge badge-danger badge-pill1">1</span> -->
                                </div>

                                <div class="col-xl-2 col-md-2">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">Alex</h5>
                                </div>
                                <div class="col-xl-3 col-md-3">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">alex@gmail.com</h5>
                                </div>
                                <div class="col-xl-2 col-md-2">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">94%</h5>
                                </div>

                                <div class="col-xl-2 col-md-2">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">Terms</h5>
                                </div>

                            </div>


                        </div>
                    </div>
                </div>
                <div class="col-xl-12 col-md-12">
                    <div class="card directory-card winerweb mb-14px">
                        <div class="card-body" style="padding-bottom: 10px;">

                            <div class="row">
                                <div class="col-xl-1 col-md-1">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">3</h5>
                                </div>
                                <div class="col-xl-2 col-md-2">

                                    <img src="<?= base_url('assets/user_panel/images/users/user-9.jpg') ?>" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg" style="margin-top: -12px; vertical-align: bottom;">
                                    <!-- <span class="badge badge-danger badge-pill1">1</span> -->
                                </div>

                                <div class="col-xl-2 col-md-2">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">Olivia</h5>
                                </div>
                                <div class="col-xl-3 col-md-3">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">olivia@gmail.com</h5>
                                </div>
                                <div class="col-xl-2 col-md-2">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">93%</h5>
                                </div>

                                <div class="col-xl-2 col-md-2">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">Terms</h5>
                                </div>

                            </div>


                        </div>
                    </div>
                </div>


            </div>
            <div class="col-xl-1 col-md-1">
            </div>

        </div>

        <div class="row">



            <div class="col-xl-4 col-md-6">
                <div class="card directory-card winer">
                    <div class="card-body">
                        <div class="media">
                            <img src="<?= base_url('assets/user_panel/images/users/user-2.jpg')?>" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg" style="height: 6rem; width:6rem;">
                            <div class="media-body ml-3">
                                <h5 class="text-primary font-size-18 mt-0 mb-1" style="color: deepskyblue!important;">1. Sophia</h5>
                                <!-- <p class="mb-0">Sophia</p> -->
                                <p class="mb-0">sophia@gmail.com</p>
                                <p class="mb-0">95%</p>
                                <p class="mb-0">terms</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="card directory-card winer">
                    <div class="card-body">
                        <div class="media">
                            <img src="<?= base_url('assets/user_panel/images/users/user-4.jpg') ?>" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg" style="height: 6rem; width:6rem;">
                            <div class="media-body ml-3">
                                <h5 class="text-primary font-size-18 mt-0 mb-1" style="color: deepskyblue!important;">2. Alex</h5>
                                <!-- <p class="mb-0">Alex</p> -->
                                <p class="mb-0">alex@gmail.com</p>
                                <p class="mb-0">94%</p>
                                <p class="mb-0">terms</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="card directory-card winer">
                    <div class="card-body">
                        <div class="media">
                            <img src="<?= base_url('assets/user_panel/images/users/user-9.jpg') ?>" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg" style="height: 6rem; width:6rem;">
                            <div class="media-body ml-3">
                                <h5 class="text-primary font-size-18 mt-0 mb-1" style="color: deepskyblue!important;">3. Olivia</h5>

                                <!-- <p class="mb-0">Olivia</p> -->
                                <p class="mb-0">olivia@gmail.com</p>
                                <p class="mb-0">93%</p>
                                <p class="mb-0">terms</p>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
            <!-- end col -->

        </div>





        <div class="row">
            <div class="col-xl-1 col-md-1">
            </div>
            <div class="col-xl-10 col-md-10">
                <div>
                    <h5 class="dt text-center text-media">2nd December, 2020</h5>
                </div>
            </div>
            <div class="col-xl-1 col-md-1">
            </div>
        </div><br>

        <div class="row">

            <div class="col-xl-1 col-md-1">
            </div>
            <div class="col-xl-10 col-md-10">
                <div class="col-xl-12 col-md-12">
                    <div class="card directory-card winerweb mb-14px">
                        <div class="card-body" style="padding-bottom: 10px;">

                            <div class="row">
                                <div class="col-xl-1 col-md-1">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">1</h5>
                                </div>
                                <div class="col-xl-2 col-md-2">

                                    <img src="<?= base_url('assets/user_panel/images/users/user-2.jpg') ?>" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg" style="margin-top: -12px; vertical-align: bottom;">
                                    <!-- <span class="badge badge-danger badge-pill1">1</span> -->
                                </div>

                                <div class="col-xl-2 col-md-2">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">Sophia</h5>
                                </div>
                                <div class="col-xl-3 col-md-3">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">sophia@gmail.com</h5>
                                </div>
                                <div class="col-xl-2 col-md-2">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">95%</h5>
                                </div>

                                <div class="col-xl-2 col-md-2">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">Terms</h5>
                                </div>

                            </div>


                        </div>
                    </div>
                </div>
                <div class="col-xl-12 col-md-12">
                    <div class="card directory-card winerweb mb-14px">
                        <div class="card-body" style="padding-bottom: 10px;">

                            <div class="row">
                                <div class="col-xl-1 col-md-1">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">2</h5>
                                </div>
                                <div class="col-xl-2 col-md-2">

                                    <img src="<?= base_url('assets/user_panel/images/users/user-4.jpg') ?>" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg" style="margin-top: -12px; vertical-align: bottom;">
                                    <!-- <span class="badge badge-danger badge-pill1">1</span> -->
                                </div>

                                <div class="col-xl-2 col-md-2">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">Alex</h5>
                                </div>
                                <div class="col-xl-3 col-md-3">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">alex@gmail.com</h5>
                                </div>
                                <div class="col-xl-2 col-md-2">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">94%</h5>
                                </div>

                                <div class="col-xl-2 col-md-2">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">Terms</h5>
                                </div>

                            </div>


                        </div>
                    </div>
                </div>
                <div class="col-xl-12 col-md-12">
                    <div class="card directory-card winerweb mb-14px">
                        <div class="card-body" style="padding-bottom: 10px;">

                            <div class="row">
                                <div class="col-xl-1 col-md-1">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">3</h5>
                                </div>
                                <div class="col-xl-2 col-md-2">

                                    <img src="<?= base_url('assets/user_panel/images/users/user-9.jpg') ?>" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg" style="margin-top: -12px; vertical-align: bottom;">
                                    <!-- <span class="badge badge-danger badge-pill1">1</span> -->
                                </div>

                                <div class="col-xl-2 col-md-2">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">Olivia</h5>
                                </div>
                                <div class="col-xl-3 col-md-3">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">olivia@gmail.com</h5>
                                </div>
                                <div class="col-xl-2 col-md-2">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">93%</h5>
                                </div>

                                <div class="col-xl-2 col-md-2">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">Terms</h5>
                                </div>

                            </div>


                        </div>
                    </div>
                </div>


            </div>
            <div class="col-xl-1 col-md-1">
            </div>

        </div>

        <div class="row">



            <div class="col-xl-4 col-md-6">
                <div class="card directory-card  winer">
                    <div class="card-body">
                        <div class="media">
                            <img src="<?= base_url('assets/user_panel/images/users/user-2.jpg')?>" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg" style="height: 6rem; width:6rem;">
                            <div class="media-body ml-3">
                                <h5 class="text-primary font-size-18 mt-0 mb-1" style="color: deepskyblue!important;">1. Sophia</h5>

                                <!-- <p class="mb-0">Sophia</p> -->
                                <p class="mb-0">sophia@gmail.com</p>
                                <p class="mb-0">95%</p>
                                <p class="mb-0">terms</p>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="card directory-card  winer">
                    <div class="card-body">
                        <div class="media">
                            <img src="<?= base_url('assets/user_panel/images/users/user-4.jpg') ?>" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg" style="height: 6rem; width:6rem;">
                            <div class="media-body ml-3">
                                <h5 class="text-primary font-size-18 mt-0 mb-1" style="color: deepskyblue!important;">2. Alex</h5>

                                <!-- <p class="mb-0">Alex</p> -->
                                <p class="mb-0">alex@gmail.com</p>
                                <p class="mb-0">94%</p>
                                <p class="mb-0">terms</p>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="card directory-card  winer">
                    <div class="card-body">
                        <div class="media">
                            <img src="<?= base_url('assets/user_panel/images/users/user-9.jpg') ?>" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg" style="height: 6rem; width:6rem;">
                            <div class="media-body ml-3">
                                <h5 class="text-primary font-size-18 mt-0 mb-1" style="color: deepskyblue!important;">3. Olivia</h5>

                                <!-- <p class="mb-0">Olivia</p> -->
                                <p class="mb-0">olivia@gmail.com</p>
                                <p class="mb-0">93%</p>
                                <p class="mb-0">terms</p>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
            <!-- end col -->

        </div>


    </div> <!-- container-fluid -->
</div>
<?php $this->load->view('trading/common/footer'); ?>

<script>
    function makeTimer() {

        //		var endTime = new Date("29 April 2018 9:56:00 GMT+01:00");	
        var endTime = new Date("20 Feb 2021 12:05:00");
        endTime = (Date.parse(endTime) / 1000);

        var now = new Date();
        now = (Date.parse(now) / 1000);

        var timeLeft = endTime - now;

        var days = Math.floor(timeLeft / 86400);
        var hours = Math.floor((timeLeft - (days * 86400)) / 3600);
        var minutes = Math.floor((timeLeft - (days * 86400) - (hours * 3600)) / 60);
        var seconds = Math.floor((timeLeft - (days * 86400) - (hours * 3600) - (minutes * 60)));

        if (hours < "10") {
            hours = "0" + hours;
        }
        if (minutes < "10") {
            minutes = "0" + minutes;
        }
        if (seconds < "10") {
            seconds = "0" + seconds;
        }

        $("#days").html(days + "<span></span>");
        $("#hours").html(hours + "<span></span>");
        $("#minutes").html(minutes + "<span></span>");
        $("#seconds").html(seconds + "<span></span>");

    }
    setInterval(function() {
        makeTimer();
    }, 1000);
</script>