<?php
$session_data = getSessionData();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="<?php if (!empty($title)) echo $title; ?>">
    <title>pwt<?php if (!empty($title)) echo $title; ?></title>
    <link rel="canonical" href="<?= base_url($this->uri->uri_string()) ?>" />

    <script src="<?= base_url('assets/front/js/minified.js') ?>"></script>
    <script src="<?= base_url('assets/user_panel/js/duplicate.js?v=' . rand()) ?>"></script>
    <script>
        $(document).ready(function() {
            if (window.IsDuplicate()) {
                document.write('<h1>Duplicate window detected close other one and refresh the page</h1>');
                window.stop();
            }
        });
    </script>
    <script src="<?= base_url('assets/front/js/devtools.js') ?>"></script>
    <script>
        var www = $(window).width();
        if (www > 600) {
            window.addEventListener('devtoolschange', function(e) {
                if (e.detail.open == true) {
                    document.write('<h1>Developer tools is opened. Please close <span style="color:red">(Press F12)</span> and refresh the page</h1>');
                    window.stop();
                }
            });
        }
    </script>

    <link rel="stylesheet" href="<?= base_url('assets/front/css/bootstrap.min.css') ?>">
    <link href="<?= base_url('assets/front/css/minified.css?v=' . rand()) ?>" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?= base_url('assets/front/css/flag_icon.css') ?>">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="shortcut icon" href="<?= base_url('assets/front/images/favicon.ico') ?>" type="image/x-icon">

    <style>
        @media only screen and (min-width: 992px) {
            .drkhed {
                max-width: 100%;
            }
        }

        @media only screen and (min-width: 1200px) {
            .drkhed {
                max-width: 85%;
            }

            .drkmnu {
                margin-left: 0% !important;
                margin-right: 0 !important;
            }
        }

        @media only screen and (min-width: 1598px) {
            .drkhed {
                max-width: 85%;
            }

            .drkmnu {
                margin-left: 5% !important;
                margin-right: 0 !important;
            }
        }
    </style>

</head>

<body>
    <!--============= ScrollToTop Section Starts Here =============-->
    <div class="preloader">
        <div class="preloader-inner">
            <div class="preloader-icon">
                <span></span>
                <span></span>
            </div>
        </div>
    </div>
    <a href="#0" class="scrollToTop"><i class="fas fa-angle-up"></i></a>
    <div class="overlay"></div>
    <!--============= ScrollToTop Section Ends Here =============-->

    <!--============= Header Section Starts Here =============-->
    <header class="header-section">
        <div class="container drkhed">
            <div class="header-wrapper">
                <div class="logo">
                    <a href="<?= base_url('page') ?>">
                        <img src="<?= base_url('assets/front/images/logo/logo.svg') ?>" alt="logo">
                    </a>
                </div>
                <ul class="menu drkmnu">

                    <li>
                        <a href="<?= base_url('page') ?>"><?= lang('home') ?></a>
                    </li>
                    <li>
                        <a href="<?= base_url('page#features') ?>"><?= lang('feature') ?></a>
                    </li>
                    <li>
                        <a href="<?= base_url('page#roadmap') ?>"><?= lang('roadmap') ?></a>
                    </li>
                    <li>
                        <a href="<?= base_url('page#faq') ?>"><?= lang('faq') ?></a>
                    </li>
                    <li>
                        <a href="<?= base_url('home/contact-us') ?>"><?= lang('contact') ?></a>
                    </li>
                    <li class="d-sm-none pl-2">
                        <?php if ($this->session->userdata('user_id')) { ?>
                            <a href="<?php echo base_url('trade/dashboard'); ?>" class="m-0 header-button"><?= lang('dashboard') ?></a>
                        <?php } else { ?>
                            <a class="m-0 header-button" href="<?= base_url('login') ?>"><span><?= lang('signinn') ?> </span> / <span> <?= lang('signupp') ?> </span></a>
                        <?php } ?>
                    </li>
                </ul>

                <div class="header-right linemob">
                    <!--<select class="select-bar mt-1" type="language" onchange="window.location='<?php echo base_url(); ?>page/index/'+this.value">-->
                    <!--<option value="english" <?= isset($this->session->language) ? ($this->session->language == 'english' ? 'selected' : "") : ''  ?> data-country="gb">EN</option>-->
                    <!--    <option value="vietnam" <?= isset($this->session->language) ? ($this->session->language == 'vietnam' ? 'selected' : "") : ''  ?> data-country="vn">VN</option>-->
                    <!--    <option value="chienese" <?= isset($this->session->language) ? ($this->session->language == 'chienese' ? 'selected' : "") : ''  ?> data-country="ch">CH</option>-->
                    <!--    <option value="thai" <?= isset($this->session->language) ? ($this->session->language == 'thai' ? 'selected' : "") : ''  ?> data-country="th">TH</option>-->
                    <!--    <option value="combodia" <?= isset($this->session->language) ? ($this->session->language == 'combodia' ? 'selected' : "") : ''  ?> data-country="kh">KH</option>-->
                    <!--    <option value="japanese" <?= isset($this->session->language) ? ($this->session->language == 'japanese' ? 'selected' : "") : ''  ?> data-country="jp">JP</option>-->
                    <!--     <option value="german" <?= isset($this->session->language) ? ($this->session->language == 'german' ? 'selected' : "") : ''  ?> data-country="de">De</option> -->
                    <!--    <option value="indonesian" <?= isset($this->session->language) ? ($this->session->language == 'indonesian' ? 'selected' : "") : ''  ?> data-country="id">ID</option>-->
                    <!--    <option value="russian" <?= isset($this->session->language) ? ($this->session->language == 'russian' ? 'selected' : "") : ''  ?> data-country="ru">RU</option>-->
                    <!--    <option value="malay" <?= isset($this->session->language) ? ($this->session->language == 'malay' ? 'selected' : "") : ''  ?> data-country="my">MY</option>-->
                    <!--    <option value="korean" <?= isset($this->session->language) ? ($this->session->language == 'korean' ? 'selected' : "") : ''  ?> data-country="kr">KR</option>-->
                    <!--</select>-->
                    <?php if ($this->session->userdata('user_id')) { ?>
                        <a href="<?php echo base_url('trade/trading'); ?>" class="m-0 header-button d-none d-sm-inline-block"><?= lang('dashboard') ?></a>
                    <?php } else { ?>
                        <a href="<?= base_url('login') ?>" class="header-button d-none d-sm-inline-block"><?= lang('signin') ?></a>
                    <?php } ?>
                </div>

                <div class="header-bar d-lg-none">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
        </div>
    </header>
    <!--============= Header Section Ends Here =============-->
