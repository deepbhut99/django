<?php
$this->load->view('trading/trade/trade_header'); ?>

<!-- Page CSS -->
<link rel="stylesheet" href="<?= base_url('assets/user_panel/css/trade/bootstrap.min.css'); ?>">
<link rel="stylesheet" href="<?= base_url('assets/user_panel/css/trade/gui.css'); ?>">
<link rel="stylesheet" href="<?= base_url('assets/user_panel/css/trade/popup.css'); ?>">

<!-- Page Scripts -->
<script src="<?= base_url('assets/user_panel/js/trade/socket.io.js'); ?>"></script>
<script src="<?= base_url('assets/user_panel/js/trade/highstock.js'); ?>"></script>
<script src="<?= base_url('assets/user_panel/js/trade/highcharts-more.js'); ?>"></script>
<script src="<?= base_url('assets/user_panel/js/trade/solid-gauge.js'); ?>"></script>
<script src="<?= base_url('assets/user_panel/js/trade/drag-panes.js'); ?>"></script>
<script src="<?= base_url('assets/user_panel/js/trade/indicators-all.js'); ?>"></script>
<script src="<?= base_url('assets/user_panel/js/trade/bollinger-bands.js'); ?>"></script>
<script src="<?= base_url('assets/user_panel/js/trade/ema.js'); ?>"></script>
<script src="<?= base_url('assets/user_panel/js/trade/annotations-advanced.js'); ?>"></script>
<!-- <script src="<?= base_url('assets/user_panel/js/trade/full-screen.js'); ?>"></script> -->
<script src="<?= base_url('assets/user_panel/js/trade/price-indicator.js'); ?>"></script>
<script src="<?= base_url('assets/user_panel/js/trade/stock-tools.js'); ?>"></script>
<script src="<?= base_url('assets/user_panel/js/trade/echarts.min.js'); ?>"></script>
<!-- <script src="<?= base_url('assets/user_panel/js/owl.carousel.js'); ?>"></script> -->

<!-- Wheel JS-->
<!-- <script src="<?= base_url('assets/user_panel/libs/wheel/Winwheel.js') ?>"></script>
<script src="<?= base_url('assets/user_panel/libs/wheel/TweenMax.min.js') ?>"></script> -->

<!-- App CSS-->
<link href="<?= base_url('assets/user_panel/css/trade.css?v=2') ?>" rel="stylesheet" type="text/css" />

<script>
    var USER = <?= $this->session->user_id ?>;
</script>

<div class="page-loader"></div>
<div class="page-content">
    <div class="container-fluid mt-2">
        <audio id="coin-sound" src="<?= base_url('assets/user_panel/mp3/coins.mp3'); ?>" preload="auto" style="display: none;" controls></audio>
        <audio id="alert-sound" src="<?= base_url('assets/user_panel/mp3/alert.mp3'); ?>" preload="auto" style="display: none;" controls></audio>
        <audio id="wheel-sound" src="<?= base_url('assets/user_panel/libs/wheel/tick.mp3'); ?>" preload="auto" style="display: none;" controls></audio>
        <div class="row mt-n2">
            <div class="col-sm-10 mt-point-nine-rem">
                <div class="card bg-transparent mt-n4 mt-xsm-n2point3rem mb-2 card-chart position-sticky border-0 bg-img-trade">
                    <div class="card-body p-0">
                        <div class="chart-wrapper">
                            <div class="profit-loader-text d-none">
                                <span>Profit Earned: 1000 USDT</span>
                            </div>
                            <div class="highcharts-stocktools-wrapper highcharts-bindings-container highcharts-bindings-wrapper">
                                <!-- <div class="highcharts-menu-wrapper"> -->
                                <div class="menu-wrapper d-flex">
                                    <button class="btn btn-indicator mr-2 hw-32" type="button" data-toggle="modal" data-target="indicators-modal">
                                        <img src="https://code.highcharts.com/gfx/stock-icons/indicators.svg" alt="">
                                    </button>
                                    <select id="ddlType" class="form-control p-0 bg-white" style="width: 85px;">
                                        <option value="BTC" <?= ($coin == 'BTC' ? 'selected' : ''); ?>>BTC/USD</option>
                                        <option value="ETH" <?= ($coin == 'ETH' ? 'selected' : ''); ?>>ETH/USD</option>
                                    </select>
                                </div>
                            </div>
                            <div id="container" class="chart"></div>
                        </div>
                        <input type="hidden" id="coin" value="<?= $coin; ?>" />
                        <input type="hidden" id="lastValue" value="" />
                        <input type="hidden" id="txtAddIndicator" value="aroonoscillator" />
                        <input type="hidden" id="txtEditIndicator" value="aroonoscillator" />
                    </div>
                </div>
                <div class="card-gauge">
                    <div class="default-display">
                        <!-- Tab panes -->
                        <div class="mt-4">
                            <div class="p-3" role="">
                                <div class="row results-container"></div>
                            </div>
                        </div>
                    </div>
                    <div class="mobile-display result-container-div">
                        <div class="p-2">
                            <div class="card-body">
                                <div class="row results-container"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="row default-display pr-2">
                    <div class="upper-default mx-auto my-auto">
                        <div class="">
                            <div class="row">
                                <div class="col-sm-12 p-1">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text text-d9d9d9" id="basic-addon1">USDT</span>
                                        </div>
                                        <input type="number" class="form-control text-d9d9d9" placeholder="Enter Bid Amount" aria-label="Enter Bid Amount" aria-describedby="basic-addon1" id="txtAmount" placeholder="Enter Bid Amount" />
                                        <span class="searchclear" class="">x</span>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="">
                            <div class="row">
                                <div class="col-sm-4 m-0 p-0 px-1">
                                    <button class="btn left-border gold btnAmt w-100 mx-auto my-1 py-1" data-amount="5" type="button">+5</button>
                                </div>
                                <div class="col-sm-4 m-0 p-0 px-1">
                                    <button class="btn light-gold btnAmt w-100 mx-auto my-1 py-1" data-amount="10" type="button">+10</button>
                                </div>
                                <div class="col-sm-4 m-0 p-0 px-1">
                                    <button class="btn right-border gold btnAmt w-100 mx-auto my-1 py-1" data-amount="20" type="button">+20</button>
                                </div>
                                <div class="col-sm-4 m-0 p-0 px-1">
                                    <button class="btn left-border light-gold btnAmt w-100 mx-auto my-1 py-1" data-amount="50" type="button">+50</button>
                                </div>
                                <div class="col-sm-4 m-0 p-0 px-1">
                                    <button class="btn gold btnAmt w-100 mx-auto my-1 py-1" data-amount="100" type="button">+100</button>
                                </div>
                                <div class="col-sm-4 m-0 p-0 px-1">
                                    <button class="btn right-border light-gold btnAmt w-100 mx-auto my-1 py-1" data-amount="All" type="button">All</button>
                                </div>
                            </div>
                        </div>
                        <div class="">
                            <div class="row text-center">
                                <div class="col-sm-12" style="color: #02C076;">
                                    <span style="font-size: 20px;">Profit: </span>
                                    <span style="font-size: 22px;">95%</span>
                                </div>
                                <span class="ml-3 mx-auto text-white" style="font-size: 36px;" id="profit">+0 USDT</span>
                            </div>
                        </div>
                        <div class="">
                            <div class="progress mx-4">
                                <div class="progress-bar progress-bar-warning green" id="contentBuy" role="progressbar" style="width:50%;">
                                    50%
                                </div>
                                <div class="progress-bar progress-bar-success red" id="contentSell" role="progressbar" style="width:50%;">
                                    50%
                                </div>
                            </div>
                        </div>
                        <div class="buttons">
                            <div class="buttons-wrapper mx-auto">
                                <button class="btn green btn-up-action w-100" data-growth='1' disabled><span>UP</span></button>
                                <div class="btn btn-secondary w-100 my-1" id="timer">Wait Time: 0s</div>
                                <button class="btn red btn-down-action w-100" data-growth='-1' disabled><span>DOWN</span></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mobile-display">
                    <div class="row">
                        <div class="col-sm-12 text-center profit-col text-white" style="font-size: 20px;">
                            <span class="text-theme-green">Profit 95%</span> <span style="font-size: 22px;" id="profit1">+0 USDT</span>
                        </div>
                        <div class="col-sm-12">
                            <div class="input-group">
                                <div class="btn btn-secondary btnAmt w-25 mr-1 left-border" data-amount="-5"><i class="fa fa-minus"></i></div>
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon2">USDT</span>
                                </div>
                                <input type="number" class="form-control mr-1 text-d9d9d9" placeholder="Enter Bid Amount" aria-label="Enter Bid Amount" aria-describedby="basic-addon2" id="txtAmount1" value="0" />
                                <span class="searchclear" class="">x</span>
                                <div class="btn btn-secondary btnAmt w-25 right-border" data-amount="5"><i class="fa fa-plus"></i></div>

                            </div>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-sm-12 buttons text-center">
                            <button class="btn green btn-up-action left-border" style="width: 24%;" data-growth='1' disabled><span>UP</span></button>
                            <div class="btn btn-secondary" id="timer1">Wait Time 0s</div>
                            <button class="btn red btn-down-action right-border" style="width: 24%;" data-growth='-1' disabled><span>DOWN</span></button>
                        </div>
                    </div>
                </div>

                <!-- <div id="owl-carousel" class="owl-carousel owl-theme notification-web">
                    <?php foreach ($news as $row) { ?>
                        <div class="card bg-4f5763 card-owl-news">
                            <div class="card-header pb-0 border-0 bg-4f5763">
                                <h4 class="font-weight-bold text-ffba00"><?= $row['title'] ?></h4>
                            </div>
                            <div class="card-body py-1 px-3 card-news-body">
                                <div class="text-white">
                                    <p class="mb-0 font-size-14"><?= $row['description'] ?></p>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div> -->
                

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="indicators-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content bg-grey">
            <div class="modal-body">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link active py-1 active-tab text-white add-indicator-tab" data-toggle="tab" href="#add">
                            <span class="d-block d-sm-none"><i class="fa fa-plus"></i></span>
                            <span class="d-none d-sm-block">Add</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active-tab py-1 text-white remove-indicator-tab" data-toggle="tab" href="#edit">
                            <span class="d-block d-sm-none"><i class="fa fa-edit"></i></span>
                            <span class="d-none d-sm-block">Edit</span>
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <br />
                    <div class="tab-pane container active" id="add">
                        <div class="indicator-wrapper row">
                            <div class="indicator-list">
                                <ul class="list-group">
                                    <li class="list-group-item add-indicator-item active" data-id="aroonoscillator" data-params="1" data-period="14" data-title="AO">AO</li>
                                    <li class="list-group-item add-indicator-item" data-id="aroon" data-params="1" data-period="25" data-title="Aroon">Aroon</li>
                                    <li class="list-group-item add-indicator-item" data-id="atr" data-params="1" data-period="20" data-title="ATR">ATR</li>
                                    <li class="list-group-item add-indicator-item" data-id="abands" data-params="1" data-period="14" data-title="Acceleration Bands">Acceleration Bands</li>
                                    <li class="list-group-item add-indicator-item" data-id="bb" data-params="1" data-period="20" data-title="Bollinger Bands">Bollinger Bands</li>
                                    <li class="list-group-item add-indicator-item" data-id="dema" data-params="1" data-period="9" data-title="DEMA">DEMA</li>
                                    <li class="list-group-item add-indicator-item" data-id="ema" data-params="1" data-period="9" data-title="EMA">EMA</li>
                                    <li class="list-group-item add-indicator-item" data-id="linearRegression" data-params="1" data-period="14" data-title="Linear Regression">Linear Regression</li>
                                    <li class="list-group-item add-indicator-item" data-id="linearRegressionAngle" data-params="1" data-period="14" data-title="Linear Regression Angle">Linear Regression Angle</li>
                                    <li class="list-group-item add-indicator-item" data-id="linearRegressionIntercept" data-params="1" data-period="14" data-title="Linear Regression Intercept">Linear Regression Intercept</li>
                                    <li class="list-group-item add-indicator-item" data-id="linearRegressionSlope" data-params="1" data-period="14" data-title="Linear Regression Slope">Linear Regression Slope</li>
                                    <li class="list-group-item add-indicator-item" data-id="macd" data-params="4" data-period="26" data-title="MACD">MACD</li>
                                    <li class="list-group-item add-indicator-item" data-id="mfi" data-params="1" data-period="14" data-title="Money Flow Index">Money Flow Index</li>
                                    <li class="list-group-item add-indicator-item" data-id="natr" data-params="1" data-period="14" data-title="NATR">NATR</li>
                                    <li class="list-group-item add-indicator-item" data-id="pivotpoints" data-params="1" data-period="28" data-title="Pivot Points">Pivot Points</li>
                                    <li class="list-group-item add-indicator-item" data-id="ppo" data-params="1" data-period="9" data-title="PPO">PPO</li>
                                    <li class="list-group-item add-indicator-item" data-id="stochastic" data-params="1" data-period="14" data-title="Stochastic">Stochastic</li>
                                    <li class="list-group-item add-indicator-item" data-id="slowstochastic" data-params="1" data-period="14" data-title="Slow Stochastic">Slow Stochastic</li>
                                    <li class="list-group-item add-indicator-item" data-id="sma" data-params="1" data-period="14" data-title="SMA">SMA</li>
                                    <li class="list-group-item add-indicator-item" data-id="supertrend" data-params="1" data-period="10" data-title="Supertrend">Supertrend</li>
                                    <li class="list-group-item add-indicator-item" data-id="tema" data-params="1" data-period="9" data-title="TEMA">TEMA</li>
                                    <li class="list-group-item add-indicator-item" data-id="vwap" data-params="1" data-period="30" data-title="VWAP">VWAP</li>
                                    <li class="list-group-item add-indicator-item" data-id="williamsr" data-params="1" data-period="14" data-title="Williams %R">Williams %R</li>
                                    <li class="list-group-item add-indicator-item" data-id="wma" data-params="1" data-period="9" data-title="WMA">WMA</li>
                                </ul>
                            </div>
                            <div class="indicator-add-params">
                                <div class="mx-4">
                                    <H3 class="add-indicator-title text-d9d9d9">AO</H3>
                                    <br />
                                    <div class="normal-add-params">
                                        <div class="form-group">
                                            <label for="" class="text-d9d9d9">Period</label>
                                            <input class="form-control w-100 bg-12161c text-d9d9d9" type="number" id="txtPeriod" value="14" />
                                        </div>
                                    </div>
                                    <div class="extra-add-params d-none">
                                        <div class="form-group">
                                            <label for="" class="text-d9d9d9">Short Period</label>
                                            <input class="form-control w-100 bg-12161c text-d9d9d9" type="number" id="txtShortPeriod" value="12" />
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="text-d9d9d9">Long Period</label>
                                            <input class="form-control w-100 bg-12161c text-d9d9d9" type="number" id="txtLongPeriod" value="26" />
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="text-d9d9d9">Signal Period</label>
                                            <input class="form-control w-100 bg-12161c text-d9d9d9" type="number" id="txtSignalPeriod" value="9" />
                                        </div>
                                        <br />
                                    </div>
                                    <div class="form-group text-right">
                                        <button class="btn btn-light btn-add-indicator">Add</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane container fade" id="edit">
                        <div class="indicator-wrapper row">
                            <div class="indicator-list">
                                <ul class="list-group edit-indicators"></ul>
                            </div>
                            <div class="indicator-edit-params px-2">
                                <div class="mx-4">
                                    <H3 class="edit-indicator-title text-d9d9d9">AO</H3>
                                    <br />
                                    <div class="normal-edit-params">
                                        <div class="form-group">
                                            <label for="" class="text-d9d9d9">Period</label>
                                            <input class="form-control w-100 bg-12161c text-d9d9d9" type="number" id="txtEditPeriod" />
                                        </div>
                                    </div>
                                    <div class="extra-edit-params d-none">
                                        <div class="form-group">
                                            <label for="" class="text-d9d9d9">Short Period</label>
                                            <input class="form-control w-100 bg-12161c text-d9d9d9" type="number" id="txtEditShortPeriod" />
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="text-d9d9d9">Long Period</label>
                                            <input class="form-control w-100 text-d9d9d9 bg-12161c" type="number" id="txtEditLongPeriod" />
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="text-d9d9d9">Signal Period</label>
                                            <input class="form-control w-100 bg-12161c text-d9d9d9" type="number" id="txtEditSignalPeriod" />
                                        </div>
                                        <br />
                                    </div>
                                    <div class="form-group text-right">
                                        <button class="btn btn-light btn-remove-indicator">Remove</button>
                                        <button class="btn btn-light btn-save-indicator">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="alerts-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content bg-grey">
            <div class="modal-body">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link active py-1 active-tab text-white add-alert-tab" data-toggle="tab" href="#alerts">
                            <span class="d-block d-sm-none"><i class="fa fa-line-chart"></i></span>
                            <span class="d-none d-sm-block">Indicator Alerts</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link py-1 active-tab text-white" data-toggle="tab" href="#price">
                            <span class="d-block d-sm-none"><i class="fa fa-dollar"></i></span>
                            <span class="d-none d-sm-block">Price Alerts</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link py-1 active-tab text-white remove-alert-tab" data-toggle="tab" href="#remove">
                            <span class="d-block d-sm-none"><i class="fa fa-trash"></i></span>
                            <span class="d-none d-sm-block">Remove Alerts</span>
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <br />
                    <div class="tab-pane container active" id="alerts">
                        <div class="form-group row">
                            <label for="ddlCharts" class="col-sm-2 col-form-label text-d9d9d9">Indicator</label>
                            <div class="col-sm-10">
                                <select id="ddlIndicators" class="form-control bg-12161c text-d9d9d9">
                                    <option value="abands-14">Acceleration Bands (14)</option>
                                    <option value="bb-20">Bollinger Bands (14)</option>
                                    <option value="dema-9">DEMA (9)</option>
                                    <option value="ema-9">EMA (9)</option>
                                    <option value="linearRegression-14">Linear Regression (14)</option>
                                    <option value="linearRegressionIntercept-14">Linear Regression Intercept (14)</option>
                                    <option value="pivotpoints-28">Pivot Points (28)</option>
                                    <option value="sma-14">SMA (14)</option>
                                    <option value="supertrend-10">Supertrend (10)</option>
                                    <option value="tema-9">TEMA (9)</option>
                                    <option value="vwap-30">VWAP (30)</option>
                                    <option value="wma-9">WMA (9)</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="ddlConditions" class="col-sm-2 col-form-label text-d9d9d9">Condition</label>
                            <div class="col-sm-10">
                                <select id="ddlIndicatorConditions" class="form-control bg-12161c text-d9d9d9">
                                    <option value="0">Crossing</option>
                                    <option value="1">Crossing up</option>
                                    <option value="2">Crossing down</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="txtDate" class="col-sm-2 col-form-label text-d9d9d9">Expiry</label>
                            <div class="col-sm-10">
                                <div class="wrapper-alert">
                                    <input type="text" id="txtIndicatorDate" class="form-control bg-12161c text-d9d9d9 alert-date">
                                    <input type="date" class="alert-picker">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row float-right">
                            <button class="btn btn-light btn-add-alert mr-3" data-type="0" type="button">Add</button>
                        </div>
                    </div>
                    <div class="tab-pane container fade" id="price">
                        <div class="form-group row">
                            <label for="ddlConditions" class="col-sm-2 col-form-label text-d9d9d9">Condition</label>
                            <div class="col-sm-10">
                                <select id="ddlPriceConditions" class="form-control bg-12161c text-d9d9d9">
                                    <option value="0">Crossing</option>
                                    <option value="1">Crossing up</option>
                                    <option value="2">Crossing down</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="txtValue" class="col-sm-2 col-form-label text-d9d9d9">Value</label>
                            <div class="col-sm-10">
                                <input type="number" step="0.1" id="txtPriceValue" class="form-control bg-12161c text-d9d9d9">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="txtDate" class="col-sm-2 col-form-label text-d9d9d9">Expiry</label>
                            <div class="col-sm-10">
                                <div class="wrapper-date">
                                    <input type="text" id="txtPriceDate" class="form-control bg-12161c text-d9d9d9 expiry-date">
                                    <input type="date" class="expiry-picker">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row float-right">
                            <button class="btn btn-light btn-add-alert mr-3" data-type="1" type="button">Add</button>
                        </div>
                    </div>
                    <div class="tab-pane container fade" id="remove">
                        <div class="alerts-wrapper row">
                            <div class="alerts-list">
                                <ul class="list-group remove-alerts">
                                </ul>
                            </div>
                            <div class="alerts-remove-params px-2">
                                <div class="mx-4">
                                    <div class="remove-options"></div>
                                    <div class="form-group text-right">
                                        <button class="btn btn-light btn-remove-alert mr-2">Remove</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- <div class="modal fade" id="winner-modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content bg-transparent max-width-0">
            <div class="ml-auto mr-auto">
                <table cellpadding="0" cellspacing="0" border="0">
                    <tbody>
                        <tr>
                            <td width="438" height="582" class="the_wheel" align="center" valign="center">
                                <canvas id="canvas" width="434" height="434" data-responsiveMinWidth="180" data-responsiveScaleHeight="true" data-responsiveMargin="50">
                                    <p style="color: white; text-align:center">Sorry, your browser doesn't support canvas. Please try another.</p>
                                </canvas>
                            </td>
                    </tbody>
                </table>
                <div class="power_controls ml-auto mr-auto text-center">
                    <button type="button" id="spin_button" class="btn btn-primary font-size-26">SPIN</button>
                    <br /><br /> &nbsp;&nbsp;
                </div>
            </div>
        </div>
    </div>
</div> -->
<?php $this->load->view('trading/trade/trade_footer'); ?>
<script src="<?= base_url('assets/user_panel/js/common.js?v=1') ?>"></script>
<!-- <script src="<?= base_url('assets/user_panel/libs/wheel/wheel.js') ?>"></script> -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    // $(window).ready(function() {
    //     let is_play = "<?= $is_play ?>";
    //     if (is_play == 0) {
    //         $('#winner-modal').modal("show");
    //     }
    // });   
</script>
<script>
    var index = 0;
    var slides = document.querySelectorAll(".slides");
    function changeSlide() {
        if (index < 0) {
            index = slides.length - 1;
        }
        if (index > slides.length - 1) {
            index = 0;
        }
        for (let i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";         
        }
        slides[index].style.display = "block";        
        index++;
        setTimeout(changeSlide, 4000);
    }
    changeSlide();
</script>
