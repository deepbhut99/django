<?php $this->load->view('trading/common/header'); ?>
<link rel="stylesheet" href="<?= base_url('assets/user_panel/css/pagination.css') ?>">
<div ng-controller="Tradingbot_Controller" ng-init="get_user_info()">
    <div class="page-content p-mobile pl-0 pr-0">
        <div class="container-fluid p-mobile pl-0 pr-0">
            <div class="card border-radius-0 h-45px">
                <div class="card-body pb-0 pt-0 pl-0">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs nav-tabs-custom border-bottom-0" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link pt-12px pb-12px bg-transparent p-portfolio active" data-toggle="tab" href="#mystat" role="tab">
                                <span class="d-block d-sm-none"><i class="fas fa-user-edit"></i></span>
                                <span class="d-none d-sm-block">My status</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link bg-transparent p-community" data-toggle="tab" href="#community" role="tab">
                                <span class="d-block d-sm-none">
                                    <img style="height: 30px !important" src="<?= base_url('assets/user_panel/images/coins/BNBUSDT.png') ?>" alt="binance">
                                </span>
                                <span class="d-none d-sm-block">
                                    <img style="height: 25px !important" src="<?= base_url('assets/user_panel/images/coins/BNBUSDT.png') ?>" alt="binance">
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link pt-12px pb-12px bg-transparent p-portfolio" data-toggle="tab" href="#portfolio" role="tab">
                                <span class="d-block d-sm-none"><i class="fa fa-folder-open"></i></span>
                                <span class="d-none d-sm-block" id="history">History</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane p-3 active" id="mystat" role="tabpanel">
                    <div class="row justify-content-center">
                        <div class="col-lg-4">
                            <div class="card w-100 theme-border">
                                <img class="reward-badge-img5" src="<?= base_url('assets/user_panel/images/banner/reward5.png') ?>" alt="">
                                <div class="reward-badge">Total Profit</div>
                                <div class="row justify-content-around">
                                    <div class="col-6 text-center">
                                        <div class="today-profit p-1.25">
                                            <span class="profit-head">Today's Profit(USDT)</span>
                                            <h2 class="profit-amt"><?php if (isset($totalprofittoday)) {
                                                                        echo (number_format($totalprofit, 3));
                                                                    } else {
                                                                        echo (0);
                                                                    } ?></h2>
                                        </div>
                                    </div>
                                    <div class="col-6 text-center">
                                        <div class="total-profit p-1.25">
                                            <span class="profit-head">Total Profit(USDT)</span>
                                            <h2 class="profit-amt"><?php if (isset($totalprofit)) {
                                                                        echo (number_format($totalprofit, 3));
                                                                    } else {
                                                                        echo (0);
                                                                    } ?></h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <a href="<?= base_url('direct-referral') ?>">
                                <div class="card w-100 theme-border">
                                    <img class="reward-badge-img2" src="<?= base_url('assets/user_panel/images/banner/reward2.png') ?>" alt="">
                                    <div class="reward-badge">Direct Referral</div>
                                    <div class="row justify-content-around">
                                        <div class="col-6 text-center">
                                            <div class="today-profit p-1.25">
                                                <span class="profit-head">Today's Profit(USDT)</span>
                                                <h2 class="profit-amt"><?php if (isset($directrefincomtoday)) {
                                                                            echo ($directrefincomtoday);
                                                                        } else {
                                                                            echo (0);
                                                                        } ?></h2>
                                            </div>
                                        </div>
                                        <div class="col-6 text-center">
                                            <div class="total-profit p-1.25">
                                                <span class="profit-head">Total Profit(USDT)</span>
                                                <h2 class="profit-amt"><?php if (isset($directrefincomtotal)) {
                                                                            echo ($directrefincomtotal);
                                                                        } else {
                                                                            echo (0);
                                                                        } ?></h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4">
                            <a href="<?= base_url('team-income') ?>">
                                <div class="card w-100 theme-border">
                                    <img class="reward-badge-img3" src="<?= base_url('assets/user_panel/images/banner/reward3.png') ?>" alt="">
                                    <div class="reward-badge">Team Income</div>
                                    <div class="row justify-content-around">
                                        <div class="col-6 text-center">
                                            <div class="today-profit p-1.25">
                                                <span class="profit-head">Today's Profit(USDT)</span>
                                                <h2 class="profit-amt"><?php if (isset($teammember)) {
                                                                            echo ($teammember);
                                                                        } else {
                                                                            echo (0);
                                                                        } ?></h2>
                                            </div>
                                        </div>
                                        <div class="col-6 text-center">
                                            <div class="total-profit p-1.25">
                                                <span class="profit-head">Total Profit(USDT)</span>
                                                <h2 class="profit-amt"><?php if (isset($teammembertoatal)) {
                                                                            echo ($teammembertoatal);
                                                                        } else {
                                                                            echo (0);
                                                                        } ?></h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4">
                            <a href="<?= base_url('team-trading') ?>">
                                <div class="card w-100 theme-border">
                                    <img class="reward-badge-img4" src="<?= base_url('assets/user_panel/images/banner/reward4.png') ?>" alt="">
                                    <div class="reward-badge">Team Trading</div>
                                    <div class="row justify-content-around">
                                        <div class="col-6 text-center">
                                            <div class="today-profit p-1.25">
                                                <span class="profit-head">Today's Profit(USDT)</span>
                                                <h2 class="profit-amt"><?= isset($todayTeamTradingIncome) ? number_format($todayTeamTradingIncome, 6) : 0 ?></h2>
                                            </div>
                                        </div>
                                        <div class="col-6 text-center">
                                            <div class="total-profit p-1.25">
                                                <span class="profit-head">Total Profit(USDT)</span>
                                                <h2 class="profit-amt"><?= isset($totalTeamTtradingIncome) ? number_format($totalTeamTtradingIncome, 6) : 0 ?></h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4">
                            <a href="<?= base_url('personal-profit') ?>">
                                <div class="card w-100 theme-border">
                                    <img class="reward-badge-img1" src="<?= base_url('assets/user_panel/images/banner/reward1.png') ?>" alt="">
                                    <div class="reward-badge">Personal Profit</div>
                                    <div class="row justify-content-around">
                                        <div class="col-6 text-center">
                                            <div class="today-profit p-1.25">
                                                <span class="profit-head">Today's Profit(USDT)</span>
                                                <h2 class="profit-amt"><?php if (isset($todasprofitforbot)) {
                                                                            echo ($todasprofitforbot);
                                                                        } else {
                                                                            echo (0);
                                                                        } ?></h2>
                                            </div>
                                        </div>
                                        <div class="col-6 text-center">
                                            <div class="total-profit p-1.25">
                                                <span class="profit-head">Total Profit(USDT)</span>
                                                <h2 class="profit-amt"><?php if (isset($tptalprofitforbot)) {
                                                                            echo ($tptalprofitforbot);
                                                                        } else {
                                                                            echo (0);
                                                                        } ?></h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="community" role="tabpanel">

                </div>
                <div class="tab-pane p-0" id="portfolio" role="tabpanel">
                    <div class="tab-pane p-3" id="copyhistory" role="tabpanel">
                        <div>
                            <form>
                                <div class="row">
                                    <div class="col-xl-7 col-lg-7 col-md-6 col-sm-4"></div>
                                    <div class="col-xl-5 col-lg-5 col-md-6 col-sm-8">
                                        <div class="wrapper-new d-flex">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="card theme-border" style="border-radius: 20px;">
                            <div class="card-body">
                                <div class="ml-auto mr-auto text-center d-none" id="historyLoader">
                                    <img src="<?= base_url('assets/user_panel/images/preloader.gif') ?>" alt="loader">
                                </div>
                                <table id="datatable" class="table dt-responsive nowrap table-striped table-borderless" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>Sr.No</th>
                                            <th>OrderId</th>
                                            <th>Date</th>
                                            <th>Buy|Sell</th>
                                            <th>Symbol</th>
                                            <th>Price</th>
                                            <th>Qunatity</th>
                                            <th>Total</th>
                                            <th>Profit</th>
                                            <th>fees</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div> <!-- container-fluid -->
    </div>

    <?php $this->load->view('trading/common/footer'); ?>


</div>
<script>
    $(".pagination-item-copy").on("click", function() {
        $(".pagination-item-copy").removeClass("active");
        $(this).addClass("active");
    });
</script>

<script>
    $(document).ready(function() {
        $('table.display').DataTable();
    });
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust()
            .responsive.recalc();
    });
</script>

<script>
    var table_history = $('#datatable').DataTable({
        destroy: true,
        pagingType: "simple",
        order: [
            [0, "asc"]
        ]
    }).clear().draw();
    var tableContainer = $(table_history.table().container());
    tableContainer.css('display', 'none');
    $('#historyLoader').addClass('d-block');
    console.log($('#startdate').val(), $('#enddate').val());
    $.ajax({
        type: 'post',
        url: '<?= base_url("trade/trading/bot_history") ?>',
        data: {
            start: $('#startdate').val(),
            end: $('#enddate').val()
        },
        success: function(res) {

            res = JSON.parse(res);
            count = 1;
            res.forEach(function(bid) {
                var totalt = bid.price * bid.origQty;

                table_history.row.add([count++, bid.id_whichsellorbuy, moment(bid.created_datetime * 1000).format('DD-MM-YYYY hh:mm:ss A'), bid.buy_or_sell == 1 ? '<span class="text-02C076">Buy</span>' : '<span class="text-danger">Sell</span>', bid.symbol, bid.price, bid.origQty, totalt.toFixed(2), bid.price_with_commision, bid.qty_with_commision, ]).draw(false);
            })
            $('#historyLoader').removeClass('d-block');
            tableContainer.css('display', 'block');
        }
    })
</script>
<script>
    $(document).ready(function() {
        $('.myslider').owlCarousel({
            items: 1,
            nav: false,
            dots: true,
            dotsData: true,
            autoplay: true,
            autoplayTimeout: 3000,
            loop: true,
            autoplayHoverPause: true
        });
    });
</script>

<script>
    function botnotpurchase() {
        toastr.error('Add API', '', toast);
    }

    function invalidwallet() {
        toastr.error("Insufficient Fund")
    }
</script>