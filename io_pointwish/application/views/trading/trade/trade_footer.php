</div>
<!-- end main content-->
</div>
<!-- END layout-wrapper -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>

<!-- JAVASCRIPT -->
<!-- <script src="<?= base_url('assets/user_panel/js/jquery.validate.js'); ?>"></script> -->
<script src="<?= base_url('assets/user_panel/libs/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>
<script src="<?= base_url('assets/user_panel/libs/metismenu/metisMenu.min.js') ?>"></script>
<script src="<?= base_url('assets/user_panel/libs/simplebar/simplebar.min.js') ?>"></script>
<script src="<?= base_url('assets/user_panel/libs/node-waves/waves.min.js') ?>"></script>


<!-- Required datatable js -->
<script src="<?= base_url('assets/user_panel/libs/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<!-- <script src="<?= base_url('assets/user_panel/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js') ?>"></script> -->

<!-- Responsive examples -->
<!-- <script src="<?= base_url('assets/user_panel/libs/datatables.net-responsive/js/dataTables.responsive.min.js') ?>"></script>
<script src="<?= base_url('assets/user_panel/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') ?>"></script> -->

<!-- Sweet Alerts js -->
<script src="<?= base_url('assets/user_panel/libs/sweetalert2/sweetalert2.min.js') ?>"></script>

<!-- owlcarousal js -->
<script src="<?= base_url('assets/user_panel/js/owl.carousel.js') ?>"></script>

<!-- Sweet alert init js-->
<script src="<?= base_url('assets/user_panel/js/pages/sweet-alerts.init.js') ?>"></script>

<script src="<?= base_url('assets/user_panel/js/multislider.min.js') ?>"></script>


<!-- <script src="<?= base_url('assets/user_panel/libs/morris.js/morris.min.js') ?>"></script>
<script src="<?= base_url('assets/user_panel/libs/raphael/raphael.min.js') ?>"></script> -->

<script src="<?= base_url('assets/user_panel/libs/toastr/toastr.min.js') ?>"></script>

<script src="<?= base_url('assets/user_panel/js/app.js') ?>"></script>
</body>

</html>

<script>
    document.onkeydown = function(e) {
        if (e.keyCode == 123) {
            return false;
        }
        if (e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)) {
            return false;
        }
        if (e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)) {
            return false;
        }
        if (e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)) {
            return false;
        }

        if (e.ctrlKey && e.shiftKey && e.keyCode == 'C'.charCodeAt(0)) {
            return false;
        }
    }

    $("html").on("contextmenu", function() {
        return false;
    });
</script>

<?php
if ($this->session->flashdata('error_message')) {
?>
    <script type="text/javascript">
        $(document).ready(function() {
            Swal.fire({
                title: "Error!",
                text: "<?php echo $this->session->flashdata('error_message'); ?>",
                type: "error",
                confirmButtonClass: "btn btn-success mt-2",
                buttonsStyling: !1
            });

        });
    </script>
<?php
}
?>

<?php
if ($this->session->flashdata('warning_message')) {
?>
    <script type="text/javascript">
        $(document).ready(function() {
            Swal.fire({
                title: "Warning!",
                text: "<?php echo $this->session->flashdata('warning_message'); ?>",
                type: "warning",
                confirmButtonClass: "btn btn-success mt-2",
                buttonsStyling: !1
            });
        });
    </script>
<?php
}
?>

<?php
if ($this->session->flashdata('success_message')) {
?>
    <script type="text/javascript">
        $(document).ready(function() {
            Swal.fire({
                title: "Done!",
                text: "<?php echo $this->session->flashdata('success_message'); ?>",
                type: "success",
                confirmButtonClass: "btn btn-success mt-2",
                buttonsStyling: !1
            });
        });
    </script>
<?php
}
?>
<script>
    var BASE_URL = "<?php echo base_url(); ?>";

    function openNav() {
        $('#mySidenav').removeClass('d-none');
        $('#mySidenav').addClass('d-block');
    }

    function closeNav() {
        $('#mySidenav').removeClass('d-block');
        $('#mySidenav').addClass('d-none');
    }

    $(document).mouseup(function(e) {

        var container = $("#mySidenav");

        if (!container.is(e.target) && container.has(e.target).length === 0) {
            container.removeClass('d-block');
            container.addClass('d-none');
        }
    });
</script>

<script>
    $(document).on("click", function(event) {
        var $trigger = $("#vertical-menu-btn");
        if ($trigger !== event.target && !$trigger.has(event.target).length) {
            $(".sidebar-close").removeClass('sidebar-enable');
        }
    });
</script>