<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>pwt: <?php if (!empty($title)) echo $title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta content="<?= $title ?>" name="description" />
    <meta http-equiv="Content-Security-Policy" content="default-src data: gap://* file://* https://ssl.gstatic.com *; img-src 'self' * data:; style-src 'self' 'unsafe-inline' *; script-src 'self' 'unsafe-eval' 'unsafe-inline' *; connect-src 'self' * ws://* wss://*;">


    <script src="<?= base_url('assets/user_panel/libs/jquery/jquery.min.js') ?>"></script>

    <!-- App favicon -->
    <link rel="shortcut icon" href="<?= base_url('assets/user_panel/images/logo/favicon.ico') ?>">

    <link href="<?= base_url('assets/user_panel/libs/chartist/chartist.min.css') ?>" rel="stylesheet">

    <!-- Sweet Alert-->
    <link href="<?= base_url('assets/user_panel/libs/sweetalert2/sweetalert2.min.css') ?>" rel="stylesheet" type="text/css" />

    <!-- Bootstrap Css -->
    <?php
    if ($title != 'Tradding') {
    ?>
        <!-- <link href="<?= base_url('assets/user_panel/css/bootstrap.min.css') ?>" id="bootstrap-style" rel="stylesheet" type="text/css" /> -->
    <?php
    }
    ?>

    <!-- owlCarousel css -->
    <link rel="stylesheet" href="<?= base_url('assets/user_panel/css/owl.carousel.min.css') ?>">

    <!-- Icons Css -->
    <!-- <link href="<?= base_url('assets/user_panel/css/icons.min.css') ?>" rel="stylesheet" type="text/css" /> -->

    <!-- Pricing table scss-->
    <!-- <link href="<?= base_url('assets/user_panel/pricing-tables.css') ?>" rel="stylesheet" type="text/css" /> -->

    <!-- App Css-->
    <link href="<?= base_url('assets/user_panel/css/app.min.css?v=2') ?>" rel="stylesheet" type="text/css" />

    <!-- Winer page css -->
    <link href="<?= base_url('assets/user_panel/css/winner.css') ?>" rel="stylesheet" type="text/css" />

    <!-- Toastr css -->
    <link href="<?= base_url('assets/user_panel/libs/toastr/toastr.min.css') ?>" rel="stylesheet" type="text/css" />

    <!-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->

    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css">


    <!-- <script src="<?= base_url('assets/user_panel/js/jquery-ui.min.js') ?>"></script> -->

    <script src="<?= base_url('assets/user_panel/js/moment.min.js') ?>"></script>

    <script>
        var BASE_URL = "<?php echo base_url(); ?>";
    </script>
    <?php
    $session_data = getSessionData();
    $amount = getWalletAmountByUserID($this->session->user_id);
    $live_amount = $amount[0];
    $demo_amount = $amount[1];
    ?>
    <script>
        sessionStorage.clear();
        sessionStorage.setItem('live-amount', <?= $live_amount; ?>);
        sessionStorage.setItem('demo-amount', <?= $demo_amount; ?>);

        var toast = {
            timeOut: 1e3,
            closeButton: !0,
            debug: !1,
            newestOnTop: !0,
            progressBar: !0,
            positionClass: "toast-top-right",
            preventDuplicates: !0,
            onclick: null,
            showDuration: "300",
            hideDuration: "1000",
            extendedTimeOut: "1000",
            showEasing: "swing",
            hideEasing: "linear",
            showMethod: "fadeIn",
            hideMethod: "fadeOut",
            tapToDismiss: !1
        }
    </script>
</head>
<div id="preloader"></div>

<body data-sidebar="dark" class="sidebar-close vertical-collpsed">
    <!-- Begin page -->
    <div id="layout-wrapper">
        <header id="page-topbar" class="text-white" style="background-color: #12003c!important;">
            <div class="preload-stopper navbar-header h-55px">
                <div class="d-flex">
                    <!-- LOGO -->
                    
                    <button type="button" class="btn btn-sm px-3 font-size-24 header-item" id="vertical-menu-btn">
                        <i class="fa fa-bars"></i>
                    </button>
                    <h4 class="header-page-name title-trade"><?php echo $title1 ?></h4>
                </div>

                <div class="d-flex">

                    <div class="dropdown d-inline-block">
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary btn-demo mt-xsm-1 mt-2 w-auto text-nowrap" id="btn-amount" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                <div class="media-body">
                                    <div class="font-size-12 text-muted">
                                        <p class="mb-0 text-white text-left"><span class='account-label text-nowrap'><?= ($this->session->trade_type == 0 ? 'Live Account' : 'Demo Account'); ?></span>&nbsp;&nbsp;&nbsp;<i class="fa fa-chevron-down float-right"></i></p>
                                    </div>
                                    <h5 class="mt-0 mb-0 h-demo text-nowrap"><span class='account-amount'></span> USDT</h5>

                                </div>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right pl-3 pr-3 text-nowrap">
                                <div class="d-flex">
                                    <div class="mb-auto mt-auto"><input class="hw-20 account-radio" type="radio" id="live" name="acc" value="live" data-label='Live Account' <?= ($this->session->trade_type == 0 ? 'checked' : ''); ?>></div>
                                    <div class="pl-3"><label for="live"><span class="font-size-12 text-light">Live Account</span><br><span class="font-size-18" id='live-amount'></span> USDT</label></div>
                                </div>
                                <div class="d-flex">
                                    <div class="mb-auto mt-auto"><input class="hw-20 account-radio" type="radio" id="demo" name="acc" value="demo" data-label='Demo Account' <?= ($this->session->trade_type == 1 ? 'checked' : ''); ?>></div>
                                    <div class="pl-3"><label for="demo"><span class="font-size-12 text-light">Demo Account</span><br><span class="font-size-18" id='demo-amount'></span> USDT</label><i class="fa fa-refresh text-white font-size-18 refresh-demo-wallet" aria-hidden="true"></i></div>
                                </div>

                            </div>
                        </div>
                    </div>


                    <div class="dropdown d-none d-lg-inline-block">
                        <button type="button" class="btn header-item noti-icon" data-toggle="fullscreen">
                            <i class="fa fa-arrows-alt"></i>
                        </button>
                    </div>

                    <div id="main">

                        <span class="pl-0 pr-2" onclick="openNav()">
                            <i class="fa fa-ellipsis-v header-more-btn" aria-hidden="true"></i>
                        </span>
                        <div class="dropdown d-inline-block">
                            <button type="button" class="btn header-item p-0" id="page-header-user-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <h4 class="profile-font text-uppercase" style="color:	#12003c!important;"><?= substr($session_data->username, 0, 1) ?></h4>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right min-width-8rem">
                                <!-- item-->
                                <h5 class="px-3 text-ffba00 text-capitalize" style="color: #ffba00 !important;"><?= $session_data->username ?></h5>
                                <a class="dropdown-item" style="color: #9ca8b3 !important;" href="<?= base_url('account') ?>"><i style="color: #9ca8b3 !important;" class="fa fa-user-circle font-size-17 align-middle mr-1"></i> Profile</a>
                                <a class="dropdown-item" style="color: #9ca8b3 !important;" href="<?= base_url('logout') ?>"><i style="color: #9ca8b3 !important;" class="fa fa-power-off font-size-17 align-middle mr-1"></i> Logout</a>
                            </div>
                        </div>
                    </div>

                    <div id="mySidenav" class="sidenav d-none">

                       
                            <div class="card bg-12161c mb-0 border-none">
                                <div class="card-body pt-0 pl-3 pr-3">
                                    <ul class="nav nav-tabs nav-tabs-custom" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-toggle="tab" href="#open" role="tab" aria-selected="true">
                                                <span style="color: #02C076 !important">Open</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#close" role="tab" aria-selected="false">
                                                <span style="color :#D9304E !important">Close</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                                        </li>
                                    </ul>

                                    <div class="tab-content">
                                        <div class="tab-pane pt-2 active" id="open" role="tabpanel"></div>
                                        <div class="tab-pane pt-2" id="close" role="tabpanel"></div>
                                    </div>
                                </div>
                            </div>
                        
                    </div>
                </div>
            </div>
        </header>
        <!-- ========== Left Sidebar Start ========== -->
        <div class="preload-stopper position-fixed vertical-menu mt-n16px bg-grdnt-1">
            <div data-simplebar class="h-100">
                <!--- Sidemenu -->
                <div id="sidebar-menu">
                    <!-- Left Menu Start -->
                    <ul class="metismenu list-unstyled mt-16px" id="side-menu">
                        <li>
                            <a href="<?= base_url('trading') ?>" data-id="trade" id="trade" class="waves-effect">
                                <i class="fas fa-chart-bar"></i>
                                <span>Trading</span>
                            </a>
                        </li>                        

                        <li>
                            <a href="<?= base_url('trade-dashboard') ?>" class="waves-effect">
                                <i class="fa fa-tachometer"></i>
                                <span><?= lang('dashboard') ?></span>
                            </a>
                        </li>

                        <li>
                            <a href="<?= base_url('copytrade') ?>" class="waves-effect">
                                <i class="fa fa-clone" aria-hidden="true"></i>
                                <span>Copy Trade</span>
                            </a>
                        </li>
                       
                        <!--<li>-->
                        <!--    <a href="<?= base_url('trade/draw') ?>" data-id="draw" id="draw" class="waves-effect">-->
                        <!--        <i class="dripicons-ticket"></i>-->
                        <!--        <span><?= lang('lucky_draw') ?></span>-->
                        <!--    </a>-->
                        <!--</li>-->

						<li>
                            <a href="<?= base_url('bot-trading') ?>" data-id="trade" id="trade" class="waves-effect">
                                <i class="fa fa-robot"></i>
                                <span><?= lang('bot') ?></span>
                            </a>
                        </li>

                        <li>
                            <a href="<?= base_url('wallet') ?>" data-id="wallet" id="wallet" class="waves-effect">
                                <i class="fa fa-credit-card"></i>
                                <span><?= lang('wallet') ?></span>
                            </a>
                        </li>

                        <li>
                            <a ng-click="get_auth_status()" href="<?= base_url('account') ?>" data-id="profile" id="profile" class="waves-effect">
                                <i class="fas fa-user-tie"></i>
                                <span><?= lang('profile') ?></span>
                            </a>
                        </li>

                        <li>
                            <a href="<?= base_url('affiliate') ?>" data-id="affiliate" id="affiliate" class="waves-effect">
                                <i class="fa fa-share-alt"></i>
                                <span><?= lang('affiliate') ?></span>
                            </a>
                        </li>

                        <?php if ($session_data->is_agency == 1) { ?>
                            <li>
                                <a href="<?= base_url('agency') ?>" data-id="agency" id="agency" class="waves-effect">
                                    <i class="far fa-building" aria-hidden="true"></i>
                                    <span>Agency</span>
                                </a>
                            </li>
                        <?php } ?>
                        <?php if (get_total_deposit() >= 10  || $session_data->is_agency == 1) { ?>
                            <li>
                                <a href="<?= base_url('exchange') ?>" data-id="exchange" id="exchange" class="waves-effect">
                                    <i class="fa fa-exchange"></i>
                                    <span><?= lang('exchange') ?></span>
                                </a>
                            </li>
                        <?php } ?>

                        <li>
                            <a href="<?= base_url('history') ?>" data-id="history" id="history" class="waves-effect">
                                <i class="fa fa-history"></i>
                                <span><?= lang('history') ?></span>
                            </a>
                        </li>

                        <li>
                            <a href="<?= base_url('ticket') ?>" data-id="ticket" id="ticket" class="waves-effect">
                                <i class="fa fa-ticket"></i>
                                <span><?= lang('ticket') ?></span>
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- Sidebar -->
            </div>
        </div>
        <!-- Left Sidebar End -->
        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="main-content">
