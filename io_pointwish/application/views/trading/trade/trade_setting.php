<?php $this->load->view('trading/common/header'); ?>
<div class="page-content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-3 col-lg-1"></div>
            <div class="col-xl-6 col-lg-10">
                <div class="row">
                    <div class="card theme-border w-100 mb-3">
                        <div class="card-body p-trade-card">
                            <div class="row">
                                <div class="col-6 mt-auto mb-auto">
                                    <h5>First Buy in Amount</h5>
                                </div>
                                <div class="col-6">
                                    <div class="d-flex">
                                        <input class="form-control ml-auto form-trade w-70px" type="number" value="10">
                                        <span class="mt-auto mb-auto">USDT</span>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="card theme-border w-100 mb-3">
                        <div class="card-body p-trade-card">
                            <div class="row">
                                <div class="col-6 mt-auto mb-auto">
                                    <h5>Open Position doubled</h5>
                                </div>
                                <div class="col-6">
                                    <div class="text-right mt-1">
                                        <input type="checkbox" hidden="hidden" id="username">
                                        <label class="switch" for="username"></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card theme-border w-100 mb-3">
                        <div class="card-body p-trade-card">
                            <div class="row border-bottom-trade py-1">
                                <div class="col-6 mt-auto mb-auto">
                                    <h5>Margin call limit</h5>
                                </div>
                                <div class="col-6">
                                    <div class="d-flex">
                                        <input class="form-control ml-auto form-trade w-70px" type="number" value="7">
                                        <span class="mt-auto mb-auto">Time</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row border-bottom-trade py-1">
                                <div class="col-6 mt-auto mb-auto">
                                    <h5>Whole position take profit ratio</h5>
                                </div>
                                <div class="col-6">
                                    <div class="d-flex">
                                        <input class="form-control ml-auto form-trade w-70px" type="number" value="1.3">
                                        <span class="mt-auto mb-auto">%</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row border-bottom-trade py-1">
                                <div class="col-6 mt-auto mb-auto">
                                    <h5>Whole position take profit callback</h5>
                                </div>
                                <div class="col-6">
                                    <div class="d-flex">
                                        <input class="form-control ml-auto form-trade w-70px" type="number" value="0.3">
                                        <span class="mt-auto mb-auto">%</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row border-bottom-trade py-1">
                                <div class="col-6 mt-auto mb-auto">
                                    <h5>Margin Configuration</h5>
                                </div>
                                <div class="col-6 mt-auto mb-auto">
                                    <div class="text-right">
                                        <i class="py-0.65rem fas fa-chevron-right"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="row border-bottom-trade py-1">
                                <div class="col-6 mt-auto mb-auto">
                                    <h5>Buy in Callback</h5>
                                </div>
                                <div class="col-6">
                                    <div class="d-flex">
                                        <input class="form-control ml-auto form-trade w-70px" type="number" value="0.5">
                                        <span class="mt-auto mb-auto">%</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row border-bottom-trade py-1">
                                <div class="col-6 mt-auto mb-auto">
                                    <h5>Distributed and Take Profit Allocation</h5>
                                </div>
                                <div class="col-6">
                                    <div class="text-right">
                                        <i class="py-0.65rem fas fa-chevron-right"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="row py-1">
                                <div class="col-6 mt-auto mb-auto">
                                    <h5>Sub-position take-profit callback</h5>
                                </div>
                                <div class="col-6">
                                    <div class="d-flex">
                                        <input class="form-control ml-auto form-trade w-70px" type="number" value="0.3">
                                        <span class="mt-auto mb-auto">%</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 pl-0">
                        <button class="btn btn-dark btn-block">Reset</button>
                    </div>
                    <div class="col-6 pr-0">
                        <button class="btn start-btn btn-primary btn-block">Save</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-1"></div>
    </div>
</div>
</div>
<?php $this->load->view('trading/common/footer'); ?>