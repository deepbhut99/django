<?php $this->load->view('trading/common/header'); ?>
<div class="page-content">
    <div class="container-fluid p-0">
        <section class="mt-14px">
            <div class="container p-0">
                <div class="row medium-padding100">
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 mb30 pr-lg-0" data-mh="pricing-item">
                        <div class="crumina-module crumina-pricing-table pricing-table--style1 mb-5" id="accordion">
                            <div class="pricing-thumb"></div>
                            <h5 class="mb-0 pricing-title text-gold"><?= lang('daily') ?></h5>
                            <span class="badge mb-2">( <?= date('d M, Y', strtotime("-1 days")); ?> )</span>
                            <?php $count = 1;
                            foreach ($daily as $row) { ?>
                                <div class="daily__item winner-accordion">
                                    <div class="daily__title" id="id<?= $row['id'] ?>">
                                        <div class="price-value-winner text-capitalize"><?= $count++ . '. ' . $row['username'] ?><span class="reward-text">[+ <?= $row['amount'] ?> pwt]</span></div>
                                    </div>
                                    <div class="daily__body">
                                        <ul class="pricing-tables-position">
                                            <li class="position-item">
                                                <div class="currency-details-item">
                                                    <h6 class="title text-capitalize font-size-14"><?= lang('name') ?>:&nbsp;&nbsp;<span class="font-white font-size-16"><?= $row['username'] ?></span></h6>
                                                </div>
                                            </li>
                                            <li class="position-item">
                                                <div class="currency-details-item">
                                                    <h6 class="title font-size-14"><?= lang('daily_revenue') ?>:&nbsp;&nbsp;<span class="font-white font-size-16"><?= $row['win'] ? number_format($row['win'], 2) : 0 ?> pwt</span></h6>
                                                </div>
                                            </li>
                                            <li class="position-item">
                                                <div class="currency-details-item">
                                                    <h6 class="title font-size-14"><?= lang('profit_sharing') ?>:&nbsp;&nbsp;<span class="font-white font-size-16"><?= $row['profit_share'] ?>%</span></h6>
                                                </div>
                                            </li>
                                            <li class="position-item">
                                                <div class="currency-details-item">
                                                    <h6 class="title font-size-14"><?= lang('min_investment') ?>:&nbsp;&nbsp;<span class="font-white font-size-16"><?= $row['min_invest'] ?> pwt</span></h6>
                                                </div>
                                            </li>
                                            <li class="position-item">
                                                <div class="currency-details-item">
                                                    <h6 class="title font-size-14"><?= lang('copiers') ?>:&nbsp;&nbsp;<span class="font-white font-size-16"><?= $row['copiers'] ?></span></h6>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 mb30 pr-lg-0" data-mh="pricing-item">
                        <div class="crumina-module crumina-pricing-table pricing-table--style1 mb-5" id="wcollapse">
                            <div class="pricing-thumb">
                            </div>
                            <h5 class="mb-0 pricing-title text-gold"><?= lang('weekly') ?></h5>
                            <span class="badge mb-2">
                                ( <?= 'Till ' . date('d M, Y', strtotime("sunday last week")); ?> )
                            </span>
                            <?php $wcount = 1;
                            foreach ($weekly as $row) { ?>
                                <div class="weekly__item winner-accordion">
                                    <div class="weekly__title" id="cweekly<?= $row['id'] ?>">
                                        <div class="price-value-winner text-capitalize"><?= $wcount++ . '. ' . $row['username'] ?><span class="reward-text">[+ <?= $row['amount'] ?> pwt]</span></span></div>
                                    </div>
                                    <div class="weekly__body">
                                        <ul class="pricing-tables-position">
                                            <li class="position-item">
                                                <div class="currency-details-item">
                                                    <h6 class="title text-capitalizes font-size-14"><?= lang('name') ?>:&nbsp;&nbsp;<span class="font-white font-size-16"><?= $row['username'] ?></span></h6>
                                                </div>
                                            </li>
                                            <li class="position-item">
                                                <div class="currency-details-item">
                                                    <h6 class="title font-size-14"><?= lang('weekly_revenue') ?>:&nbsp;&nbsp;<span class="font-white font-size-16"><?= $row['win'] ? number_format($row['win'], 2) : 0 ?> pwt</span></h6>
                                                </div>
                                            </li>
                                            <li class="position-item">
                                                <div class="currency-details-item">
                                                    <h6 class="title font-size-14"><?= lang('profit_sharing') ?>:&nbsp;&nbsp;<span class="font-white font-size-16"><?= $row['profit_share'] ?>%</span></h6>
                                                </div>
                                            </li>
                                            <li class="position-item">
                                                <div class="currency-details-item">
                                                    <h6 class="title font-size-14"><?= lang('min_investment') ?>:&nbsp;&nbsp;<span class="font-white font-size-16"><?= $row['min_invest'] ?> pwt</span></h6>
                                                </div>
                                            </li>
                                            <li class="position-item">
                                                <div class="currency-details-item">
                                                    <h6 class="title font-size-14"><?= lang('copiers') ?>:&nbsp;&nbsp;<span class="font-white font-size-16"><?= $row['copiers'] ?></span></h6>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            <?php  } ?>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 mb30 pr-lg-0" data-mh="pricing-item">
                        <div class="crumina-module crumina-pricing-table pricing-table--style1 mb-5" id="mcollapse">
                            <div class="pricing-thumb">
                            </div>
                            <h5 class="mb-0 pricing-title text-gold"><?= lang('monthly') ?></h5>
                            <span class="badge mb-2">
                                ( <?= '01 - ' . Date("d M, Y", strtotime("last day of last month")) ?> )
                            </span>
                            <?php $mcount = 1;
                            foreach ($monthly as $row) { ?>
                                <div class="monthly__item winner-accordion">
                                    <div class="monthly__title" id="monthly<?= $row['id'] ?>">
                                        <div class="price-value-winner text-capitalize"><?= $mcount++ . '. ' . $row['username'] ?><span class="reward-text">[+ <?= $row['amount'] ?> pwt]</span></span></div>
                                    </div>
                                    <div class="monthly__body">
                                        <ul class="pricing-tables-position">
                                            <li class="position-item">
                                                <div class="currency-details-item">
                                                    <h6 class="title text-capitalize font-size-14"><?= lang('name') ?>:&nbsp;&nbsp;<span class="font-white font-size-16"><?= $row['username'] ?></span></h6>
                                                </div>
                                            </li>
                                            <li class="position-item">
                                                <div class="currency-details-item">
                                                    <h6 class="title font-size-14"><?= lang('monthly_revenue') ?>:&nbsp;&nbsp;<span class="font-white font-size-16">$ <?= $row['win'] ? number_format($row['win'], 2) : 0 ?> pwt</span></h6>
                                                </div>
                                            </li>
                                            <li class="position-item">
                                                <div class="currency-details-item">
                                                    <h6 class="title font-size-14"><?= lang('profit_sharing') ?>:&nbsp;&nbsp;<span class="font-white font-size-16"><?= $row['profit_share'] ?>%</span></h6>
                                                </div>
                                            </li>
                                            <li class="position-item">
                                                <div class="currency-details-item">
                                                    <h6 class="title font-size-14"><?= lang('min_investment') ?>:&nbsp;&nbsp;<span class="font-white font-size-16"><?= $row['min_invest'] ?> pwt</span></h6>
                                                </div>
                                            </li>
                                            <li class="position-item">
                                                <div class="currency-details-item">
                                                    <h6 class="title font-size-14"><?= lang('copiers') ?>:&nbsp;&nbsp;<span class="font-white font-size-16"><?= $row['copiers'] ?></span></h6>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <!-- <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 mb30 pr-lg-0" data-mh="pricing-item">
                        <div class="crumina-module crumina-pricing-table pricing-table--style1 mb-5" id="Pcollapse">
                            <div class="pricing-thumb">
                            </div>
                            <h5 class="mb-0 pricing-title text-gold"><?= lang('popular') ?></h5>
                            <span class="badge mb-2">
                                ( <?= lang('life_time') ?> )
                            </span>
                            <?php
                            $pcount = 1;
                            foreach ($popular as $row) { ?>
                                <div class="popular__item winner-accordion">
                                    <div class="popular__title" id="PHead<?= $row['id'] ?>">
                                        <div class="price-value-winner text-capitalize"><?= $pcount++ . '. ' . $row['username']  ?></span></div>
                                    </div>
                                    <div class="popular__body">
                                        <ul class="pricing-tables-position">
                                            <li class="position-item">
                                                <div class="currency-details-item">
                                                    <h6 class="title text-capitalize font-size-14"><?= lang('name') ?>:&nbsp;&nbsp;<span class="font-white font-size-16"><?= $row['username'] ?></span></h6>
                                                </div>
                                            </li>
                                            <li class="position-item">
                                                <div class="currency-details-item">
                                                    <h6 class="title font-size-14"><?= lang('total_revenue') ?>:&nbsp;&nbsp;<span class="font-white font-size-16">$ <?= $row['win'] ? number_format($row['win'], 2) : 0 ?></span></h6>
                                                </div>
                                            </li>
                                            <li class="position-item">
                                                <div class="currency-details-item">
                                                    <h6 class="title font-size-14"><?= lang('profit_sharing') ?>:&nbsp;&nbsp;<span class="font-white font-size-16"><?= $row['profit_share'] ?>%</span></h6>                                                    
                                                </div>
                                            </li>
                                            <li class="position-item">
                                                <div class="currency-details-item">
                                                    <h6 class="title font-size-14"><?= lang('min_investment') ?>:&nbsp;&nbsp;<span class="font-white font-size-16">$ <?= $row['min_invest'] ?></span></h6>
                                                </div>
                                            </li>
                                            <li class="position-item">
                                                <div class="currency-details-item">
                                                    <h6 class="title font-size-14"><?= lang('copiers') ?>:&nbsp;&nbsp;<span class="font-white font-size-16"><?= $row['copiers'] ?></span></h6>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            <?php }  ?>
                        </div>
                    </div> -->
                </div>
            </div>
        </section>
    </div>
    <!-- container-fluid -->
</div>
<?php $this->load->view('trading/common/footer'); ?>
<script>
    $(".popular__title.active").next().slideDown();
    $(".popular__title").on("click", function() {
        if ($(this).hasClass('active')) {
            $(this).removeClass("active").next().slideUp();
        } else {
            $(".popular__title.active").removeClass("active").next(".popular__body").slideUp();
            $(this).addClass('active').next('.popular__body').slideDown();
        }
    });
    $(".popular__body").on("click", function() {
        $(".popular__title.active").removeClass("active").next(".popular__body").slideUp();
    });

    $(".monthly__title.active").next().slideDown();
    $(".monthly__title").on("click", function() {
        if ($(this).hasClass('active')) {
            $(this).removeClass("active").next().slideUp();
        } else {
            $(".monthly__title.active").removeClass("active").next(".monthly__body").slideUp();
            $(this).addClass('active').next('.monthly__body').slideDown();
        }
    });
    $(".monthly__body").on("click", function() {
        $(".monthly__title.active").removeClass("active").next(".monthly__body").slideUp();
    });

    $(".weekly__title.active").next().slideDown();
    $(".weekly__title").on("click", function() {
        if ($(this).hasClass('active')) {
            $(this).removeClass("active").next().slideUp();
        } else {
            $(".weekly__title.active").removeClass("active").next(".weekly__body").slideUp();
            $(this).addClass('active').next('.weekly__body').slideDown();
        }
    });
    $(".weekly__body").on("click", function() {
        $(".weekly__title.active").removeClass("active").next(".weekly__body").slideUp();
    });

    $(".daily__title.active").next().slideDown();
    $(".daily__title").on("click", function() {
        if ($(this).hasClass('active')) {
            $(this).removeClass("active").next().slideUp();
        } else {
            $(".daily__title.active").removeClass("active").next(".daily__body").slideUp();
            $(this).addClass('active').next('.daily__body').slideDown();
        }
    });
    $(".daily__body").on("click", function() {
        $(".daily__title.active").removeClass("active").next(".daily__body").slideUp();
    });
</script>