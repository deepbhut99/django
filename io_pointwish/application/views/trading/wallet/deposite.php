<?php $this->load->view('trading/common/header'); ?>
<div class="page-content mx-cryp" ng-controller="Wallet_Controller2" ng-cloak>
    <div class="container-fluid p-0 mt-12px">
        <div class="row">
            <div class="col-md-4 col-sm-6 px-0">
                <div class="card card-wallet-coin card-usdt-trc active" ng-click="show_depositpwt('USDTTRC20')">
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <div>
                                <img class="deposit-image" src="<?= base_url('assets/user_panel/images/icon/usdt.svg') ?>" alt="">
                            </div>
                            <div>
                                <h4 class="wallet-coin-head">USDT <small>(TRC20)</small></h4>
                                <h6 class="wallet-amount text-trc20">697.50</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 px-0">
                <div class="card card-wallet-coin card-eth" ng-click="show_depositpwt('ETH')">
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <div>
                                <img class="deposit-image" src="<?= base_url('assets/user_panel/images/icon/eth.svg') ?>" alt="">
                            </div>
                            <div>
                                <h4 class="wallet-coin-head">Ethereum </h4>
                                <h6 class="wallet-amount text-eth">ETH</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 px-0">
                <div class="card card-wallet-coin card-bnb" ng-click="show_depositpwt('BNB')">
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <div>
                                <img class="deposit-image" src="<?= base_url('assets/user_panel/images/icon/bnb.svg') ?>" alt="">
                            </div>
                            <div>
                                <h4 class="wallet-coin-head">Binance </h4>
                                <h6 class="wallet-amount text-bnb">BNB</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 px-0">
                <div class="card card-wallet-coin card-btc" ng-click="show_depositpwt('BTC')">
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <div>
                                <img class="deposit-image" src="<?= base_url('assets/user_panel/images/icon/btc.png') ?>" alt="">
                            </div>
                            <div>
                                <h4 class="wallet-coin-head">Bitcoin </h4>
                                <h6 class="wallet-amount text-btc">BTC</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 px-0">
                <div class="card card-wallet-coin card-usdt-erc" ng-click="show_depositpwt('USDTERC20')">
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <div>
                                <img class="deposit-image" src="<?= base_url('assets/user_panel/images/icon/usdt.svg') ?>" alt="">
                            </div>
                            <div>
                                <h4 class="wallet-coin-head">USDT <small>(ERC20)</small></h4>
                                <h6 class="wallet-amount text-trc20">697.50</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 px-0">
                <div class="card card-wallet-coin card-pwt" ng-click="show_depositpwt('PWT')">
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <div>
                                <img class="deposit-image" src="<?= base_url('assets/user_panel/images/logo/pointwish.png') ?>" alt="">
                            </div>
                            <div>
                                <h4 class="wallet-coin-head">Point Wish Token</h4>
                                <h6 class="wallet-amount text-pwt">PWT</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 px-3px">
                <div class="card deposit-card">
                    <div class="card-body">
                        <h3 class="deposite-head">{{wallettype}} <small>(min. ${{minDeposit}})</small></h3>
                        <div class="deposit-amt">
                            <h6>Deposite</h6>
                            <div class="">                            
                                <form id="data_form" class="deposite-input-sec" name="data_form" method="POST">
                                    <input required type="number" min="{{minDeposit}}" class="form-control required" ng-model="amountf" id="amount" name="amountf" placeholder="Enter {{wallettype}} Amount">
                                    <input type="hidden" id="token" name="token" value="<?= $user_data->id ?>">
                                    <input type="hidden" name="payment_type" id="payment_type" value="{{payment_type}}">                            
                                    <button id="submit_button_id" type="submit" class="btn btn-dark"><?= lang('pay') ?></button>
                                </form>
                                <div id="payment_details" class="d-none">
                                    <div class="text-center">
                                        <h4 style="text-align: center;"> <strong style="color: #31BAA0;"></strong> <?= lang('deposit_address') ?></h4>
                                    </div>
                                    <br>
                                    <div class="text-center">
                                        <h4 style="text-align: center;"><span id="amountc"></span> <span id="currency"></span></h4>
                                    </div>
                                    <div class="justify-content-center" style="display: flex;">
                                        <div id="qrcode" class="bg-white waves-button-input"></div>
                                    </div>
                                    <br>
                                    <div class="text-center">
                                        <input type="text" class="form-control text-center" id="address" readonly>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-12 mt-2 text-center">
                                            <button id="copy_button_id" onclick="copy_text();" type="button" class="btn btn-primary waves-effect waves-light mr-1"><?= lang('copy') ?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('trading/common/footer'); ?>



<script>
    $(".card-wallet-coin").click(function() {
        $(".card-wallet-coin").removeClass("active");
        $(this).addClass("active");
    });
</script>


<script type="text/javascript">
    var base = "<?= base_url() ?>";
    $("#data_form").validate();
    $("#withdraw_form").validate();
    $("#otp_form").validate();
    $("#data_form").submit(function(e) {
        e.preventDefault();
        var data_form = $("#data_form");
        if (data_form.valid() == false) {
            return;
        } else {
            $('#data_form').addClass('d-none');
            // $('#myModal').modal('show');
            console.log($('#address').val());
            if ($('#address').val() == '') {
                show_deposit_address();

            } else {
                $('#payment_details').removeClass('d-none');

            }
        }
    })

    function copy_text() {
        var copyText = document.getElementById("address");
        copyText.select();
        document.execCommand("copy");
        toastr.success('Copied Successfully', '', toast);
        $("#copy_button_id").html('Copied');

        setTimeout(function() {
            $("#copy_button_id").html('Copy');
        }, 3000);

        //alert("Copied the text: " + copyText.value);
    }

    let utoken = $("#token").val();
    // $.post(base + "trade/payment/check", {},
    //     function(data, status) {
    //         console.log(data);
    //     });

    function show_deposit_address() {

        let amount = $("#amount").val();
        let type = $("#payment_type").val();
        let token = $("#token").val();

        if (type == 'USDTERC20' || type == 'USDTTRC20' || type == "BTC" || type == "ETH" || type == "BNB") {
            url = 'trade/payment/nowpayment';
        } else {
            url = 'trade/payment/coinbase';
        }
        $.post(base + url, {
                amount: amount,
                type: type,
                token: token
            },
            function(data) {
                if (data.payments == 'coinbase') {
                    if (data.address) {
                        $('#payment_details').removeClass('d-none').addClass('d-block');
                        // $("#payment_details").show();
                        // $("#data_form").hide();
                        $("#amountc").text(data.amount);
                        $("#currency").text(data.currency);
                        $("#address").val(data.address);
                        var qrcode = new QRCode("qrcode", {
                            text: data.address,
                            width: 128,
                            height: 128,
                            colorDark: "#000000",
                            colorLight: "#ffffff",
                            correctLevel: QRCode.CorrectLevel.H
                        });
                        // console.log(data.address);
                        // alert("Data: " + data + "\nStatus: " + status);
                    }
                } else {
                    window.location.href = data.url;
                }
            });
    }

    $('#amount').bind("paste", function(e) {
        e.preventDefault();
    });

    var errorMsg = '',
        $valid = false;
    $.validator.addMethod("isMemberAvailabe", function(val, elem) {
        var len = $("#member_id").val();
        if (len.length >= 3) {
            $.ajax({
                async: false,
                url: '<?php echo base_url(); ?>trade/account/isMemberAvailabe',
                type: "POST",
                dataType: "json",
                data: {
                    member_id: $("#member_id").val()
                },
                success: function(response) {
                    if (response.status == 1) {
                        errorMsg = 'Member Available !';
                        $valid = true;
                        $("#member_section_name").html('User Name: ' + response.member_name);
                        $("#member_section_name").removeClass("text-danger");
                        $("#member_section_name").addClass("text-success");

                    } else if (response.status == 2) {
                        errorMsg = 'User not exist !';
                        $valid = false;
                        $("#member_id").addClass("error");
                        $("#member_section_name").addClass("text-danger");
                        $("#member_section_name").html(errorMsg);
                    } else if (response.status == 3) {
                        errorMsg = "You can't transfer funds to yourself !";
                        $valid = false;
                        $("#member_id").addClass("error");
                        $("#member_section_name").addClass("text-danger");
                        $("#member_section_name").html(errorMsg);
                    }
                }
            });
        }

        $.validator.messages["is_sponser_exist"] = errorMsg;
        return $valid;
    }, '');


    $("#transfer_form").validate({
        rules: {
            member_id: {
                required: true,
                isMemberAvailabe: true,
            },
        },
    });

    function transfer_submit() {
        var form = $("#transfer_form");

        if (form.valid() == false) {
            return;
        } else {
            Swal.fire({
                title: "Are you sure?",
                text: "",
                type: "warning",
                showCancelButton: !0,
                confirmButtonColor: "#34c38f",
                cancelButtonColor: "#f46a6a",
                confirmButtonText: "Yes",
                confirmButtonClass: "btn btn-success mt-2",
                cancelButtonClass: "btn btn-danger ml-2 mt-2",
                buttonsStyling: !1,
            }).then(function(t) {
                if (t.value == true) {
                    var url = '<?php echo base_url(); ?>trade/account/transfer_fund';
                    $.ajax({
                        type: "POST",
                        data: {
                            member_id: $('#member_id').val(),
                            amount: $('#ts_amount').val(),
                            type: $('#transfer_type').val(),
                        },
                        url: url,
                        dataType: "json",
                        success: function(response) {
                            window.location.reload(true);
                        }

                    });

                }
            });

        }
    }
</script>