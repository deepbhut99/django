<?php $this->load->view('trading/common/header'); ?>
<div class="page-content" ng-controller="Profile_Controller" ng-cloak>
    <div class="container-fluid p-0 mt-12px">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <table id="transactionwallet" class="table dt-responsive table-striped table-borderless" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Date</th>
                                    <th>Amount</th>
                                    <th>Message</th>
                                    <th>Balance</th>
                                    <th>Transaction Id</th>
                                    <!-- <th>Message</th>
                                    <th>Balance</th>
                                    <th>Transaction Id</th> -->
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('trading/common/footer'); ?>

<script>
    $(document).ready(function() {
        // var earn_table = $("#transactionwallet").DataTable({
        //     "pagingType": "simple",
        //     "order": [
        //         [0, "asc"]
        //     ]
        // });
        // var peers = <?= json_encode($history) ?>;
        // count = 1;
        // peers.forEach(function(bid) {
        //     if (bid.type == "fund") {
        //         earn_table.row.add([count++, bid.created_datetime, bid.amount, bid.message]).draw(false);
        //     } else {

        //     }
        // });



        var table = $("#transactionwallet").DataTable({
            "pagingType": "simple",
            responsive: true
        });
        var peers = <?= json_encode($history) ?>;
        count = 1;

        peers.forEach(function(bid) {
            if (bid.type == "fund") {
                table.row.add([
                    count++,
                    bid.created_datetime,
                    bid.payment_type == 'pwt' ? 'USDT <br>' + (+bid.amount).toFixed(2) : 'USDT <br> ' + (+bid.amount).toFixed(6),
                    bid.message,
                    bid.balance,
                    bid.txn_id
                ]).draw(false);
            } else {

            }
        });
        setTimeout(function() {}, 1000);
        var base = "<?= base_url() ?>";
    });
</script>