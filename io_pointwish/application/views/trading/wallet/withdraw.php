<?php $this->load->view('trading/common/header'); ?>
<div class="page-content" ng-controller="Wallet_Controller3" ng-cloak>
    <div class="container-fluid mt-12px">
        <div class="row">
            <div class="col-sm-6 px-0">
                <div class="card card-wallet-coin card-usdt-trc" ng-click="show_withdrawalpwt('USDTTRC20')">
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <div>
                                <img class="deposit-image" src="<?= base_url('assets/user_panel/images/icon/usdt.svg') ?>" alt="">
                            </div>
                            <div>
                                <h4 class="wallet-coin-head">USDT <small>(TRC20)</small></h4>
                                <h6 class="wallet-amount text-trc20">697.50</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 px-0">
                <div class="card card-wallet-coin card-pwt" ng-click="show_withdrawalpwt('PWT')">
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <div>
                                <img class="deposit-image" src="<?= base_url('assets/user_panel/images/logo/pointwish.png') ?>" alt="">
                            </div>
                            <div>
                                <h4 class="wallet-coin-head">Point Wish Token</h4>
                                <h6 class="wallet-amount text-pwt">PWT</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 px-3px">
                <div class="card deposit-card">
                    <div class="card-body">
                        <h3 class="deposite-head">{{wallettype}}<small>(min. $0)</small></h3>
                        <div class="deposit-amt">
                            <!-- <h6>Withdraw</h6> -->
                            <form name="withdraw_form" id="withdraw_form" class="custom-validation" novalidate="" method="POST">
                                <!-- <input type="text" class="form-control" placeholder="Enter Amount">
                                <button class="btn btn-dark">Submit</button> -->

                                <div class="form-group">
                                    <label><?= lang('amount') ?>
                                        <small class="text-danger font-weight-bold">{{minamount ? '(min '+minamount+')' : '' }}</small>
                                    </label>
                                    <small class="float-right" ng-show="withdraw_in == 'usdt'">(Wallet: $<?= number_format($user_data->wallet_amount, 2) ?>)</small>
                                    <small class="float-right" ng-show="withdraw_in == 'pwt'">(Wallet: <?= number_format($user_data->unlocked_pwt, 2) ?> USDT)</small>
                                    <div>
                                        <input ng-model="withdraw_amount" name="withdraw_amount" id="withdraw_amount" type="number" min="{{minamount}}" class="form-control required" required="" data-parsley-minlength="6" placeholder="<?= lang('enter_amount') ?>">
                                        <input type="hidden" name="withdraw_type" id="withdraw_type" value="{{type}}">
                                        <input type="hidden" name="withdraw_fee" id="withdraw_fee" value="{{fee}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label><?= lang('receiving_address') ?></label>
                                    <div>
                                        <input name="withdraw_address" id="withdraw_address" readonly="" type="text" class="form-control required" required="" data-parsley-maxlength="6" ng-model="address" placeholder="<?= lang('enter_rcvng_address') ?>">
                                    </div>
                                </div>
                                <div class="text-white" ng-show="withdraw_amount">
                                    <label><?= lang('you_will_get') ?> {{ withdraw_in == 'usdt' ? '$' : 'pwt '   }}{{withdraw_amount - fee > 0 ? withdraw_amount - fee : 0}}</label>
                                </div>
                                <div class="form-group mb-0">
                                    <div>
                                        <button type="submit" ng-disabled="withdraw_amount - fee < 0" ng-click="make_withdrawal(withdraw_form)" class="btn btn-dark float-right">
                                            <?= lang('submit') ?>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('trading/common/footer'); ?>

<!-- <script>
    $(".card-usdt-trc").click(function() {
        $(this).addClass("active");
    });
</script> -->

<script>
    $(".card-wallet-coin").click(function() {
        $(".card-wallet-coin").removeClass("active");
        $(this).addClass("active");
    });
</script>