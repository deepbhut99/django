<?php $this->load->view('trading/common/header'); ?>
<style>
    .table-striped tbody tr:nth-of-type(odd) {
        background-color: #12161C;
    }

    .dataTables_length {
        display: none;
    }
</style>
<div class="page-content">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row align-items-center">
            <div class="col-sm-6">
                <div class="page-title-box">
                    <!-- <h4 class="font-size-18">Directory</h4> -->
                </div>
            </div>

            <div class="col-sm-6">
                <div class="float-right d-none d-md-block">
                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-xl-1 col-md-1">
            </div>
            <div class="col-xl-10 col-md-10">
                <h5 style="text-align: center; font-size:22px;" class="dt">Lucky Draw Winner</h5>
            </div>
            <div class="col-xl-1 col-md-1">
            </div>
        </div><br>


        <div class="row">
            <div class="col-xl-1 col-lg-1 col-md-12 col-sm-12"></div>
            <div class="col-xl-10 col-lg-10 col-md-12 col-sm-12">
                <div class="card bg-transparent mt-n4">
                    <div class="card-body">



                        <table id="datatable" class="table dt-responsive nowrap table-striped table-borderless" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <tr>
                                    <th>Sr.no</th>
                                    <th>Image</th>
                                    <th>Name</th>
                                    <th>Date</th>
                                    <th>Email</th>
                                    <th>Profit</th>
                                    <th>Terms</th>
                                </tr>
                            </thead>


                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td><img src="<?= base_url('assets/user_panel/images/users/user-1.jpg') ?>" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg"></td>
                                    <td>Sophia</td>
                                    <td>1st Dec 2020</td>
                                    <td>So***@g***.com</td>
                                    <td>100%</td>
                                    <td>Terms</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td><img src="<?= base_url('assets/user_panel/images/users/user-2.jpg') ?>" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg"></td>
                                    <td>Sophia</td>
                                    <td>1st Dec 2020</td>
                                    <td>So***@g***.com</td>
                                    <td>100%</td>
                                    <td>Terms</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td><img src="<?= base_url('assets/user_panel/images/users/user-3.jpg') ?>" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg"></td>
                                    <td>Sophia</td>
                                    <td>1st Dec 2020</td>
                                    <td>So***@g***.com</td>
                                    <td>100%</td>
                                    <td>Terms</td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td><img src="<?= base_url('assets/user_panel/images/users/user-4.jpg') ?>" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg"></td>
                                    <td>Sophia</td>
                                    <td>12th Dec 2020</td>
                                    <td>So***@g***.com</td>
                                    <td>100%</td>
                                    <td>Terms</td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td><img src="<?= base_url('assets/user_panel/images/users/user-5.jpg') ?>" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg"></td>
                                    <td>Sophia</td>
                                    <td>25th Dec 2020</td>
                                    <td>So***@g***.com</td>
                                    <td>100%</td>
                                    <td>Terms</td>
                                </tr>
                                <tr>
                                    <td>6</td>
                                    <td><img src="<?= base_url('assets/user_panel/images/users/user-6.jpg') ?>" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg"></td>
                                    <td>Sophia</td>
                                    <td>7th Dec 2020</td>
                                    <td>So***@g***.com</td>
                                    <td>100%</td>
                                    <td>Terms</td>
                                </tr>
                                <tr>
                                    <td>7</td>
                                    <td><img src="<?= base_url('assets/user_panel/images/users/user-7.jpg') ?>" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg"></td>
                                    <td>Sophia</td>
                                    <td>1st Oct 2020</td>
                                    <td>So***@g***.com</td>
                                    <td>100%</td>
                                    <td>Terms</td>
                                </tr>
                                <tr>
                                    <td>8</td>
                                    <td><img src="<?= base_url('assets/user_panel/images/users/user-8.jpg') ?>" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg"></td>
                                    <td>Sophia</td>
                                    <td>1st Nov 2020</td>
                                    <td>So***@g***.com</td>
                                    <td>100%</td>
                                    <td>Terms</td>
                                </tr>
                                <tr>
                                    <td>9</td>
                                    <td><img src="<?= base_url('assets/user_panel/images/users/user-9.jpg') ?>" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg"></td>
                                    <td>Sophia</td>
                                    <td>9th Dec 2020</td>
                                    <td>So***@g***.com</td>
                                    <td>100%</td>
                                    <td>Terms</td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td><img src="<?= base_url('assets/user_panel/images/users/user-1.jpg') ?>" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg"></td>
                                    <td>Sophia</td>
                                    <td>1st Dec 2020</td>
                                    <td>So***@g***.com</td>
                                    <td>100%</td>
                                    <td>Terms</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td><img src="<?= base_url('assets/user_panel/images/users/user-2.jpg') ?>" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg"></td>
                                    <td>Sophia</td>
                                    <td>1st Dec 2020</td>
                                    <td>So***@g***.com</td>
                                    <td>100%</td>
                                    <td>Terms</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td><img src="<?= base_url('assets/user_panel/images/users/user-3.jpg') ?>" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg"></td>
                                    <td>Sophia</td>
                                    <td>1st Dec 2020</td>
                                    <td>So***@g***.com</td>
                                    <td>100%</td>
                                    <td>Terms</td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td><img src="<?= base_url('assets/user_panel/images/users/user-4.jpg') ?>" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg"></td>
                                    <td>Sophia</td>
                                    <td>12th Dec 2020</td>
                                    <td>So***@g***.com</td>
                                    <td>100%</td>
                                    <td>Terms</td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td><img src="<?= base_url('assets/user_panel/images/users/user-5.jpg') ?>" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg"></td>
                                    <td>Sophia</td>
                                    <td>25th Dec 2020</td>
                                    <td>So***@g***.com</td>
                                    <td>100%</td>
                                    <td>Terms</td>
                                </tr>
                                <tr>
                                    <td>6</td>
                                    <td><img src="<?= base_url('assets/user_panel/images/users/user-6.jpg') ?>" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg"></td>
                                    <td>Sophia</td>
                                    <td>7th Dec 2020</td>
                                    <td>So***@g***.com</td>
                                    <td>100%</td>
                                    <td>Terms</td>
                                </tr>
                                <tr>
                                    <td>7</td>
                                    <td><img src="<?= base_url('assets/user_panel/images/users/user-7.jpg') ?>" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg"></td>
                                    <td>Sophia</td>
                                    <td>1st Oct 2020</td>
                                    <td>So***@g***.com</td>
                                    <td>100%</td>
                                    <td>Terms</td>
                                </tr>
                                <tr>
                                    <td>8</td>
                                    <td><img src="<?= base_url('assets/user_panel/images/users/user-8.jpg') ?>" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg"></td>
                                    <td>Sophia</td>
                                    <td>1st Nov 2020</td>
                                    <td>So***@g***.com</td>
                                    <td>100%</td>
                                    <td>Terms</td>
                                </tr>
                                <tr>
                                    <td>9</td>
                                    <td><img src="<?= base_url('assets/user_panel/images/users/user-9.jpg') ?>" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg"></td>
                                    <td>Sophia</td>
                                    <td>9th Dec 2020</td>
                                    <td>So***@g***.com</td>
                                    <td>100%</td>
                                    <td>Terms</td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-xl-1 col-lg-1 col-md-12 col-sm-12"></div> <!-- end col -->
        </div> <!-- end row -->

        <!-- <div class="row">

            <div class="col-xl-1 col-md-1">
            </div>
            <div class="col-xl-10 col-md-10">
                <div class="col-xl-12 col-md-12">
                    <div class="card directory-card winerweb">
                        <div class="card-body" style="padding-bottom: 10px;">

                            <div class="row">
                                <div class="col-xl-1 col-md-1">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">1</h5>
                                </div>
                                <div class="col-xl-2 col-md-2">

                                    <img src="<?= base_url('assets/user_panel/images/users/user-2.jpg') ?>" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg" style="margin-top: -12px; vertical-align: bottom;">

                                </div>

                                <div class="col-xl-2 col-md-2">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">Sophia</h5>
                                </div>
                                <div class="col-xl-3 col-md-3">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">sophia@gmail.com</h5>
                                </div>
                                <div class="col-xl-2 col-md-2">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">95%</h5>
                                </div>

                                <div class="col-xl-2 col-md-2">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">Terms</h5>
                                </div>

                            </div>


                        </div>
                    </div>
                </div>
                <div class="col-xl-12 col-md-12">
                    <div class="card directory-card winerweb">
                        <div class="card-body" style="padding-bottom: 10px;">

                            <div class="row">
                                <div class="col-xl-1 col-md-1">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">2</h5>
                                </div>
                                <div class="col-xl-2 col-md-2">

                                    <img src="<?= base_url('assets/user_panel/images/users/user-4.jpg') ?>" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg" style="margin-top: -12px; vertical-align: bottom;">

                                </div>

                                <div class="col-xl-2 col-md-2">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">Alex</h5>
                                </div>
                                <div class="col-xl-3 col-md-3">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">alex@gmail.com</h5>
                                </div>
                                <div class="col-xl-2 col-md-2">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">94%</h5>
                                </div>

                                <div class="col-xl-2 col-md-2">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">Terms</h5>
                                </div>

                            </div>


                        </div>
                    </div>
                </div>
                <div class="col-xl-12 col-md-12">
                    <div class="card directory-card winerweb">
                        <div class="card-body" style="padding-bottom: 10px;">

                            <div class="row">
                                <div class="col-xl-1 col-md-1">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">3</h5>
                                </div>
                                <div class="col-xl-2 col-md-2">

                                    <img src="<?= base_url('assets/user_panel/images/users/user-9.jpg') ?>" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg" style="margin-top: -12px; vertical-align: bottom;">

                                </div>

                                <div class="col-xl-2 col-md-2">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">Olivia</h5>
                                </div>
                                <div class="col-xl-3 col-md-3">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">olivia@gmail.com</h5>
                                </div>
                                <div class="col-xl-2 col-md-2">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">93%</h5>
                                </div>

                                <div class="col-xl-2 col-md-2">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">Terms</h5>
                                </div>

                            </div>


                        </div>
                    </div>
                </div>


            </div>
            <div class="col-xl-1 col-md-1">
            </div>

        </div>

        <div class="row">



            <div class="col-xl-4 col-md-6">
                <div class="card directory-card  winer">
                    <div class="card-body">
                        <div class="media">
                            <img src="assets/images/users/user-5.jpg" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg" style="height: 6rem; width:6rem;">
                            <div class="media-body ml-3">
                                <h5 class="text-primary font-size-18 mt-0 mb-1" style="color: deepskyblue!important;">1. Sophia</h5>

                                <p class="mb-0">sophia@gmail.com</p>
                                <p class="mb-0">95%</p>
                                <p class="mb-0">terms</p>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="card directory-card  winer">
                    <div class="card-body">
                        <div class="media">
                            <img src="<?= base_url('assets/user_panel/images/users/user-4.jpg') ?>" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg" style="height: 6rem; width:6rem;">
                            <div class="media-body ml-3">
                                <h5 class="text-primary font-size-18 mt-0 mb-1" style="color: deepskyblue!important;">2. Alex</h5>

                                <p class="mb-0">alex@gmail.com</p>
                                <p class="mb-0">94%</p>
                                <p class="mb-0">terms</p>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="card directory-card  winer">
                    <div class="card-body">
                        <div class="media">
                            <img src="<?= base_url('assets/user_panel/images/users/user-9.jpg') ?>" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg" style="height: 6rem; width:6rem;">
                            <div class="media-body ml-3">
                                <h5 class="text-primary font-size-18 mt-0 mb-1" style="color: deepskyblue!important;">3. Olivia</h5>

                                <p class="mb-0">olivia@gmail.com</p>
                                <p class="mb-0">93%</p>
                                <p class="mb-0">terms</p>
                            </div>

                        </div>

                    </div>
                </div>
            </div>

        </div> -->





        <!-- <div class="row">
            <div class="col-xl-1 col-md-1">
            </div>
            <div class="col-xl-10 col-md-10">
                <div>
                    <h5 style="text-align: center; font-size:22px;" class="dt">2nd December, 2020</h5>
                </div>
            </div>
            <div class="col-xl-1 col-md-1">
            </div>
        </div><br>

        <div class="row">

            <div class="col-xl-1 col-md-1">
            </div>
            <div class="col-xl-10 col-md-10">
                <div class="col-xl-12 col-md-12">
                    <div class="card directory-card winerweb">
                        <div class="card-body" style="padding-bottom: 10px;">

                            <div class="row">
                                <div class="col-xl-1 col-md-1">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">1</h5>
                                </div>
                                <div class="col-xl-2 col-md-2">

                                    <img src="<?= base_url('assets/user_panel/images/users/user-2.jpg') ?>" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg" style="margin-top: -12px; vertical-align: bottom;">
                                </div>

                                <div class="col-xl-2 col-md-2">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">Sophia</h5>
                                </div>
                                <div class="col-xl-3 col-md-3">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">sophia@gmail.com</h5>
                                </div>
                                <div class="col-xl-2 col-md-2">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">95%</h5>
                                </div>

                                <div class="col-xl-2 col-md-2">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">Terms</h5>
                                </div>

                            </div>


                        </div>
                    </div>
                </div>
                <div class="col-xl-12 col-md-12">
                    <div class="card directory-card winerweb">
                        <div class="card-body" style="padding-bottom: 10px;">

                            <div class="row">
                                <div class="col-xl-1 col-md-1">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">2</h5>
                                </div>
                                <div class="col-xl-2 col-md-2">

                                    <img src="<?= base_url('assets/user_panel/images/users/user-4.jpg') ?>" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg" style="margin-top: -12px; vertical-align: bottom;">
                                </div>

                                <div class="col-xl-2 col-md-2">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">Alex</h5>
                                </div>
                                <div class="col-xl-3 col-md-3">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">alex@gmail.com</h5>
                                </div>
                                <div class="col-xl-2 col-md-2">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">94%</h5>
                                </div>

                                <div class="col-xl-2 col-md-2">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">Terms</h5>
                                </div>

                            </div>


                        </div>
                    </div>
                </div>
                <div class="col-xl-12 col-md-12">
                    <div class="card directory-card winerweb">
                        <div class="card-body" style="padding-bottom: 10px;">

                            <div class="row">
                                <div class="col-xl-1 col-md-1">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">3</h5>
                                </div>
                                <div class="col-xl-2 col-md-2">

                                    <img src="<?= base_url('assets/user_panel/images/users/user-9.jpg') ?>" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg" style="margin-top: -12px; vertical-align: bottom;">
                        
                                </div>

                                <div class="col-xl-2 col-md-2">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">Olivia</h5>
                                </div>
                                <div class="col-xl-3 col-md-3">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">olivia@gmail.com</h5>
                                </div>
                                <div class="col-xl-2 col-md-2">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">93%</h5>
                                </div>

                                <div class="col-xl-2 col-md-2">
                                    <h5 class="text-primary font-size-16 mt-0 mb-1" style="color: white!important;">Terms</h5>
                                </div>

                            </div>


                        </div>
                    </div>
                </div>


            </div>
            <div class="col-xl-1 col-md-1">
            </div>

        </div>

        <div class="row">



            <div class="col-xl-4 col-md-6">
                <div class="card directory-card  winer">
                    <div class="card-body">
                        <div class="media">
                            <img src="assets/images/users/user-5.jpg" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg" style="height: 6rem; width:6rem;">
                            <div class="media-body ml-3">
                                <h5 class="text-primary font-size-18 mt-0 mb-1" style="color: deepskyblue!important;">1. Sophia</h5>

                                <p class="mb-0">sophia@gmail.com</p>
                                <p class="mb-0">95%</p>
                                <p class="mb-0">terms</p>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="card directory-card  winer">
                    <div class="card-body">
                        <div class="media">
                            <img src="<?= base_url('assets/user_panel/images/users/user-4.jpg') ?>" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg" style="height: 6rem; width:6rem;">
                            <div class="media-body ml-3">
                                <h5 class="text-primary font-size-18 mt-0 mb-1" style="color: deepskyblue!important;">2. Alex</h5>

                                <p class="mb-0">alex@gmail.com</p>
                                <p class="mb-0">94%</p>
                                <p class="mb-0">terms</p>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="card directory-card  winer">
                    <div class="card-body">
                        <div class="media">
                            <img src="<?= base_url('assets/user_panel/images/users/user-9.jpg') ?>" alt="" class="img-fluid img-thumbnail rounded-circle avatar-lg" style="height: 6rem; width:6rem;">
                            <div class="media-body ml-3">
                                <h5 class="text-primary font-size-18 mt-0 mb-1" style="color: deepskyblue!important;">3. Olivia</h5>
                                <p class="mb-0">olivia@gmail.com</p>
                                <p class="mb-0">93%</p>
                                <p class="mb-0">terms</p>
                            </div>

                        </div>

                    </div>
                </div>
            </div>


        </div> -->


    </div> <!-- container-fluid -->
</div>
<?php $this->load->view('trading/common/footer'); ?>