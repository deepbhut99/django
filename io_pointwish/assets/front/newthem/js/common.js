var pageNo = 1;

function pagingAjax(e, r, s) {
    showTdiv(), $.ajax({
        url: r,
        cache: !1,
        success: function(e) {
            hideTdiv(), $("#" + s).html(e), $("html, body").animate({
                scrollTop: $("#" + s).offset().top - 200
            }, "500", "swing", function() {})
        }
    })
}

function showTdiv() {
    $(".overlay").show()
}

function hideTdiv() {
    $(".overlay").hide()
}

function keyupOtp(e) {
    13 === e.which && checkOtp()
}

function keyupOtpCheck(e) {
    13 == e.which && checkOtpLogin()
}

function keyupOtpSignCheck(e) {
    13 === e.which && signupCheckOtp()
}

function isNumberKey() {
    var e = event.which ? event.which : event.keyCode;
    return !(e > 31 && (e < 48 || e > 57))
}

function signUpFunction() {
    $("#signUp").trigger("click")
}

function validateEmail(e) {
    return /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(e)
}

// function checkValidate() {
//     var e = $.trim($("#sponsor_id").val()),
//         r = $.trim($("#fullname").val()),
//         s = $.trim($("#email").val()),
//         a = $("#password").val(),
//         o = $("#confirm_password").val(),
//         t = 0;
//     return "" == e ? ($("#sponsor_id").addClass("error1"), $("#member_name_section").text("*Please enter Sponcer ID."), t++) : $("#sponsor_id").removeClass("error1"), "" == r ? ($("#fullname").addClass("error1"), $("#error_full_name").text("*Please enter Username."), t++) : ($("#error_full_name").text(""), $("#fullname").removeClass("error1")), "" == s ? ($("#email").addClass("error1"), $("#error_email").text("*Please enter your email."), t++) : validateEmail(s) || "" == s ? ($("#error_email").text(""), $("#email").removeClass("error1")) : ($("#email").addClass("error1"), $("#error_email").text("*Please enter valid email."), t++), "" == a ? ($("#password").addClass("error1"), $("#error_passwordUp").text("*Please enter your password."), t++) : a.length < 6 && "" != a ? ($("#password").addClass("error1"), $("#error_passwordUp").text("*Password should be of minimum 6 digits."), t++) : ($("#error_passwordUp").text(""), $("#password").removeClass("error1")), a != o && ($("#confirm_password").addClass("error1"), $("#error_cpassword").text("*Password not match."), t++), t
// }

$("#terms").change(function() {
    if ($('#terms').prop('checked')) {
        $("#signUpSubmit").attr("disabled", false);
        $(".label-terms").removeClass("redClass");
    } else {
        $("#signUpSubmit").attr("disabled", true);
        $(".label-terms").addClass("redClass");
    }
});

function hasWhiteSpace(s) {
    return (/\s/).test(s);
}

function checkValidate() {

    var sponsor_id = $.trim($("#sponsor_id").val());
    var name = $.trim($("#fullname").val());
    // var name = name1.replace(/ /g, "");
    var email = $.trim($("#email").val());
    var passwordUp = $("#password").val();
    var confirm_password = $('#confirm_password').val();
    var chk_err = 0;
    if (name == "") {
        $("#fullname").addClass("error1");
        $("#error_full_name").text("( Please enter Username. )");
        chk_err++;
    } else if (name.length < 5) {
        $("#fullname").addClass("error1");
        $("#error_full_name").addClass("text-danger");
        $("#error_full_name").text("( Username should be of minimum 5 letter. )");
        chk_err++;
    } else if (name.length > 9) {
        $("#fullname").addClass("error1");
        $("#error_full_name").addClass("text-danger");
        $("#error_full_name").text("( Username should be of maximum 9 letter. )");
        chk_err++;
    } else if (hasWhiteSpace(name) == true) {
        $("#fullname").addClass("error1");
        $("#error_full_name").addClass("text-danger");
        $("#error_full_name").text("( Space inbetween character. )");
        chk_err++;
    } else if (!/^[a-zA-Z0-9](?!.*?[^\na-zA-Z0-9]{2}).*?[a-zA-Z0-9]$/.test(name)) {
        $("#fullname").addClass("error1");
        $("#error_full_name").addClass("text-danger");
        $("#error_full_name").text("( Special character are not allowed. )");
        chk_err++;
    } else {
        $("#error_full_name").text("");
        $("#error_full_name").removeClass("text-danger");
        $("#error_full_name").addClass("text-success");
        $("#fullname").removeClass("error1");
    }
    if (email == "") {
        $("#email").addClass("error1");
        $("#error_email").text("( Please enter your email. )");
        chk_err++;
    } else if (!validateEmail(email) && email != '') {
        $("#error_email").removeClass("text-success");
        $("#email").addClass("error1");
        $("#error_email").text("( Please enter valid email. )");
        chk_err++;
    } else {
        $("#error_email").text("");
        $("#email").removeClass("error1");
    }
    if (passwordUp == "") {
        $("#password").addClass("error1");
        $("#error_passwordUp").text("( Please enter your password. )");
        chk_err++;
    } else if (/\s/.test(passwordUp)) {
        $("#password").addClass("error1");
        $("#error_passwordUp").text("( White space not allowed. )");
        chk_err++;
    } else if (passwordUp.length < 6 && passwordUp != "") {
        $("#password").addClass("error1");
        $("#error_passwordUp").text("( Password should be of minimum 6 digits. )");
        chk_err++;
    } else {
        $("#error_passwordUp").text("");
        $("#password").removeClass("error1");
    }
    if (passwordUp != confirm_password) {
        $("#confirm_password").addClass("error1");
        $("#error_cpassword").text("( Password not match. )");
        chk_err++;
    } else {
        $("#error_cpassword").text("");
        $("#confirm_password").removeClass("error1");
    }

    return chk_err;
}

function viewPassFunc() {
    var e = $("#confirm_password").attr("type");
    "password" == e ? $("#confirm_password").attr("type", "text") : "text" == e && $("#confirm_password").attr("type", "password")
}

function viewPassFuncIn() {
    var e = $("#lpassword").attr("type");
    "password" == e ? $("#lpassword").attr("type", "text") : "text" == e && $("#lpassword").attr("type", "password")
}
$(".removescroll").click(function() {
    $("body").css("overflow", "scroll")
}), $(document).ready(function(e) {
    $("#fullname").focusin(function() {
        $(this).removeClass("error1"), $("#error_full_name").text("")
    }), $("#email").focusin(function() {
        $(this).removeClass("error1"), $("#error_email").text("")
    }), $("#password").focusin(function() {
        $(this).removeClass("error1"), $("#error_password").text("")
    }), $("#confirm_password").focusin(function() {
        $(this).removeClass("error1"), $("#error_cpassword").text("")
    }), $("#signUpSubmit").click(function(e) {
        if (e.preventDefault(), checkValidate() > 0) return !1;
        $.ajax({
            type: "POST",
            url: "login/signup",
            data: $("#signUpForm").serialize(),
            beforeSend: function(e) {
                $('#signUpSubmit').attr('disabled', true);
                $('#signUpSubmit span i').addClass("fa fa-refresh fa-spin fa-fw");
            },
            success: function(e) {
                e = JSON.parse(e);
                console.log(e), "1" == e.success ? ($("#username_span").text(e.username), $("#password_span").text(e.password), $("#login_detail_modal").modal({
                    backdrop: "static",
                    keyboard: !1,
                }), $("#register-success").html("Registration done Successfully !"), $('#signUpSubmit span i').removeClass("fa fa-refresh fa-spin fa-fw")) : "4" == e.success ? ($("#username_span").text(e.username), $("#password_span").text(e.password), $("#login_detail_modal").modal({
                    backdrop: "static",
                    keyboard: !1
                })) : "2" == e.success ? ($("#signUpSubmit span").html("Register"), $("#member_name_section").html("Sponsor not exist !")) : "3" == e.success && ($("#signUpSubmit span").html("Register"), $("#errorMessageOnline1").html("IP is already register !"))
            }
        })
    })
}), $(".onlyAlphabet").keydown(function(e) {
    if (e.ctrlKey || e.altKey) e.preventDefault();
    else {
        var r = e.keyCode;
        9 == r || 8 == r || 32 == r || 46 == r || r >= 35 && r <= 40 || r >= 65 && r <= 90 || e.preventDefault()
    }
}), $(".onlyNumber").keydown(function(e) {
    var r = e.keyCode;
    r > 31 && (r < 48 || r > 57) && (r < 95 || r > 105) && e.preventDefault()
}), $(document).ready(function() {
    $("#data_form").submit(function(e) {
        e.preventDefault();
        var r = $("#username").val(),
            s = $("#lpassword").val(),
            a = 0;
        if ($("#errorMessageOnline").html(""), "" == r ? ($("#username").addClass("error1"), $("#error_user").html("*Required Field."), a++) : ($("#username").removeClass("error1"), $("#error_user").html("")), "" == s ? ($("#lpassword").addClass("error1"), $("#error_pass").html("*Required Field."), a++) : ($("#lpassword").removeClass("error1"), $("#error_pass").html("")), a > 0) return !1;
        $.ajax({
            url: "login",
            type: "POST",
            data: $("#data_form").serialize(),
            beforeSend: function() {
                $("#login_btn_Online span i").addClass("fa fa-refresh fa-spin fa-fw")
            },
            success: function(e) {
                "1" == (e = JSON.parse(e)).success ? location.href = "affiliate" : "2" == e.success ? ($("#login_btn_Online span").html("Login"), $("#errorMessageOnline").html("Invalid Username or Password !")) : "3" == e.success ? ($("#login_btn_Online span").html("Login"), $("#verification_model").modal(), $("#tm-login").modal('toggle')) : location.href = BASE_URL
            }
        })
    });
    $("#authForm").submit(function(e) {
        e.preventDefault();
        $("#error_user").html("");
        $.ajax({
            type: 'POST',
            url: "login-auth",
            data: $("#authForm").serialize(),
            beforeSend: function() {
                $("#auth_btn_Online").html("Verifying...");
            },
            success: function(result) {
                var result = JSON.parse(result);
                if (result['success'] == true) {
                    location.href = BASE_URL + 'affiliate';
                } else if (result['success'] == false) {
                    $("#auth_btn_Online").html("Submit")
                    $("#error_user").html(result['message']);
                }
            }
        });
    });
    $("#form_forgot").submit(function(event) {
        event.preventDefault();
        var user = $("#forgetemail").val();
        // var userc = $("#userNameForConfirm").val();
        var retVal = 0;
        if (user == "") {
            $("#error_forgotU").html("*Required Field.");
            retVal++
        } else {
            $("#error_forgotU").html("");
        }
        if (retVal > 0) {
            return false;
        }
        $.ajax({
            url: "login/forgot-password",
            type: 'POST',
            data: 'action=forgotPassOnline&email=' + user,
            beforeSend: function(e) {
                $('#forgot_btn_Online').attr('disabled', true);
                $('#forgot_btn_Online i').addClass("fa fa-refresh fa-spin fa-fw");
            },
            success: function(e) {
                $('#forgot_btn_Online').attr('disabled', false);
                $('#forgot_btn_Online span').html('SUBMIT');
                var result = JSON.parse(e);
                if (result['success'] == 1) {

                    $("#reset-confirmation").removeClass('d-none');
                    $("#reset-form").addClass('d-none');
                    $("#reset-form").removeClass('d-block');

                    $("#successForget").html("Please check your email for the password !");

                    // $('#forgotPassModal').modal('hide');

                    // $('#otp_modal').modal('show');
                    // $('body').css('overflow', 'hidden');
                    // $("#otp_Error").html("OTP sent");                   

                } else if (result['success'] == 2) {
                    $("#error_forgotU").html("Please enter your registered email !");
                    $('#forgot_btn_Online i').removeClass("fa fa-refresh fa-spin fa-fw");
                    $("#form-grp-femail").removeClass("is-valid");
                    $("#form-grp-femail").addClass("has-error");
                }
            }
        });

    });

    $("#form_reset").submit(function(event) {
        event.preventDefault();
        var password = $("#password").val();
        var cpassword = $("#confirm_password").val();
        var retVal = 0;
        if (password == "") {
            $("#error_passwordUp").html("*Required Field.");
            retVal++
        } else if (/\s/.test(password)) {
            $("#password").addClass("error1");
            $("#error_passwordUp").text("( White space not allowed. )");
            retVal++;
        } else if (password.length < 6 && password != "") {
            $("#password").addClass("error1");
            $("#error_passwordUp").text("( Password should be of minimum 6 digits. )");
            retVal++;
        } else {
            $("#error_passwordUp").html("");
        }
        if (cpassword == "") {
            $("#error_cpassword").html("*Required Field.");
            retVal++
        } else {
            if (password == cpassword) {
                $("#error_cpassword").html("");
            } else {
                $("#error_cpassword").html("*Password not match.");
                retVal++
            }
        }
        if (retVal > 0) {
            return false;
        }
        $.ajax({
            url: "",
            type: 'POST',
            data: $("#form_reset").serialize(),
            beforeSend: function(e) {
                $('#reset_btn_Online').attr('disabled', true);
                $('#reset_btn_Online span').html('Please wait..');
            },
            success: function(result) {
                console.log(result);
                $('#reset_btn_Online').attr('disabled', false);
                $('#reset_btn_Online span').html('SUBMIT');
                var result = JSON.parse(result);
                if (result['success'] == 1) {
                    $('#errorMessage').addClass('successSpan');
                    $('#errorMessage').removeClass('errorSpan');
                    $("#errorMessage").html("Password has been reset successfully.");
                    location.href = BASE_URL + "home?resetsuccess";
                } else if (result['success'] == 0) {
                    $('#errorMessage').addClass('errorSpan');
                    $('#errorMessage').removeClass('successSpan');
                    $("#errorMessage").html("User Not Exist");
                }
            }
        });

    });
    $("#close_category").click(function() {
        $("input:radio[name=program][value=1]").prop("checked", !1), $("input:radio[name=adProgram][value=1]").prop("checked", !1)
    });
});