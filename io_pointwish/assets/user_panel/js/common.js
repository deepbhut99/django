$(document).ready(function() {
        $(".btn-demo").removeClass("btn-primary"),
            $(".btn-demo").addClass("btn-secondary"),
            $("#page-topbar *").addClass("text-white");
        var t = USER;
        $("#live-amount").html(
                Math.round(100 * parseFloat(sessionStorage.getItem("live-amount"))) /
                100
            ),
            $("#demo-amount").html(
                Math.round(
                    100 * parseFloat(sessionStorage.getItem("demo-amount"))
                ) / 100
            );
        var a = sessionStorage.getItem(
            $("input[name=acc]:checked").val() + "-amount"
        );
        $(".account-amount").html(Math.round(100 * parseFloat(a)) / 100);
        var e,
            o = "live" == $("input[name=acc]:checked").val() ? 0 : 1,
            r = io("https://trading.pwt.io"),
            s = 0,
            n = 0,
            d = 0,
            l = [
                "aroonoscillator",
                "aroon",
                "atr",
                "linearRegressionAngle",
                "linearRegressionSlope",
                "macd",
                "mfi",
                "natr",
                "ppo",
                "stochastic",
                "slowstochastic",
                "williamsr",
            ],
            c = $("#transactions").DataTable({
                order: [
                    [0, "desc"]
                ],
                columnDefs: [{ targets: [0], visible: !1, searchable: !1 }],
            }),
            m = [],
            h = "",
            p = 0,
            u = 1;
        sessionStorage.setItem("tableData", JSON.stringify([])),
            $(".buttons > *").prop("disabled", "disabled"),
            r.emit("join", { userId: t });
        var v = [],
            g = [],
            b = 0,
            f = 0;

        function x(t) {
            if (
                ((index = m.indexOf("empty")),
                    (m[index] = t),
                    (index = m.indexOf("empty")), -1 == index)
            )
                for (m = m.slice(20), i = 0; i < 20; i++) m.push("empty");
            for (content = "", k = 0, i = 0; i < p / 20; i++) {
                for (
                    content += '<div class="col-xs-3 result-box m-auto">', j = 0; j < 20; j++
                )
                    j % 5 == 0 && (content += '<div class="row">'),
                    (content += '<div class="dots ' + m[k] + '"></div>'),
                    j % 5 == 4 && (content += "</div>"),
                    k++;
                content += "</div>";
            }
            $(".results-container").html(content);
        }

        function y(t) {
            $("#lblValue text").html(
                    Math.round(100 * (t.close + Number.EPSILON)) / 100
                ),
                $("#lblO text").html(
                    "O: " + Math.round(100 * (t.open + Number.EPSILON)) / 100
                ),
                $("#lblH text").html(
                    "H: " + Math.round(100 * (t.high + Number.EPSILON)) / 100
                ),
                $("#lblL text").html(
                    "L: " + Math.round(100 * (t.low + Number.EPSILON)) / 100
                ),
                $("#lblC text").html(
                    "C: " + Math.round(100 * (t.close + Number.EPSILON)) / 100
                );
        }

        function w() {
            0 == $(".remove-alerts").children().length ?
                $(".remove-alert-tab").addClass("disabled") :
                $(".remove-alert-tab").removeClass("disabled"),
                0 == $(".edit-indicators").children().length ?
                $(".remove-indicator-tab").addClass("disabled") :
                $(".remove-indicator-tab").removeClass("disabled");
        }
        (width = $(window).width()),
        (height = 350),
        width >= 1920 ?
            ((f = 50), (p = 100)) :
            width < 1920 && width >= 1768 ?
            ((f = 45), (p = 100)) :
            width < 1768 && width >= 1680 ?
            ((f = 41), (p = 100)) :
            width < 1680 && width >= 1440 ?
            ((f = 35), (p = 100)) :
            width < 1440 && width >= 1366 ?
            ((f = 34), (p = 100)) :
            width < 1366 && width >= 1280 ?
            ((f = 28), (p = 100)) :
            width < 1280 && width >= 1152 ?
            ((f = 25), (p = 80)) :
            width < 1152 && width >= 768 ?
            ((f = 20), (p = 80), (height = 300)) :
            width < 768 && width >= 540 ?
            ((f = 14), (p = 60), (height = 250)) :
            width < 540 && ((f = 12), !1, (p = 40), (height = 100)),
            r.on("initChart", function(t) {
                for (value = 0, i = t.length - 1; i > 0; i--)
                    v.push([
                        t[i].timestamp,
                        t[i].open,
                        t[i].high,
                        t[i].low,
                        t[i].close,
                    ]),
                    (volumeColor = ""),
                    t[i].open < t[i].close ?
                    (volumeColor = "#02C076") :
                    (volumeColor = "#D9304E"),
                    g.push({
                        x: t[i].timestamp,
                        y: t[i].volume,
                        color: volumeColor,
                    });
                for (k = p - 20, content = "", i = 0; i < p / 20; i++) {
                    for (
                        content += '<div class="col-xs-3 result-box m-auto">',
                        j = 0; j < 20; j++
                    )
                        j % 5 == 0 && (content += '<div class="row">'),
                        (color = ""),
                        k > 0 ?
                        ((color =
                                t[k].close > t[k].open ? "green" : "red"),
                            k--) :
                        (color = "empty"),
                        m.push(color),
                        (content += '<div class="dots ' + color + '"></div>'),
                        j % 5 == 4 && (content += "</div>");
                    content += "</div>";
                }
                $(".results-container").html(content),
                    (b = v.length),
                    (value = t[0].close),
                    Highcharts.setOptions({
                        colors: [
                            "#B0CB1F",
                            "#F6B096",
                            "#834E98",
                            "#CB5499",
                            "#F08365",
                            "#FECC00",
                            "#393185",
                            "#008DD2",
                            "#EF7F1A",
                            "#1C5359",
                            "#8C3B17",
                            "#AF9778",
                            "#82B8C9",
                            "#A76281",
                        ],
                    }),
                    Highcharts.stockChart(
                        "container", {
                            rangeSelector: { enabled: !1 },
                            chart: {
                                backgroundColor: "transparent",
                                margin: [0, 77, 0, 0],
                            },
                            xAxis: {
                                gridLineWidth: 0,
                                lineWidth: 0,
                                range: 6e4 * f,
                            },
                            yAxis: [{
                                    gridLineWidth: 0,
                                    labels: { align: "left" },
                                    height: "80%",
                                    lineWidth: 0,
                                    resize: { enabled: !1 },
                                },
                                {
                                    gridLineWidth: 0,
                                    labels: { enabled: !1 },
                                    top: "80%",
                                    height: "20%",
                                    offset: 0,
                                },
                                {
                                    gridLineWidth: 0,
                                    height: "40%",
                                    top: "60%",
                                    offset: 0,
                                },
                            ],
                            tooltip: { split: !1, xDateFormat: "%d %B, %Y" },
                            exporting: { enabled: !1 },
                            scrollbar: { enabled: !1 },
                            plotOptions: {
                                series: {
                                    dataGrouping: {
                                        forced: true,
                                        units: [
                                            ['minute', [1]]
                                        ]
                                    },
                                },
                                candlestick: {
                                    color: "#D9304E",
                                    lineColor: "#D9304E",
                                    upColor: "#02C076",
                                    upLineColor: "#02C076",
                                    groupPadding: 0.3,
                                    pointPadding: -0.3,
                                },
                                column: { groupPadding: 0.2, pointPadding: -0.2 },
                                series: {
                                    marker: { enabled: !1 },
                                    enableMouseTracking: !1,
                                },
                            },
                            navigator: {
                                enabled: !0,
                                height: 0,
                                xAxis: { labels: { enabled: !1 } },
                                handles: {
                                    backgroundColor: "transparent",
                                    borderColor: "transparent",
                                },
                                outlineWidth: 0,
                            },
                            series: [{
                                    type: "candlestick",
                                    id: "candle",
                                    name: "Candle",
                                    data: v,
                                    enableMouseTracking: !0,
                                    lastVisiblePrice: {
                                        enabled: !0,
                                        label: { enabled: !0 },
                                    },
                                },
                                {
                                    type: "column",
                                    id: "volume",
                                    name: "Volume",
                                    data: g,
                                    enableMouseTracking: !1,
                                    yAxis: 1,
                                },
                            ],
                            stockTools: { gui: { enabled: !1 } },
                            responsive: {
                                rules: [{
                                    condition: { maxWidth: 800 },
                                    chartOptions: {
                                        rangeSelector: { inputEnabled: !1 },
                                    },
                                }, ],
                            },
                            credits: { enabled: !1 },
                        },
                        function(t) {
                            t.renderer
                                .label("Value: ", 450, 8)
                                .attr({ zIndex: 5, id: "lblValue" })
                                .css({ fontSize: 30, color: "white" })
                                .add(),
                                t.renderer
                                .label("(Timer)", 600, 13)
                                .attr({ zIndex: 5, id: "lblTimer" })
                                .css({ fontSize: 24, color: "white" })
                                .add(),
                                t.renderer
                                .label("H: ", 260, 8)
                                .attr({ zIndex: 5, id: "lblH" })
                                .css({ color: "white" })
                                .add(),
                                t.renderer
                                .label("O: ", 345, 8)
                                .attr({ zIndex: 5, id: "lblO" })
                                .css({ color: "white" })
                                .add(),
                                t.renderer
                                .label("L: ", 260, 30)
                                .attr({ zIndex: 5, id: "lblL" })
                                .css({ color: "white" })
                                .add(),
                                t.renderer
                                .label("C: ", 345, 30)
                                .attr({ zIndex: 5, id: "lblC" })
                                .css({ color: "white" })
                                .add();
                        }
                    ),
                    $(".highcharts-navigator").hide(),
                    $(".highcharts-yaxis").css("display", "none"),
                    $("#lblValue text").html(
                        Math.round(100 * (value + Number.EPSILON)) / 100
                    ),
                    $(".page-loader").fadeOut(500),
                    setTimeout(function() {
                        $("#container").highcharts().series[0].removePoint(b, !1),
                            $("#container")
                            .highcharts()
                            .series[1].removePoint(b, !1),
                            r.on("addBTCData", function(t) {
                                "BTC" == $("#coin").val() &&
                                    ((series = $("#container").highcharts().series),
                                        null != series[0].data[b - 1] &&
                                        (series[1].data[b - 1].color =
                                            series[0].data[b - 1].color),
                                        b++,
                                        series[0].addPoint(
                                            [
                                                t.dataTimestamp,
                                                t.open,
                                                t.high,
                                                t.low,
                                                Math.round(
                                                    100 * (t.close + Number.EPSILON)
                                                ) / 100,
                                            ], !0, !1
                                        ),
                                        series[1].addPoint({
                                            x: t.dataTimestamp,
                                            y: t.volume,
                                            color: "#D9304E",
                                        }, !0, !1),
                                        $("#container").highcharts().redraw(),
                                        y(t),
                                        x(h));
                            }),
                            r.on("updateBTCData", function(t) {
                                "BTC" == $("#coin").val() &&
                                    ($("#lastValue").val(t.close),
                                        (chart = $("#container").highcharts()),
                                        (series = chart.series),
                                        (greenColor = t.open < t.close),
                                        (h = greenColor ? "green" : "red"),
                                        series[0].removePoint(b, !1),
                                        series[0].addPoint(
                                            [
                                                t.dataTimestamp,
                                                t.open,
                                                t.high,
                                                t.low,
                                                Math.round(
                                                    100 * (t.close + Number.EPSILON)
                                                ) / 100,
                                            ], !0, !1
                                        ),
                                        series[1].removePoint(b, !1),
                                        series[1].addPoint({
                                            x: t.dataTimestamp,
                                            y: t.volume,
                                            color: greenColor ?
                                                "#02C076" : "#D9304E",
                                        }, !0, !1),
                                        $("#container").highcharts().redraw(),
                                        y(t));
                            }),
                            r.on("addETHData", function(t) {
                                "ETH" == $("#coin").val() &&
                                    ((series = $("#container").highcharts().series),
                                        null != series[0].data[b - 1] &&
                                        (series[1].data[b - 1].color =
                                            series[0].data[b - 1].color),
                                        b++,
                                        series[0].addPoint(
                                            [
                                                t.dataTimestamp,
                                                t.open,
                                                t.high,
                                                t.low,
                                                Math.round(
                                                    100 * (t.close + Number.EPSILON)
                                                ) / 100,
                                            ], !0, !1
                                        ),
                                        series[1].addPoint({
                                            x: t.dataTimestamp,
                                            y: t.volume,
                                            color: "#D9304E",
                                        }, !0, !1),
                                        $("#container").highcharts().redraw(),
                                        y(t),
                                        x(h));
                            }),
                            r.on("updateETHData", function(t) {
                                "ETH" == $("#coin").val() &&
                                    ($("#lastValue").val(t.close),
                                        (chart = $("#container").highcharts()),
                                        (series = chart.series),
                                        (greenColor = t.open < t.close),
                                        (h = greenColor ? "green" : "red"),
                                        series[0].removePoint(b, !1),
                                        series[0].addPoint(
                                            [
                                                t.dataTimestamp,
                                                t.open,
                                                t.high,
                                                t.low,
                                                Math.round(
                                                    100 * (t.close + Number.EPSILON)
                                                ) / 100,
                                            ], !0, !1
                                        ),
                                        series[1].removePoint(b, !1),
                                        series[1].addPoint({
                                            x: t.dataTimestamp,
                                            y: t.volume,
                                            color: greenColor ?
                                                "#02C076" : "#D9304E",
                                        }, !0, !1),
                                        $("#container").highcharts().redraw(),
                                        y(t));
                            }),
                            r.on("timer", function(t) {
                                -1 == (e = t.sec - 1) && (e = 59),
                                    $("#timer").html(
                                        (e < 30 ?
                                            "Please Trade: " + (30 - e) :
                                            "Wait Time: " + (60 - e)) + "s"
                                    ),
                                    $("#timer1").html(
                                        (e < 30 ?
                                            "Please Trade: " + (30 - e) :
                                            "Wait Time: " + (60 - e)) + "s"
                                    ),
                                    $("#lblTimer text").html(
                                        "(" + (e < 30 ? 30 - e : 60 - e) + "s)"
                                    ),
                                    e >= 29 ?
                                    (e >= 55 ?
                                        ($("#timer").removeClass(
                                                "blink-me"
                                            ),
                                            $("#timer").addClass(
                                                "blink-me-fast"
                                            )) :
                                        $("#timer").addClass("blink-me"),
                                        $(".btn-up-action span").removeClass(
                                            "blink-me"
                                        ),
                                        $(".btn-down-action span").removeClass(
                                            "blink-me"
                                        ),
                                        $(".btn-up-action").prop(
                                            "disabled",
                                            "disabled"
                                        ),
                                        $(".btn-down-action").prop(
                                            "disabled",
                                            "disabled"
                                        )) :
                                    ($(".btn-up-action span").addClass(
                                            "blink-me"
                                        ),
                                        $(".btn-down-action span").addClass(
                                            "blink-me"
                                        ),
                                        $("#timer").removeClass("blink-me"),
                                        $("#timer").removeClass("blink-me-fast"),
                                        $(".btn-up-action").prop("disabled", !1),
                                        $(".btn-down-action").prop(
                                            "disabled", !1
                                        ));
                            }),
                            r.on("ratio", function(t) {
                                (buy = t.buy),
                                (sell = t.sell),
                                (buyWidth = (buy / (buy + sell)) * 100),
                                (sellWidth = (sell / (buy + sell)) * 100),
                                buyWidth < 15 ?
                                    ($("#contentBuy").css(
                                            "width",
                                            buyWidth + "%"
                                        ),
                                        $("#contentBuy").html(""),
                                        $("#contentSell").css(
                                            "width",
                                            sellWidth + "%"
                                        ),
                                        $("#contentSell").html(
                                            Math.round(sellWidth) + "%"
                                        )) :
                                    sellWidth < 15 ?
                                    ($("#contentBuy").css(
                                            "width",
                                            buyWidth + "%"
                                        ),
                                        $("#contentBuy").html(
                                            Math.round(buyWidth) + "%"
                                        ),
                                        $("#contentSell").css(
                                            "width",
                                            sellWidth + "%"
                                        ),
                                        $("#contentSell").html("")) :
                                    ($("#contentBuy").css(
                                            "width",
                                            buyWidth + "%"
                                        ),
                                        $("#contentBuy").html(
                                            Math.round(buyWidth) + "%"
                                        ),
                                        $("#contentSell").css(
                                            "width",
                                            sellWidth + "%"
                                        ),
                                        $("#contentSell").html(
                                            Math.round(sellWidth) + "%"
                                        ));
                            });
                    }, 500),
                    setTimeout(function() {
                        $(".preload-stopper").removeClass("d-none"),
                            $("#activity").val("");
                    }, 500);
            }),
            r.on("lastBids", function(t) {
                $("#open, #close").html(""),
                    (openBids = ""),
                    (closeBids = ""),
                    (empty = ""),
                    t.forEach(function(t) {
                        (bidCard =
                            '<div class="card card-open mb-2"><div class="card-body p-0"><div class="d-flex justify-content-between"><h5 class="text-gold">' +
                            t.coin.toUpperCase() +
                            "/USD</h5><h5>" +
                            (1 == t.growth ? "Buy" : "Sell") +
                            '<span class="fa fa-arrow-circle-' +
                            (1 == t.growth ? "up" : "down") +
                            " font-weight-900 text-theme-" +
                            (1 == t.growth ? "green" : "red") +
                            '"></span></h5></div><div><div class="d-flex justify-content-between"><h5 class="text-light font-size-17">' +
                            moment(t.timestamp).format("hh:mm:ss A") +
                            '</h5><h5 class="text-gold font-size-17"><span>' +
                            t.amount +
                            '</span> USDT</h5></div><div class="d-flex justify-content-between"><span class="badge badge-theme-' +
                            (0 == t.type ? "green" : "red") +
                            ' line-h-inherit pt-0">' +
                            (0 == t.type ? "Live" : "Demo") +
                            '</span><h5 class="text-' +
                            (-1 == t.result ?
                                "gold" :
                                "theme-" + (1 == t.result ? "green" : "red")) +
                            ' mb-0">' +
                            (-1 == t.result ?
                                "<span>TBD" :
                                1 == t.result ?
                                "+<span>" + (t.amount + (95 * t.amount) / 100) :
                                0) +
                            " USDT </span></h5></div></div></div></div>"), -1 == t.result ?
                            (openBids += bidCard) :
                            (closeBids += bidCard);
                    }),
                    $("#open").html("" == openBids ? empty : openBids),
                    $("#close").html("" == closeBids ? empty : closeBids);
            }),
            r.on("indicators", function(t) {
                (indicators = t.indicators),
                indicators.forEach(function(t) {
                        (options = JSON.parse(t.options)),
                        (options.id = "chart-" + options.id),
                        $("#container").highcharts().addSeries(options),
                            (html = `<li class="list-group-item edit-indicator-item" data-chartid="${
                            options.id
                        }" data-id="${options.type}" data-title="${
                            t.title
                        }" data-period="${options.params.period}" ${
                            "macd" == options.type
                                ? ' data-shortperiod = "' +
                                  options.params.shortPeriod +
                                  '" data-longperiod="' +
                                  options.params.longPeriod +
                                  '" data-signalperiod = "' +
                                  options.params.signalPeriod +
                                  '"'
                                : ""
                        }>${t.name}</li>`),
                            $(".edit-indicators").append(html);
                    }),
                    (totalSeries = t.charts);
            }),
            r.on("confirmBid", function(t) {}),
            r.on("rejectBid", function(t) {
                (a += t.amount),
                0 == t.type ?
                    ($("#live-amount").html(
                            Math.round(100 * parseFloat(a)) / 100
                        ),
                        sessionStorage.setItem("live-amount", a)) :
                    ($("#demo-amount").html(
                            Math.round(100 * parseFloat(a)) / 100
                        ),
                        sessionStorage.setItem("demo-amount", a)),
                    $(".account-amount").html(
                        Math.round(100 * parseFloat(a)) / 100
                    ),
                    toastr.error(t.message);
            }),
            r.on("result", function(t) {
                (tableData = JSON.parse(sessionStorage.getItem("tableData"))),
                tableData.push({
                        srn: u++,
                        datetime: moment(t.timestamp).format(
                            "YYYY-MM-DD hh:mm:ss A"
                        ),
                        type: t.type,
                        closing: t.closeVal,
                        baseAmount: t.value,
                        tradeAmount: t.amount,
                        prediction: t.prediction,
                        profit: t.profit,
                    }),
                    0 == t.type ? (n += t.profit) : (d += t.profit),
                    (s += t.profit),
                    sessionStorage.setItem("tableData", JSON.stringify(tableData));
            }),
            r.on("giveResults", function(e) {
                s > 0 &&
                    (n > 0 &&
                        ((liveAmount = parseFloat(
                                JSON.parse(sessionStorage.getItem("live-amount"))
                            )),
                            (liveAmount += n),
                            sessionStorage.setItem("live-amount", liveAmount),
                            $("#live-amount").html(
                                Math.round(
                                    100 *
                                    parseFloat(
                                        sessionStorage.getItem("live-amount")
                                    )
                                ) / 100
                            )),
                        d > 0 &&
                        ((demoAmount = parseFloat(
                                JSON.parse(sessionStorage.getItem("demo-amount"))
                            )),
                            (demoAmount += d),
                            sessionStorage.setItem("demo-amount", demoAmount),
                            $("#demo-amount").html(
                                Math.round(
                                    100 *
                                    parseFloat(
                                        sessionStorage.getItem("demo-amount")
                                    )
                                ) / 100
                            )),
                        $("#txtWallet").val(a),
                        $("#coin-sound")[0].play(),
                        $(".chart-wrapper").addClass("profit-loader"),
                        $(".profit-loader-text").removeClass("d-none"),
                        $(".profit-loader-text span").html(
                            `Profit earned: ${s.toFixed(2)} USDT`
                        ),
                        // swal({
                        //     title: `Profit earned: ${s.toFixed(2)} USDT`,
                        //     icon: BASE_URL + 'assets/user_panel/images/users/user-4.jpg'
                        // }),
                        setTimeout(function() {
                            $(".chart-wrapper").removeClass("profit-loader"),
                                $(".profit-loader-text").addClass("d-none");
                        }, 5e3)),
                    r.emit("refreshBalance", t),
                    (a = sessionStorage.getItem(
                        $("input[name=acc]:checked").val() + "-amount"
                    )),
                    $(".account-amount").html(
                        Math.round(100 * parseFloat(a)) / 100
                    ),
                    (tableData = JSON.parse(sessionStorage.getItem("tableData"))),
                    tableData.forEach(function(t) {
                        c.row
                            .add([
                                t.srn,
                                t.datetime,
                                t.closing,
                                t.baseAmount,
                                t.tradeAmount,
                                t.prediction,
                                t.profit,
                            ])
                            .draw(!1);
                    }),
                    sessionStorage.setItem("tableData", "[]"),
                    (s = 0),
                    (n = 0),
                    (d = 0),
                    r.emit("lastBids", USER);
            }),
            r.on("activeAlerts", function(t) {
                t.forEach(function(t) {
                    (expiry = new Date(t.expiry)),
                    (html = `<li class="list-group-item remove-alert-item" data-alertid="${
                        t.id
                    }" data-type="${t.chart}" data-condition="${t.criteria}" ${
                        1 == t.type ? 'data-value="' + t.price + '"' : ""
                    } data-expiry="${expiry.getDate()}-${expiry.getMonth()}-${expiry.getFullYear()}" data-status="${
                        t.expiry < t.curr_time
                            ? "Expired"
                            : 1 == t.alert_active
                            ? "Active"
                            : "Inactive"
                    }">${1 == t.type ? "Price" : t.chart.toUpperCase()}</li>`),
                    $(".remove-alerts").append(html);
                });
            }),
            r.on("addedIndicator", function(t) {
                toastr.success(t.message);
            }),
            r.on("alertAdded", function(t) {
                "true" == t.status ?
                    ((expiry = new Date(t.expiry)),
                        (html = `<li class="list-group-item remove-alert-item" data-alertid="${
                      t.id
                  }" data-type="${t.chart}" data-condition="${t.condition}" ${
                      1 == t.type ? 'data-value="' + t.price + '"' : ""
                  } data-expiry="${expiry.getDate()}-${expiry.getMonth()}-${expiry.getFullYear()}"  data-status="Active">${
                      1 == t.type ? "Price" : t.chart.toUpperCase()
                  }</li>`),
                        $(".remove-alerts").append(html),
                        $(".btn-alert").click(),
                        toastr.success(t.message)) :
                    "false" == t.status && toastr.error(t.message);
            }),
            r.on("alertExpired", function(t) {
                toastr.success(t.message);
            }),
            r.on("userAlert", function(t) {
                $("#alert-sound")[0].play(),
                    swal.fire("Alert", t.message, "success");
            }),
            r.on("technicalBTCData", function(t) {
                "BTC" == $("#coin").val() &&
                    (t.broadcastData.ema.forEach(function(t) {
                            $(`.ema-${t.period}-val`).html(t.data),
                                $(`.ema-${t.period}-action`).html(t.action),
                                $(`.ema-${t.period}-action`).css(
                                    "color",
                                    "Buy" == t.action ? "green" : "red"
                                );
                        }),
                        t.broadcastData.sma.forEach(function(t) {
                            $(`.sma-${t.period}-val`).html(t.data),
                                $(`.sma-${t.period}-action`).html(t.action),
                                $(`.sma-${t.period}-action`).css(
                                    "color",
                                    "Buy" == t.action ? "green" : "red"
                                );
                        }),
                        $(".rsi-val").html(t.broadcastData.others.rsi[0].data),
                        $(".rsi-action").html(t.broadcastData.others.rsi[0].action),
                        $(".rsi-action").css(
                            "color",
                            "Buy" == t.broadcastData.others.rsi[0].action ?
                            "green" :
                            "red"
                        ),
                        $(".stochastic-val").html(
                            t.broadcastData.others.stochastic[0].data
                        ),
                        $(".stochastic-action").html(
                            t.broadcastData.others.stochastic[0].action
                        ),
                        $(".stochastic-action").css(
                            "color",
                            "Buy" == t.broadcastData.others.stochastic[0].action ?
                            "green" :
                            "red"
                        ),
                        $(".slowStochastic-val").html(
                            t.broadcastData.others.slowStochastic[0].data
                        ),
                        $(".slowStochastic-action").html(
                            t.broadcastData.others.slowStochastic[0].action
                        ),
                        $(".slowStochastic-action").css(
                            "color",
                            "Buy" == t.broadcastData.others.slowStochastic[0].action ?
                            "green" :
                            "red"
                        ),
                        $(".williamr-val").html(
                            t.broadcastData.others.williamr[0].data
                        ),
                        $(".williamr-action").html(
                            t.broadcastData.others.williamr[0].action
                        ),
                        $(".williamr-action").css(
                            "color",
                            "Buy" == t.broadcastData.others.williamr[0].action ?
                            "green" :
                            "red"
                        ),
                        $(".cci-val").html(t.broadcastData.others.cci[0].data),
                        $(".cci-action").html(t.broadcastData.others.cci[0].action),
                        $(".cci-action").css(
                            "color",
                            "Buy" == t.broadcastData.others.cci[0].action ?
                            "green" :
                            "red"
                        ),
                        $(".macd-val").html(t.broadcastData.others.macd[0].data),
                        $(".macd-action").html(t.broadcastData.others.macd[0].action),
                        $(".macd-action").css(
                            "color",
                            "Buy" == t.broadcastData.others.macd[0].action ?
                            "green" :
                            "red"
                        ),
                        $(".supertrend-val").html(
                            t.broadcastData.others.supertrend[0].data
                        ),
                        $(".supertrend-action").html(
                            t.broadcastData.others.supertrend[0].action
                        ),
                        $(".supertrend-action").css(
                            "color",
                            "Buy" == t.broadcastData.others.supertrend[0].action ?
                            "green" :
                            "red"
                        ),
                        $(".wma-val").html(t.broadcastData.others.wma[0].data),
                        $(".wma-action").html(t.broadcastData.others.wma[0].action),
                        $(".wma-action").css(
                            "color",
                            "Buy" == t.broadcastData.others.wma[0].action ?
                            "green" :
                            "red"
                        ),
                        $(".vwap-val").html(t.broadcastData.others.vwap[0].data),
                        $(".vwap-action").html(t.broadcastData.others.vwap[0].action),
                        $(".vwap-action").css(
                            "color",
                            "Buy" == t.broadcastData.others.vwap[0].action ?
                            "green" :
                            "red"
                        ),
                        $(".psar-val").html(t.broadcastData.others.psar[0].data),
                        $(".psar-action").html(t.broadcastData.others.psar[0].action),
                        $(".psar-action").css(
                            "color",
                            "Buy" == t.broadcastData.others.psar[0].action ?
                            "green" :
                            "red"
                        ));
            }),
            r.on("technicalETHData", function(t) {
                "ETH" == $("#coin").val() &&
                    (t.broadcastData.ema.forEach(function(t) {
                            $(`.ema-${t.period}-val`).html(t.data),
                                $(`.ema-${t.period}-action`).html(t.action),
                                $(`.ema-${t.period}-action`).css(
                                    "color",
                                    "Buy" == t.action ? "green" : "red"
                                );
                        }),
                        t.broadcastData.sma.forEach(function(t) {
                            $(`.sma-${t.period}-val`).html(t.data),
                                $(`.sma-${t.period}-action`).html(t.action),
                                $(`.sma-${t.period}-action`).css(
                                    "color",
                                    "Buy" == t.action ? "green" : "red"
                                );
                        }),
                        $(".rsi-val").html(t.broadcastData.others.rsi[0].data),
                        $(".rsi-action").html(t.broadcastData.others.rsi[0].action),
                        $(".rsi-action").css(
                            "color",
                            "Buy" == t.broadcastData.others.rsi[0].action ?
                            "green" :
                            "red"
                        ),
                        $(".stochastic-val").html(
                            t.broadcastData.others.stochastic[0].data
                        ),
                        $(".stochastic-action").html(
                            t.broadcastData.others.stochastic[0].action
                        ),
                        $(".stochastic-action").css(
                            "color",
                            "Buy" == t.broadcastData.others.stochastic[0].action ?
                            "green" :
                            "red"
                        ),
                        $(".slowStochastic-val").html(
                            t.broadcastData.others.slowStochastic[0].data
                        ),
                        $(".slowStochastic-action").html(
                            t.broadcastData.others.slowStochastic[0].action
                        ),
                        $(".slowStochastic-action").css(
                            "color",
                            "Buy" == t.broadcastData.others.slowStochastic[0].action ?
                            "green" :
                            "red"
                        ),
                        $(".williamr-val").html(
                            t.broadcastData.others.williamr[0].data
                        ),
                        $(".williamr-action").html(
                            t.broadcastData.others.williamr[0].action
                        ),
                        $(".williamr-action").css(
                            "color",
                            "Buy" == t.broadcastData.others.williamr[0].action ?
                            "green" :
                            "red"
                        ),
                        $(".cci-val").html(t.broadcastData.others.cci[0].data),
                        $(".cci-action").html(t.broadcastData.others.cci[0].action),
                        $(".cci-action").css(
                            "color",
                            "Buy" == t.broadcastData.others.cci[0].action ?
                            "green" :
                            "red"
                        ),
                        $(".macd-val").html(t.broadcastData.others.macd[0].data),
                        $(".macd-action").html(t.broadcastData.others.macd[0].action),
                        $(".macd-action").css(
                            "color",
                            "Buy" == t.broadcastData.others.macd[0].action ?
                            "green" :
                            "red"
                        ),
                        $(".supertrend-val").html(
                            t.broadcastData.others.supertrend[0].data
                        ),
                        $(".supertrend-action").html(
                            t.broadcastData.others.supertrend[0].action
                        ),
                        $(".supertrend-action").css(
                            "color",
                            "Buy" == t.broadcastData.others.supertrend[0].action ?
                            "green" :
                            "red"
                        ),
                        $(".wma-val").html(t.broadcastData.others.wma[0].data),
                        $(".wma-action").html(t.broadcastData.others.wma[0].action),
                        $(".wma-action").css(
                            "color",
                            "Buy" == t.broadcastData.others.wma[0].action ?
                            "green" :
                            "red"
                        ),
                        $(".vwap-val").html(t.broadcastData.others.vwap[0].data),
                        $(".vwap-action").html(t.broadcastData.others.vwap[0].action),
                        $(".vwap-action").css(
                            "color",
                            "Buy" == t.broadcastData.others.vwap[0].action ?
                            "green" :
                            "red"
                        ),
                        $(".psar-val").html(t.broadcastData.others.psar[0].data),
                        $(".psar-action").html(t.broadcastData.others.psar[0].action),
                        $(".psar-action").css(
                            "color",
                            "Buy" == t.broadcastData.others.psar[0].action ?
                            "green" :
                            "red"
                        ));
            }),
            r.on("refreshBalance", function() {
                r.emit("refreshBalance", t);
            }),
            r.on("refreshedBalance", function(t) {
                $("#live-amount").html(Math.round(100 * parseFloat(t.live)) / 100),
                    sessionStorage.setItem("live-amount", t.live),
                    $("#demo-amount").html(
                        Math.round(100 * parseFloat(t.demo)) / 100
                    ),
                    sessionStorage.setItem("demo-amount", t.demo),
                    $(".account-amount").html(
                        Math.round(
                            100 *
                            parseFloat(
                                "live" == $('input[name="acc"]:checked').val() ?
                                t.live :
                                t.demo
                            )
                        ) / 100
                    );
            }),
            $("#txtAmount1").on("focus", function() {
                $(".profit-col").hide(), $(".result-container-div *").hide();
            }),
            $("#txtAmount1").on("focusout", function() {
                $(".profit-col").show(), $(".result-container-div *").show();
            }),
            $("#txtWallet").val(a),
            $(".btnAmt").on("click", function() {
                (amt = $(this).attr("data-amount")),
                (val = parseFloat($("#txtAmount").val())),
                isNaN(val) && (val = 0),
                    (val = "All" == amt ? parseFloat(a) : val + parseInt(amt)),
                    (val = Math.round(100 * val) / 100),
                    val > 0 ?
                    ($("#txtAmount, #txtAmount1").val(val),
                        $("#profit, #profit1").html(
                            "+" +
                            Math.round(100 * (val + (95 * val) / 100)) / 100 +
                            " USDT"
                        )) :
                    ($("#profit, #profit1").html("+0 USDT"),
                        $("#txtAmount, #txtAmount1").val(0));
            }),
            $("#txtAmount, #txtAmount1").on("keyup", function() {
                (val = isNaN(parseInt($(this).val())) ?
                    0 :
                    parseInt($(this).val())),
                val > 0 ?
                    ($("#txtAmount, #txtAmount1").val(val),
                        $("#profit, #profit1").html(
                            "+" +
                            Math.round(100 * (val + (95 * val) / 100)) / 100 +
                            " USDT"
                        )) :
                    ($("#profit, #profit1").html("+0 USDT"),
                        $("#txtAmount, #txtAmount1").val(0));
            }),
            $(".btn-up-action").on("click", async function() {
                (amount = parseFloat($("#txtAmount").val())),
                $(".btn-up-action").prop("disabled", "disabled"),
                    $(".btn-down-action").prop("disabled", "disabled"),
                    amount <= a && amount > 0 ?
                    ((a -= amount),
                        $(".account-amount").html(
                            Math.round(100 * parseFloat(a)) / 100
                        ),
                        0 == o ?
                        ($("#live-amount").html(
                                Math.round(100 * parseFloat(a)) / 100
                            ),
                            sessionStorage.setItem("live-amount", a)) :
                        ($("#demo-amount").html(
                                Math.round(100 * parseFloat(a)) / 100
                            ),
                            sessionStorage.setItem("demo-amount", a)),
                        $.ajax({
                            type: "get",
                            url: BASE_URL + "trade/trading/getRatios",
                            success: function(a) {
                                var i = (a = JSON.parse(a)).winRatio,
                                    s = a.lastWinRatio;
                                (coin = $("#coin").val()),
                                (value = parseFloat($("#lastValue").val())),
                                (timestamp = new Date().getTime() - 1e3),
                                r.emit("bid", {
                                        userId: t,
                                        type: o,
                                        amount: amount,
                                        growth: 1,
                                        coin: coin,
                                        value: value,
                                        sec: e,
                                        winRatio: i,
                                        lastWinRatio: s,
                                        timestamp: timestamp,
                                    }),
                                    toastr.success("Bid Confirmed");
                            },
                        })) :
                    toastr.error("Insufficient funds");
            }),
            $(".btn-down-action").on("click", async function() {
                (amount = parseFloat($("#txtAmount").val())),
                $(".btn-up-action").prop("disabled", "disabled"),
                    $(".btn-down-action").prop("disabled", "disabled"),
                    amount <= a && amount > 0 ?
                    ((a -= amount),
                        $(".account-amount").html(
                            Math.round(100 * parseFloat(a)) / 100
                        ),
                        0 == o ?
                        ($("#live-amount").html(
                                Math.round(100 * parseFloat(a)) / 100
                            ),
                            sessionStorage.setItem("live-amount", a)) :
                        ($("#demo-amount").html(
                                Math.round(100 * parseFloat(a)) / 100
                            ),
                            sessionStorage.setItem("demo-amount", a)),
                        $.ajax({
                            type: "get",
                            url: BASE_URL + "trade/trading/getRatios",
                            success: function(a) {
                                var i = (a = JSON.parse(a)).winRatio,
                                    s = a.lastWinRatio;
                                (coin = $("#coin").val()),
                                (value = parseFloat($("#lastValue").val())),
                                (timestamp = new Date().getTime() - 1e3),
                                r.emit("bid", {
                                        userId: t,
                                        type: o,
                                        amount: amount,
                                        growth: -1,
                                        coin: coin,
                                        value: value,
                                        sec: e,
                                        winRatio: i,
                                        lastWinRatio: s,
                                        timestamp: timestamp,
                                    }),
                                    toastr.success("Bid Confirmed");
                            },
                        })) :
                    toastr.error("Insufficient funds");
            }),
            $(".btn-clear").on("click", function() {
                $("#txtAmount, #txtAmount1").val(0),
                    $("#txtWallet").val(a),
                    $("#profit, #profit1").html("+$0");
            }),
            $(".searchclear").on("click", function() {
                $("#txtAmount, #txtAmount1").val(0),
                    $("#profit, #profit1").html("+$0");
            }),
            $(".account-radio").on("change", function() {
                $(".account-label").html($(this).attr("data-label")),
                    (a = sessionStorage.getItem(
                        $("input[name=acc]:checked").val() + "-amount"
                    )),
                    $(".account-amount").html(
                        Math.round(100 * parseFloat(a)) / 100
                    ),
                    (o = "live" == $("input[name=acc]:checked").val() ? 0 : 1),
                    $.ajax({
                        type: "post",
                        url: BASE_URL + "trade/trading/updateTradeType",
                        data: { type: o },
                        success: function(t) {},
                    });
            }),
            $(".btn-indicator").on("click", function() {
                w(),
                    $("#txtAddIndicator").val("aroonoscillator"),
                    $("#txtEditIndicator").val(
                        $(".edit-indicators > li").attr("data-id")
                    ),
                    $(".edit-indicator-title").html(
                        $(".edit-indicators > li").attr("data-title")
                    ),
                    (first = $(".edit-indicators > li").html()),
                    "undefined" != typeof first ?
                    ((editVal = first.slice(
                            first.indexOf("(") + 1,
                            first.indexOf(")")
                        )),
                        $("#txtEditPeriod").val(editVal),
                        $(".indicator-edit-params").removeClass("d-none")) :
                    $(".indicator-edit-params").addClass("d-none"),
                    $("#indicators-modal").modal(),
                    $(".edit-indicators > li:first-child").click();
            }),
            $(".add-indicator-item").on("click", function() {
                $(".add-indicator-item").removeClass("active"),
                    $(this).addClass("active"),
                    (chart = $(this).attr("data-id")),
                    (title = $(this).attr("data-title")),
                    (period = $(this).attr("data-period")),
                    $(".add-indicator-title").html(title),
                    $("#txtAddIndicator").val(chart),
                    $("#txtPeriod").val(period),
                    "macd" == chart ?
                    ($(".normal-add-params").removeClass("d-none"),
                        $(".extra-add-params").removeClass("d-none")) :
                    ($(".normal-add-params").removeClass("d-none"),
                        $(".extra-add-params").addClass("d-none"));
            }),
            $(".btn-add-indicator").on("click", function() {
                (chart = $("#txtAddIndicator").val()),
                $.ajax({
                    type: "get",
                    url: BASE_URL + "trade/trading/getChartId",
                    success: function(a) {
                        (chartId = JSON.parse(a)),
                        (options = {
                            id: "chart-" + chartId,
                            type: chart,
                            linkedTo: "candle",
                        }),
                        l.includes(chart) && (options.yAxis = 2),
                            "macd" == chart ?
                            (options.params = {
                                period: parseInt($("#txtPeriod").val()),
                                shortPeriod: parseInt(
                                    $("#txtShortPeriod").val()
                                ),
                                longPeriod: parseInt(
                                    $("#txtLongPeriod").val()
                                ),
                                signalPeriod: parseInt(
                                    $("#txtSignalPeriod").val()
                                ),
                            }) :
                            (options.params = {
                                period: parseInt($("#txtPeriod").val()),
                            }),
                            $("#container").highcharts().addSeries(options),
                            (options.id = chartId),
                            r.emit("addIndicator", {
                                userId: t,
                                chartId: chartId,
                                title: $(".add-indicator-title").html(),
                                name: $("#container")
                                    .highcharts()
                                    .get("chart-" + chartId).name,
                                options: JSON.stringify(options),
                            }),
                            (html = `<li class="list-group-item edit-indicator-item" data-chartid="chart-${
                                options.id
                            }" data-id="${options.type}" data-title="${$(
                                ".add-indicator-title"
                            ).html()}"  data-period="${
                                options.params.period
                            }"  ${
                                "macd" == options.type
                                    ? ' data-shortperiod = "' +
                                      options.params.shortPeriod +
                                      '" data-longperiod="' +
                                      options.params.longPeriod +
                                      '" data-signalperiod = "' +
                                      options.params.signalPeriod +
                                      '"'
                                    : ""
                            }>${
                                $("#container")
                                    .highcharts()
                                    .get("chart-" + chartId).name
                            }</li>`),
                            $(".edit-indicators").append(html),
                            $(".indicator-edit-params").removeClass("d-none"),
                            $(".edit-indicators > li:first-child").click(),
                            w();
                    },
                });
            }),
            $(".btn-remove-indicator").on("click", function() {
                (chart = $(this).attr("data-chart-id")),
                (chartId = chart.slice(chart.indexOf("-") + 1)),
                $("#container").highcharts().get(chart).remove(),
                    $(".edit-indicator-item[data-chartid=" + chart + "]").remove(),
                    $("option[data-chartid=" + chart + "]", "#ddlCharts").remove(),
                    r.emit("removeIndicator", { userId: t, chartId: chartId }),
                    void 0 === $(".edit-indicators > li").html() ?
                    $(".indicator-edit-params").addClass("d-none") :
                    $(".edit-indicators > li:first-child").click(),
                    w(),
                    0 == $(".edit-indicators").children().length &&
                    $(".add-indicator-tab").click();
            }),
            $(".btn-save-indicator").on("click", function() {
                (chart = $(this).attr("data-chart-id")),
                (chartId = chart.slice(chart.indexOf("-") + 1)),
                "macd" == $("#container").highcharts().get(chart).type ?
                    $("#container")
                    .highcharts()
                    .get(chart)
                    .update({
                        params: {
                            period: parseInt($("#txtEditPeriod").val()),
                            shortPeriod: parseInt(
                                $("#txtEditShortPeriod").val()
                            ),
                            longPeriod: parseInt(
                                $("#txtEditLongPeriod").val()
                            ),
                            signalPeriod: parseInt(
                                $("#txtEditSignalPeriod").val()
                            ),
                        },
                    }) :
                    $("#container")
                    .highcharts()
                    .get(chart)
                    .update({
                        params: {
                            period: parseInt($("#txtEditPeriod").val()),
                        },
                    }),
                    $(".edit-indicator-item[data-chartid=" + chart + "]").html(
                        $("#container").highcharts().get(chart).name
                    ),
                    $(".edit-indicator-item[data-chartid=" + chart + "]").attr(
                        "data-period",
                        $("#txtEditPeriod").val()
                    ),
                    (userOptions = $("#container")
                        .highcharts()
                        .get(chart).userOptions),
                    (options = null),
                    "macd" == userOptions.type ?
                    (options = {
                        id: parseInt(chartId),
                        type: userOptions.type,
                        linkedTo: "candle",
                        yAxis: 2,
                        params: {
                            period: parseInt($("#txtEditPeriod").val()),
                            shortPeriod: parseInt(
                                $("#txtEditShortPeriod").val()
                            ),
                            longPeriod: parseInt(
                                $("#txtEditLongPeriod").val()
                            ),
                            signalPeriod: parseInt(
                                $("#txtEditSignalPeriod").val()
                            ),
                        },
                    }) :
                    (options = {
                        id: parseInt(chartId),
                        type: userOptions.type,
                        linkedTo: "candle",
                        params: {
                            period: parseInt($("#txtEditPeriod").val()),
                        },
                    }),
                    l.includes(userOptions.type) && (options.yAxis = 2),
                    r.emit("updateIndicator", {
                        userId: t,
                        chartId: chartId,
                        options: JSON.stringify(options),
                        name: $("#container").highcharts().get(chart).name,
                    });
            }),
            $(".refresh-demo-wallet").on("click", function() {
                $("#demo-amount").html(1e3),
                    sessionStorage.setItem("demo-amount", 1e3),
                    "Demo Account" == $(".account-label").html() &&
                    $(".account-amount").html(1e3),
                    r.emit("refreshDemoWallet", { userId: t });
            }),
            $(".btn-alert").on("click", function() {
                w(),
                    $(".remove-alerts-title").html(
                        $(".remove-alerts > li").attr("data-title")
                    ),
                    (first = $(".remove-alerts > li").html()),
                    "undefined" != typeof first ?
                    ((editVal = first.slice(
                            first.indexOf("(") + 1,
                            first.indexOf(")")
                        )),
                        $("#txtEditPeriod").val(editVal),
                        $(".alerts-remove-params").removeClass("d-none")) :
                    $(".alerts-remove-params").addClass("d-none"),
                    (date = new Date(new Date().getTime() + 864e5)),
                    (defaultDate =
                        date.getFullYear() +
                        "-" +
                        (date.getMonth() + 1).toString().padStart(2, "0") +
                        "-" +
                        date.getDate().toString().padStart(2, "0")),
                    $("#txtIndicatorDate").val(defaultDate),
                    $("#txtPriceDate").val(defaultDate),
                    (date = new Date(new Date().getTime() + 5184e6)),
                    (maxDate =
                        date.getFullYear() +
                        "-" +
                        (date.getMonth() + 1).toString().padStart(2, "0") +
                        "-" +
                        date.getDate().toString().padStart(2, "0")),
                    $("#txtIndicatorDate").attr("max", maxDate),
                    $("#txtPriceDate").attr("max", maxDate),
                    $("#alerts-modal").modal(),
                    $(".remove-alerts > li:first-child").click();
            }),
            $(".btn-add-alert").on("click", function() {
                switch (
                    ((type = parseInt($(this).attr("data-type"))),
                        (data = {
                            userId: t,
                            type: type,
                            coin: $("#coin").val(),
                            timestamp: new Date().getTime(),
                        }),
                        type)
                ) {
                    case 0:
                        (data.chart = $("#ddlIndicators").val()),
                        (data.condition = parseInt(
                            $("#ddlIndicatorConditions").val()
                        )),
                        (data.expiry = new Date(
                            $("#txtIndicatorDate").val()
                        ).getTime()),
                        (data.price = 0),
                        (data.prev = 0);
                        break;
                    case 1:
                        (data.chart = "candle"),
                        (data.condition = parseInt(
                            $("#ddlPriceConditions").val()
                        )),
                        (data.expiry = new Date(
                            $("#txtPriceDate").val()
                        ).getTime()),
                        (data.price = parseInt($("#txtPriceValue").val())),
                        (data.prev = $("#lblValue text").html());
                }
                r.emit("alert", data);
            }),
            $(".btn-remove-alert").on("click", function() {
                (id = $(this).attr("data-id")),
                r.emit("removeAlert", { id: id }),
                    $(".remove-alert-item[data-alertid=" + id + "]").remove(),
                    $(".remove-alert-item[data-alertid=" + id + "]").click(),
                    void 0 === $(".remove-alerts > li").html() ?
                    $(".alerts-remove-params").addClass("d-none") :
                    $(".remove-alerts > li:first-child").click(),
                    w(),
                    0 == $(".remove-alerts").children().length &&
                    $(".add-alert-tab").click();
            }),
            $("#ddlType").on("change", function() {
                $(".page-loader").css("display", "block"),
                    $("#coin").val($(this).val()),
                    $("#activity").val($(this).val()),
                    $.ajax({
                        url: BASE_URL + "trading/update_coin",
                        type: "POST",
                        data: { coin: $(this).val() },
                        success: function(t) {
                            location.reload(!0);
                        },
                    });
            });
    }),
    $(document).on("click", ".edit-indicator-item", function() {
        $(".edit-indicator-item").removeClass("active"),
            $(this).addClass("active"),
            (chart = $(this).attr("data-id")),
            (chartId = $(this).attr("data-chartid")),
            (title = $(this).attr("data-title")),
            (period = $(this).attr("data-period")),
            (shortPeriod = $(this).attr("data-shortperiod")),
            (longPeriod = $(this).attr("data-longperiod")),
            (signalPeriod = $(this).attr("data-signalperiod")),
            $(".btn-remove-indicator").attr("data-chart-id", chartId),
            $(".btn-save-indicator").attr("data-chart-id", chartId),
            $("#txtEditIndicator").val(chartId),
            $(".edit-indicator-title").html(title),
            $("#txtEditPeriod").val(period),
            $("#txtEditIndicator").val(chart),
            "macd" == chart ?
            ($(".normal-edit-params").removeClass("d-none"),
                $(".extra-edit-params").removeClass("d-none"),
                $("#txtEditShortPeriod").val(shortPeriod),
                $("#txtEditLongPeriod").val(longPeriod),
                $("#txtEditSignalPeriod").val(signalPeriod)) :
            "psar" == chart ?
            $(".normal-edit-params").addClass("d-none") :
            ($(".normal-edit-params").removeClass("d-none"),
                $(".extra-edit-params").addClass("d-none"));
    }),
    $(document).on("click", ".remove-alert-item", function() {
        $(".remove-alert-item").removeClass("active"),
            $(this).addClass("active"),
            (id = $(this).attr("data-alertid")),
            (condition = $(this).attr("data-condition")),
            (type = $(this).attr("data-type")),
            (content = `<div class="form-group row">\n        <div class="col-sm-12">\n            <input type="text" class="form-control text-d9d9d9" value="Status: ${$(
                this
            ).attr(
                "data-status"
            )}" disabled />\n        </div>\n    </div>\n    <div class="form-group row">\n        <div class="col-sm-12">\n            <input type="text" class="form-control text-d9d9d9" value="Chart: ${
                "candle" == type ? "Price" : type.toUpperCase()
            }" disabled />\n        </div>\n    </div>\n    <div class="form-group row">\n        <div class="col-sm-12">\n            <input type="text" class="form-control text-d9d9d9" value="Condition: ${
                0 == condition
                    ? "Crossing"
                    : 1 == condition
                    ? "Crossing up"
                    : "Crossing down"
            }" disabled />\n        </div>\n    </div>\n    ${
                "candle" == type
                    ? '<div class="form-group row"><div class="col-sm-12"><input type="text" id="txtAlertValue" class="form-control text-d9d9d9" value="Value: ' +
                      $(this).attr("data-value") +
                      '" disabled /></div></div>'
                    : ""
            }\n    <div class="form-group row">\n        <div class="col-sm-12">\n            <input type="text" class="form-control text-d9d9d9" value="Expiry: ${$(
                this
            ).attr("data-expiry")}" disabled />\n        </div>\n    </div>`),
            $(".remove-options").html(content),
            $(".btn-remove-alert").attr("data-id", id);
    });
