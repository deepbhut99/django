var pwt = angular.module('pwt', ['ui.mask']);
var config = {
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
    }
};

var toast = { timeOut: 5e3, closeButton: !0, debug: !1, newestOnTop: !0, progressBar: !0, positionClass: "toast-top-right", preventDuplicates: !0, onclick: null, showDuration: "300", hideDuration: "1000", extendedTimeOut: "1000", showEasing: "swing", hideEasing: "linear", showMethod: "fadeIn", hideMethod: "fadeOut", tapToDismiss: !1 };
var binanceSocket1 = new WebSocket("wss://stream.binance.com:9443/ws/!ticker@arr");
var priseopen = 0;
var pricechangepercentage = 0;
binanceSocket1.onmessage = function(event) {
    var message = JSON.parse(event.data);
    for (const ele of message) {
        if (ele.s === "1INCHUSDT") {
            priseclose = parseFloat(ele.c)
            pricechangepercentage = parseFloat(ele.P)
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#1INCHUSDT-h").text(ele.s);

            $("#1INCHUSDT-p").text(pricechangepercentage + "%");


            $("#1INCHUSDT-c").text(priseopenf);
        }
        if (ele.s === "AAVEUSDT") {
            priseclose = parseFloat(ele.c)
            pricechangepercentage = parseFloat(ele.P)
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#AAVEUSDT-h").text(ele.s);
            $("#AAVEUSDT-p").text(pricechangepercentage + "%");
            $("#AAVEUSDT-c").text(priseopenf);

        }
        if (ele.s === "ADAUSDT") {
            priseclose = parseFloat(ele.c)
            pricechangepercentage = parseFloat(ele.P)
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#ADAUSDT-h").text(ele.s);
            $("#ADAUSDT-p").text(pricechangepercentage + "%");
            $("#ADAUSDT-c").text(priseopenf);

        }


        if (ele.s === "AKROUSDT") {
            priseclose = parseFloat(ele.c)
            pricechangepercentage = parseFloat(ele.P)
            priseopenf = priseclose.toFixed(6);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#AKROUSDT-h").text(ele.s);
            $("#AKROUSDT-p").text(pricechangepercentage + "%");
            $("#AKROUSDT-c").text(priseopenf);

        }

        if (ele.s === "ANTUSDT") {
            priseclose = parseFloat(ele.c)
            pricechangepercentage = parseFloat(ele.P)
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#ANTUSDT-h").text(ele.s);
            $("#ANTUSDT-p").text(pricechangepercentage + "%");
            $("#ANTUSDT-c").text(priseopenf);

        }

        if (ele.s === "ATOMUSDT") {
            priseclose = parseFloat(ele.c)
            pricechangepercentage = parseFloat(ele.P)
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#ATOMUSDT-h").text(ele.s);
            $("#ATOMUSDT-p").text(pricechangepercentage + "%");
            $("#ATOMUSDT-c").text(priseopenf);

        }
        if (ele.s === "BATUSDT") {
            priseclose = parseFloat(ele.c)
            pricechangepercentage = parseFloat(ele.P)
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#BATUSDT-h").text(ele.s);
            $("#BATUSDT-p").text(pricechangepercentage + "%");
            $("#BATUSDT-c").text(priseopenf);

        }
        if (ele.s === "BCHUSDT") {
            priseclose = parseFloat(ele.c)
            pricechangepercentage = parseFloat(ele.P)
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#BCHUSDT-h").text(ele.s);
            $("#BCHUSDT-p").text(pricechangepercentage + "%");
            $("#BCHUSDT-c").text(priseopenf);

        }
        if (ele.s === "BNBUSDT") {
            priseclose = parseFloat(ele.c)
            pricechangepercentage = parseFloat(ele.P)
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#BNBUSDT-h").text(ele.s);
            $("#BNBUSDT-p").text(pricechangepercentage + "%");
            $("#BNBUSDT-c").text(priseopenf);

        }

        if (ele.s === "BTCUSDT") {
            priseclose = parseFloat(ele.c)
            pricechangepercentage = parseFloat(ele.P)
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#BTCUSDT-h").text(ele.s);
            $("#BTCUSDT-p").text(pricechangepercentage + "%");
            $("#BTCUSDT-c").text(priseopenf);

        }
        if (ele.s === "CAKEUSDT") {
            priseclose = parseFloat(ele.c)
            pricechangepercentage = parseFloat(ele.P)
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#CAKEUSDT-h").text(ele.s);
            $("#CAKEUSDT-p").text(pricechangepercentage + "%");
            $("#CAKEUSDT-c").text(priseopenf);

        }
        if (ele.s === "COMPUSDT") {
            priseclose = parseFloat(ele.c)
            pricechangepercentage = parseFloat(ele.P)
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#COMPUSDT-h").text(ele.s);
            $("#COMPUSDT-p").text(pricechangepercentage + "%");
            $("#COMPUSDT-c").text(priseopenf);

        }
        if (ele.s === "COSUSDT") {
            priseclose = parseFloat(ele.c)
            pricechangepercentage = parseFloat(ele.P)
            priseopenf = priseclose.toFixed(6);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#COSUSDT-h").text(ele.s);
            $("#COSUSDT-p").text(pricechangepercentage + "%");
            $("#COSUSDT-c").text(priseopenf);

        }
        if (ele.s === "CRVUSDT") {
            priseclose = parseFloat(ele.c)
            pricechangepercentage = parseFloat(ele.P)
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#CRVUSDT-h").text(ele.s);
            $("#CRVUSDT-p").text(pricechangepercentage + "%");
            $("#CRVUSDT-c").text(priseopenf);

        }
        if (ele.s === "DASHUSDT") {
            priseclose = parseFloat(ele.c)
            pricechangepercentage = parseFloat(ele.P)
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#DASHUSDT-h").text(ele.s);
            $("#DASHUSDT-p").text(pricechangepercentage + "%");
            $("#DASHUSDT-c").text(priseopenf);

        }
        if (ele.s === "DOGEUSDT") {
            priseclose = parseFloat(ele.c)
            pricechangepercentage = parseFloat(ele.P)
            priseopenf = priseclose.toFixed(6);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#DOGEUSDT-h").text(ele.s);
            $("#DOGEUSDT-p").text(pricechangepercentage + "%");
            $("#DOGEUSDT-c").text(priseopenf);

        }
        if (ele.s === "DOTUSDT") {
            priseclose = parseFloat(ele.c)
            pricechangepercentage = parseFloat(ele.P)
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#DOTUSDT-h").text(ele.s);
            $("#DOTUSDT-p").text(pricechangepercentage + "%");
            $("#DOTUSDT-c").text(priseopenf);

        }
        if (ele.s === "EOSUSDT") {
            priseclose = parseFloat(ele.c)
            pricechangepercentage = parseFloat(ele.P)
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#EOSUSDT-h").text(ele.s);
            $("#EOSUSDT-p").text(pricechangepercentage + "%");
            $("#EOSUSDT-c").text(priseopenf);

        }
        if (ele.s === "ETHUSDT") {
            priseclose = parseFloat(ele.c)
            pricechangepercentage = parseFloat(ele.P)
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#ETHUSDT-h").text(ele.s);
            $("#ETHUSDT-p").text(pricechangepercentage + "%");
            $("#ETHUSDT-c").text(priseopenf);

        }
        if (ele.s === "ETCUSDT") {
            priseclose = parseFloat(ele.c)
            pricechangepercentage = parseFloat(ele.P)
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#ETCUSDT-h").text(ele.s);
            $("#ETCUSDT-p").text(pricechangepercentage + "%");
            $("#ETCUSDT-c").text(priseopenf);

        }
        if (ele.s === "FILUSDT") {
            priseclose = parseFloat(ele.c)
            pricechangepercentage = parseFloat(ele.P)
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#FILUSDT-h").text(ele.s);
            $("#FILUSDT-p").text(pricechangepercentage + "%");
            $("#FILUSDT-c").text(priseopenf);

        }
        if (ele.s === "FTTUSDT") {
            priseclose = parseFloat(ele.c)
            pricechangepercentage = parseFloat(ele.P)
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#FTTUSDT-h").text(ele.s);
            $("#FTTUSDT-p").text(pricechangepercentage + "%");
            $("#FTTUSDT-c").text(priseopenf);

        }
        if (ele.s === "GRTUSDT") {
            priseclose = parseFloat(ele.c)
            pricechangepercentage = parseFloat(ele.P)
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#GRTUSDT-h").text(ele.s);
            $("#GRTUSDT-p").text(pricechangepercentage + "%");
            $("#GRTUSDT-c").text(priseopenf);

        }
        if (ele.s === "IOSTUSDT") {
            priseclose = parseFloat(ele.c)
            pricechangepercentage = parseFloat(ele.P)
            priseopenf = priseclose.toFixed(6);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#IOSTUSDT-h").text(ele.s);
            $("#IOSTUSDT-p").text(pricechangepercentage + "%");
            $("#IOSTUSDT-c").text(priseopenf);

        }
        if (ele.s === "IOTAUSDT") {
            priseclose = parseFloat(ele.c)
            pricechangepercentage = parseFloat(ele.P)
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#IOTAUSDT-h").text(ele.s);
            $("#IOTAUSDT-p").text(pricechangepercentage + "%");
            $("#IOTAUSDT-c").text(priseopenf);

        }
        if (ele.s === "JSTUSDT") {
            priseclose = parseFloat(ele.c)
            pricechangepercentage = parseFloat(ele.P)
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#JSTUSDT-h").text(ele.s);
            $("#JSTUSDT-p").text(pricechangepercentage + "%");
            $("#JSTUSDT-c").text(priseopenf);

        }
        if (ele.s === "KAVAUSDT") {
            priseclose = parseFloat(ele.c)
            pricechangepercentage = parseFloat(ele.P)
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#KAVAUSDT-h").text(ele.s);
            $("#KAVAUSDT-p").text(pricechangepercentage + "%");
            $("#KAVAUSDT-c").text(priseopenf);

        }
        if (ele.s === "LINKUSDT") {
            priseclose = parseFloat(ele.c)
            pricechangepercentage = parseFloat(ele.P)
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#LINKUSDT-h").text(ele.s);
            $("#LINKUSDT-p").text(pricechangepercentage + "%");
            $("#LINKUSDT-c").text(priseopenf);

        }
        if (ele.s === "LITUSDT") {
            priseclose = parseFloat(ele.c)
            pricechangepercentage = parseFloat(ele.P)
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#LITUSDT-h").text(ele.s);
            $("#LITUSDT-p").text(pricechangepercentage + "%");
            $("#LITUSDT-c").text(priseopenf);

        }
        if (ele.s === "LTCUSDT") {
            priseclose = parseFloat(ele.c)
            pricechangepercentage = parseFloat(ele.P)
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#LTCUSDT-h").text(ele.s);
            $("#LTCUSDT-p").text(pricechangepercentage + "%");
            $("#LTCUSDT-c").text(priseopenf);

        }
        if (ele.s === "MANAUSDT") {
            priseclose = parseFloat(ele.c)
            pricechangepercentage = parseFloat(ele.P)
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#MANAUSDT-h").text(ele.s);
            $("#MANAUSDT-p").text(pricechangepercentage + "%");
            $("#MANAUSDT-c").text(priseopenf);

        }
        if (ele.s === "MDXUSDT") {
            priseclose = parseFloat(ele.c)
            pricechangepercentage = parseFloat(ele.P)
            priseopenf = priseclose.toFixed(6);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#MDXUSDT-h").text(ele.s);
            $("#MDXUSDT-p").text(pricechangepercentage + "%");
            $("#MDXUSDT-c").text(priseopenf);

        }
        if (ele.s === "NEOUSDT") {
            priseclose = parseFloat(ele.c)
            pricechangepercentage = parseFloat(ele.P)
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#NEOUSDT-h").text(ele.s);
            $("#NEOUSDT-p").text(pricechangepercentage + "%");
            $("#NEOUSDT-c").text(priseopenf);

        }
        if (ele.s === "OMGUSDT") {
            priseclose = parseFloat(ele.c)
            pricechangepercentage = parseFloat(ele.P)
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#OMGUSDT-h").text(ele.s);
            $("#OMGUSDT-p").text(pricechangepercentage + "%");
            $("#OMGUSDT-c").text(priseopenf);

        }
        if (ele.s === "SUSHIUSDT") {
            priseclose = parseFloat(ele.c)
            pricechangepercentage = parseFloat(ele.P)
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#SUSHIUSDT-h").text(ele.s);
            $("#SUSHIUSDT-p").text(pricechangepercentage + "%");
            $("#SUSHIUSDT-c").text(priseopenf);

        }
        if (ele.s === "THETAUSDT") {
            priseclose = parseFloat(ele.c)
            pricechangepercentage = parseFloat(ele.P)
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#THETAUSDT-h").text(ele.s);
            $("#THETAUSDT-p").text(pricechangepercentage + "%");
            $("#THETAUSDT-c").text(priseopenf);

        }
        if (ele.s === "TRXUSDT") {
            priseclose = parseFloat(ele.c)
            pricechangepercentage = parseFloat(ele.P)
            priseopenf = priseclose.toFixed(6);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#TRXUSDT-h").text(ele.s);
            $("#TRXUSDT-p").text(pricechangepercentage + "%");
            $("#TRXUSDT-c").text(priseopenf);

        }
        if (ele.s === "UNIUSDT") {
            priseclose = parseFloat(ele.c)
            pricechangepercentage = parseFloat(ele.P)
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#UNIUSDT-h").text(ele.s);
            $("#UNIUSDT-p").text(pricechangepercentage + "%");
            $("#UNIUSDT-c").text(priseopenf);

        }
        if (ele.s === "XMRUSDT") {
            priseclose = parseFloat(ele.c)
            pricechangepercentage = parseFloat(ele.P)
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#XMRUSDT-h").text(ele.s);
            $("#XMRUSDT-p").text(pricechangepercentage + "%");
            $("#XMRUSDT-c").text(priseopenf);

        }
        if (ele.s === "XRPUSDT") {
            priseclose = parseFloat(ele.c)
            pricechangepercentage = parseFloat(ele.P)
            priseopenf = priseclose.toFixed(6);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#XRPUSDT-h").text(ele.s);
            $("#XRPUSDT-p").text(pricechangepercentage + "%");
            $("#XRPUSDT-c").text(priseopenf);

        }
        if (ele.s === "XTZUSDT") {
            priseclose = parseFloat(ele.c)
            pricechangepercentage = parseFloat(ele.P)
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#XTZUSDT-h").text(ele.s);
            $("#XTZUSDT-p").text(pricechangepercentage + "%");
            $("#XTZUSDT-c").text(priseopenf);

        }
        if (ele.s === "RVNUSDT") {
            priseclose = parseFloat(ele.c)
            pricechangepercentage = parseFloat(ele.P)
            priseopenf = priseclose.toFixed(6);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#RVNUSDT-h").text(ele.s);
            $("#RVNUSDT-p").text(pricechangepercentage + "%");
            $("#RVNUSDT-c").text(priseopenf);

        }
        if (ele.s === "SHIBUSDT") {
            priseclose = parseFloat(ele.c)
            pricechangepercentage = parseFloat(ele.P)
            priseopenf = priseclose.toFixed(8);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#SHIBUSDT-h").text(ele.s);
            $("#SHIBUSDT-p").text(pricechangepercentage + "%");
            $("#SHIBUSDT-c").text(priseopenf);

        }
        if (ele.s === "MATICUSDT") {
            priseclose = parseFloat(ele.c)
            pricechangepercentage = parseFloat(ele.P)
            priseopenf = priseclose.toFixed(3);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#MATICUSDT-h").text(ele.s);
            $("#MATICUSDT-p").text(pricechangepercentage + "%");
            $("#MATICUSDT-c").text(priseopenf);

        }
        if (ele.s === "CELRUSDT") {
            priseclose = parseFloat(ele.c)
            pricechangepercentage = parseFloat(ele.P)
            priseopenf = priseclose.toFixed(6);
            pricechangepercentage = pricechangepercentage.toFixed(2);
            $("#CELRUSDT-h").text(ele.s);
            $("#CELRUSDT-p").text(pricechangepercentage + "%");
            $("#CELRUSDT-c").text(priseopenf);

        }

    }
}
const App_Controller = ($scope, $http) => {

    $scope.loader = true;
    $scope.get_notification = () => {
        $http.get(BASE_URL + 'account/get_notification').then((res) => {
            $scope.notifications = res.data.notifications;
            $scope.pending_noti = res.data.total;
            $scope.loader = false;
        })
    }

    $scope.get_notification();

    $scope.get_auth_status = () => {
        $http.get(BASE_URL + 'trade/account/get_auth_status').then((res) => {
            $scope.auths = res.data;
            if ($scope.auths.google_auth_status == 'true') {
                $scope.authsuccess = true;
                $scope.google_auth_status = true;
            } else {
                $scope.authsuccess = false;
                $scope.google_auth_status = false;
            }
        })
    }

    $scope.show_withdrawal = (type) => {
        $scope.withdraw_type = type;
        $scope.addressLoader = true
        $http.get(BASE_URL + 'trade/account/get_address/').then((res) => {
            $scope.type = type;
            $scope.fee = 25;
            $scope.minamount = 50
            $scope.withdraw_in = 'usdt';
            if (type == 'BTC') {
                $scope.address = res.data.btc_address;
            } else if (type == 'BNB') {
                $scope.address = res.data.ltc_address;
                $scope.fee = 5;
            } else if (type == 'ETH') {
                $scope.address = res.data.eth_address;
            } else if (type == 'USDTTRC20') {
                $scope.address = res.data.trx_address;
                $scope.fee = 0;
            } else if (type == 'USDTERC20') {
                $scope.address = res.data.eth_address;
                $scope.fee = 20;
            } else {
                $scope.fee = 0;
                // $scope.minamount = 50;
                $scope.withdraw_in = 'pwt';
                $scope.address = res.data.ltc_address;
            }
            $scope.addressLoader = false;
        });
        $('#modal_withdrawal').modal('show');
    }

    $scope.show_transfer = (type) => {
        $scope.transfer_type = type;
        if (type == 'pwt') {
            $scope.mintransfer = 20;
        } else {
            $scope.mintransfer = 50;
        }
        $('#model_transfer').modal('show');
    }

    $scope.pwt_withdrawal = (type) => {
        $scope.fee = type == 'pwt' ? 50 : 25;
    }

    $scope.show_deposit = (type) => {
        $scope.hide_modal();
        $scope.type = type;
        $scope.minDeposit = 20;
        $scope.payment_type = type;
        if (type == 'USDTTRC20') {
            $scope.minDeposit = 15;
        } else if (type == 'USDTERC20') {
            $scope.minDeposit = 30;
        }
        $('#modal_deposit').modal('show');
    }

    $scope.hide_modal = () => {
        $scope.amountf = "";
        $scope.address = "";
        $('#amount').value = "";
        $('#qrcode').text('');
        $('#address').val('');
        $('#data_form').removeClass('d-none');
        $('#payment_details').removeClass('d-block').addClass('d-none');
        // $('#payment_details');
        $('#modal_deposit').modal('hide');
    }

    $scope.exportData = (form) => {
        if (!form.$invalid) {
            $scope.csvLoader = true;
            var dataObj = $.param({
                startdate: $scope.startdate,
                enddate: $scope.enddate,
            });
            $http.post(BASE_URL + 'admin/trading/export_bids', dataObj, config).then(
                function(response) {
                    d = new Date();
                    var date = d.getDate() + '-' + d.getMonth() + '-' + d.getFullYear();
                    var time = d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                    var file = new Blob([response.data], {
                        type: 'application/csv'
                    });
                    var fileURL = URL.createObjectURL(file);
                    var a = document.createElement('a');
                    a.href = fileURL;
                    a.target = '_blank';
                    a.download = "Bids-" + date + '_' + time + '_earnfinex.csv';
                    document.body.appendChild(a);
                    a.click();
                    document.body.removeChild(a);
                    $scope.csvLoader = false;
                });
        }
    }

    $scope.export_user = (form) => {
        if (!form.$invalid) {
            $scope.csvLoader = true;
            var dataObj = $.param({
                startdate: $scope.startdate,
                enddate: $scope.enddate,
            });
            $("#history").html('<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>');
            $("#history").attr('disable', true);
            $http.post(BASE_URL + 'admin/user/export_user', dataObj, config).then(
                function(response) {
                    d = new Date();
                    var date = d.getDate() + '-' + d.getMonth() + '-' + d.getFullYear();
                    var time = d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                    var file = new Blob([response.data], {
                        type: 'application/csv'
                    });
                    var fileURL = URL.createObjectURL(file);
                    var a = document.createElement('a');
                    a.href = fileURL;
                    a.target = '_blank';
                    a.download = "Users-" + date + '_' + time + '_earnfinex.csv';
                    document.body.appendChild(a);
                    a.click();
                    document.body.removeChild(a);
                    $scope.csvLoader = false;
                    $("#history").html('<i class="fa fa-search" aria-hidden="true"></i>');
                    $("#history").attr('disable', false);
                });
        }
    }

    $scope.redirect_url = (url) => {
        window.location.href = BASE_URL + url;
    }

    $scope.mark_read = (id) => {
        $http.get(BASE_URL + 'read-notification/' + id).then(res => {
            window.location.href = res.data.target;
        });
    }

    $scope.mark_all_read = () => {
        $http.get(BASE_URL + 'trade-dashboard/mark_all_read').then(res => {
            $scope.get_notification();
        });
    }

    // $scope.close_ticket = (ticket_id) => {

    //     Swal.fire({
    //         title: "Are you sure?",
    //         text: "",
    //         type: "warning",
    //         showCancelButton: !0,
    //         confirmButtonColor: "#34c38f",
    //         cancelButtonColor: "#f46a6a",
    //         confirmButtonText: "Yes",
    //         confirmButtonClass: "btn btn-success mt-2",
    //         cancelButtonClass: "btn btn-danger ml-2 mt-2",
    //         buttonsStyling: !1,
    //     }).then(function(t) {
    //         if (t.value == true) {
    //             var data = $.param({
    //                 ticket_id: ticket_id
    //             });
    //             $http.post(BASE_URL + 'ticket/close_ticket', data, config).then((res) => {
    //                 if (res.data.success) {
    //                     location.reload();
    //                 } else {
    //                     toastr.error(res.data.message, '', toast);
    //                 }
    //             })
    //         } else {

    //         }
    //     });
    // }
}

const Dashboard_Controller = ($scope, $http) => {
    // $http.get(BASE_URL + 'trade/dashboard/get_bids').then((res) => {
    //     $scope.bids = res.data;
    // })
}

const CopyTrade_Controller = ($scope, $http) => {

    $scope.topLoader = true;
    $scope.topp_gainers = () => {
        $http.get(BASE_URL + 'trade/trading/top_gainers').then((res) => {
            $scope.gainers = res.data;
            $scope.topLoader = false;
            $scope.itemsPerPage = 6;
            $scope.currentPage = 0;
            $scope.range = function() {
                var rangeSize = 6;
                var ps = [];
                var start;

                start = $scope.currentPage;
                //  console.log($scope.pageCount(),$scope.currentPage)
                if (start > $scope.pageCount() - rangeSize) {
                    start = $scope.pageCount() - rangeSize + 1;
                }

                for (var i = start; i < start + rangeSize; i++) {
                    if (i >= 0) {
                        ps.push(i);
                    }
                }
                return ps;
            };
            $scope.prevPage = function() {
                if ($scope.currentPage > 0) {
                    $scope.currentPage--;
                }
            };
            $scope.DisablePrevPage = function() {
                return $scope.currentPage === 0 ? "disabled" : "";
            };
            $scope.nextPage = function() {
                if ($scope.currentPage < $scope.pageCount()) {
                    $scope.currentPage++;
                }
            };
            $scope.DisableNextPage = function() {
                return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
            };
            $scope.setPage = function(n) {
                $scope.currentPage = n;
            };
            $scope.pageCount = function() {
                return Math.ceil($scope.gainers.length / $scope.itemsPerPage) - 1;
            };
        })
    }

    $scope.topp_gainers();

    $scope.most_populars = () => {
        $scope.dataLoader = true;
        $http.get(BASE_URL + 'trade/trading/top_gainers/populars').then((res) => {
            $scope.populars = res.data;
            $scope.dataLoader = false;
            $scope.usersPerPage = 6;
            $scope.activePage = 0;
            $scope.usersrange = function() {
                var rangeSize = 6;
                var ps = [];
                var start;

                start = $scope.activePage;
                //  console.log($scope.pageCount(),$scope.activePage)
                if (start > $scope.pageUserCount() - rangeSize) {
                    start = $scope.pageUserCount() - rangeSize + 1;
                }

                for (var i = start; i < start + rangeSize; i++) {
                    if (i >= 0) {
                        ps.push(i);
                    }
                }
                return ps;
            };
            $scope.backPage = function() {
                if ($scope.activePage > 0) {
                    $scope.activePage--;
                }
            };
            $scope.DisableBackPage = function() {
                return $scope.activePage === 0 ? "disabled" : "";
            };
            $scope.nxtPage = function() {
                if ($scope.activePage < $scope.pageUserCount()) {
                    $scope.activePage++;
                }
            };
            $scope.DisableNxtPage = function() {
                return $scope.activePage === $scope.pageUserCount() ? "disabled" : "";
            };
            $scope.setActivePage = function(n) {
                $scope.activePage = n;
            };
            $scope.pageUserCount = function() {
                return Math.ceil($scope.populars.length / $scope.usersPerPage) - 1;
            };
        })
    }

    $scope.start_copy = (userid, profit_share, min_invest) => {
        console.log(userid + ' ' + profit_share + ' ' + min_invest);
        Swal.fire({
            title: "Are you sure?",
            text: "",
            type: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#34c38f",
            cancelButtonColor: "#f46a6a",
            confirmButtonText: "Yes",
            confirmButtonClass: "btn btn-success mt-2",
            cancelButtonClass: "btn btn-danger ml-2 mt-2",
            buttonsStyling: !1,
        }).then(function(t) {
            if (t.value == true) {
                var data = $.param({
                    userid: userid,
                    profit: profit_share,
                    min_invest: min_invest,
                });
                $http.post(BASE_URL + 'trade/trading/start_copy/', data, config).then(function(res) {
                    if (res.data.success == true) {
                        $scope.topp_gainers()
                        toastr.success(res.data.message);
                    } else {
                        toastr.error(res.data.message);
                    }
                })
            } else {

            }
        });

    }

    $scope.stop_copy = (userid) => {
        Swal.fire({
            title: "Are you sure?",
            text: "",
            type: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#34c38f",
            cancelButtonColor: "#f46a6a",
            confirmButtonText: "Yes",
            confirmButtonClass: "btn btn-success mt-2",
            cancelButtonClass: "btn btn-danger ml-2 mt-2",
            buttonsStyling: !1,
        }).then(function(t) {
            if (t.value == true) {
                var data = $.param({
                    userid: userid,
                });
                $http.post(BASE_URL + 'trade/trading/stop_copy/', data, config).then(function(res) {
                    if (res.data.success == true) {
                        $scope.topp_gainers()
                        toastr.success(res.data.message);
                    } else {
                        toastr.error(res.data.message);
                    }
                })
            } else {

            }
        });

    }

    $scope.get_portfolio_lists = () => {
        $scope.listLoader = true;
        $http.get(BASE_URL + 'trade/trading/get_portfolio_lists').then((res) => {
            $scope.lists = res.data;
            $scope.listLoader = false;
            $scope.itemsPerPage = 6;
            $scope.currentPage = 0;
            $scope.range = function() {
                var rangeSize = 6;
                var ps = [];
                var start;

                start = $scope.currentPage;
                //  console.log($scope.pageCount(),$scope.currentPage)
                if (start > $scope.pageCount() - rangeSize) {
                    start = $scope.pageCount() - rangeSize + 1;
                }

                for (var i = start; i < start + rangeSize; i++) {
                    if (i >= 0) {
                        ps.push(i);
                    }
                }
                return ps;
            };
            $scope.prevPage = function() {
                if ($scope.currentPage > 0) {
                    $scope.currentPage--;
                }
            };
            $scope.DisablePrevPage = function() {
                return $scope.currentPage === 0 ? "disabled" : "";
            };
            $scope.nextPage = function() {
                if ($scope.currentPage < $scope.pageCount()) {
                    $scope.currentPage++;
                }
            };
            $scope.DisableNextPage = function() {
                return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
            };
            $scope.setPage = function(n) {
                $scope.currentPage = n;
            };
            $scope.pageCount = function() {
                return Math.ceil($scope.lists.length / $scope.itemsPerPage) - 1;
            };
        })
    }
}
const CopyTrade_Controller1 = ($scope, $http) => {

    $scope.topLoader = true;
    $scope.topp_gainers = () => {
        $http.get(BASE_URL + 'trade/trading/top_gainers').then((res) => {
            $scope.gainers = res.data;
            $scope.topLoader = false;
            $scope.itemsPerPage = 6;
            $scope.currentPage = 0;
            $scope.range = function() {
                var rangeSize = 6;
                var ps = [];
                var start;

                start = $scope.currentPage;
                //  console.log($scope.pageCount(),$scope.currentPage)
                if (start > $scope.pageCount() - rangeSize) {
                    start = $scope.pageCount() - rangeSize + 1;
                }

                for (var i = start; i < start + rangeSize; i++) {
                    if (i >= 0) {
                        ps.push(i);
                    }
                }
                return ps;
            };
            $scope.prevPage = function() {
                if ($scope.currentPage > 0) {
                    $scope.currentPage--;
                }
            };
            $scope.DisablePrevPage = function() {
                return $scope.currentPage === 0 ? "disabled" : "";
            };
            $scope.nextPage = function() {
                if ($scope.currentPage < $scope.pageCount()) {
                    $scope.currentPage++;
                }
            };
            $scope.DisableNextPage = function() {
                return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
            };
            $scope.setPage = function(n) {
                $scope.currentPage = n;
            };
            $scope.pageCount = function() {
                return Math.ceil($scope.gainers.length / $scope.itemsPerPage) - 1;
            };
        })
    }

    $scope.topp_gainers();

    $scope.most_populars = () => {
        $scope.dataLoader = true;
        $http.get(BASE_URL + 'trade/trading/top_gainers/populars').then((res) => {
            $scope.populars = res.data;
            $scope.dataLoader = false;
            $scope.usersPerPage = 6;
            $scope.activePage = 0;
            $scope.usersrange = function() {
                var rangeSize = 6;
                var ps = [];
                var start;

                start = $scope.activePage;
                //  console.log($scope.pageCount(),$scope.activePage)
                if (start > $scope.pageUserCount() - rangeSize) {
                    start = $scope.pageUserCount() - rangeSize + 1;
                }

                for (var i = start; i < start + rangeSize; i++) {
                    if (i >= 0) {
                        ps.push(i);
                    }
                }
                return ps;
            };
            $scope.backPage = function() {
                if ($scope.activePage > 0) {
                    $scope.activePage--;
                }
            };
            $scope.DisableBackPage = function() {
                return $scope.activePage === 0 ? "disabled" : "";
            };
            $scope.nxtPage = function() {
                if ($scope.activePage < $scope.pageUserCount()) {
                    $scope.activePage++;
                }
            };
            $scope.DisableNxtPage = function() {
                return $scope.activePage === $scope.pageUserCount() ? "disabled" : "";
            };
            $scope.setActivePage = function(n) {
                $scope.activePage = n;
            };
            $scope.pageUserCount = function() {
                return Math.ceil($scope.populars.length / $scope.usersPerPage) - 1;
            };
        })
    }

    $scope.start_copy = (userid, profit_share, min_invest) => {
        console.log(userid + ' ' + profit_share + ' ' + min_invest);
        Swal.fire({
            title: "Are you sure?",
            text: "",
            type: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#34c38f",
            cancelButtonColor: "#f46a6a",
            confirmButtonText: "Yes",
            confirmButtonClass: "btn btn-success mt-2",
            cancelButtonClass: "btn btn-danger ml-2 mt-2",
            buttonsStyling: !1,
        }).then(function(t) {
            if (t.value == true) {
                var data = $.param({
                    userid: userid,
                    profit: profit_share,
                    min_invest: min_invest,
                });
                $http.post(BASE_URL + 'trade/trading/start_copy/', data, config).then(function(res) {
                    if (res.data.success == true) {
                        $scope.topp_gainers()
                        toastr.success(res.data.message);
                    } else {
                        toastr.error(res.data.message);
                    }
                })
            } else {

            }
        });

    }

    $scope.stop_copy = (userid) => {
        Swal.fire({
            title: "Are you sure?",
            text: "",
            type: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#34c38f",
            cancelButtonColor: "#f46a6a",
            confirmButtonText: "Yes",
            confirmButtonClass: "btn btn-success mt-2",
            cancelButtonClass: "btn btn-danger ml-2 mt-2",
            buttonsStyling: !1,
        }).then(function(t) {
            if (t.value == true) {
                var data = $.param({
                    userid: userid,
                });
                $http.post(BASE_URL + 'trade/trading/stop_copy/', data, config).then(function(res) {
                    if (res.data.success == true) {
                        $scope.topp_gainers()
                        toastr.success(res.data.message);
                    } else {
                        toastr.error(res.data.message);
                    }
                })
            } else {

            }
        });

    }

    $scope.get_portfolio_lists = () => {
        $scope.listLoader = true;
        $http.get(BASE_URL + 'trade/trading/get_portfolio_lists').then((res) => {
            $scope.lists = res.data;
            $scope.listLoader = false;
            $scope.itemsPerPage = 6;
            $scope.currentPage = 0;
            $scope.range = function() {
                var rangeSize = 6;
                var ps = [];
                var start;

                start = $scope.currentPage;
                //  console.log($scope.pageCount(),$scope.currentPage)
                if (start > $scope.pageCount() - rangeSize) {
                    start = $scope.pageCount() - rangeSize + 1;
                }

                for (var i = start; i < start + rangeSize; i++) {
                    if (i >= 0) {
                        ps.push(i);
                    }
                }
                return ps;
            };
            $scope.prevPage = function() {
                if ($scope.currentPage > 0) {
                    $scope.currentPage--;
                }
            };
            $scope.DisablePrevPage = function() {
                return $scope.currentPage === 0 ? "disabled" : "";
            };
            $scope.nextPage = function() {
                if ($scope.currentPage < $scope.pageCount()) {
                    $scope.currentPage++;
                }
            };
            $scope.DisableNextPage = function() {
                return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
            };
            $scope.setPage = function(n) {
                $scope.currentPage = n;
            };
            $scope.pageCount = function() {
                return Math.ceil($scope.lists.length / $scope.itemsPerPage) - 1;
            };
        })
    }
    $scope.get_portfolio_lists();
}

const Profile_Controller = ($scope, $http) => {

    $scope.get_auth_status();

    $scope.get_address = () => {
        $http.get(BASE_URL + 'trade/account/get_address').then((res) => {
            $scope.address = res.data;
            $scope.eth_address = res.data.eth_address;
            $scope.btc_address = res.data.btc_address;
            $scope.ltcaddress = res.data.ltc_address;
            $scope.trx_address = res.data.trx_address;
        });
    }

    $scope.get_address();

    $scope.set_eth_address = () => {
        $scope.saving = true;
        if (!$scope) {
            var data = $.param({
                eth_address: '',
            });
        } else {
            var data = $.param({
                eth_address: $scope.eth_address,
            });
        }
        $http.post(BASE_URL + 'trade/account/set_eth_address/', data, config).then(function(res) {
            if (res.data.success == true) {
                $scope.get_address();
                toastr.success(res.data.message);
            } else {
                toastr.error(res.data.message);
            }
        })
    }

    $scope.set_btc_address = () => {
        if (!$scope) {
            var data = $.param({
                btc_address: '',
            });
        } else {
            var data = $.param({
                btc_address: $scope.btc_address,
            });
        }
        $http.post(BASE_URL + 'trade/account/set_btc_address/', data, config).then(function(res) {
            if (res.data.success == true) {
                $scope.get_address();
                toastr.success(res.data.message);
            } else {
                toastr.error(res.data.message);
            }
        })
    }

    $scope.set_ltc_address = () => {
        if (!$scope) {
            var data = $.param({
                ltc_address: '',
            });
        } else {
            var data = $.param({
                ltc_address: $scope.ltcaddress,
            });
        }
        $http.post(BASE_URL + 'trade/account/set_ltc_address/', data, config).then(function(res) {
            if (res.data.success == true) {
                $scope.get_address();
                toastr.success(res.data.message);
            } else {
                toastr.error(res.data.message);
            }
        })
    }

    $scope.set_trx_address = () => {
        if (!$scope) {
            var data = $.param({
                trx_address: '',
            });
        } else {
            var data = $.param({
                trx_address: $scope.trx_address,
            });
        }
        $http.post(BASE_URL + 'trade/account/set_trx_address/', data, config).then(function(res) {
            if (res.data.success == true) {
                $scope.get_address();
                toastr.success(res.data.message);
            } else {
                toastr.error(res.data.message);
            }
        })
    }

    $scope.reset_wallet_address = (address) => {
        var data = $.param({
            wallet_address: address,
        });
        $http.post(BASE_URL + 'trade/account/reset_wallet_address/', data, config).then(function(res) {
            if (res.data.success == true) {
                $scope.get_address();
                toastr.success(res.data.message, "", toast);
            } else {
                toastr.error(res.data.message, "", toast);
            }
        })
    }
    $scope.reset_binance_address = () => {
        var data = $.param({

        });
        $http.post(BASE_URL + 'trade/account/reset_binance_address/', data, config).then(function(res) {
            if (res.data.success == true) {
                $scope.get_address();
                toastr.success(res.data.message, "", toast);
            } else {
                toastr.error(res.data.message, "", toast);
            }
        })
    }

    $scope.change_password = () => {
        Swal.fire({
            title: "Are you sure?",
            text: "",
            type: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#34c38f",
            cancelButtonColor: "#f46a6a",
            confirmButtonText: "Yes",
            confirmButtonClass: "btn btn-success mt-2",
            cancelButtonClass: "btn btn-danger ml-2 mt-2",
            buttonsStyling: !1,
        }).then(function(t) {
            if (t.value == true) {
                $scope.update_password();
            } else {

            }
        });
    }

    $scope.update_password = () => {
        if (!$scope) {
            var data = $.param({
                old_password: '',
                new_password: '',
                confirm_password: ''
            });
        } else {
            var data = $.param({
                old_password: $scope.old_password,
                new_password: $scope.new_password,
                confirm_password: $scope.confirm_password,
            });
        }
        $http.post(BASE_URL + 'trade/account/update_password/', data, config).then(function(res) {
            if (res.data.success == true) {
                toastr.success(res.data.message);
                $scope.old_password = '';
                $scope.new_password = '';
                $scope.confirm_password = '';
            } else {
                toastr.error(res.data.message);
            }
        })
    }

    $scope.set_auth = () => {

        if ($scope.google_auth_status == true) {
            $http.get(BASE_URL + 'trade/account/set_auth_key').then((res) => {
                $scope.auth = res.data;
                $scope.authsuccess = false;
            })
        }
    }

    $scope.check_auth = () => {

        if (!$scope) {
            var data = $.param({
                code: '',
                status: 'true'
            })
        } else {
            var data = $.param({
                code: $scope.code,
                status: 'true'
            });
        }
        $http.post(BASE_URL + 'trade/account/chcek_auth', data, config).then((res) => {
            if (res.data.success) {
                $scope.code = '';
                $scope.authsuccess = true;
                $scope.google_auth_status = true;
                toastr.success(res.data.message);
                console.log('success');
            } else {
                toastr.error(res.data.message);
                console.log('failed');
            }
        })
    }

    $scope.off_auth = () => {

        if (!$scope) {
            var data = $.param({
                code: '',
                status: 'false'
            })
        } else {
            var data = $.param({
                code: $scope.code,
                status: 'false'
            });
        }
        $http.post(BASE_URL + 'trade/account/chcek_auth', data, config).then((res) => {
            if (res.data.success) {
                $scope.code = '';
                $scope.authsuccess = false;
                $scope.google_auth_status = false;
                toastr.success(res.data.message);
            } else {
                toastr.error(res.data.message);
            }
        })
    }
}

const Users_Controller = ($scope, $http) => {
    $scope.get_users = (form) => {
        $scope.matrixLoader = true;
        if (!form.$invalid) {
            var data = $.param({
                userid: $scope.user
            });
            $http.post(BASE_URL + 'admin/copytrade/get_users_list/', data, config).then((res) => {
                $scope.lists = res.data;
                $scope.matrixLoader = false;
            });
        } else {
            console.log('error');
            $scope.matrixLoader = false;
            // toastr.error("Invalid user");
        }
    }

    $scope.change_status = (id, status) => {
        BootstrapDialog.show({
            title: "",
            message: "<p>Do you really want to change status. ?</p>",
            buttons: [{
                    label: 'Yes',
                    cssClass: 'bg-theme-1 button mr-1 text-white yes_class',
                    action: function(dialogItself) {

                        // $(".yes_class").attr('disable', true);
                        // $("#submit_button_id").attr('disable', true);

                        var data = $.param({
                            id: id,
                            status: status == 1 ? 0 : 1
                        });

                        $http.post(BASE_URL + 'admin/copytrade/change_status/', data, config).then((res) => {
                            if (res.data.success == true) {
                                location.reload();
                                // $scope.get_users();
                            } else {
                                location.reload();
                            }
                        })
                    }
                },
                {
                    label: 'No',
                    cssClass: 'bg-theme-6 button text-white',
                    action: function(dialogItself) {
                        dialogItself.close();
                    }
                }
            ]
        });
    }
}

const Agency_Controller = ($scope, $http) => {
    $scope.get_user_info = () => {
        $scope.userLoader = true;
        $http.get(BASE_URL + 'trade/agency/get_user_info').then((res) => {
            $scope.user = res.data;
            $scope.userLoader = false;
        })
    }

    $scope.make_agengy_payment = (buyType) => {
        var data = $.param({
            amount: buyType == 'pwt' ? 200 : 100,
            type: buyType
        });
        $http.post(BASE_URL + 'trade/agency/make_payment/', data, config).then((res) => {
            if (res.data.success == true) {
                $('.buy-agency').modal('hide');
                toastr.success(res.data.message);
                location.reload();
                // $scope.get_user_info();
            } else {
                toastr.error(res.data.message);
            }
        })
    }

    $scope.make_bot_payment = (buyType) => {
        var data = $.param({
            amount: buyType == 'pwt' ? 200 : 120,
            type: buyType
        });
        $http.post(BASE_URL + 'trade/bottrade/make_payment/', data, config).then((res) => {
            if (res.data.success == true) {
                $('.buy-agency').modal('hide');
                toastr.success(res.data.message);
                location.reload();
                // $scope.get_user_info();
            } else {
                toastr.error(res.data.message);
            }
        })
    }

    $scope.buy_agency = (type) => {
        $scope.buyType = type;
        $('#agengyModal').modal('show');
    }
    $scope.buy_bottrading = (type) => {
        $scope.buyType = type;
        $('#buybotModal').modal('show');
    }

    // $scope.get_my_network = () => {
    //     $scope.listLoader = true;
    //     $http.get(BASE_URL + 'trade/agency/get_my_network').then((res) => {
    //         $scope.users = res.data;
    //         $scope.listLoader = false;
    //     })
    // }

    // $scope.get_commission_history = () => {
    //     $scope.historyLoader = true;
    //     $http.get(BASE_URL + 'trade/agency/get_commission_history').then((res) => {
    //         $scope.user = res.data;
    //         $scope.historyLoader = false;
    //     })
    // }
}

const Wallet_Controller = ($scope, $http) => {
    $scope.make_withdrawal = (form) => {
        if (!form.$invalid) {
            Swal.fire({
                title: "Are you sure?",
                text: "",
                type: "warning",
                showCancelButton: !0,
                confirmButtonColor: "#34c38f",
                cancelButtonColor: "#f46a6a",
                confirmButtonText: "Yes",
                confirmButtonClass: "btn btn-success mt-2",
                cancelButtonClass: "btn btn-danger ml-2 mt-2",
                buttonsStyling: !1,
            }).then(function(t) {
                if (t.value == true) {
                    var data = $.param({
                        withdraw_amount: $scope.withdraw_amount,
                        withdraw_type: $scope.withdraw_type,
                        withdraw_address: $scope.address,
                        withdraw_fee: $scope.withdraw_fee
                    })
                    $http.post(BASE_URL + 'trade/account/make_withdrawal', data, config).then((res) => {
                        if (res.data.success == true) {
                            location.reload();
                        } else if (res.data.success == 'auth') {
                            $('#modal_withdrawal').modal('hide');
                            $('#otp_model').modal('show');
                        } else {
                            toastr.error(res.data.message, "", toast);
                        }
                    })
                } else {

                }
            });
        }
    }

    $scope.check_auth = () => {

        if (!$scope) {
            var data = $.param({
                code: '',
                withdraw_amount: '',
                withdraw_type: '',
                withdraw_address: '',
                withdraw_fee: '',
            })
        } else {
            var data = $.param({
                code: $scope.code,
                withdraw_amount: $scope.withdraw_amount,
                withdraw_type: $scope.withdraw_type,
                withdraw_address: $scope.address,
                withdraw_fee: $scope.withdraw_fee
            });
        }
        $http.post(BASE_URL + 'account/success_withdrawal', data, config).then((res) => {
            if (res.data.success) {
                location.reload();
            } else {
                toastr.error(res.data.message, "", toast);
            }
        })
    }
}



const Wallet_Controller2 = ($scope, $http) => {
    $scope.make_withdrawal = (form) => {
        if (!form.$invalid) {
            Swal.fire({
                title: "Are you sure?",
                text: "",
                type: "warning",
                showCancelButton: !0,
                confirmButtonColor: "#34c38f",
                cancelButtonColor: "#f46a6a",
                confirmButtonText: "Yes",
                confirmButtonClass: "btn btn-success mt-2",
                cancelButtonClass: "btn btn-danger ml-2 mt-2",
                buttonsStyling: !1,
            }).then(function(t) {
                if (t.value == true) {
                    var data = $.param({
                        withdraw_amount: $scope.withdraw_amount,
                        withdraw_type: $scope.withdraw_type,
                        withdraw_address: $scope.address,
                        withdraw_fee: $scope.withdraw_fee
                    })
                    $http.post(BASE_URL + 'trade/account/make_withdrawal', data, config).then((res) => {
                        if (res.data.success == true) {
                            location.reload();
                        } else if (res.data.success == 'auth') {
                            $('#modal_withdrawal').modal('hide');
                            $('#otp_model').modal('show');
                        } else {
                            toastr.error(res.data.message, "", toast);
                        }
                    })
                } else {

                }
            });
        }
    }

    $scope.check_auth = () => {

        if (!$scope) {
            var data = $.param({
                code: '',
                withdraw_amount: '',
                withdraw_type: '',
                withdraw_address: '',
                withdraw_fee: '',
            })
        } else {
            var data = $.param({
                code: $scope.code,
                withdraw_amount: $scope.withdraw_amount,
                withdraw_type: $scope.withdraw_type,
                withdraw_address: $scope.address,
                withdraw_fee: $scope.withdraw_fee
            });
        }
        $http.post(BASE_URL + 'account/success_withdrawal', data, config).then((res) => {
            if (res.data.success) {
                location.reload();
            } else {
                toastr.error(res.data.message, "", toast);
            }
        })
    }
    $scope.show_depositpwt = (type) => {
        $scope.hide_modal();
        $scope.type = type;
        $scope.minDeposit = 20;
        $scope.payment_type = type;
        if (type == 'USDTTRC20') {
            $scope.minDeposit = 15;
            $scope.wallettype = type;

        } else if (type == 'USDTERC20') {
            $scope.minDeposit = 30;
            $scope.wallettype = type;

        } else if (type == 'ETH' || type == 'BNB' || type == "BTC" || type == "PWT") {
            $scope.minDeposit = 15;
            $scope.wallettype = type;
        }
    }
    $scope.show_depositpwt('USDTTRC20');
}

const Wallet_Controller3 = ($scope, $http) => {
    $scope.make_withdrawal = (form) => {
        if (!form.$invalid) {
            Swal.fire({
                title: "Are you sure?",
                text: "",
                type: "warning",
                showCancelButton: !0,
                confirmButtonColor: "#34c38f",
                cancelButtonColor: "#f46a6a",
                confirmButtonText: "Yes",
                confirmButtonClass: "btn btn-success mt-2",
                cancelButtonClass: "btn btn-danger ml-2 mt-2",
                buttonsStyling: !1,
            }).then(function(t) {
                if (t.value == true) {
                    var data = $.param({
                        withdraw_amount: $scope.withdraw_amount,
                        withdraw_type: $scope.withdraw_type,
                        withdraw_address: $scope.address,
                        withdraw_fee: $scope.withdraw_fee
                    })
                    $http.post(BASE_URL + 'trade/account/make_withdrawal', data, config).then((res) => {
                        if (res.data.success == true) {
                            location.reload();
                        } else if (res.data.success == 'auth') {
                            $('#modal_withdrawal').modal('hide');
                            $('#otp_model').modal('show');
                        } else {
                            toastr.error(res.data.message, "", toast);
                        }
                    })
                } else {

                }
            });
        }
    }

    $scope.check_auth = () => {

        if (!$scope) {
            var data = $.param({
                code: '',
                withdraw_amount: '',
                withdraw_type: '',
                withdraw_address: '',
                withdraw_fee: '',
            })
        } else {
            var data = $.param({
                code: $scope.code,
                withdraw_amount: $scope.withdraw_amount,
                withdraw_type: $scope.withdraw_type,
                withdraw_address: $scope.address,
                withdraw_fee: $scope.withdraw_fee
            });
        }
        $http.post(BASE_URL + 'account/success_withdrawal', data, config).then((res) => {
            if (res.data.success) {
                location.reload();
            } else {
                toastr.error(res.data.message, "", toast);
            }
        })
    }

    // $scope.show_depositpwt = (type) => {
    //     $scope.hide_modal();
    //     $scope.type = type;
    //     $scope.minDeposit = 20;
    //     $scope.payment_type = type;
    //     if (type == 'USDTTRC20') {
    //         $scope.minDeposit = 15;
    //         $scope.wallettype = type;

    //     } else if (type == 'USDTERC20') {
    //         $scope.minDeposit = 30;
    //         $scope.wallettype = type;

    //     } else if (type == 'ETH' || type == 'BNB' || type == "BTC" || type == "PWT") {
    //         $scope.minDeposit = 15;
    //         $scope.wallettype = type;
    //     }
    // }

    $scope.show_withdrawalpwt = (type) => {
        $scope.hide_modal();
        $scope.withdraw_type = type;
        $scope.addressLoader = true
        $http.get(BASE_URL + 'trade/account/get_address/').then((res) => {
            $scope.type = type;
            $scope.fee = 25;
            $scope.minamount = 50
            $scope.withdraw_in = 'usdt';
            if (type == 'USDTTRC20') {
                $scope.address = res.data.trx_address;
                $scope.fee = 0;
                $scope.wallettype = type;
            } else if (type == 'PWT') {
                $scope.address = res.data.eth_address;
                $scope.fee = 20;
                $scope.wallettype = type;
            }
            $scope.addressLoader = false;
        });
    }
    $scope.show_withdrawalpwt('USDTTRC20')
}



const Tradingbot_Controller = ($scope, $http) => {
    $scope.topp_gainersforbot = () => {
        $http.get(BASE_URL + 'trade/trading/getlastvaluforbot').then((res) => {
            $scope.gainersforbot = res.data;
        })
    }
    $scope.topp_gainersforbot();

    $scope.get_user_info = () => {
        $scope.userLoader = true;
        $http.get(BASE_URL + 'trade/agency/get_user_info').then((res) => {
            $scope.user = res.data;
            $scope.coin_name = ['AAVEUSDT', 'ADAUSDT', 'AKROUSDT', 'ANTUSDT', 'ATOMUSDT', 'BATUSDT', 'BCHUSDT', 'BNBUSDT', 'BTCUSDT', 'CAKEUSDT', 'COMPUSDT', 'COSUSDT', 'CRVUSDT', 'DASHUSDT', 'DOGEUSDT', 'DOTUSDT', 'EOSUSDT', 'ETHUSDT', 'ETCUSDT', 'FILUSDT', 'FTTUSDT', 'GRTUSDT', 'IOSTUSDT', 'IOTAUSDT', 'JSTUSDT', 'KAVAUSDT', 'LINKUSDT', 'LITUSDT', 'LTCUSDT', 'MANAUSDT', 'MDXUSDT', 'NEOUSDT', 'OMGUSDT', 'SUSHIUSDT', 'THETAUSDT', 'TRXUSDT', 'UNIUSDT', 'XMRUSDT', 'XRPUSDT', 'XTZUSDT', 'RVNUSDT', 'SHIBUSDT', 'MATICUSDT', 'CELRUSDT'];
            $scope.userLoader = false;
        })
    }
    $scope.topp_gainersforbot = () => {
        $http.get(BASE_URL + 'trade/trading/getlastvaluforbot').then((res) => {
            $scope.gainersforbot = res.data;
        })
    }
    $scope.topp_gainersforbot();
    $scope.treding_set_val = () => {
        if (!$scope) {
            var data = $.param({
                margin_calllimit: '',
                position_takeprofitratio: '',

            });
        } else {
            if ($scope.sell < $scope.stoploss) {
                toastr.error("Sell is lessthan stoploss");
            } else {
                if ($scope.buy < 0) {
                    toastr.error("Buy Value is minus");

                } else {
                    var nn = $scope.tradamount;
                    if (14 < nn) {
                        var data = $.param({
                            tradamount: $scope.tradamount,
                            buy: $scope.buy,
                            numberofbuy: $scope.numberofbuy,
                            sell: $scope.sell,
                            // stoploss: $scope.stoploss,
                            // repetbuy: $scope.repetbuy,
                            // groupsell: $scope.groupsell,
                            sellstoplimit: $scope.sellstoplimit,
                            ordertype: document.getElementById('odertype').value,
                            // orderperce: document.getElementById('#' + ordertype + '-p').value,

                        });

                    } else {

                        toastr.error("Minimum trade amount is 15")
                    }
                }
            }
        }
        $http.post(BASE_URL + 'trade/account/treding_set_val/', data, config).then(function(res) {
            if (res.data.success == true) {
                $scope.topp_gainersforbot()
                location.reload();
                toastr.success(res.data.message);
            } else {
                toastr.error(res.data.message);
            }
        })
    }

    $scope.bidoncoin = (type) => {
        $scope.coin = type;
        if (type != "") {
            if (type != "") {

                var data = $.param({
                    data: type
                });
                $http.post(BASE_URL + 'trade/account/treding_bot_last_val/', data, config).then(function(res) {
                    if (res.data != "") {

                        $('#odertypepercent').val(res.data.persent);
                        $('#odertypeamount').val(res.data.val);
                    } else {
                        toastr.error(res.data.message);
                    }
                })

            }
            $('#odertype').val(type);
            $('#modal_treding_bot').modal('show');
        }
    }
    $scope.bidoncoinstop = (type) => {
        $scope.coin = type;
        if (type != "") {
            var data = $.param({
                data: type
            });
            $http.post(BASE_URL + 'trade/account/treding_bot_last_val_stop/', data, config).then(function(res) {
                if (res.data == "") {
                    toastr.success("Success Done");
                    $scope.topp_gainersforbot()
                        // $scope.get_user_info();
                } else {
                    toastr.error("Somethio");
                }
            })

        }

    }

}
const Tradingbot_Controller1 = ($scope, $http) => {
    $scope.topp_gainersforbot = () => {
        $http.get(BASE_URL + 'trade/trading/getlastvaluforbot').then((res) => {
            $scope.gainersforbot = res.data;
        })
    }
    $scope.topp_gainersforbot();
    $scope.bidoncoin = (type) => {
        $scope.coin = type;
        if (type != "") {
            if (type != "") {

                var data = $.param({
                    data: type
                });
                $http.post(BASE_URL + 'trade/account/treding_bot_last_val/', data, config).then(function(res) {
                    if (res.data != "") {

                        $('#odertypepercent').val(res.data.persent);
                        $('#odertypeamount').val(res.data.val);
                    } else {
                        toastr.error(res.data.message);
                    }
                })

            }
            $('#odertype').val(type);
            $('#modal_treding_bot').modal('show');
        }
    }
    $scope.bidoncoinstop = (type) => {
        $scope.coin = type;
        if (type != "") {
            var data = $.param({
                data: type
            });
            $http.post(BASE_URL + 'trade/account/treding_bot_last_val_stop/', data, config).then(function(res) {
                if (res.data == "") {
                    toastr.success("Success Done");
                    $scope.topp_gainersforbot()
                        // $scope.get_user_info();
                } else {
                    toastr.error("Somethio");
                }
            })

        }

        // }


    }
}

const Tradingbot_Controller2 = ($scope, $http) => {

    $scope.get_user_info = (type) => {
        $scope.userLoader = true;
        var data = $.param({
            type: type,
        });

        $http.post(BASE_URL + 'trade/bottradebid/cyclechek/', data, config).then(function(res) {
            if (res.data.success == true) {
                $scope.user = res.data.check_stratage[0];
                $scope.userLoader = false;

            } else {
                toastr.error(res.data.message);
            }
        })
    }
    $scope.topp_gainersforbot = () => {
        $http.get(BASE_URL + 'trade/trading/getlastvaluforbot').then((res) => {
            $scope.gainersforbot = res.data;
        })
    }
    $scope.topp_gainersforbot();
    $scope.treding_set_val = () => {
        if (!$scope) {
            var data = $.param({
                margin_calllimit: '',
                position_takeprofitratio: '',

            });
        } else {
            if ($scope.sell < $scope.stoploss) {
                toastr.error("Sell is lessthan stoploss");
            } else {
                if ($scope.buy < 0) {
                    toastr.error("Buy Value is minus");

                } else {
                    var nn = $scope.tradamount;
                    if (14 < nn) {
                        var data = $.param({
                            tradamount: $scope.tradamount,
                            buy: $scope.buy,
                            numberofbuy: $scope.numberofbuy,
                            sell: $scope.sell,
                            sellstoplimit: $scope.sellstoplimit,
                            ordertype: document.getElementById('odertype').value,
                        });
                    } else {
                        toastr.error("Minimum trade amount is 15")
                    }
                }
            }
        }
        $http.post(BASE_URL + 'trade/bottradebid/treding_set_val/', data, config).then(function(res) {
            if (res.data.success == true) {
                $scope.topp_gainersforbot()
                location.reload();

                toastr.success(res.data.message);

            } else {
                toastr.error(res.data.message);
            }
        })

    }

    $scope.startprevious = (type, id) => {
        $scope.coin = type;
        if (type != "") {
            if (type != "") {

                var data = $.param({
                    data: type,
                    id: id,
                });
                $http.post(BASE_URL + 'trade/bottradebid/aginusepre/', data, config).then(function(res) {
                    if (res.data == "") {
                        $scope.topp_gainersforbot();
                        toastr.error("Error");
                        $scope.get_user_info(type);

                    } else {
                        toastr.success("Started Successfully");
                        $scope.topp_gainersforbot();
                        $scope.get_user_info(type);
                    }
                })

            }

        }
    }
    $scope.start = (type) => {
        $scope.coin = type;
        if (type != "") {
            if (type != "") {

                var data = $.param({
                    data: type
                });
                $http.post(BASE_URL + 'trade/bottradebid/treding_bot_last_bid_start/', data, config).then(function(res) {
                    if (res.data == "") {
                        toastr.success("Started Successfully");
                        $scope.topp_gainersforbot();
                        $scope.get_user_info(type);
                    } else {
                        toastr.error("Error");
                    }
                })

            }

        }
    }
    $scope.bidoncoinstop = (type, id) => {
        $scope.coin = type;
        if (type != "") {
            var data = $.param({
                data: type,
                id: id,
            });
            $http.post(BASE_URL + 'trade/bottradebid/treding_bot_last_bid_stop/', data, config).then(function(res) {
                if (res.data == "") {
                    toastr.success("Stopped Successfully");
                    $scope.topp_gainersforbot()
                    $scope.get_user_info(type);
                } else {
                    toastr.error("Error");
                }
            })

        }

    }

    $scope.bidoncoin = (type) => {
        $scope.coin = type;
        if (type != "") {
            if (type != "") {

                var data = $.param({
                    data: type
                });
                $http.post(BASE_URL + 'trade/account/treding_bot_last_val/', data, config).then(function(res) {
                    if (res.data != "") {

                        $('#odertypepercent').val(res.data.persent);
                        $('#odertypeamount').val(res.data.val);
                    } else {
                        toastr.error("Error");
                    }
                })

            }
            $('#odertype').val(type);
            $('#modal_treding_bot').modal('show');
        }
    }

    $scope.oncliclbuy = (type) => {
        (async() => {

            /* inputOptions can be an object or Promise */
            const inputOptions = new Promise((resolve) => {
                setTimeout(() => {
                    resolve({
                        '1': 'Old strategy',
                        '2': 'Default'
                    })
                }, 1000)
            })
            const { value: color } = await Swal.fire({
                title: 'Select stratage',
                input: 'radio',
                inputOptions: inputOptions,
                inputValidator: (value) => {
                    if (!value) {
                        return 'You need to choose something!'
                    } else {
                        var data = $.param({
                            data: value,
                            type: type
                        });
                        $http.post(BASE_URL + 'trade/bottradebid/atatimebid/', data, config).then(function(res) {
                            if (res.data.success == true) {
                                // $scope.topp_gainers()
                                toastr.success(res.data.message);
                            } else {
                                toastr.error(res.data.message);
                            }
                        })
                    }
                }
            })
            if (color) {
                Swal.fire({ html: `You selected: ${color}` })
            }
        })()
    }
    $scope.oncliclstop = (type) => {

        Swal.fire({
            title: "Are you sure?",
            text: "",
            type: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#34c38f",
            cancelButtonColor: "#f46a6a",
            confirmButtonText: "Yes",
            confirmButtonClass: "btn btn-success mt-2",
            cancelButtonClass: "btn btn-danger ml-2 mt-2",
            buttonsStyling: !1,
        }).then(function(t) {
            if (t.value == true) {
                var data = $.param({
                    type: type,
                });
                $http.post(BASE_URL + 'trade/bottradebid/stop_bid/', data, config).then(function(res) {
                    if (res.data.success == true) {
                        toastr.success(res.data.message);
                    } else {
                        toastr.error(res.data.message);
                    }
                })
            } else {

            }
        });

    }
    $scope.infoforstartbot = () => {

        Swal.fire({
            title: "Please start the bot?",
            text: "",
            type: "warning",
            confirmButtonColor: "#34c38f",
            confirmButtonText: "Exit",
            confirmButtonClass: "btn btn-success mt-2",
            buttonsStyling: !1,
        }).then(function(t) {
            if (t.value == true) {

            } else {

            }
        });
    }
    $scope.cyclestart = (type, id) => {
        $scope.userLoader = true;
        var data = $.param({
            type: type,
            id: id,
        });

        $http.post(BASE_URL + 'trade/bottradebid/cyclestart/', data, config).then(function(res) {
            if (res.data.success == true) {
                toastr.success("Success Done");
                $scope.get_user_info(type)
            } else {
                toastr.error(res.data.message);
            }
        })

    }
    $scope.cyclestop = (type, id) => {
        $scope.userLoader = true;
        var data = $.param({
            type: type,
            id: id,
        });

        $http.post(BASE_URL + 'trade/bottradebid/cyclestop/', data, config).then(function(res) {
            if (res.data.success == true) {
                toastr.success("Success Done");
                $scope.get_user_info(type)
            } else {
                toastr.error(res.data.message);
            }
        })

    }

}

const Tradingbot_TradeSetting = ($scope, $http) => {

    $scope.savedata = () => {
        if (!$scope) {
            toastr.error("Plz fill all value");
        } else {

            if ($scope.buyincallback < 0) {
                toastr.error("Buy Value is minus");

            } else {
                var nn = $scope.tradamount;
                if (14 < nn) {
                    var mm = $scope.margincall;
                    if (mm > 0) {
                        const numbaric = [];
                        const datanum = [];
                        const datarati = [];
                        const sell = [];
                        const sellstop = [];
                        var number = document.getElementById("margincall").value;
                        var number = Number(number);
                        for (let i = 1; i <= number; i++) {
                            buy = document.getElementById("buy" + i).value;
                            buyratio = document.getElementById("buyratio" + i).value;
                            sell1 = document.getElementById("sellprofit" + i).value;
                            sellstop1 = document.getElementById("sellstoploss" + i).value;
                            if (buy !== '' && buyratio !== '') {
                                numbaric.push(i)
                                datanum.push(buy)
                                datarati.push(buyratio)
                                sell.push(sell1)
                                sellstop.push(sellstop1)
                            } else {
                                toastr.error("Fill all the value");
                            }
                        }
                        var data = $.param({
                            tradamount: $scope.tradamount,
                            margincall: $scope.margincall,
                            srnum: numbaric,
                            buy: datanum,
                            buyratio: datarati,
                            sell: sell,
                            sellstop: sellstop,
                            ordertype: document.getElementById('odertype').value,
                        });
                    } else {
                        toastr.error("Margin call incorrect")
                    }
                } else {
                    toastr.error("Minimum trade amount is 15")
                }
            }

        }
        $http.post(BASE_URL + 'trade/bottradebid/treding_set_val/', data, config).then(function(res) {
            if (res.data.success == true) {
                // $scope.topp_gainersforbot()
                window.location.href = BASE_URL + 'coin-status/' + res.data.type;
                toastr.success(res.data.message);

            } else {
                toastr.error(res.data.message);
            }
        })

    }

    $scope.savemargincall = () => {

        var calllimite = document.getElementById('margincall').value;
        var newdata = Number(calllimite);
        var setnumber = 20;
        if (newdata == 0) {
            toastr.error("Please Correct value insert Margin call limit")
        } else if (newdata > Number(setnumber)) {
            toastr.error("Margin call limit is greater than 20")
        } else {
            const numberofbox = [];
            const element = document.getElementById("inputbox");
            var mylist = '<div>';
            for (let i = 0; i < newdata; i++) {
                numberofbox.push(i)
                var id = i + 1;
                mylist += '<div class="model-margin"> <div class="row"> <div class="col-12 mt-2"> <h6 class="text-theme-green">' + id + 'st Call Buy</h6> </div> <div class="col-6"> <div class="d-flex min-w-40px"><input value="' + (id + 0.5) + '" placeholder="' + (id + 0.5) + '" step="any" id="buy' + id + '" type="number" class="form-control px-2"> <div class="mt-auto mb-auto text-center"><span class="ml-2">%</span></div> </div> </div> <div class="col-6"> <div class="d-flex min-w-40px"><input value="1" placeholder="1" step="any" id="buyratio' + id + '" type="number" class="form-control px-2"> <div class="mt-auto mb-auto text-center"><span class="ml-2">x</span></div> </div> </div> </div> <div class="row"> <div class="col-12 mt-2"> <h6 class="text-danger">' + id + 'st Call Sell</h6> </div> <div class="col-6"> <div class="d-flex min-w-40px"><input value="1.3" placeholder="1.3" step="any" id="sellprofit' + id + '" type="number" class="form-control px-2"> <div class="mt-auto mb-auto text-center"><span class="ml-2">%</span></div> </div> </div> <div class="col-6"> <div class="d-flex min-w-40px"><input placeholder="0.3" value="0.3" step="any" id="sellstoploss' + id + '" type="number" class="form-control px-2"> <div class="mt-auto mb-auto text-center"><span class="ml-2">%</span></div> </div> </div> </div> </div>'
            }

            mylist += '</div>';
            element.innerHTML = mylist;
            document.getElementById("numberofbid").value = newdata;
            $('#margin-conf').modal('show');

        }
    }

}

const Tradingbot_Controller_Margin = ($scope, $http) => {

    $scope.margin_call_val = () => {
        if (!$scope) {
            var data = $.param({
                margin_calllimit: '',
                position_takeprofitratio: '',

            });
        } else {

        }
        $http.post(BASE_URL + 'trade/account/margin_call_val/', data, config).then(function(res) {
            if (res.data.success == true) {
                $scope.topp_gainersforbot()
                location.reload();
                toastr.success(res.data.message);
            } else {
                toastr.error(res.data.message);
            }
        })
    }

}

const Multiplebid_Controller = ($scope, $http) => {
    $scope.savedataformultibid = () => {
        if (!$scope) {
            toastr.error("Plz fill all value");
        } else {
            const numbaric = [];
            const datanum = [];
            const datarati = [];
            const sell = [];
            const sellstop = [];
            var number = document.getElementById("numberofbid").value;
            var number = Number(number);
            for (let i = 1; i <= number; i++) {
                buy = document.getElementById("buy" + i).value;
                buyratio = document.getElementById("buyratio" + i).value;
                sell1 = document.getElementById("sellprofit" + i).value;
                sellstop1 = document.getElementById("sellstoploss" + i).value;
                if (buy !== '' && buyratio !== '') {
                    numbaric.push(i)
                    datanum.push(buy)
                    datarati.push(buyratio)
                    sell.push(sell1)
                    sellstop.push(sellstop1)
                } else {
                    toastr.error("Fill all the value");
                }
            }
            data = $.param({
                numberofbid: document.getElementById("numberofbid").value,
                srnum: numbaric,
                buy: datanum,
                buyratio: datarati,
                sell: sell,
                sellstop: sellstop,
            });
        }

        $http.post(BASE_URL + 'trade/bottradebid/savemultiplebid/', data, config).then(function(res) {
            if (res.data.success == true) {
                $('#margin-conf').modal('hide');
                toastr.success(res.data.message);
            } else {
                toastr.error(res.data.message);
            }
        })

    }
}

pwt.controller('App_Controller', App_Controller);
pwt.controller('Dashboard_Controller', Dashboard_Controller);
pwt.controller('Profile_Controller', Profile_Controller);
pwt.controller('CopyTrade_Controller', CopyTrade_Controller);
pwt.controller('CopyTrade_Controller1', CopyTrade_Controller1);
pwt.controller('Users_Controller', Users_Controller);
pwt.controller('Agency_Controller', Agency_Controller);
pwt.controller('Wallet_Controller', Wallet_Controller);
pwt.controller('Wallet_Controller2', Wallet_Controller2);
pwt.controller('Wallet_Controller3', Wallet_Controller3);
pwt.controller('Tradingbot_Controller', Tradingbot_Controller);
pwt.controller('Tradingbot_Controller1', Tradingbot_Controller1);
pwt.controller('Tradingbot_Controller2', Tradingbot_Controller2);
pwt.controller('Tradingbot_TradeSetting', Tradingbot_TradeSetting);
pwt.controller('Tradingbot_Controller_Margin', Tradingbot_Controller_Margin);
pwt.controller('Multiplebid_Controller', Multiplebid_Controller);



pwt.filter('pagination', function() {
    "use strict";

    return function(input, start) {
        if (!input || !input.length) {
            return;
        }
        start = +start; //parse to int
        return input.slice(start);
    };
});