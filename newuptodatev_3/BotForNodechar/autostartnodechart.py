import asyncio
import datetime
from threading import Timer
import os, sys
from numpy import arange
from subprocess import check_output
from win32com.client import GetObject

nodechartpath = "C:\\Users\\support\\Desktop\\node-chart\\"

import MySQLdb.cursors

db = MySQLdb.connect("localhost", "root", "", "xenbot")

async def work():
    while True:
        await asyncio.sleep(120.0)
        curs = db.cursor(MySQLdb.cursors.DictCursor)
        curs.execute("SELECT * FROM btcdata ORDER BY id DESC LIMIT 1")
        timinfo = curs.fetchall()

        for b in timinfo:
            timecal = b["timestamp"]
            your_dt = datetime.datetime.fromtimestamp(
                int(timecal) / 1000
            )  # using the local timezone
            dat = your_dt.strftime("%Y-%m-%d %H:%M %p")
            result = datetime.datetime.now()
            dat_new = result.strftime("%Y-%m-%d %H:%M %p")
            print(dat_new)
            print(dat)
            if dat != dat_new:
                # START "for new nodechart stop"
                WMI = GetObject("winmgmts:")
                processes = WMI.InstancesOf("Win32_Process")
                for p in WMI.ExecQuery(
                    'select * from Win32_Process where Name="cmd.exe"'
                ):
                    old_path = str(p.Properties_("CommandLine").Value)
                    print(old_path)
                    if old_path == "cmd  /K node app.js":
                        print(old_path)
                        os.system(
                            "taskkill /pid " +
                            str(p.Properties_("ProcessId").Value)
                        )
                        print("old close")
                        await asyncio.sleep(5.0)
                # START "for new nodechart stop"
                ########################################################################
                # START "for new nodechart start"

                class cd:
                    """Context manager for changing the current working directory"""

                    def __init__(self, newPath):
                        self.newPath = os.path.expanduser(newPath)

                    def __enter__(self):
                        self.savedPath = os.getcwd()
                        os.chdir(self.newPath)

                    def __exit__(self, etype, value, traceback):
                        os.chdir(self.savedPath)

                with cd(nodechartpath):
                    # we are in ~/Library
                    new = os.getcwd()
                    os.system("start cmd /K node app.js")
                    print(new)
                # END "for new nodechart start"
                ############################################################################


loop = asyncio.get_event_loop()
try:
    asyncio.ensure_future(work())
    loop.run_forever()
except KeyboardInterrupt:
    pass
finally:
    print("Closing Loop")
    loop.close()
