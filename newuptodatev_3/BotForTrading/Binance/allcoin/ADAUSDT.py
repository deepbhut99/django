import websockets
import requests
import asyncio
import json
import sys
import datetime
from threading import Timer
from operator import itemgetter
import time
from numpy import arange
import numpy as np
from binance.client import Client
from binance.enums import *
from binance.exceptions import BinanceAPIException, BinanceOrderException
from decimal import *

# 1INCHUSDT (open, close, high, low, timestamp, symbol, pricechange, prisechangepercentage, weightedaverageprice, totaltradebaseasset


async def listen():
    while True:
        await asyncio.sleep(1)
        import MySQLdb.cursors

db = MySQLdb.connect("localhost", "root", "", "xenbot")

        curs = db.cursor(MySQLdb.cursors.DictCursor)
        curs.execute(
            """select * from efx_percentage_change_adausdt ORDER BY id DESC LIMIT 1"""
        )
        adausdt = curs.fetchall()
        for value in adausdt:
            # /////// get value \\\\\\\\\\\\\
            open = value["open"]
            high = value["high"]
            low = value["low"]
            close = value["close"]
            # volume = input_dict[""]
            timestamp = value["timestamp"]
            symbol = value["symbol"]
            pricechange = value["pricechange"]
            pricechangepercentage = value["prisechangepercentage"]
            weightedaverageprice = value["weightedaverageprice"]
            totaltradebaseasset = value["totaltradebaseasset"]
            curs.execute(
                """select * from etx_condition_for_btctousdt   where margincalllimit != 0 AND typeofpoint = "ADAUSDT" AND  buysell in (-1,1,4,5)"""
            )
            adausdt1 = curs.fetchall()
            for a in adausdt1:
                setvalu = Decimal(a["set_profitratio_portfo_finalvalli"])
                lastpointvalu = Decimal(close)
                id = a["id"]
                buyper = Decimal(a["takieprofitratio"])
                prisec = Decimal(pricechangepercentage)
                close = Decimal(close)
                conditionfirst = int(a["conditionfirst"])
                api_key = a["binance_apikey"]
                api_secret = a["binance_securitykey"]
                try:
                    if a["buysell"] == -1:
                        if prisec <= buyper:
                            id = a["id"]
                            # print(id)
                            user_id = a["user_id"]
                            buy_or_sell_amount = a["firstbuyinamout"]
                            created_datetime = int(time.time())
                            margincalllimit = a["margincalllimit"]
                            sell = 1
                            set_profit_ratio = a["sell_point"]
                            cal_loss = a["stop_loss"]
                            stoplosslimite = a["sell_stop_limit"]
                            symbol = a["typeofpoint"]
                            # get balance
                            api_key = a["binance_apikey"]
                            api_secret = a["binance_securitykey"]
                            client = Client(api_key, api_secret)
                            # fetch exchange info
                            balance = client.get_asset_balance(asset="USDT")
                            current_bln = float(balance["free"])
                            current_bln_fin = current_bln + 0.5

                            if current_bln_fin >= buy_or_sell_amount:
                                try:
                                    info = client.get_symbol_info(symbol)
                                    val = info["filters"][2]["stepSize"]
                                    decimal = 0
                                    is_dec = False
                                    for c in val:
                                        if is_dec is True:
                                            decimal += 1
                                        if c == "1":
                                            break
                                        if c == ".":
                                            is_dec = True

                                    qun_num1 = float(buy_or_sell_amount / close)
                                    qun_num = round(qun_num1, decimal)
                                    buy_limit = client.order_market_buy(
                                        symbol=symbol, quantity=qun_num
                                    )
                                    symbol = buy_limit["symbol"]
                                    orderId = buy_limit["orderId"]
                                    orderListId = buy_limit["orderListId"]
                                    clientOrderId = buy_limit["clientOrderId"]
                                    price_orideal = buy_limit["fills"]
                                    for i in price_orideal:
                                        price_ori = i["price"]
                                        price_ori = Decimal(price_ori)
                                    origQty = buy_limit["origQty"]
                                    cummulativeQuoteQty = buy_limit[
                                        "cummulativeQuoteQty"
                                    ]
                                    status = buy_limit["status"]
                                    side = buy_limit["side"]
                                    price_after = 0
                                    qty = 0
                                    commission = 0
                                    commissionAsset = "Null"
                                    tradeId = 0
                                    calu_profit = (price_ori * set_profit_ratio) / 100
                                    sell2 = (set_profit_ratio) - (cal_loss)
                                    fin_cal_loss = (price_ori * sell2) / 100

                                    stoplosslimite1 = (price_ori * stoplosslimite) / 100
                                    stoplosslimite2 = price_ori - stoplosslimite1

                                    stoplimit = set_profit_ratio - stoplosslimite
                                    fin_thired_point = (price_ori * stoplimit) / 100

                                    set_profitratio_portfoli = calu_profit
                                    set_profitratio_portfo_finalvalli = (
                                        calu_profit + price_ori
                                    )
                                    call_back_for_loss = price_ori + fin_thired_point
                                    stoplosslimite = stoplosslimite2

                                    curs = db.cursor(MySQLdb.cursors.DictCursor)

                                    sql = "INSERT INTO efx_bids_for_buysell (user_id, buy_or_sell_amount, btctousdpointaction, btctousdtprise, created_datetime, id_whichsellorbuy, buy_or_sell, symbol, orderId, orderListId, clientOrderId, price, origQty, cummulativeQuoteQty, status, 	side, price_with_commision, qty_with_commision, commission, commissionAsset, tradeId) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                                    val = (
                                        user_id,
                                        buy_or_sell_amount,
                                        pricechange,
                                        close,
                                        created_datetime,
                                        id,
                                        sell,
                                        symbol,
                                        orderId,
                                        orderListId,
                                        clientOrderId,
                                        price_ori,
                                        origQty,
                                        cummulativeQuoteQty,
                                        status,
                                        side,
                                        price_after,
                                        qty,
                                        commission,
                                        commissionAsset,
                                        tradeId,
                                    )
                                    curs.execute(sql, val)
                                    db.commit()

                                    # update query for set
                                    curs.execute(
                                        """
                                                UPDATE etx_condition_for_btctousdt
                                                SET margincalllimit = %s, buysell = %s, set_profitratio_portfoli = %s, set_profitratio_portfo_finalvalli = %s, call_back_for_loss = %s, stoplosslimite = %s
                                                WHERE id = %s
                                                """,
                                        (
                                            margincalllimit,
                                            sell,
                                            set_profitratio_portfoli,
                                            set_profitratio_portfo_finalvalli,
                                            call_back_for_loss,
                                            stoplosslimite,
                                            id,
                                        ),
                                    )
                                    db.commit()

                                    print("done buy")

                                except BinanceAPIException as e:
                                    # error handling goes here
                                    curs = db.cursor(MySQLdb.cursors.DictCursor)

                                    sql = "INSERT INTO efx_bids_for_buysell (user_id, buy_or_sell_amount, btctousdpointaction, btctousdtprise, created_datetime, id_whichsellorbuy, buy_or_sell, symbol, orderId, orderListId, clientOrderId, price, origQty, cummulativeQuoteQty, status, 	side, price_with_commision, qty_with_commision, commission, commissionAsset, tradeId) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                                    val = (
                                        user_id,
                                        buy_or_sell_amount,
                                        pricechange,
                                        close,
                                        created_datetime,
                                        id,
                                        sell,
                                        symbol,
                                        0,
                                        0,
                                        0,
                                        0,
                                        0,
                                        0,
                                        e,
                                        "none",
                                        0,
                                        0,
                                        0,
                                        "none",
                                        0,
                                    )
                                    curs.execute(sql, val)
                                    db.commit()
                                except BinanceOrderException as e:
                                    # error handling goes here
                                    curs = db.cursor(MySQLdb.cursors.DictCursor)

                                    sql = "INSERT INTO efx_bids_for_buysell (user_id, buy_or_sell_amount, btctousdpointaction, btctousdtprise, created_datetime, id_whichsellorbuy, buy_or_sell, symbol, orderId, orderListId, clientOrderId, price, origQty, cummulativeQuoteQty, status, 	side, price_with_commision, qty_with_commision, commission, commissionAsset, tradeId) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                                    val = (
                                        user_id,
                                        buy_or_sell_amount,
                                        pricechange,
                                        close,
                                        created_datetime,
                                        id,
                                        sell,
                                        symbol,
                                        0,
                                        0,
                                        0,
                                        0,
                                        0,
                                        0,
                                        e,
                                        "none",
                                        0,
                                        0,
                                        0,
                                        "none",
                                        0,
                                    )
                                    curs.execute(sql, val)
                                    db.commit()
                            else:
                                margincalllimit = 0
                                sell = 0
                                curs = db.cursor(MySQLdb.cursors.DictCursor)
                                curs.execute(
                                    """
                                                UPDATE etx_condition_for_btctousdt
                                                SET margincalllimit = %s, buysell = %s
                                                WHERE id = %s
                                                """,
                                    (margincalllimit, sell, id),
                                )
                                db.commit()
                            # buy here call_back_for_loss
                    elif a["buysell"] == 1:
                        if setvalu <= lastpointvalu:
                            # check hiher
                            curs.execute(
                                """select * from efx_percentage_change_adausdt  order by id DESC LIMIT 1,1"""
                            )
                            adausdt2 = curs.fetchall()
                            dic = list(map(itemgetter("close"), adausdt2))
                            dic = np.array(dic)
                            dic = dic.astype(float)
                            secondlastval = float(dic)
                            conditionfirst = 1
                            curs.execute(
                                """
                                            UPDATE etx_condition_for_btctousdt
                                            SET conditionfirst = %s
                                            WHERE id = %s
                                            """,
                                (conditionfirst, id),
                            )
                            db.commit()
                            if secondlastval < lastpointvalu:
                                setpoint = a["check_log"] + 1
                                sellingpriceperce = a["sell_stop_limit"]
                                sellprice = sellingpriceperce / 100
                                newsellpri = sellprice * lastpointvalu
                                sellprisefinal = lastpointvalu - newsellpri
                                old_last = a["positiontakecall"]
                                if old_last < sellprisefinal:
                                    curs.execute(
                                        """
                                                UPDATE etx_condition_for_btctousdt
                                                SET check_log = %s, positiontakecall = %s
                                                WHERE id = %s
                                                """,
                                        (setpoint, sellprisefinal, id),
                                    )
                                    db.commit()
                            else:
                                sellsecondprise = a["positiontakecall"]
                                if sellsecondprise > lastpointvalu:
                                    id = a["id"]
                                    user_id = a["user_id"]
                                    buy_or_sell_amount = a["firstbuyinamout"]
                                    margincalllimit = 0
                                    created_datetime = int(time.time())
                                    sell = 0
                                    symbol = a["typeofpoint"]
                                    # get balance
                                    api_key = a["binance_apikey"]
                                    api_secret = a["binance_securitykey"]
                                    client = Client(api_key, api_secret)
                                    # fetch exchange info
                                    balance = client.get_asset_balance(asset="ADA")
                                    current_bln = float(balance["free"])
                                    current_bln_fin = current_bln
                                    sql_bal1 = """select * from efx_bids_for_buysell where  buy_or_sell = 1 AND id_whichsellorbuy = %s"""
                                    val_bal1 = (id,)
                                    curs.execute(sql_bal1, val_bal1)
                                    adausdt3 = curs.fetchall()
                                    new = list(sub["origQty"] for sub in adausdt3)
                                    secondlastval2 = new[0]
                                    priseforbuy = list(sub["price"] for sub in adausdt3)
                                    priseforbuyf = priseforbuy[0]
                                    qunforbuy = list(sub["origQty"] for sub in adausdt3)
                                    qunforbuyf = qunforbuy[0]
                                    finalget = priseforbuyf * qunforbuyf
                                    if current_bln_fin >= secondlastval2:
                                        try:
                                            buy_limit = client.order_market_sell(
                                                symbol=symbol,
                                                side=SIDE_SELL,
                                                quantity=secondlastval2,
                                            )
                                            symbol = buy_limit["symbol"]
                                            orderId = buy_limit["orderId"]
                                            orderListId = buy_limit["orderListId"]
                                            clientOrderId = buy_limit["clientOrderId"]
                                            price_orideal = buy_limit["fills"]
                                            for i in price_orideal:
                                                price_ori = i["price"]
                                            origQty = buy_limit["origQty"]
                                            cummulativeQuoteQty = buy_limit[
                                                "cummulativeQuoteQty"
                                            ]
                                            status = buy_limit["status"]
                                            side = buy_limit["side"]
                                            lastpriceforfinal = list(
                                                sub["price"] for sub in adausdt3
                                            )
                                            secondlastval5 = lastpriceforfinal[0]
                                            price_after = float(price_ori) * float(
                                                origQty
                                            ) - float(finalget)
                                            if price_after <= 0:
                                                prise_for_bot = 0
                                            else:
                                                prise_for_bot = (price_after * 20) / 100
                                                cut_bal = float(prise_for_bot)
                                                curs.execute(
                                                    """
                                                        UPDATE efx_users
                                                        SET wallet_amount = wallet_amount - %s
                                                        WHERE id = %s
                                                        """,
                                                    (cut_bal, user_id),
                                                )
                                                db.commit()
                                                sub_id = a["copy_bottradebid"]
                                                if sub_id != 0:
                                                    sub_profit = a[
                                                        "copy_tradepercentage"
                                                    ]
                                                    sub_profit1 = (
                                                        price_after * sub_profit
                                                    ) / 100
                                                    cut_bal1 = float(sub_profit1)
                                                    curs.execute(
                                                        """
                                                        UPDATE efx_users
                                                        SET wallet_amount = wallet_amount - %s
                                                        WHERE id = %s
                                                        """,
                                                        (cut_bal1, user_id),
                                                    )
                                                    db.commit()
                                                    sub_commision = cut_bal1
                                                    sub_type = "BOT"
                                                    sub_mastuserid = a[
                                                        "copy_bottradebid"
                                                    ]

                                                    sql = "INSERT INTO efx_copy_history (user_id, master_user_id, trade_amount, trade_date, profit, commission, type) VALUE (%s,%s,%s,%s,%s,%s,%s)"
                                                    val = (
                                                        user_id,
                                                        sub_mastuserid,
                                                        buy_or_sell_amount,
                                                        created_datetime,
                                                        sub_profit1,
                                                        sub_commision,
                                                        sub_type,
                                                    )
                                                    curs.execute(sql, val)
                                                    db.commit()
                                            qty = 0
                                            if "cut_bal1" in locals():
                                                commission = cut_bal1
                                            else:
                                                commission = 0
                                            commissionAsset = "Null"
                                            tradeId = 0
                                            curs = db.cursor(MySQLdb.cursors.DictCursor)
                                            
                                            sql = "INSERT INTO efx_bids_for_buysell (user_id, buy_or_sell_amount, btctousdpointaction, btctousdtprise, created_datetime, id_whichsellorbuy, buy_or_sell, symbol, orderId, orderListId, clientOrderId, price, origQty, cummulativeQuoteQty, status, 	side, price_with_commision, qty_with_commision, commission, commissionAsset, tradeId) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                                            val = (
                                                user_id,
                                                buy_or_sell_amount,
                                                pricechange,
                                                close,
                                                created_datetime,
                                                id,
                                                sell,
                                                symbol,
                                                orderId,
                                                orderListId,
                                                clientOrderId,
                                                price_ori,
                                                origQty,
                                                cummulativeQuoteQty,
                                                status,
                                                side,
                                                price_after,
                                                prise_for_bot,
                                                commission,
                                                commissionAsset,
                                                tradeId,
                                            )
                                            curs.execute(sql, val)
                                            db.commit()
                                            
                                            # update query for sell
                                            curs.execute(
                                                """
                                                        UPDATE etx_condition_for_btctousdt
                                                        SET margincalllimit = %s, buysell = %s
                                                        WHERE id = %s
                                                        """,
                                                (margincalllimit, sell, id),
                                            )
                                            db.commit()
                                            sql = "INSERT INTO profit_from_bot(total_profit,date_whenget,id_whom_give) VALUE (%s,%s,%s)"
                                            val = (prise_for_bot, created_datetime, id)
                                            curs.execute(sql, val)
                                            db.commit()
                                        except BinanceAPIException as e:
                                            # error handling goes here
                                            curs = db.cursor(MySQLdb.cursors.DictCursor)
                                            
                                            sql = "INSERT INTO efx_bids_for_buysell (user_id, buy_or_sell_amount, btctousdpointaction, btctousdtprise, created_datetime, id_whichsellorbuy, buy_or_sell, symbol, orderId, orderListId, clientOrderId, price, origQty, cummulativeQuoteQty, status, 	side, price_with_commision, qty_with_commision, commission, commissionAsset, tradeId) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                                            val = (
                                                user_id,
                                                buy_or_sell_amount,
                                                pricechange,
                                                close,
                                                created_datetime,
                                                id,
                                                sell,
                                                symbol,
                                                0,
                                                0,
                                                0,
                                                0,
                                                0,
                                                0,
                                                e,
                                                "none",
                                                0,
                                                0,
                                                0,
                                                "none",
                                                0,
                                            )
                                            curs.execute(sql, val)
                                            db.commit()
                                        except BinanceOrderException as e:
                                            # error handling goes here
                                            curs = db.cursor(MySQLdb.cursors.DictCursor)

                                            sql = "INSERT INTO efx_bids_for_buysell (user_id, buy_or_sell_amount, btctousdpointaction, btctousdtprise, created_datetime, id_whichsellorbuy, buy_or_sell, symbol, orderId, orderListId, clientOrderId, price, origQty, cummulativeQuoteQty, status, 	side, price_with_commision, qty_with_commision, commission, commissionAsset, tradeId) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                                            val = (
                                                user_id,
                                                buy_or_sell_amount,
                                                pricechange,
                                                close,
                                                created_datetime,
                                                id,
                                                sell,
                                                symbol,
                                                0,
                                                0,
                                                0,
                                                0,
                                                0,
                                                0,
                                                e,
                                                "none",
                                                0,
                                                0,
                                                0,
                                                "none",
                                                0,
                                            )
                                            curs.execute(sql, val)
                                            db.commit()
                                    else:
                                        margincalllimit = 0
                                        sell = 0
                                        curs = db.cursor(MySQLdb.cursors.DictCursor)
                                        curs.execute(
                                            """
                                                        UPDATE etx_condition_for_btctousdt
                                                        SET margincalllimit = %s, buysell = %s
                                                        WHERE id = %s
                                                        """,
                                            (margincalllimit, sell, id),
                                        )
                                        db.commit()
                        elif conditionfirst == 1:
                            sellvaluefisrt = a["call_back_for_loss"]
                            if sellvaluefisrt >= lastpointvalu:
                                id = a["id"]
                                user_id = a["user_id"]
                                buy_or_sell_amount = a["firstbuyinamout"]
                                margincalllimit = 0
                                created_datetime = int(time.time())

                                sell = 0
                                symbol = a["typeofpoint"]
                                # get balance
                                api_key = a["binance_apikey"]
                                api_secret = a["binance_securitykey"]
                                client = Client(api_key, api_secret)
                                # fetch exchange info
                                balance = client.get_asset_balance(asset="ADA")
                                current_bln = float(balance["free"])
                                current_bln_fin = current_bln
                                sql_bal1 = """select * from efx_bids_for_buysell where  buy_or_sell = 1 AND id_whichsellorbuy = %s"""
                                val_bal1 = (id,)
                                curs.execute(sql_bal1, val_bal1)
                                adausdt5 = curs.fetchall()
                                new = list(sub["origQty"] for sub in adausdt5)
                                secondlastval2 = new[0]
                                priseforbuy = list(sub["price"] for sub in adausdt5)
                                priseforbuyf = priseforbuy[0]
                                qunforbuy = list(sub["origQty"] for sub in adausdt5)
                                qunforbuyf = qunforbuy[0]
                                finalget = priseforbuyf * qunforbuyf
                                if current_bln_fin >= secondlastval2:
                                    try:
                                        buy_limit = client.order_market_sell(
                                            symbol=symbol,
                                            side=SIDE_SELL,
                                            quantity=secondlastval2,
                                        )
                                        symbol = buy_limit["symbol"]
                                        orderId = buy_limit["orderId"]
                                        orderListId = buy_limit["orderListId"]
                                        clientOrderId = buy_limit["clientOrderId"]
                                        price_orideal = buy_limit["fills"]
                                        for i in price_orideal:
                                            price_ori = i["price"]
                                        origQty = buy_limit["origQty"]
                                        cummulativeQuoteQty = buy_limit[
                                            "cummulativeQuoteQty"
                                        ]
                                        status = buy_limit["status"]
                                        side = buy_limit["side"]
                                        lastpriceforfinal = list(
                                            sub["price"] for sub in adausdt5
                                        )
                                        secondlastval5 = lastpriceforfinal[0]
                                        price_after = float(price_ori) * float(
                                            origQty
                                        ) - float(finalget)
                                        if price_after < 0:
                                            prise_for_bot = 0
                                        else:
                                            prise_for_bot = (price_after * 20) / 100
                                            cut_bal = float(prise_for_bot)
                                            curs.execute(
                                                """
                                                    UPDATE efx_users
                                                    SET wallet_amount = wallet_amount - %s
                                                    WHERE id = %s
                                                    """,
                                                (cut_bal, user_id),
                                            )
                                            db.commit()
                                            sub_id = a["copy_bottradebid"]
                                            if sub_id != 0:
                                                sub_profit = a[
                                                    "copy_tradepercentage"
                                                ]
                                                sub_profit1 = (
                                                    price_after * sub_profit
                                                ) / 100
                                                cut_bal1 = float(sub_profit1)
                                                curs.execute(
                                                    """
                                                    UPDATE efx_users
                                                    SET wallet_amount = wallet_amount - %s
                                                    WHERE id = %s
                                                    """,
                                                    (cut_bal1, user_id),
                                                )
                                                db.commit()
                                                sub_commision = cut_bal1
                                                sub_type = "BOT"
                                                sub_mastuserid = a[
                                                    "copy_bottradebid"
                                                ]

                                                sql = "INSERT INTO efx_copy_history (user_id, master_user_id, trade_amount, trade_date, profit, commission, type) VALUE (%s,%s,%s,%s,%s,%s,%s)"
                                                val = (
                                                    user_id,
                                                    sub_mastuserid,
                                                    buy_or_sell_amount,
                                                    created_datetime,
                                                    sub_profit1,
                                                    sub_commision,
                                                    sub_type,
                                                )
                                                curs.execute(sql, val)
                                                db.commit()
                                        qty = 0
                                        if "cut_bal1" in locals():
                                            commission = cut_bal1
                                        else:
                                            commission = 0
                                        commissionAsset = "Null"
                                        tradeId = 0
                                        curs = db.cursor(MySQLdb.cursors.DictCursor)
                                        sql = "INSERT INTO efx_bids_for_buysell (user_id, buy_or_sell_amount, btctousdpointaction, btctousdtprise, created_datetime, id_whichsellorbuy, buy_or_sell, symbol, orderId, orderListId, clientOrderId, price, origQty, cummulativeQuoteQty, status, 	side, price_with_commision, qty_with_commision, commission, commissionAsset, tradeId) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                                        val = (
                                            user_id,
                                            buy_or_sell_amount,
                                            pricechange,
                                            close,
                                            created_datetime,
                                            id,
                                            sell,
                                            symbol,
                                            orderId,
                                            orderListId,
                                            clientOrderId,
                                            price_ori,
                                            origQty,
                                            cummulativeQuoteQty,
                                            status,
                                            side,
                                            price_after,
                                            prise_for_bot,
                                            commission,
                                            commissionAsset,
                                            tradeId,
                                        )
                                        curs.execute(sql, val)
                                        db.commit()
                                        # update query for sell
                                        curs.execute(
                                            """
                                                    UPDATE etx_condition_for_btctousdt
                                                    SET margincalllimit = %s, buysell = %s
                                                    WHERE id = %s
                                                    """,
                                            (margincalllimit, sell, id),
                                        )
                                        db.commit()
                                        sql = "INSERT INTO profit_from_bot(total_profit,date_whenget,id_whom_give) VALUE (%s,%s,%s)"
                                        val = (prise_for_bot, created_datetime, id)
                                        curs.execute(sql, val)
                                        db.commit()
                                    except BinanceAPIException as e:
                                        # error handling goes here
                                        curs = db.cursor(MySQLdb.cursors.DictCursor)
                                        sql = "INSERT INTO efx_bids_for_buysell (user_id, buy_or_sell_amount, btctousdpointaction, btctousdtprise, created_datetime, id_whichsellorbuy, buy_or_sell, symbol, orderId, orderListId, clientOrderId, price, origQty, cummulativeQuoteQty, status, 	side, price_with_commision, qty_with_commision, commission, commissionAsset, tradeId) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                                        val = (
                                            user_id,
                                            buy_or_sell_amount,
                                            pricechange,
                                            close,
                                            created_datetime,
                                            id,
                                            sell,
                                            symbol,
                                            0,
                                            0,
                                            0,
                                            0,
                                            0,
                                            0,
                                            e,
                                            "none",
                                            0,
                                            0,
                                            0,
                                            "none",
                                            0,
                                        )
                                        curs.execute(sql, val)
                                        db.commit()
                                    except BinanceOrderException as e:
                                        # error handling goes here
                                        curs = db.cursor(MySQLdb.cursors.DictCursor)

                                        sql = "INSERT INTO efx_bids_for_buysell (user_id, buy_or_sell_amount, btctousdpointaction, btctousdtprise, created_datetime, id_whichsellorbuy, buy_or_sell, symbol, orderId, orderListId, clientOrderId, price, origQty, cummulativeQuoteQty, status, 	side, price_with_commision, qty_with_commision, commission, commissionAsset, tradeId) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                                        val = (
                                            user_id,
                                            buy_or_sell_amount,
                                            pricechange,
                                            close,
                                            created_datetime,
                                            id,
                                            sell,
                                            symbol,
                                            0,
                                            0,
                                            0,
                                            0,
                                            0,
                                            0,
                                            e,
                                            "none",
                                            0,
                                            0,
                                            0,
                                            "none",
                                            0,
                                        )
                                        curs.execute(sql, val)
                                        db.commit()
                                else:
                                    margincalllimit = 0
                                    sell = 0
                                    curs = db.cursor(MySQLdb.cursors.DictCursor)
                                    curs.execute(
                                        """
                                                        UPDATE etx_condition_for_btctousdt
                                                        SET margincalllimit = %s, buysell = %s
                                                        WHERE id = %s
                                                        """,
                                        (margincalllimit, sell, id),
                                    )
                                    db.commit()
                    elif a["buysell"] == 4:
                        # buyinstatli
                        id = a["id"]
                        # print(id)
                        user_id = a["user_id"]
                        buy_or_sell_amount = a["firstbuyinamout"]
                        created_datetime = int(time.time())
                        margincalllimit = a["margincalllimit"]
                        sell = 1
                        set_profit_ratio = a["sell_point"]
                        cal_loss = a["stop_loss"]
                        stoplosslimite = a["sell_stop_limit"]
                        symbol = a["typeofpoint"]
                        # get balance
                        api_key = a["binance_apikey"]
                        api_secret = a["binance_securitykey"]
                        client = Client(api_key, api_secret)
                        # fetch exchange info
                        balance = client.get_asset_balance(asset="USDT")
                        current_bln = float(balance["free"])
                        current_bln_fin = current_bln + 0.5
                        
                        if current_bln_fin >= buy_or_sell_amount:
                            try:
                                info = client.get_symbol_info(symbol)
                                val = info["filters"][2]["stepSize"]
                                decimal = 0
                                is_dec = False
                                for c in val:
                                    if is_dec is True:
                                        decimal += 1
                                    if c == "1":
                                        break
                                    if c == ".":
                                        is_dec = True

                                qun_num1 = float(buy_or_sell_amount / close)
                                qun_num = round(qun_num1, decimal)
                                buy_limit = client.order_market_buy(
                                    symbol=symbol, quantity=qun_num
                                )
                                symbol = buy_limit["symbol"]
                                orderId = buy_limit["orderId"]
                                orderListId = buy_limit["orderListId"]
                                clientOrderId = buy_limit["clientOrderId"]
                                price_orideal = buy_limit["fills"]
                                for i in price_orideal:
                                    price_ori = i["price"]
                                    price_ori = Decimal(price_ori)
                                origQty = buy_limit["origQty"]
                                cummulativeQuoteQty = buy_limit["cummulativeQuoteQty"]
                                status = buy_limit["status"]
                                side = buy_limit["side"]
                                price_after = 0
                                qty = 0
                                commission = 0
                                commissionAsset = "Null"
                                tradeId = 0
                                calu_profit = (price_ori * set_profit_ratio) / 100
                                sell2 = (set_profit_ratio) - (cal_loss)
                                fin_cal_loss = (price_ori * sell2) / 100

                                stoplosslimite1 = (price_ori * stoplosslimite) / 100
                                stoplosslimite2 = price_ori - stoplosslimite1

                                stoplimit = set_profit_ratio - stoplosslimite
                                fin_thired_point = (price_ori * stoplimit) / 100

                                set_profitratio_portfoli = calu_profit
                                set_profitratio_portfo_finalvalli = (
                                    calu_profit + price_ori
                                )
                                call_back_for_loss = price_ori + fin_thired_point
                                stoplosslimite = stoplosslimite2

                                curs = db.cursor(MySQLdb.cursors.DictCursor)

                                sql = "INSERT INTO efx_bids_for_buysell (user_id, buy_or_sell_amount, btctousdpointaction, btctousdtprise, created_datetime, id_whichsellorbuy, buy_or_sell, symbol, orderId, orderListId, clientOrderId, price, origQty, cummulativeQuoteQty, status, 	side, price_with_commision, qty_with_commision, commission, commissionAsset, tradeId) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                                val = (
                                    user_id,
                                    buy_or_sell_amount,
                                    pricechange,
                                    close,
                                    created_datetime,
                                    id,
                                    sell,
                                    symbol,
                                    orderId,
                                    orderListId,
                                    clientOrderId,
                                    price_ori,
                                    origQty,
                                    cummulativeQuoteQty,
                                    status,
                                    side,
                                    price_after,
                                    qty,
                                    commission,
                                    commissionAsset,
                                    tradeId,
                                )
                                curs.execute(sql, val)
                                db.commit()

                                # update query for set
                                curs.execute(
                                    """
                                            UPDATE etx_condition_for_btctousdt
                                            SET margincalllimit = %s, buysell = %s, set_profitratio_portfoli = %s, set_profitratio_portfo_finalvalli = %s, call_back_for_loss = %s, stoplosslimite = %s
                                            WHERE id = %s
                                            """,
                                    (
                                        margincalllimit,
                                        sell,
                                        set_profitratio_portfoli,
                                        set_profitratio_portfo_finalvalli,
                                        call_back_for_loss,
                                        stoplosslimite,
                                        id,
                                    ),
                                )
                                db.commit()

                                print("done buy")

                            except BinanceAPIException as e:
                                # error handling goes here
                                curs = db.cursor(MySQLdb.cursors.DictCursor)

                                sql = "INSERT INTO efx_bids_for_buysell (user_id, buy_or_sell_amount, btctousdpointaction, btctousdtprise, created_datetime, id_whichsellorbuy, buy_or_sell, symbol, orderId, orderListId, clientOrderId, price, origQty, cummulativeQuoteQty, status, 	side, price_with_commision, qty_with_commision, commission, commissionAsset, tradeId) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                                val = (
                                    user_id,
                                    buy_or_sell_amount,
                                    pricechange,
                                    close,
                                    created_datetime,
                                    id,
                                    sell,
                                    symbol,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    e,
                                    "none",
                                    0,
                                    0,
                                    0,
                                    "none",
                                    0,
                                )
                                curs.execute(sql, val)
                                db.commit()
                            except BinanceOrderException as e:
                                # error handling goes here
                                curs = db.cursor(MySQLdb.cursors.DictCursor)

                                sql = "INSERT INTO efx_bids_for_buysell (user_id, buy_or_sell_amount, btctousdpointaction, btctousdtprise, created_datetime, id_whichsellorbuy, buy_or_sell, symbol, orderId, orderListId, clientOrderId, price, origQty, cummulativeQuoteQty, status, 	side, price_with_commision, qty_with_commision, commission, commissionAsset, tradeId) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                                val = (
                                    user_id,
                                    buy_or_sell_amount,
                                    pricechange,
                                    close,
                                    created_datetime,
                                    id,
                                    sell,
                                    symbol,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    e,
                                    "none",
                                    0,
                                    0,
                                    0,
                                    "none",
                                    0,
                                )
                                curs.execute(sql, val)
                                db.commit()
                        else:
                            margincalllimit = 0
                            sell = 0
                            curs = db.cursor(MySQLdb.cursors.DictCursor)
                            curs.execute(
                                """
                                            UPDATE etx_condition_for_btctousdt
                                            SET margincalllimit = %s, buysell = %s
                                            WHERE id = %s
                                            """,
                                (margincalllimit, sell, id),
                            )
                            db.commit()
                    # buy here call_back_for_loss
                    elif a["buysell"] == 5:
                        id = a["id"]
                        user_id = a["user_id"]
                        buy_or_sell_amount = a["firstbuyinamout"]
                        margincalllimit = 0
                        created_datetime = int(time.time())
                        sell = 0
                        symbol = a["typeofpoint"]
                        # get balance
                        api_key = a["binance_apikey"]
                        api_secret = a["binance_securitykey"]
                        client = Client(api_key, api_secret)
                        # fetch exchange info
                        balance = client.get_asset_balance(asset="ADA")
                        current_bln = float(balance["free"])
                        current_bln_fin = current_bln
                        sql_bal1 = """select * from efx_bids_for_buysell where  buy_or_sell = 1 AND id_whichsellorbuy = %s"""
                        val_bal1 = (id,)
                        curs.execute(sql_bal1, val_bal1)
                        adausdt3 = curs.fetchall()
                        new = list(sub["origQty"] for sub in adausdt3)
                        secondlastval2 = new[0]
                        priseforbuy = list(sub["price"] for sub in adausdt3)
                        priseforbuyf = priseforbuy[0]
                        qunforbuy = list(sub["origQty"] for sub in adausdt3)
                        qunforbuyf = qunforbuy[0]
                        finalget = priseforbuyf * qunforbuyf
                        if current_bln_fin >= secondlastval2:
                            try:
                                buy_limit = client.order_market_sell(
                                    symbol=symbol, side=SIDE_SELL, quantity=qun_num
                                )
                                symbol = buy_limit["symbol"]
                                orderId = buy_limit["orderId"]
                                orderListId = buy_limit["orderListId"]
                                clientOrderId = buy_limit["clientOrderId"]
                                price_orideal = buy_limit["fills"]
                                for i in price_orideal:
                                    price_ori = i["price"]
                                origQty = buy_limit["origQty"]
                                cummulativeQuoteQty = buy_limit["cummulativeQuoteQty"]
                                status = buy_limit["status"]
                                side = buy_limit["side"]
                                lastpriceforfinal = list(
                                    sub["price"] for sub in adausdt3
                                )
                                secondlastval5 = lastpriceforfinal[0]
                                price_after = float(price_ori) * float(origQty) - float(
                                    finalget
                                )
                                if price_after < 0:
                                    prise_for_bot = 0
                                else:
                                    prise_for_bot = (price_after * 20) / 100
                                    cut_bal = float(prise_for_bot)
                                    curs.execute(
                                        """
                                            UPDATE efx_users
                                            SET wallet_amount = wallet_amount - %s
                                            WHERE id = %s
                                            """,
                                        (cut_bal, user_id),
                                    )
                                    db.commit()
                                    sub_id = a["copy_bottradebid"]
                                    if sub_id != 0:
                                        sub_profit = a[
                                            "copy_tradepercentage"
                                        ]
                                        sub_profit1 = (
                                            price_after * sub_profit
                                        ) / 100
                                        cut_bal1 = float(sub_profit1)
                                        curs.execute(
                                            """
                                            UPDATE efx_users
                                            SET wallet_amount = wallet_amount - %s
                                            WHERE id = %s
                                            """,
                                            (cut_bal1, user_id),
                                        )
                                        db.commit()
                                        sub_commision = cut_bal1
                                        sub_type = "BOT"
                                        sub_mastuserid = a[
                                            "copy_bottradebid"
                                        ]

                                        sql = "INSERT INTO efx_copy_history (user_id, master_user_id, trade_amount, trade_date, profit, commission, type) VALUE (%s,%s,%s,%s,%s,%s,%s)"
                                        val = (
                                            user_id,
                                            sub_mastuserid,
                                            buy_or_sell_amount,
                                            created_datetime,
                                            sub_profit1,
                                            sub_commision,
                                            sub_type,
                                        )
                                        curs.execute(sql, val)
                                        db.commit()
                                qty = 0
                                if "cut_bal1" in locals():
                                    commission = cut_bal1
                                else:
                                    commission = 0
                                commissionAsset = "Null"
                                tradeId = 0
                                curs = db.cursor(MySQLdb.cursors.DictCursor)
                                
                                sql = "INSERT INTO efx_bids_for_buysell (user_id, buy_or_sell_amount, btctousdpointaction, btctousdtprise, created_datetime, id_whichsellorbuy, buy_or_sell, symbol, orderId, orderListId, clientOrderId, price, origQty, cummulativeQuoteQty, status, 	side, price_with_commision, qty_with_commision, commission, commissionAsset, tradeId) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                                val = (
                                    user_id,
                                    buy_or_sell_amount,
                                    pricechange,
                                    close,
                                    created_datetime,
                                    id,
                                    sell,
                                    symbol,
                                    orderId,
                                    orderListId,
                                    clientOrderId,
                                    price_ori,
                                    origQty,
                                    cummulativeQuoteQty,
                                    status,
                                    side,
                                    price_after,
                                    prise_for_bot,
                                    commission,
                                    commissionAsset,
                                    tradeId,
                                )
                                curs.execute(sql, val)
                                db.commit()
                                
                                # update query for sell
                                curs.execute(
                                    """
                                            UPDATE etx_condition_for_btctousdt
                                            SET margincalllimit = %s, buysell = %s
                                            WHERE id = %s
                                            """,
                                    (margincalllimit, sell, id),
                                )
                                db.commit()
                                sql = "INSERT INTO profit_from_bot(total_profit,date_whenget,id_whom_give) VALUE (%s,%s,%s)"
                                val = (prise_for_bot, created_datetime, id)
                                curs.execute(sql, val)
                                db.commit()
                                
                            except BinanceAPIException as e:
                                # error handling goes here
                                curs = db.cursor(MySQLdb.cursors.DictCursor)
                                
                                sql = "INSERT INTO efx_bids_for_buysell (user_id, buy_or_sell_amount, btctousdpointaction, btctousdtprise, created_datetime, id_whichsellorbuy, buy_or_sell, symbol, orderId, orderListId, clientOrderId, price, origQty, cummulativeQuoteQty, status, 	side, price_with_commision, qty_with_commision, commission, commissionAsset, tradeId) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                                val = (
                                    user_id,
                                    buy_or_sell_amount,
                                    pricechange,
                                    close,
                                    created_datetime,
                                    id,
                                    sell,
                                    symbol,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    e,
                                    "none",
                                    0,
                                    0,
                                    0,
                                    "none",
                                    0,
                                )
                                curs.execute(sql, val)
                                db.commit()
                            except BinanceOrderException as e:
                                # error handling goes here
                                curs = db.cursor(MySQLdb.cursors.DictCursor)

                                sql = "INSERT INTO efx_bids_for_buysell (user_id, buy_or_sell_amount, btctousdpointaction, btctousdtprise, created_datetime, id_whichsellorbuy, buy_or_sell, symbol, orderId, orderListId, clientOrderId, price, origQty, cummulativeQuoteQty, status, 	side, price_with_commision, qty_with_commision, commission, commissionAsset, tradeId) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                                val = (
                                    user_id,
                                    buy_or_sell_amount,
                                    pricechange,
                                    close,
                                    created_datetime,
                                    id,
                                    sell,
                                    symbol,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    e,
                                    "none",
                                    0,
                                    0,
                                    0,
                                    "none",
                                    0,
                                )
                                curs.execute(sql, val)
                                db.commit()
                        else:
                            margincalllimit = 0
                            sell = 0
                            curs = db.cursor(MySQLdb.cursors.DictCursor)
                            curs.execute(
                                """
                                            UPDATE etx_condition_for_btctousdt
                                            SET margincalllimit = %s, buysell = %s
                                            WHERE id = %s
                                            """,
                                (margincalllimit, sell, id),
                            )
                            db.commit()
                except:
                    e = "key error"
                    # cycle check
            sql_bal3 = """select * from bot_stratage where coin = 'ADAUSDT' and cycle_status = 1"""
            curs.execute(
                sql_bal3,
            )
            kavausdtforstratage = curs.fetchall()
            if not kavausdtforstratage:
                new = 0
            else:
                for kavausdtforstratagesingle in kavausdtforstratage:
                    user_id1 = kavausdtforstratagesingle["id_whom_set"]
                    stratage_id = kavausdtforstratagesingle["id"]
                    margincalllimit = 0
                    buysell = 0
                    typeofpoint = "ADAUSDT"
                    curs.execute(
                        """
                            UPDATE etx_condition_for_btctousdt
                            SET margincalllimit = %s, buysell = %s
                            WHERE typeofpoint = %s and user_id = %s
                            """,
                        (
                            margincalllimit,
                            buysell,
                            typeofpoint,
                            user_id1,
                        ),
                    )
                    db.commit()
                    sql_bal4 = """select * from etx_condition_for_btctousdt where stratage_id = %s and user_id = %s and margincalllimit != 0 and  buysell in (-1,1)"""
                    val_bal4 = (stratage_id, user_id1)
                    curs.execute(sql_bal4, val_bal4)
                    kavausdtforstratage1 = curs.fetchall()
                    if not kavausdtforstratage1:
                        sql_bal5 = """SELECT * FROM xenbot.bot_stratage c left join xenbot.multiple_bid_forbot o on  o.stratage_id = c.id where c.id = %s and c.id_whom_set = %s"""
                        val_bal5 = (stratage_id, user_id1)
                        curs.execute(sql_bal5, val_bal5)
                        kavausdtforstratage2 = curs.fetchall()
                        for stratage in kavausdtforstratage2:
                            tradeamount = float(stratage["tradeamount"]) * float(
                                stratage["buyamountmul"]
                            )
                            coin = stratage["coin"]
                            user_idfor_cycle = stratage["id_whom_set"]
                            sellfirstpoint = stratage["sell"]
                            sellsecondpoint = stratage["sellstop"]
                            creatdate = int(time.time())
                            stratage_id = stratage_id
                            margincalllimitforcyc = 1
                            buysellforcycle = -1
                            buypoint = -float(stratage["buypoint"])
                            api_keyforcycle = stratage["apikey"]
                            api_keysecurityforcycle = stratage["apisecurity"]
                            curs = db.cursor(MySQLdb.cursors.DictCursor)
                            total_stratageforcy = stratage["total_stratage"]
                            multiply_cycle = stratage["buyamountmul"]

                            sql = "INSERT INTO etx_condition_for_btctousdt (user_id, firstbuyinamout, margincalllimit,buysell, takieprofitratio, created_date, sell_point, sell_stop_limit, typeofpoint, binance_apikey, binance_securitykey, total_stratage, stratage_id, multiplebuyinratio) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            val = (
                                user_idfor_cycle,
                                tradeamount,
                                margincalllimitforcyc,
                                buysellforcycle,
                                buypoint,
                                creatdate,
                                sellfirstpoint,
                                sellsecondpoint,
                                coin,
                                api_keyforcycle,
                                api_keysecurityforcycle,
                                total_stratageforcy,
                                stratage_id,
                                multiply_cycle,
                            )
                            curs.execute(sql, val)
                            db.commit()
                    else:
                        new = 1


loop = asyncio.get_event_loop()
try:
    asyncio.ensure_future(listen())
    loop.run_forever()
except KeyboardInterrupt:
    pass
finally:
    print("Closing Loop")
    loop.close()
