import asyncio
import datetime
import datetime
import os, sys
from win32com.client import GetObject
import MySQLdb.cursors

db = MySQLdb.connect("localhost", "root", "", "xenbot")


async def listen():
    while True:
        await asyncio.sleep(10)
        curs = db.cursor(MySQLdb.cursors.DictCursor)
        result = datetime.datetime.now()
        dat_new = result.strftime("%Y-%m-%d %H:%M %p")
        showtime = result.strftime("%Y-%m-%d %H:%M:%S %p")
        curs.execute("SELECT * FROM apidata_get_check")
        myresult = curs.fetchall()

        for i in myresult:
            store_tiem = i['timestamp']
            if i["timestamp"] != dat_new:
                # START "for new nodechart stop"
                WMI = GetObject("winmgmts:")
                processes = WMI.InstancesOf("Win32_Process")
                for p in WMI.ExecQuery(
                    'select * from Win32_Process where Name="cmd.exe"'
                ):
                    old_path = str(p.Properties_("CommandLine").Value)
                    print(old_path)
                    if old_path == "cmd  /K python datagetandstore.py":
                        print(old_path)
                        os.system(
                            "taskkill /pid " + str(p.Properties_("ProcessId").Value)
                        )
                        print("old close")
                        await asyncio.sleep(5.0)
                # START "for new nodechart stop"
                ########################################################################
                # START "for new nodechart start"
                class cd:
                    """Context manager for changing the current working directory"""

                    def __init__(self, newPath):
                        self.newPath = os.path.expanduser(newPath)

                    def __enter__(self):
                        self.savedPath = os.getcwd()
                        os.chdir(self.newPath)

                    def __exit__(self, etype, value, traceback):
                        os.chdir(self.savedPath)

                with cd("C:\\newuptodatev_3\\BotForTrading\\Binance\\"):
                    # we are in ~/Library
                    new = os.getcwd()
                    os.system("start cmd /K python datagetandstore.py")
                    print(new)
                    await asyncio.sleep(60.0)
                # END "for new nodechart start"
                ############################################################################
        print(showtime)


loop = asyncio.get_event_loop()
try:
    asyncio.ensure_future(listen())
    loop.run_forever()
except KeyboardInterrupt:
    pass
finally:
    print("Closing Loop")
    loop.close()
