import websockets
import requests
import asyncio
import json
import MySQLdb.cursors
import datetime
from threading import Timer
from operator import itemgetter
import time
import datetime
from numpy import arange
import numpy as np
import os,sys
from binance.client import Client
from binance.enums import *
from binance.exceptions import BinanceAPIException, BinanceOrderException
import MySQLdb.cursors

db = MySQLdb.connect("localhost", "root", "", "xenbot")


async def listen():
    url = "wss://stream.binance.com:9443/ws/!ticker@arr"
    try:
        async with websockets.connect(url) as ws:
            while True:
                await asyncio.sleep(1)
                x = datetime.datetime.now()
                print(x)
                msg = await ws.recv()
                input_dict = json.loads(msg)
                curs = db.cursor(MySQLdb.cursors.DictCursor)
                try:
                    for value in input_dict:
                        if value["s"] == "1INCHUSDT":
                            print("hello")
                            curs = db.cursor(MySQLdb.cursors.DictCursor)
                            result = datetime.datetime.now()
                            dat_new = result.strftime("%Y-%m-%d %H:%M %p")
                            curs.execute(
                                """
                                        UPDATE apidata_get_check
                                        SET timestamp = %s
                                        WHERE id = %s
                                        """,
                                (dat_new, 1),
                            )
                            db.commit()
                            print(dat_new)
                            # /////// get value \\\\\\\\\\\\\
                            open = value["o"]
                            high = value["h"]
                            low = value["l"]
                            close = value["c"]
                            # volume = input_dict[""]
                            timestamp = value["E"]
                            symbol = value["s"]
                            pricechange = value["p"]
                            pricechangepercentage = value["P"]
                            weightedaverageprice = value["w"]
                            totaltradebaseasset = value["v"]

                            sql = "INSERT INTO 	efx_percentage_change_1inchusdt (open, close, high, low, timestamp, symbol, pricechange, prisechangepercentage, weightedaverageprice, totaltradebaseasset) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            val = (
                                open,
                                close,
                                high,
                                low,
                                timestamp,
                                symbol,
                                pricechange,
                                pricechangepercentage,
                                weightedaverageprice,
                                totaltradebaseasset,
                            )
                            curs.execute(sql, val)
                            db.commit()
                            # old record delete

                            # curs.execute(
                            #     "SELECT * FROM efx_percentage_change_1inchusdt")
                            # myresult = curs.fetchall()
                            # numberva = len(myresult)

                            # if numberva > 300:
                            #     sql = "DELETE FROM efx_percentage_change_1inchusdt ORDER BY id DESC limit %s"
                            #     val = (100,)
                            #     curs.execute(sql, val)
                            #     db.commit()

                        if value["s"] == "AAVEUSDT":
                            print("hello")
                            # /////// get value \\\\\\\\\\\\\
                            open = value["o"]
                            high = value["h"]
                            low = value["l"]
                            close = value["c"]
                            # volume = input_dict[""]
                            timestamp = value["E"]
                            symbol = value["s"]
                            pricechange = value["p"]
                            pricechangepercentage = value["P"]
                            weightedaverageprice = value["w"]
                            totaltradebaseasset = value["v"]

                            sql = "INSERT INTO 	efx_percentage_change_aaveusdt (open, close, high, low, timestamp, symbol, pricechange, prisechangepercentage, weightedaverageprice, totaltradebaseasset) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            val = (
                                open,
                                close,
                                high,
                                low,
                                timestamp,
                                symbol,
                                pricechange,
                                pricechangepercentage,
                                weightedaverageprice,
                                totaltradebaseasset,
                            )
                            curs.execute(sql, val)
                            db.commit()
                            # old record delete
                            # curs.execute(
                            #     "SELECT * FROM efx_percentage_change_aaveusdt")
                            # myresult = curs.fetchall()
                            # numberva = len(myresult)
                            # if numberva > 300:
                            #     sql = "DELETE FROM efx_percentage_change_aaveusdt ORDER BY id DESC limit %s"
                            #     val = (100,)
                            #     curs.execute(sql, val)
                            #     db.commit()

                        if value["s"] == "ADAUSDT":
                            # /////// get value \\\\\\\\\\\\\
                            open = value["o"]
                            high = value["h"]
                            low = value["l"]
                            close = value["c"]
                            # volume = input_dict[""]
                            timestamp = value["E"]
                            symbol = value["s"]
                            pricechange = value["p"]
                            pricechangepercentage = value["P"]
                            weightedaverageprice = value["w"]
                            totaltradebaseasset = value["v"]

                            sql = "INSERT INTO  efx_percentage_change_adausdt (open, close, high, low, timestamp, symbol, pricechange, prisechangepercentage, weightedaverageprice, totaltradebaseasset) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            val = (
                                open,
                                close,
                                high,
                                low,
                                timestamp,
                                symbol,
                                pricechange,
                                pricechangepercentage,
                                weightedaverageprice,
                                totaltradebaseasset,
                            )
                            curs.execute(sql, val)
                            db.commit()
                            # old record delete
                            # curs.execute(
                            #     "SELECT * FROM efx_percentage_change_aaveusdt")
                            # myresult = curs.fetchall()
                            # numberva = len(myresult)
                            # if numberva > 300:
                            #     sql = "DELETE FROM efx_percentage_change_aaveusdt ORDER BY id DESC limit %s"
                            #     val = (100,)
                            #     curs.execute(sql, val)
                            #     db.commit()

                        if value["s"] == "AKROUSDT":
                            # /////// get value \\\\\\\\\\\\\
                            open = value["o"]
                            high = value["h"]
                            low = value["l"]
                            close = value["c"]
                            # volume = input_dict[""]
                            timestamp = value["E"]
                            symbol = value["s"]
                            pricechange = value["p"]
                            pricechangepercentage = value["P"]
                            weightedaverageprice = value["w"]
                            totaltradebaseasset = value["v"]

                            sql = "INSERT INTO 	efx_percentage_change_akrousdt (open, close, high, low, timestamp, symbol, pricechange, prisechangepercentage, weightedaverageprice, totaltradebaseasset) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            val = (
                                open,
                                close,
                                high,
                                low,
                                timestamp,
                                symbol,
                                pricechange,
                                pricechangepercentage,
                                weightedaverageprice,
                                totaltradebaseasset,
                            )
                            curs.execute(sql, val)
                            db.commit()
                            # old record delete
                            # curs.execute(
                            #     "SELECT * FROM efx_percentage_change_akrousdt")
                            # myresult = curs.fetchall()
                            # numberva = len(myresult)
                            # if numberva > 300:
                            #     sql = "DELETE FROM efx_percentage_change_akrousdt ORDER BY id DESC limit %s"
                            #     val = (100,)
                            #     curs.execute(sql, val)
                            #     db.commit()

                        if value["s"] == "ANTUSDT":
                            # /////// get value \\\\\\\\\\\\\
                            open = value["o"]
                            high = value["h"]
                            low = value["l"]
                            close = value["c"]
                            # volume = input_dict[""]
                            timestamp = value["E"]
                            symbol = value["s"]
                            pricechange = value["p"]
                            pricechangepercentage = value["P"]
                            weightedaverageprice = value["w"]
                            totaltradebaseasset = value["v"]

                            sql = "INSERT INTO 	efx_percentage_change_antusdt (open, close, high, low, timestamp, symbol, pricechange, prisechangepercentage, weightedaverageprice, totaltradebaseasset) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            val = (
                                open,
                                close,
                                high,
                                low,
                                timestamp,
                                symbol,
                                pricechange,
                                pricechangepercentage,
                                weightedaverageprice,
                                totaltradebaseasset,
                            )
                            curs.execute(sql, val)
                            db.commit()
                            # old record delete
                            # curs.execute(
                            #     "SELECT * FROM efx_percentage_change_antusdt")
                            # myresult = curs.fetchall()
                            # numberva = len(myresult)
                            # if numberva > 300:
                            #     sql = "DELETE FROM efx_percentage_change_antusdt ORDER BY id DESC limit %s"
                            #     val = (100,)
                            #     curs.execute(sql, val)
                            #     db.commit()

                        if value["s"] == "ATOMUSDT":
                            # /////// get value \\\\\\\\\\\\\
                            open = value["o"]
                            high = value["h"]
                            low = value["l"]
                            close = value["c"]
                            # volume = input_dict[""]
                            timestamp = value["E"]
                            symbol = value["s"]
                            pricechange = value["p"]
                            pricechangepercentage = value["P"]
                            weightedaverageprice = value["w"]
                            totaltradebaseasset = value["v"]

                            sql = "INSERT INTO 	efx_percentage_change_atomusdt (open, close, high, low, timestamp, symbol, pricechange, prisechangepercentage, weightedaverageprice, totaltradebaseasset) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            val = (
                                open,
                                close,
                                high,
                                low,
                                timestamp,
                                symbol,
                                pricechange,
                                pricechangepercentage,
                                weightedaverageprice,
                                totaltradebaseasset,
                            )
                            curs.execute(sql, val)
                            db.commit()
                            # old record delete
                            # curs.execute(
                            #     "SELECT * FROM efx_percentage_change_atomusdt")
                            # myresult = curs.fetchall()
                            # numberva = len(myresult)
                            # if numberva > 300:
                            #     sql = "DELETE FROM efx_percentage_change_atomusdt ORDER BY id DESC limit %s"
                            #     val = (100,)
                            #     curs.execute(sql, val)
                            #     db.commit()

                        if value["s"] == "BATUSDT":
                            # /////// get value \\\\\\\\\\\\\
                            open = value["o"]
                            high = value["h"]
                            low = value["l"]
                            close = value["c"]
                            # volume = input_dict[""]
                            timestamp = value["E"]
                            symbol = value["s"]
                            pricechange = value["p"]
                            pricechangepercentage = value["P"]
                            weightedaverageprice = value["w"]
                            totaltradebaseasset = value["v"]

                            sql = "INSERT INTO 	efx_percentage_change_batusdt (open, close, high, low, timestamp, symbol, pricechange, prisechangepercentage, weightedaverageprice, totaltradebaseasset) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            val = (
                                open,
                                close,
                                high,
                                low,
                                timestamp,
                                symbol,
                                pricechange,
                                pricechangepercentage,
                                weightedaverageprice,
                                totaltradebaseasset,
                            )
                            curs.execute(sql, val)
                            db.commit()
                            # old record delete
                            # curs.execute(
                            #     "SELECT * FROM efx_percentage_change_batusdt")
                            # myresult = curs.fetchall()
                            # numberva = len(myresult)
                            # if numberva > 300:
                            #     sql = "DELETE FROM efx_percentage_change_batusdt ORDER BY id DESC limit %s"
                            #     val = (100,)
                            #     curs.execute(sql, val)
                            #     db.commit()

                        if value["s"] == "BCHUSDT":
                            # /////// get value \\\\\\\\\\\\\
                            open = value["o"]
                            high = value["h"]
                            low = value["l"]
                            close = value["c"]
                            # volume = input_dict[""]
                            timestamp = value["E"]
                            symbol = value["s"]
                            pricechange = value["p"]
                            pricechangepercentage = value["P"]
                            weightedaverageprice = value["w"]
                            totaltradebaseasset = value["v"]

                            sql = "INSERT INTO 	efx_percentage_change_bchusdt (open, close, high, low, timestamp, symbol, pricechange, prisechangepercentage, weightedaverageprice, totaltradebaseasset) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            val = (
                                open,
                                close,
                                high,
                                low,
                                timestamp,
                                symbol,
                                pricechange,
                                pricechangepercentage,
                                weightedaverageprice,
                                totaltradebaseasset,
                            )
                            curs.execute(sql, val)
                            db.commit()
                            # old record delete
                            # curs.execute(
                            #     "SELECT * FROM efx_percentage_change_bchusdt")
                            # myresult = curs.fetchall()
                            # numberva = len(myresult)
                            # if numberva > 300:
                            #     sql = "DELETE FROM efx_percentage_change_bchusdt ORDER BY id DESC limit %s"
                            #     val = (100,)
                            #     curs.execute(sql, val)
                            #     db.commit()

                        if value["s"] == "BNBUSDT":
                            # /////// get value \\\\\\\\\\\\\
                            open = value["o"]
                            high = value["h"]
                            low = value["l"]
                            close = value["c"]
                            # volume = input_dict[""]
                            timestamp = value["E"]
                            symbol = value["s"]
                            pricechange = value["p"]
                            pricechangepercentage = value["P"]
                            weightedaverageprice = value["w"]
                            totaltradebaseasset = value["v"]

                            sql = "INSERT INTO 	efx_percentage_change_bnbusdt (open, close, high, low, timestamp, symbol, pricechange, prisechangepercentage, weightedaverageprice, totaltradebaseasset) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            val = (
                                open,
                                close,
                                high,
                                low,
                                timestamp,
                                symbol,
                                pricechange,
                                pricechangepercentage,
                                weightedaverageprice,
                                totaltradebaseasset,
                            )
                            curs.execute(sql, val)
                            db.commit()
                            # old record delete
                            # curs.execute(
                            #     "SELECT * FROM efx_percentage_change_bnbusdt")
                            # myresult = curs.fetchall()
                            # numberva = len(myresult)
                            # if numberva > 300:
                            #     sql = "DELETE FROM efx_percentage_change_bnbusdt ORDER BY id DESC limit %s"
                            #     val = (100,)
                            #     curs.execute(sql, val)
                            #     db.commit()

                        if value["s"] == "BTCUSDT":
                            # /////// get value \\\\\\\\\\\\\
                            open = value["o"]
                            high = value["h"]
                            low = value["l"]
                            close = value["c"]
                            # volume = input_dict[""]
                            timestamp = value["E"]
                            symbol = value["s"]
                            pricechange = value["p"]
                            pricechangepercentage = value["P"]
                            weightedaverageprice = value["w"]
                            totaltradebaseasset = value["v"]

                            sql = "INSERT INTO 	efx_percentage_change_btcusdt (open, close, high, low, timestamp, symbol, pricechange, prisechangepercentage, weightedaverageprice, totaltradebaseasset) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            val = (
                                open,
                                close,
                                high,
                                low,
                                timestamp,
                                symbol,
                                pricechange,
                                pricechangepercentage,
                                weightedaverageprice,
                                totaltradebaseasset,
                            )
                            curs.execute(sql, val)
                            db.commit()
                            # old record delete
                            # curs.execute(
                            #     "SELECT * FROM efx_percentage_change_btcusdt")
                            # myresult = curs.fetchall()
                            # numberva = len(myresult)
                            # if numberva > 300:
                            #     sql = "DELETE FROM efx_percentage_change_btcusdt ORDER BY id DESC limit %s"
                            #     val = (100,)
                            #     curs.execute(sql, val)
                            #     db.commit()

                        if value["s"] == "CAKEUSDT":
                            # /////// get value \\\\\\\\\\\\\
                            open = value["o"]
                            high = value["h"]
                            low = value["l"]
                            close = value["c"]
                            # volume = input_dict[""]
                            timestamp = value["E"]
                            symbol = value["s"]
                            pricechange = value["p"]
                            pricechangepercentage = value["P"]
                            weightedaverageprice = value["w"]
                            totaltradebaseasset = value["v"]

                            sql = "INSERT INTO 	efx_percentage_change_cakeusdt (open, close, high, low, timestamp, symbol, pricechange, prisechangepercentage, weightedaverageprice, totaltradebaseasset) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            val = (
                                open,
                                close,
                                high,
                                low,
                                timestamp,
                                symbol,
                                pricechange,
                                pricechangepercentage,
                                weightedaverageprice,
                                totaltradebaseasset,
                            )
                            curs.execute(sql, val)
                            db.commit()
                            # old record delete
                            # curs.execute(
                            #     "SELECT * FROM efx_percentage_change_cakeusdt")
                            # myresult = curs.fetchall()
                            # numberva = len(myresult)
                            # if numberva > 300:
                            #     sql = "DELETE FROM efx_percentage_change_cakeusdt ORDER BY id DESC limit %s"
                            #     val = (100,)
                            #     curs.execute(sql, val)
                            #     db.commit()

                        if value["s"] == "COMPUSDT":
                            # /////// get value \\\\\\\\\\\\\
                            open = value["o"]
                            high = value["h"]
                            low = value["l"]
                            close = value["c"]
                            # volume = input_dict[""]
                            timestamp = value["E"]
                            symbol = value["s"]
                            pricechange = value["p"]
                            pricechangepercentage = value["P"]
                            weightedaverageprice = value["w"]
                            totaltradebaseasset = value["v"]

                            sql = "INSERT INTO 	efx_percentage_change_compusdt (open, close, high, low, timestamp, symbol, pricechange, prisechangepercentage, weightedaverageprice, totaltradebaseasset) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            val = (
                                open,
                                close,
                                high,
                                low,
                                timestamp,
                                symbol,
                                pricechange,
                                pricechangepercentage,
                                weightedaverageprice,
                                totaltradebaseasset,
                            )
                            curs.execute(sql, val)
                            db.commit()
                            # old record delete
                            # curs.execute(
                            #     "SELECT * FROM efx_percentage_change_compusdt")
                            # myresult = curs.fetchall()
                            # numberva = len(myresult)
                            # if numberva > 300:
                            #     sql = "DELETE FROM efx_percentage_change_compusdt ORDER BY id DESC limit %s"
                            #     val = (100,)
                            #     curs.execute(sql, val)
                            #     db.commit()

                        if value["s"] == "COSUSDT":
                            # /////// get value \\\\\\\\\\\\\
                            open = value["o"]
                            high = value["h"]
                            low = value["l"]
                            close = value["c"]
                            # volume = input_dict[""]
                            timestamp = value["E"]
                            symbol = value["s"]
                            pricechange = value["p"]
                            pricechangepercentage = value["P"]
                            weightedaverageprice = value["w"]
                            totaltradebaseasset = value["v"]

                            sql = "INSERT INTO 	efx_percentage_change_cosusdt (open, close, high, low, timestamp, symbol, pricechange, prisechangepercentage, weightedaverageprice, totaltradebaseasset) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            val = (
                                open,
                                close,
                                high,
                                low,
                                timestamp,
                                symbol,
                                pricechange,
                                pricechangepercentage,
                                weightedaverageprice,
                                totaltradebaseasset,
                            )
                            curs.execute(sql, val)
                            db.commit()
                            # old record delete
                            # curs.execute(
                            #     "SELECT * FROM efx_percentage_change_cosusdt")
                            # myresult = curs.fetchall()
                            # numberva = len(myresult)
                            # if numberva > 300:
                            #     sql = "DELETE FROM efx_percentage_change_cosusdt ORDER BY id DESC limit %s"
                            #     val = (100,)
                            #     curs.execute(sql, val)
                            #     db.commit()

                        if value["s"] == "CRVUSDT":
                            # /////// get value \\\\\\\\\\\\\
                            open = value["o"]
                            high = value["h"]
                            low = value["l"]
                            close = value["c"]
                            # volume = input_dict[""]
                            timestamp = value["E"]
                            symbol = value["s"]
                            pricechange = value["p"]
                            pricechangepercentage = value["P"]
                            weightedaverageprice = value["w"]
                            totaltradebaseasset = value["v"]

                            sql = "INSERT INTO 	efx_percentage_change_crvusdt (open, close, high, low, timestamp, symbol, pricechange, prisechangepercentage, weightedaverageprice, totaltradebaseasset) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            val = (
                                open,
                                close,
                                high,
                                low,
                                timestamp,
                                symbol,
                                pricechange,
                                pricechangepercentage,
                                weightedaverageprice,
                                totaltradebaseasset,
                            )
                            curs.execute(sql, val)
                            db.commit()
                            # old record delete
                            # curs.execute(
                            #     "SELECT * FROM efx_percentage_change_crvusdt")
                            # myresult = curs.fetchall()
                            # numberva = len(myresult)
                            # if numberva > 300:
                            #     sql = "DELETE FROM efx_percentage_change_crvusdt ORDER BY id DESC limit %s"
                            #     val = (100,)
                            #     curs.execute(sql, val)
                            #     db.commit()

                        if value["s"] == "DASHUSDT":
                            # /////// get value \\\\\\\\\\\\\
                            open = value["o"]
                            high = value["h"]
                            low = value["l"]
                            close = value["c"]
                            # volume = input_dict[""]
                            timestamp = value["E"]
                            symbol = value["s"]
                            pricechange = value["p"]
                            pricechangepercentage = value["P"]
                            weightedaverageprice = value["w"]
                            totaltradebaseasset = value["v"]

                            sql = "INSERT INTO 	efx_percentage_change_dashusdt (open, close, high, low, timestamp, symbol, pricechange, prisechangepercentage, weightedaverageprice, totaltradebaseasset) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            val = (
                                open,
                                close,
                                high,
                                low,
                                timestamp,
                                symbol,
                                pricechange,
                                pricechangepercentage,
                                weightedaverageprice,
                                totaltradebaseasset,
                            )
                            curs.execute(sql, val)
                            db.commit()
                            # old record delete
                            # curs.execute(
                            #     "SELECT * FROM efx_percentage_change_dashusdt")
                            # myresult = curs.fetchall()
                            # numberva = len(myresult)
                            # if numberva > 300:
                            #     sql = "DELETE FROM efx_percentage_change_dashusdt ORDER BY id DESC limit %s"
                            #     val = (100,)
                            #     curs.execute(sql, val)
                            #     db.commit()

                        if value["s"] == "DOGEUSDT":
                            # /////// get value \\\\\\\\\\\\\
                            open = value["o"]
                            high = value["h"]
                            low = value["l"]
                            close = value["c"]
                            # volume = input_dict[""]
                            timestamp = value["E"]
                            symbol = value["s"]
                            pricechange = value["p"]
                            pricechangepercentage = value["P"]
                            weightedaverageprice = value["w"]
                            totaltradebaseasset = value["v"]

                            sql = "INSERT INTO 	efx_percentage_change_dogeusdt (open, close, high, low, timestamp, symbol, pricechange, prisechangepercentage, weightedaverageprice, totaltradebaseasset) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            val = (
                                open,
                                close,
                                high,
                                low,
                                timestamp,
                                symbol,
                                pricechange,
                                pricechangepercentage,
                                weightedaverageprice,
                                totaltradebaseasset,
                            )
                            curs.execute(sql, val)
                            db.commit()
                            # old record delete
                            curs.execute("SELECT * FROM efx_percentage_change_dogeusdt")
                            myresult = curs.fetchall()
                            numberva = len(myresult)
                            if numberva > 300:
                                sql = "DELETE FROM efx_percentage_change_dogeusdt ORDER BY id DESC limit %s"
                                val = (100,)
                                curs.execute(sql, val)
                                db.commit()

                        if value["s"] == "DOTUSDT":
                            # /////// get value \\\\\\\\\\\\\
                            open = value["o"]
                            high = value["h"]
                            low = value["l"]
                            close = value["c"]
                            # volume = input_dict[""]
                            timestamp = value["E"]
                            symbol = value["s"]
                            pricechange = value["p"]
                            pricechangepercentage = value["P"]
                            weightedaverageprice = value["w"]
                            totaltradebaseasset = value["v"]

                            sql = "INSERT INTO 	efx_percentage_change_dotusdt (open, close, high, low, timestamp, symbol, pricechange, prisechangepercentage, weightedaverageprice, totaltradebaseasset) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            val = (
                                open,
                                close,
                                high,
                                low,
                                timestamp,
                                symbol,
                                pricechange,
                                pricechangepercentage,
                                weightedaverageprice,
                                totaltradebaseasset,
                            )
                            curs.execute(sql, val)
                            db.commit()
                            # old record delete
                            # curs.execute(
                            #     "SELECT * FROM efx_percentage_change_dotusdt")
                            # myresult = curs.fetchall()
                            # numberva = len(myresult)
                            # if numberva > 300:
                            #     sql = "DELETE FROM efx_percentage_change_dotusdt ORDER BY id DESC limit %s"
                            #     val = (100,)
                            #     curs.execute(sql, val)
                            #     db.commit()

                        if value["s"] == "EOSUSDT":
                            # /////// get value \\\\\\\\\\\\\
                            open = value["o"]
                            high = value["h"]
                            low = value["l"]
                            close = value["c"]
                            # volume = input_dict[""]
                            timestamp = value["E"]
                            symbol = value["s"]
                            pricechange = value["p"]
                            pricechangepercentage = value["P"]
                            weightedaverageprice = value["w"]
                            totaltradebaseasset = value["v"]

                            sql = "INSERT INTO 	efx_percentage_change_eosusdt (open, close, high, low, timestamp, symbol, pricechange, prisechangepercentage, weightedaverageprice, totaltradebaseasset) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            val = (
                                open,
                                close,
                                high,
                                low,
                                timestamp,
                                symbol,
                                pricechange,
                                pricechangepercentage,
                                weightedaverageprice,
                                totaltradebaseasset,
                            )
                            curs.execute(sql, val)
                            db.commit()
                            # old record delete
                            # curs.execute(
                            #     "SELECT * FROM efx_percentage_change_eosusdt")
                            # myresult = curs.fetchall()
                            # numberva = len(myresult)
                            # if numberva > 300:
                            #     sql = "DELETE FROM efx_percentage_change_eosusdt ORDER BY id DESC limit %s"
                            #     val = (100,)
                            #     curs.execute(sql, val)
                            #     db.commit()

                        if value["s"] == "ETHUSDT":
                            # /////// get value \\\\\\\\\\\\\
                            open = value["o"]
                            high = value["h"]
                            low = value["l"]
                            close = value["c"]
                            # volume = input_dict[""]
                            timestamp = value["E"]
                            symbol = value["s"]
                            pricechange = value["p"]
                            pricechangepercentage = value["P"]
                            weightedaverageprice = value["w"]
                            totaltradebaseasset = value["v"]

                            sql = "INSERT INTO 	efx_percentage_change_ethusdt (open, close, high, low, timestamp, symbol, pricechange, prisechangepercentage, weightedaverageprice, totaltradebaseasset) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            val = (
                                open,
                                close,
                                high,
                                low,
                                timestamp,
                                symbol,
                                pricechange,
                                pricechangepercentage,
                                weightedaverageprice,
                                totaltradebaseasset,
                            )
                            curs.execute(sql, val)
                            db.commit()
                            # # old record delete
                            # curs.execute(
                            #     "SELECT * FROM efx_percentage_change_ethusdt")
                            # myresult = curs.fetchall()
                            # numberva = len(myresult)
                            # if numberva > 300:
                            #     sql = "DELETE FROM efx_percentage_change_ethusdt ORDER BY id DESC limit %s"
                            #     val = (100,)
                            #     curs.execute(sql, val)
                            #     db.commit()

                        if value["s"] == "ETCUSDT":
                            # /////// get value \\\\\\\\\\\\\
                            open = value["o"]
                            high = value["h"]
                            low = value["l"]
                            close = value["c"]
                            # volume = input_dict[""]
                            timestamp = value["E"]
                            symbol = value["s"]
                            pricechange = value["p"]
                            pricechangepercentage = value["P"]
                            weightedaverageprice = value["w"]
                            totaltradebaseasset = value["v"]

                            sql = "INSERT INTO 	efx_percentage_change_etcusdt (open, close, high, low, timestamp, symbol, pricechange, prisechangepercentage, weightedaverageprice, totaltradebaseasset) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            val = (
                                open,
                                close,
                                high,
                                low,
                                timestamp,
                                symbol,
                                pricechange,
                                pricechangepercentage,
                                weightedaverageprice,
                                totaltradebaseasset,
                            )
                            curs.execute(sql, val)
                            db.commit()
                            # # old record delete
                            # curs.execute(
                            #     "SELECT * FROM efx_percentage_change_etcusdt")
                            # myresult = curs.fetchall()
                            # numberva = len(myresult)
                            # if numberva > 300:
                            #     sql = "DELETE FROM efx_percentage_change_etcusdt ORDER BY id DESC limit %s"
                            #     val = (100,)
                            #     curs.execute(sql, val)
                            #     db.commit()

                        if value["s"] == "FILUSDT":
                            # /////// get value \\\\\\\\\\\\\
                            open = value["o"]
                            high = value["h"]
                            low = value["l"]
                            close = value["c"]
                            # volume = input_dict[""]
                            timestamp = value["E"]
                            symbol = value["s"]
                            pricechange = value["p"]
                            pricechangepercentage = value["P"]
                            weightedaverageprice = value["w"]
                            totaltradebaseasset = value["v"]

                            sql = "INSERT INTO 	efx_percentage_change_filusdt (open, close, high, low, timestamp, symbol, pricechange, prisechangepercentage, weightedaverageprice, totaltradebaseasset) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            val = (
                                open,
                                close,
                                high,
                                low,
                                timestamp,
                                symbol,
                                pricechange,
                                pricechangepercentage,
                                weightedaverageprice,
                                totaltradebaseasset,
                            )
                            curs.execute(sql, val)
                            db.commit()
                            # old record delete
                            # curs.execute(
                            #     "SELECT * FROM efx_percentage_change_filusdt")
                            # myresult = curs.fetchall()
                            # numberva = len(myresult)
                            # if numberva > 300:
                            #     sql = "DELETE FROM efx_percentage_change_filusdt ORDER BY id DESC limit %s"
                            #     val = (100,)
                            #     curs.execute(sql, val)
                            #     db.commit()

                        if value["s"] == "FTTUSDT":
                            # /////// get value \\\\\\\\\\\\\
                            open = value["o"]
                            high = value["h"]
                            low = value["l"]
                            close = value["c"]
                            # volume = input_dict[""]
                            timestamp = value["E"]
                            symbol = value["s"]
                            pricechange = value["p"]
                            pricechangepercentage = value["P"]
                            weightedaverageprice = value["w"]
                            totaltradebaseasset = value["v"]

                            sql = "INSERT INTO 	efx_percentage_change_fttusdt (open, close, high, low, timestamp, symbol, pricechange, prisechangepercentage, weightedaverageprice, totaltradebaseasset) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            val = (
                                open,
                                close,
                                high,
                                low,
                                timestamp,
                                symbol,
                                pricechange,
                                pricechangepercentage,
                                weightedaverageprice,
                                totaltradebaseasset,
                            )
                            curs.execute(sql, val)
                            db.commit()
                            # old record delete
                            # curs.execute(
                            #     "SELECT * FROM efx_percentage_change_fttusdt")
                            # myresult = curs.fetchall()
                            # numberva = len(myresult)
                            # if numberva > 300:
                            #     sql = "DELETE FROM efx_percentage_change_fttusdt ORDER BY id DESC limit %s"
                            #     val = (100,)
                            #     curs.execute(sql, val)
                            #     db.commit()

                        if value["s"] == "GRTUSDT":
                            # /////// get value \\\\\\\\\\\\\
                            open = value["o"]
                            high = value["h"]
                            low = value["l"]
                            close = value["c"]
                            # volume = input_dict[""]
                            timestamp = value["E"]
                            symbol = value["s"]
                            pricechange = value["p"]
                            pricechangepercentage = value["P"]
                            weightedaverageprice = value["w"]
                            totaltradebaseasset = value["v"]

                            sql = "INSERT INTO 	efx_percentage_change_grtusdt (open, close, high, low, timestamp, symbol, pricechange, prisechangepercentage, weightedaverageprice, totaltradebaseasset) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            val = (
                                open,
                                close,
                                high,
                                low,
                                timestamp,
                                symbol,
                                pricechange,
                                pricechangepercentage,
                                weightedaverageprice,
                                totaltradebaseasset,
                            )
                            curs.execute(sql, val)
                            db.commit()
                            # old record delete
                            # curs.execute(
                            #     "SELECT * FROM efx_percentage_change_grtusdt")
                            # myresult = curs.fetchall()
                            # numberva = len(myresult)
                            # if numberva > 300:
                            #     sql = "DELETE FROM efx_percentage_change_grtusdt ORDER BY id DESC limit %s"
                            #     val = (100,)
                            #     curs.execute(sql, val)
                            #     db.commit()

                        if value["s"] == "IOSTUSDT":
                            # /////// get value \\\\\\\\\\\\\
                            open = value["o"]
                            high = value["h"]
                            low = value["l"]
                            close = value["c"]
                            # volume = input_dict[""]
                            timestamp = value["E"]
                            symbol = value["s"]
                            pricechange = value["p"]
                            pricechangepercentage = value["P"]
                            weightedaverageprice = value["w"]
                            totaltradebaseasset = value["v"]

                            sql = "INSERT INTO 	efx_percentage_change_iostusdt (open, close, high, low, timestamp, symbol, pricechange, prisechangepercentage, weightedaverageprice, totaltradebaseasset) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            val = (
                                open,
                                close,
                                high,
                                low,
                                timestamp,
                                symbol,
                                pricechange,
                                pricechangepercentage,
                                weightedaverageprice,
                                totaltradebaseasset,
                            )
                            curs.execute(sql, val)
                            db.commit()
                            # old record delete
                            # curs.execute(
                            #     "SELECT * FROM efx_percentage_change_iostusdt")
                            # myresult = curs.fetchall()
                            # numberva = len(myresult)
                            # if numberva > 300:
                            #     sql = "DELETE FROM efx_percentage_change_iostusdt ORDER BY id DESC limit %s"
                            #     val = (100,)
                            #     curs.execute(sql, val)
                            #     db.commit()

                        if value["s"] == "IOTAUSDT":
                            # /////// get value \\\\\\\\\\\\\
                            open = value["o"]
                            high = value["h"]
                            low = value["l"]
                            close = value["c"]
                            # volume = input_dict[""]
                            timestamp = value["E"]
                            symbol = value["s"]
                            pricechange = value["p"]
                            pricechangepercentage = value["P"]
                            weightedaverageprice = value["w"]
                            totaltradebaseasset = value["v"]

                            sql = "INSERT INTO 	efx_percentage_change_iotausdt (open, close, high, low, timestamp, symbol, pricechange, prisechangepercentage, weightedaverageprice, totaltradebaseasset) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            val = (
                                open,
                                close,
                                high,
                                low,
                                timestamp,
                                symbol,
                                pricechange,
                                pricechangepercentage,
                                weightedaverageprice,
                                totaltradebaseasset,
                            )
                            curs.execute(sql, val)
                            db.commit()
                            # old record delete
                            # curs.execute(
                            #     "SELECT * FROM efx_percentage_change_iotausdt")
                            # myresult = curs.fetchall()
                            # numberva = len(myresult)
                            # if numberva > 300:
                            #     sql = "DELETE FROM efx_percentage_change_iotausdt ORDER BY id DESC limit %s"
                            #     val = (100,)
                            #     curs.execute(sql, val)
                            #     db.commit()

                        if value["s"] == "JSTUSDT":
                            # /////// get value \\\\\\\\\\\\\
                            open = value["o"]
                            high = value["h"]
                            low = value["l"]
                            close = value["c"]
                            # volume = input_dict[""]
                            timestamp = value["E"]
                            symbol = value["s"]
                            pricechange = value["p"]
                            pricechangepercentage = value["P"]
                            weightedaverageprice = value["w"]
                            totaltradebaseasset = value["v"]

                            sql = "INSERT INTO 	efx_percentage_change_jstusdt (open, close, high, low, timestamp, symbol, pricechange, prisechangepercentage, weightedaverageprice, totaltradebaseasset) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            val = (
                                open,
                                close,
                                high,
                                low,
                                timestamp,
                                symbol,
                                pricechange,
                                pricechangepercentage,
                                weightedaverageprice,
                                totaltradebaseasset,
                            )
                            curs.execute(sql, val)
                            db.commit()
                            # old record delete
                            # curs.execute(
                            #     "SELECT * FROM efx_percentage_change_jstusdt")
                            # myresult = curs.fetchall()
                            # numberva = len(myresult)
                            # if numberva > 300:
                            #     sql = "DELETE FROM efx_percentage_change_jstusdt ORDER BY id DESC limit %s"
                            #     val = (100,)
                            #     curs.execute(sql, val)
                            #     db.commit()

                        if value["s"] == "KAVAUSDT":
                            # /////// get value \\\\\\\\\\\\\
                            open = value["o"]
                            high = value["h"]
                            low = value["l"]
                            close = value["c"]
                            # volume = input_dict[""]
                            timestamp = value["E"]
                            symbol = value["s"]
                            pricechange = value["p"]
                            pricechangepercentage = value["P"]
                            weightedaverageprice = value["w"]
                            totaltradebaseasset = value["v"]

                            sql = "INSERT INTO 	efx_percentage_change_kavausdt (open, close, high, low, timestamp, symbol, pricechange, prisechangepercentage, weightedaverageprice, totaltradebaseasset) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            val = (
                                open,
                                close,
                                high,
                                low,
                                timestamp,
                                symbol,
                                pricechange,
                                pricechangepercentage,
                                weightedaverageprice,
                                totaltradebaseasset,
                            )
                            curs.execute(sql, val)
                            db.commit()
                            # old record delete
                            # curs.execute(
                            #     "SELECT * FROM efx_percentage_change_kavausdt")
                            # myresult = curs.fetchall()
                            # numberva = len(myresult)
                            # if numberva > 300:
                            #     sql = "DELETE FROM efx_percentage_change_kavausdt ORDER BY id DESC limit %s"
                            #     val = (100,)
                            #     curs.execute(sql, val)
                            #     db.commit()

                        if value["s"] == "LINKUSDT":
                            # /////// get value \\\\\\\\\\\\\
                            open = value["o"]
                            high = value["h"]
                            low = value["l"]
                            close = value["c"]
                            # volume = input_dict[""]
                            timestamp = value["E"]
                            symbol = value["s"]
                            pricechange = value["p"]
                            pricechangepercentage = value["P"]
                            weightedaverageprice = value["w"]
                            totaltradebaseasset = value["v"]

                            sql = "INSERT INTO 	efx_percentage_change_linkusdt (open, close, high, low, timestamp, symbol, pricechange, prisechangepercentage, weightedaverageprice, totaltradebaseasset) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            val = (
                                open,
                                close,
                                high,
                                low,
                                timestamp,
                                symbol,
                                pricechange,
                                pricechangepercentage,
                                weightedaverageprice,
                                totaltradebaseasset,
                            )
                            curs.execute(sql, val)
                            db.commit()
                            # old record delete
                            # curs.execute(
                            #     "SELECT * FROM efx_percentage_change_linkusdt")
                            # myresult = curs.fetchall()
                            # numberva = len(myresult)
                            # if numberva > 300:
                            #     sql = "DELETE FROM efx_percentage_change_linkusdt ORDER BY id DESC limit %s"
                            #     val = (100,)
                            #     curs.execute(sql, val)
                            #     db.commit()

                        if value["s"] == "LITUSDT":
                            # /////// get value \\\\\\\\\\\\\
                            open = value["o"]
                            high = value["h"]
                            low = value["l"]
                            close = value["c"]
                            # volume = input_dict[""]
                            timestamp = value["E"]
                            symbol = value["s"]
                            pricechange = value["p"]
                            pricechangepercentage = value["P"]
                            weightedaverageprice = value["w"]
                            totaltradebaseasset = value["v"]

                            sql = "INSERT INTO 	efx_percentage_change_litusdt (open, close, high, low, timestamp, symbol, pricechange, prisechangepercentage, weightedaverageprice, totaltradebaseasset) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            val = (
                                open,
                                close,
                                high,
                                low,
                                timestamp,
                                symbol,
                                pricechange,
                                pricechangepercentage,
                                weightedaverageprice,
                                totaltradebaseasset,
                            )
                            curs.execute(sql, val)
                            db.commit()
                            # old record delete
                            # curs.execute(
                            #     "SELECT * FROM efx_percentage_change_litusdt")
                            # myresult = curs.fetchall()
                            # numberva = len(myresult)
                            # if numberva > 300:
                            #     sql = "DELETE FROM efx_percentage_change_litusdt ORDER BY id DESC limit %s"
                            #     val = (100,)
                            #     curs.execute(sql, val)
                            #     db.commit()

                        if value["s"] == "LTCUSDT":
                            # /////// get value \\\\\\\\\\\\\
                            open = value["o"]
                            high = value["h"]
                            low = value["l"]
                            close = value["c"]
                            # volume = input_dict[""]
                            timestamp = value["E"]
                            symbol = value["s"]
                            pricechange = value["p"]
                            pricechangepercentage = value["P"]
                            weightedaverageprice = value["w"]
                            totaltradebaseasset = value["v"]

                            sql = "INSERT INTO 	efx_percentage_change_ltcusdt (open, close, high, low, timestamp, symbol, pricechange, prisechangepercentage, weightedaverageprice, totaltradebaseasset) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            val = (
                                open,
                                close,
                                high,
                                low,
                                timestamp,
                                symbol,
                                pricechange,
                                pricechangepercentage,
                                weightedaverageprice,
                                totaltradebaseasset,
                            )
                            curs.execute(sql, val)
                            db.commit()
                            # old record delete
                            # curs.execute(
                            #     "SELECT * FROM efx_percentage_change_ltcusdt")
                            # myresult = curs.fetchall()
                            # numberva = len(myresult)
                            # if numberva > 300:
                            #     sql = "DELETE FROM efx_percentage_change_ltcusdt ORDER BY id DESC limit %s"
                            #     val = (100,)
                            #     curs.execute(sql, val)
                            #     db.commit()

                        if value["s"] == "MANAUSDT":
                            # /////// get value \\\\\\\\\\\\\
                            open = value["o"]
                            high = value["h"]
                            low = value["l"]
                            close = value["c"]
                            # volume = input_dict[""]
                            timestamp = value["E"]
                            symbol = value["s"]
                            pricechange = value["p"]
                            pricechangepercentage = value["P"]
                            weightedaverageprice = value["w"]
                            totaltradebaseasset = value["v"]

                            sql = "INSERT INTO 	efx_percentage_change_manausdt (open, close, high, low, timestamp, symbol, pricechange, prisechangepercentage, weightedaverageprice, totaltradebaseasset) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            val = (
                                open,
                                close,
                                high,
                                low,
                                timestamp,
                                symbol,
                                pricechange,
                                pricechangepercentage,
                                weightedaverageprice,
                                totaltradebaseasset,
                            )
                            curs.execute(sql, val)
                            db.commit()
                            # old record delete
                            # curs.execute(
                            #     "SELECT * FROM efx_percentage_change_manausdt")
                            # myresult = curs.fetchall()
                            # numberva = len(myresult)
                            # if numberva > 300:
                            #     sql = "DELETE FROM efx_percentage_change_manausdt ORDER BY id DESC limit %s"
                            #     val = (100,)
                            #     curs.execute(sql, val)
                            #     db.commit()

                        if value["s"] == "MDXUSDT":
                            # /////// get value \\\\\\\\\\\\\
                            open = value["o"]
                            high = value["h"]
                            low = value["l"]
                            close = value["c"]
                            # volume = input_dict[""]
                            timestamp = value["E"]
                            symbol = value["s"]
                            pricechange = value["p"]
                            pricechangepercentage = value["P"]
                            weightedaverageprice = value["w"]
                            totaltradebaseasset = value["v"]

                            sql = "INSERT INTO 	efx_percentage_change_mdxusdt (open, close, high, low, timestamp, symbol, pricechange, prisechangepercentage, weightedaverageprice, totaltradebaseasset) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            val = (
                                open,
                                close,
                                high,
                                low,
                                timestamp,
                                symbol,
                                pricechange,
                                pricechangepercentage,
                                weightedaverageprice,
                                totaltradebaseasset,
                            )
                            curs.execute(sql, val)
                            db.commit()
                            # old record delete
                            # curs.execute(
                            #     "SELECT * FROM efx_percentage_change_mdxusdt")
                            # myresult = curs.fetchall()
                            # numberva = len(myresult)
                            # if numberva > 300:
                            #     sql = "DELETE FROM efx_percentage_change_mdxusdt ORDER BY id DESC limit %s"
                            #     val = (100,)
                            #     curs.execute(sql, val)
                            #     db.commit()

                        if value["s"] == "NEOUSDT":
                            # /////// get value \\\\\\\\\\\\\
                            open = value["o"]
                            high = value["h"]
                            low = value["l"]
                            close = value["c"]
                            # volume = input_dict[""]
                            timestamp = value["E"]
                            symbol = value["s"]
                            pricechange = value["p"]
                            pricechangepercentage = value["P"]
                            weightedaverageprice = value["w"]
                            totaltradebaseasset = value["v"]

                            sql = "INSERT INTO 	efx_percentage_change_neousdt (open, close, high, low, timestamp, symbol, pricechange, prisechangepercentage, weightedaverageprice, totaltradebaseasset) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            val = (
                                open,
                                close,
                                high,
                                low,
                                timestamp,
                                symbol,
                                pricechange,
                                pricechangepercentage,
                                weightedaverageprice,
                                totaltradebaseasset,
                            )
                            curs.execute(sql, val)
                            db.commit()
                            # old record delete
                            # curs.execute(
                            #     "SELECT * FROM efx_percentage_change_neousdt")
                            # myresult = curs.fetchall()
                            # numberva = len(myresult)
                            # if numberva > 300:
                            #     sql = "DELETE FROM efx_percentage_change_neousdt ORDER BY id DESC limit %s"
                            #     val = (100,)
                            #     curs.execute(sql, val)
                            #     db.commit()

                        if value["s"] == "OMGUSDT":
                            # /////// get value \\\\\\\\\\\\\
                            open = value["o"]
                            high = value["h"]
                            low = value["l"]
                            close = value["c"]
                            # volume = input_dict[""]
                            timestamp = value["E"]
                            symbol = value["s"]
                            pricechange = value["p"]
                            pricechangepercentage = value["P"]
                            weightedaverageprice = value["w"]
                            totaltradebaseasset = value["v"]

                            sql = "INSERT INTO 	efx_percentage_change_omgusdt (open, close, high, low, timestamp, symbol, pricechange, prisechangepercentage, weightedaverageprice, totaltradebaseasset) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            val = (
                                open,
                                close,
                                high,
                                low,
                                timestamp,
                                symbol,
                                pricechange,
                                pricechangepercentage,
                                weightedaverageprice,
                                totaltradebaseasset,
                            )
                            curs.execute(sql, val)
                            db.commit()
                            # old record delete
                            # curs.execute(
                            #     "SELECT * FROM efx_percentage_change_omgusdt")
                            # myresult = curs.fetchall()
                            # numberva = len(myresult)
                            # if numberva > 300:
                            #     sql = "DELETE FROM efx_percentage_change_omgusdt ORDER BY id DESC limit %s"
                            #     val = (100,)
                            #     curs.execute(sql, val)
                            #     db.commit()

                        if value["s"] == "SUSHIUSDT":
                            # /////// get value \\\\\\\\\\\\\
                            open = value["o"]
                            high = value["h"]
                            low = value["l"]
                            close = value["c"]
                            # volume = input_dict[""]
                            timestamp = value["E"]
                            symbol = value["s"]
                            pricechange = value["p"]
                            pricechangepercentage = value["P"]
                            weightedaverageprice = value["w"]
                            totaltradebaseasset = value["v"]

                            sql = "INSERT INTO 	efx_percentage_change_sushiusdt (open, close, high, low, timestamp, symbol, pricechange, prisechangepercentage, weightedaverageprice, totaltradebaseasset) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            val = (
                                open,
                                close,
                                high,
                                low,
                                timestamp,
                                symbol,
                                pricechange,
                                pricechangepercentage,
                                weightedaverageprice,
                                totaltradebaseasset,
                            )
                            curs.execute(sql, val)
                            db.commit()
                            # old record delete
                            # curs.execute(
                            #     "SELECT * FROM efx_percentage_change_sushiusdt")
                            # myresult = curs.fetchall()
                            # numberva = len(myresult)
                            # if numberva > 300:
                            #     sql = "DELETE FROM efx_percentage_change_sushiusdt ORDER BY id DESC limit %s"
                            #     val = (100,)
                            #     curs.execute(sql, val)
                            #     db.commit()

                        if value["s"] == "THETAUSDT":
                            # /////// get value \\\\\\\\\\\\\
                            open = value["o"]
                            high = value["h"]
                            low = value["l"]
                            close = value["c"]
                            # volume = input_dict[""]
                            timestamp = value["E"]
                            symbol = value["s"]
                            pricechange = value["p"]
                            pricechangepercentage = value["P"]
                            weightedaverageprice = value["w"]
                            totaltradebaseasset = value["v"]

                            sql = "INSERT INTO 	efx_percentage_change_thetausdt (open, close, high, low, timestamp, symbol, pricechange, prisechangepercentage, weightedaverageprice, totaltradebaseasset) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            val = (
                                open,
                                close,
                                high,
                                low,
                                timestamp,
                                symbol,
                                pricechange,
                                pricechangepercentage,
                                weightedaverageprice,
                                totaltradebaseasset,
                            )
                            curs.execute(sql, val)
                            db.commit()
                            # old record delete
                            # curs.execute(
                            #     "SELECT * FROM efx_percentage_change_thetausdt")
                            # myresult = curs.fetchall()
                            # numberva = len(myresult)
                            # if numberva > 300:
                            #     sql = "DELETE FROM efx_percentage_change_thetausdt ORDER BY id DESC limit %s"
                            #     val = (100,)
                            #     curs.execute(sql, val)
                            #     db.commit()

                        if value["s"] == "TRXUSDT":
                            # /////// get value \\\\\\\\\\\\\
                            open = value["o"]
                            high = value["h"]
                            low = value["l"]
                            close = value["c"]
                            # volume = input_dict[""]
                            timestamp = value["E"]
                            symbol = value["s"]
                            pricechange = value["p"]
                            pricechangepercentage = value["P"]
                            weightedaverageprice = value["w"]
                            totaltradebaseasset = value["v"]

                            sql = "INSERT INTO 	efx_percentage_change_trxusdt (open, close, high, low, timestamp, symbol, pricechange, prisechangepercentage, weightedaverageprice, totaltradebaseasset) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            val = (
                                open,
                                close,
                                high,
                                low,
                                timestamp,
                                symbol,
                                pricechange,
                                pricechangepercentage,
                                weightedaverageprice,
                                totaltradebaseasset,
                            )
                            curs.execute(sql, val)
                            db.commit()
                            # old record delete
                            # curs.execute(
                            #     "SELECT * FROM efx_percentage_change_trxusdt")
                            # myresult = curs.fetchall()
                            # numberva = len(myresult)
                            # if numberva > 300:
                            #     sql = "DELETE FROM efx_percentage_change_trxusdt ORDER BY id DESC limit %s"
                            #     val = (100,)
                            #     curs.execute(sql, val)
                            #     db.commit()

                        if value["s"] == "UNIUSDT":
                            # /////// get value \\\\\\\\\\\\\
                            open = value["o"]
                            high = value["h"]
                            low = value["l"]
                            close = value["c"]
                            # volume = input_dict[""]
                            timestamp = value["E"]
                            symbol = value["s"]
                            pricechange = value["p"]
                            pricechangepercentage = value["P"]
                            weightedaverageprice = value["w"]
                            totaltradebaseasset = value["v"]
                            sql = "INSERT INTO 	efx_percentage_change_uniusdt (open, close, high, low, timestamp, symbol, pricechange, prisechangepercentage, weightedaverageprice, totaltradebaseasset) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            val = (
                                open,
                                close,
                                high,
                                low,
                                timestamp,
                                symbol,
                                pricechange,
                                pricechangepercentage,
                                weightedaverageprice,
                                totaltradebaseasset,
                            )
                            curs.execute(sql, val)
                            db.commit()
                            # old record delete
                            # curs.execute(
                            #     "SELECT * FROM efx_percentage_change_uniusdt")
                            # myresult = curs.fetchall()
                            # numberva = len(myresult)
                            # if numberva > 300:
                            #     sql = "DELETE FROM efx_percentage_change_uniusdt ORDER BY id DESC limit %s"
                            #     val = (100,)
                            #     curs.execute(sql, val)
                            #     db.commit()

                        if value["s"] == "XMRUSDT":
                            # /////// get value \\\\\\\\\\\\\
                            open = value["o"]
                            high = value["h"]
                            low = value["l"]
                            close = value["c"]
                            # volume = input_dict[""]
                            timestamp = value["E"]
                            symbol = value["s"]
                            pricechange = value["p"]
                            pricechangepercentage = value["P"]
                            weightedaverageprice = value["w"]
                            totaltradebaseasset = value["v"]

                            sql = "INSERT INTO 	efx_percentage_change_xmrusdt (open, close, high, low, timestamp, symbol, pricechange, prisechangepercentage, weightedaverageprice, totaltradebaseasset) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            val = (
                                open,
                                close,
                                high,
                                low,
                                timestamp,
                                symbol,
                                pricechange,
                                pricechangepercentage,
                                weightedaverageprice,
                                totaltradebaseasset,
                            )
                            curs.execute(sql, val)
                            db.commit()
                            # old record delete
                            # curs.execute(
                            #     "SELECT * FROM efx_percentage_change_xmrusdt")
                            # myresult = curs.fetchall()
                            # numberva = len(myresult)
                            # if numberva > 300:
                            #     sql = "DELETE FROM efx_percentage_change_xmrusdt ORDER BY id DESC limit %s"
                            #     val = (100,)
                            #     curs.execute(sql, val)
                            #     db.commit()

                        if value["s"] == "XRPUSDT":
                            # /////// get value \\\\\\\\\\\\\
                            open = value["o"]
                            high = value["h"]
                            low = value["l"]
                            close = value["c"]
                            # volume = input_dict[""]
                            timestamp = value["E"]
                            symbol = value["s"]
                            pricechange = value["p"]
                            pricechangepercentage = value["P"]
                            weightedaverageprice = value["w"]
                            totaltradebaseasset = value["v"]

                            sql = "INSERT INTO 	efx_percentage_change_xrpusdt (open, close, high, low, timestamp, symbol, pricechange, prisechangepercentage, weightedaverageprice, totaltradebaseasset) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            val = (
                                open,
                                close,
                                high,
                                low,
                                timestamp,
                                symbol,
                                pricechange,
                                pricechangepercentage,
                                weightedaverageprice,
                                totaltradebaseasset,
                            )
                            curs.execute(sql, val)
                            db.commit()
                            # old record delete
                            # curs.execute(
                            #     "SELECT * FROM efx_percentage_change_xrpusdt")
                            # myresult = curs.fetchall()
                            # numberva = len(myresult)
                            # if numberva > 300:
                            #     sql = "DELETE FROM efx_percentage_change_xrpusdt ORDER BY id DESC limit %s"
                            #     val = (100,)
                            #     curs.execute(sql, val)
                            #     db.commit()

                        if value["s"] == "XTZUSDT":
                            # /////// get value \\\\\\\\\\\\\
                            open = value["o"]
                            high = value["h"]
                            low = value["l"]
                            close = value["c"]
                            # volume = input_dict[""]
                            timestamp = value["E"]
                            symbol = value["s"]
                            pricechange = value["p"]
                            pricechangepercentage = value["P"]
                            weightedaverageprice = value["w"]
                            totaltradebaseasset = value["v"]

                            sql = "INSERT INTO 	efx_percentage_change_xtzusdt (open, close, high, low, timestamp, symbol, pricechange, prisechangepercentage, weightedaverageprice, totaltradebaseasset) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            val = (
                                open,
                                close,
                                high,
                                low,
                                timestamp,
                                symbol,
                                pricechange,
                                pricechangepercentage,
                                weightedaverageprice,
                                totaltradebaseasset,
                            )
                            curs.execute(sql, val)
                            db.commit()
                            # old record delete
                            # curs.execute(
                            #     "SELECT * FROM efx_percentage_change_xtzusdt")
                            # myresult = curs.fetchall()
                            # numberva = len(myresult)
                            # if numberva > 300:
                            #     sql = "DELETE FROM efx_percentage_change_xtzusdt ORDER BY id DESC limit %s"
                            #     val = (100,)
                            #     curs.execute(sql, val)
                            #     db.commit()

                        if value["s"] == "RVNUSDT":
                            # /////// get value \\\\\\\\\\\\\
                            open = value["o"]
                            high = value["h"]
                            low = value["l"]
                            close = value["c"]
                            # volume = input_dict[""]
                            timestamp = value["E"]
                            symbol = value["s"]
                            pricechange = value["p"]
                            pricechangepercentage = value["P"]
                            weightedaverageprice = value["w"]
                            totaltradebaseasset = value["v"]

                            sql = "INSERT INTO 	efx_percentage_change_rvnusdt (open, close, high, low, timestamp, symbol, pricechange, prisechangepercentage, weightedaverageprice, totaltradebaseasset) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            val = (
                                open,
                                close,
                                high,
                                low,
                                timestamp,
                                symbol,
                                pricechange,
                                pricechangepercentage,
                                weightedaverageprice,
                                totaltradebaseasset,
                            )
                            curs.execute(sql, val)
                            db.commit()
                            # old record delete
                            # curs.execute(
                            #     "SELECT * FROM efx_percentage_change_rvnusdt")
                            # myresult = curs.fetchall()
                            # numberva = len(myresult)
                            # if numberva > 300:
                            #     sql = "DELETE FROM efx_percentage_change_rvnusdt ORDER BY id DESC limit %s"
                            #     val = (100,)
                            #     curs.execute(sql, val)
                            #     db.commit()

                        if value["s"] == "SHIBUSDT":
                            # /////// get value \\\\\\\\\\\\\
                            open = value["o"]
                            high = value["h"]
                            low = value["l"]
                            close = value["c"]
                            # volume = input_dict[""]
                            timestamp = value["E"]
                            symbol = value["s"]
                            pricechange = value["p"]
                            pricechangepercentage = value["P"]
                            weightedaverageprice = value["w"]
                            totaltradebaseasset = value["v"]

                            sql = "INSERT INTO 	efx_percentage_change_shibusdt (open, close, high, low, timestamp, symbol, pricechange, prisechangepercentage, weightedaverageprice, totaltradebaseasset) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            val = (
                                open,
                                close,
                                high,
                                low,
                                timestamp,
                                symbol,
                                pricechange,
                                pricechangepercentage,
                                weightedaverageprice,
                                totaltradebaseasset,
                            )
                            curs.execute(sql, val)
                            db.commit()
                            # old record delete
                            # curs.execute(
                            #     "SELECT * FROM efx_percentage_change_shibusdt")
                            # myresult = curs.fetchall()
                            # numberva = len(myresult)
                            # if numberva > 300:
                            #     sql = "DELETE FROM efx_percentage_change_shibusdt ORDER BY id DESC limit %s"
                            #     val = (100,)
                            #     curs.execute(sql, val)
                            #     db.commit()

                        if value["s"] == "MATICUSDT":
                            # /////// get value \\\\\\\\\\\\\
                            open = value["o"]
                            high = value["h"]
                            low = value["l"]
                            close = value["c"]
                            # volume = input_dict[""]
                            timestamp = value["E"]
                            symbol = value["s"]
                            pricechange = value["p"]
                            pricechangepercentage = value["P"]
                            weightedaverageprice = value["w"]
                            totaltradebaseasset = value["v"]

                            sql = "INSERT INTO 	efx_percentage_change_maticusdt (open, close, high, low, timestamp, symbol, pricechange, prisechangepercentage, weightedaverageprice, totaltradebaseasset) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            val = (
                                open,
                                close,
                                high,
                                low,
                                timestamp,
                                symbol,
                                pricechange,
                                pricechangepercentage,
                                weightedaverageprice,
                                totaltradebaseasset,
                            )
                            curs.execute(sql, val)
                            db.commit()
                            # old record delete
                            # curs.execute(
                            #     "SELECT * FROM efx_percentage_change_maticusdt")
                            # myresult = curs.fetchall()
                            # numberva = len(myresult)
                            # if numberva > 300:
                            #     sql = "DELETE FROM efx_percentage_change_maticusdt ORDER BY id DESC limit %s"
                            #     val = (100,)
                            #     curs.execute(sql, val)
                            #     db.commit()

                        if value["s"] == "CELRUSDT":
                            # /////// get value \\\\\\\\\\\\\
                            open = value["o"]
                            high = value["h"]
                            low = value["l"]
                            close = value["c"]
                            # volume = input_dict[""]
                            timestamp = value["E"]
                            symbol = value["s"]
                            pricechange = value["p"]
                            pricechangepercentage = value["P"]
                            weightedaverageprice = value["w"]
                            totaltradebaseasset = value["v"]

                            sql = "INSERT INTO 	efx_percentage_change_celrusdt (open, close, high, low, timestamp, symbol, pricechange, prisechangepercentage, weightedaverageprice, totaltradebaseasset) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            val = (
                                open,
                                close,
                                high,
                                low,
                                timestamp,
                                symbol,
                                pricechange,
                                pricechangepercentage,
                                weightedaverageprice,
                                totaltradebaseasset,
                            )
                            curs.execute(sql, val)
                            db.commit()
                            # old record delete
                            # curs.execute(
                            #     "SELECT * FROM efx_percentage_change_celrusdt")
                            # myresult = curs.fetchall()
                            # numberva = len(myresult)
                            # if numberva > 300:
                            #     sql = "DELETE FROM efx_percentage_change_celrusdt ORDER BY id DESC limit %s"
                            #     val = (100,)
                            #     curs.execute(sql, val)
                            #     db.commit()

                        if value["s"] == "BTCTUSDT":
                            # /////// get value \\\\\\\\\\\\\
                            open = value["o"]
                            high = value["h"]
                            low = value["l"]
                            close = value["c"]
                            # volume = input_dict[""]
                            timestamp = value["E"]
                            symbol = value["s"]
                            pricechange = value["p"]
                            pricechangepercentage = value["P"]
                            weightedaverageprice = value["w"]
                            totaltradebaseasset = value["v"]

                            sql = "INSERT INTO 	efx_percentage_change_btctusdt (open, close, high, low, timestamp, symbol, pricechange, prisechangepercentage, weightedaverageprice, totaltradebaseasset) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            val = (
                                open,
                                close,
                                high,
                                low,
                                timestamp,
                                symbol,
                                pricechange,
                                pricechangepercentage,
                                weightedaverageprice,
                                totaltradebaseasset,
                            )
                            curs.execute(sql, val)
                            db.commit()
                            # old record delete
                            # curs.execute(
                            #     "SELECT * FROM efx_percentage_change_btctusdt")
                            # myresult = curs.fetchall()
                            # numberva = len(myresult)
                            # if numberva > 300:
                            #     sql = "DELETE FROM efx_percentage_change_btctusdt ORDER BY id DESC limit %s"
                            #     val = (100,)
                            #     curs.execute(sql, val)
                            #     db.commit()
                except Exception as e:
                    cur_tm = 0
                    curs = db.cursor(MySQLdb.cursors.DictCursor)
                    curs.execute(
                        """
                                UPDATE apidata_get_check
                                SET timestamp = %s
                                WHERE id = %s
                                """,
                        (cur_tm, 1),
                    )
                    db.commit()
                    print(e)
    except Exception as e:
        print(e)
        exit()


loop = asyncio.get_event_loop()
try:
    asyncio.ensure_future(listen())
    loop.run_forever()
except KeyboardInterrupt:
    exit()
    pass
finally:
    print("Closing Loop")
    exit()
    loop.close()
