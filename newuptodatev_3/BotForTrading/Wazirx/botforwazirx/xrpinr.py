from logging import exception
import MySQLdb.cursors
from wazirx_sapi_client.rest import Client
import websockets
import requests
import asyncio
import json
import sys
import os
import datetime
from threading import Timer
from operator import itemgetter
import time
from numpy import arange
import numpy as np
from decimal import *

PATH_TO_ADD = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
if PATH_TO_ADD not in sys.path:
    sys.path.append(PATH_TO_ADD)


# xrpinr (open, close, high, low, timestamp, symbol, pricechange, prisechangepercentage, weightedaverageprice, totaltradebaseasset

db = MySQLdb.connect("localhost", "root", "", "xenbot")


async def listen():
    while True:
        await asyncio.sleep(1)

        curs = db.cursor(MySQLdb.cursors.DictCursor)
        curs.execute(
            """select * from efx_perchange_wz_xrpinr ORDER BY id DESC LIMIT 1"""
        )
        inchusdt = curs.fetchall()
        for value in inchusdt:
            # /////// get value \\\\\\\\\\\\\
            open = value["open"]
            high = value["high"]
            low = value["low"]
            close = value["close"]
            # volume = input_dict[""]
            timestamp = value["timestamp"]
            symbol = value["symbol"]
            pricechange = value["pricechange"]
            pricechangepercentage = value["prisechangepercentage"]
            weightedaverageprice = value["weightedaverageprice"]
            totaltradebaseasset = value["totaltradebaseasset"]
            curs.execute(
                """select * from etx_condition_for_btctousdt   where margincalllimit != 0 AND typeofpoint = "xrpinr" AND  buysell in (-1,1,4,5)"""
            )
            inchusdt1 = curs.fetchall()
            for a in inchusdt1:
                setvalu = Decimal(a["set_profitratio_portfo_finalvalli"])
                lastpointvalu = Decimal(close)
                id = a["id"]
                buyper = Decimal(a["takieprofitratio"])
                prisec = Decimal(pricechangepercentage)
                close = Decimal(close)
                conditionfirst = int(a["conditionfirst"])
                api_key = a["binance_apikey"]
                api_secret = a["binance_securitykey"]
                try:
                    if a["buysell"] == -1:
                        if prisec <= buyper:
                            id = a["id"]
                            # print(id)
                            user_id = a["user_id"]
                            buy_or_sell_amount = Decimal(a["firstbuyinamout"])
                            created_datetime = int(time.time())
                            margincalllimit = a["margincalllimit"]
                            sell = 1
                            set_profit_ratio = Decimal(a["sell_point"])
                            cal_loss = Decimal(a["stop_loss"])
                            stoplosslimite = Decimal(a["sell_stop_limit"])
                            symbol = a["typeofpoint"]
                            # get balance
                            api_key = a["binance_apikey"]
                            api_secret = a["binance_securitykey"]
                            # check balance
                            c = Client(api_key=api_key, secret_key=api_secret)
                            data = c.send(
                                "funds_info",
                                {
                                    "timestamp": int(time.time() * 1000),
                                },
                            )
                            for i in data[1]:
                                if i["asset"] == "inr":
                                    inr_bal = Decimal(i["free"])

                            if inr_bal >= buy_or_sell_amount:
                                try:
                                    qum_aave = Decimal(
                                        buy_or_sell_amount/close)
                                    buy_limit = c.send("create_order", {"side": "buy",
                                                                        "price": buy_or_sell_amount,
                                                                        "quantity": qum_aave,
                                                                        "type": "limit",
                                                                        "symbol": symbol,
                                                                        "timestamp": int(time.time() * 1000), })

                                    symbol = buy_limit[1]["symbol"]
                                    orderId = buy_limit[1]["id"]
                                    price_ori = Decimal(close*qum_aave)
                                    origQty = buy_limit[1]["origQty"]

                                    status = buy_limit[1]["status"]
                                    side = buy_limit[1]["side"]
                                    price_after = 0
                                    qty = 0
                                    commission = 0
                                    commissionAsset = "Null"
                                    tradeId = 0
                                    calu_profit = (
                                        price_ori * set_profit_ratio) / 100
                                    sell2 = (set_profit_ratio) - (cal_loss)
                                    fin_cal_loss = (price_ori * sell2) / 100

                                    stoplosslimite1 = (
                                        price_ori * stoplosslimite) / 100
                                    stoplosslimite2 = price_ori - stoplosslimite1

                                    stoplimit = set_profit_ratio - stoplosslimite
                                    fin_thired_point = (
                                        price_ori * stoplimit) / 100

                                    set_profitratio_portfoli = calu_profit
                                    set_profitratio_portfo_finalvalli = (
                                        calu_profit + price_ori
                                    )
                                    call_back_for_loss = price_ori + fin_thired_point
                                    stoplosslimite = stoplosslimite2

                                    curs = db.cursor(
                                        MySQLdb.cursors.DictCursor)

                                    sql = "INSERT INTO efx_bids_for_buysell (user_id, buy_or_sell_amount, btctousdpointaction, btctousdtprise, created_datetime, id_whichsellorbuy, buy_or_sell, symbol, orderId, orderListId, clientOrderId, price, origQty, cummulativeQuoteQty, status, 	side, price_with_commision, qty_with_commision, commission, commissionAsset, tradeId) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                                    val = (
                                        user_id,
                                        buy_or_sell_amount,
                                        pricechange,
                                        close,
                                        created_datetime,
                                        id,
                                        sell,
                                        symbol,
                                        orderId,
                                        orderId,
                                        orderId,
                                        price_ori,
                                        origQty,
                                        0,
                                        status,
                                        side,
                                        price_after,
                                        qty,
                                        commission,
                                        commissionAsset,
                                        tradeId,
                                    )
                                    curs.execute(sql, val)
                                    db.commit()

                                    # update query for set
                                    curs.execute(
                                        """
                                                UPDATE etx_condition_for_btctousdt
                                                SET margincalllimit = %s, buysell = %s, set_profitratio_portfoli = %s, set_profitratio_portfo_finalvalli = %s, call_back_for_loss = %s, stoplosslimite = %s
                                                WHERE id = %s
                                                """,
                                        (
                                            margincalllimit,
                                            sell,
                                            set_profitratio_portfoli,
                                            set_profitratio_portfo_finalvalli,
                                            call_back_for_loss,
                                            stoplosslimite,
                                            id,
                                        ),
                                    )
                                    db.commit()

                                    print("done buy")
                                except Exception as e:
                                    print(e)
                                    margincalllimit = 0
                                    sell = 0
                                    curs = db.cursor(
                                        MySQLdb.cursors.DictCursor)
                                    curs.execute(
                                        """
                                            UPDATE etx_condition_for_btctousdt
                                            SET margincalllimit = %s, buysell = %s
                                            WHERE id = %s
                                            """,
                                        (margincalllimit, sell, id),
                                    )
                                    db.commit()
                                    pass
                            else:
                                margincalllimit = 0
                                sell = 0
                                curs = db.cursor(MySQLdb.cursors.DictCursor)
                                curs.execute(
                                    """
                                        UPDATE etx_condition_for_btctousdt
                                        SET margincalllimit = %s, buysell = %s
                                        WHERE id = %s
                                        """,
                                    (margincalllimit, sell, id),
                                )
                                db.commit()
                            # buy here call_back_for_loss
                    elif a["buysell"] == 1:
                        if setvalu <= lastpointvalu:
                            # check hiher
                            curs.execute(
                                """select * from efx_perchange_wz_xrpinr  order by id DESC LIMIT 1,1"""
                            )
                            inchusdt2 = curs.fetchall()
                            for inchusdt2 in inchusdt2:
                                secondlastval = Decimal(inchusdt2['close'])
                            conditionfirst = 1
                            curs.execute(
                                """
                                    UPDATE etx_condition_for_btctousdt
                                    SET conditionfirst = %s
                                    WHERE id = %s
                                    """,
                                (conditionfirst, id),
                            )
                            db.commit()
                            if secondlastval < lastpointvalu:
                                setpoint = a["check_log"] + 1
                                sellingpriceperce = Decimal(
                                    a["sell_stop_limit"])
                                sellprice = Decimal(sellingpriceperce / 100)
                                newsellpri = sellprice * lastpointvalu
                                sellprisefinal = lastpointvalu - newsellpri
                                old_last = Decimal(a["positiontakecall"])
                                if old_last < sellprisefinal:
                                    curs.execute(
                                        """
                                        UPDATE etx_condition_for_btctousdt
                                        SET check_log = %s, positiontakecall = %s
                                        WHERE id = %s
                                        """,
                                        (setpoint, newsellpri, id),
                                    )
                                    db.commit()
                            else:
                                sellsecondprise = a["positiontakecall"]
                                if sellsecondprise > lastpointvalu:
                                    id = a["id"]
                                    user_id = a["user_id"]
                                    buy_or_sell_amount = Decimal(
                                        a["firstbuyinamout"])
                                    margincalllimit = 0
                                    created_datetime = int(time.time())

                                    sell = 0
                                    symbol = a["typeofpoint"]
                                    # get balance

                                    sql_bal1 = """select * from efx_bids_for_buysell where id_whichsellorbuy = %s"""
                                    val_bal1 = (id,)
                                    curs.execute(sql_bal1, val_bal1)
                                    inchusdt5 = curs.fetchall()
                                    for sellsub in inchusdt5:
                                        secondlastval2 = Decimal(
                                            sellsub["origQty"])
                                        priseforbuyf = Decimal(
                                            sellsub["price"])
                                        finalget = secondlastval2 * priseforbuyf
                                    api_key = a["binance_apikey"]
                                    api_secret = a["binance_securitykey"]
                                    # check balance
                                    c = Client(api_key=api_key,
                                               secret_key=api_secret)
                                    data = c.send(
                                        "funds_info",
                                        {
                                            "timestamp": int(time.time() * 1000),
                                        },
                                    )
                                    for i in data[1]:
                                        if i["asset"] == "xrp":
                                            current_bln_fin = Decimal(
                                                i["free"])

                                    if current_bln_fin >= secondlastval2:
                                        try:
                                            qumsell_aave = Decimal(
                                                priseforbuyf/close)
                                            sell_limit = c.send("create_order", {"side": "sell",
                                                                                 "price": buy_or_sell_amount,
                                                                                 "quantity": qumsell_aave,
                                                                                 "type": "limit",
                                                                                 "symbol": symbol,
                                                                                 "timestamp": int(time.time() * 1000), })

                                            symbol = sell_limit[1]["symbol"]
                                            orderId = sell_limit[1]["id"]
                                            price_ori = Decimal(close*qum_aave)
                                            origQty = sell_limit[1]["origQty"]

                                            status = sell_limit[1]["status"]
                                            side = sell_limit[1]["side"]

                                            price_after = float(price_ori) * float(
                                                origQty
                                            ) - float(finalget)
                                            if price_after < 0:
                                                prise_for_bot = 0
                                            else:
                                                prise_for_bot = (
                                                    price_after * 20) / 100
                                                cut_bal = float(prise_for_bot)
                                                curs.execute(
                                                    """
                                                        UPDATE efx_users
                                                        SET efx_wallet = efx_wallet - %s
                                                        WHERE id = %s
                                                        """,
                                                    (cut_bal, user_id),
                                                )
                                                db.commit()
                                                sub_id = a["copy_bottradebid"]
                                                if sub_id != 0:
                                                    sub_profit = a[
                                                        "copy_tradepercentage"
                                                    ]
                                                    sub_profit1 = (
                                                        price_after * sub_profit
                                                    ) / 100
                                                    cut_bal1 = float(
                                                        sub_profit1)
                                                    curs.execute(
                                                        """
                                                        UPDATE efx_users
                                                        SET efx_wallet = efx_wallet - %s
                                                        WHERE id = %s
                                                        """,
                                                        (cut_bal1, user_id),
                                                    )
                                                    db.commit()
                                                    sub_commision = cut_bal1
                                                    sub_type = "BOT"
                                                    sub_mastuserid = a[
                                                        "copy_bottradebid"
                                                    ]

                                                    sql = "INSERT INTO efx_copy_history (user_id, master_user_id, trade_amount, trade_date, profit, commission, type) VALUE (%s,%s,%s,%s,%s,%s,%s)"
                                                    val = (
                                                        user_id,
                                                        sub_mastuserid,
                                                        buy_or_sell_amount,
                                                        created_datetime,
                                                        sub_profit1,
                                                        sub_commision,
                                                        sub_type,
                                                    )
                                                    curs.execute(sql, val)
                                                    db.commit()
                                            qty = 0
                                            if "cut_bal1" in locals():
                                                commission = cut_bal1
                                            else:
                                                commission = 0
                                            commissionAsset = "Null"
                                            tradeId = 0
                                            curs = db.cursor(
                                                MySQLdb.cursors.DictCursor)
                                            sql = "INSERT INTO efx_bids_for_buysell (user_id, buy_or_sell_amount, btctousdpointaction, btctousdtprise, created_datetime, id_whichsellorbuy, buy_or_sell, symbol, orderId, orderListId, clientOrderId, price, origQty, cummulativeQuoteQty, status, 	side, price_with_commision, qty_with_commision, commission, commissionAsset, tradeId) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                                            val = (
                                                user_id,
                                                buy_or_sell_amount,
                                                pricechange,
                                                close,
                                                created_datetime,
                                                id,
                                                sell,
                                                symbol,
                                                orderId,
                                                orderId,
                                                orderId,
                                                price_ori,
                                                origQty,
                                                origQty,
                                                status,
                                                side,
                                                price_after,
                                                prise_for_bot,
                                                commission,
                                                commissionAsset,
                                                tradeId,
                                            )
                                            curs.execute(sql, val)
                                            db.commit()
                                            # update query for sell
                                            curs.execute(
                                                """
                                                        UPDATE etx_condition_for_btctousdt
                                                        SET margincalllimit = %s, buysell = %s
                                                        WHERE id = %s
                                                        """,
                                                (margincalllimit, sell, id),
                                            )
                                            db.commit()
                                            sql = "INSERT INTO profit_from_bot(total_profit,date_whenget,id_whom_give) VALUE (%s,%s,%s)"
                                            val = (prise_for_bot,
                                                   created_datetime, id)
                                            curs.execute(sql, val)
                                            db.commit()
                                        except exception as e:
                                            print(e)
                                            margincalllimit = 0
                                            sell = 0
                                            curs = db.cursor(
                                                MySQLdb.cursors.DictCursor)
                                            curs.execute(
                                                """
                                                UPDATE etx_condition_for_btctousdt
                                                SET margincalllimit = %s, buysell = %s
                                                WHERE id = %s
                                                """,
                                                (margincalllimit, sell, id),
                                            )
                                            db.commit()
                                            pass
                                    else:
                                        margincalllimit = 0
                                        sell = 0
                                        curs = db.cursor(
                                            MySQLdb.cursors.DictCursor)
                                        curs.execute(
                                            """
                                                            UPDATE etx_condition_for_btctousdt
                                                            SET margincalllimit = %s, buysell = %s
                                                            WHERE id = %s
                                                            """,
                                            (margincalllimit, sell, id),
                                        )
                                        db.commit()
                        elif conditionfirst == 1:
                            sellvaluefisrt = a["call_back_for_loss"]
                            if sellvaluefisrt >= lastpointvalu:
                                id = a["id"]
                                user_id = a["user_id"]
                                buy_or_sell_amount = Decimal(
                                    a["firstbuyinamout"])
                                margincalllimit = 0
                                created_datetime = int(time.time())

                                sell = 0
                                symbol = a["typeofpoint"]
                                # get balance

                                sql_bal1 = """select * from efx_bids_for_buysell where id_whichsellorbuy = %s"""
                                val_bal1 = (id,)
                                curs.execute(sql_bal1, val_bal1)
                                inchusdt5 = curs.fetchall()
                                for sellsub in inchusdt5:
                                    secondlastval2 = Decimal(
                                        sellsub["origQty"])
                                    priseforbuyf = Decimal(sellsub["price"])
                                    finalget = secondlastval2 * priseforbuyf
                                api_key = a["binance_apikey"]
                                api_secret = a["binance_securitykey"]
                                # check balance
                                c = Client(api_key=api_key,
                                           secret_key=api_secret)
                                data = c.send(
                                    "funds_info",
                                    {
                                        "timestamp": int(time.time() * 1000),
                                    },
                                )
                                for i in data[1]:
                                    if i["asset"] == "xrp":
                                        current_bln_fin = Decimal(i["free"])

                                if current_bln_fin >= secondlastval2:
                                    try:
                                        qumsell_aave = Decimal(
                                            priseforbuyf/close)
                                        sell_limit = c.send("create_order", {"side": "sell",
                                                                             "price": buy_or_sell_amount,
                                                                             "quantity": qumsell_aave,
                                                                             "type": "limit",
                                                                             "symbol": symbol,
                                                                             "timestamp": int(time.time() * 1000), })

                                        symbol = sell_limit[1]["symbol"]
                                        orderId = sell_limit[1]["id"]
                                        price_ori = Decimal(close*qum_aave)
                                        origQty = sell_limit[1]["origQty"]

                                        status = sell_limit[1]["status"]
                                        side = sell_limit[1]["side"]

                                        price_after = float(price_ori) * float(
                                            origQty
                                        ) - float(finalget)
                                        if price_after < 0:
                                            prise_for_bot = 0
                                        else:
                                            prise_for_bot = (
                                                price_after * 20) / 100
                                            cut_bal = float(prise_for_bot)
                                            curs.execute(
                                                """
                                                    UPDATE efx_users
                                                    SET efx_wallet = efx_wallet - %s
                                                    WHERE id = %s
                                                    """,
                                                (cut_bal, user_id),
                                            )
                                            db.commit()
                                            sub_id = a["copy_bottradebid"]
                                            if sub_id != 0:
                                                sub_profit = a[
                                                    "copy_tradepercentage"
                                                ]
                                                sub_profit1 = (
                                                    price_after * sub_profit
                                                ) / 100
                                                cut_bal1 = float(sub_profit1)
                                                curs.execute(
                                                    """
                                                    UPDATE efx_users
                                                    SET efx_wallet = efx_wallet - %s
                                                    WHERE id = %s
                                                    """,
                                                    (cut_bal1, user_id),
                                                )
                                                db.commit()
                                                sub_commision = cut_bal1
                                                sub_type = "BOT"
                                                sub_mastuserid = a[
                                                    "copy_bottradebid"
                                                ]

                                                sql = "INSERT INTO efx_copy_history (user_id, master_user_id, trade_amount, trade_date, profit, commission, type) VALUE (%s,%s,%s,%s,%s,%s,%s)"
                                                val = (
                                                    user_id,
                                                    sub_mastuserid,
                                                    buy_or_sell_amount,
                                                    created_datetime,
                                                    sub_profit1,
                                                    sub_commision,
                                                    sub_type,
                                                )
                                                curs.execute(sql, val)
                                                db.commit()
                                        qty = 0
                                        if "cut_bal1" in locals():
                                            commission = cut_bal1
                                        else:
                                            commission = 0
                                        commissionAsset = "Null"
                                        tradeId = 0
                                        curs = db.cursor(
                                            MySQLdb.cursors.DictCursor)
                                        sql = "INSERT INTO efx_bids_for_buysell (user_id, buy_or_sell_amount, btctousdpointaction, btctousdtprise, created_datetime, id_whichsellorbuy, buy_or_sell, symbol, orderId, orderListId, clientOrderId, price, origQty, cummulativeQuoteQty, status, 	side, price_with_commision, qty_with_commision, commission, commissionAsset, tradeId) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                                        val = (
                                            user_id,
                                            buy_or_sell_amount,
                                            pricechange,
                                            close,
                                            created_datetime,
                                            id,
                                            sell,
                                            symbol,
                                            orderId,
                                            orderId,
                                            orderId,
                                            price_ori,
                                            origQty,
                                            origQty,
                                            status,
                                            side,
                                            price_after,
                                            prise_for_bot,
                                            commission,
                                            commissionAsset,
                                            tradeId,
                                        )
                                        curs.execute(sql, val)
                                        db.commit()
                                        # update query for sell
                                        curs.execute(
                                            """
                                                    UPDATE etx_condition_for_btctousdt
                                                    SET margincalllimit = %s, buysell = %s
                                                    WHERE id = %s
                                                    """,
                                            (margincalllimit, sell, id),
                                        )
                                        db.commit()
                                        sql = "INSERT INTO profit_from_bot(total_profit,date_whenget,id_whom_give) VALUE (%s,%s,%s)"
                                        val = (prise_for_bot,
                                               created_datetime, id)
                                        curs.execute(sql, val)
                                        db.commit()
                                    except exception as e:
                                        print(e)
                                        margincalllimit = 0
                                        sell = 0
                                        curs = db.cursor(
                                            MySQLdb.cursors.DictCursor)
                                        curs.execute(
                                            """
                                            UPDATE etx_condition_for_btctousdt
                                            SET margincalllimit = %s, buysell = %s
                                            WHERE id = %s
                                            """,
                                            (margincalllimit, sell, id),
                                        )
                                        db.commit()
                                        pass
                                else:
                                    margincalllimit = 0
                                    sell = 0
                                    curs = db.cursor(
                                        MySQLdb.cursors.DictCursor)
                                    curs.execute(
                                        """
                                                        UPDATE etx_condition_for_btctousdt
                                                        SET margincalllimit = %s, buysell = %s
                                                        WHERE id = %s
                                                        """,
                                        (margincalllimit, sell, id),
                                    )
                                    db.commit()
                    elif a["buysell"] == 4:
                        # buyinstatli
                        id = a["id"]
                        user_id = a["user_id"]
                        buy_or_sell_amount = Decimal(a["firstbuyinamout"])
                        margincalllimit = 0
                        created_datetime = int(time.time())

                        sell = 0
                        symbol = a["typeofpoint"]
                        # get balance

                        sql_bal1 = """select * from efx_bids_for_buysell where id_whichsellorbuy = %s"""
                        val_bal1 = (id,)
                        curs.execute(sql_bal1, val_bal1)
                        inchusdt5 = curs.fetchall()
                        for sellsub in inchusdt5:
                            secondlastval2 = Decimal(
                                sellsub["origQty"])
                            priseforbuyf = Decimal(sellsub["price"])
                            finalget = secondlastval2 * priseforbuyf
                        api_key = a["binance_apikey"]
                        api_secret = a["binance_securitykey"]
                        # check balance
                        c = Client(api_key=api_key,
                                    secret_key=api_secret)
                        data = c.send(
                            "funds_info",
                            {
                                "timestamp": int(time.time() * 1000),
                            },
                        )
                        for i in data[1]:
                            if i["asset"] == "xrp":
                                current_bln_fin = Decimal(i["free"])

                        if current_bln_fin >= secondlastval2:
                            try:
                                qumsell_aave = Decimal(
                                    priseforbuyf/close)
                                sell_limit = c.send("create_order", {"side": "buy",
                                                                        "price": buy_or_sell_amount,
                                                                        "quantity": qumsell_aave,
                                                                        "type": "limit",
                                                                        "symbol": symbol,
                                                                        "timestamp": int(time.time() * 1000), })

                                symbol = sell_limit[1]["symbol"]
                                orderId = sell_limit[1]["id"]
                                price_ori = Decimal(close*qum_aave)
                                origQty = sell_limit[1]["origQty"]

                                status = sell_limit[1]["status"]
                                side = sell_limit[1]["side"]

                                price_after = float(price_ori) * float(
                                    origQty
                                ) - float(finalget)
                                if price_after < 0:
                                    prise_for_bot = 0
                                else:
                                    prise_for_bot = (
                                        price_after * 20) / 100
                                    cut_bal = float(prise_for_bot)
                                    curs.execute(
                                        """
                                            UPDATE efx_users
                                            SET efx_wallet = efx_wallet - %s
                                            WHERE id = %s
                                            """,
                                        (cut_bal, user_id),
                                    )
                                    db.commit()
                                    sub_id = a["copy_bottradebid"]
                                    if sub_id != 0:
                                        sub_profit = a[
                                            "copy_tradepercentage"
                                        ]
                                        sub_profit1 = (
                                            price_after * sub_profit
                                        ) / 100
                                        cut_bal1 = float(sub_profit1)
                                        curs.execute(
                                            """
                                            UPDATE efx_users
                                            SET efx_wallet = efx_wallet - %s
                                            WHERE id = %s
                                            """,
                                            (cut_bal1, user_id),
                                        )
                                        db.commit()
                                        sub_commision = cut_bal1
                                        sub_type = "BOT"
                                        sub_mastuserid = a[
                                            "copy_bottradebid"
                                        ]

                                        sql = "INSERT INTO efx_copy_history (user_id, master_user_id, trade_amount, trade_date, profit, commission, type) VALUE (%s,%s,%s,%s,%s,%s,%s)"
                                        val = (
                                            user_id,
                                            sub_mastuserid,
                                            buy_or_sell_amount,
                                            created_datetime,
                                            sub_profit1,
                                            sub_commision,
                                            sub_type,
                                        )
                                        curs.execute(sql, val)
                                        db.commit()
                                qty = 0
                                if "cut_bal1" in locals():
                                    commission = cut_bal1
                                else:
                                    commission = 0
                                commissionAsset = "Null"
                                tradeId = 0
                                curs = db.cursor(
                                    MySQLdb.cursors.DictCursor)
                                sql = "INSERT INTO efx_bids_for_buysell (user_id, buy_or_sell_amount, btctousdpointaction, btctousdtprise, created_datetime, id_whichsellorbuy, buy_or_sell, symbol, orderId, orderListId, clientOrderId, price, origQty, cummulativeQuoteQty, status, 	side, price_with_commision, qty_with_commision, commission, commissionAsset, tradeId) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                                val = (
                                    user_id,
                                    buy_or_sell_amount,
                                    pricechange,
                                    close,
                                    created_datetime,
                                    id,
                                    sell,
                                    symbol,
                                    orderId,
                                    orderId,
                                    orderId,
                                    price_ori,
                                    origQty,
                                    origQty,
                                    status,
                                    side,
                                    price_after,
                                    prise_for_bot,
                                    commission,
                                    commissionAsset,
                                    tradeId,
                                )
                                curs.execute(sql, val)
                                db.commit()
                                # update query for sell
                                curs.execute(
                                    """
                                            UPDATE etx_condition_for_btctousdt
                                            SET margincalllimit = %s, buysell = %s
                                            WHERE id = %s
                                            """,
                                    (margincalllimit, sell, id),
                                )
                                db.commit()
                                sql = "INSERT INTO profit_from_bot(total_profit,date_whenget,id_whom_give) VALUE (%s,%s,%s)"
                                val = (prise_for_bot,
                                        created_datetime, id)
                                curs.execute(sql, val)
                                db.commit()
                            except exception as e:
                                print(e)
                                margincalllimit = 0
                                sell = 0
                                curs = db.cursor(
                                    MySQLdb.cursors.DictCursor)
                                curs.execute(
                                    """
                                    UPDATE etx_condition_for_btctousdt
                                    SET margincalllimit = %s, buysell = %s
                                    WHERE id = %s
                                    """,
                                    (margincalllimit, sell, id),
                                )
                                db.commit()
                                pass
                        else:
                            margincalllimit = 0
                            sell = 0
                            curs = db.cursor(
                                MySQLdb.cursors.DictCursor)
                            curs.execute(
                                """
                                                UPDATE etx_condition_for_btctousdt
                                                SET margincalllimit = %s, buysell = %s
                                                WHERE id = %s
                                                """,
                                (margincalllimit, sell, id),
                            )
                            db.commit()
                    elif a["buysell"] == 5:
                        id = a["id"]
                        user_id = a["user_id"]
                        buy_or_sell_amount = Decimal(a["firstbuyinamout"])
                        margincalllimit = 0
                        created_datetime = int(time.time())

                        sell = 0
                        symbol = a["typeofpoint"]
                        # get balance

                        sql_bal1 = """select * from efx_bids_for_buysell where id_whichsellorbuy = %s"""
                        val_bal1 = (id,)
                        curs.execute(sql_bal1, val_bal1)
                        inchusdt5 = curs.fetchall()
                        for sellsub in inchusdt5:
                            secondlastval2 = Decimal(
                                sellsub["origQty"])
                            priseforbuyf = Decimal(sellsub["price"])
                            finalget = secondlastval2 * priseforbuyf
                        api_key = a["binance_apikey"]
                        api_secret = a["binance_securitykey"]
                        # check balance
                        c = Client(api_key=api_key,
                                    secret_key=api_secret)
                        data = c.send(
                            "funds_info",
                            {
                                "timestamp": int(time.time() * 1000),
                            },
                        )
                        for i in data[1]:
                            if i["asset"] == "xrp":
                                current_bln_fin = Decimal(i["free"])

                        if current_bln_fin >= secondlastval2:
                            try:
                                qumsell_aave = Decimal(
                                    priseforbuyf/close)
                                sell_limit = c.send("create_order", {"side": "sell",
                                                                        "price": buy_or_sell_amount,
                                                                        "quantity": qumsell_aave,
                                                                        "type": "limit",
                                                                        "symbol": symbol,
                                                                        "timestamp": int(time.time() * 1000), })

                                symbol = sell_limit[1]["symbol"]
                                orderId = sell_limit[1]["id"]
                                price_ori = Decimal(close*qum_aave)
                                origQty = sell_limit[1]["origQty"]

                                status = sell_limit[1]["status"]
                                side = sell_limit[1]["side"]

                                price_after = float(price_ori) * float(
                                    origQty
                                ) - float(finalget)
                                if price_after < 0:
                                    prise_for_bot = 0
                                else:
                                    prise_for_bot = (
                                        price_after * 20) / 100
                                    cut_bal = float(prise_for_bot)
                                    curs.execute(
                                        """
                                            UPDATE efx_users
                                            SET efx_wallet = efx_wallet - %s
                                            WHERE id = %s
                                            """,
                                        (cut_bal, user_id),
                                    )
                                    db.commit()
                                    sub_id = a["copy_bottradebid"]
                                    if sub_id != 0:
                                        sub_profit = a[
                                            "copy_tradepercentage"
                                        ]
                                        sub_profit1 = (
                                            price_after * sub_profit
                                        ) / 100
                                        cut_bal1 = float(sub_profit1)
                                        curs.execute(
                                            """
                                            UPDATE efx_users
                                            SET efx_wallet = efx_wallet - %s
                                            WHERE id = %s
                                            """,
                                            (cut_bal1, user_id),
                                        )
                                        db.commit()
                                        sub_commision = cut_bal1
                                        sub_type = "BOT"
                                        sub_mastuserid = a[
                                            "copy_bottradebid"
                                        ]

                                        sql = "INSERT INTO efx_copy_history (user_id, master_user_id, trade_amount, trade_date, profit, commission, type) VALUE (%s,%s,%s,%s,%s,%s,%s)"
                                        val = (
                                            user_id,
                                            sub_mastuserid,
                                            buy_or_sell_amount,
                                            created_datetime,
                                            sub_profit1,
                                            sub_commision,
                                            sub_type,
                                        )
                                        curs.execute(sql, val)
                                        db.commit()
                                qty = 0
                                if "cut_bal1" in locals():
                                    commission = cut_bal1
                                else:
                                    commission = 0
                                commissionAsset = "Null"
                                tradeId = 0
                                curs = db.cursor(
                                    MySQLdb.cursors.DictCursor)
                                sql = "INSERT INTO efx_bids_for_buysell (user_id, buy_or_sell_amount, btctousdpointaction, btctousdtprise, created_datetime, id_whichsellorbuy, buy_or_sell, symbol, orderId, orderListId, clientOrderId, price, origQty, cummulativeQuoteQty, status, 	side, price_with_commision, qty_with_commision, commission, commissionAsset, tradeId) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                                val = (
                                    user_id,
                                    buy_or_sell_amount,
                                    pricechange,
                                    close,
                                    created_datetime,
                                    id,
                                    sell,
                                    symbol,
                                    orderId,
                                    orderId,
                                    orderId,
                                    price_ori,
                                    origQty,
                                    origQty,
                                    status,
                                    side,
                                    price_after,
                                    prise_for_bot,
                                    commission,
                                    commissionAsset,
                                    tradeId,
                                )
                                curs.execute(sql, val)
                                db.commit()
                                # update query for sell
                                curs.execute(
                                    """
                                            UPDATE etx_condition_for_btctousdt
                                            SET margincalllimit = %s, buysell = %s
                                            WHERE id = %s
                                            """,
                                    (margincalllimit, sell, id),
                                )
                                db.commit()
                                sql = "INSERT INTO profit_from_bot(total_profit,date_whenget,id_whom_give) VALUE (%s,%s,%s)"
                                val = (prise_for_bot,
                                        created_datetime, id)
                                curs.execute(sql, val)
                                db.commit()
                            except exception as e:
                                print(e)
                                margincalllimit = 0
                                sell = 0
                                curs = db.cursor(
                                    MySQLdb.cursors.DictCursor)
                                curs.execute(
                                    """
                                    UPDATE etx_condition_for_btctousdt
                                    SET margincalllimit = %s, buysell = %s
                                    WHERE id = %s
                                    """,
                                    (margincalllimit, sell, id),
                                )
                                db.commit()
                                pass
                        else:
                            margincalllimit = 0
                            sell = 0
                            curs = db.cursor(
                                MySQLdb.cursors.DictCursor)
                            curs.execute(
                                """
                                                UPDATE etx_condition_for_btctousdt
                                                SET margincalllimit = %s, buysell = %s
                                                WHERE id = %s
                                                """,
                                (margincalllimit, sell, id),
                            )
                            db.commit()
                except:
                    e = "key error"
            # cycle check
            sql_bal3 = """select * from bot_stratage where coin = 'xrpinr' and cycle_status = 1"""
            curs.execute(
                sql_bal3,
            )
            kavausdtforstratage = curs.fetchall()
            if not kavausdtforstratage:
                new = 0
            else:
                for kavausdtforstratagesingle in kavausdtforstratage:
                    user_id1 = kavausdtforstratagesingle["id_whom_set"]
                    stratage_id = kavausdtforstratagesingle["id"]
                    margincalllimit = 0
                    buysell = 0
                    typeofpoint = "xrpinr"
                    curs.execute(
                        """
                            UPDATE etx_condition_for_btctousdt
                            SET margincalllimit = %s, buysell = %s
                            WHERE typeofpoint = %s and user_id = %s
                            """,
                        (
                            margincalllimit,
                            buysell,
                            typeofpoint,
                            user_id1,
                        ),
                    )
                    db.commit()
                    sql_bal4 = """select * from etx_condition_for_btctousdt where stratage_id = %s and user_id = %s and margincalllimit != 0 and  buysell in (-1,1)"""
                    val_bal4 = (stratage_id, user_id1)
                    curs.execute(sql_bal4, val_bal4)
                    kavausdtforstratage1 = curs.fetchall()
                    if not kavausdtforstratage1:
                        sql_bal5 = """SELECT * FROM xenbot.bot_stratage c left join xenbot.multiple_bid_forbot o on  o.stratage_id = c.id where c.id = %s and c.id_whom_set = %s"""
                        val_bal5 = (stratage_id, user_id1)
                        curs.execute(sql_bal5, val_bal5)
                        kavausdtforstratage2 = curs.fetchall()
                        for stratage in kavausdtforstratage2:
                            tradeamount = float(stratage["tradeamount"]) * float(
                                stratage["buyamountmul"]
                            )
                            coin = stratage["coin"]
                            user_idfor_cycle = stratage["id_whom_set"]
                            sellfirstpoint = stratage["sell"]
                            sellsecondpoint = stratage["sellstop"]
                            creatdate = int(time.time())
                            stratage_id = stratage_id
                            margincalllimitforcyc = 1
                            buysellforcycle = -1
                            buypoint = -float(stratage["buypoint"])
                            api_keyforcycle = stratage["apikey"]
                            api_keysecurityforcycle = stratage["apisecurity"]
                            curs = db.cursor(MySQLdb.cursors.DictCursor)
                            total_stratageforcy = stratage["total_stratage"]
                            multiply_cycle = stratage["buyamountmul"]

                            sql = "INSERT INTO etx_condition_for_btctousdt (user_id, firstbuyinamout, margincalllimit,buysell, takieprofitratio, created_date, sell_point, sell_stop_limit, typeofpoint, binance_apikey, binance_securitykey, total_stratage, stratage_id, multiplebuyinratio) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            val = (
                                user_idfor_cycle,
                                tradeamount,
                                margincalllimitforcyc,
                                buysellforcycle,
                                buypoint,
                                creatdate,
                                sellfirstpoint,
                                sellsecondpoint,
                                coin,
                                api_keyforcycle,
                                api_keysecurityforcycle,
                                total_stratageforcy,
                                stratage_id,
                                multiply_cycle,
                            )
                            curs.execute(sql, val)
                            db.commit()
                    else:
                        new = 1


loop = asyncio.get_event_loop()
try:
    asyncio.ensure_future(listen())
    loop.run_forever()
except KeyboardInterrupt:
    pass
finally:
    print("Closing Loop")
    loop.close()
