import asyncio
import sys
import datetime
import datetime
import os
from win32com.client import GetObject
import MySQLdb.cursors

db = MySQLdb.connect("localhost", "root", "", "xenbot")

path = "C:\\newuptodatev_3\\BotForTrading\\Wazirx\\storedata\\"


async def listen():
    while True:
        await asyncio.sleep(1)
        curs = db.cursor(MySQLdb.cursors.DictCursor)
        dat_new = datetime.datetime.now()

        dat_new = dat_new.strftime("%Y-%m-%d %H:%M %p")
        curs.execute(
            "SELECT * FROM efx_perchange_wz_adainr ORDER BY id DESC LIMIT 1")
        myresult = curs.fetchall()

        for i in myresult:
            old_wazirx = i["timestamp"]
            oldt_wazirx = datetime.datetime.strptime(
                old_wazirx, "%Y-%m-%d %H:%M:%S %p"
            )
            date_sub = datetime.timedelta(minutes=1)
            dat_new_1 = oldt_wazirx + date_sub
            dat_new_1 = dat_new_1.strftime("%Y-%m-%d %H:%M %p")
            dat_new_2 = oldt_wazirx - date_sub
            dat_new_2 = dat_new_2.strftime("%Y-%m-%d %H:%M %p")

            oldt_wazirx = oldt_wazirx.strftime("%Y-%m-%d %H:%M %p")
            if oldt_wazirx != dat_new:
                if dat_new_1 != dat_new and dat_new_2 != dat_new:
                    # START "for new nodechart stop"
                    WMI = GetObject("winmgmts:")
                    processes = WMI.InstancesOf("Win32_Process")
                    for p in WMI.ExecQuery(
                        'select * from Win32_Process where Name="cmd.exe"'
                    ):
                        old_path = str(p.Properties_("CommandLine").Value)
                        print(old_path)
                        if old_path == "cmd  /K python datagetandstoreforpercent.py":
                            print(old_path)
                            os.system(
                                "taskkill /pid " +
                                str(p.Properties_("ProcessId").Value)
                            )
                            print("old close")
                            await asyncio.sleep(0.10)
                    # START "for new nodechart stop"
                    ########################################################################
                    # START "for new nodechart start"

                    class cd:
                        """Context manager for changing the current working directory"""

                        def __init__(self, newPath):
                            self.newPath = os.path.expanduser(newPath)

                        def __enter__(self):
                            self.savedPath = os.getcwd()
                            os.chdir(self.newPath)

                        def __exit__(self, etype, value, traceback):
                            os.chdir(self.savedPath)

                    with cd(path):
                        # we are in ~/Library
                        new = os.getcwd()
                        os.system(
                            "start cmd /K python datagetandstoreforpercent.py")
                        await asyncio.sleep(10.0)
                    # END "for new nodechart start"
                    ############################################################################
        print(dat_new)


loop = asyncio.get_event_loop()
try:
    asyncio.ensure_future(listen())
    loop.run_forever()
except KeyboardInterrupt:
    pass
finally:
    print("Closing Loop")
    loop.close()
