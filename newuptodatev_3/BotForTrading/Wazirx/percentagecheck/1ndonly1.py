from unicodedata import decimal
import websockets
import requests
import asyncio
import json
import sys
import datetime
from threading import Timer
from operator import itemgetter
import datetime
from time import process_time
from decimal import *

import MySQLdb.cursors

db = MySQLdb.connect("localhost", "root", "", "xenbot")



async def listen():
    url = "https://api.wazirx.com/api/v2/tickers"
    while True:
        await asyncio.sleep(0.60)
        x = datetime.datetime.now()
        # ! check datetime
        calltime = x.strftime("%Y-%m-%d %H:%M:%S %p")
        r = requests.get(url)
        input_dictr = r.json()
        curs = db.cursor(MySQLdb.cursors.DictCursor)
        try:
            curs = db.cursor(MySQLdb.cursors.DictCursor)
            input_dict1 = input_dictr["adainr"]
            if input_dict1["base_unit"] == "ada":
                open_p1 = input_dict1["open"]
                high1 = input_dict1["high"]
                low1 = input_dict1["low"]
                close1 = input_dict1["last"]
                # volume = input_dict[""]
                timestamp1 = input_dict1["at"]
                symbol1 = input_dict1["base_unit"]
                totaltradebaseasset1 = input_dict1["volume"]

                # ! check datetime convert store formate
                timestamnewapi1 = datetime.datetime.fromtimestamp(timestamp1 / 1e3)
                timestamnewapi1 = timestamnewapi1.strftime("%Y-%m-%d %H:%M:%S %p")

                sql = "INSERT INTO 	efx_percentage_change_adainr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                val = (
                    open_p1,
                    close1,
                    high1,
                    low1,
                    symbol1,
                    totaltradebaseasset1,
                    timestamnewapi1,
                    calltime,
                )
                curs.execute(sql, val)
                db.commit()
            input_dict2 = input_dictr["atominr"]
            if input_dict2["base_unit"] == "atom":
                open_p2 = input_dict2["open"]
                high2 = input_dict2["high"]
                low2 = input_dict2["low"]
                close2 = input_dict2["last"]
                # volume = input_dict[""]
                timestamp2 = input_dict2["at"]
                symbol2 = input_dict2["base_unit"]
                totaltradebaseasset2 = input_dict2["volume"]

                # ! check datetime convert store formate
                timestamnewapi2 = datetime.datetime.fromtimestamp(timestamp2 / 1e3)
                timestamnewapi2 = timestamnewapi2.strftime("%Y-%m-%d %H:%M:%S %p")

                sql = "INSERT INTO 	efx_percentage_change_atominr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                val = (
                    open_p2,
                    close2,
                    high2,
                    low2,
                    symbol2,
                    totaltradebaseasset2,
                    timestamnewapi2,
                    calltime,
                )
                curs.execute(sql, val)
                db.commit()
            input_dict3 = input_dictr["batinr"]
            if input_dict3["base_unit"] == "bat":
                open_p3 = input_dict3["open"]
                high3 = input_dict3["high"]
                low3 = input_dict3["low"]
                close3 = input_dict3["last"]
                # volume = input_dict[""]
                timestamp3 = input_dict3["at"]
                symbol3 = input_dict3["base_unit"]
                totaltradebaseasset3 = input_dict3["volume"]

                # ! check datetime convert store formate
                timestamnewapi3 = datetime.datetime.fromtimestamp(timestamp3 / 1e3)
                timestamnewapi3 = timestamnewapi3.strftime("%Y-%m-%d %H:%M:%S %p")

                sql = "INSERT INTO 	efx_percentage_change_batinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                val = (
                    open_p3,
                    close3,
                    high3,
                    low3,
                    symbol3,
                    totaltradebaseasset3,
                    timestamnewapi3,
                    calltime,
                )
                curs.execute(sql, val)
                db.commit()
            input_dict4 = input_dictr["bchinr"]
            if input_dict4["base_unit"] == "bch":
                open_p4 = input_dict4["open"]
                high4 = input_dict4["high"]
                low4 = input_dict4["low"]
                close4 = input_dict4["last"]
                # volume = input_dict[""]
                timestamp4 = input_dict4["at"]
                symbol4 = input_dict4["base_unit"]
                totaltradebaseasset4 = input_dict4["volume"]

                # ! check datetime convert store formate
                timestamnewapi4 = datetime.datetime.fromtimestamp(timestamp4 / 1e3)
                timestamnewapi4 = timestamnewapi4.strftime("%Y-%m-%d %H:%M:%S %p")

                sql = "INSERT INTO 	efx_percentage_change_bchinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                val = (
                    open_p4,
                    close4,
                    high4,
                    low4,
                    symbol4,
                    totaltradebaseasset4,
                    timestamnewapi4,
                    calltime,
                )
                curs.execute(sql, val)
                db.commit()
            input_dict4 = input_dictr["bnbinr"]
            if input_dict4["base_unit"] == "bnb":
                open_p4 = input_dict4["open"]
                high4 = input_dict4["high"]
                low4 = input_dict4["low"]
                close4 = input_dict4["last"]
                # volume = input_dict[""]
                timestamp4 = input_dict4["at"]
                symbol4 = input_dict4["base_unit"]
                totaltradebaseasset4 = input_dict4["volume"]

                # ! check datetime convert store formate
                timestamnewapi4 = datetime.datetime.fromtimestamp(timestamp4 / 1e3)
                timestamnewapi4 = timestamnewapi4.strftime("%Y-%m-%d %H:%M:%S %p")

                sql = "INSERT INTO 	efx_percentage_change_bnbinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                val = (
                    open_p4,
                    close4,
                    high4,
                    low4,
                    symbol4,
                    totaltradebaseasset4,
                    timestamnewapi4,
                    calltime,
                )
                curs.execute(sql, val)
                db.commit()
            input_dict5 = input_dictr["btcinr"]
            if input_dict5["base_unit"] == "btc":
                open_p5 = input_dict5["open"]
                high5 = input_dict5["high"]
                low5 = input_dict5["low"]
                close5 = input_dict5["last"]
                # volume = input_dict[""]
                timestamp5 = input_dict5["at"]
                symbol5 = input_dict5["base_unit"]
                totaltradebaseasset5 = input_dict5["volume"]

                # ! check datetime convert store formate
                timestamnewapi5 = datetime.datetime.fromtimestamp(timestamp5 / 1e3)
                timestamnewapi5 = timestamnewapi5.strftime("%Y-%m-%d %H:%M:%S %p")

                sql = "INSERT INTO 	efx_percentage_change_btcinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                val = (
                    open_p5,
                    close5,
                    high5,
                    low5,
                    symbol5,
                    totaltradebaseasset5,
                    timestamnewapi5,
                    calltime,
                )
                curs.execute(sql, val)
                db.commit()
            input_dict6 = input_dictr["cakeinr"]
            if input_dict6["base_unit"] == "cake":
                open_p6 = input_dict6["open"]
                high6 = input_dict6["high"]
                low6 = input_dict6["low"]
                close6 = input_dict6["last"]
                # volume = input_dict[""]
                timestamp6 = input_dict6["at"]
                symbol6 = input_dict6["base_unit"]
                totaltradebaseasset6 = input_dict6["volume"]

                # ! check datetime convert store formate
                timestamnewapi6 = datetime.datetime.fromtimestamp(timestamp6 / 1e3)
                timestamnewapi6 = timestamnewapi6.strftime("%Y-%m-%d %H:%M:%S %p")

                sql = "INSERT INTO 	efx_percentage_change_cakeinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                val = (
                    open_p6,
                    close6,
                    high6,
                    low6,
                    symbol6,
                    totaltradebaseasset6,
                    timestamnewapi6,
                    calltime,
                )
                curs.execute(sql, val)
                db.commit()
            input_dict7 = input_dictr["compinr"]
            if input_dict7["base_unit"] == "comp":
                open_p7 = input_dict7["open"]
                high7 = input_dict7["high"]
                low7 = input_dict7["low"]
                close7 = input_dict7["last"]
                # volume = input_dict[""]
                timestamp7 = input_dict7["at"]
                symbol7 = input_dict7["base_unit"]
                totaltradebaseasset7 = input_dict7["volume"]

                # ! check datetime convert store formate
                timestamnewapi7 = datetime.datetime.fromtimestamp(timestamp7 / 1e3)
                timestamnewapi7 = timestamnewapi7.strftime("%Y-%m-%d %H:%M:%S %p")

                sql = "INSERT INTO 	efx_percentage_change_compinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                val = (
                    open_p7,
                    close7,
                    high7,
                    low7,
                    symbol7,
                    totaltradebaseasset7,
                    timestamnewapi7,
                    calltime,
                )
                curs.execute(sql, val)
                db.commit()
            input_dict8 = input_dictr["crvinr"]
            if input_dict8["base_unit"] == "crv":
                open_p8 = input_dict8["open"]
                high8 = input_dict8["high"]
                low8 = input_dict8["low"]
                close8 = input_dict8["last"]
                # volume = input_dict[""]
                timestamp8 = input_dict8["at"]
                symbol8 = input_dict8["base_unit"]
                totaltradebaseasset8 = input_dict8["volume"]

                # ! check datetime convert store formate
                timestamnewapi8 = datetime.datetime.fromtimestamp(timestamp8 / 1e3)
                timestamnewapi8 = timestamnewapi8.strftime("%Y-%m-%d %H:%M:%S %p")

                sql = "INSERT INTO 	efx_percentage_change_crvinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                val = (
                    open_p8,
                    close8,
                    high8,
                    low8,
                    symbol8,
                    totaltradebaseasset8,
                    timestamnewapi8,
                    calltime,
                )
                curs.execute(sql, val)
                db.commit()
            input_dict9 = input_dictr["celrinr"]
            if input_dict9["base_unit"] == "celr":
                open_p9 = input_dict9["open"]
                high9 = input_dict9["high"]
                low9 = input_dict9["low"]
                close9 = input_dict9["last"]
                # volume = input_dict[""]
                timestamp9 = input_dict9["at"]
                symbol9 = input_dict9["base_unit"]
                totaltradebaseasset9 = input_dict9["volume"]

                # ! check datetime convert store formate
                timestamnewapi9 = datetime.datetime.fromtimestamp(timestamp9 / 1e3)
                timestamnewapi9 = timestamnewapi9.strftime("%Y-%m-%d %H:%M:%S %p")

                sql = "INSERT INTO 	efx_percentage_change_celrinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                val = (
                    open_p9,
                    close9,
                    high9,
                    low9,
                    symbol9,
                    totaltradebaseasset9,
                    timestamnewapi9,
                    calltime,
                )
                curs.execute(sql, val)
                db.commit()
            input_dict10 = input_dictr["dashinr"]
            if input_dict10["base_unit"] == "dash":
                open_p10 = input_dict10["open"]
                high10 = input_dict10["high"]
                low10 = input_dict10["low"]
                close10 = input_dict10["last"]
                # volume = input_dict[""]
                timestamp10 = input_dict10["at"]
                symbol10 = input_dict10["base_unit"]
                totaltradebaseasset10 = input_dict10["volume"]

                # ! check datetime convert store formate
                timestamnewapi10 = datetime.datetime.fromtimestamp(timestamp10 / 1e3)
                timestamnewapi10 = timestamnewapi10.strftime("%Y-%m-%d %H:%M:%S %p")

                sql = "INSERT INTO 	efx_percentage_change_dashinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                val = (
                    open_p10,
                    close10,
                    high10,
                    low10,
                    symbol10,
                    totaltradebaseasset10,
                    timestamnewapi10,
                    calltime,
                )
                curs.execute(sql, val)
                db.commit()
            input_dict11 = input_dictr["dotinr"]
            if input_dict11["base_unit"] == "dot":
                open_p11 = input_dict11["open"]
                high11 = input_dict11["high"]
                low11 = input_dict11["low"]
                close11 = input_dict11["last"]
                # volume = input_dict[""]
                timestamp11 = input_dict11["at"]
                symbol11 = input_dict11["base_unit"]
                totaltradebaseasset11 = input_dict11["volume"]

                # ! check datetime convert store formate
                timestamnewapi11 = datetime.datetime.fromtimestamp(timestamp11 / 1e3)
                timestamnewapi11 = timestamnewapi11.strftime("%Y-%m-%d %H:%M:%S %p")

                sql = "INSERT INTO 	efx_percentage_change_dotinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                val = (
                    open_p11,
                    close11,
                    high11,
                    low11,
                    symbol11,
                    totaltradebaseasset11,
                    timestamnewapi11,
                    calltime,
                )
                curs.execute(sql, val)
                db.commit()
            input_dict12 = input_dictr["eosinr"]
            if input_dict12["base_unit"] == "eos":
                open_p12 = input_dict12["open"]
                high12 = input_dict12["high"]
                low12 = input_dict12["low"]
                close12 = input_dict12["last"]
                # volume = input_dict[""]
                timestamp12 = input_dict12["at"]
                symbol12 = input_dict12["base_unit"]
                totaltradebaseasset12 = input_dict12["volume"]

                # ! check datetime convert store formate
                timestamnewapi12 = datetime.datetime.fromtimestamp(timestamp12 / 1e3)
                timestamnewapi12 = timestamnewapi12.strftime("%Y-%m-%d %H:%M:%S %p")

                sql = "INSERT INTO 	efx_percentage_change_eosinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                val = (
                    open_p12,
                    close12,
                    high12,
                    low12,
                    symbol12,
                    totaltradebaseasset12,
                    timestamnewapi12,
                    calltime,
                )
                curs.execute(sql, val)
                db.commit()
            input_dict13 = input_dictr["ethinr"]
            if input_dict13["base_unit"] == "eth":
                open_p13 = input_dict13["open"]
                high13 = input_dict13["high"]
                low13 = input_dict13["low"]
                close13 = input_dict13["last"]
                # volume = input_dict[""]
                timestamp13 = input_dict13["at"]
                symbol13 = input_dict13["base_unit"]
                totaltradebaseasset13 = input_dict13["volume"]

                # ! check datetime convert store formate
                timestamnewapi13 = datetime.datetime.fromtimestamp(timestamp13 / 1e3)
                timestamnewapi13 = timestamnewapi13.strftime("%Y-%m-%d %H:%M:%S %p")

                sql = "INSERT INTO 	efx_percentage_change_ethinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                val = (
                    open_p13,
                    close13,
                    high13,
                    low13,
                    symbol13,
                    totaltradebaseasset13,
                    timestamnewapi13,
                    calltime,
                )
                curs.execute(sql, val)
                db.commit()
            input_dict14 = input_dictr["etcinr"]
            if input_dict14["base_unit"] == "etc":
                open_p14 = input_dict14["open"]
                high14 = input_dict14["high"]
                low14 = input_dict14["low"]
                close14 = input_dict14["last"]
                # volume = input_dict[""]
                timestamp14 = input_dict14["at"]
                symbol14 = input_dict14["base_unit"]
                totaltradebaseasset14 = input_dict14["volume"]

                # ! check datetime convert store formate
                timestamnewapi14 = datetime.datetime.fromtimestamp(timestamp14 / 1e3)
                timestamnewapi14 = timestamnewapi14.strftime("%Y-%m-%d %H:%M:%S %p")

                sql = "INSERT INTO 	efx_percentage_change_etcinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                val = (
                    open_p14,
                    close14,
                    high14,
                    low14,
                    symbol14,
                    totaltradebaseasset14,
                    timestamnewapi14,
                    calltime,
                )
                curs.execute(sql, val)
                db.commit()
            input_dict15 = input_dictr["filinr"]
            if input_dict15["base_unit"] == "fil":
                open_p15 = input_dict15["open"]
                high15 = input_dict15["high"]
                low15 = input_dict15["low"]
                close15 = input_dict15["last"]
                # volume = input_dict[""]
                timestamp15 = input_dict15["at"]
                symbol15 = input_dict15["base_unit"]
                totaltradebaseasset15 = input_dict15["volume"]

                # ! check datetime convert store formate
                timestamnewapi15 = datetime.datetime.fromtimestamp(timestamp15 / 1e3)
                timestamnewapi15 = timestamnewapi15.strftime("%Y-%m-%d %H:%M:%S %p")

                sql = "INSERT INTO 	efx_percentage_change_filinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                val = (
                    open_p15,
                    close15,
                    high15,
                    low15,
                    symbol15,
                    totaltradebaseasset15,
                    timestamnewapi15,
                    calltime,
                )
                curs.execute(sql, val)
                db.commit()
            input_dict16 = input_dictr["fttinr"]
            if input_dict16["base_unit"] == "ftt":
                open_p16 = input_dict16["open"]
                high16 = input_dict16["high"]
                low16 = input_dict16["low"]
                close16 = input_dict16["last"]
                # volume = input_dict[""]
                timestamp16 = input_dict16["at"]
                symbol16 = input_dict16["base_unit"]
                totaltradebaseasset16 = input_dict16["volume"]

                # ! check datetime convert store formate
                timestamnewapi16 = datetime.datetime.fromtimestamp(timestamp16 / 1e3)
                timestamnewapi16 = timestamnewapi16.strftime("%Y-%m-%d %H:%M:%S %p")

                sql = "INSERT INTO 	efx_percentage_change_fttinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                val = (
                    open_p16,
                    close16,
                    high16,
                    low16,
                    symbol16,
                    totaltradebaseasset16,
                    timestamnewapi16,
                    calltime,
                )
                curs.execute(sql, val)
                db.commit()
            input_dict17 = input_dictr["iostinr"]
            if input_dict17["base_unit"] == "iost":
                open_p17 = input_dict17["open"]
                high17 = input_dict17["high"]
                low17 = input_dict17["low"]
                close17 = input_dict17["last"]
                # volume = input_dict[""]
                timestamp17 = input_dict17["at"]
                symbol17 = input_dict17["base_unit"]
                totaltradebaseasset17 = input_dict17["volume"]

                # ! check datetime convert store formate
                timestamnewapi17 = datetime.datetime.fromtimestamp(timestamp17 / 1e3)
                timestamnewapi17 = timestamnewapi17.strftime("%Y-%m-%d %H:%M:%S %p")

                sql = "INSERT INTO 	efx_percentage_change_iostinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                val = (
                    open_p17,
                    close17,
                    high17,
                    low17,
                    symbol17,
                    totaltradebaseasset17,
                    timestamnewapi17,
                    calltime,
                )
                curs.execute(sql, val)
                db.commit()
            input_dict18 = input_dictr["linkinr"]
            if input_dict18["base_unit"] == "link":
                open_p18 = input_dict18["open"]
                high18 = input_dict18["high"]
                low18 = input_dict18["low"]
                close18 = input_dict18["last"]
                # volume = input_dict[""]
                timestamp18 = input_dict18["at"]
                symbol18 = input_dict18["base_unit"]
                totaltradebaseasset18 = input_dict18["volume"]

                # ! check datetime convert store formate
                timestamnewapi18 = datetime.datetime.fromtimestamp(timestamp18 / 1e3)
                timestamnewapi18 = timestamnewapi18.strftime("%Y-%m-%d %H:%M:%S %p")

                sql = "INSERT INTO 	efx_percentage_change_linkinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                val = (
                    open_p18,
                    close18,
                    high18,
                    low18,
                    symbol18,
                    totaltradebaseasset18,
                    timestamnewapi18,
                    calltime,
                )
                curs.execute(sql, val)
                db.commit()
            input_dict19 = input_dictr["ltcinr"]
            if input_dict19["base_unit"] == "ltc":
                open_p19 = input_dict19["open"]
                high19 = input_dict19["high"]
                low19 = input_dict19["low"]
                close19 = input_dict19["last"]
                # volume = input_dict[""]
                timestamp19 = input_dict19["at"]
                symbol19 = input_dict19["base_unit"]
                totaltradebaseasset19 = input_dict19["volume"]

                # ! check datetime convert store formate
                timestamnewapi19 = datetime.datetime.fromtimestamp(timestamp19 / 1e3)
                timestamnewapi19 = timestamnewapi19.strftime("%Y-%m-%d %H:%M:%S %p")

                sql = "INSERT INTO 	efx_percentage_change_ltcinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                val = (
                    open_p19,
                    close19,
                    high19,
                    low19,
                    symbol19,
                    totaltradebaseasset19,
                    timestamnewapi19,
                    calltime,
                )
                curs.execute(sql, val)
                db.commit()
            input_dict20 = input_dictr["manainr"]
            if input_dict20["base_unit"] == "mana":
                open_p20 = input_dict20["open"]
                high20 = input_dict20["high"]
                low20 = input_dict20["low"]
                close20 = input_dict20["last"]
                # volume = input_dict[""]
                timestamp20 = input_dict20["at"]
                symbol20 = input_dict20["base_unit"]
                totaltradebaseasset20 = input_dict20["volume"]

                # ! check datetime convert store formate
                timestamnewapi20 = datetime.datetime.fromtimestamp(timestamp20 / 1e3)
                timestamnewapi20 = timestamnewapi20.strftime("%Y-%m-%d %H:%M:%S %p")

                sql = "INSERT INTO 	efx_percentage_change_manainr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                val = (
                    open_p20,
                    close20,
                    high20,
                    low20,
                    symbol20,
                    totaltradebaseasset20,
                    timestamnewapi20,
                    calltime,
                )
                curs.execute(sql, val)
                db.commit()
            input_dict21 = input_dictr["mdxinr"]
            if input_dict21["base_unit"] == "mdx":
                open_p21 = input_dict21["open"]
                high21 = input_dict21["high"]
                low21 = input_dict21["low"]
                close21 = input_dict21["last"]
                # volume = input_dict[""]
                timestamp21 = input_dict21["at"]
                symbol21 = input_dict21["base_unit"]
                totaltradebaseasset21 = input_dict21["volume"]

                # ! check datetime convert store formate
                timestamnewapi21 = datetime.datetime.fromtimestamp(timestamp21 / 1e3)
                timestamnewapi21 = timestamnewapi21.strftime("%Y-%m-%d %H:%M:%S %p")

                sql = "INSERT INTO 	efx_percentage_change_mdxinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                val = (
                    open_p21,
                    close21,
                    high21,
                    low21,
                    symbol21,
                    totaltradebaseasset21,
                    timestamnewapi21,
                    calltime,
                )
                curs.execute(sql, val)
                db.commit()
            input_dict22 = input_dictr["omginr"]
            if input_dict22["base_unit"] == "omg":
                open_p22 = input_dict22["open"]
                high22 = input_dict22["high"]
                low22 = input_dict22["low"]
                close22 = input_dict22["last"]
                # volume = input_dict[""]
                timestamp22 = input_dict22["at"]
                symbol22 = input_dict22["base_unit"]
                totaltradebaseasset22 = input_dict22["volume"]

                # ! check datetime convert store formate
                timestamnewapi22 = datetime.datetime.fromtimestamp(timestamp22 / 1e3)
                timestamnewapi22 = timestamnewapi22.strftime("%Y-%m-%d %H:%M:%S %p")

                sql = "INSERT INTO 	efx_percentage_change_omginr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                val = (
                    open_p22,
                    close22,
                    high22,
                    low22,
                    symbol22,
                    totaltradebaseasset22,
                    timestamnewapi22,
                    calltime,
                )
                curs.execute(sql, val)
                db.commit()
            input_dict23 = input_dictr["sushiinr"]
            if input_dict23["base_unit"] == "sushi":
                open_p23 = input_dict23["open"]
                high23 = input_dict23["high"]
                low23 = input_dict23["low"]
                close23 = input_dict23["last"]
                # volume = input_dict[""]
                timestamp23 = input_dict23["at"]
                symbol23 = input_dict23["base_unit"]
                totaltradebaseasset23 = input_dict23["volume"]

                # ! check datetime convert store formate
                timestamnewapi23 = datetime.datetime.fromtimestamp(timestamp23 / 1e3)
                timestamnewapi23 = timestamnewapi23.strftime("%Y-%m-%d %H:%M:%S %p")

                sql = "INSERT INTO 	efx_percentage_change_sushiinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                val = (
                    open_p23,
                    close23,
                    high23,
                    low23,
                    symbol23,
                    totaltradebaseasset23,
                    timestamnewapi23,
                    calltime,
                )
                curs.execute(sql, val)
                db.commit()
            input_dict24 = input_dictr["trxinr"]
            if input_dict24["base_unit"] == "trx":
                open_p24 = input_dict24["open"]
                high24 = input_dict24["high"]
                low24 = input_dict24["low"]
                close24 = input_dict24["last"]
                # volume = input_dict[""]
                timestamp24 = input_dict24["at"]
                symbol24 = input_dict24["base_unit"]
                totaltradebaseasset24 = input_dict24["volume"]

                # ! check datetime convert store formate
                timestamnewapi24 = datetime.datetime.fromtimestamp(timestamp24 / 1e3)
                timestamnewapi24 = timestamnewapi24.strftime("%Y-%m-%d %H:%M:%S %p")

                sql = "INSERT INTO 	efx_percentage_change_trxinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                val = (
                    open_p24,
                    close24,
                    high24,
                    low24,
                    symbol24,
                    totaltradebaseasset24,
                    timestamnewapi24,
                    calltime,
                )
                curs.execute(sql, val)
                db.commit()
            input_dict25 = input_dictr["uniinr"]
            if input_dict25["base_unit"] == "uni":
                open_p25 = input_dict25["open"]
                high25 = input_dict25["high"]
                low25 = input_dict25["low"]
                close25 = input_dict25["last"]
                # volume = input_dict[""]
                timestamp25 = input_dict25["at"]
                symbol25 = input_dict25["base_unit"]
                totaltradebaseasset25 = input_dict25["volume"]

                # ! check datetime convert store formate
                timestamnewapi25 = datetime.datetime.fromtimestamp(timestamp25 / 1e3)
                timestamnewapi25 = timestamnewapi25.strftime("%Y-%m-%d %H:%M:%S %p")

                sql = "INSERT INTO 	efx_percentage_change_uniinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                val = (
                    open_p25,
                    close25,
                    high25,
                    low25,
                    symbol25,
                    totaltradebaseasset25,
                    timestamnewapi25,
                    calltime,
                )
                curs.execute(sql, val)
                db.commit()
            input_dict26 = input_dictr["xrpinr"]
            if input_dict26["base_unit"] == "xrp":
                open_p26 = input_dict26["open"]
                high26 = input_dict26["high"]
                low26 = input_dict26["low"]
                close26 = input_dict26["last"]
                # volume = input_dict[""]
                timestamp26 = input_dict26["at"]
                symbol26 = input_dict26["base_unit"]
                totaltradebaseasset26 = input_dict26["volume"]

                # ! check datetime convert store formate
                timestamnewapi26 = datetime.datetime.fromtimestamp(timestamp26 / 1e3)
                timestamnewapi26 = timestamnewapi26.strftime("%Y-%m-%d %H:%M:%S %p")

                sql = "INSERT INTO 	efx_percentage_change_xrpinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                val = (
                    open_p26,
                    close26,
                    high26,
                    low26,
                    symbol26,
                    totaltradebaseasset26,
                    timestamnewapi26,
                    calltime,
                )
                curs.execute(sql, val)
                db.commit()
            input_dict27 = input_dictr["shibinr"]
            if input_dict27["base_unit"] == "shib":
                open_p27 = input_dict27["open"]
                high27 = input_dict27["high"]
                low27 = input_dict27["low"]
                close27 = input_dict27["last"]
                # volume = input_dict[""]
                timestamp27 = input_dict27["at"]
                symbol27 = input_dict27["base_unit"]
                totaltradebaseasset27 = input_dict27["volume"]

                # ! check datetime convert store formate
                timestamnewapi27 = datetime.datetime.fromtimestamp(timestamp27 / 1e3)
                timestamnewapi27 = timestamnewapi27.strftime("%Y-%m-%d %H:%M:%S %p")

                sql = "INSERT INTO 	efx_percentage_change_shibinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                val = (
                    open_p27,
                    close27,
                    high27,
                    low27,
                    symbol27,
                    totaltradebaseasset27,
                    timestamnewapi27,
                    calltime,
                )
                curs.execute(sql, val)
                db.commit()
            input_dict28 = input_dictr["maticinr"]
            if input_dict28["base_unit"] == "matic":
                open_p28 = input_dict28["open"]
                high28 = input_dict28["high"]
                low28 = input_dict28["low"]
                close28 = input_dict28["last"]
                # volume = input_dict[""]
                timestamp28 = input_dict28["at"]
                symbol28 = input_dict28["base_unit"]
                totaltradebaseasset28 = input_dict28["volume"]

                # ! check datetime convert store formate
                timestamnewapi28 = datetime.datetime.fromtimestamp(timestamp28 / 1e3)
                timestamnewapi28 = timestamnewapi28.strftime("%Y-%m-%d %H:%M:%S %p")

                sql = "INSERT INTO 	efx_percentage_change_maticinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                val = (
                    open_p28,
                    close28,
                    high28,
                    low28,
                    symbol28,
                    totaltradebaseasset28,
                    timestamnewapi28,
                    calltime,
                )
                curs.execute(sql, val)
                db.commit()

        except Exception as e:
            print(e)


loop = asyncio.get_event_loop()
try:
    print("start 1st 4")
    asyncio.ensure_future(listen())
    loop.run_forever()
except KeyboardInterrupt:
    pass
finally:
    print("Closing Loop")
    loop.close()
