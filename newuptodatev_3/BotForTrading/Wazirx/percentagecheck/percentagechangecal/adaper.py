from ast import Pass
from unicodedata import decimal
import websockets
import requests
import asyncio
import json
import sys
import datetime
from threading import Timer
from operator import itemgetter
import datetime
from time import process_time
from decimal import *
import time

import MySQLdb.cursors

db = MySQLdb.connect("localhost", "root", "", "xenbot")



async def listen():
    while True:
        await asyncio.sleep(0.1)
        try:
            coin = "adainr"  # four
            # one by one coin select
            x = datetime.datetime.now()
            # ! check datetime
            calltime = x.strftime("%Y-%m-%d %H:%M:%S %p")
            date_sub1secpre = datetime.timedelta(seconds=1)
            dat1secpre = x - date_sub1secpre
            dat1secpre = dat1secpre.strftime("%Y-%m-%d %H:%M:%S %p")
            print(dat1secpre)

            curs1secpre = db.cursor(MySQLdb.cursors.DictCursor)
            sql_checksecpre = "select * from efx_perchange_wz_adainr order by id DESC LIMIT 1"
            curs1secpre.execute(sql_checksecpre)
            oldt1secpre = curs1secpre.fetchall()
            for value in oldt1secpre:
                if value["symbol"] == coin:
                    close = value["close"]
                    # volume = input_dict[""]
                    # !check datatime alreday there
                    date_time = value["timestamp"]
                    oldt = datetime.datetime.strptime(
                        date_time, "%Y-%m-%d %H:%M:%S %p"
                    )
                    date_sub = datetime.timedelta(days=1)
                    dat_uncode = oldt - date_sub
                    dat = dat_uncode.strftime("%Y-%m-%d %H:%M:%S %p")

                    date_sub5moresec = datetime.timedelta(seconds=5)
                    dat5moresec = dat_uncode - date_sub5moresec
                    dat5moresec = dat5moresec.strftime(
                        "%Y-%m-%d %H:%M:%S %p"
                    )

                    curs = db.cursor(MySQLdb.cursors.DictCursor)
                    sql_check1 = "SELECT * from efx_perchange_wz_adainr where (timestamp BETWEEN %s AND %s) ORDER BY id DESC"
                    val_check1 = (dat5moresec, dat)
                    curs.execute(sql_check1, val_check1)
                    oldt1 = curs.fetchall()
                    if oldt1:
                        for newtime1 in oldt1:
                            close_cal = newtime1["close"]
                            diffrence = (
                                Decimal(close) - Decimal(close_cal)
                            ) / Decimal(close)
                            persentage = diffrence * 100
                            formatted_persentage = "{:.2f}".format(persentage)
                            break
                    else:
                        formatted_persentage = 400

                    sql = "UPDATE efx_perchange_wz_adainr SET pricechange=%s WHERE timestamp = %s"
                    val = (
                        formatted_persentage,
                        date_time,
                    )
                    curs.execute(sql, val)
                    db.commit()
                    print(formatted_persentage)
                    print(date_time)
        except Exception as e:
            print(e)


loop = asyncio.get_event_loop()
try:
    print("start 1st 4")
    asyncio.ensure_future(listen())
    loop.run_forever()
except KeyboardInterrupt:
    pass
finally:
    print("Closing Loop")
    loop.close()
