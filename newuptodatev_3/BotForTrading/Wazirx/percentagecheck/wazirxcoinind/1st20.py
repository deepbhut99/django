from unicodedata import decimal
import websockets
import requests
import asyncio
import json
import MySQLdb.cursors
import datetime
from threading import Timer
from operator import itemgetter
import datetime
from time import process_time
from decimal import *

db = MySQLdb.connect("localhost", "root", "", "xenbot")


async def listen():
    url = "https://api.wazirx.com/sapi/v1/tickers/24hr"
    while True:
        await asyncio.sleep(0.60)
        x = datetime.datetime.now()
        # ! check datetime
        calltime = x.strftime("%Y-%m-%d %H:%M:%S %p")
        r = requests.get(url)
        input_dict = r.json()
        curs = db.cursor(MySQLdb.cursors.DictCursor)
        try:
            COINNAME = [
                "fttinr",  # 20
                "iostinr",  # 20
                "linkinr",  # 20
                "ltcinr",  # 20
            ]
            for coin in COINNAME:  # one by one coin select
                for value in input_dict:  # particular api value compare
                    if value["symbol"] == coin:
                        open_p = value["openPrice"]
                        high = value["highPrice"]
                        low = value["lowPrice"]
                        close = value["lastPrice"]
                        # volume = input_dict[""]
                        timestamp = value["at"]
                        symbol = value["symbol"]
                        totaltradebaseasset = value["volume"]

                        # ! check datetime convert store formate
                        timestamnewapi = datetime.datetime.fromtimestamp(
                            timestamp / 1e3
                        )
                        timestamnewapi = timestamnewapi.strftime("%Y-%m-%d %H:%M:%S %p")
                        # !check datatime alreday there
                        curs = db.cursor(MySQLdb.cursors.DictCursor)
                        sql_check = (
                            "SELECT * FROM efx_perchange_wazirx20 where timestamp = %s"
                        )
                        val_check = (calltime,)
                        curs.execute(sql_check, val_check)
                        check_timeupdate = curs.rowcount

                        oldt = (
                            curs.fetchall()
                        )  # fetch data for same date time pre select
                        if oldt:
                            for newtime in oldt:
                                date_time = newtime["timestamp"]
                                oldt = datetime.datetime.strptime(
                                    date_time, "%Y-%m-%d %H:%M:%S %p"
                                )
                                date_sub = datetime.timedelta(days=1)
                                dat = oldt - date_sub
                                dat = dat.strftime("%Y-%m-%d %H:%M:%S %p")
                                curs = db.cursor(MySQLdb.cursors.DictCursor)
                                sql_check1 = "SELECT * FROM efx_perchange_wazirx20 where timestamp = %s"
                                val_check1 = (dat,)
                                curs.execute(sql_check1, val_check1)
                                oldt1 = curs.fetchall()
                                if oldt1:
                                    for newtime1 in oldt1:
                                        nameofcol = "close_" + coin
                                        close_cal = newtime1[nameofcol]
                                        diffrence = (
                                            Decimal(close) - Decimal(close_cal)
                                        ) / Decimal(close)
                                        persentage = diffrence * 100
                                        formatted_persentage = "{:.2f}".format(
                                            persentage
                                        )
                                else:
                                    # if "formatted_persentage" not in locals():
                                    date_sub1moresec = datetime.timedelta(seconds=1)
                                    dat1moresec = oldt - date_sub1moresec
                                    dat1moresec = dat1moresec.strftime(
                                        "%Y-%m-%d %H:%M:%S %p"
                                    )
                                    curs = db.cursor(MySQLdb.cursors.DictCursor)
                                    sql_check1 = "SELECT * FROM efx_perchange_wazirx20 where timestamp = %s"
                                    val_check1 = (dat1moresec,)
                                    curs.execute(sql_check1, val_check1)
                                    oldt1 = curs.fetchall()
                                    for newtime1 in oldt1:
                                        nameofcol = "close_" + coin
                                        close_cal = newtime1[nameofcol]
                                        diffrence = (
                                            Decimal(close) - Decimal(close_cal)
                                        ) / Decimal(close)
                                        persentage = diffrence * 100
                                        formatted_persentage = "{:.2f}".format(
                                            persentage
                                        )
                        else:
                            formatted_persentage = 0
                        if check_timeupdate == 1:
                            sql = "UPDATE efx_perchange_wazirx20 SET open_{column_names} = %s, close_{column_names} =%s, high_{column_names} =%s, low_{column_names} =%s,symbol_{column_names} =%s,volume_{column_names}=%s,pricechange_{column_names}=%s WHERE timestamp = %s"
                            sql = sql.format(column_names=symbol)
                            val = (
                                open_p,
                                close,
                                high,
                                low,
                                symbol,
                                totaltradebaseasset,
                                formatted_persentage,
                                calltime,
                            )
                            curs.execute(sql, val)
                            db.commit()
                        else:
                            sql = "INSERT INTO 	efx_perchange_wazirx20 (open_{column_names} , close_{column_names} , high_{column_names}, low_{column_names},symbol_{column_names},volume_{column_names},pricechange_{column_names},timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                            sql = sql.format(column_names=symbol)
                            val = (
                                open_p,
                                close,
                                high,
                                low,
                                symbol,
                                totaltradebaseasset,
                                formatted_persentage,
                                timestamnewapi,
                                calltime,
                            )
                            curs.execute(sql, val)
                            db.commit()
            print(calltime)
        except Exception as e:
            print(e)


loop = asyncio.get_event_loop()
try:
    print("start 1st 16")
    asyncio.ensure_future(listen())
    loop.run_forever()
except KeyboardInterrupt:
    pass
finally:
    print("Closing Loop")
    loop.close()
