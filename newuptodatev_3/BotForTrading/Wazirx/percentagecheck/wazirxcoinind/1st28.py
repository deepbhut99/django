import sys
sys.path.append('C:/Users/rktyc/Desktop/newuptodatev_2/')

from tokenize import Double
from unicodedata import decimal
from xml.dom.minidom import Document
import websockets
import requests
import asyncio
import json
import MySQLdb.cursors
import datetime
from threading import Timer
from operator import itemgetter
import datetime
from time import process_time
from decimal import *
from mongoengine import *
import  mongo 


class efx_perchange_wz_atominr(Document):
    cid = SequenceField(primary_key=True)
    open = DecimalField()
    high = DecimalField()
    close = DecimalField()
    low = DecimalField()
    timestamp = StringField()
    symbol = StringField()
    volume = DecimalField()
    timestampwz = StringField()
    


async def listen():
    url = "https://api.wazirx.com/sapi/v1/tickers/24hr"
    while True:
        await asyncio.sleep(1)
        x = datetime.datetime.now()
        # ! check datetime
        calltime = x.strftime("%Y-%m-%d %H:%M:%S %p")
        r = requests.get(url)
        input_dict = r.json()
        try:
            for value in input_dict:  # particular api value compare
                if value["symbol"] == "adainr": 
                    open_p = value["openPrice"]
                    high = value["highPrice"]
                    low = value["lowPrice"]
                    close = value["lastPrice"]
                    timestamp = value["at"]
                    symbol = value["symbol"]
                    volume = value["volume"]
                    
                    # ! check datetime convert store formate
                    timestamnewapi = datetime.datetime.fromtimestamp(timestamp / 1e3)
                    timestamnewapi = timestamnewapi.strftime("%Y-%m-%d %H:%M:%S %p")
                    
                    anainr = efx_perchange_wz_atominr()
                    anainr.open = open_p
                    anainr.high = high
                    anainr.low = low
                    anainr.close = close
                    anainr.symbol = symbol
                    anainr.volume = volume
                    anainr.timestamp = timestamnewapi
                    anainr.timestampwz = calltime
                    anainr.save()

                    
                    
            

            print(calltime)
        except Exception as e:
            print(e)


loop = asyncio.get_event_loop()
try:
    print("start 1st 4")
    asyncio.ensure_future(listen())
    loop.run_forever()
except KeyboardInterrupt:
    pass
finally:
    print("Closing Loop")
    loop.close()
