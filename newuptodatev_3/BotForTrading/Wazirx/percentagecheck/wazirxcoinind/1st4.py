from unicodedata import decimal
import websockets
import requests
import asyncio
import json
import MySQLdb.cursors
import datetime
from threading import Timer
from operator import itemgetter
import datetime
from time import process_time
from decimal import *

db = MySQLdb.connect("localhost", "root", "", "xenbot")


async def listen():
    while True:
        await asyncio.sleep(0.20)
        try:
            x = datetime.datetime.now()
            # ! check datetime
            calltime = x.strftime("%Y-%m-%d %H:%M:%S %p")
            date_sub1secpre = datetime.timedelta(seconds=1)
            dat1secpre = x - date_sub1secpre
            dat1secpre = dat1secpre.strftime("%Y-%m-%d %H:%M:%S %p")
            print(dat1secpre)
            # adainr
            curs1secpre_adainr = db.cursor(MySQLdb.cursors.DictCursor)
            sql_checksecpre_adainr = (
                "select * from efx_perchange_wz_adainr order by id DESC LIMIT 1"
            )
            curs1secpre_adainr.execute(sql_checksecpre_adainr)
            oldt1secpre_adainr = curs1secpre_adainr.fetchall()
            for (
                value_adainr
            ) in oldt1secpre_adainr:  # particular api value_adainr compare
                close_adainr = value_adainr["close"]
                # volume = input_dict[""]
                # !check datatime alreday there
                date_time_adainr = value_adainr["timestamp"]
                oldt_adainr = datetime.datetime.strptime(
                    date_time_adainr, "%Y-%m-%d %H:%M:%S %p"
                )
                date_sub_adainr = datetime.timedelta(days=1)
                datuncode_adainr = oldt_adainr - date_sub_adainr
                dat_adainr = datuncode_adainr.strftime("%Y-%m-%d %H:%M:%S %p")

                curs_adainr = db.cursor(MySQLdb.cursors.DictCursor)
                sqlcheck1_adainr = (
                    "SELECT * FROM efx_perchange_wz_adainr where timestamp = %s"
                )
                valcheck1_adainr = (dat_adainr,)
                curs_adainr.execute(sqlcheck1_adainr, valcheck1_adainr)

                oldt1_adainr = curs_adainr.fetchall()
                if oldt1_adainr:
                    for newtime1_adainr in oldt1_adainr:
                        closecal_adainr = newtime1_adainr["close"]
                        diffrence_adainr = (
                            Decimal(close_adainr) - Decimal(closecal_adainr)
                        ) / Decimal(close_adainr)
                        persentage_adainr = diffrence_adainr * 100
                        formdpers_adainr = "{:.2f}".format(persentage_adainr)
                else:
                    # if "formdpers_adainr" not in locals():
                    onemoresec_adainr = datetime.timedelta(seconds=1)
                    dat1moresec_adainr = datuncode_adainr - onemoresec_adainr
                    dat1moresec_adainr = dat1moresec_adainr.strftime(
                        "%Y-%m-%d %H:%M:%S %p"
                    )
                    curs1sec_adainr = db.cursor(MySQLdb.cursors.DictCursor)
                    sqlchecksec1_adainr = (
                        "SELECT * FROM efx_perchange_wz_adainr where timestamp = %s"
                    )

                    valchecksec1_adainr = (dat1moresec_adainr,)
                    curs1sec_adainr.execute(sqlchecksec1_adainr, valchecksec1_adainr)
                    oldt1sec_adainr = curs1sec_adainr.fetchall()
                    if oldt1sec_adainr:
                        for newtime1sec_adainr in oldt1sec_adainr:
                            closecal_adainr = newtime1sec_adainr["close"]
                            diffrence_adainr = (
                                Decimal(close_adainr) - Decimal(closecal_adainr)
                            ) / Decimal(close_adainr)
                            persentage_adainr = diffrence_adainr * 100
                            formdpers_adainr = "{:.2f}".format(persentage_adainr)
                    else:
                        # if "formdpers_adainr" not in locals():
                        date_sub2moresec = datetime.timedelta(seconds=2)
                        dat2moresec_adainr = datuncode_adainr - date_sub2moresec
                        dat2moresec_adainr = dat2moresec_adainr.strftime(
                            "%Y-%m-%d %H:%M:%S %p"
                        )
                        curs2sec_adainr = db.cursor(MySQLdb.cursors.DictCursor)
                        sqlchecksec2_adainr = (
                            "SELECT * FROM efx_perchange_wz_adainr where timestamp = %s"
                        )
                        valchecksec2_adainr = (dat2moresec_adainr,)
                        curs2sec_adainr.execute(
                            sqlchecksec2_adainr, valchecksec2_adainr
                        )
                        oldt2sec_adainr = curs2sec_adainr.fetchall()
                        if oldt2sec_adainr:
                            for newtime2sec_adainr in oldt2sec_adainr:
                                closecal_adainr = newtime2sec_adainr["close"]
                                diffrence_adainr = (
                                    Decimal(close_adainr) - Decimal(closecal_adainr)
                                ) / Decimal(close_adainr)
                                persentage_adainr = diffrence_adainr * 100
                                formdpers_adainr = "{:.2f}".format(persentage_adainr)
                        else:
                            # if "formdpers_adainr" not in locals():
                            dasub3sec_adainr = datetime.timedelta(seconds=3)
                            dat3moresec_adainr = datuncode_adainr - dasub3sec_adainr
                            dat3moresec_adainr = dat3moresec_adainr.strftime(
                                "%Y-%m-%d %H:%M:%S %p"
                            )
                            curs3sec_adainr = db.cursor(MySQLdb.cursors.DictCursor)
                            sqlcksec3_adainr = "SELECT * FROM efx_perchange_wz_adainr where timestamp = %s"

                            valcksec3_adainr = (dat3moresec_adainr,)
                            curs3sec_adainr.execute(sqlcksec3_adainr, valcksec3_adainr)
                            oldt3sec_adainr = curs3sec_adainr.fetchall()
                            if oldt3sec_adainr:
                                for newtime3sec_adainr in oldt3sec_adainr:

                                    closecal_adainr = newtime3sec_adainr["close"]
                                    diffrence_adainr = (
                                        Decimal(close_adainr) - Decimal(closecal_adainr)
                                    ) / Decimal(close_adainr)
                                    persentage_adainr = diffrence_adainr * 100
                                    formdpers_adainr = "{:.2f}".format(
                                        persentage_adainr
                                    )
                            else:
                                # if "formdpers_adainr" not in locals():
                                datesub4sec_adainr = datetime.timedelta(seconds=4)
                                dat4moresec_adainr = (
                                    datuncode_adainr - datesub4sec_adainr
                                )
                                dat4moresec_adainr = dat4moresec_adainr.strftime(
                                    "%Y-%m-%d %H:%M:%S %p"
                                )
                                curs4sec_adainr = db.cursor(MySQLdb.cursors.DictCursor)
                                sqlcksec4_adainr = "SELECT * FROM efx_perchange_wz_adainr where timestamp = %s"

                                valcksec4_adainr = (dat4moresec_adainr,)
                                curs4sec_adainr.execute(
                                    sqlcksec4_adainr, valcksec4_adainr
                                )
                                oldt4sec_adainr = curs4sec_adainr.fetchall()
                                if oldt4sec_adainr:
                                    for newtime4sec_adainr in oldt4sec_adainr:

                                        closecal_adainr = newtime4sec_adainr["close"]
                                        diffrence_adainr = (
                                            Decimal(close_adainr)
                                            - Decimal(closecal_adainr)
                                        ) / Decimal(close_adainr)
                                        persentage_adainr = diffrence_adainr * 100
                                        formdpers_adainr = "{:.2f}".format(
                                            persentage_adainr
                                        )
                                else:
                                    # if "formdpers_adainr" not in locals():
                                    datesub5sec_adainr = datetime.timedelta(seconds=5)
                                    dat5moresec_adainr = (
                                        datuncode_adainr - datesub5sec_adainr
                                    )
                                    dat5moresec_adainr = dat5moresec_adainr.strftime(
                                        "%Y-%m-%d %H:%M:%S %p"
                                    )
                                    curs5sec_adainr = db.cursor(
                                        MySQLdb.cursors.DictCursor
                                    )
                                    sqlcksec5_adainr = "SELECT * FROM efx_perchange_wz_adainr where timestamp = %s"

                                    valcksec5_adainr = (dat5moresec_adainr,)
                                    curs5sec_adainr.execute(
                                        sqlcksec5_adainr, valcksec5_adainr
                                    )
                                    oldt5sec_adainr = curs5sec_adainr.fetchall()
                                    if oldt5sec_adainr:
                                        for newtime5sec in oldt5sec_adainr:
                                            closecal_adainr = newtime5sec["close"]
                                            diffrence_adainr = (
                                                Decimal(close_adainr)
                                                - Decimal(closecal_adainr)
                                            ) / Decimal(close_adainr)
                                            persentage_adainr = diffrence_adainr * 100
                                            formdpers_adainr = "{:.2f}".format(
                                                persentage_adainr
                                            )
                                    else:
                                        formdpers_adainr = 100

                sql_adainr = "UPDATE efx_perchange_wz_adainr SET pricechange=%s WHERE timestamp = %s"
                val_adainr = (
                    formdpers_adainr,
                    date_time_adainr,
                )
                curs_adainr.execute(sql_adainr, val_adainr)
                db.commit()

            # atominr
            curs1secpre_atominr = db.cursor(MySQLdb.cursors.DictCursor)
            sql_checksecpre_atominr = (
                "select * from efx_perchange_wz_atominr order by id DESC LIMIT 1"
            )
            curs1secpre_atominr.execute(sql_checksecpre_atominr)
            oldt1secpre_atominr = curs1secpre_atominr.fetchall()
            for (
                value_atominr
            ) in oldt1secpre_atominr:  # particular api value_atominr compare
                close_atominr = value_atominr["close"]
                # volume = input_dict[""]
                # !check datatime alreday there
                date_time_atominr = value_atominr["timestamp"]
                oldt_atominr = datetime.datetime.strptime(
                    date_time_atominr, "%Y-%m-%d %H:%M:%S %p"
                )
                date_sub_atominr = datetime.timedelta(days=1)
                datuncode_atominr = oldt_atominr - date_sub_atominr
                dat_atominr = datuncode_atominr.strftime("%Y-%m-%d %H:%M:%S %p")

                curs_atominr = db.cursor(MySQLdb.cursors.DictCursor)
                sqlcheck1_atominr = (
                    "SELECT * FROM efx_perchange_wz_atominr where timestamp = %s"
                )
                valcheck1_atominr = (dat_atominr,)
                curs_atominr.execute(sqlcheck1_atominr, valcheck1_atominr)

                oldt1_atominr = curs_atominr.fetchall()
                if oldt1_atominr:
                    for newtime1_atominr in oldt1_atominr:
                        closecal_atominr = newtime1_atominr["close"]
                        diffrence_atominr = (
                            Decimal(close_atominr) - Decimal(closecal_atominr)
                        ) / Decimal(close_atominr)
                        persentage_atominr = diffrence_atominr * 100
                        formdpers_atominr = "{:.2f}".format(persentage_atominr)
                else:
                    # if "formdpers_atominr" not in locals():
                    onemoresec_atominr = datetime.timedelta(seconds=1)
                    dat1moresec_atominr = datuncode_atominr - onemoresec_atominr
                    dat1moresec_atominr = dat1moresec_atominr.strftime(
                        "%Y-%m-%d %H:%M:%S %p"
                    )
                    curs1sec_atominr = db.cursor(MySQLdb.cursors.DictCursor)
                    sqlchecksec1_atominr = (
                        "SELECT * FROM efx_perchange_wz_atominr where timestamp = %s"
                    )

                    valchecksec1_atominr = (dat1moresec_atominr,)
                    curs1sec_atominr.execute(sqlchecksec1_atominr, valchecksec1_atominr)
                    oldt1sec_atominr = curs1sec_atominr.fetchall()
                    if oldt1sec_atominr:
                        for newtime1sec_atominr in oldt1sec_atominr:
                            closecal_atominr = newtime1sec_atominr["close"]
                            diffrence_atominr = (
                                Decimal(close_atominr) - Decimal(closecal_atominr)
                            ) / Decimal(close_atominr)
                            persentage_atominr = diffrence_atominr * 100
                            formdpers_atominr = "{:.2f}".format(persentage_atominr)
                    else:
                        # if "formdpers_atominr" not in locals():
                        date_sub2moresec = datetime.timedelta(seconds=2)
                        dat2moresec_atominr = datuncode_atominr - date_sub2moresec
                        dat2moresec_atominr = dat2moresec_atominr.strftime(
                            "%Y-%m-%d %H:%M:%S %p"
                        )
                        curs2sec_atominr = db.cursor(MySQLdb.cursors.DictCursor)
                        sqlchecksec2_atominr = "SELECT * FROM efx_perchange_wz_atominr where timestamp = %s"
                        valchecksec2_atominr = (dat2moresec_atominr,)
                        curs2sec_atominr.execute(
                            sqlchecksec2_atominr, valchecksec2_atominr
                        )
                        oldt2sec_atominr = curs2sec_atominr.fetchall()
                        if oldt2sec_atominr:
                            for newtime2sec_atominr in oldt2sec_atominr:
                                closecal_atominr = newtime2sec_atominr["close"]
                                diffrence_atominr = (
                                    Decimal(close_atominr) - Decimal(closecal_atominr)
                                ) / Decimal(close_atominr)
                                persentage_atominr = diffrence_atominr * 100
                                formdpers_atominr = "{:.2f}".format(persentage_atominr)
                        else:
                            # if "formdpers_atominr" not in locals():
                            dasub3sec_atominr = datetime.timedelta(seconds=3)
                            dat3moresec_atominr = datuncode_atominr - dasub3sec_atominr
                            dat3moresec_atominr = dat3moresec_atominr.strftime(
                                "%Y-%m-%d %H:%M:%S %p"
                            )
                            curs3sec_atominr = db.cursor(MySQLdb.cursors.DictCursor)
                            sqlcksec3_atominr = "SELECT * FROM efx_perchange_wz_atominr where timestamp = %s"

                            valcksec3_atominr = (dat3moresec_atominr,)
                            curs3sec_atominr.execute(
                                sqlcksec3_atominr, valcksec3_atominr
                            )
                            oldt3sec_atominr = curs3sec_atominr.fetchall()
                            if oldt3sec_atominr:
                                for newtime3sec_atominr in oldt3sec_atominr:

                                    closecal_atominr = newtime3sec_atominr["close"]
                                    diffrence_atominr = (
                                        Decimal(close_atominr)
                                        - Decimal(closecal_atominr)
                                    ) / Decimal(close_atominr)
                                    persentage_atominr = diffrence_atominr * 100
                                    formdpers_atominr = "{:.2f}".format(
                                        persentage_atominr
                                    )
                            else:
                                # if "formdpers_atominr" not in locals():
                                datesub4sec_atominr = datetime.timedelta(seconds=4)
                                dat4moresec_atominr = (
                                    datuncode_atominr - datesub4sec_atominr
                                )
                                dat4moresec_atominr = dat4moresec_atominr.strftime(
                                    "%Y-%m-%d %H:%M:%S %p"
                                )
                                curs4sec_atominr = db.cursor(MySQLdb.cursors.DictCursor)
                                sqlcksec4_atominr = "SELECT * FROM efx_perchange_wz_atominr where timestamp = %s"

                                valcksec4_atominr = (dat4moresec_atominr,)
                                curs4sec_atominr.execute(
                                    sqlcksec4_atominr, valcksec4_atominr
                                )
                                oldt4sec_atominr = curs4sec_atominr.fetchall()
                                if oldt4sec_atominr:
                                    for newtime4sec_atominr in oldt4sec_atominr:

                                        closecal_atominr = newtime4sec_atominr["close"]
                                        diffrence_atominr = (
                                            Decimal(close_atominr)
                                            - Decimal(closecal_atominr)
                                        ) / Decimal(close_atominr)
                                        persentage_atominr = diffrence_atominr * 100
                                        formdpers_atominr = "{:.2f}".format(
                                            persentage_atominr
                                        )
                                else:
                                    # if "formdpers_atominr" not in locals():
                                    datesub5sec_atominr = datetime.timedelta(seconds=5)
                                    dat5moresec_atominr = (
                                        datuncode_atominr - datesub5sec_atominr
                                    )
                                    dat5moresec_atominr = dat5moresec_atominr.strftime(
                                        "%Y-%m-%d %H:%M:%S %p"
                                    )
                                    curs5sec_atominr = db.cursor(
                                        MySQLdb.cursors.DictCursor
                                    )
                                    sqlcksec5_atominr = "SELECT * FROM efx_perchange_wz_atominr where timestamp = %s"

                                    valcksec5_atominr = (dat5moresec_atominr,)
                                    curs5sec_atominr.execute(
                                        sqlcksec5_atominr, valcksec5_atominr
                                    )
                                    oldt5sec_atominr = curs5sec_atominr.fetchall()
                                    if oldt5sec_atominr:
                                        for newtime5sec in oldt5sec_atominr:
                                            closecal_atominr = newtime5sec["close"]
                                            diffrence_atominr = (
                                                Decimal(close_atominr)
                                                - Decimal(closecal_atominr)
                                            ) / Decimal(close_atominr)
                                            persentage_atominr = diffrence_atominr * 100
                                            formdpers_atominr = "{:.2f}".format(
                                                persentage_atominr
                                            )
                                    else:
                                        formdpers_atominr = 100

                sql_atominr = "UPDATE efx_perchange_wz_atominr SET pricechange=%s WHERE timestamp = %s"
                val_atominr = (
                    formdpers_atominr,
                    date_time_atominr,
                )
                curs_atominr.execute(sql_atominr, val_atominr)
                db.commit()

            # batinr
            curs1secpre_batinr = db.cursor(MySQLdb.cursors.DictCursor)
            sql_checksecpre_batinr = (
                "select * from efx_perchange_wz_batinr order by id DESC LIMIT 1"
            )
            curs1secpre_batinr.execute(sql_checksecpre_batinr)
            oldt1secpre_batinr = curs1secpre_batinr.fetchall()
            for (
                value_batinr
            ) in oldt1secpre_batinr:  # particular api value_batinr compare
                close_batinr = value_batinr["close"]
                # volume = input_dict[""]
                # !check datatime alreday there
                date_time_batinr = value_batinr["timestamp"]
                oldt_batinr = datetime.datetime.strptime(
                    date_time_batinr, "%Y-%m-%d %H:%M:%S %p"
                )
                date_sub_batinr = datetime.timedelta(days=1)
                datuncode_batinr = oldt_batinr - date_sub_batinr
                dat_batinr = datuncode_batinr.strftime("%Y-%m-%d %H:%M:%S %p")

                curs_batinr = db.cursor(MySQLdb.cursors.DictCursor)
                sqlcheck1_batinr = (
                    "SELECT * FROM efx_perchange_wz_batinr where timestamp = %s"
                )
                valcheck1_batinr = (dat_batinr,)
                curs_batinr.execute(sqlcheck1_batinr, valcheck1_batinr)

                oldt1_batinr = curs_batinr.fetchall()
                if oldt1_batinr:
                    for newtime1_batinr in oldt1_batinr:
                        closecal_batinr = newtime1_batinr["close"]
                        diffrence_batinr = (
                            Decimal(close_batinr) - Decimal(closecal_batinr)
                        ) / Decimal(close_batinr)
                        persentage_batinr = diffrence_batinr * 100
                        formdpers_batinr = "{:.2f}".format(persentage_batinr)
                else:
                    # if "formdpers_batinr" not in locals():
                    onemoresec_batinr = datetime.timedelta(seconds=1)
                    dat1moresec_batinr = datuncode_batinr - onemoresec_batinr
                    dat1moresec_batinr = dat1moresec_batinr.strftime(
                        "%Y-%m-%d %H:%M:%S %p"
                    )
                    curs1sec_batinr = db.cursor(MySQLdb.cursors.DictCursor)
                    sqlchecksec1_batinr = (
                        "SELECT * FROM efx_perchange_wz_batinr where timestamp = %s"
                    )

                    valchecksec1_batinr = (dat1moresec_batinr,)
                    curs1sec_batinr.execute(sqlchecksec1_batinr, valchecksec1_batinr)
                    oldt1sec_batinr = curs1sec_batinr.fetchall()
                    if oldt1sec_batinr:
                        for newtime1sec_batinr in oldt1sec_batinr:
                            closecal_batinr = newtime1sec_batinr["close"]
                            diffrence_batinr = (
                                Decimal(close_batinr) - Decimal(closecal_batinr)
                            ) / Decimal(close_batinr)
                            persentage_batinr = diffrence_batinr * 100
                            formdpers_batinr = "{:.2f}".format(persentage_batinr)
                    else:
                        # if "formdpers_batinr" not in locals():
                        date_sub2moresec = datetime.timedelta(seconds=2)
                        dat2moresec_batinr = datuncode_batinr - date_sub2moresec
                        dat2moresec_batinr = dat2moresec_batinr.strftime(
                            "%Y-%m-%d %H:%M:%S %p"
                        )
                        curs2sec_batinr = db.cursor(MySQLdb.cursors.DictCursor)
                        sqlchecksec2_batinr = (
                            "SELECT * FROM efx_perchange_wz_batinr where timestamp = %s"
                        )
                        valchecksec2_batinr = (dat2moresec_batinr,)
                        curs2sec_batinr.execute(
                            sqlchecksec2_batinr, valchecksec2_batinr
                        )
                        oldt2sec_batinr = curs2sec_batinr.fetchall()
                        if oldt2sec_batinr:
                            for newtime2sec_batinr in oldt2sec_batinr:
                                closecal_batinr = newtime2sec_batinr["close"]
                                diffrence_batinr = (
                                    Decimal(close_batinr) - Decimal(closecal_batinr)
                                ) / Decimal(close_batinr)
                                persentage_batinr = diffrence_batinr * 100
                                formdpers_batinr = "{:.2f}".format(persentage_batinr)
                        else:
                            # if "formdpers_batinr" not in locals():
                            dasub3sec_batinr = datetime.timedelta(seconds=3)
                            dat3moresec_batinr = datuncode_batinr - dasub3sec_batinr
                            dat3moresec_batinr = dat3moresec_batinr.strftime(
                                "%Y-%m-%d %H:%M:%S %p"
                            )
                            curs3sec_batinr = db.cursor(MySQLdb.cursors.DictCursor)
                            sqlcksec3_batinr = "SELECT * FROM efx_perchange_wz_batinr where timestamp = %s"

                            valcksec3_batinr = (dat3moresec_batinr,)
                            curs3sec_batinr.execute(sqlcksec3_batinr, valcksec3_batinr)
                            oldt3sec_batinr = curs3sec_batinr.fetchall()
                            if oldt3sec_batinr:
                                for newtime3sec_batinr in oldt3sec_batinr:

                                    closecal_batinr = newtime3sec_batinr["close"]
                                    diffrence_batinr = (
                                        Decimal(close_batinr) - Decimal(closecal_batinr)
                                    ) / Decimal(close_batinr)
                                    persentage_batinr = diffrence_batinr * 100
                                    formdpers_batinr = "{:.2f}".format(
                                        persentage_batinr
                                    )
                            else:
                                # if "formdpers_batinr" not in locals():
                                datesub4sec_batinr = datetime.timedelta(seconds=4)
                                dat4moresec_batinr = (
                                    datuncode_batinr - datesub4sec_batinr
                                )
                                dat4moresec_batinr = dat4moresec_batinr.strftime(
                                    "%Y-%m-%d %H:%M:%S %p"
                                )
                                curs4sec_batinr = db.cursor(MySQLdb.cursors.DictCursor)
                                sqlcksec4_batinr = "SELECT * FROM efx_perchange_wz_batinr where timestamp = %s"

                                valcksec4_batinr = (dat4moresec_batinr,)
                                curs4sec_batinr.execute(
                                    sqlcksec4_batinr, valcksec4_batinr
                                )
                                oldt4sec_batinr = curs4sec_batinr.fetchall()
                                if oldt4sec_batinr:
                                    for newtime4sec_batinr in oldt4sec_batinr:

                                        closecal_batinr = newtime4sec_batinr["close"]
                                        diffrence_batinr = (
                                            Decimal(close_batinr)
                                            - Decimal(closecal_batinr)
                                        ) / Decimal(close_batinr)
                                        persentage_batinr = diffrence_batinr * 100
                                        formdpers_batinr = "{:.2f}".format(
                                            persentage_batinr
                                        )
                                else:
                                    # if "formdpers_batinr" not in locals():
                                    datesub5sec_batinr = datetime.timedelta(seconds=5)
                                    dat5moresec_batinr = (
                                        datuncode_batinr - datesub5sec_batinr
                                    )
                                    dat5moresec_batinr = dat5moresec_batinr.strftime(
                                        "%Y-%m-%d %H:%M:%S %p"
                                    )
                                    curs5sec_batinr = db.cursor(
                                        MySQLdb.cursors.DictCursor
                                    )
                                    sqlcksec5_batinr = "SELECT * FROM efx_perchange_wz_batinr where timestamp = %s"

                                    valcksec5_batinr = (dat5moresec_batinr,)
                                    curs5sec_batinr.execute(
                                        sqlcksec5_batinr, valcksec5_batinr
                                    )
                                    oldt5sec_batinr = curs5sec_batinr.fetchall()
                                    if oldt5sec_batinr:
                                        for newtime5sec in oldt5sec_batinr:
                                            closecal_batinr = newtime5sec["close"]
                                            diffrence_batinr = (
                                                Decimal(close_batinr)
                                                - Decimal(closecal_batinr)
                                            ) / Decimal(close_batinr)
                                            persentage_batinr = diffrence_batinr * 100
                                            formdpers_batinr = "{:.2f}".format(
                                                persentage_batinr
                                            )
                                    else:
                                        formdpers_batinr = 100

                sql_batinr = "UPDATE efx_perchange_wz_batinr SET pricechange=%s WHERE timestamp = %s"
                val_batinr = (
                    formdpers_batinr,
                    date_time_batinr,
                )
                curs_batinr.execute(sql_batinr, val_batinr)
                db.commit()

            # bchinr
            curs1secpre_bchinr = db.cursor(MySQLdb.cursors.DictCursor)
            sql_checksecpre_bchinr = (
                "select * from efx_perchange_wz_bchinr order by id DESC LIMIT 1"
            )
            curs1secpre_bchinr.execute(sql_checksecpre_bchinr)
            oldt1secpre_bchinr = curs1secpre_bchinr.fetchall()
            for (
                value_bchinr
            ) in oldt1secpre_bchinr:  # particular api value_bchinr compare
                close_bchinr = value_bchinr["close"]
                # volume = input_dict[""]
                # !check datatime alreday there
                date_time_bchinr = value_bchinr["timestamp"]
                oldt_bchinr = datetime.datetime.strptime(
                    date_time_bchinr, "%Y-%m-%d %H:%M:%S %p"
                )
                date_sub_bchinr = datetime.timedelta(days=1)
                datuncode_bchinr = oldt_bchinr - date_sub_bchinr
                dat_bchinr = datuncode_bchinr.strftime("%Y-%m-%d %H:%M:%S %p")

                curs_bchinr = db.cursor(MySQLdb.cursors.DictCursor)
                sqlcheck1_bchinr = (
                    "SELECT * FROM efx_perchange_wz_bchinr where timestamp = %s"
                )
                valcheck1_bchinr = (dat_bchinr,)
                curs_bchinr.execute(sqlcheck1_bchinr, valcheck1_bchinr)

                oldt1_bchinr = curs_bchinr.fetchall()
                if oldt1_bchinr:
                    for newtime1_bchinr in oldt1_bchinr:
                        closecal_bchinr = newtime1_bchinr["close"]
                        diffrence_bchinr = (
                            Decimal(close_bchinr) - Decimal(closecal_bchinr)
                        ) / Decimal(close_bchinr)
                        persentage_bchinr = diffrence_bchinr * 100
                        formdpers_bchinr = "{:.2f}".format(persentage_bchinr)
                else:
                    # if "formdpers_bchinr" not in locals():
                    onemoresec_bchinr = datetime.timedelta(seconds=1)
                    dat1moresec_bchinr = datuncode_bchinr - onemoresec_bchinr
                    dat1moresec_bchinr = dat1moresec_bchinr.strftime(
                        "%Y-%m-%d %H:%M:%S %p"
                    )
                    curs1sec_bchinr = db.cursor(MySQLdb.cursors.DictCursor)
                    sqlchecksec1_bchinr = (
                        "SELECT * FROM efx_perchange_wz_bchinr where timestamp = %s"
                    )

                    valchecksec1_bchinr = (dat1moresec_bchinr,)
                    curs1sec_bchinr.execute(sqlchecksec1_bchinr, valchecksec1_bchinr)
                    oldt1sec_bchinr = curs1sec_bchinr.fetchall()
                    if oldt1sec_bchinr:
                        for newtime1sec_bchinr in oldt1sec_bchinr:
                            closecal_bchinr = newtime1sec_bchinr["close"]
                            diffrence_bchinr = (
                                Decimal(close_bchinr) - Decimal(closecal_bchinr)
                            ) / Decimal(close_bchinr)
                            persentage_bchinr = diffrence_bchinr * 100
                            formdpers_bchinr = "{:.2f}".format(persentage_bchinr)
                    else:
                        # if "formdpers_bchinr" not in locals():
                        date_sub2moresec = datetime.timedelta(seconds=2)
                        dat2moresec_bchinr = datuncode_bchinr - date_sub2moresec
                        dat2moresec_bchinr = dat2moresec_bchinr.strftime(
                            "%Y-%m-%d %H:%M:%S %p"
                        )
                        curs2sec_bchinr = db.cursor(MySQLdb.cursors.DictCursor)
                        sqlchecksec2_bchinr = (
                            "SELECT * FROM efx_perchange_wz_bchinr where timestamp = %s"
                        )
                        valchecksec2_bchinr = (dat2moresec_bchinr,)
                        curs2sec_bchinr.execute(
                            sqlchecksec2_bchinr, valchecksec2_bchinr
                        )
                        oldt2sec_bchinr = curs2sec_bchinr.fetchall()
                        if oldt2sec_bchinr:
                            for newtime2sec_bchinr in oldt2sec_bchinr:
                                closecal_bchinr = newtime2sec_bchinr["close"]
                                diffrence_bchinr = (
                                    Decimal(close_bchinr) - Decimal(closecal_bchinr)
                                ) / Decimal(close_bchinr)
                                persentage_bchinr = diffrence_bchinr * 100
                                formdpers_bchinr = "{:.2f}".format(persentage_bchinr)
                        else:
                            # if "formdpers_bchinr" not in locals():
                            dasub3sec_bchinr = datetime.timedelta(seconds=3)
                            dat3moresec_bchinr = datuncode_bchinr - dasub3sec_bchinr
                            dat3moresec_bchinr = dat3moresec_bchinr.strftime(
                                "%Y-%m-%d %H:%M:%S %p"
                            )
                            curs3sec_bchinr = db.cursor(MySQLdb.cursors.DictCursor)
                            sqlcksec3_bchinr = "SELECT * FROM efx_perchange_wz_bchinr where timestamp = %s"

                            valcksec3_bchinr = (dat3moresec_bchinr,)
                            curs3sec_bchinr.execute(sqlcksec3_bchinr, valcksec3_bchinr)
                            oldt3sec_bchinr = curs3sec_bchinr.fetchall()
                            if oldt3sec_bchinr:
                                for newtime3sec_bchinr in oldt3sec_bchinr:

                                    closecal_bchinr = newtime3sec_bchinr["close"]
                                    diffrence_bchinr = (
                                        Decimal(close_bchinr) - Decimal(closecal_bchinr)
                                    ) / Decimal(close_bchinr)
                                    persentage_bchinr = diffrence_bchinr * 100
                                    formdpers_bchinr = "{:.2f}".format(
                                        persentage_bchinr
                                    )
                            else:
                                # if "formdpers_bchinr" not in locals():
                                datesub4sec_bchinr = datetime.timedelta(seconds=4)
                                dat4moresec_bchinr = (
                                    datuncode_bchinr - datesub4sec_bchinr
                                )
                                dat4moresec_bchinr = dat4moresec_bchinr.strftime(
                                    "%Y-%m-%d %H:%M:%S %p"
                                )
                                curs4sec_bchinr = db.cursor(MySQLdb.cursors.DictCursor)
                                sqlcksec4_bchinr = "SELECT * FROM efx_perchange_wz_bchinr where timestamp = %s"

                                valcksec4_bchinr = (dat4moresec_bchinr,)
                                curs4sec_bchinr.execute(
                                    sqlcksec4_bchinr, valcksec4_bchinr
                                )
                                oldt4sec_bchinr = curs4sec_bchinr.fetchall()
                                if oldt4sec_bchinr:
                                    for newtime4sec_bchinr in oldt4sec_bchinr:

                                        closecal_bchinr = newtime4sec_bchinr["close"]
                                        diffrence_bchinr = (
                                            Decimal(close_bchinr)
                                            - Decimal(closecal_bchinr)
                                        ) / Decimal(close_bchinr)
                                        persentage_bchinr = diffrence_bchinr * 100
                                        formdpers_bchinr = "{:.2f}".format(
                                            persentage_bchinr
                                        )
                                else:
                                    # if "formdpers_bchinr" not in locals():
                                    datesub5sec_bchinr = datetime.timedelta(seconds=5)
                                    dat5moresec_bchinr = (
                                        datuncode_bchinr - datesub5sec_bchinr
                                    )
                                    dat5moresec_bchinr = dat5moresec_bchinr.strftime(
                                        "%Y-%m-%d %H:%M:%S %p"
                                    )
                                    curs5sec_bchinr = db.cursor(
                                        MySQLdb.cursors.DictCursor
                                    )
                                    sqlcksec5_bchinr = "SELECT * FROM efx_perchange_wz_bchinr where timestamp = %s"

                                    valcksec5_bchinr = (dat5moresec_bchinr,)
                                    curs5sec_bchinr.execute(
                                        sqlcksec5_bchinr, valcksec5_bchinr
                                    )
                                    oldt5sec_bchinr = curs5sec_bchinr.fetchall()
                                    if oldt5sec_bchinr:
                                        for newtime5sec in oldt5sec_bchinr:
                                            closecal_bchinr = newtime5sec["close"]
                                            diffrence_bchinr = (
                                                Decimal(close_bchinr)
                                                - Decimal(closecal_bchinr)
                                            ) / Decimal(close_bchinr)
                                            persentage_bchinr = diffrence_bchinr * 100
                                            formdpers_bchinr = "{:.2f}".format(
                                                persentage_bchinr
                                            )
                                    else:
                                        formdpers_bchinr = 100

                sql_bchinr = "UPDATE efx_perchange_wz_bchinr SET pricechange=%s WHERE timestamp = %s"
                val_bchinr = (
                    formdpers_bchinr,
                    date_time_bchinr,
                )
                curs_bchinr.execute(sql_bchinr, val_bchinr)
                db.commit()

            # bnbinr
            curs1secpre_bnbinr = db.cursor(MySQLdb.cursors.DictCursor)
            sql_checksecpre_bnbinr = (
                "select * from efx_perchange_wz_bnbinr order by id DESC LIMIT 1"
            )
            curs1secpre_bnbinr.execute(sql_checksecpre_bnbinr)
            oldt1secpre_bnbinr = curs1secpre_bnbinr.fetchall()
            for (
                value_bnbinr
            ) in oldt1secpre_bnbinr:  # particular api value_bnbinr compare
                close_bnbinr = value_bnbinr["close"]
                # volume = input_dict[""]
                # !check datatime alreday there
                date_time_bnbinr = value_bnbinr["timestamp"]
                oldt_bnbinr = datetime.datetime.strptime(
                    date_time_bnbinr, "%Y-%m-%d %H:%M:%S %p"
                )
                date_sub_bnbinr = datetime.timedelta(days=1)
                datuncode_bnbinr = oldt_bnbinr - date_sub_bnbinr
                dat_bnbinr = datuncode_bnbinr.strftime("%Y-%m-%d %H:%M:%S %p")

                curs_bnbinr = db.cursor(MySQLdb.cursors.DictCursor)
                sqlcheck1_bnbinr = (
                    "SELECT * FROM efx_perchange_wz_bnbinr where timestamp = %s"
                )
                valcheck1_bnbinr = (dat_bnbinr,)
                curs_bnbinr.execute(sqlcheck1_bnbinr, valcheck1_bnbinr)

                oldt1_bnbinr = curs_bnbinr.fetchall()
                if oldt1_bnbinr:
                    for newtime1_bnbinr in oldt1_bnbinr:
                        closecal_bnbinr = newtime1_bnbinr["close"]
                        diffrence_bnbinr = (
                            Decimal(close_bnbinr) - Decimal(closecal_bnbinr)
                        ) / Decimal(close_bnbinr)
                        persentage_bnbinr = diffrence_bnbinr * 100
                        formdpers_bnbinr = "{:.2f}".format(persentage_bnbinr)
                else:
                    # if "formdpers_bnbinr" not in locals():
                    onemoresec_bnbinr = datetime.timedelta(seconds=1)
                    dat1moresec_bnbinr = datuncode_bnbinr - onemoresec_bnbinr
                    dat1moresec_bnbinr = dat1moresec_bnbinr.strftime(
                        "%Y-%m-%d %H:%M:%S %p"
                    )
                    curs1sec_bnbinr = db.cursor(MySQLdb.cursors.DictCursor)
                    sqlchecksec1_bnbinr = (
                        "SELECT * FROM efx_perchange_wz_bnbinr where timestamp = %s"
                    )

                    valchecksec1_bnbinr = (dat1moresec_bnbinr,)
                    curs1sec_bnbinr.execute(sqlchecksec1_bnbinr, valchecksec1_bnbinr)
                    oldt1sec_bnbinr = curs1sec_bnbinr.fetchall()
                    if oldt1sec_bnbinr:
                        for newtime1sec_bnbinr in oldt1sec_bnbinr:
                            closecal_bnbinr = newtime1sec_bnbinr["close"]
                            diffrence_bnbinr = (
                                Decimal(close_bnbinr) - Decimal(closecal_bnbinr)
                            ) / Decimal(close_bnbinr)
                            persentage_bnbinr = diffrence_bnbinr * 100
                            formdpers_bnbinr = "{:.2f}".format(persentage_bnbinr)
                    else:
                        # if "formdpers_bnbinr" not in locals():
                        date_sub2moresec = datetime.timedelta(seconds=2)
                        dat2moresec_bnbinr = datuncode_bnbinr - date_sub2moresec
                        dat2moresec_bnbinr = dat2moresec_bnbinr.strftime(
                            "%Y-%m-%d %H:%M:%S %p"
                        )
                        curs2sec_bnbinr = db.cursor(MySQLdb.cursors.DictCursor)
                        sqlchecksec2_bnbinr = (
                            "SELECT * FROM efx_perchange_wz_bnbinr where timestamp = %s"
                        )
                        valchecksec2_bnbinr = (dat2moresec_bnbinr,)
                        curs2sec_bnbinr.execute(
                            sqlchecksec2_bnbinr, valchecksec2_bnbinr
                        )
                        oldt2sec_bnbinr = curs2sec_bnbinr.fetchall()
                        if oldt2sec_bnbinr:
                            for newtime2sec_bnbinr in oldt2sec_bnbinr:
                                closecal_bnbinr = newtime2sec_bnbinr["close"]
                                diffrence_bnbinr = (
                                    Decimal(close_bnbinr) - Decimal(closecal_bnbinr)
                                ) / Decimal(close_bnbinr)
                                persentage_bnbinr = diffrence_bnbinr * 100
                                formdpers_bnbinr = "{:.2f}".format(persentage_bnbinr)
                        else:
                            # if "formdpers_bnbinr" not in locals():
                            dasub3sec_bnbinr = datetime.timedelta(seconds=3)
                            dat3moresec_bnbinr = datuncode_bnbinr - dasub3sec_bnbinr
                            dat3moresec_bnbinr = dat3moresec_bnbinr.strftime(
                                "%Y-%m-%d %H:%M:%S %p"
                            )
                            curs3sec_bnbinr = db.cursor(MySQLdb.cursors.DictCursor)
                            sqlcksec3_bnbinr = "SELECT * FROM efx_perchange_wz_bnbinr where timestamp = %s"

                            valcksec3_bnbinr = (dat3moresec_bnbinr,)
                            curs3sec_bnbinr.execute(sqlcksec3_bnbinr, valcksec3_bnbinr)
                            oldt3sec_bnbinr = curs3sec_bnbinr.fetchall()
                            if oldt3sec_bnbinr:
                                for newtime3sec_bnbinr in oldt3sec_bnbinr:

                                    closecal_bnbinr = newtime3sec_bnbinr["close"]
                                    diffrence_bnbinr = (
                                        Decimal(close_bnbinr) - Decimal(closecal_bnbinr)
                                    ) / Decimal(close_bnbinr)
                                    persentage_bnbinr = diffrence_bnbinr * 100
                                    formdpers_bnbinr = "{:.2f}".format(
                                        persentage_bnbinr
                                    )
                            else:
                                # if "formdpers_bnbinr" not in locals():
                                datesub4sec_bnbinr = datetime.timedelta(seconds=4)
                                dat4moresec_bnbinr = (
                                    datuncode_bnbinr - datesub4sec_bnbinr
                                )
                                dat4moresec_bnbinr = dat4moresec_bnbinr.strftime(
                                    "%Y-%m-%d %H:%M:%S %p"
                                )
                                curs4sec_bnbinr = db.cursor(MySQLdb.cursors.DictCursor)
                                sqlcksec4_bnbinr = "SELECT * FROM efx_perchange_wz_bnbinr where timestamp = %s"

                                valcksec4_bnbinr = (dat4moresec_bnbinr,)
                                curs4sec_bnbinr.execute(
                                    sqlcksec4_bnbinr, valcksec4_bnbinr
                                )
                                oldt4sec_bnbinr = curs4sec_bnbinr.fetchall()
                                if oldt4sec_bnbinr:
                                    for newtime4sec_bnbinr in oldt4sec_bnbinr:

                                        closecal_bnbinr = newtime4sec_bnbinr["close"]
                                        diffrence_bnbinr = (
                                            Decimal(close_bnbinr)
                                            - Decimal(closecal_bnbinr)
                                        ) / Decimal(close_bnbinr)
                                        persentage_bnbinr = diffrence_bnbinr * 100
                                        formdpers_bnbinr = "{:.2f}".format(
                                            persentage_bnbinr
                                        )
                                else:
                                    # if "formdpers_bnbinr" not in locals():
                                    datesub5sec_bnbinr = datetime.timedelta(seconds=5)
                                    dat5moresec_bnbinr = (
                                        datuncode_bnbinr - datesub5sec_bnbinr
                                    )
                                    dat5moresec_bnbinr = dat5moresec_bnbinr.strftime(
                                        "%Y-%m-%d %H:%M:%S %p"
                                    )
                                    curs5sec_bnbinr = db.cursor(
                                        MySQLdb.cursors.DictCursor
                                    )
                                    sqlcksec5_bnbinr = "SELECT * FROM efx_perchange_wz_bnbinr where timestamp = %s"

                                    valcksec5_bnbinr = (dat5moresec_bnbinr,)
                                    curs5sec_bnbinr.execute(
                                        sqlcksec5_bnbinr, valcksec5_bnbinr
                                    )
                                    oldt5sec_bnbinr = curs5sec_bnbinr.fetchall()
                                    if oldt5sec_bnbinr:
                                        for newtime5sec in oldt5sec_bnbinr:
                                            closecal_bnbinr = newtime5sec["close"]
                                            diffrence_bnbinr = (
                                                Decimal(close_bnbinr)
                                                - Decimal(closecal_bnbinr)
                                            ) / Decimal(close_bnbinr)
                                            persentage_bnbinr = diffrence_bnbinr * 100
                                            formdpers_bnbinr = "{:.2f}".format(
                                                persentage_bnbinr
                                            )
                                    else:
                                        formdpers_bnbinr = 100

                sql_bnbinr = "UPDATE efx_perchange_wz_bnbinr SET pricechange=%s WHERE timestamp = %s"
                val_bnbinr = (
                    formdpers_bnbinr,
                    date_time_bnbinr,
                )
                curs_bnbinr.execute(sql_bnbinr, val_bnbinr)
                db.commit()

        except Exception as e:
            print(e)


loop = asyncio.get_event_loop()
try:
    print("start 1st 4")
    asyncio.ensure_future(listen())
    loop.run_forever()
except KeyboardInterrupt:
    pass
finally:
    print("Closing Loop")
    loop.close()
