from unicodedata import decimal
import websockets
import requests
import asyncio
import json
import MySQLdb.cursors
import datetime
from threading import Timer
from operator import itemgetter
import datetime
from time import process_time
from decimal import *

db = MySQLdb.connect("localhost", "root", "", "xenbot")


async def listen():
    url = "https://api.wazirx.com/sapi/v1/tickers/24hr"
    while True:
        await asyncio.sleep(1)
        x = datetime.datetime.now()
        # ! check datetime
        calltime = x.strftime("%Y-%m-%d %H:%M:%S %p")
        r = requests.get(url)
        input_dict = r.json()
        curs = db.cursor(MySQLdb.cursors.DictCursor)
        try:
            for value in input_dict:  # particular api value compare
                curs = db.cursor(MySQLdb.cursors.DictCursor)
                if value["symbol"] == "adainr":
                    
                    open_p = value["openPrice"]
                    high = value["highPrice"]
                    low = value["lowPrice"]
                    close = value["lastPrice"]
                    # volume = input_dict[""]
                    timestamp = value["at"]
                    symbol = value["symbol"]
                    totaltradebaseasset = value["volume"]

                    # ! check datetime convert store formate
                    timestamnewapi = datetime.datetime.fromtimestamp(timestamp / 1e3)
                    timestamnewapi = timestamnewapi.strftime("%Y-%m-%d %H:%M:%S %p")

                    sql = "INSERT INTO 	efx_perchange_wz_adainr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                    val = (
                        open_p,
                        close,
                        high,
                        low,
                        symbol,
                        totaltradebaseasset,
                        timestamnewapi,
                        calltime,

                    )
                    curs.execute(sql, val)
                    db.commit()
                if value["symbol"] == "atominr":
                    open_p = value["openPrice"]
                    high = value["highPrice"]
                    low = value["lowPrice"]
                    close = value["lastPrice"]
                    # volume = input_dict[""]
                    timestamp = value["at"]
                    symbol = value["symbol"]
                    totaltradebaseasset = value["volume"]

                    # ! check datetime convert store formate
                    timestamnewapi = datetime.datetime.fromtimestamp(timestamp / 1e3)
                    timestamnewapi = timestamnewapi.strftime("%Y-%m-%d %H:%M:%S %p")

                    sql = "INSERT INTO 	efx_perchange_wz_atominr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                    val = (
                        open_p,
                        close,
                        high,
                        low,
                        symbol,
                        totaltradebaseasset,
                        timestamnewapi,
                        calltime,

                    )
                    curs.execute(sql, val)
                    db.commit()
                if value["symbol"] == "batinr":
                    open_p = value["openPrice"]
                    high = value["highPrice"]
                    low = value["lowPrice"]
                    close = value["lastPrice"]
                    # volume = input_dict[""]
                    timestamp = value["at"]
                    symbol = value["symbol"]
                    totaltradebaseasset = value["volume"]

                    # ! check datetime convert store formate
                    timestamnewapi = datetime.datetime.fromtimestamp(timestamp / 1e3)
                    timestamnewapi = timestamnewapi.strftime("%Y-%m-%d %H:%M:%S %p")

                    sql = "INSERT INTO 	efx_perchange_wz_batinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                    val = (
                        open_p,
                        close,
                        high,
                        low,
                        symbol,
                        totaltradebaseasset,
                        timestamnewapi,
                        calltime,

                    )
                    curs.execute(sql, val)
                    db.commit()
                if value["symbol"] == "bchinr":
                    open_p = value["openPrice"]
                    high = value["highPrice"]
                    low = value["lowPrice"]
                    close = value["lastPrice"]
                    # volume = input_dict[""]
                    timestamp = value["at"]
                    symbol = value["symbol"]
                    totaltradebaseasset = value["volume"]

                    # ! check datetime convert store formate
                    timestamnewapi = datetime.datetime.fromtimestamp(timestamp / 1e3)
                    timestamnewapi = timestamnewapi.strftime("%Y-%m-%d %H:%M:%S %p")

                    sql = "INSERT INTO 	efx_perchange_wz_bchinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                    val = (
                        open_p,
                        close,
                        high,
                        low,
                        symbol,
                        totaltradebaseasset,
                        timestamnewapi,
                        calltime,

                    )
                    curs.execute(sql, val)
                    db.commit()
                if value["symbol"] == "bnbinr":
                    open_p = value["openPrice"]
                    high = value["highPrice"]
                    low = value["lowPrice"]
                    close = value["lastPrice"]
                    # volume = input_dict[""]
                    timestamp = value["at"]
                    symbol = value["symbol"]
                    totaltradebaseasset = value["volume"]

                    # ! check datetime convert store formate
                    timestamnewapi = datetime.datetime.fromtimestamp(timestamp / 1e3)
                    timestamnewapi = timestamnewapi.strftime("%Y-%m-%d %H:%M:%S %p")

                    sql = "INSERT INTO 	efx_perchange_wz_bnbinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                    val = (
                        open_p,
                        close,
                        high,
                        low,
                        symbol,
                        totaltradebaseasset,
                        timestamnewapi,
                        calltime,

                    )
                    curs.execute(sql, val)
                    db.commit()
                if value["symbol"] == "btcinr":
                    open_p = value["openPrice"]
                    high = value["highPrice"]
                    low = value["lowPrice"]
                    close = value["lastPrice"]
                    # volume = input_dict[""]
                    timestamp = value["at"]
                    symbol = value["symbol"]
                    totaltradebaseasset = value["volume"]

                    # ! check datetime convert store formate
                    timestamnewapi = datetime.datetime.fromtimestamp(timestamp / 1e3)
                    timestamnewapi = timestamnewapi.strftime("%Y-%m-%d %H:%M:%S %p")

                    sql = "INSERT INTO 	efx_perchange_wz_btcinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                    val = (
                        open_p,
                        close,
                        high,
                        low,
                        symbol,
                        totaltradebaseasset,
                        timestamnewapi,
                        calltime,

                    )
                    curs.execute(sql, val)
                    db.commit()
                if value["symbol"] == "cakeinr":
                    open_p = value["openPrice"]
                    high = value["highPrice"]
                    low = value["lowPrice"]
                    close = value["lastPrice"]
                    # volume = input_dict[""]
                    timestamp = value["at"]
                    symbol = value["symbol"]
                    totaltradebaseasset = value["volume"]

                    # ! check datetime convert store formate
                    timestamnewapi = datetime.datetime.fromtimestamp(timestamp / 1e3)
                    timestamnewapi = timestamnewapi.strftime("%Y-%m-%d %H:%M:%S %p")

                    sql = "INSERT INTO 	efx_perchange_wz_cakeinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                    val = (
                        open_p,
                        close,
                        high,
                        low,
                        symbol,
                        totaltradebaseasset,
                        timestamnewapi,
                        calltime,

                    )
                    curs.execute(sql, val)
                    db.commit()
                if value["symbol"] == "compinr":
                    open_p = value["openPrice"]
                    high = value["highPrice"]
                    low = value["lowPrice"]
                    close = value["lastPrice"]
                    # volume = input_dict[""]
                    timestamp = value["at"]
                    symbol = value["symbol"]
                    totaltradebaseasset = value["volume"]

                    # ! check datetime convert store formate
                    timestamnewapi = datetime.datetime.fromtimestamp(timestamp / 1e3)
                    timestamnewapi = timestamnewapi.strftime("%Y-%m-%d %H:%M:%S %p")

                    sql = "INSERT INTO 	efx_perchange_wz_compinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                    val = (
                        open_p,
                        close,
                        high,
                        low,
                        symbol,
                        totaltradebaseasset,
                        timestamnewapi,
                        calltime,

                    )
                    curs.execute(sql, val)
                    db.commit()
                if value["symbol"] == "crvinr":
                    open_p = value["openPrice"]
                    high = value["highPrice"]
                    low = value["lowPrice"]
                    close = value["lastPrice"]
                    # volume = input_dict[""]
                    timestamp = value["at"]
                    symbol = value["symbol"]
                    totaltradebaseasset = value["volume"]

                    # ! check datetime convert store formate
                    timestamnewapi = datetime.datetime.fromtimestamp(timestamp / 1e3)
                    timestamnewapi = timestamnewapi.strftime("%Y-%m-%d %H:%M:%S %p")

                    sql = "INSERT INTO 	efx_perchange_wz_crvinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                    val = (
                        open_p,
                        close,
                        high,
                        low,
                        symbol,
                        totaltradebaseasset,
                        timestamnewapi,
                        calltime,

                    )
                    curs.execute(sql, val)
                    db.commit()
                if value["symbol"] == "celrinr":
                    open_p = value["openPrice"]
                    high = value["highPrice"]
                    low = value["lowPrice"]
                    close = value["lastPrice"]
                    # volume = input_dict[""]
                    timestamp = value["at"]
                    symbol = value["symbol"]
                    totaltradebaseasset = value["volume"]

                    # ! check datetime convert store formate
                    timestamnewapi = datetime.datetime.fromtimestamp(timestamp / 1e3)
                    timestamnewapi = timestamnewapi.strftime("%Y-%m-%d %H:%M:%S %p")

                    sql = "INSERT INTO 	efx_perchange_wz_celrinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                    val = (
                        open_p,
                        close,
                        high,
                        low,
                        symbol,
                        totaltradebaseasset,
                        timestamnewapi,
                        calltime,

                    )
                    curs.execute(sql, val)
                    db.commit()
                if value["symbol"] == "dashinr":
                    open_p = value["openPrice"]
                    high = value["highPrice"]
                    low = value["lowPrice"]
                    close = value["lastPrice"]
                    # volume = input_dict[""]
                    timestamp = value["at"]
                    symbol = value["symbol"]
                    totaltradebaseasset = value["volume"]

                    # ! check datetime convert store formate
                    timestamnewapi = datetime.datetime.fromtimestamp(timestamp / 1e3)
                    timestamnewapi = timestamnewapi.strftime("%Y-%m-%d %H:%M:%S %p")

                    sql = "INSERT INTO 	efx_perchange_wz_dashinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                    val = (
                        open_p,
                        close,
                        high,
                        low,
                        symbol,
                        totaltradebaseasset,
                        timestamnewapi,
                        calltime,

                    )
                    curs.execute(sql, val)
                    db.commit()
                if value["symbol"] == "dotinr":
                    open_p = value["openPrice"]
                    high = value["highPrice"]
                    low = value["lowPrice"]
                    close = value["lastPrice"]
                    # volume = input_dict[""]
                    timestamp = value["at"]
                    symbol = value["symbol"]
                    totaltradebaseasset = value["volume"]

                    # ! check datetime convert store formate
                    timestamnewapi = datetime.datetime.fromtimestamp(timestamp / 1e3)
                    timestamnewapi = timestamnewapi.strftime("%Y-%m-%d %H:%M:%S %p")

                    sql = "INSERT INTO 	efx_perchange_wz_dotinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                    val = (
                        open_p,
                        close,
                        high,
                        low,
                        symbol,
                        totaltradebaseasset,
                        timestamnewapi,
                        calltime,

                    )
                    curs.execute(sql, val)
                    db.commit()
                if value["symbol"] == "eosinr":
                    open_p = value["openPrice"]
                    high = value["highPrice"]
                    low = value["lowPrice"]
                    close = value["lastPrice"]
                    # volume = input_dict[""]
                    timestamp = value["at"]
                    symbol = value["symbol"]
                    totaltradebaseasset = value["volume"]

                    # ! check datetime convert store formate
                    timestamnewapi = datetime.datetime.fromtimestamp(timestamp / 1e3)
                    timestamnewapi = timestamnewapi.strftime("%Y-%m-%d %H:%M:%S %p")

                    sql = "INSERT INTO 	efx_perchange_wz_eosinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                    val = (
                        open_p,
                        close,
                        high,
                        low,
                        symbol,
                        totaltradebaseasset,
                        timestamnewapi,
                        calltime,

                    )
                    curs.execute(sql, val)
                    db.commit()
                if value["symbol"] == "ethinr":
                    open_p = value["openPrice"]
                    high = value["highPrice"]
                    low = value["lowPrice"]
                    close = value["lastPrice"]
                    # volume = input_dict[""]
                    timestamp = value["at"]
                    symbol = value["symbol"]
                    totaltradebaseasset = value["volume"]

                    # ! check datetime convert store formate
                    timestamnewapi = datetime.datetime.fromtimestamp(timestamp / 1e3)
                    timestamnewapi = timestamnewapi.strftime("%Y-%m-%d %H:%M:%S %p")

                    sql = "INSERT INTO 	efx_perchange_wz_ethinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                    val = (
                        open_p,
                        close,
                        high,
                        low,
                        symbol,
                        totaltradebaseasset,
                        timestamnewapi,
                        calltime,

                    )
                    curs.execute(sql, val)
                    db.commit()
                if value["symbol"] == "etcinr":
                    open_p = value["openPrice"]
                    high = value["highPrice"]
                    low = value["lowPrice"]
                    close = value["lastPrice"]
                    # volume = input_dict[""]
                    timestamp = value["at"]
                    symbol = value["symbol"]
                    totaltradebaseasset = value["volume"]

                    # ! check datetime convert store formate
                    timestamnewapi = datetime.datetime.fromtimestamp(timestamp / 1e3)
                    timestamnewapi = timestamnewapi.strftime("%Y-%m-%d %H:%M:%S %p")

                    sql = "INSERT INTO 	efx_perchange_wz_etcinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                    val = (
                        open_p,
                        close,
                        high,
                        low,
                        symbol,
                        totaltradebaseasset,
                        timestamnewapi,
                        calltime,

                    )
                    curs.execute(sql, val)
                    db.commit()
                if value["symbol"] == "filinr":
                    open_p = value["openPrice"]
                    high = value["highPrice"]
                    low = value["lowPrice"]
                    close = value["lastPrice"]
                    # volume = input_dict[""]
                    timestamp = value["at"]
                    symbol = value["symbol"]
                    totaltradebaseasset = value["volume"]

                    # ! check datetime convert store formate
                    timestamnewapi = datetime.datetime.fromtimestamp(timestamp / 1e3)
                    timestamnewapi = timestamnewapi.strftime("%Y-%m-%d %H:%M:%S %p")

                    sql = "INSERT INTO 	efx_perchange_wz_filinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                    val = (
                        open_p,
                        close,
                        high,
                        low,
                        symbol,
                        totaltradebaseasset,
                        timestamnewapi,
                        calltime,

                    )
                    curs.execute(sql, val)
                    db.commit()
                if value["symbol"] == "fttinr":
                    open_p = value["openPrice"]
                    high = value["highPrice"]
                    low = value["lowPrice"]
                    close = value["lastPrice"]
                    # volume = input_dict[""]
                    timestamp = value["at"]
                    symbol = value["symbol"]
                    totaltradebaseasset = value["volume"]

                    # ! check datetime convert store formate
                    timestamnewapi = datetime.datetime.fromtimestamp(timestamp / 1e3)
                    timestamnewapi = timestamnewapi.strftime("%Y-%m-%d %H:%M:%S %p")

                    sql = "INSERT INTO 	efx_perchange_wz_fttinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                    val = (
                        open_p,
                        close,
                        high,
                        low,
                        symbol,
                        totaltradebaseasset,
                        timestamnewapi,
                        calltime,

                    )
                    curs.execute(sql, val)
                    db.commit()
                if value["symbol"] == "iostinr":
                    open_p = value["openPrice"]
                    high = value["highPrice"]
                    low = value["lowPrice"]
                    close = value["lastPrice"]
                    # volume = input_dict[""]
                    timestamp = value["at"]
                    symbol = value["symbol"]
                    totaltradebaseasset = value["volume"]

                    # ! check datetime convert store formate
                    timestamnewapi = datetime.datetime.fromtimestamp(timestamp / 1e3)
                    timestamnewapi = timestamnewapi.strftime("%Y-%m-%d %H:%M:%S %p")

                    sql = "INSERT INTO 	efx_perchange_wz_iostinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                    val = (
                        open_p,
                        close,
                        high,
                        low,
                        symbol,
                        totaltradebaseasset,
                        timestamnewapi,
                        calltime,

                    )
                    curs.execute(sql, val)
                    db.commit()
                if value["symbol"] == "linkinr":
                    open_p = value["openPrice"]
                    high = value["highPrice"]
                    low = value["lowPrice"]
                    close = value["lastPrice"]
                    # volume = input_dict[""]
                    timestamp = value["at"]
                    symbol = value["symbol"]
                    totaltradebaseasset = value["volume"]

                    # ! check datetime convert store formate
                    timestamnewapi = datetime.datetime.fromtimestamp(timestamp / 1e3)
                    timestamnewapi = timestamnewapi.strftime("%Y-%m-%d %H:%M:%S %p")

                    sql = "INSERT INTO 	efx_perchange_wz_linkinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                    val = (
                        open_p,
                        close,
                        high,
                        low,
                        symbol,
                        totaltradebaseasset,
                        timestamnewapi,
                        calltime,

                    )
                    curs.execute(sql, val)
                    db.commit()
                if value["symbol"] == "ltcinr":
                    open_p = value["openPrice"]
                    high = value["highPrice"]
                    low = value["lowPrice"]
                    close = value["lastPrice"]
                    # volume = input_dict[""]
                    timestamp = value["at"]
                    symbol = value["symbol"]
                    totaltradebaseasset = value["volume"]

                    # ! check datetime convert store formate
                    timestamnewapi = datetime.datetime.fromtimestamp(timestamp / 1e3)
                    timestamnewapi = timestamnewapi.strftime("%Y-%m-%d %H:%M:%S %p")

                    sql = "INSERT INTO 	efx_perchange_wz_ltcinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                    val = (
                        open_p,
                        close,
                        high,
                        low,
                        symbol,
                        totaltradebaseasset,
                        timestamnewapi,
                        calltime,

                    )
                    curs.execute(sql, val)
                    db.commit()
                if value["symbol"] == "manainr":
                    open_p = value["openPrice"]
                    high = value["highPrice"]
                    low = value["lowPrice"]
                    close = value["lastPrice"]
                    # volume = input_dict[""]
                    timestamp = value["at"]
                    symbol = value["symbol"]
                    totaltradebaseasset = value["volume"]

                    # ! check datetime convert store formate
                    timestamnewapi = datetime.datetime.fromtimestamp(timestamp / 1e3)
                    timestamnewapi = timestamnewapi.strftime("%Y-%m-%d %H:%M:%S %p")

                    sql = "INSERT INTO 	efx_perchange_wz_manainr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                    val = (
                        open_p,
                        close,
                        high,
                        low,
                        symbol,
                        totaltradebaseasset,
                        timestamnewapi,
                        calltime,

                    )
                    curs.execute(sql, val)
                    db.commit()
                if value["symbol"] == "mdxinr":
                    open_p = value["openPrice"]
                    high = value["highPrice"]
                    low = value["lowPrice"]
                    close = value["lastPrice"]
                    # volume = input_dict[""]
                    timestamp = value["at"]
                    symbol = value["symbol"]
                    totaltradebaseasset = value["volume"]

                    # ! check datetime convert store formate
                    timestamnewapi = datetime.datetime.fromtimestamp(timestamp / 1e3)
                    timestamnewapi = timestamnewapi.strftime("%Y-%m-%d %H:%M:%S %p")

                    sql = "INSERT INTO 	efx_perchange_wz_mdxinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                    val = (
                        open_p,
                        close,
                        high,
                        low,
                        symbol,
                        totaltradebaseasset,
                        timestamnewapi,
                        calltime,

                    )
                    curs.execute(sql, val)
                    db.commit()
                if value["symbol"] == "omginr":
                    open_p = value["openPrice"]
                    high = value["highPrice"]
                    low = value["lowPrice"]
                    close = value["lastPrice"]
                    # volume = input_dict[""]
                    timestamp = value["at"]
                    symbol = value["symbol"]
                    totaltradebaseasset = value["volume"]

                    # ! check datetime convert store formate
                    timestamnewapi = datetime.datetime.fromtimestamp(timestamp / 1e3)
                    timestamnewapi = timestamnewapi.strftime("%Y-%m-%d %H:%M:%S %p")

                    sql = "INSERT INTO 	efx_perchange_wz_omginr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                    val = (
                        open_p,
                        close,
                        high,
                        low,
                        symbol,
                        totaltradebaseasset,
                        timestamnewapi,
                        calltime,

                    )
                    curs.execute(sql, val)
                    db.commit()
                if value["symbol"] == "sushiinr":
                    open_p = value["openPrice"]
                    high = value["highPrice"]
                    low = value["lowPrice"]
                    close = value["lastPrice"]
                    # volume = input_dict[""]
                    timestamp = value["at"]
                    symbol = value["symbol"]
                    totaltradebaseasset = value["volume"]

                    # ! check datetime convert store formate
                    timestamnewapi = datetime.datetime.fromtimestamp(timestamp / 1e3)
                    timestamnewapi = timestamnewapi.strftime("%Y-%m-%d %H:%M:%S %p")

                    sql = "INSERT INTO 	efx_perchange_wz_sushiinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                    val = (
                        open_p,
                        close,
                        high,
                        low,
                        symbol,
                        totaltradebaseasset,
                        timestamnewapi,
                        calltime,

                    )
                    curs.execute(sql, val)
                    db.commit()
                if value["symbol"] == "trxinr":
                    open_p = value["openPrice"]
                    high = value["highPrice"]
                    low = value["lowPrice"]
                    close = value["lastPrice"]
                    # volume = input_dict[""]
                    timestamp = value["at"]
                    symbol = value["symbol"]
                    totaltradebaseasset = value["volume"]

                    # ! check datetime convert store formate
                    timestamnewapi = datetime.datetime.fromtimestamp(timestamp / 1e3)
                    timestamnewapi = timestamnewapi.strftime("%Y-%m-%d %H:%M:%S %p")

                    sql = "INSERT INTO 	efx_perchange_wz_trxinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                    val = (
                        open_p,
                        close,
                        high,
                        low,
                        symbol,
                        totaltradebaseasset,
                        timestamnewapi,
                        calltime,

                    )
                    curs.execute(sql, val)
                    db.commit()
                if value["symbol"] == "uniinr":
                    open_p = value["openPrice"]
                    high = value["highPrice"]
                    low = value["lowPrice"]
                    close = value["lastPrice"]
                    # volume = input_dict[""]
                    timestamp = value["at"]
                    symbol = value["symbol"]
                    totaltradebaseasset = value["volume"]

                    # ! check datetime convert store formate
                    timestamnewapi = datetime.datetime.fromtimestamp(timestamp / 1e3)
                    timestamnewapi = timestamnewapi.strftime("%Y-%m-%d %H:%M:%S %p")

                    sql = "INSERT INTO 	efx_perchange_wz_uniinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                    val = (
                        open_p,
                        close,
                        high,
                        low,
                        symbol,
                        totaltradebaseasset,
                        timestamnewapi,
                        calltime,

                    )
                    curs.execute(sql, val)
                    db.commit()
                if value["symbol"] == "xrpinr":
                    open_p = value["openPrice"]
                    high = value["highPrice"]
                    low = value["lowPrice"]
                    close = value["lastPrice"]
                    # volume = input_dict[""]
                    timestamp = value["at"]
                    symbol = value["symbol"]
                    totaltradebaseasset = value["volume"]

                    # ! check datetime convert store formate
                    timestamnewapi = datetime.datetime.fromtimestamp(timestamp / 1e3)
                    timestamnewapi = timestamnewapi.strftime("%Y-%m-%d %H:%M:%S %p")

                    sql = "INSERT INTO 	efx_perchange_wz_xrpinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                    val = (
                        open_p,
                        close,
                        high,
                        low,
                        symbol,
                        totaltradebaseasset,
                        timestamnewapi,
                        calltime,

                    )
                    curs.execute(sql, val)
                    db.commit()
                if value["symbol"] == "shibinr":
                    open_p = value["openPrice"]
                    high = value["highPrice"]
                    low = value["lowPrice"]
                    close = value["lastPrice"]
                    # volume = input_dict[""]
                    timestamp = value["at"]
                    symbol = value["symbol"]
                    totaltradebaseasset = value["volume"]

                    # ! check datetime convert store formate
                    timestamnewapi = datetime.datetime.fromtimestamp(timestamp / 1e3)
                    timestamnewapi = timestamnewapi.strftime("%Y-%m-%d %H:%M:%S %p")

                    sql = "INSERT INTO 	efx_perchange_wz_shibinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                    val = (
                        open_p,
                        close,
                        high,
                        low,
                        symbol,
                        totaltradebaseasset,
                        timestamnewapi,
                        calltime,

                    )
                    curs.execute(sql, val)
                    db.commit()
                if value["symbol"] == "maticinr":
                    open_p = value["openPrice"]
                    high = value["highPrice"]
                    low = value["lowPrice"]
                    close = value["lastPrice"]
                    # volume = input_dict[""]
                    timestamp = value["at"]
                    symbol = value["symbol"]
                    totaltradebaseasset = value["volume"]

                    # ! check datetime convert store formate
                    timestamnewapi = datetime.datetime.fromtimestamp(timestamp / 1e3)
                    timestamnewapi = timestamnewapi.strftime("%Y-%m-%d %H:%M:%S %p")

                    sql = "INSERT INTO 	efx_perchange_wz_maticinr (open, close , high, low,symbol,volume,timestamp_wz,timestamp) VALUE (%s,%s,%s,%s,%s,%s,%s,%s)"
                    val = (
                        open_p,
                        close,
                        high,
                        low,
                        symbol,
                        totaltradebaseasset,
                        timestamnewapi,
                        calltime,

                    )
                    curs.execute(sql, val)
                    db.commit()

            print(calltime)
        except Exception as e:
            print(e)


loop = asyncio.get_event_loop()
try:
    print("start 1st 4")
    asyncio.ensure_future(listen())
    loop.run_forever()
except KeyboardInterrupt:
    pass
finally:
    print("Closing Loop")
    loop.close()
