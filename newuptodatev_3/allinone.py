from threading import Timer
from operator import itemgetter
import os
from numpy import arange
from subprocess import check_output


def startjob():
    path_nodechart = "D:\\newuptodatev_3\\BotForNodechar\\"
    path_binancebot = "D:\\newuptodatev_3\\BotForTrading\\Binance\\"
    path_wazirxbot = "D:\\newuptodatev_3\\BotForTrading\\Wazirx\\"
    bscscan = "D:\\newuptodatev_3\\"
    wazirxpercentagePath = "D:\\newuptodatev_3\\BotForTrading\\Wazirx\\percentagecheck\\percentagechangecal\\"
    wazirxbotPath = "D:\\newuptodatev_3\\BotForTrading\\Wazirx\\botforwazirx\\"

    class cd:
        """Context manager for changing the current working directory"""

        def __init__(self, newPath):
            self.newPath = os.path.expanduser(newPath)

        def __enter__(self):
            self.savedPath = os.getcwd()
            os.chdir(self.newPath)

        def __exit__(self, etype, value, traceback):
            os.chdir(self.savedPath)
    ################################ BINANCE ############################################
    # with cd("D:\\wamp64\\www\\newuptodatev_2\\BotForTrading\\allcoin\\"):
    #     # we are in ~/Library
    #     new = os.getcwd()
    #     os.system("start cmd /K python 1INCHUSDT.py")
    #     os.system("start cmd /K python AAVEUSDT.py")
    #     os.system("start cmd /K python ADAUSDT.py")
    #     os.system("start cmd /K python AKROUSDT.py")
    #     os.system("start cmd /K python ANTUSDT.py")
    #     os.system("start cmd /K python ATOMUSDT.py")
    #     os.system("start cmd /K python BATUSDT.py")
    #     os.system("start cmd /K python BCHUSDT.py")
    #     os.system("start cmd /K python BNBUSDT.py")
    #     os.system("start cmd /K python BTCUSDT.py")
    #     os.system("start cmd /K python CAKEUSDT.py")
    #     os.system("start cmd /K python CELRUSDT.py")
    #     os.system("start cmd /K python COMPUSDT.py")
    #     os.system("start cmd /K python COSUSDT.py")
    #     os.system("start cmd /K python CRVUSDT.py")
    #     os.system("start cmd /K python DASHUSDT.py")
    #     os.system("start cmd /K python DOGEUSDT.py")
    #     os.system("start cmd /K python DOTUSDT.py")
    #     os.system("start cmd /K python EOSUSDT.py")
    #     os.system("start cmd /K python ETCUSDT.py")
    #     os.system("start cmd /K python ETHUSDT.py")
    #     os.system("start cmd /K python FILUSDT.py")
    #     os.system("start cmd /K python FTTUSDT.py")
    #     os.system("start cmd /K python GRTUSDT.py")
    #     os.system("start cmd /K python IOSTUSDT.py")
    #     os.system("start cmd /K python IOTAUSDT.py")
    #     os.system("start cmd /K python JSTUSDT.py")
    #     os.system("start cmd /K python KAVAUSDT.py")
    #     os.system("start cmd /K python LINKUSDT.py")
    #     os.system("start cmd /K python LITUSDT.py")
    #     os.system("start cmd /K python LTCUSDT.py")
    #     os.system("start cmd /K python MANAUSDT.py")
    #     os.system("start cmd /K python MATICUSDT.py")
    #     os.system("start cmd /K python MDXUSDT.py")
    #     os.system("start cmd /K python NEOUSDT.py")
    #     os.system("start cmd /K python OMGUSDT.py")
    #     os.system("start cmd /K python RVNUSDT.py")
    #     os.system("start cmd /K python SHIBUSDT.py")
    #     os.system("start cmd /K python SUSHIUSDT.py")
    #     os.system("start cmd /K python THETAUSDT.py")
    #     os.system("start cmd /K python TRXUSDT.py")
    #     os.system("start cmd /K python UNIUSDT.py")
    #     os.system("start cmd /K python XMRUSDT.py")
    #     os.system("start cmd /K python XRPUSDT.py")
    #     os.system("start cmd /K python XTZUSDT.py")
    #     print("start all coin")
    #     print(new)
    with cd(path_binancebot):  # autostart bot for binanace
        # we are in ~/Library
        new = os.getcwd()
        os.system("start cmd /K python checkuserbid.py")
        print("start auto start binance bot")
    with cd(path_binancebot):
        # we are in ~/Library
        new = os.getcwd()
        os.system("start cmd /K python autostartbinancebot.py")
        print(new)
   ##################################### WAZIRX ###########################################
    with cd(path_wazirxbot):
        # we are in ~/Library
        new = os.getcwd()
        os.system("start cmd /K python check_wzbot.py")
        print(new)
    ############## bot for percentage ############
    with cd(wazirxpercentagePath):
        # we are in ~/Library
        new = os.getcwd()
        os.system("start cmd /K python adaper.py")
        os.system("start cmd /K python atomper.py")
        os.system("start cmd /K python batper.py")
        os.system("start cmd /K python bchper.py")
        os.system("start cmd /K python btcper.py")
        os.system("start cmd /K python cakeper.py")
        os.system("start cmd /K python compper.py")
        os.system("start cmd /K python celrper.py")
        os.system("start cmd /K python crvper.py")
        os.system("start cmd /K python dashper.py")
        os.system("start cmd /K python dotper.py")
        os.system("start cmd /K python eosper.py")
        os.system("start cmd /K python etcper.py")
        os.system("start cmd /K python ethper.py")
        os.system("start cmd /K python filper.py")
        os.system("start cmd /K python fttper.py")
        os.system("start cmd /K python iostper.py")
        os.system("start cmd /K python linkper.py")
        os.system("start cmd /K python ltcper.py")
        os.system("start cmd /K python manaper.py")
        os.system("start cmd /K python maticper.py")
        os.system("start cmd /K python mdxper.py")
        os.system("start cmd /K python omgper.py")
        os.system("start cmd /K python shibper.py")
        os.system("start cmd /K python sushiper.py")
        os.system("start cmd /K python trxper.py")
        os.system("start cmd /K python uniper.py")
        os.system("start cmd /K python xrpper.py")
        print("start all coin percentage chage in wazirx")
        print(new)
    ############## bot for wazirx ############
    with cd(wazirxbotPath):
        # we are in ~/Library
        new = os.getcwd()
        os.system("start cmd /K python adainr.py")
        os.system("start cmd /K python atominr.py")
        os.system("start cmd /K python batinr.py")
        os.system("start cmd /K python bchinr.py")
        os.system("start cmd /K python btcinr.py")
        os.system("start cmd /K python cakeinr.py")
        os.system("start cmd /K python compinr.py")
        os.system("start cmd /K python crvinr.py")
        os.system("start cmd /K python celrinr.py")
        os.system("start cmd /K python dashinr.py")
        os.system("start cmd /K python dotinr.py")
        os.system("start cmd /K python eosinr.py")
        os.system("start cmd /K python etcinr.py")
        os.system("start cmd /K python ethinr.py")
        os.system("start cmd /K python filinr.py")
        os.system("start cmd /K python fttinr.py")
        os.system("start cmd /K python iostinr.py")
        os.system("start cmd /K python linkinr.py")
        os.system("start cmd /K python ltcinr.py")
        os.system("start cmd /K python manainr.py")
        os.system("start cmd /K python maticinr.py")
        os.system("start cmd /K python mdxinr.py")
        os.system("start cmd /K python omginr.py")
        os.system("start cmd /K python shibinr.py")
        os.system("start cmd /K python sushiinr.py")
        os.system("start cmd /K python trxinr.py")
        os.system("start cmd /K python uniinr.py")
        os.system("start cmd /K python xrpinr.py")
        print("start all coin")
        print(new)
   ################################ NODE CHART #################################
    # with cd(path_nodechart):
    #     # we are in ~/Library
    #     new = os.getcwd()
    #     os.system("start cmd /K python autostartnodechart.py")
    #     print('nodechart')
    ################################################################

   ################################ BSCSCAN CHART #################################
    with cd(bscscan):  # autostart bot for binanace
        # we are in ~/Library
        new = os.getcwd()
        os.system("start cmd /K python bscscan.py")
        print("start auto start binance bot")


startjob()
