import websockets
import requests
import asyncio
import json
import MySQLdb.cursors
import datetime
from flask import jsonify
from threading import Timer
from operator import itemgetter
import time
from numpy import arange
import numpy as np
from binance.client import Client
from binance.enums import *
from binance.exceptions import BinanceAPIException, BinanceOrderException


async def listen():
    url = "https://api.bscscan.com/api?module=account&action=tokenbalance&contractaddress=0xe679956bb9029a6b8d915e4a47e3d2826b188e62&address=0x91e9859b8615250cd439175815779434675e1fde&tag=latest&apikey=SMGYN9141FIHPIFGM2J92MYDUXG5YEZBC2"
    while True:
        await asyncio.sleep(10)
        r = requests.get(url)
        input_dict = r.json()
        db = MySQLdb.connect("localhost", "root", "", "xenbot")
        curs = db.cursor(MySQLdb.cursors.DictCursor)
        
        result = input_dict['result']
        curs.execute("""
                    UPDATE bscscan
                    SET result = %s
                    WHERE id = %s
                    """, (result, 1))
        db.commit()

loop = asyncio.get_event_loop()
try:
    asyncio.ensure_future(listen())
    loop.run_forever()
except KeyboardInterrupt:
    pass
finally:
    print("Closing Loop")
    loop.close()
