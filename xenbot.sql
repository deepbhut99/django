-- MySQL dump 10.13  Distrib 8.0.27, for Win64 (x86_64)
--
-- Host: localhost    Database: xenbot
-- ------------------------------------------------------
-- Server version	8.0.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin_login`
--

DROP TABLE IF EXISTS `admin_login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_login` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `username` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  `password_d` varchar(20) CHARACTER SET utf8 NOT NULL,
  `auto_refresh` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `apidata_get_check`
--

DROP TABLE IF EXISTS `apidata_get_check`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `apidata_get_check` (
  `id` int NOT NULL AUTO_INCREMENT,
  `timestamp` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `apidata_get_check_for_binance`
--

DROP TABLE IF EXISTS `apidata_get_check_for_binance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `apidata_get_check_for_binance` (
  `id` int NOT NULL AUTO_INCREMENT,
  `timestamp` longtext,
  `name_of_websocket` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_group` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_group_permissions` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `group_id` int NOT NULL,
  `permission_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_group_id_b120cbf9` (`group_id`),
  KEY `auth_group_permissions_permission_id_84c5c92e` (`permission_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_permission` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  KEY `auth_permission_content_type_id_2f476e4b` (`content_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=721 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(150) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_user_groups` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `group_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_user_id_6a12ed8b` (`user_id`),
  KEY `auth_user_groups_group_id_97559544` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `permission_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_permissions_user_id_a95ead1b` (`user_id`),
  KEY `auth_user_user_permissions_permission_id_1fbb5f2c` (`permission_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bids`
--

DROP TABLE IF EXISTS `bids`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bids` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user` int NOT NULL,
  `winRatio` float NOT NULL,
  `lastWinRatio` float NOT NULL,
  `type` int NOT NULL,
  `amount` float NOT NULL,
  `total_amount` float NOT NULL DEFAULT '0',
  `coin` varchar(10) CHARACTER SET utf8 NOT NULL,
  `value` float NOT NULL,
  `closeVal` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `mCloseVal` float NOT NULL,
  `growth` int NOT NULL,
  `result` int NOT NULL,
  `timestamp` bigint NOT NULL,
  `sec` int NOT NULL DEFAULT '0',
  `peers` longtext CHARACTER SET utf8,
  `bootCloseval` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=385 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `binancefuture_binancefuturebid`
--

DROP TABLE IF EXISTS `binancefuture_binancefuturebid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `binancefuture_binancefuturebid` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `created_date` bigint DEFAULT NULL,
  `firstbuyinamout` double DEFAULT NULL,
  `triger_prise` double DEFAULT NULL,
  `buy_sell_done` tinyint DEFAULT NULL COMMENT '0 = pending,\\r\\n1 = buy,\\r\\n2 = sell,\\r\\n3 = done,',
  `triger_prise_buy` double DEFAULT NULL,
  `triger_prise_sell` double DEFAULT NULL,
  `stoploss` double DEFAULT NULL,
  `takeprofit` double DEFAULT NULL,
  `quntaty` double DEFAULT NULL,
  `symbol` varchar(144) NOT NULL,
  `type` varchar(144) NOT NULL,
  `clientOrderId` varchar(144) DEFAULT NULL,
  `cumQty` varchar(144) DEFAULT NULL,
  `executedQty` varchar(144) DEFAULT NULL,
  `orderId` varchar(144) DEFAULT NULL,
  `origQty` varchar(144) DEFAULT NULL,
  `price` varchar(144) DEFAULT NULL,
  `side` varchar(144) DEFAULT NULL,
  `positionSide` varchar(144) DEFAULT NULL,
  `status` varchar(144) DEFAULT NULL,
  `stopPrice` varchar(144) DEFAULT NULL,
  `closePosition` varchar(144) DEFAULT NULL,
  `timeInForce` varchar(144) DEFAULT NULL,
  `activatePrice` varchar(144) DEFAULT NULL,
  `priceRate` varchar(144) DEFAULT NULL,
  `updateTime` varchar(144) DEFAULT NULL,
  `priceProtect` varchar(144) DEFAULT NULL,
  `apikey` longtext,
  `secretkey` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=537 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bot_stratage`
--

DROP TABLE IF EXISTS `bot_stratage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bot_stratage` (
  `id` int NOT NULL AUTO_INCREMENT,
  `tradeamount` float DEFAULT NULL,
  `buytoatl_total` float DEFAULT NULL,
  `coin` varchar(45) DEFAULT NULL,
  `id_whom_set` int DEFAULT NULL,
  `createdate` int DEFAULT NULL,
  `total_stratage` int DEFAULT '0',
  `cycle_status` int DEFAULT '0',
  `apikey` longtext,
  `apisecurity` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=248 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bottrading_teamcount`
--

DROP TABLE IF EXISTS `bottrading_teamcount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bottrading_teamcount` (
  `id` int NOT NULL AUTO_INCREMENT,
  `sponser_id` int DEFAULT NULL,
  `team_member` varchar(45) DEFAULT NULL,
  `total_number` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `btc_minute_data`
--

DROP TABLE IF EXISTS `btc_minute_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `btc_minute_data` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL,
  `timestamp` bigint NOT NULL,
  `sec1` double NOT NULL DEFAULT '0',
  `sec2` double NOT NULL DEFAULT '0',
  `sec3` double NOT NULL DEFAULT '0',
  `sec4` double NOT NULL DEFAULT '0',
  `sec5` double NOT NULL DEFAULT '0',
  `sec6` double NOT NULL DEFAULT '0',
  `sec7` double NOT NULL DEFAULT '0',
  `sec8` double NOT NULL DEFAULT '0',
  `sec9` double NOT NULL DEFAULT '0',
  `sec10` double NOT NULL DEFAULT '0',
  `sec11` double NOT NULL DEFAULT '0',
  `sec12` double NOT NULL DEFAULT '0',
  `sec13` double NOT NULL DEFAULT '0',
  `sec14` double NOT NULL DEFAULT '0',
  `sec15` double NOT NULL DEFAULT '0',
  `sec16` double NOT NULL DEFAULT '0',
  `sec17` double NOT NULL DEFAULT '0',
  `sec18` double NOT NULL DEFAULT '0',
  `sec19` double NOT NULL DEFAULT '0',
  `sec20` double NOT NULL DEFAULT '0',
  `sec21` double NOT NULL DEFAULT '0',
  `sec22` double NOT NULL DEFAULT '0',
  `sec23` double NOT NULL DEFAULT '0',
  `sec24` double NOT NULL DEFAULT '0',
  `sec25` double NOT NULL DEFAULT '0',
  `sec26` double NOT NULL DEFAULT '0',
  `sec27` double NOT NULL DEFAULT '0',
  `sec28` double NOT NULL DEFAULT '0',
  `sec29` double NOT NULL DEFAULT '0',
  `sec30` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `timestamp` (`timestamp`)
) ENGINE=InnoDB AUTO_INCREMENT=26533 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `btcdata`
--

DROP TABLE IF EXISTS `btcdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `btcdata` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL,
  `lastchanepercentage` float NOT NULL,
  `timestamp` bigint NOT NULL,
  `sec1` double NOT NULL DEFAULT '0',
  `sec2` double NOT NULL DEFAULT '0',
  `sec3` double NOT NULL DEFAULT '0',
  `sec4` double NOT NULL DEFAULT '0',
  `sec5` double NOT NULL DEFAULT '0',
  `sec6` double NOT NULL DEFAULT '0',
  `sec7` double NOT NULL DEFAULT '0',
  `sec8` double NOT NULL DEFAULT '0',
  `sec9` double NOT NULL DEFAULT '0',
  `sec10` double NOT NULL DEFAULT '0',
  `sec11` double NOT NULL DEFAULT '0',
  `sec12` double NOT NULL DEFAULT '0',
  `sec13` double NOT NULL DEFAULT '0',
  `sec14` double NOT NULL DEFAULT '0',
  `sec15` double NOT NULL DEFAULT '0',
  `sec16` double NOT NULL DEFAULT '0',
  `sec17` double NOT NULL DEFAULT '0',
  `sec18` double NOT NULL DEFAULT '0',
  `sec19` double NOT NULL DEFAULT '0',
  `sec20` double NOT NULL DEFAULT '0',
  `sec21` double NOT NULL DEFAULT '0',
  `sec22` double NOT NULL DEFAULT '0',
  `sec23` double NOT NULL DEFAULT '0',
  `sec24` double NOT NULL DEFAULT '0',
  `sec25` double NOT NULL DEFAULT '0',
  `sec26` double NOT NULL DEFAULT '0',
  `sec27` double NOT NULL DEFAULT '0',
  `sec28` double NOT NULL DEFAULT '0',
  `sec29` double NOT NULL DEFAULT '0',
  `sec30` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `timestamp` (`timestamp`)
) ENGINE=InnoDB AUTO_INCREMENT=599259 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `chek_botautotreding`
--

DROP TABLE IF EXISTS `chek_botautotreding`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `chek_botautotreding` (
  `id` int NOT NULL AUTO_INCREMENT,
  `AAVEUSDT` float DEFAULT NULL,
  `ADAUSDT` float DEFAULT NULL,
  `AKROUSDT` float DEFAULT NULL,
  `ANTUSDT` float DEFAULT NULL,
  `ATOMUSDT` float DEFAULT NULL,
  `BATUSDT` float DEFAULT NULL,
  `BCHUSDT` float DEFAULT NULL,
  `BNBUSDT` float DEFAULT NULL,
  `BTCUSDT` float DEFAULT NULL,
  `CAKEUSDT` float DEFAULT NULL,
  `COMPUSDT` float DEFAULT NULL,
  `COSUSDT` float DEFAULT NULL,
  `CRVUSDT` float DEFAULT NULL,
  `DASHUSDT` float DEFAULT NULL,
  `DOGEUSDT` float DEFAULT NULL,
  `DOTUSDT` float DEFAULT NULL,
  `EOSUSDT` float DEFAULT NULL,
  `ETHUSDT` float DEFAULT NULL,
  `ETCUSDT` float DEFAULT NULL,
  `FILUSDT` float DEFAULT NULL,
  `FTTUSDT` float DEFAULT NULL,
  `GRTUSDT` float DEFAULT NULL,
  `IOSTUSDT` float DEFAULT NULL,
  `IOTAUSDT` float DEFAULT NULL,
  `JSTUSDT` float DEFAULT NULL,
  `KAVAUSDT` float DEFAULT NULL,
  `LINKUSDT` float DEFAULT NULL,
  `LITUSDT` float DEFAULT NULL,
  `LTCUSDT` float DEFAULT NULL,
  `MANAUSDT` float DEFAULT NULL,
  `MDXUSDT` float DEFAULT NULL,
  `NEOUSDT` float DEFAULT NULL,
  `OMGUSDT` float DEFAULT NULL,
  `SUSHIUSDT` float DEFAULT NULL,
  `THETAUSDT` float DEFAULT NULL,
  `TRXUSDT` float DEFAULT NULL,
  `UNIUSDT` float DEFAULT NULL,
  `XMRUSDT` float DEFAULT NULL,
  `XRPUSDT` float DEFAULT NULL,
  `XTZUSDT` float DEFAULT NULL,
  `RVNUSDT` float DEFAULT NULL,
  `SHIBUSDT` float DEFAULT NULL,
  `MATICUSDT` float DEFAULT NULL,
  `CELRUSDT` float DEFAULT NULL,
  `creattime` int NOT NULL,
  `INCHUSDT` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ci_sessions`
--

DROP TABLE IF EXISTS `ci_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_admin_log` (
  `id` int NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int DEFAULT NULL,
  `user_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6` (`user_id`),
  CONSTRAINT `django_admin_log_chk_1` CHECK ((`action_flag` >= 0))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_content_type` (
  `id` int NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=MyISAM AUTO_INCREMENT=181 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_migrations` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_add_fund_history`
--

DROP TABLE IF EXISTS `efx_add_fund_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_add_fund_history` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL DEFAULT '0',
  `txn_id` longtext CHARACTER SET utf8,
  `amount` double NOT NULL DEFAULT '0',
  `payment_type` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `status` int NOT NULL DEFAULT '0' COMMENT '2 = Complete, 1 = Funds Sent, 0 = Pending, -1 = Cancelled, -2 = Cancelled: Below ShapeShift Minimum, -3 = Cancelled: Above ShapeShift Maximum, -4 = Cancelled: No ETH available to use as gas',
  `status_message` mediumtext CHARACTER SET utf8,
  `response_data` mediumtext CHARACTER SET utf8,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_admin_coin_setting`
--

DROP TABLE IF EXISTS `efx_admin_coin_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_admin_coin_setting` (
  `id` int NOT NULL AUTO_INCREMENT,
  `item_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `merchant_id` mediumtext CHARACTER SET utf8,
  `private_key` mediumtext CHARACTER SET utf8,
  `public_key` mediumtext CHARACTER SET utf8,
  `currency` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `amount` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_agency_commission`
--

DROP TABLE IF EXISTS `efx_agency_commission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_agency_commission` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `sponsor_user` varchar(50) CHARACTER SET utf8 NOT NULL,
  `level` varchar(10) CHARACTER SET utf8 NOT NULL,
  `commission_type` tinyint(1) NOT NULL COMMENT '1=agency; 0=trading',
  `amount` double NOT NULL DEFAULT '0',
  `earned` double NOT NULL DEFAULT '0',
  `created_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_agency_tree`
--

DROP TABLE IF EXISTS `efx_agency_tree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_agency_tree` (
  `id` int NOT NULL AUTO_INCREMENT,
  `sponsor_id` int NOT NULL,
  `sponsor_user` varchar(50) CHARACTER SET utf8 NOT NULL,
  `tree` text CHARACTER SET utf8,
  `tree_count` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_alert_logs`
--

DROP TABLE IF EXISTS `efx_alert_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_alert_logs` (
  `id` int NOT NULL AUTO_INCREMENT,
  `alert_id` int NOT NULL,
  `user_id` int NOT NULL,
  `type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `criteria` int NOT NULL,
  `timestamp` bigint NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_alerts`
--

DROP TABLE IF EXISTS `efx_alerts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_alerts` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `coin` varchar(10) CHARACTER SET utf8 NOT NULL DEFAULT 'BTC',
  `chart` varchar(20) CHARACTER SET utf8 NOT NULL,
  `price` float NOT NULL,
  `criteria` int NOT NULL,
  `expiry` bigint NOT NULL,
  `prev` float NOT NULL DEFAULT '0',
  `type` int NOT NULL,
  `alert_active` int NOT NULL DEFAULT '1',
  `active` int NOT NULL DEFAULT '1',
  `deleted` int NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_alerts_charts`
--

DROP TABLE IF EXISTS `efx_alerts_charts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_alerts_charts` (
  `id` int NOT NULL AUTO_INCREMENT,
  `chart` varchar(30) CHARACTER SET utf8 NOT NULL,
  `prevBTC` float NOT NULL,
  `prevETH` float NOT NULL,
  `active` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_algo_profit`
--

DROP TABLE IF EXISTS `efx_algo_profit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_algo_profit` (
  `id` int NOT NULL AUTO_INCREMENT,
  `admin_id` int NOT NULL,
  `profit` float NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_algo_setting`
--

DROP TABLE IF EXISTS `efx_algo_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_algo_setting` (
  `id` int NOT NULL AUTO_INCREMENT,
  `userid` int NOT NULL,
  `top_users_value` int NOT NULL,
  `other_users_value` int NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_api`
--

DROP TABLE IF EXISTS `efx_api`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_api` (
  `id` int NOT NULL AUTO_INCREMENT,
  `api` varchar(10) CHARACTER SET utf8 NOT NULL DEFAULT 'pro',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_bids_for_buysell`
--

DROP TABLE IF EXISTS `efx_bids_for_buysell`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_bids_for_buysell` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `buy_or_sell_amount` float DEFAULT '0',
  `btctousdpointaction` float DEFAULT NULL,
  `btctousdtprise` float DEFAULT NULL,
  `created_datetime` int DEFAULT NULL,
  `id_whichsellorbuy` int DEFAULT NULL,
  `buy_or_sell` int DEFAULT '0',
  `symbol` varchar(144) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT '0',
  `orderId` int DEFAULT '0',
  `orderListId` int DEFAULT '0',
  `clientOrderId` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `price` float DEFAULT '0',
  `origQty` float DEFAULT '0',
  `cummulativeQuoteQty` float DEFAULT '0',
  `status` varchar(144) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT '',
  `side` varchar(144) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT '',
  `price_with_commision` float DEFAULT '0',
  `qty_with_commision` float DEFAULT NULL,
  `commission` float DEFAULT NULL,
  `commissionAsset` varchar(144) DEFAULT '',
  `tradeId` int DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11533 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_bottrade_commission`
--

DROP TABLE IF EXISTS `efx_bottrade_commission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_bottrade_commission` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `amount` double NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_bottrade_tree`
--

DROP TABLE IF EXISTS `efx_bottrade_tree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_bottrade_tree` (
  `id` int NOT NULL AUTO_INCREMENT,
  `sponsor_id` int NOT NULL,
  `direct_count` int NOT NULL DEFAULT '0',
  `team_count` double NOT NULL DEFAULT '0',
  `team_tree` text,
  `level` tinyint(1) NOT NULL DEFAULT '0',
  `tree_count` double DEFAULT '0',
  `tree` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=95 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_coin_history`
--

DROP TABLE IF EXISTS `efx_coin_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_coin_history` (
  `id` int NOT NULL AUTO_INCREMENT,
  `current_price` double NOT NULL,
  `created_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_copy_history`
--

DROP TABLE IF EXISTS `efx_copy_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_copy_history` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `master_user_id` int NOT NULL,
  `trade_amount` double NOT NULL DEFAULT '0',
  `trade_date` bigint DEFAULT NULL,
  `profit` double NOT NULL DEFAULT '0',
  `commission` float NOT NULL DEFAULT '0',
  `type` tinyint(1) NOT NULL COMMENT '1=Up; -1=Down',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_copy_trade`
--

DROP TABLE IF EXISTS `efx_copy_trade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_copy_trade` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `master_user_id` int NOT NULL,
  `profit_sharing` double NOT NULL DEFAULT '0',
  `minimum_invest` double NOT NULL DEFAULT '0',
  `start_date` date DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=Active; 0=INactive',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_countries`
--

DROP TABLE IF EXISTS `efx_countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_countries` (
  `id` mediumint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `sortname` char(2) CHARACTER SET utf8 DEFAULT NULL,
  `phonecode` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=249 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_database_backup`
--

DROP TABLE IF EXISTS `efx_database_backup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_database_backup` (
  `id` int NOT NULL AUTO_INCREMENT,
  `file_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `created_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_exchange_history`
--

DROP TABLE IF EXISTS `efx_exchange_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_exchange_history` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `from_currency` varchar(5) CHARACTER SET utf8 NOT NULL,
  `to_currency` varchar(5) CHARACTER SET utf8 NOT NULL,
  `amount` double NOT NULL,
  `conv_amount` double NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_indicators`
--

DROP TABLE IF EXISTS `efx_indicators`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_indicators` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `chart_id` int NOT NULL,
  `title` text CHARACTER SET utf8 NOT NULL,
  `name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `options` varchar(500) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1899 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_log_history`
--

DROP TABLE IF EXISTS `efx_log_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_log_history` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL DEFAULT '0',
  `message` mediumtext CHARACTER SET utf8,
  `browser_name` mediumtext CHARACTER SET utf8,
  `browser_image` mediumtext CHARACTER SET utf8,
  `ip_address` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `os` varchar(15) CHARACTER SET utf8 DEFAULT NULL,
  `device` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `status` tinyint NOT NULL DEFAULT '0' COMMENT '1=success, 2= falied',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1443 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_logs`
--

DROP TABLE IF EXISTS `efx_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_logs` (
  `id` int NOT NULL AUTO_INCREMENT,
  `detail` varchar(40) CHARACTER SET utf8 NOT NULL,
  `date` datetime NOT NULL,
  `userid` int NOT NULL COMMENT 'efx_users -> ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_lucky_winner`
--

DROP TABLE IF EXISTS `efx_lucky_winner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_lucky_winner` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `amount` double NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_mailcount`
--

DROP TABLE IF EXISTS `efx_mailcount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_mailcount` (
  `id` int NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `count` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=109 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_news`
--

DROP TABLE IF EXISTS `efx_news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_news` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(20) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `created_time` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_news_`
--

DROP TABLE IF EXISTS `efx_news_`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_news_` (
  `id` int NOT NULL AUTO_INCREMENT,
  `document` text NOT NULL,
  `created_time` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_notification`
--

DROP TABLE IF EXISTS `efx_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_notification` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `type` varchar(15) CHARACTER SET latin1 NOT NULL,
  `target` varchar(256) CHARACTER SET latin1 NOT NULL COMMENT 'redirection',
  `detail` varchar(100) CHARACTER SET latin1 NOT NULL,
  `is_read` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=NotRead, 1=Read',
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=751 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_change_1inchusdt`
--

DROP TABLE IF EXISTS `efx_percentage_change_1inchusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_change_1inchusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_change_aaveusdt`
--

DROP TABLE IF EXISTS `efx_percentage_change_aaveusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_change_aaveusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=230672 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_change_adausdt`
--

DROP TABLE IF EXISTS `efx_percentage_change_adausdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_change_adausdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=239098 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_change_akrousdt`
--

DROP TABLE IF EXISTS `efx_percentage_change_akrousdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_change_akrousdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=156667 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_change_antusdt`
--

DROP TABLE IF EXISTS `efx_percentage_change_antusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_change_antusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=194622 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_change_atomusdt`
--

DROP TABLE IF EXISTS `efx_percentage_change_atomusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_change_atomusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=237730 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_change_batusdt`
--

DROP TABLE IF EXISTS `efx_percentage_change_batusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_change_batusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=216083 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_change_bchusdt`
--

DROP TABLE IF EXISTS `efx_percentage_change_bchusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_change_bchusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=234170 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_change_bnbusdt`
--

DROP TABLE IF EXISTS `efx_percentage_change_bnbusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_change_bnbusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=239105 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_change_btctusdt`
--

DROP TABLE IF EXISTS `efx_percentage_change_btctusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_change_btctusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_change_btcusdt`
--

DROP TABLE IF EXISTS `efx_percentage_change_btcusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_change_btcusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=239108 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_change_cakeusdt`
--

DROP TABLE IF EXISTS `efx_percentage_change_cakeusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_change_cakeusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=68959 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_change_celrusdt`
--

DROP TABLE IF EXISTS `efx_percentage_change_celrusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_change_celrusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=212842 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_change_compusdt`
--

DROP TABLE IF EXISTS `efx_percentage_change_compusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_change_compusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=379440 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_change_cosusdt`
--

DROP TABLE IF EXISTS `efx_percentage_change_cosusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_change_cosusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=139956 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_change_crvusdt`
--

DROP TABLE IF EXISTS `efx_percentage_change_crvusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_change_crvusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=224558 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_change_dashusdt`
--

DROP TABLE IF EXISTS `efx_percentage_change_dashusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_change_dashusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=223036 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_change_dogeusdt`
--

DROP TABLE IF EXISTS `efx_percentage_change_dogeusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_change_dogeusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=238906 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_change_dotusdt`
--

DROP TABLE IF EXISTS `efx_percentage_change_dotusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_change_dotusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=238598 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_change_eosusdt`
--

DROP TABLE IF EXISTS `efx_percentage_change_eosusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_change_eosusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=227775 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_change_etcusdt`
--

DROP TABLE IF EXISTS `efx_percentage_change_etcusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_change_etcusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=231087 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_change_ethusdt`
--

DROP TABLE IF EXISTS `efx_percentage_change_ethusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_change_ethusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=237961 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_change_filusdt`
--

DROP TABLE IF EXISTS `efx_percentage_change_filusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_change_filusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=236658 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_change_fttusdt`
--

DROP TABLE IF EXISTS `efx_percentage_change_fttusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_change_fttusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=217649 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_change_grtusdt`
--

DROP TABLE IF EXISTS `efx_percentage_change_grtusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_change_grtusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=236276 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_change_iostusdt`
--

DROP TABLE IF EXISTS `efx_percentage_change_iostusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_change_iostusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=209366 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_change_iotausdt`
--

DROP TABLE IF EXISTS `efx_percentage_change_iotausdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_change_iotausdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=212657 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_change_jstusdt`
--

DROP TABLE IF EXISTS `efx_percentage_change_jstusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_change_jstusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=206886 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_change_kavausdt`
--

DROP TABLE IF EXISTS `efx_percentage_change_kavausdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_change_kavausdt` (
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  `id` int NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=223194 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_change_linkusdt`
--

DROP TABLE IF EXISTS `efx_percentage_change_linkusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_change_linkusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=238577 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_change_litusdt`
--

DROP TABLE IF EXISTS `efx_percentage_change_litusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_change_litusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=208922 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_change_ltcusdt`
--

DROP TABLE IF EXISTS `efx_percentage_change_ltcusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_change_ltcusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=237577 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_change_manausdt`
--

DROP TABLE IF EXISTS `efx_percentage_change_manausdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_change_manausdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=238376 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_change_maticusdt`
--

DROP TABLE IF EXISTS `efx_percentage_change_maticusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_change_maticusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=238695 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_change_mdxusdt`
--

DROP TABLE IF EXISTS `efx_percentage_change_mdxusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_change_mdxusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=175317 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_change_neousdt`
--

DROP TABLE IF EXISTS `efx_percentage_change_neousdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_change_neousdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=222485 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_change_omgusdt`
--

DROP TABLE IF EXISTS `efx_percentage_change_omgusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_change_omgusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=218629 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_change_rvnusdt`
--

DROP TABLE IF EXISTS `efx_percentage_change_rvnusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_change_rvnusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=200561 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_change_shibusdt`
--

DROP TABLE IF EXISTS `efx_percentage_change_shibusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_change_shibusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=238701 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_change_sushiusdt`
--

DROP TABLE IF EXISTS `efx_percentage_change_sushiusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_change_sushiusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=224119 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_change_thetausdt`
--

DROP TABLE IF EXISTS `efx_percentage_change_thetausdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_change_thetausdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=231504 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_change_trxusdt`
--

DROP TABLE IF EXISTS `efx_percentage_change_trxusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_change_trxusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=238782 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_change_uniusdt`
--

DROP TABLE IF EXISTS `efx_percentage_change_uniusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_change_uniusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=232145 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_change_xmrusdt`
--

DROP TABLE IF EXISTS `efx_percentage_change_xmrusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_change_xmrusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=231106 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_change_xrpusdt`
--

DROP TABLE IF EXISTS `efx_percentage_change_xrpusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_change_xrpusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=239085 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_change_xtzusdt`
--

DROP TABLE IF EXISTS `efx_percentage_change_xtzusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_change_xtzusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=230887 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_future_change_aaveusdt`
--

DROP TABLE IF EXISTS `efx_percentage_future_change_aaveusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_future_change_aaveusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=43882 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_future_change_adausdt`
--

DROP TABLE IF EXISTS `efx_percentage_future_change_adausdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_future_change_adausdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=203810 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_future_change_antusdt`
--

DROP TABLE IF EXISTS `efx_percentage_future_change_antusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_future_change_antusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=112103 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_future_change_atomusdt`
--

DROP TABLE IF EXISTS `efx_percentage_future_change_atomusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_future_change_atomusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=157877 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_future_change_batusdt`
--

DROP TABLE IF EXISTS `efx_percentage_future_change_batusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_future_change_batusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=114580 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_future_change_bchusdt`
--

DROP TABLE IF EXISTS `efx_percentage_future_change_bchusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_future_change_bchusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=135909 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_future_change_bnbusdt`
--

DROP TABLE IF EXISTS `efx_percentage_future_change_bnbusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_future_change_bnbusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=187986 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_future_change_btcusdt`
--

DROP TABLE IF EXISTS `efx_percentage_future_change_btcusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_future_change_btcusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=221393 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_future_change_celrusdt`
--

DROP TABLE IF EXISTS `efx_percentage_future_change_celrusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_future_change_celrusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=120615 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_future_change_compusdt`
--

DROP TABLE IF EXISTS `efx_percentage_future_change_compusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_future_change_compusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=124508 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_future_change_crvusdt`
--

DROP TABLE IF EXISTS `efx_percentage_future_change_crvusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_future_change_crvusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=122147 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_future_change_dashusdt`
--

DROP TABLE IF EXISTS `efx_percentage_future_change_dashusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_future_change_dashusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=115267 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_future_change_dogeusdt`
--

DROP TABLE IF EXISTS `efx_percentage_future_change_dogeusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_future_change_dogeusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=141283 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_future_change_dotusdt`
--

DROP TABLE IF EXISTS `efx_percentage_future_change_dotusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_future_change_dotusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=171512 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_future_change_eosusdt`
--

DROP TABLE IF EXISTS `efx_percentage_future_change_eosusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_future_change_eosusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=126428 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_future_change_ethusdt`
--

DROP TABLE IF EXISTS `efx_percentage_future_change_ethusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_future_change_ethusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=142123 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_future_change_filusdt`
--

DROP TABLE IF EXISTS `efx_percentage_future_change_filusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_future_change_filusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=131312 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_future_change_grtusdt`
--

DROP TABLE IF EXISTS `efx_percentage_future_change_grtusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_future_change_grtusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=145157 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_future_change_iostusdt`
--

DROP TABLE IF EXISTS `efx_percentage_future_change_iostusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_future_change_iostusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=121661 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_future_change_iotausdt`
--

DROP TABLE IF EXISTS `efx_percentage_future_change_iotausdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_future_change_iotausdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=114340 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_future_change_kavausdt`
--

DROP TABLE IF EXISTS `efx_percentage_future_change_kavausdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_future_change_kavausdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=142413 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_future_change_linkusdt`
--

DROP TABLE IF EXISTS `efx_percentage_future_change_linkusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_future_change_linkusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=188438 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_future_change_litusdt`
--

DROP TABLE IF EXISTS `efx_percentage_future_change_litusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_future_change_litusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=154459 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_future_change_ltcusdt`
--

DROP TABLE IF EXISTS `efx_percentage_future_change_ltcusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_future_change_ltcusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=148867 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_future_change_manausdt`
--

DROP TABLE IF EXISTS `efx_percentage_future_change_manausdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_future_change_manausdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=148413 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_future_change_maticusdt`
--

DROP TABLE IF EXISTS `efx_percentage_future_change_maticusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_future_change_maticusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=158704 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_future_change_neousdt`
--

DROP TABLE IF EXISTS `efx_percentage_future_change_neousdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_future_change_neousdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=135223 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_future_change_omgusdt`
--

DROP TABLE IF EXISTS `efx_percentage_future_change_omgusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_future_change_omgusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=123145 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_future_change_rvnusdt`
--

DROP TABLE IF EXISTS `efx_percentage_future_change_rvnusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_future_change_rvnusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=105125 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_future_change_sushiusdt`
--

DROP TABLE IF EXISTS `efx_percentage_future_change_sushiusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_future_change_sushiusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=123406 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_future_change_thetatrxusdt`
--

DROP TABLE IF EXISTS `efx_percentage_future_change_thetatrxusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_future_change_thetatrxusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=80267 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_future_change_trxusdt`
--

DROP TABLE IF EXISTS `efx_percentage_future_change_trxusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_future_change_trxusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=173242 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_future_change_uniusdt`
--

DROP TABLE IF EXISTS `efx_percentage_future_change_uniusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_future_change_uniusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=130693 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_future_change_xmrusdt`
--

DROP TABLE IF EXISTS `efx_percentage_future_change_xmrusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_future_change_xmrusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=134969 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_future_change_xrpusdt`
--

DROP TABLE IF EXISTS `efx_percentage_future_change_xrpusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_future_change_xrpusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=171261 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_percentage_future_change_xtzusdt`
--

DROP TABLE IF EXISTS `efx_percentage_future_change_xtzusdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_percentage_future_change_xtzusdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `timestamp` bigint NOT NULL,
  `symbol` varchar(144) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `pricechange` float NOT NULL DEFAULT '0',
  `prisechangepercentage` float NOT NULL DEFAULT '0',
  `weightedaverageprice` float NOT NULL DEFAULT '0',
  `totaltradebaseasset` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=155523 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_perchange_wazirx12`
--

DROP TABLE IF EXISTS `efx_perchange_wazirx12`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_perchange_wazirx12` (
  `id` int NOT NULL AUTO_INCREMENT,
  `timestamp` longtext NOT NULL,
  `timestamp_wz` longtext NOT NULL,
  `open_crvinr` float NOT NULL,
  `close_crvinr` float NOT NULL,
  `high_crvinr` float NOT NULL,
  `low_crvinr` float NOT NULL,
  `volume_crvinr` float NOT NULL DEFAULT '0',
  `symbol_crvinr` varchar(144) NOT NULL DEFAULT '0',
  `pricechange_crvinr` float NOT NULL DEFAULT '0',
  `prisechangepercentage_crvinr` float NOT NULL DEFAULT '0',
  `weightedaverageprice_crvinr` float NOT NULL DEFAULT '0',
  `totaltradebaseasset_crvinr` float NOT NULL DEFAULT '0',
  `open_celrinr` float NOT NULL,
  `close_celrinr` float NOT NULL,
  `high_celrinr` float NOT NULL,
  `low_celrinr` float NOT NULL,
  `volume_celrinr` float NOT NULL DEFAULT '0',
  `symbol_celrinr` varchar(144) NOT NULL DEFAULT '0',
  `pricechange_celrinr` float NOT NULL DEFAULT '0',
  `prisechangepercentage_celrinr` float NOT NULL DEFAULT '0',
  `weightedaverageprice_celrinr` float NOT NULL DEFAULT '0',
  `totaltradebaseasset_celrinr` float NOT NULL DEFAULT '0',
  `open_dashinr` float NOT NULL,
  `close_dashinr` float NOT NULL,
  `high_dashinr` float NOT NULL,
  `low_dashinr` float NOT NULL,
  `volume_dashinr` float NOT NULL DEFAULT '0',
  `symbol_dashinr` varchar(144) NOT NULL DEFAULT '0',
  `pricechange_dashinr` float NOT NULL DEFAULT '0',
  `prisechangepercentage_dash` float NOT NULL DEFAULT '0',
  `weightedaverageprice_dashinr` float NOT NULL DEFAULT '0',
  `totaltradebaseasset_dashinr` float NOT NULL DEFAULT '0',
  `open_dotinr` float NOT NULL,
  `close_dotinr` float NOT NULL,
  `high_dotinr` float NOT NULL,
  `low_dotinr` float NOT NULL,
  `volume_dotinr` float NOT NULL DEFAULT '0',
  `symbol_dotinr` varchar(144) NOT NULL DEFAULT '0',
  `pricechange_dotinr` float NOT NULL DEFAULT '0',
  `prisechangepercentage_dotinr` float NOT NULL DEFAULT '0',
  `weightedaverageprice_dotinr` float NOT NULL DEFAULT '0',
  `totaltradebaseasset_dotinr` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10208 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_perchange_wazirx16`
--

DROP TABLE IF EXISTS `efx_perchange_wazirx16`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_perchange_wazirx16` (
  `id` int NOT NULL AUTO_INCREMENT,
  `timestamp` longtext NOT NULL,
  `timestamp_wz` longtext NOT NULL,
  `open_eosinr` float NOT NULL,
  `close_eosinr` float NOT NULL,
  `high_eosinr` float NOT NULL,
  `low_eosinr` float NOT NULL,
  `volume_eosinr` float NOT NULL DEFAULT '0',
  `symbol_eosinr` varchar(144) NOT NULL DEFAULT '0',
  `pricechange_eosinr` float NOT NULL DEFAULT '0',
  `prisechangepercentage_eosinr` float NOT NULL DEFAULT '0',
  `weightedaverageprice_eosinr` float NOT NULL DEFAULT '0',
  `totaltradebaseasset_eosinr` float NOT NULL DEFAULT '0',
  `open_ethinr` float NOT NULL,
  `close_ethinr` float NOT NULL,
  `high_ethinr` float NOT NULL,
  `low_ethinr` float NOT NULL,
  `volume_ethinr` float NOT NULL DEFAULT '0',
  `symbol_ethinr` varchar(144) NOT NULL DEFAULT '0',
  `pricechange_ethinr` float NOT NULL DEFAULT '0',
  `prisechangepercentage_ethinr` float NOT NULL DEFAULT '0',
  `weightedaverageprice_ethinr` float NOT NULL DEFAULT '0',
  `totaltradebaseasset_ethinr` float NOT NULL DEFAULT '0',
  `open_etcinr` float NOT NULL,
  `close_etcinr` float NOT NULL,
  `high_etcinr` float NOT NULL,
  `low_etcinr` float NOT NULL,
  `volume_etcinr` float NOT NULL DEFAULT '0',
  `symbol_etcinr` varchar(144) NOT NULL DEFAULT '0',
  `pricechange_etcinr` float NOT NULL DEFAULT '0',
  `prisechangepercentage_etcinr` float NOT NULL DEFAULT '0',
  `weightedaverageprice_etcinr` float NOT NULL DEFAULT '0',
  `totaltradebaseasset_etcinr` float NOT NULL DEFAULT '0',
  `open_filinr` float NOT NULL,
  `close_filinr` float NOT NULL,
  `high_filinr` float NOT NULL,
  `low_filinr` float NOT NULL,
  `volume_filinr` float NOT NULL DEFAULT '0',
  `symbol_filinr` varchar(144) NOT NULL DEFAULT '0',
  `pricechange_filinr` float NOT NULL DEFAULT '0',
  `prisechangepercentage_filinr` float NOT NULL DEFAULT '0',
  `weightedaverageprice_filinr` float NOT NULL DEFAULT '0',
  `totaltradebaseasset_filinr` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10080 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_perchange_wazirx20`
--

DROP TABLE IF EXISTS `efx_perchange_wazirx20`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_perchange_wazirx20` (
  `id` int NOT NULL AUTO_INCREMENT,
  `timestamp` longtext NOT NULL,
  `timestamp_wz` longtext NOT NULL,
  `open_fttinr` float NOT NULL,
  `close_fttinr` float NOT NULL,
  `high_fttinr` float NOT NULL,
  `low_fttinr` float NOT NULL,
  `volume_fttinr` float NOT NULL DEFAULT '0',
  `symbol_fttinr` varchar(144) NOT NULL DEFAULT '0',
  `pricechange_fttinr` float NOT NULL DEFAULT '0',
  `prisechangepercentage_fttinr` float NOT NULL DEFAULT '0',
  `weightedaverageprice_fttinr` float NOT NULL DEFAULT '0',
  `totaltradebaseasset_fttinr` float NOT NULL DEFAULT '0',
  `open_iostinr` float NOT NULL,
  `close_iostinr` float NOT NULL,
  `high_iostinr` float NOT NULL,
  `low_iostinr` float NOT NULL,
  `volume_iostinr` float NOT NULL DEFAULT '0',
  `symbol_iostinr` varchar(144) NOT NULL DEFAULT '0',
  `pricechange_iostinr` float NOT NULL DEFAULT '0',
  `prisechangepercentage_iostinr` float NOT NULL DEFAULT '0',
  `weightedaverageprice_iostinr` float NOT NULL DEFAULT '0',
  `totaltradebaseasset_iostinr` float NOT NULL DEFAULT '0',
  `open_linkinr` float NOT NULL,
  `close_linkinr` float NOT NULL,
  `high_linkinr` float NOT NULL,
  `low_linkinr` float NOT NULL,
  `volume_linkinr` float NOT NULL DEFAULT '0',
  `symbol_linkinr` varchar(144) NOT NULL DEFAULT '0',
  `pricechange_linkinr` float NOT NULL DEFAULT '0',
  `prisechangepercentage_linkinr` float NOT NULL DEFAULT '0',
  `weightedaverageprice_linkinr` float NOT NULL DEFAULT '0',
  `totaltradebaseasset_linkinr` float NOT NULL DEFAULT '0',
  `open_ltcinr` float NOT NULL,
  `close_ltcinr` float NOT NULL,
  `high_ltcinr` float NOT NULL,
  `low_ltcinr` float NOT NULL,
  `volume_ltcinr` float NOT NULL DEFAULT '0',
  `symbol_ltcinr` varchar(144) NOT NULL DEFAULT '0',
  `pricechange_ltcinr` float NOT NULL DEFAULT '0',
  `prisechangepercentage_ltcinr` float NOT NULL DEFAULT '0',
  `weightedaverageprice_ltcinr` float NOT NULL DEFAULT '0',
  `totaltradebaseasset_ltcinr` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10025 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_perchange_wazirx24`
--

DROP TABLE IF EXISTS `efx_perchange_wazirx24`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_perchange_wazirx24` (
  `id` int NOT NULL AUTO_INCREMENT,
  `timestamp` longtext NOT NULL,
  `timestamp_wz` longtext NOT NULL,
  `open_manainr` float NOT NULL,
  `close_manainr` float NOT NULL,
  `high_manainr` float NOT NULL,
  `low_manainr` float NOT NULL,
  `volume_manainr` float NOT NULL DEFAULT '0',
  `symbol_manainr` varchar(144) NOT NULL DEFAULT '0',
  `pricechange_manainr` float NOT NULL DEFAULT '0',
  `prisechangepercentage_manainr` float NOT NULL DEFAULT '0',
  `weightedaverageprice_manainr` float NOT NULL DEFAULT '0',
  `totaltradebaseasset_manainr` float NOT NULL DEFAULT '0',
  `open_mdxinr` float NOT NULL,
  `close_mdxinr` float NOT NULL,
  `high_mdxinr` float NOT NULL,
  `low_mdxinr` float NOT NULL,
  `volume_mdxinr` float NOT NULL DEFAULT '0',
  `symbol_mdxinr` varchar(144) NOT NULL DEFAULT '0',
  `pricechange_mdxinr` float NOT NULL DEFAULT '0',
  `prisechangepercentage_mdxinr` float NOT NULL DEFAULT '0',
  `weightedaverageprice_mdxinr` float NOT NULL DEFAULT '0',
  `totaltradebaseasset_mdxinr` float NOT NULL DEFAULT '0',
  `open_omginr` float NOT NULL,
  `close_omginr` float NOT NULL,
  `high_omginr` float NOT NULL,
  `low_omginr` float NOT NULL,
  `volume_omginr` float NOT NULL DEFAULT '0',
  `symbol_omginr` varchar(144) NOT NULL DEFAULT '0',
  `pricechange_omginr` float NOT NULL DEFAULT '0',
  `prisechangepercentage_omginr` float NOT NULL DEFAULT '0',
  `weightedaverageprice_omginr` float NOT NULL DEFAULT '0',
  `totaltradebaseasset_omginr` float NOT NULL DEFAULT '0',
  `open_sushiinr` float NOT NULL,
  `close_sushiinr` float NOT NULL,
  `high_sushiinr` float NOT NULL,
  `low_sushiinr` float NOT NULL,
  `volume_sushiinr` float NOT NULL DEFAULT '0',
  `symbol_sushiinr` varchar(144) NOT NULL DEFAULT '0',
  `pricechange_sushiinr` float NOT NULL DEFAULT '0',
  `prisechangepercentage_sushiinr` float NOT NULL DEFAULT '0',
  `weightedaverageprice_sushiinr` float NOT NULL DEFAULT '0',
  `totaltradebaseasset_sushiinr` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10026 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_perchange_wazirx28`
--

DROP TABLE IF EXISTS `efx_perchange_wazirx28`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_perchange_wazirx28` (
  `id` int NOT NULL AUTO_INCREMENT,
  `timestamp` longtext NOT NULL,
  `timestamp_wz` longtext NOT NULL,
  `open_trxinr` float NOT NULL,
  `close_trxinr` float NOT NULL,
  `high_trxinr` float NOT NULL,
  `low_trxinr` float NOT NULL,
  `volume_trxinr` float NOT NULL DEFAULT '0',
  `symbol_trxinr` varchar(144) NOT NULL DEFAULT '0',
  `pricechange_trxinr` float NOT NULL DEFAULT '0',
  `prisechangepercentage_trxinr` float NOT NULL DEFAULT '0',
  `weightedaverageprice_trxinr` float NOT NULL DEFAULT '0',
  `totaltradebaseasset_trxinr` float NOT NULL DEFAULT '0',
  `open_uniinr` float NOT NULL,
  `close_uniinr` float NOT NULL,
  `high_uniinr` float NOT NULL,
  `low_uniinr` float NOT NULL,
  `volume_uniinr` float NOT NULL DEFAULT '0',
  `symbol_uniinr` varchar(144) NOT NULL DEFAULT '0',
  `pricechange_uniinr` float NOT NULL DEFAULT '0',
  `prisechangepercentage_uniinr` float NOT NULL DEFAULT '0',
  `weightedaverageprice_uniinr` float NOT NULL DEFAULT '0',
  `totaltradebaseasset_uniinr` float NOT NULL DEFAULT '0',
  `open_xrpinr` float NOT NULL,
  `close_xrpinr` float NOT NULL,
  `high_xrpinr` float NOT NULL,
  `low_xrpinr` float NOT NULL,
  `volume_xrpinr` float NOT NULL DEFAULT '0',
  `symbol_xrpinr` varchar(144) NOT NULL DEFAULT '0',
  `pricechange_xrpinr` float NOT NULL DEFAULT '0',
  `prisechangepercentage_xrpinr` float NOT NULL DEFAULT '0',
  `weightedaverageprice_xrpinr` float NOT NULL DEFAULT '0',
  `totaltradebaseasset_xrpinr` float NOT NULL DEFAULT '0',
  `open_shibinr` float NOT NULL,
  `close_shibinr` float NOT NULL,
  `high_shibinr` float NOT NULL,
  `low_shibinr` float NOT NULL,
  `volume_shibinr` float NOT NULL DEFAULT '0',
  `symbol_shibinr` varchar(144) NOT NULL DEFAULT '0',
  `pricechange_shibinr` float NOT NULL DEFAULT '0',
  `prisechangepercentage_shibinr` float NOT NULL DEFAULT '0',
  `weightedaverageprice_shibinr` float NOT NULL DEFAULT '0',
  `totaltradebaseasset_shibinr` float NOT NULL DEFAULT '0',
  `open_maticinr` float NOT NULL,
  `close_maticinr` float NOT NULL,
  `high_maticinr` float NOT NULL,
  `low_maticinr` float NOT NULL,
  `volume_maticinr` float NOT NULL DEFAULT '0',
  `symbol_maticinr` varchar(144) NOT NULL DEFAULT '0',
  `pricechange_maticinr` float NOT NULL DEFAULT '0',
  `prisechangepercentage_maticinr` float NOT NULL DEFAULT '0',
  `weightedaverageprice_maticinr` float NOT NULL DEFAULT '0',
  `totaltradebaseasset_maticinr` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11005 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_perchange_wazirx4`
--

DROP TABLE IF EXISTS `efx_perchange_wazirx4`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_perchange_wazirx4` (
  `id` int NOT NULL AUTO_INCREMENT,
  `timestamp` longtext NOT NULL,
  `timestamp_wz` longtext NOT NULL,
  `open_adainr` float NOT NULL,
  `close_adainr` float NOT NULL,
  `high_adainr` float NOT NULL,
  `low_adainr` float NOT NULL,
  `volume_adainr` float NOT NULL DEFAULT '0',
  `symbol_adainr` varchar(144) NOT NULL DEFAULT '0',
  `pricechange_adainr` float NOT NULL DEFAULT '0',
  `prisechangepercentage_adainr` float NOT NULL DEFAULT '0',
  `weightedaverageprice_adainr` float NOT NULL DEFAULT '0',
  `totaltradebaseasset_adainr` float NOT NULL DEFAULT '0',
  `open_atominr` float NOT NULL,
  `close_atominr` float NOT NULL,
  `high_atominr` float NOT NULL,
  `low_atominr` float NOT NULL,
  `volume_atominr` float NOT NULL DEFAULT '0',
  `symbol_atominr` varchar(144) NOT NULL DEFAULT '0',
  `pricechange_atominr` float NOT NULL DEFAULT '0',
  `prisechangepercentage_atominr` float NOT NULL DEFAULT '0',
  `weightedaverageprice_atominr` float NOT NULL DEFAULT '0',
  `totaltradebaseasset_atominr` float NOT NULL DEFAULT '0',
  `open_batinr` float NOT NULL,
  `close_batinr` float NOT NULL,
  `high_batinr` float NOT NULL,
  `low_batinr` float NOT NULL,
  `volume_batinr` float NOT NULL DEFAULT '0',
  `symbol_batinr` varchar(144) NOT NULL DEFAULT '0',
  `pricechange_batinr` float NOT NULL DEFAULT '0',
  `prisechangepercentage_batinr` float NOT NULL DEFAULT '0',
  `weightedaverageprice_batinr` float NOT NULL DEFAULT '0',
  `totaltradebaseasset_batinr` float NOT NULL DEFAULT '0',
  `open_bchinr` float NOT NULL,
  `close_bchinr` float NOT NULL,
  `high_bchinr` float NOT NULL,
  `low_bchinr` float NOT NULL,
  `volume_bchinr` float NOT NULL DEFAULT '0',
  `symbol_bchinr` varchar(144) NOT NULL DEFAULT '0',
  `pricechange_bchinr` float NOT NULL DEFAULT '0',
  `prisechangepercentage_bchinr` float NOT NULL DEFAULT '0',
  `weightedaverageprice_bchinr` float NOT NULL DEFAULT '0',
  `totaltradebaseasset_bchinr` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15175 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_perchange_wazirx8`
--

DROP TABLE IF EXISTS `efx_perchange_wazirx8`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_perchange_wazirx8` (
  `id` int NOT NULL AUTO_INCREMENT,
  `timestamp` longtext NOT NULL,
  `timestamp_wz` longtext NOT NULL,
  `open_bnbinr` float NOT NULL,
  `close_bnbinr` float NOT NULL,
  `high_bnbinr` float NOT NULL,
  `low_bnbinr` float NOT NULL,
  `volume_bnbinr` float NOT NULL DEFAULT '0',
  `symbol_bnbinr` varchar(144) NOT NULL DEFAULT '0',
  `pricechange_bnbinr` float NOT NULL DEFAULT '0',
  `prisechangepercentage_bnbinr` float NOT NULL DEFAULT '0',
  `weightedaverageprice_bnbinr` float NOT NULL DEFAULT '0',
  `totaltradebaseasset_bnbinr` float NOT NULL DEFAULT '0',
  `open_btcinr` float NOT NULL,
  `close_btcinr` float NOT NULL,
  `high_btcinr` float NOT NULL,
  `low_btcinr` float NOT NULL,
  `volume_btcinr` float NOT NULL DEFAULT '0',
  `symbol_btcinr` varchar(144) NOT NULL DEFAULT '0',
  `pricechange_btcinr` float NOT NULL DEFAULT '0',
  `prisechangepercentage_btcinr` float NOT NULL DEFAULT '0',
  `weightedaverageprice_btcinr` float NOT NULL DEFAULT '0',
  `totaltradebaseasset_btcinr` float NOT NULL DEFAULT '0',
  `open_cakeinr` float NOT NULL,
  `close_cakeinr` float NOT NULL,
  `high_cakeinr` float NOT NULL,
  `low_cakeinr` float NOT NULL,
  `volume_cakeinr` float NOT NULL DEFAULT '0',
  `symbol_cakeinr` varchar(144) NOT NULL DEFAULT '0',
  `pricechange_cakeinr` float NOT NULL DEFAULT '0',
  `prisechangepercentage_cakeinr` float NOT NULL DEFAULT '0',
  `weightedaverageprice_cakeinr` float NOT NULL DEFAULT '0',
  `totaltradebaseasset_cakeinr` float NOT NULL DEFAULT '0',
  `open_compinr` float NOT NULL,
  `close_compinr` float NOT NULL,
  `high_compinr` float NOT NULL,
  `low_compinr` float NOT NULL,
  `volume_compinr` float NOT NULL DEFAULT '0',
  `symbol_compinr` varchar(144) NOT NULL DEFAULT '0',
  `pricechange_compinr` float NOT NULL DEFAULT '0',
  `prisechangepercentage_compinr` float NOT NULL DEFAULT '0',
  `weightedaverageprice_compinr` float NOT NULL DEFAULT '0',
  `totaltradebaseasset_compinr` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10251 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_perchange_wz_adainr`
--

DROP TABLE IF EXISTS `efx_perchange_wz_adainr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_perchange_wz_adainr` (
  `id` int NOT NULL AUTO_INCREMENT,
  `timestamp_wz` longtext,
  `open` float DEFAULT NULL,
  `close` float DEFAULT NULL,
  `high` float DEFAULT NULL,
  `low` float DEFAULT NULL,
  `volume` float DEFAULT '0',
  `symbol` varchar(144) DEFAULT '0',
  `pricechange` float DEFAULT '0',
  `prisechangepercentage` float DEFAULT '0',
  `weightedaverageprice` float DEFAULT '0',
  `totaltradebaseasset` float DEFAULT '0',
  `timestamp` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=29873 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_perchange_wz_atominr`
--

DROP TABLE IF EXISTS `efx_perchange_wz_atominr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_perchange_wz_atominr` (
  `id` int NOT NULL AUTO_INCREMENT,
  `timestamp_wz` longtext,
  `open` float DEFAULT NULL,
  `close` float DEFAULT NULL,
  `high` float DEFAULT NULL,
  `low` float DEFAULT NULL,
  `volume` float DEFAULT '0',
  `symbol` varchar(144) DEFAULT '0',
  `pricechange` float DEFAULT '0',
  `prisechangepercentage` float DEFAULT '0',
  `weightedaverageprice` float DEFAULT '0',
  `totaltradebaseasset` float DEFAULT '0',
  `timestamp` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=124481 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_perchange_wz_batinr`
--

DROP TABLE IF EXISTS `efx_perchange_wz_batinr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_perchange_wz_batinr` (
  `id` int NOT NULL AUTO_INCREMENT,
  `timestamp_wz` longtext,
  `open` float DEFAULT NULL,
  `close` float DEFAULT NULL,
  `high` float DEFAULT NULL,
  `low` float DEFAULT NULL,
  `volume` float DEFAULT '0',
  `symbol` varchar(144) DEFAULT '0',
  `pricechange` float DEFAULT '0',
  `prisechangepercentage` float DEFAULT '0',
  `weightedaverageprice` float DEFAULT '0',
  `totaltradebaseasset` float DEFAULT '0',
  `timestamp` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=96925 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_perchange_wz_bchinr`
--

DROP TABLE IF EXISTS `efx_perchange_wz_bchinr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_perchange_wz_bchinr` (
  `id` int NOT NULL AUTO_INCREMENT,
  `timestamp_wz` longtext,
  `open` float DEFAULT NULL,
  `close` float DEFAULT NULL,
  `high` float DEFAULT NULL,
  `low` float DEFAULT NULL,
  `volume` float DEFAULT '0',
  `symbol` varchar(144) DEFAULT '0',
  `pricechange` float DEFAULT '0',
  `prisechangepercentage` float DEFAULT '0',
  `weightedaverageprice` float DEFAULT '0',
  `totaltradebaseasset` float DEFAULT '0',
  `timestamp` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=94393 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_perchange_wz_bnbinr`
--

DROP TABLE IF EXISTS `efx_perchange_wz_bnbinr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_perchange_wz_bnbinr` (
  `id` int NOT NULL AUTO_INCREMENT,
  `timestamp_wz` longtext,
  `open` float DEFAULT NULL,
  `close` float DEFAULT NULL,
  `high` float DEFAULT NULL,
  `low` float DEFAULT NULL,
  `volume` float DEFAULT '0',
  `symbol` varchar(144) DEFAULT '0',
  `pricechange` float DEFAULT '0',
  `prisechangepercentage` float DEFAULT '0',
  `weightedaverageprice` float DEFAULT '0',
  `totaltradebaseasset` float DEFAULT '0',
  `timestamp` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=109747 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_perchange_wz_btcinr`
--

DROP TABLE IF EXISTS `efx_perchange_wz_btcinr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_perchange_wz_btcinr` (
  `id` int NOT NULL AUTO_INCREMENT,
  `timestamp_wz` longtext,
  `open` float DEFAULT NULL,
  `close` float DEFAULT NULL,
  `high` float DEFAULT NULL,
  `low` float DEFAULT NULL,
  `volume` float DEFAULT '0',
  `symbol` varchar(144) DEFAULT '0',
  `pricechange` float DEFAULT '0',
  `prisechangepercentage` float DEFAULT '0',
  `weightedaverageprice` float DEFAULT '0',
  `totaltradebaseasset` float DEFAULT '0',
  `timestamp` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=125802 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_perchange_wz_cakeinr`
--

DROP TABLE IF EXISTS `efx_perchange_wz_cakeinr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_perchange_wz_cakeinr` (
  `id` int NOT NULL AUTO_INCREMENT,
  `timestamp_wz` longtext,
  `open` float DEFAULT NULL,
  `close` float DEFAULT NULL,
  `high` float DEFAULT NULL,
  `low` float DEFAULT NULL,
  `volume` float DEFAULT '0',
  `symbol` varchar(144) DEFAULT '0',
  `pricechange` float DEFAULT '0',
  `prisechangepercentage` float DEFAULT '0',
  `weightedaverageprice` float DEFAULT '0',
  `totaltradebaseasset` float DEFAULT '0',
  `timestamp` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=94580 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_perchange_wz_celrinr`
--

DROP TABLE IF EXISTS `efx_perchange_wz_celrinr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_perchange_wz_celrinr` (
  `id` int NOT NULL AUTO_INCREMENT,
  `timestamp_wz` longtext,
  `open` float DEFAULT NULL,
  `close` float DEFAULT NULL,
  `high` float DEFAULT NULL,
  `low` float DEFAULT NULL,
  `volume` float DEFAULT '0',
  `symbol` varchar(144) DEFAULT '0',
  `pricechange` float DEFAULT '0',
  `prisechangepercentage` float DEFAULT '0',
  `weightedaverageprice` float DEFAULT '0',
  `totaltradebaseasset` float DEFAULT '0',
  `timestamp` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=105332 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_perchange_wz_compinr`
--

DROP TABLE IF EXISTS `efx_perchange_wz_compinr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_perchange_wz_compinr` (
  `id` int NOT NULL AUTO_INCREMENT,
  `timestamp_wz` longtext,
  `open` float DEFAULT NULL,
  `close` float DEFAULT NULL,
  `high` float DEFAULT NULL,
  `low` float DEFAULT NULL,
  `volume` float DEFAULT '0',
  `symbol` varchar(144) DEFAULT '0',
  `pricechange` float DEFAULT '0',
  `prisechangepercentage` float DEFAULT '0',
  `weightedaverageprice` float DEFAULT '0',
  `totaltradebaseasset` float DEFAULT '0',
  `timestamp` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=94160 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_perchange_wz_crvinr`
--

DROP TABLE IF EXISTS `efx_perchange_wz_crvinr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_perchange_wz_crvinr` (
  `id` int NOT NULL AUTO_INCREMENT,
  `timestamp_wz` longtext,
  `open` float DEFAULT NULL,
  `close` float DEFAULT NULL,
  `high` float DEFAULT NULL,
  `low` float DEFAULT NULL,
  `volume` float DEFAULT '0',
  `symbol` varchar(144) DEFAULT '0',
  `pricechange` float DEFAULT '0',
  `prisechangepercentage` float DEFAULT '0',
  `weightedaverageprice` float DEFAULT '0',
  `totaltradebaseasset` float DEFAULT '0',
  `timestamp` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=103135 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_perchange_wz_dashinr`
--

DROP TABLE IF EXISTS `efx_perchange_wz_dashinr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_perchange_wz_dashinr` (
  `id` int NOT NULL AUTO_INCREMENT,
  `timestamp_wz` longtext,
  `open` float DEFAULT NULL,
  `close` float DEFAULT NULL,
  `high` float DEFAULT NULL,
  `low` float DEFAULT NULL,
  `volume` float DEFAULT '0',
  `symbol` varchar(144) DEFAULT '0',
  `pricechange` float DEFAULT '0',
  `prisechangepercentage` float DEFAULT '0',
  `weightedaverageprice` float DEFAULT '0',
  `totaltradebaseasset` float DEFAULT '0',
  `timestamp` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=94143 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_perchange_wz_dotinr`
--

DROP TABLE IF EXISTS `efx_perchange_wz_dotinr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_perchange_wz_dotinr` (
  `id` int NOT NULL AUTO_INCREMENT,
  `timestamp_wz` longtext,
  `open` float DEFAULT NULL,
  `close` float DEFAULT NULL,
  `high` float DEFAULT NULL,
  `low` float DEFAULT NULL,
  `volume` float DEFAULT '0',
  `symbol` varchar(144) DEFAULT '0',
  `pricechange` float DEFAULT '0',
  `prisechangepercentage` float DEFAULT '0',
  `weightedaverageprice` float DEFAULT '0',
  `totaltradebaseasset` float DEFAULT '0',
  `timestamp` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=105249 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_perchange_wz_eosinr`
--

DROP TABLE IF EXISTS `efx_perchange_wz_eosinr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_perchange_wz_eosinr` (
  `id` int NOT NULL AUTO_INCREMENT,
  `timestamp_wz` longtext,
  `open` float DEFAULT NULL,
  `close` float DEFAULT NULL,
  `high` float DEFAULT NULL,
  `low` float DEFAULT NULL,
  `volume` float DEFAULT '0',
  `symbol` varchar(144) DEFAULT '0',
  `pricechange` float DEFAULT '0',
  `prisechangepercentage` float DEFAULT '0',
  `weightedaverageprice` float DEFAULT '0',
  `totaltradebaseasset` float DEFAULT '0',
  `timestamp` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=96314 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_perchange_wz_etcinr`
--

DROP TABLE IF EXISTS `efx_perchange_wz_etcinr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_perchange_wz_etcinr` (
  `id` int NOT NULL AUTO_INCREMENT,
  `timestamp_wz` longtext,
  `open` float DEFAULT NULL,
  `close` float DEFAULT NULL,
  `high` float DEFAULT NULL,
  `low` float DEFAULT NULL,
  `volume` float DEFAULT '0',
  `symbol` varchar(144) DEFAULT '0',
  `pricechange` float DEFAULT '0',
  `prisechangepercentage` float DEFAULT '0',
  `weightedaverageprice` float DEFAULT '0',
  `totaltradebaseasset` float DEFAULT '0',
  `timestamp` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=109358 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_perchange_wz_ethinr`
--

DROP TABLE IF EXISTS `efx_perchange_wz_ethinr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_perchange_wz_ethinr` (
  `id` int NOT NULL AUTO_INCREMENT,
  `timestamp_wz` longtext,
  `open` float DEFAULT NULL,
  `close` float DEFAULT NULL,
  `high` float DEFAULT NULL,
  `low` float DEFAULT NULL,
  `volume` float DEFAULT '0',
  `symbol` varchar(144) DEFAULT '0',
  `pricechange` float DEFAULT '0',
  `prisechangepercentage` float DEFAULT '0',
  `weightedaverageprice` float DEFAULT '0',
  `totaltradebaseasset` float DEFAULT '0',
  `timestamp` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=105211 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_perchange_wz_filinr`
--

DROP TABLE IF EXISTS `efx_perchange_wz_filinr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_perchange_wz_filinr` (
  `id` int NOT NULL AUTO_INCREMENT,
  `timestamp_wz` longtext,
  `open` float DEFAULT NULL,
  `close` float DEFAULT NULL,
  `high` float DEFAULT NULL,
  `low` float DEFAULT NULL,
  `volume` float DEFAULT '0',
  `symbol` varchar(144) DEFAULT '0',
  `pricechange` float DEFAULT '0',
  `prisechangepercentage` float DEFAULT '0',
  `weightedaverageprice` float DEFAULT '0',
  `totaltradebaseasset` float DEFAULT '0',
  `timestamp` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=100245 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_perchange_wz_fttinr`
--

DROP TABLE IF EXISTS `efx_perchange_wz_fttinr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_perchange_wz_fttinr` (
  `id` int NOT NULL AUTO_INCREMENT,
  `timestamp_wz` longtext,
  `open` float DEFAULT NULL,
  `close` float DEFAULT NULL,
  `high` float DEFAULT NULL,
  `low` float DEFAULT NULL,
  `volume` float DEFAULT '0',
  `symbol` varchar(144) DEFAULT '0',
  `pricechange` float DEFAULT '0',
  `prisechangepercentage` float DEFAULT '0',
  `weightedaverageprice` float DEFAULT '0',
  `totaltradebaseasset` float DEFAULT '0',
  `timestamp` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=98883 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_perchange_wz_iostinr`
--

DROP TABLE IF EXISTS `efx_perchange_wz_iostinr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_perchange_wz_iostinr` (
  `id` int NOT NULL AUTO_INCREMENT,
  `timestamp_wz` longtext,
  `open` float DEFAULT NULL,
  `close` float DEFAULT NULL,
  `high` float DEFAULT NULL,
  `low` float DEFAULT NULL,
  `volume` float DEFAULT '0',
  `symbol` varchar(144) DEFAULT '0',
  `pricechange` float DEFAULT '0',
  `prisechangepercentage` float DEFAULT '0',
  `weightedaverageprice` float DEFAULT '0',
  `totaltradebaseasset` float DEFAULT '0',
  `timestamp` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=95064 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_perchange_wz_linkinr`
--

DROP TABLE IF EXISTS `efx_perchange_wz_linkinr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_perchange_wz_linkinr` (
  `id` int NOT NULL AUTO_INCREMENT,
  `timestamp_wz` longtext,
  `open` float DEFAULT NULL,
  `close` float DEFAULT NULL,
  `high` float DEFAULT NULL,
  `low` float DEFAULT NULL,
  `volume` float DEFAULT '0',
  `symbol` varchar(144) DEFAULT '0',
  `pricechange` float DEFAULT '0',
  `prisechangepercentage` float DEFAULT '0',
  `weightedaverageprice` float DEFAULT '0',
  `totaltradebaseasset` float DEFAULT '0',
  `timestamp` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=105887 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_perchange_wz_ltcinr`
--

DROP TABLE IF EXISTS `efx_perchange_wz_ltcinr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_perchange_wz_ltcinr` (
  `id` int NOT NULL AUTO_INCREMENT,
  `timestamp_wz` longtext,
  `open` float DEFAULT NULL,
  `close` float DEFAULT NULL,
  `high` float DEFAULT NULL,
  `low` float DEFAULT NULL,
  `volume` float DEFAULT '0',
  `symbol` varchar(144) DEFAULT '0',
  `pricechange` float DEFAULT '0',
  `prisechangepercentage` float DEFAULT '0',
  `weightedaverageprice` float DEFAULT '0',
  `totaltradebaseasset` float DEFAULT '0',
  `timestamp` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=116005 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_perchange_wz_manainr`
--

DROP TABLE IF EXISTS `efx_perchange_wz_manainr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_perchange_wz_manainr` (
  `id` int NOT NULL AUTO_INCREMENT,
  `timestamp_wz` longtext,
  `open` float DEFAULT NULL,
  `close` float DEFAULT NULL,
  `high` float DEFAULT NULL,
  `low` float DEFAULT NULL,
  `volume` float DEFAULT '0',
  `symbol` varchar(144) DEFAULT '0',
  `pricechange` float DEFAULT '0',
  `prisechangepercentage` float DEFAULT '0',
  `weightedaverageprice` float DEFAULT '0',
  `totaltradebaseasset` float DEFAULT '0',
  `timestamp` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=99894 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_perchange_wz_maticinr`
--

DROP TABLE IF EXISTS `efx_perchange_wz_maticinr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_perchange_wz_maticinr` (
  `id` int NOT NULL AUTO_INCREMENT,
  `timestamp_wz` longtext,
  `open` float DEFAULT NULL,
  `close` float DEFAULT NULL,
  `high` float DEFAULT NULL,
  `low` float DEFAULT NULL,
  `volume` float DEFAULT '0',
  `symbol` varchar(144) DEFAULT '0',
  `pricechange` float DEFAULT '0',
  `prisechangepercentage` float DEFAULT '0',
  `weightedaverageprice` float DEFAULT '0',
  `totaltradebaseasset` float DEFAULT '0',
  `timestamp` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=112428 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_perchange_wz_mdxinr`
--

DROP TABLE IF EXISTS `efx_perchange_wz_mdxinr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_perchange_wz_mdxinr` (
  `id` int NOT NULL AUTO_INCREMENT,
  `timestamp_wz` longtext,
  `open` float DEFAULT NULL,
  `close` float DEFAULT NULL,
  `high` float DEFAULT NULL,
  `low` float DEFAULT NULL,
  `volume` float DEFAULT '0',
  `symbol` varchar(144) DEFAULT '0',
  `pricechange` float DEFAULT '0',
  `prisechangepercentage` float DEFAULT '0',
  `weightedaverageprice` float DEFAULT '0',
  `totaltradebaseasset` float DEFAULT '0',
  `timestamp` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=97113 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_perchange_wz_omginr`
--

DROP TABLE IF EXISTS `efx_perchange_wz_omginr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_perchange_wz_omginr` (
  `id` int NOT NULL AUTO_INCREMENT,
  `timestamp_wz` longtext,
  `open` float DEFAULT NULL,
  `close` float DEFAULT NULL,
  `high` float DEFAULT NULL,
  `low` float DEFAULT NULL,
  `volume` float DEFAULT '0',
  `symbol` varchar(144) DEFAULT '0',
  `pricechange` float DEFAULT '0',
  `prisechangepercentage` float DEFAULT '0',
  `weightedaverageprice` float DEFAULT '0',
  `totaltradebaseasset` float DEFAULT '0',
  `timestamp` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=94438 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_perchange_wz_shibinr`
--

DROP TABLE IF EXISTS `efx_perchange_wz_shibinr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_perchange_wz_shibinr` (
  `id` int NOT NULL AUTO_INCREMENT,
  `timestamp_wz` longtext,
  `open` float DEFAULT NULL,
  `close` float DEFAULT NULL,
  `high` float DEFAULT NULL,
  `low` float DEFAULT NULL,
  `volume` float DEFAULT '0',
  `symbol` varchar(144) DEFAULT '0',
  `pricechange` float DEFAULT '0',
  `prisechangepercentage` float DEFAULT '0',
  `weightedaverageprice` float DEFAULT '0',
  `totaltradebaseasset` float DEFAULT '0',
  `timestamp` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=103132 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_perchange_wz_sushiinr`
--

DROP TABLE IF EXISTS `efx_perchange_wz_sushiinr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_perchange_wz_sushiinr` (
  `id` int NOT NULL AUTO_INCREMENT,
  `timestamp_wz` longtext,
  `open` float DEFAULT NULL,
  `close` float DEFAULT NULL,
  `high` float DEFAULT NULL,
  `low` float DEFAULT NULL,
  `volume` float DEFAULT '0',
  `symbol` varchar(144) DEFAULT '0',
  `pricechange` float DEFAULT '0',
  `prisechangepercentage` float DEFAULT '0',
  `weightedaverageprice` float DEFAULT '0',
  `totaltradebaseasset` float DEFAULT '0',
  `timestamp` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=94320 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_perchange_wz_trxinr`
--

DROP TABLE IF EXISTS `efx_perchange_wz_trxinr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_perchange_wz_trxinr` (
  `id` int NOT NULL AUTO_INCREMENT,
  `timestamp_wz` longtext,
  `open` float DEFAULT NULL,
  `close` float DEFAULT NULL,
  `high` float DEFAULT NULL,
  `low` float DEFAULT NULL,
  `volume` float DEFAULT '0',
  `symbol` varchar(144) DEFAULT '0',
  `pricechange` float DEFAULT '0',
  `prisechangepercentage` float DEFAULT '0',
  `weightedaverageprice` float DEFAULT '0',
  `totaltradebaseasset` float DEFAULT '0',
  `timestamp` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=109179 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_perchange_wz_uniinr`
--

DROP TABLE IF EXISTS `efx_perchange_wz_uniinr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_perchange_wz_uniinr` (
  `id` int NOT NULL AUTO_INCREMENT,
  `timestamp_wz` longtext,
  `open` float DEFAULT NULL,
  `close` float DEFAULT NULL,
  `high` float DEFAULT NULL,
  `low` float DEFAULT NULL,
  `volume` float DEFAULT '0',
  `symbol` varchar(144) DEFAULT '0',
  `pricechange` float DEFAULT '0',
  `prisechangepercentage` float DEFAULT '0',
  `weightedaverageprice` float DEFAULT '0',
  `totaltradebaseasset` float DEFAULT '0',
  `timestamp` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=99252 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_perchange_wz_wrxinr`
--

DROP TABLE IF EXISTS `efx_perchange_wz_wrxinr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_perchange_wz_wrxinr` (
  `id` int NOT NULL AUTO_INCREMENT,
  `timestamp_wz` longtext,
  `open` float DEFAULT NULL,
  `close` float DEFAULT NULL,
  `high` float DEFAULT NULL,
  `low` float DEFAULT NULL,
  `volume` float DEFAULT '0',
  `symbol` varchar(144) DEFAULT '0',
  `pricechange` float DEFAULT '0',
  `prisechangepercentage` float DEFAULT '0',
  `weightedaverageprice` float DEFAULT '0',
  `totaltradebaseasset` float DEFAULT '0',
  `timestamp` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101161 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_perchange_wz_xrpinr`
--

DROP TABLE IF EXISTS `efx_perchange_wz_xrpinr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_perchange_wz_xrpinr` (
  `id` int NOT NULL AUTO_INCREMENT,
  `timestamp_wz` longtext,
  `open` float DEFAULT NULL,
  `close` float DEFAULT NULL,
  `high` float DEFAULT NULL,
  `low` float DEFAULT NULL,
  `volume` float DEFAULT '0',
  `symbol` varchar(144) DEFAULT '0',
  `pricechange` float DEFAULT '0',
  `prisechangepercentage` float DEFAULT '0',
  `weightedaverageprice` float DEFAULT '0',
  `totaltradebaseasset` float DEFAULT '0',
  `timestamp` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=117335 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_set_price`
--

DROP TABLE IF EXISTS `efx_set_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_set_price` (
  `id` int NOT NULL AUTO_INCREMENT,
  `current_price` double NOT NULL DEFAULT '0',
  `total_supply` double NOT NULL DEFAULT '0',
  `total_supplied` double NOT NULL DEFAULT '0',
  `is_maintenance` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_tickets`
--

DROP TABLE IF EXISTS `efx_tickets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_tickets` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL DEFAULT '0',
  `ticket_id` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `attachment` mediumtext,
  `subject` mediumtext,
  `description` mediumtext,
  `status` varchar(20) DEFAULT 'open' COMMENT 'open, process, close',
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_tickets_chat`
--

DROP TABLE IF EXISTS `efx_tickets_chat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_tickets_chat` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL DEFAULT '0',
  `ticket_id` varchar(255) DEFAULT NULL,
  `description` mediumtext,
  `from` varchar(10) DEFAULT 'user' COMMENT 'user, admin',
  `image` varchar(225) DEFAULT NULL,
  `is_read` int DEFAULT '0' COMMENT '0=not read, 1= read',
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_transaction_history`
--

DROP TABLE IF EXISTS `efx_transaction_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_transaction_history` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` varchar(11) DEFAULT NULL,
  `member_id` varchar(20) NOT NULL,
  `type` tinyint(1) DEFAULT NULL COMMENT '1=Fund,2=Transfer,3=Withdrawal',
  `log` varchar(10) DEFAULT NULL COMMENT 'credit, debit',
  `amount` int DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `txn_id` varchar(15) DEFAULT NULL,
  `created_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_transfer_history`
--

DROP TABLE IF EXISTS `efx_transfer_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_transfer_history` (
  `id` int NOT NULL AUTO_INCREMENT,
  `from_user` int NOT NULL DEFAULT '0',
  `to_user` int NOT NULL,
  `transfer_charge` float NOT NULL DEFAULT '0' COMMENT 'in %',
  `amount` double NOT NULL DEFAULT '0',
  `transfer_type` varchar(5) NOT NULL DEFAULT 'EFX',
  `transfer_by` varchar(255) DEFAULT NULL COMMENT 'admin, user',
  `created_datetime` datetime DEFAULT NULL,
  `status` int NOT NULL DEFAULT '0',
  `txn_id` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb3 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_unlock_efx`
--

DROP TABLE IF EXISTS `efx_unlock_efx`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_unlock_efx` (
  `id` int NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_user_notification`
--

DROP TABLE IF EXISTS `efx_user_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_user_notification` (
  `id` int NOT NULL AUTO_INCREMENT,
  `from_user` int NOT NULL DEFAULT '0',
  `to_user` int NOT NULL DEFAULT '0',
  `type` varchar(20) DEFAULT NULL COMMENT '1=Fund,2=Transfer,3=Withdrawal ',
  `payment_type` varchar(10) DEFAULT NULL,
  `response_data` mediumtext,
  `log` varchar(10) DEFAULT NULL COMMENT 'credit, debit',
  `amount` double DEFAULT '0',
  `pay_amount` double NOT NULL DEFAULT '0',
  `balance` varchar(255) DEFAULT NULL,
  `txn_id` varchar(100) DEFAULT NULL,
  `message` text,
  `status` int DEFAULT '0',
  `is_read` int NOT NULL DEFAULT '0' COMMENT '1=read, 0=not read',
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=519 DEFAULT CHARSET=utf8mb3 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_users`
--

DROP TABLE IF EXISTS `efx_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `token` text,
  `full_name` varchar(100) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `password_decrypted` varchar(100) DEFAULT NULL,
  `is_sponsor` tinyint NOT NULL DEFAULT '0' COMMENT '1=yes, 2=no',
  `sponsor` varchar(50) DEFAULT NULL,
  `sponsor_id` int DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `login_country` varchar(100) DEFAULT NULL,
  `state` varchar(10) NOT NULL DEFAULT '0',
  `city` int NOT NULL DEFAULT '0',
  `address` text,
  `package` tinyint(1) DEFAULT '0',
  `upgrade_time` datetime DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `upgrade_agency` datetime DEFAULT NULL,
  `upgrade_bottrade` datetime NOT NULL,
  `modified_time` datetime DEFAULT NULL,
  `lwr` int NOT NULL DEFAULT '10',
  `coin` varchar(6) NOT NULL DEFAULT 'BTC',
  `wallet_amount` double NOT NULL DEFAULT '0',
  `unlocked_efx` double NOT NULL DEFAULT '0',
  `efx_wallet` double NOT NULL DEFAULT '0',
  `demo_wallet_amount` float NOT NULL DEFAULT '1000',
  `copy_trade` tinyint NOT NULL DEFAULT '0' COMMENT '1=active;0=inactive',
  `copiers` double NOT NULL DEFAULT '0',
  `profit_share` double NOT NULL DEFAULT '0' COMMENT 'number in percentage',
  `min_invest` double NOT NULL DEFAULT '0' COMMENT 'amount in USD',
  `users` text,
  `total_user` double NOT NULL DEFAULT '0',
  `active_user` double NOT NULL DEFAULT '0',
  `is_profile_update` tinyint NOT NULL DEFAULT '0' COMMENT '1=updated by the user ',
  `is_delete` tinyint NOT NULL DEFAULT '1' COMMENT '1=not deleted, 0=deleted',
  `is_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=Active, 0=In Active ',
  `trade_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=Demo, 0=Live',
  `is_agency` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1=true; 0=false',
  `is_bottrade` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1=true; 0=false',
  `is_android_bonus` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=Not get, 1=get',
  `google_auth_status` varchar(5) NOT NULL DEFAULT 'false',
  `google_auth_code` varchar(50) DEFAULT NULL,
  `my_os` varchar(15) DEFAULT NULL,
  `is_play` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=not played, 1=played',
  `grouping` tinyint NOT NULL DEFAULT '30',
  `binance_apikey` longtext NOT NULL,
  `binance_securitykey` longtext NOT NULL,
  `binance_apikey_active_check` int NOT NULL DEFAULT '0',
  `copy_trade_forbot` tinyint(1) NOT NULL DEFAULT '0',
  `new_listen_key` longtext,
  `wz_apikey` longtext,
  `wz_sekey` longtext,
  `copy_trade_future` tinyint DEFAULT '0',
  `copytrade_future_profit` double DEFAULT NULL,
  `copytrade_future_mininv` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=434 DEFAULT CHARSET=utf8mb3 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_users_bank_details`
--

DROP TABLE IF EXISTS `efx_users_bank_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_users_bank_details` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL DEFAULT '0',
  `bank_name` varchar(255) DEFAULT NULL,
  `branch_name` varchar(255) DEFAULT NULL,
  `ifsc_code` varchar(255) DEFAULT NULL,
  `account_holder_name` varchar(255) DEFAULT NULL,
  `account_no` varchar(255) DEFAULT NULL,
  `btc_address` text,
  `eth_address` varchar(200) DEFAULT NULL,
  `ltc_address` varchar(200) DEFAULT NULL,
  `trx_address` varchar(45) DEFAULT NULL,
  `is_user_update` tinyint NOT NULL DEFAULT '0' COMMENT '1= updated by the user',
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_users_document`
--

DROP TABLE IF EXISTS `efx_users_document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_users_document` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL DEFAULT '0',
  `fname` varchar(50) DEFAULT NULL,
  `lname` varchar(50) DEFAULT NULL,
  `document_id` varchar(50) DEFAULT NULL,
  `document_type` varchar(255) DEFAULT NULL COMMENT 'photo, id, address, cheque',
  `id_proof` text,
  `document_file` text,
  `approved_datetime` datetime DEFAULT NULL,
  `reject_datetime` datetime DEFAULT NULL,
  `reject_reason` text,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  `status` int DEFAULT '0' COMMENT '0=pending, 1= approved or verified, 2=rejected',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_users_session_data`
--

DROP TABLE IF EXISTS `efx_users_session_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_users_session_data` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `user_email` varchar(150) NOT NULL,
  `full_name` varchar(50) DEFAULT NULL,
  `trade_type` tinyint(1) DEFAULT NULL,
  `logsid` int DEFAULT NULL,
  `language` varchar(45) DEFAULT NULL,
  `lang` varchar(45) DEFAULT NULL,
  `login_type` longtext,
  `message` mediumtext,
  `browser_name` mediumtext,
  `browser_image` mediumtext,
  `ip_address` varchar(255) DEFAULT NULL,
  `os` varchar(15) DEFAULT NULL,
  `device` varchar(10) DEFAULT NULL,
  `status` tinyint DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `created_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=97 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_wazirx`
--

DROP TABLE IF EXISTS `efx_wazirx`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_wazirx` (
  `id` int NOT NULL AUTO_INCREMENT,
  `timestamp` longtext NOT NULL,
  `adainr_open` double NOT NULL,
  `adainr_close` double NOT NULL,
  `adainr_high` double NOT NULL,
  `adainr_low` double NOT NULL,
  `adainr_volume` double NOT NULL DEFAULT '0',
  `adainr_symbol` varchar(144) NOT NULL DEFAULT '0',
  `adainr_pricechange` double NOT NULL DEFAULT '0',
  `atominr_open` double NOT NULL,
  `atominr_close` double NOT NULL,
  `atominr_high` double NOT NULL,
  `atominr_low` double NOT NULL,
  `atominr_volume` double NOT NULL DEFAULT '0',
  `atominr_symbol` varchar(144) NOT NULL DEFAULT '0',
  `atominr_pricechange` double NOT NULL DEFAULT '0',
  `batinr_open` double NOT NULL,
  `batinr_close` double NOT NULL,
  `batinr_high` double NOT NULL,
  `batinr_low` double NOT NULL,
  `batinr_volume` double NOT NULL DEFAULT '0',
  `batinr_symbol` varchar(144) NOT NULL DEFAULT '0',
  `batinr_pricechange` double NOT NULL DEFAULT '0',
  `bchinr_open` double NOT NULL,
  `bchinr_close` double NOT NULL,
  `bchinr_high` double NOT NULL,
  `bchinr_low` double NOT NULL,
  `bchinr_volume` double NOT NULL DEFAULT '0',
  `bchinr_symbol` varchar(144) NOT NULL DEFAULT '0',
  `bchinr_pricechange` double NOT NULL DEFAULT '0',
  `bnbinr_open` double NOT NULL,
  `bnbinr_close` double NOT NULL,
  `bnbinr_high` double NOT NULL,
  `bnbinr_low` double NOT NULL,
  `bnbinr_volume` double NOT NULL DEFAULT '0',
  `bnbinr_symbol` varchar(144) NOT NULL DEFAULT '0',
  `bnbinr_pricechange` double NOT NULL DEFAULT '0',
  `btcinr_open` double NOT NULL,
  `btcinr_close` double NOT NULL,
  `btcinr_high` double NOT NULL,
  `btcinr_low` double NOT NULL,
  `btcinr_volume` double NOT NULL DEFAULT '0',
  `btcinr_symbol` varchar(144) NOT NULL DEFAULT '0',
  `btcinr_pricechange` double NOT NULL DEFAULT '0',
  `cakeinr_open` double NOT NULL,
  `cakeinr_close` double NOT NULL,
  `cakeinr_high` double NOT NULL,
  `cakeinr_low` double NOT NULL,
  `cakeinr_volume` double NOT NULL DEFAULT '0',
  `cakeinr_symbol` varchar(144) NOT NULL DEFAULT '0',
  `cakeinr_pricechange` double NOT NULL DEFAULT '0',
  `compinr_open` double NOT NULL,
  `compinr_close` double NOT NULL,
  `compinr_high` double NOT NULL,
  `compinr_low` double NOT NULL,
  `compinr_volume` double NOT NULL DEFAULT '0',
  `compinr_symbol` varchar(144) NOT NULL DEFAULT '0',
  `compinr_pricechange` double NOT NULL DEFAULT '0',
  `crvinr_open` double NOT NULL,
  `crvinr_close` double NOT NULL,
  `crvinr_high` double NOT NULL,
  `crvinr_low` double NOT NULL,
  `crvinr_volume` double NOT NULL DEFAULT '0',
  `crvinr_symbol` varchar(144) NOT NULL DEFAULT '0',
  `crvinr_pricechange` double NOT NULL DEFAULT '0',
  `dashinr_open` double NOT NULL,
  `dashinr_close` double NOT NULL,
  `dashinr_high` double NOT NULL,
  `dashinr_low` double NOT NULL,
  `dashinr_volume` double NOT NULL DEFAULT '0',
  `dashinr_symbol` varchar(144) NOT NULL DEFAULT '0',
  `dashinr_pricechange` double NOT NULL DEFAULT '0',
  `dotinr_open` double NOT NULL,
  `dotinr_close` double NOT NULL,
  `dotinr_high` double NOT NULL,
  `dotinr_low` double NOT NULL,
  `dotinr_volume` double NOT NULL DEFAULT '0',
  `dotinr_symbol` varchar(144) NOT NULL DEFAULT '0',
  `dotinr_pricechange` double NOT NULL DEFAULT '0',
  `eosinr_open` double NOT NULL,
  `eosinr_close` double NOT NULL,
  `eosinr_high` double NOT NULL,
  `eosinr_low` double NOT NULL,
  `eosinr_volume` double NOT NULL DEFAULT '0',
  `eosinr_symbol` varchar(144) NOT NULL DEFAULT '0',
  `eosinr_pricechange` double NOT NULL DEFAULT '0',
  `ethinr_open` double NOT NULL,
  `ethinr_close` double NOT NULL,
  `ethinr_high` double NOT NULL,
  `ethinr_low` double NOT NULL,
  `ethinr_volume` double NOT NULL DEFAULT '0',
  `ethinr_symbol` varchar(144) NOT NULL DEFAULT '0',
  `ethinr_pricechange` double NOT NULL DEFAULT '0',
  `etcinr_open` double NOT NULL,
  `etcinr_close` double NOT NULL,
  `etcinr_high` double NOT NULL,
  `etcinr_low` double NOT NULL,
  `etcinr_volume` double NOT NULL DEFAULT '0',
  `etcinr_symbol` varchar(144) NOT NULL DEFAULT '0',
  `etcinr_pricechange` double NOT NULL DEFAULT '0',
  `filinr_open` double NOT NULL,
  `filinr_close` double NOT NULL,
  `filinr_high` double NOT NULL,
  `filinr_low` double NOT NULL,
  `filinr_volume` double NOT NULL DEFAULT '0',
  `filinr_symbol` varchar(144) NOT NULL DEFAULT '0',
  `filinr_pricechange` double NOT NULL DEFAULT '0',
  `fttinr_open` double NOT NULL,
  `fttinr_close` double NOT NULL,
  `fttinr_high` double NOT NULL,
  `fttinr_low` double NOT NULL,
  `fttinr_volume` double NOT NULL DEFAULT '0',
  `fttinr_symbol` varchar(144) NOT NULL DEFAULT '0',
  `fttinr_pricechange` double NOT NULL DEFAULT '0',
  `iostinr_open` double NOT NULL,
  `iostinr_close` double NOT NULL,
  `iostinr_high` double NOT NULL,
  `iostinr_low` double NOT NULL,
  `iostinr_volume` double NOT NULL DEFAULT '0',
  `iostinr_symbol` varchar(144) NOT NULL DEFAULT '0',
  `iostinr_pricechange` double NOT NULL DEFAULT '0',
  `linkinr_open` double NOT NULL,
  `linkinr_close` double NOT NULL,
  `linkinr_high` double NOT NULL,
  `linkinr_low` double NOT NULL,
  `linkinr_volume` double NOT NULL DEFAULT '0',
  `linkinr_symbol` varchar(144) NOT NULL DEFAULT '0',
  `linkinr_pricechange` double NOT NULL DEFAULT '0',
  `ltcinr_open` double NOT NULL,
  `ltcinr_close` double NOT NULL,
  `ltcinr_high` double NOT NULL,
  `ltcinr_low` double NOT NULL,
  `ltcinr_volume` double NOT NULL DEFAULT '0',
  `ltcinr_symbol` varchar(144) NOT NULL DEFAULT '0',
  `ltcinr_pricechange` double NOT NULL DEFAULT '0',
  `manainr_open` double NOT NULL,
  `manainr_close` double NOT NULL,
  `manainr_high` double NOT NULL,
  `manainr_low` double NOT NULL,
  `manainr_volume` double NOT NULL DEFAULT '0',
  `manainr_symbol` varchar(144) NOT NULL DEFAULT '0',
  `manainr_pricechange` double NOT NULL DEFAULT '0',
  `mdxinr_open` double NOT NULL,
  `mdxinr_close` double NOT NULL,
  `mdxinr_high` double NOT NULL,
  `mdxinr_low` double NOT NULL,
  `mdxinr_volume` double NOT NULL DEFAULT '0',
  `mdxinr_symbol` varchar(144) NOT NULL DEFAULT '0',
  `mdxinr_pricechange` double NOT NULL DEFAULT '0',
  `omginr_open` double NOT NULL,
  `omginr_close` double NOT NULL,
  `omginr_high` double NOT NULL,
  `omginr_low` double NOT NULL,
  `omginr_volume` double NOT NULL DEFAULT '0',
  `omginr_symbol` varchar(144) NOT NULL DEFAULT '0',
  `omginr_pricechange` double NOT NULL DEFAULT '0',
  `sushiinr_open` double NOT NULL,
  `sushiinr_close` double NOT NULL,
  `sushiinr_high` double NOT NULL,
  `sushiinr_low` double NOT NULL,
  `sushiinr_volume` double NOT NULL DEFAULT '0',
  `sushiinr_symbol` varchar(144) NOT NULL DEFAULT '0',
  `sushiinr_pricechange` double NOT NULL DEFAULT '0',
  `trxinr_open` double NOT NULL,
  `trxinr_close` double NOT NULL,
  `trxinr_high` double NOT NULL,
  `trxinr_low` double NOT NULL,
  `trxinr_volume` double NOT NULL DEFAULT '0',
  `trxinr_symbol` varchar(144) NOT NULL DEFAULT '0',
  `trxinr_pricechange` double NOT NULL DEFAULT '0',
  `uniinr_open` double NOT NULL,
  `uniinr_close` double NOT NULL,
  `uniinr_high` double NOT NULL,
  `uniinr_low` double NOT NULL,
  `uniinr_volume` double NOT NULL DEFAULT '0',
  `uniinr_symbol` varchar(144) NOT NULL DEFAULT '0',
  `uniinr_pricechange` double NOT NULL DEFAULT '0',
  `xrpinr_open` double NOT NULL,
  `xrpinr_close` double NOT NULL,
  `xrpinr_high` double NOT NULL,
  `xrpinr_low` double NOT NULL,
  `xrpinr_volume` double NOT NULL DEFAULT '0',
  `xrpinr_symbol` varchar(144) NOT NULL DEFAULT '0',
  `xrpinr_pricechange` double NOT NULL DEFAULT '0',
  `shibinr_open` double NOT NULL,
  `shibinr_close` double NOT NULL,
  `shibinr_high` double NOT NULL,
  `shibinr_low` double NOT NULL,
  `shibinr_volume` double NOT NULL DEFAULT '0',
  `shibinr_symbol` varchar(144) NOT NULL DEFAULT '0',
  `shibinr_pricechange` double NOT NULL DEFAULT '0',
  `maticinr_open` double NOT NULL,
  `maticinr_close` double NOT NULL,
  `maticinr_high` double NOT NULL,
  `maticinr_low` double NOT NULL,
  `maticinr_volume` double NOT NULL DEFAULT '0',
  `maticinr_symbol` varchar(144) NOT NULL DEFAULT '0',
  `maticinr_pricechange` double NOT NULL DEFAULT '0',
  `celrinr_open` double NOT NULL,
  `celrinr_close` double NOT NULL,
  `celrinr_high` double NOT NULL,
  `celrinr_low` double NOT NULL,
  `celrinr_volume` double NOT NULL DEFAULT '0',
  `celrinr_symbol` varchar(144) NOT NULL DEFAULT '0',
  `celrinr_pricechange` double NOT NULL DEFAULT '0',
  `timestamp_wz` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_wazirx_bid`
--

DROP TABLE IF EXISTS `efx_wazirx_bid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_wazirx_bid` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `firstbuyinamout` float DEFAULT NULL,
  `margincalllimit` int DEFAULT NULL,
  `takieprofitratio` float DEFAULT NULL,
  `positiontakecall` float DEFAULT '0',
  `copy_bottradebid` int DEFAULT '0',
  `created_date` bigint DEFAULT NULL,
  `btctousdpointaction` float DEFAULT NULL,
  `buysell` int DEFAULT NULL,
  `copy_tradepercentage` double DEFAULT NULL,
  `set_profitratio_portfoli` float DEFAULT NULL,
  `set_profitratio_portfo_finalvalli` float DEFAULT NULL,
  `call_back_for_loss` float DEFAULT NULL,
  `stoplosslimite` float DEFAULT NULL,
  `check_log` int DEFAULT '0',
  `sell_point` float DEFAULT NULL,
  `stop_loss` float DEFAULT NULL,
  `sell_stop_limit` float DEFAULT NULL,
  `typeofpoint` varchar(144) DEFAULT NULL,
  `binance_apikey` longtext,
  `binance_securitykey` longtext,
  `conditionfirst` int DEFAULT '0',
  `total_stratage` int DEFAULT NULL,
  `stratage_id` int DEFAULT NULL,
  `multiplebuyinratio` float DEFAULT NULL,
  `orderId` longtext,
  `side` longtext,
  `origQty` longtext,
  `status` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1152 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `efx_withdrawal`
--

DROP TABLE IF EXISTS `efx_withdrawal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `efx_withdrawal` (
  `id` int NOT NULL AUTO_INCREMENT,
  `session_id` int NOT NULL DEFAULT '0',
  `username` varchar(255) DEFAULT NULL,
  `total_amount` double DEFAULT NULL,
  `amount` double NOT NULL DEFAULT '0',
  `withdrawal_in` varchar(255) DEFAULT NULL COMMENT 'bank, btc',
  `admin_charge` double NOT NULL DEFAULT '0' COMMENT 'in %',
  `given_datetime` datetime DEFAULT NULL,
  `reject_reason` text,
  `reject_datetime` datetime DEFAULT NULL,
  `status` enum('P','G','R') NOT NULL DEFAULT 'P' COMMENT 'p=pending, G=given or approved, R= Rejected',
  `created_datetime` datetime DEFAULT NULL,
  `txn_id` varchar(20) DEFAULT NULL COMMENT 'Transaction -> ID',
  `comments` text,
  `pay_address` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb3 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ethdata`
--

DROP TABLE IF EXISTS `ethdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ethdata` (
  `id` int NOT NULL AUTO_INCREMENT,
  `open` float NOT NULL,
  `close` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `volume` float NOT NULL,
  `timestamp` bigint NOT NULL,
  `sec1` double NOT NULL DEFAULT '0',
  `sec2` double NOT NULL DEFAULT '0',
  `sec3` double NOT NULL DEFAULT '0',
  `sec4` double NOT NULL DEFAULT '0',
  `sec5` double NOT NULL DEFAULT '0',
  `sec6` double NOT NULL DEFAULT '0',
  `sec7` double NOT NULL DEFAULT '0',
  `sec8` double NOT NULL DEFAULT '0',
  `sec9` double NOT NULL DEFAULT '0',
  `sec10` double NOT NULL DEFAULT '0',
  `sec11` double NOT NULL DEFAULT '0',
  `sec12` double NOT NULL DEFAULT '0',
  `sec13` double NOT NULL DEFAULT '0',
  `sec14` double NOT NULL DEFAULT '0',
  `sec15` double NOT NULL DEFAULT '0',
  `sec16` double NOT NULL DEFAULT '0',
  `sec17` double NOT NULL DEFAULT '0',
  `sec18` double NOT NULL DEFAULT '0',
  `sec19` double NOT NULL DEFAULT '0',
  `sec20` double NOT NULL DEFAULT '0',
  `sec21` double NOT NULL DEFAULT '0',
  `sec22` double NOT NULL DEFAULT '0',
  `sec23` double NOT NULL DEFAULT '0',
  `sec24` double NOT NULL DEFAULT '0',
  `sec25` double NOT NULL DEFAULT '0',
  `sec26` double NOT NULL DEFAULT '0',
  `sec27` double NOT NULL DEFAULT '0',
  `sec28` double NOT NULL DEFAULT '0',
  `sec29` double NOT NULL DEFAULT '0',
  `sec30` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=581841 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `etx_condition_for_binancefuture`
--

DROP TABLE IF EXISTS `etx_condition_for_binancefuture`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `etx_condition_for_binancefuture` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `firstbuyinamout` float NOT NULL,
  `margincalllimit` int NOT NULL,
  `takieprofitratio` float NOT NULL,
  `positiontakecall` float NOT NULL DEFAULT '0',
  `copy_bottradebid` int NOT NULL DEFAULT '0',
  `created_date` bigint DEFAULT NULL,
  `btctousdpointaction` float NOT NULL,
  `buysell` int NOT NULL,
  `copy_tradepercentage` double NOT NULL,
  `set_profitratio_portfoli` float NOT NULL,
  `set_profitratio_portfo_finalvalli` float NOT NULL,
  `call_back_for_loss` float NOT NULL,
  `stoplosslimite` float NOT NULL,
  `check_log` int NOT NULL DEFAULT '0',
  `sell_point` float NOT NULL,
  `stop_loss` float NOT NULL,
  `sell_stop_limit` float NOT NULL,
  `typeofpoint` varchar(144) NOT NULL,
  `binance_apikey` longtext NOT NULL,
  `binance_securitykey` longtext NOT NULL,
  `conditionfirst` int NOT NULL DEFAULT '0',
  `total_stratage` int DEFAULT NULL,
  `stratage_id` int DEFAULT NULL,
  `multiplebuyinratio` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=887 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `etx_condition_for_btctousdt`
--

DROP TABLE IF EXISTS `etx_condition_for_btctousdt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `etx_condition_for_btctousdt` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `firstbuyinamout` float NOT NULL,
  `margincalllimit` int NOT NULL,
  `takieprofitratio` float NOT NULL,
  `positiontakecall` float NOT NULL DEFAULT '0',
  `copy_bottradebid` int NOT NULL DEFAULT '0',
  `created_date` bigint DEFAULT NULL,
  `btctousdpointaction` float NOT NULL,
  `buysell` int NOT NULL,
  `copy_tradepercentage` double NOT NULL,
  `set_profitratio_portfoli` float NOT NULL,
  `set_profitratio_portfo_finalvalli` float NOT NULL,
  `call_back_for_loss` float NOT NULL,
  `stoplosslimite` float NOT NULL,
  `check_log` int NOT NULL DEFAULT '0',
  `sell_point` float NOT NULL,
  `stop_loss` float NOT NULL,
  `sell_stop_limit` float NOT NULL,
  `typeofpoint` varchar(144) NOT NULL,
  `binance_apikey` longtext NOT NULL,
  `binance_securitykey` longtext NOT NULL,
  `conditionfirst` int NOT NULL DEFAULT '0',
  `total_stratage` int DEFAULT NULL,
  `stratage_id` int DEFAULT NULL,
  `multiplebuyinratio` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1143 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `multiple_bid_forbot`
--

DROP TABLE IF EXISTS `multiple_bid_forbot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `multiple_bid_forbot` (
  `id` int NOT NULL AUTO_INCREMENT,
  `buypoint` float DEFAULT NULL,
  `creatdate` int DEFAULT NULL,
  `buyamountmul` float DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `sell` float DEFAULT NULL,
  `sellstop` float DEFAULT NULL,
  `stratage_id` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=236 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `profit_from_bot`
--

DROP TABLE IF EXISTS `profit_from_bot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `profit_from_bot` (
  `id` int NOT NULL AUTO_INCREMENT,
  `total_profit` float DEFAULT '0',
  `date_whenget` tinyint DEFAULT NULL,
  `id_whom_give` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `store_date_for_tp_sl`
--

DROP TABLE IF EXISTS `store_date_for_tp_sl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `store_date_for_tp_sl` (
  `id` int NOT NULL AUTO_INCREMENT,
  `userid` int DEFAULT NULL,
  `created_date` bigint DEFAULT NULL,
  `order_id` varchar(445) DEFAULT NULL,
  `stoploss` double DEFAULT NULL,
  `takeprofit` double DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `buy_sell_done` varchar(45) DEFAULT NULL COMMENT 'buy sell done',
  `point_where_buysell` double DEFAULT NULL,
  `size` double DEFAULT NULL,
  `status` longtext,
  `coinname` varchar(45) DEFAULT NULL,
  `entry_price` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `time_cal_algo`
--

DROP TABLE IF EXISTS `time_cal_algo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `time_cal_algo` (
  `id` int NOT NULL AUTO_INCREMENT,
  `datetime_s` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `time_check_for_botsingle`
--

DROP TABLE IF EXISTS `time_check_for_botsingle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `time_check_for_botsingle` (
  `id` int NOT NULL,
  `timestamp` longtext NOT NULL,
  `timefor1st` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmp_bids`
--

DROP TABLE IF EXISTS `tmp_bids`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tmp_bids` (
  `id` int NOT NULL,
  `user` int NOT NULL,
  `winRatio` float NOT NULL,
  `lastWinRatio` float NOT NULL,
  `type` int NOT NULL,
  `amount` float NOT NULL,
  `total_amount` float NOT NULL DEFAULT '0',
  `coin` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `value` float NOT NULL,
  `closeVal` float NOT NULL,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `mCloseVal` float NOT NULL,
  `growth` int NOT NULL,
  `result` int NOT NULL,
  `timestamp` bigint NOT NULL,
  `sec` int NOT NULL DEFAULT '0',
  `peers` longtext CHARACTER SET utf8 COLLATE utf8_general_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_coin_info`
--

DROP TABLE IF EXISTS `user_coin_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_coin_info` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `AAVEUSDT_p` float DEFAULT NULL,
  `AAVEUSDT_qty` float DEFAULT NULL,
  `AAVEUSDT_quoteqty` float DEFAULT NULL,
  `ADAUSDT_p` float DEFAULT NULL,
  `ADAUSDT_qty` float DEFAULT NULL,
  `ADAUSDT_quoteqty` float DEFAULT NULL,
  `AKROUSDT_p` float DEFAULT NULL,
  `AKROUSDT_qty` float DEFAULT NULL,
  `AKROUSDT_quoteqty` float DEFAULT NULL,
  `ANTUSDT_p` float DEFAULT NULL,
  `ANTUSDT_qty` float DEFAULT NULL,
  `ANTUSDT_quoteqty` float DEFAULT NULL,
  `ATOMUSDT_p` float DEFAULT NULL,
  `ATOMUSDT_qty` float DEFAULT NULL,
  `ATOMUSDT_quoteqty` float DEFAULT NULL,
  `BATUSDT_p` float DEFAULT NULL,
  `BATUSDT_qty` float DEFAULT NULL,
  `BATUSDT_quoteqty` float DEFAULT NULL,
  `BCHUSDT_p` float DEFAULT NULL,
  `BCHUSDT_qty` float DEFAULT NULL,
  `BCHUSDT_quoteqty` float DEFAULT NULL,
  `BNBUSDT_p` float DEFAULT NULL,
  `BNBUSDT_qty` float DEFAULT NULL,
  `BNBUSDT_quoteqty` float DEFAULT NULL,
  `BTCUSDT_p` float DEFAULT NULL,
  `BTCUSDT_qty` float DEFAULT NULL,
  `BTCUSDT_quoteqty` float DEFAULT NULL,
  `CAKEUSDT_p` float DEFAULT NULL,
  `CAKEUSDT_qty` float DEFAULT NULL,
  `CAKEUSDT_quoteqty` float DEFAULT NULL,
  `COMPUSDT_p` float DEFAULT NULL,
  `COMPUSDT_qty` float DEFAULT NULL,
  `COMPUSDT_quoteqty` float DEFAULT NULL,
  `COSUSDT_p` float DEFAULT NULL,
  `COSUSDT_qty` float DEFAULT NULL,
  `COSUSDT_quoteqty` float DEFAULT NULL,
  `CRVUSDT_p` float DEFAULT NULL,
  `CRVUSDT_qty` float DEFAULT NULL,
  `CRVUSDT_quoteqty` float DEFAULT NULL,
  `DASHUSDT_p` float DEFAULT NULL,
  `DASHUSDT_qty` float DEFAULT NULL,
  `DASHUSDT_quoteqty` float DEFAULT NULL,
  `DOGEUSDT_p` float DEFAULT NULL,
  `DOGEUSDT_qty` float DEFAULT NULL,
  `DOGEUSDT_quoteqty` float DEFAULT NULL,
  `DOTUSDT_p` float DEFAULT NULL,
  `DOTUSDT_qty` float DEFAULT NULL,
  `DOTUSDT_quoteqty` float DEFAULT NULL,
  `EOSUSDT_p` float DEFAULT NULL,
  `EOSUSDT_qty` float DEFAULT NULL,
  `EOSUSDT_quoteqty` float DEFAULT NULL,
  `ETHUSDT_p` float DEFAULT NULL,
  `ETHUSDT_qty` float DEFAULT NULL,
  `ETHUSDT_quoteqty` float DEFAULT NULL,
  `ETCUSDT_p` float DEFAULT NULL,
  `ETCUSDT_qty` float DEFAULT NULL,
  `ETCUSDT_quoteqty` float DEFAULT NULL,
  `FILUSDT_p` float DEFAULT NULL,
  `FILUSDT_qty` float DEFAULT NULL,
  `FILUSDT_quoteqty` float DEFAULT NULL,
  `FTTUSDT_p` float DEFAULT NULL,
  `FTTUSDT_qty` float DEFAULT NULL,
  `FTTUSDT_quoteqty` float DEFAULT NULL,
  `GRTUSDT_p` float DEFAULT NULL,
  `GRTUSDT_qty` float DEFAULT NULL,
  `GRTUSDT_quoteqty` float DEFAULT NULL,
  `IOSTUSDT_p` float DEFAULT NULL,
  `IOSTUSDT_qty` float DEFAULT NULL,
  `IOSTUSDT_quoteqty` float DEFAULT NULL,
  `IOTAUSDT_p` float DEFAULT NULL,
  `IOTAUSDT_qty` float DEFAULT NULL,
  `IOTAUSDT_quoteqty` float DEFAULT NULL,
  `JSTUSDT_p` float DEFAULT NULL,
  `JSTUSDT_qty` float DEFAULT NULL,
  `JSTUSDT_quoteqty` float DEFAULT NULL,
  `KAVAUSDT_p` float DEFAULT NULL,
  `KAVAUSDT_qty` float DEFAULT NULL,
  `KAVAUSDT_quoteqty` float DEFAULT NULL,
  `LINKUSDT_p` float DEFAULT NULL,
  `LINKUSDT_qty` float DEFAULT NULL,
  `LINKUSDT_quoteqty` float DEFAULT NULL,
  `LITUSDT_p` float DEFAULT NULL,
  `LITUSDT_qty` float DEFAULT NULL,
  `LITUSDT_quoteqty` float DEFAULT NULL,
  `LTCUSDT_p` float DEFAULT NULL,
  `LTCUSDT_qty` float DEFAULT NULL,
  `LTCUSDT_quoteqty` float DEFAULT NULL,
  `MANAUSDT_p` float DEFAULT NULL,
  `MANAUSDT_qty` float DEFAULT NULL,
  `MANAUSDT__quoteqty` float DEFAULT NULL,
  `MDXUSDT_p` float DEFAULT NULL,
  `MDXUSDT_qty` float DEFAULT NULL,
  `MDXUSDT_quoteqty` float DEFAULT NULL,
  `NEOUSDT_p` float DEFAULT NULL,
  `NEOUSDT_qty` float DEFAULT NULL,
  `NEOUSDT_quoteqty` float DEFAULT NULL,
  `OMGUSDT_p` float DEFAULT NULL,
  `OMGUSDT_qty` float DEFAULT NULL,
  `OMGUSDT_quoteqty` float DEFAULT NULL,
  `SUSHIUSDT_p` float DEFAULT NULL,
  `SUSHIUSDT_qty` float DEFAULT NULL,
  `SUSHIUSDT_quoteqty` float DEFAULT NULL,
  `THETAUSDT_p` float DEFAULT NULL,
  `THETAUSDT_qty` float DEFAULT NULL,
  `THETAUSDT_quoteqty` float DEFAULT NULL,
  `TRXUSDT_p` float DEFAULT NULL,
  `TRXUSDT_qty` float DEFAULT NULL,
  `TRXUSDT_quoteqty` float DEFAULT NULL,
  `UNIUSDT_p` float DEFAULT NULL,
  `UNIUSDT_qty` float DEFAULT NULL,
  `UNIUSDT_quoteqty` float DEFAULT NULL,
  `XMRUSDT_p` float DEFAULT NULL,
  `XMRUSDT_qty` float DEFAULT NULL,
  `XMRUSDT_quoteqty` float DEFAULT NULL,
  `XRPUSDT_p` float DEFAULT NULL,
  `XRPUSDT_qty` float DEFAULT NULL,
  `XRPUSDT_quoteqty` float DEFAULT NULL,
  `XTZUSDT_p` float DEFAULT NULL,
  `XTZUSDT_qty` float DEFAULT NULL,
  `XTZUSDT_quoteqty` float DEFAULT NULL,
  `RVNUSDT_p` float DEFAULT NULL,
  `RVNUSDT_qty` float DEFAULT NULL,
  `RVNUSDT_quoteqty` float DEFAULT NULL,
  `SHIBUSDT_p` float DEFAULT NULL,
  `SHIBUSDT_qty` float DEFAULT NULL,
  `SHIBUSDT_quoteqty` float DEFAULT NULL,
  `MATICUSDT_p` float DEFAULT NULL,
  `MATICUSDT_qty` float DEFAULT NULL,
  `MATICUSDT_quoteqty` float DEFAULT NULL,
  `CELRUSDT_p` float DEFAULT NULL,
  `CELRUSDT_qty` float DEFAULT NULL,
  `CELRUSDT_quoteqty` float DEFAULT NULL,
  `creattime` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-06-11 18:30:05
